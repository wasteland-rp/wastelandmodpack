## About The Pull Request
<!-- Describe what you did. -->
<!-- You can use dashes for bullet points. -->


## Pre-Merge Checklist
<!-- Dont check these here. Wait until you have made the merge request. -->
- [ ] Items/Models/Basic Lua: You at least tested this mod in singleplayer Zomboid
- [ ] Lua heavy mods: You hosted a local server with similar settings to Wasteland and tested your code works
- [ ] Weapons/Armor/Traits: A member of the balance team checked your numbers (Jordan for Armor, Mallas for Weapons, Zoomies for Occupations and Traits)
- [ ] You know to @ modding supervisors for server updates if needed after merge

<!-- It is important to test your code/feature/fix locally! -->
