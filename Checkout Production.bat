:: Run this to fetch the most recent tags and checkout the production commit
:: That commit is what is currently running on the server and so will not cause lua mismatch errors when you join
git fetch --tags --force
git checkout production