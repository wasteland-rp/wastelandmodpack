local function hitCrossbow(attacker,target,weapon,damage)
    local ammoType = weapon:getAmmoType();

    if ammoType ~= nil then
        --If ammoType is nil, it's a melee attack, do nothing
        if ammoType == "KCMweapons.CrossbowBoltLarge" then
            --large bolt, get the targets data.
            local modData = target:getModData();
            --Check if there's already a variable for large bolts, if not create it, if there is add to it.
            if modData.LCquarrels == nil then
                modData.LCquarrels = 1;
            else
                modData.LCquarrels = modData.LCquarrels + 1;
            end
        end

        if ammoType == "KCMweapons.CrossbowBolt" then
            --short bolt
            local modData = target:getModData();
            if modData.LCquarrels2 == nil then
                modData.LCquarrels2 = 1;
            else
                modData.LCquarrels2 = modData.LCquarrels2 + 1;
            end
        end

        if ammoType == "KCMweapons.WoodenBolt" then
            --wooden bolt
            local modData = target:getModData();
            if modData.LCquarrels3 == nil then
                modData.LCquarrels3 = 1;
            else
                modData.LCquarrels3 = modData.LCquarrels3 + 1;
            end
        end
    end
end



local function KCMOnEquiPrimary(player,item)
    if item ~= nil then
        local itemType = item:getType();
        if itemType == "HandCrossbow" or itemType == "KCM_Compound" or itemType == "KCM_Compound02" or itemType == "KCM_Handmade" or itemType == "KCM_Handmade02" then
            if item:getCurrentAmmoCount() > 0 then
                if item:getType() == "HandCrossbow" then item:setWeaponSprite("KCMweapons.HandCrossbowDrawn"); end
                if item:getType() == "KCM_Compound" then item:setWeaponSprite("KCMweapons.KCM_CompoundDrawn"); end
                if item:getType() == "KCM_Compound02" then item:setWeaponSprite("KCMweapons.KCM_CompoundDrawn02"); end
                if item:getType() == "KCM_Handmade" then item:setWeaponSprite("KCMweapons.KCM_HandmadeDrawn"); end
                if item:getType() == "KCM_Handmade02" then item:setWeaponSprite("KCMweapons.KCM_HandmadeDrawn02"); end
                player:resetEquippedHandsModels();
            end
        end
    end
end

local function KCMOnLoad()
    local player = getPlayer();
    local item = player:getPrimaryHandItem();
    if item ~= nil then
        local itemType = item:getType();
        if itemType == "HandCrossbow" or itemType == "KCM_Compound" or itemType == "KCM_Compound02" or itemType == "KCM_Handmade" or itemType == "KCM_Handmade02"  then
            if item:getCurrentAmmoCount() > 0 then
                if item:getType() == "HandCrossbow" then item:setWeaponSprite("KCMweapons.HandCrossbowDrawn"); end
                if item:getType() == "KCM_Compound" then item:setWeaponSprite("KCMweapons.KCM_CompoundDrawn"); end
                if item:getType() == "KCM_Compound02" then item:setWeaponSprite("KCMweapons.KCM_CompoundDrawn02"); end
                if item:getType() == "KCM_Handmade" then item:setWeaponSprite("KCMweapons.KCM_HandmadeDrawn"); end
                if item:getType() == "KCM_Handmade02" then item:setWeaponSprite("KCMweapons.KCM_HandmadeDrawn02"); end
                player:resetEquippedHandsModels();
            end
        end
    end
    local modData = player:getModData();
end

local function KCMOnZombieDead(zombie)
    local modData = zombie:getModData();

    if modData.LCquarrels ~= nil then
        for i = 1,modData.LCquarrels, 1
        do
            local luckyNumber = ZombRand(1,100);
            print(luckyNumber)

            local bolt

            if luckyNumber <= 90 then
                bolt = zombie:getInventory():AddItem("KCMweapons.CrossbowBoltLarge");
            else
                bolt = zombie:getInventory():AddItem("KCMweapons.LongBrokenBolt");
            end
        end
        modData.LCquarrels = 0;
    end

    if modData.LCquarrels2 ~= nil then
        for i = 1,modData.LCquarrels2, 1
        do
            local luckyNumber = ZombRand(1,100);
            print(luckyNumber)

            local bolt

            if luckyNumber <= 90 then
                bolt = zombie:getInventory():AddItem("KCMweapons.CrossbowBolt");
            else
                bolt = zombie:getInventory():AddItem("KCMweapons.ShortBrokenBolt");
            end
        end
        modData.LCquarrels2 = 0;
    end

    if modData.LCquarrels3 ~= nil then
        for i = 1,modData.LCquarrels3, 1
        do
            local luckyNumber = ZombRand(1,100);
            print(luckyNumber)

            local bolt

            if luckyNumber <= 75 then
                bolt = zombie:getInventory():AddItem("KCMweapons.WoodenBolt");
            else
                bolt = zombie:getInventory():AddItem("KCMweapons.WoodenBrokenBolt");
            end

        end
        modData.LCquarrels3 = 0;
    end
end


Events.OnWeaponSwingHitPoint.Remove(ISReloadWeaponAction.onShoot);
local original_onShoot = ISReloadWeaponAction.onShoot
-- shoot shoot bang bang
-- handle ammo removal, new chamber & jam chance
ISReloadWeaponAction.onShoot = function(player, weapon)
    if not weapon:isRanged() then return; end
    if weapon:getType() == "HandCrossbow" or weapon:getType() == "KCM_Compound" or weapon:getType() == "KCM_Compound02" or weapon:getType() == "KCM_Handmade" or weapon:getType() == "KCM_Handmade02" then
        if weapon:haveChamber() then
            weapon:setRoundChambered(false);
        end
        -- remove ammo, add one to chamber if we still have some
        if weapon:getCurrentAmmoCount() >= weapon:getAmmoPerShoot() then
            if weapon:haveChamber() then
                weapon:setRoundChambered(true);
            end
            weapon:setCurrentAmmoCount(weapon:getCurrentAmmoCount() - weapon:getAmmoPerShoot())
        end
        if weapon:isRackAfterShoot() then -- shotgun need to be rack after each shot to rechamber round
            player:setVariable("RackWeapon", weapon:getWeaponReloadType());
        end
        if weapon:getType() == "HandCrossbow" then weapon:setWeaponSprite("KCMweapons.HandCrossbow"); end
        if weapon:getType() == "KCM_Compound" then weapon:setWeaponSprite("KCMweapons.KCM_Compound"); end
        if weapon:getType() == "KCM_Compound02" then weapon:setWeaponSprite("KCMweapons.KCM_Compound02"); end
        if weapon:getType() == "KCM_Handmade" then weapon:setWeaponSprite("KCMweapons.KCM_Handmade"); end
        if weapon:getType() == "KCM_Handmade02" then weapon:setWeaponSprite("KCMweapons.KCM_Handmade02"); end
        player:resetEquippedHandsModels();
    else
        original_onShoot(player, weapon);
    end
end

Events.OnPressReloadButton.Add(ISReloadWeaponAction.OnPressReloadButton);
Events.OnWeaponSwingHitPoint.Add(ISReloadWeaponAction.onShoot);
Hook.Attack.Add(ISReloadWeaponAction.attackHook);
--------------------------------------------------------


Events.OnZombieDead.Add(KCMOnZombieDead);
Events.OnLoad.Add(KCMOnLoad);
Events.OnWeaponHitCharacter.Add(hitCrossbow);
Events.OnEquipPrimary.Add(KCMOnEquiPrimary);