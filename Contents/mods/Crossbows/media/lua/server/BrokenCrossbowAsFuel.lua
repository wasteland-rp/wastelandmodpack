---
--- BrokenCrossbowAsFuel.lua
--- 20/04/2024
---

require 'Camping/camping_fuel'

campingFuelType['ShortBrokenBolt'] = 0.1
campingFuelType['LongBrokenBolt'] = 0.25
campingFuelType['WoodenBrokenBolt'] = 0.2
