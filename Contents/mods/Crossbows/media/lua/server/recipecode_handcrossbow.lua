
function Recipe.GetItemTypes.HandCrossbow(scriptItems)
    scriptItems:add(getScriptManager():getItem("KCMweapons.HandCrossbow"))
end

function Recipe.GetItemTypes.WoodCrossbow(scriptItems)
    scriptItems:add(getScriptManager():getItem("KCMweapons.KCM_Handmade"))
end

function Recipe.GetItemTypes.PrecisionCrossbow(scriptItems)
    scriptItems:add(getScriptManager():getItem("KCMweapons.KCM_Handmade02"))
end

---@param result InventoryItem
---@param player IsoPlayer
function Recipe.OnCreate.dismantleHandCrossbow(items, result, player)

    if ZombRand(0, 3) > 0 then  -- 2/3 chance we get it back as ZombRand returns 0, 1, or 2
        player:getInventory():AddItem("Base.WoodenStick");
    end

    if ZombRand(0, 3) > 0 then -- 2/3 chance we get it back as ZombRand returns 0, 1, or 2
        player:getInventory():AddItem("Base.RubberBand");
    end

    if ZombRand(0, 3) > 0 then -- 2/3 chance we get it back as ZombRand returns 0, 1, or 2
        player:getInventory():AddItem("Base.Screws"); -- This is actually FIVE screws
    end
end

---@param result InventoryItem
---@param player IsoPlayer
function Recipe.OnCreate.dismantleWoodCrossbow(items, result, player)

    if ZombRand(0, 3) > 1 then  -- 1/3 chance we get it back as ZombRand returns 0, 1, or 2
        player:getInventory():AddItem("Base.Hinge");
    end

    if ZombRand(0, 3) > 0 then -- 2/3 chance we get it back as ZombRand returns 0, 1, or 2
        player:getInventory():AddItem("Base.WoodenStick");
    end

    if ZombRand(0, 3) > 0 then -- 2/3 chance we get it back as ZombRand returns 0, 1, or 2
        player:getInventory():AddItem("Base.Nails"); -- This is actually FIVE nails
    end
end

---@param result InventoryItem
---@param player IsoPlayer
function Recipe.OnCreate.dismantlePrecisionCrossbow(items, result, player)

    if ZombRand(0, 3) > 1 then  -- 1/3 chance we get it back as ZombRand returns 0, 1, or 2
        player:getInventory():AddItem("Base.Hinge");
    end

    if ZombRand(0, 3) > 0 then -- 2/3 chance we get it back as ZombRand returns 0, 1, or 2
        player:getInventory():AddItem("Base.WoodenStick");
    end

    if ZombRand(0, 3) > 0 then -- 2/3 chance we get it back as ZombRand returns 0, 1, or 2
        player:getInventory():AddItem("Base.Screws"); -- This is actually FIVE screws
    end

    local strips = ZombRand(0, 3)  -- Give back 0 to 2 leather strips
    for i = 1, strips do
        player:getInventory():AddItem("Base.LeatherStrips");
    end
end