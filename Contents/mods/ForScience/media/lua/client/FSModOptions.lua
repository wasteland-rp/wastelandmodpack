FSSETTINGS = {
	options = { 
		items_on_dissect = true,
	},
	names = {
		items_on_dissect = "IGUI_FSDissectItems",
	},
	mod_id = "ForScience",
	mod_shortname = "For Science!",
}

-- Update settings at game boot
if ModOptions and ModOptions.getInstance then	
	ModOptions:getInstance(FSSETTINGS)
end

-- Check options at game loading
-- Events.OnGameStart.Add(function()
  -- print("ForScience: Items on Dissect = ", FSSETTINGS.options.items_on_dissect)
-- end)

-- -- Check options in-game
-- if ModOptions and ModOptions.getInstance then
  -- local settings = ModOptions:getInstance(OPTIONS, "MyModID", "My Mod")

  -- local opt1 = settings:getData("box1")
	
  -- function opt1:OnApplyInGame(val)
    -- print('Option is updated!', self.id, val)
  -- end

-- end