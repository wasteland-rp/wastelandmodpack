require "TimedActions/ISBaseTimedAction"

local function iterList(_list)
	local list = _list;
	local size = list:size() - 1;
	local i = -1;
	return function()
		i = i + 1;
		if i <= size and not list:isEmpty() then
			return list:get(i), i;
		end;
	end;
end

ISFSDissectCorpseAction = ISBaseTimedAction:derive("ISFSDissectCorpseAction");

function ISFSDissectCorpseAction:isValid()
	if self.corpse:getStaticMovingObjectIndex() < 0 then
		return false
	end
	if not self.scalpel then
		return false;
	end
	return true;
end

function ISFSDissectCorpseAction:update()
	self.scalpel:setJobDelta(self:getJobDelta());
	
	self.character:faceThisObject(self.corpse);
end

function ISFSDissectCorpseAction:start()
	self.scalpel:setJobType(getText("IGUI_JobType_FSDissect"));
	self.scalpel:setJobDelta(0.0);
	
	if self.medJournal ~= nil then
		self.medJournal:setJobType(getText("IGUI_JobType_FSDissect"));
		self.medJournal:setJobDelta(0.0);    
		--self:setOverrideHandModels(nil, self.medJournal:getStaticModel());
	end
	
	self:setOverrideHandModels(self.scalpel:getStaticModel(), nil);
	self:setActionAnim("SawLog");
end

function ISFSDissectCorpseAction:stop()
	ISBaseTimedAction.stop(self);
	if self.scalpel then
		self.scalpel:setJobDelta(0.0);
	end
end

function ISFSDissectCorpseAction:perform()
	self.scalpel:setJobDelta(0.0);
	
	local luckRoll = ZombRand(7);
	local docSkill = self.doctorLevel - 2 + (self.medJournalBonus * 2);
	if (luckRoll < docSkill) then
		if FSSETTINGS.options.items_on_dissect then
			local corpseItems = self.corpse:getItemContainer():getItems()
			if corpseItems then
				local bodybag = InventoryItemFactory.CreateItem("FSItemBox");
				local bagItems = bodybag:getItemContainer()
				for i=1,corpseItems:size() do
					bagItems:addItem(corpseItems:get(0))
				end
				self.square:AddWorldInventoryItem(bodybag, 0, 0, 0);
			end
		end
		self.corpse:getSquare():removeCorpse(self.corpse, false);
		--self.corpse:removeFromWorld();
		--self.corpse:removeFromSquare();
		addBloodSplat(self.square, 10);
		--self.character:getEmitter():playSound("Unstuckweapon");
		local xpGain = (5 + luckRoll) * (self.medJournalBonus + 1);
		self.character:getXp():AddXP(Perks.Doctor, xpGain);
		self.character:Say(getText("IGUI_PlayerText_FSDissectEureka" .. (ZombRand(3)+1)), 0.55, 0.55, 0.55, UIFont.Dialogue, 0, "default");
	else
		if luckRoll == 6 then 
			self.scalpel:setCondition(self.scalpel:getCondition() - 1)
		end
		ISWorldObjectContextMenu.checkWeapon(self.character);
		if FSSETTINGS.options.items_on_dissect then
			local corpseItems = self.corpse:getItemContainer():getItems()
			if corpseItems then
				local bodybag = InventoryItemFactory.CreateItem("FSItemBox");
				local bagItems = bodybag:getItemContainer()
				for i=1,corpseItems:size() do
					bagItems:addItem(corpseItems:get(0))
				end
				self.square:AddWorldInventoryItem(bodybag, 0, 0, 0);
			end
		end
		self.corpse:getSquare():removeCorpse(self.corpse, false);
		--self.corpse:removeFromWorld();
		--self.corpse:removeFromSquare();
		addBloodSplat(self.square, 50);
		self.character:getEmitter():playSound("HeadSmash")
		local xpGain = 2 * (self.medJournalBonus + 1);
		self.character:getXp():AddXP(Perks.Doctor, xpGain);
		self.character:Say(getText("IGUI_PlayerText_FSDissectFail" .. (ZombRand(3)+1)), 0.55, 0.55, 0.55, UIFont.Dialogue, 0, "default");
	end

	-- needed to remove from queue / start next.
	ISBaseTimedAction.perform(self);
end

function ISFSDissectCorpseAction:new(character, corpse, scalpel)
	local o = {}
	setmetatable(o, self)
	self.__index = self
	o.character = character;
	o.corpse = corpse;
	o.scalpel = scalpel;
	o.square = corpse:getSquare();
	o.doctorLevel = character:getPerkLevel(Perks.Doctor);
	o.medJournal = nil
	o.medJournalBonus = 0;
	if character:getInventory():containsTypeRecurse("Base.BookMedicalJournal") then
		o.medJournal = character:getInventory():getItemFromTypeRecurse("Base.BookMedicalJournal");
		o.medJournalBonus = 1;
	end
	o.stopOnWalk = true;
	o.stopOnRun = true;
	o.maxTime = 300 - (o.doctorLevel * 20);
	if character:isTimedActionInstant() then
		o.maxTime = 1;
	end
	return o
end