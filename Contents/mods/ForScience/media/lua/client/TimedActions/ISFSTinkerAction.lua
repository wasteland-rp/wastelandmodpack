require "TimedActions/ISBaseTimedAction"

ISFSTinkerAction = ISBaseTimedAction:derive("ISFSTinkerAction");

function ISFSTinkerAction:isValid()
	return self.character:getInventory():contains(self.screwdriver) and
		self.character:getInventory():containsTypeRecurse("Base.ElectronicsScrap");
end

function ISFSTinkerAction:waitToStart()
	if self.deviceWorld then
		self.character:faceThisObject(self.deviceWorld)
		return self.character:shouldBeTurning()
	else
		return false;
	end
end

function ISFSTinkerAction:update()
	self.screwdriver:setJobDelta(self:getJobDelta());	
	if self.circDummy then
		self.circDummy:setJobDelta(self:getJobDelta());    
	end
	if self.deviceInv then
		self.deviceInv:setJobDelta(self:getJobDelta());    
	end
	
	if self.deviceWorld then
		self.character:faceThisObject(self.deviceWorld);
	end
end

function ISFSTinkerAction:start()	
	self.screwdriver:setJobType(getText("IGUI_JobType_FSTinker"));
	self.screwdriver:setJobDelta(0.0);
	
	if self.circDummy then
		self.circDummy:setJobType(getText("IGUI_JobType_FSTinker"));
		self.circDummy:setJobDelta(0.0);    
		--self:setOverrideHandModels(nil, self.circDummy:getStaticModel());
	end
	if self.deviceInv then
		self.deviceInv:setJobType(getText("IGUI_JobType_FSTinker"));
		self.deviceInv:setJobDelta(0.0);    
	end
	
	if self.character:isSitOnGround() then
		self.character:setVariable("sitonground", false);
		self.character:setVariable("forceGetUp", true);
	end
	
	self:setActionAnim("Loot")
	self:setOverrideHandModels(self.screwdriver, nil)
	self.character:SetVariable("LootPosition", "Low")
	self.sound = self.character:playSound("GeneratorRepair")
end

function ISFSTinkerAction:stop()
	if self.sound then
		self.character:getEmitter():stopSound(self.sound)
		self.sound = nil
	end
	
	self.screwdriver:setJobDelta(0.0);
	if self.circDummy then
		self.circDummy:setJobDelta(0.0);    
	end
	if self.deviceInv then
		self.deviceInv:setJobDelta(0.0);    
	end

	ISBaseTimedAction.stop(self)
end

function ISFSTinkerAction:perform()
	self.screwdriver:setJobDelta(0.0);
	if self.circDummy then
		self.circDummy:setJobDelta(0.0);    
	end

	-- needed to remove from queue / start next.
	ISBaseTimedAction.perform(self)
end

function ISFSTinkerAction:animEvent(event, parameter)
	if event == 'SetVariable' then	
		if ((self.simple) and (self.elecLevel < 4)) or ((self.simple == false) and (self.elecLevel >= 4)) then
			local scrapItem = self.character:getInventory():getFirstTypeRecurse("Base.ElectronicsScrap");
			if not scrapItem then 
				self.character:Say(getText("IGUI_PlayerText_FSTinkerNeedMore"), 0.55, 0.55, 0.55, UIFont.Dialogue, 0, "default");
				if self.sound then
					self.character:getEmitter():stopSound(self.sound)
					self.sound = nil
				end
				self:forceComplete();
			else
				local luckRoll = ZombRand((self.elecLevel+4)*self.circDummyBonus);
				if (luckRoll == 0) then
					--self.character:getInventory():Remove(scrapItem);
					if scrapItem:getContainer() then
						scrapItem:getContainer():Remove(scrapItem)
					end
				end
				self.progress = self.progress + 1;
				if (self.progress >= self.progressGoal) then
					self.progress = 0;
					self.character:getXp():AddXP(Perks.Electricity, (self.reward * self.circDummyBonus));
					self.character:Say(getText("IGUI_PlayerText_FSTinkerEureka" .. (ZombRand(3)+1)), 0.55, 0.55, 0.55, UIFont.Dialogue, 0, "default");
					self.elecLevel = self.character:getPerkLevel(Perks.Electricity);
					if (self.simple) and (self.elecLevel >= 4) then
						self.character:Say(getText("IGUI_PlayerText_FSTinkerMastered"), 0.55, 0.55, 0.55, UIFont.Dialogue, 0, "default");
						if self.sound then
							self.character:getEmitter():stopSound(self.sound)
							self.sound = nil
						end
						self:forceComplete();
					end
				end
			end
		end
	end
end

function ISFSTinkerAction:new (character, deviceWorld, deviceInv, screwdriver, simple)
	local o = {}
	setmetatable(o, self)
	self.__index = self
	o.character = character;
	o.deviceWorld = deviceWorld;
	o.deviceInv = deviceInv;
	o.simple = simple;
	o.screwdriver = screwdriver;
	o.progress = 0;
	o.progressGoal = 0;
	o.reward = 0;
	if simple then
		o.progressGoal = 5;
		o.reward = 5;
	else
		o.progressGoal = ZombRand(16) + 10;
		o.reward = 10;
	end
	o.elecLevel = character:getPerkLevel(Perks.Electricity);
	o.circDummy = nil
	o.circDummyBonus = 1;
	if character:getInventory():containsTypeRecurse("Base.BookElectricManual") then
		o.circDummy = character:getInventory():getItemFromTypeRecurse("Base.BookElectricManual");
		o.circDummyBonus = 2;
	end
	o.stopOnWalk = true;
	o.stopOnRun = true;
	o.maxTime = -1;
	o.forceProgressBar = true;
	return o
end