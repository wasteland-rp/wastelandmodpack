module Base
{
	item Light_Switch_01_0 {
        DisplayCategory = Furniture,
		Type = Moveable,
		Icon = PkokoLightSwitch,
		Weight = 5.0,
		DisplayName = Light Switch 1 (Crafted),
		WorldObjectSprite = Light_Switch_01_0,
		}

    item Light_Switch_01_4 {
        DisplayCategory = Furniture,
		Type = Moveable,
		Icon = PkokoLightSwitch,
		Weight = 5.0,
		DisplayName = Light Switch 2 (Crafted),
		WorldObjectSprite = Light_Switch_01_4,
	}

    recipe Make Light Switch 1 New {
		keep Screwdriver,
		keep Scissors,
		Radio.ElectricWire=5,
		ElectronicsScrap=3,
		LightBulb=1,
		Screws=4,
		OnGiveXP:Recipe.OnGiveXP.DismantleElectronics,
		AnimNode:Disassemble,
		Result:Light_Switch_01_0,
		SkillRequired:Electricity=5,
		Time:120.0,
		Category:Electrical,
	}

	recipe Make Light Switch 2 New {
		keep Screwdriver,
        keep Scissors,
        Radio.ElectricWire=5,
        ElectronicsScrap=3,
        LightBulb=1,
        Screws=4,
        OnGiveXP:Recipe.OnGiveXP.DismantleElectronics,
        AnimNode:Disassemble,
		Result:Light_Switch_01_4,
		SkillRequired:Electricity=5,
		Time:120.0,
		Category:Electrical,
	}

}