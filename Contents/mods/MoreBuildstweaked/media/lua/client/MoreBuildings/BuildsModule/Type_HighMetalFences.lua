if not getMoreBuildInstance then
  require('MoreBuildings/MoreBuilds_Main')
end

local MoreBuild = getMoreBuildInstance()


WastelandMetalFences = {}

WastelandMetalFence = {}

function WastelandMetalFence:new(name, tooltip, xpPerkType, xpAmount, baseHitPoints, sprites, neededSkills, neededMaterials)
	local obj = {}
	setmetatable(obj, self)
	self.__index = self
	obj.name = name
	obj.tooltip = tooltip
	obj.xpPerkType = xpPerkType
	obj.xpAmount = xpAmount
	obj.baseHitPoints = baseHitPoints
	obj.sprite = {}
	obj.sprite.sprite1 = sprites[1]
	obj.sprite.sprite2 = sprites[2]
	obj.sprite.northSprite1 = sprites[3]
	obj.sprite.northSprite2 = sprites[4]
	obj.neededSkills = neededSkills
	obj.neededMaterials = neededMaterials
	obj.neededTools = {'Hammer', 'Screwdriver'}
	obj.blockAllTheSquare = false
	obj.completionSound = 'BuildMetalStructureMedium'
	return obj
end

function WastelandMetalFence:getHealth(player)
	local bonusHealth = (player:getPerkLevel(Perks.MetalWelding) * 50);
	if player:HasTrait("Handy") then
		bonusHealth = bonusHealth + 100;
	end
	return self.baseHitPoints + bonusHealth
end

function WastelandMetalFence:createBuildingObject(playerId)
	local hitPoints = self:getHealth(getSpecificPlayer(playerId))
	return ISHighMetalFence:new(hitPoints, self.sprite.sprite1, self.sprite.sprite2, self.sprite.northSprite1, self.sprite.northSprite2)
end

table.insert(WastelandMetalFences, WastelandMetalFence:new("Reinforced Metal Fence",
		"An expensive concrete reinforced fence, with barbed wire to stop anyone climbing over",
		"MetalWelding", 25, 1700,
		{'fencing_01_50', 'fencing_01_51', 'fencing_01_49', 'fencing_01_48'},
		{ MetalWelding = 10 },
		{
			{ Material = 'Base.Wire', Amount = 1, UseDelta = true },
			{ Material = 'Base.BarbedWire', Amount = 1 },
			{ Material = 'Base.BucketConcreteFull', Amount = 1, UseDelta = true },
			{ Material = 'Base.MetalBar', Amount = 2 },
			{ Material = 'Base.Screws', Amount = 10 }
		}
))

table.insert(WastelandMetalFences, WastelandMetalFence:new("Chain Link Fence",
		"A cheap and easy to build chain link fence, can be climbed over",
		"MetalWelding", 15,700,
		{ 'fencing_01_58', 'fencing_01_59', 'fencing_01_57', 'fencing_01_56' },
		{ MetalWelding = 4 },
		{
			{ Material = 'Base.Wire', Amount = 1, UseDelta = true },
			{ Material = 'Base.MetalBar', Amount = 2 },
			{ Material = 'Base.Screws', Amount = 4 }
		}
))

table.insert(WastelandMetalFences, WastelandMetalFence:new("Fancy Metal Fence",
		"A sturdy fence for an estate or official building, but can be climbed over, so not entirely secure",
		"MetalWelding", 20,1000,
		{'fencing_01_66', 'fencing_01_67', 'fencing_01_65', 'fencing_01_64'},
		{ MetalWelding = 6 },
		{
			{ Material = 'Base.MetalBar', Amount = 4 },
			{ Material = 'Base.Screws', Amount = 8 }
		}
))

table.insert(WastelandMetalFences, WastelandMetalFence:new("Weighted Metal Fence",
		"A fence weighted by bricks to keep it from being falling over easily, though it can be climbed over",
		"MetalWelding", 15,900,
		{'fencing_01_82', 'fencing_01_83', 'fencing_01_81', 'fencing_01_80'},
		{ MetalWelding = 6 },
		{
			{ Material = 'Base.Wire', Amount = 1, UseDelta = true },
			{ Material = 'Base.MetalBar', Amount = 2 },
			{ Material = 'Base.Screws', Amount = 6 },
			{ Material = 'Base.Stone', Amount = 2 }
		}
))

table.insert(WastelandMetalFences, WastelandMetalFence:new("Weighted Metal Fence with Barbed Wire",
		"A fence weighted by bricks to keep it from being falling over easily and with barbed wire to stop anyone climbing it",
		"MetalWelding", 20,900,
		{'fencing_01_90', 'fencing_01_91', 'fencing_01_89', 'fencing_01_88'},
		{ MetalWelding = 8 },
		{
			{ Material = 'Base.Wire', Amount = 1, UseDelta = true },
			{ Material = 'Base.BarbedWire', Amount = 1 },
			{ Material = 'Base.MetalBar', Amount = 2 },
			{ Material = 'Base.Screws', Amount = 6 },
			{ Material = 'Base.Stone', Amount = 2 }
		}
))

MoreBuild.highMetalFenceMenuBuilder = function(subMenu, playerId)
	local _option
	local _tooltip
	for _, fence in ipairs(WastelandMetalFences) do
		MoreBuild.neededTools = fence.neededTools
		_option = subMenu:addOption(fence.name, nil, MoreBuild.onBuildHighMetalFence, fence, playerId)
		MoreBuild.neededMaterials = fence.neededMaterials
		_tooltip = MoreBuild.canBuildObject(fence.neededSkills, _option, playerId)
		_tooltip:setName(fence.name)
		_tooltip.description = fence.tooltip .. _tooltip.description
		_tooltip.description = _tooltip.description .. "<LINE><RGB:0,1,0>Health: " .. tostring(fence:getHealth(getSpecificPlayer(playerId)))
		_tooltip:setTexture(fence.sprite.sprite1)
	end
end

MoreBuild.onBuildHighMetalFence = function(_, buildingData, playerId)
	local buildingObject = buildingData:createBuildingObject(playerId)
	buildingObject.player = playerId
	buildingObject.name = buildingData.name
	buildingObject.blockAllTheSquare = buildingData.blockAllTheSquare
	buildingObject.completionSound = buildingData.completionSound

	for _, component in ipairs(buildingData.neededMaterials) do
		local type = component.UseDelta == nil and "need:" or "use:"
		local typeAndMaterial = type .. component.Material
		buildingObject.modData[typeAndMaterial] = component.Amount
	end

	local xpType = "xp:" .. buildingData.xpPerkType
	buildingObject.modData[xpType] = buildingData.xpAmount

	MoreBuild.equipToolPrimary(buildingObject, playerId, buildingData.neededTools[1])
	getCell():setDrag(buildingObject, playerId)
end

