-- IDCardSign.lua

-- Helper for date formatting (dd-mmm-yy)
local monthNames = {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"}
local function formatDate(day, month, year)
    return string.format("%02d-%s-%02d", day, monthNames[month], (year % 100))
end

-- Base modal for single-line text input
ISCustomInputModal = ISPanel:derive("ISCustomInputModal")

function ISCustomInputModal:new(x, y, width, height, title, player, onConfirm, maxLength)
    local fontHeight = FONT_HGT_SMALL or 12
    local scale = fontHeight / 12

    local baseWidth = width or 250
    local baseHeight = height or 130
    local w = baseWidth * scale
    local h = baseHeight * scale

    local screenWidth = getCore():getScreenWidth()
    local screenHeight = getCore():getScreenHeight()
    local centerX = (screenWidth - w) / 2
    local centerY = (screenHeight - h) / 2

    local o = ISPanel.new(self, math.floor(centerX), math.floor(centerY), w, h)
    o.title = title
    o.player = player
    o.onConfirm = onConfirm
    o.input = ""
    o.maxLength = maxLength or 15 -- default to 15 if not provided
    o:setAlwaysOnTop(true)
    o:setVisible(true)
    return o
end

function ISCustomInputModal:initialise()
    ISPanel.initialise(self)
    self:createChildren()
end

function ISCustomInputModal:createChildren()
    local label = ISLabel:new(0, 10, 20, self.title, 1, 1, 1, 1, UIFont.Small)
    label:initialise()
    label:setX((self.width - label:getWidth()) / 2)
    self:addChild(label)

    self.textEntry = ISTextEntryBox:new("", 20, 40, self.width - 40, 25)
    self.textEntry:initialise()
    self.textEntry:instantiate()
    self.textEntry:setText("")
    self:addChild(self.textEntry)

    local okButtonX = (self.width / 2) - 70
    self.okButton = ISButton:new(okButtonX, 75, 60, 25, "OK", self, ISCustomInputModal.onClickOK)
    self.okButton.internal = "OK"
    self.okButton:initialise()
    self.okButton:instantiate()
    self:addChild(self.okButton)

    local cancelButtonX = (self.width / 2) + 10
    self.cancelButton = ISButton:new(cancelButtonX, 75, 60, 25, "Cancel", self, ISCustomInputModal.onClickCancel)
    self.cancelButton.internal = "CANCEL"
    self.cancelButton:initialise()
    self.cancelButton:instantiate()
    self:addChild(self.cancelButton)
end

function ISCustomInputModal:onClickOK()
    local inputText = self.textEntry:getText()
    if string.len(inputText) > self.maxLength then
        self.player:Say("The text entered is too long.")
        return
    end
    if self.onConfirm then
        self.onConfirm(inputText)
    end
    self:removeFromUIManager()
end

function ISCustomInputModal:onClickCancel()
    self:removeFromUIManager()
end

-- A new modal for multi-line description (3 lines, each max 10 chars)
local ISMultiLineDescriptionModal = ISPanel:derive("ISMultiLineDescriptionModal")

function ISMultiLineDescriptionModal:new(title, player, onConfirm)
    local fontHeight = FONT_HGT_SMALL or 12
    local scale = fontHeight / 12

    local baseWidth = 300
    local baseHeight = 200
    local w = baseWidth * scale
    local h = baseHeight * scale

    local screenWidth = getCore():getScreenWidth()
    local screenHeight = getCore():getScreenHeight()
    local centerX = (screenWidth - w) / 2
    local centerY = (screenHeight - h) / 2

    local o = ISPanel.new(self, math.floor(centerX), math.floor(centerY), w, h)
    o.title = title
    o.player = player
    o.onConfirm = onConfirm
    o.maxLength = 10
    o:setAlwaysOnTop(true)
    o:setVisible(true)
    return o
end

function ISMultiLineDescriptionModal:initialise()
    ISPanel.initialise(self)
    self:createChildren()
end

function ISMultiLineDescriptionModal:createChildren()
    local label = ISLabel:new(0, 10, 20, self.title, 1,1,1,1, UIFont.Small)
    label:initialise()
    label:setX((self.width - label:getWidth()) / 2)
    self:addChild(label)

    local infoText = "Insert description (3 lines, each max 10 chars):"
    local infoLabel = ISLabel:new(0, 30, 20, infoText, 1,1,1,1, UIFont.Small)
    infoLabel:initialise()
    infoLabel:setX((self.width - infoLabel:getWidth()) / 2)
    self:addChild(infoLabel)

    -- Three text entries for three lines
    self.textEntry1 = ISTextEntryBox:new("", 20, 60, self.width - 40, 25)
    self.textEntry1:initialise()
    self.textEntry1:instantiate()
    self.textEntry1:setText("")
    self:addChild(self.textEntry1)

    self.textEntry2 = ISTextEntryBox:new("", 20, 95, self.width - 40, 25)
    self.textEntry2:initialise()
    self.textEntry2:instantiate()
    self.textEntry2:setText("")
    self:addChild(self.textEntry2)

    self.textEntry3 = ISTextEntryBox:new("", 20, 130, self.width - 40, 25)
    self.textEntry3:initialise()
    self.textEntry3:instantiate()
    self.textEntry3:setText("")
    self:addChild(self.textEntry3)

    local okButtonX = (self.width / 2) - 70
    self.okButton = ISButton:new(okButtonX, 165, 60, 25, "OK", self, ISMultiLineDescriptionModal.onClickOK)
    self.okButton.internal = "OK"
    self.okButton:initialise()
    self.okButton:instantiate()
    self:addChild(self.okButton)

    local cancelButtonX = (self.width / 2) + 10
    self.cancelButton = ISButton:new(cancelButtonX, 165, 60, 25, "Cancel", self, ISMultiLineDescriptionModal.onClickCancel)
    self.cancelButton.internal = "CANCEL"
    self.cancelButton:initialise()
    self.cancelButton:instantiate()
    self:addChild(self.cancelButton)
end

function ISMultiLineDescriptionModal:onClickOK()
    local line1 = self.textEntry1:getText() or ""
    local line2 = self.textEntry2:getText() or ""
    local line3 = self.textEntry3:getText() or ""

    if string.len(line1) > self.maxLength or string.len(line2) > self.maxLength or string.len(line3) > self.maxLength then
        self.player:Say("One of the lines is too long (max 10 chars per line).")
        return
    end

    local description = line1 .. "\n" .. line2 .. "\n" .. line3

    if self.onConfirm then
        self.onConfirm(description)
    end
    self:removeFromUIManager()
end

function ISMultiLineDescriptionModal:onClickCancel()
    self:removeFromUIManager()
end

-- Category modal with bold title and more spacing
local function showCategoryModal(player, name, description, onConfirm)
    local fontHeight = FONT_HGT_SMALL or 12
    local scale = fontHeight / 12

    local baseWidth = 300
    local baseHeight = 200
    local w = baseWidth * scale
    local h = baseHeight * scale

    local screenWidth = getCore():getScreenWidth()
    local screenHeight = getCore():getScreenHeight()
    local centerX = (screenWidth - w) / 2
    local centerY = (screenHeight - h) / 2

    local modal = ISPanel:new(math.floor(centerX), math.floor(centerY), w, h)
    modal.title = "Enter Category:"
    modal.player = player
    modal.name = name
    modal.description = description
    modal.onConfirm = onConfirm
    modal.setAlwaysOnTop = ISPanel.setAlwaysOnTop
    modal:setAlwaysOnTop(true)
    modal:setVisible(true)

    function modal:initialise()
        ISPanel.initialise(self)
        self:createChildren()
    end

    function modal:createChildren()
        local titleLabel = ISLabel:new(0, 10, 20, self.title, 1,1,1,1, UIFont.Medium)
        titleLabel:initialise()
        titleLabel:setX((self.width - titleLabel:getWidth()) / 2)
        self:addChild(titleLabel)

        local descText = "Type 'visitor' for a 1-week expiry, 'citizen' for a 6-month expiry,\nor enter any other category name (max 10 chars) for a 6-month expiry."
        local descLabel = ISRichTextPanel:new(20, 40, self.width - 40, 40)
        descLabel:initialise()
        descLabel:noBackground()
        descLabel:setText("<SIZE:12>"..descText)
        descLabel:paginate()
        self:addChild(descLabel)

        self.textEntry = ISTextEntryBox:new("", 20, 130, self.width - 40, 25)
        self.textEntry:initialise()
        self.textEntry:instantiate()
        self.textEntry:setText("")
        self:addChild(self.textEntry)

        local okButtonX = (self.width / 2) - 70
        self.okButton = ISButton:new(okButtonX, 170, 60, 25, "OK", self, modal.onClickOK)
        self.okButton.internal = "OK"
        self.okButton:initialise()
        self.okButton:instantiate()
        self:addChild(self.okButton)

        local cancelButtonX = (self.width / 2) + 10
        self.cancelButton = ISButton:new(cancelButtonX, 170, 60, 25, "Cancel", self, modal.onClickCancel)
        self.cancelButton.internal = "CANCEL"
        self.cancelButton:initialise()
        self.cancelButton:instantiate()
        self:addChild(self.cancelButton)
    end

    function modal:onClickOK()
        local categoryInput = self.textEntry:getText()
        if string.len(categoryInput) > 10 then
            self.player:Say("Category name is too long (max 10 chars).")
            return
        end

        local catLower = categoryInput:lower()
        local category, expiryMonths, expiryDays
        if catLower == "visitor" then
            category = "Visitor"
            expiryDays = 7
            expiryMonths = nil
        elseif catLower == "citizen" then
            category = "Citizen"
            expiryMonths = 6
            expiryDays = nil
        else
            category = categoryInput
            expiryMonths = 6
            expiryDays = nil
        end

        if self.onConfirm then
            self.onConfirm(self.name, self.description, category, expiryMonths, expiryDays)
        end
        self:removeFromUIManager()
    end

    function modal:onClickCancel()
        self:removeFromUIManager()
    end

    modal:initialise()
    modal:addToUIManager()
end

local function finalizeCard(item, player, name, description, category, expiryMonths, expiryDays)
    local inventory = player:getInventory()
    local signedCard = InventoryItemFactory.CreateItem("PresIDCards.SignedIDCard")
    if not signedCard then
        player:Say("Error: Could not create Signed ID Card.")
        return
    end

    local username = player:getUsername()
    local formattedUsername = username:gsub("([a-z])([A-Z])", "%1 %2")

    local modData = signedCard:getModData()
    modData.signature = formattedUsername
    modData.pages = modData.pages or {}
    modData.pages[1] = name ~= "" and name or "(No name provided)"
    modData.pages[2] = description ~= "" and description or "(No description provided)"
    modData.category = category

    local gameTime = getGameTime()
    local year = gameTime:getYear()
    local month = gameTime:getMonth()
    local day = gameTime:getDay()

    modData.issuedYear = year
    modData.issuedMonth = month
    modData.issuedDay = day

    if expiryMonths then
        local newMonth = month + expiryMonths
        local newYear = year
        while newMonth > 11 do
            newMonth = newMonth - 12
            newYear = newYear + 1
        end
        modData.expiryYear = newYear
        modData.expiryMonth = newMonth
        modData.expiryDay = day
    elseif expiryDays then
        local totalDays = day + expiryDays
        local newMonth = month
        local newYear = year
        while totalDays > 29 do
            totalDays = totalDays - 30
            newMonth = newMonth + 1
            if newMonth > 11 then
                newMonth = newMonth - 12
                newYear = newYear + 1
            end
        end
        modData.expiryYear = newYear
        modData.expiryMonth = newMonth
        modData.expiryDay = totalDays
    else
        modData.expiryYear = nil
        modData.expiryMonth = nil
        modData.expiryDay = nil
    end

    signedCard:setCustomName(true)
    signedCard:setName("Signed ID Card by " .. formattedUsername)

    inventory:Remove(item)
    inventory:AddItem(signedCard)
    player:Say("You signed the ID Card.")
end

local function showDescriptionModal(player, name, onConfirm)
    -- Now uses the multiline description modal
    local modal = ISMultiLineDescriptionModal:new("Insert description (3 lines, 10 chars each)", player, function(description)
        onConfirm(name, description)
    end)
    modal:initialise()
    modal:addToUIManager()
end

local function showNameModal(player, onConfirm)
    local modal = ISCustomInputModal:new(0, 0, 250, 130, "Insert name (max 15 chars)", player, function(name)
        if not name or name == "" then
            name = ""
        end
        onConfirm(name)
    end, 15)
    modal:initialise()
    modal:addToUIManager()
end

local function signIDCard(item, playerIndex)
    local player = getSpecificPlayer(playerIndex)

    showNameModal(player, function(name)
        showDescriptionModal(player, name, function(name, description)
            showCategoryModal(player, name, description, function(name, description, category, expiryMonths, expiryDays)
                finalizeCard(item, player, name, description, category, expiryMonths, expiryDays)
            end)
        end)
    end)
end

local function viewSignedIDCard(item, player)
    local modData = item:getModData() or {}
    local signature = modData.signature or "Unknown"
    local pages = modData.pages or {}
    local name = pages[1] or "(No name provided)"
    local description = pages[2] or "(No description provided)"
    local category = modData.category or "N/A"

    local text = "<SIZE:25><RGB:0,0,0>"
    text = text .. "Name: " .. name .. "\n\n"
    text = text .. "Description: " .. description .. "\n\n"
    text = text .. "Category: " .. category .. "\n\n"

    local issuedYear = modData.issuedYear
    local expiryYear = modData.expiryYear
    if issuedYear then
        local issuedDay = (modData.issuedDay or 0) + 1
        local issuedMonth = (modData.issuedMonth or 0) + 1
        text = text .. "Issued: " .. formatDate(issuedDay, issuedMonth, issuedYear) .. "\n"
    end

    if expiryYear then
        local expiryDay = (modData.expiryDay or 0) + 1
        local expiryMonth = (modData.expiryMonth or 0) + 1
        text = text .. "Expires: " .. formatDate(expiryDay, expiryMonth, expiryYear) .. "\n"
    end

    text = text .. "\nSigned by: " .. signature .. "\n"

    local modalWidth, modalHeight = 340, 500
    local modal = ISPanel:new(0, 0, modalWidth, modalHeight)
    modal:initialise()
    modal:addToUIManager()
    modal:setAlwaysOnTop(true)

    modal.backgroundColor = {r=0, g=0, b=0, a=0}
    modal.borderColor = {r=0, g=0, b=0, a=0}

    local imageXOffset = -80
    local image = ISImage:new(imageXOffset, 0, modal.width, modal.height, getTexture("media/textures/IDCardClipboard.png"))
    image:initialise()
    modal:addChild(image)

    local textXOffset = 50
    local textYOffset = 140
    local richTextWidth = modal.width - textXOffset * 2
    local richTextHeight = modal.height - textYOffset - 60

    local richText = ISRichTextPanel:new(textXOffset, textYOffset, richTextWidth, richTextHeight)
    richText:initialise()
    richText:setText(text)
    richText:paginate()
    richText.background = false
    modal:addChild(richText)

    local closeButton = ISButton:new(modalWidth / 2 - 30, modalHeight - 40, 60, 25, "Close", modal, function()
        modal:removeFromUIManager()
    end)
    closeButton:initialise()
    closeButton:instantiate()
    modal:addChild(closeButton)

    modal:setX((getCore():getScreenWidth() - modalWidth) / 2)
    modal:setY((getCore():getScreenHeight() - modalHeight) / 2)
end

local function addIDCardOptions(playerIndex, context, items)
    local player = getSpecificPlayer(playerIndex)
    local writeOptionText = getText("ContextMenu_Write")

    for _, v in ipairs(items) do
        local item = v
        if not instanceof(item, "InventoryItem") and v.items then
            item = v.items[1]
        end

        if item then
            local itemType = item:getFullType()

            if itemType == "PresIDCards.IDCard" then
                context:addOption("Sign ID Card", item, function()
                    signIDCard(item, playerIndex)
                end)
            elseif itemType == "PresIDCards.SignedIDCard" then
                context:removeOptionByName(writeOptionText)
                context:addOption("View ID Card", item, function()
                    viewSignedIDCard(item, player)
                end)
            end
        end
    end
end

Events.OnFillInventoryObjectContextMenu.Add(addIDCardOptions)
