module PresIDCards
{

    recipe Create ID Card
    {
        Base.SheetPaper2,
        Result: IDCard,
        Time:5.0,
        Category:Survival,
    }

}
