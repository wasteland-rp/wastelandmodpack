PzWebStats = PzWebStats or {}


function PzWebStats.GetPlayerSkills(player)
    local skills = {}

    for i=1, Perks.getMaxIndex()-1 do
		local perk = Perks.fromIndex(i)
        if perk then
            local currentLevel = player:getPerkLevel(perk)
            local perkName = perk:getId()
            skills[perkName] = currentLevel
        end
    end

    return skills
end

function PzWebStats.SendMyStats()
    local player = getPlayer()
    local stats = {}
    stats.weight = player:getNutrition():getWeight()
    stats.zombieKills = player:getZombieKills()
    stats.hoursSurvived = player:getHoursSurvived()
    local skills = PzWebStats.GetPlayerSkills(player)
    for perk, data in pairs(skills) do
        stats["skill," .. perk] = data
    end
    sendClientCommand(player, "PzWebStats", "MyStats", stats)
end

function PzWebStats.GetGuidForUsername(username)
    sendClientCommand(getPlayer(), "PzWebStats", "GetGuidForUsername", {username:getUsername()})
end

function PzWebStats.ShowBioForGuid(guid)
    if isSteamOverlayEnabled() then
        activateSteamOverlayToWebPage("https://wastelandrp.net/Character/" .. guid)
    else
        getPlayer():addLineChatElement("Requires Steam Overlay Enabled.", 1.0, 0.4, 0.4)
    end
end

Events.OnServerCommand.Add(function (module, command, args)
    if module == "PzWebStats" then
        if command == "SendStats" then
            PzWebStats.SendMyStats()
        end
        if command == "GuidForUsername" then
            if args and args[1] then
                PzWebStats.ShowBioForGuid(args[1])
            else
                getPlayer():addLineChatElement("Bio URL not synced yet.", 1.0, 0.4, 0.4)
            end
        end
    end
end)

local GetPlayerDisplayName;

if getActivatedMods():contains("WastelandRpChat") then
    GetPlayerDisplayName = function(player)
        return WRC.Meta.GetName(player:getUsername())
    end
else
    GetPlayerDisplayName = function(player)
        return player:getUsername()
    end
end

Events.OnFillWorldObjectContextMenu.Add(function (_, context, worldobjects)
    local firstWorldObject = worldobjects and worldobjects[1]
    if not firstWorldObject then return end
    local sq = firstWorldObject:getSquare()
    if not sq then return end
    local sx = sq:getX()
    local sy = sq:getY()
    local z = sq:getZ()
    for x=sx-1,sx+1 do
        for y=sy-1,sy+1 do
            local s = getCell():getGridSquare(x, y, z)
            if s then
                local objects = s:getObjects()
                for i=0,objects:size()-1 do
                    local object = objects:get(i)
                    if instanceof(object, "IsoPlayer") then
                        local displayName = GetPlayerDisplayName(object)
                        context:addOption("View Bio: " .. displayName, object:getUsername(), PzWebStats.GetGuidForUsername)
                        return
                    end
                end
            end
        end
    end
    
    for _, v in ipairs(worldobjects) do
        local movingObjects = v:getSquare():getMovingObjects()
        for i = 0, movingObjects:size() - 1 do
            local movingObject = movingObjects:get(i)
            if instanceof(movingObject, "IsoPlayer") then
                context:addOption("View Bio", movingObject, PzWebStats.GetGuidForUsername)
                return
            end
        end
    end
end)