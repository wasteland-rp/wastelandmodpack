local maxSearchRange = 10

local textManager = getTextManager()
local overheadEle = ISUIElement:derive("RPD_OverheadEle")
function overheadEle:new(square, text, color, worldItem, range)
    local o = ISUIElement:new(0, 0, 0, 0)
    setmetatable(o, self)
    self.__index = self
    o.square = square
    o.text = text
    o.color = color
    o.worldItem = worldItem
    o.anchorTop = false
    o.anchorBottom = true
    o.range = range
    o.playerIdx = getPlayer():getPlayerNum()
    o:initialise()
    o:addToUIManager()
    o:backMost()
    o:setVisible(true)
    return o
end
function overheadEle:render()
    if not self.worldItem or not self.square:isCanSee(self.playerIdx) then
        return
    end
    local x = self.worldItem:getWorldPosX()
    local y = self.worldItem:getWorldPosY()
    local z = self.worldItem:getWorldPosZ()
    local sx = isoToScreenX(0, x, y, z)
    local sy = isoToScreenY(0, x, y, z)
    if sx < 0 or sy < 0 or sx > getPlayerScreenWidth(0) or sy > getPlayerScreenHeight(0) then
        return
    end
    local w = 0
    for i = 1, #self.text do
        w = math.max(w, textManager:MeasureStringX(UIFont.Small, self.text[i]))
    end
    local h = textManager:MeasureStringY(UIFont.Small, self.text[1]) * #self.text
    local zoom = getCore():getZoom(0)
    self:setX(sx - w/2)
    self:setY(sy - (30 * (1/zoom)))
    -- self:setY(sy - 10 * zoom)
    self:setWidth(w)
    self:setHeight(h)
    for i = 1, #self.text do
        local k = (i - 1) * h / #self.text
        self:drawText(self.text[i], 0, k, self.color.r, self.color.g, self.color.b, 1, UIFont.Small)
    end
end

local itemColors = {
    ["Red"] = { r = 1, g = 0, b = 0 },
    ["Black"] = { r = 0, g = 0, b = 0 },
    ["Yellow"] = { r = 1, g = 1, b = 0 },
    ["Blue"] = { r = 0, g = 0, b = 1 },
    ["Green"] = { r = 0, g = 1, b = 0 },
    ["Pink"] = { r = 1, g = .75, b = 0.79 },
    ["Purple"] = { r = 0.5, g = 0, b = 0.5 },
    ["Orange"] = { r = 1, g = 0.64, b = 0 },
    ["Grey"] = { r = 0.5, g = 0.5, b = 0.5 },
    ["White"] = { r = 1, g = 1, b = 1 },
}

-- Table to track whether the item has already shown its title
local displayedItems = {}

local function isInRange(square1, square2, range)
    if not square1 or not square2 then return false end
    return square1:DistTo(square2) < range
end

-- Function to check for items on the exact square of the player
local function displayOverheads()
    local player = getPlayer()
    if not player then return end

    local playerSquare = player:getSquare()
    if not playerSquare then return end

    local sx = playerSquare:getX()
    local sy = playerSquare:getY()
    local z = playerSquare:getZ()

    for dx=-maxSearchRange,maxSearchRange do
    for dy=-maxSearchRange,maxSearchRange do
        local x = sx + dx
        local y = sy + dy
        local square = getCell():getGridSquare(x, y, z)
        if square then
            local objects = square:getWorldObjects()
            for i=0, objects:size()-1 do
                local object = objects:get(i)
                if object then
                    local item = object:getItem()
                    if item and not displayedItems[item] then
                        local md = item:getModData()
                        if md and md.RPD_Popup then
                            local range = tonumber(md.RPD_Popup)
                            if isInRange(playerSquare, square, range) then
                                local itemType = item:getFullType()
                                local colorKey = nil
                                if luautils.stringStarts(itemType, "RPDescriptors.Exclamation") then
                                    colorKey = itemType:gsub("RPDescriptors.Exclamation", "")
                                elseif luautils.stringStarts(itemType, "RPDescriptors.Question") then
                                    colorKey = itemType:gsub("RPDescriptors.Question", "")
                                end
                                if colorKey and itemColors[colorKey] then
                                    displayedItems[item] = overheadEle:new(square, { item:getName() }, itemColors[colorKey], object, range)
                                end
                            end
                        end
                    end
                end
            end
        end
    end
    end
end

local function removeOverheads()
    local player = getPlayer()
    local playerSquare = player:getSquare()

    for item, element in pairs(displayedItems) do
        if not item or not item:getWorldItem() then
            element:removeFromUIManager()
            displayedItems[item] = nil
        else
            local square = item:getWorldItem():getSquare()
            if not isInRange(playerSquare, square, element.range) then
                element:removeFromUIManager()
                displayedItems[item] = nil
            end
        end
    end
end

local function onPlayerUpdate()
    displayOverheads()
    removeOverheads()
end

local function promptItemOverheadRange(item)
    local scale = getTextManager():getFontHeight(UIFont.Small) / 14
    local width = 220 * scale
    local height = 130 * scale
    local x = (getCore():getScreenWidth() / 2) - (width / 2)
    local y = (getCore():getScreenHeight() / 2) - (height / 2)
    local current = item:getModData().RPD_Popup or 4
    local modal = ISTextBox:new(x, y, width, height, "Range for name to show in world?", tostring(current), nil, function (_, button)
        if button.internal == "OK" then
            local range = tonumber(button.target.entry:getText())
            if range then
                range = math.max(1, math.min(maxSearchRange, range))
                local md = item:getModData()
                md.RPD_Popup = range
            end
        end
    end, nil)
    modal:initialise()
    modal.entry:setOnlyNumbers(true)
    modal:addToUIManager()
end

local function removeItemOverhead(item)
    local md = item:getModData()
    md.RPD_Popup = nil
end

local function showMenu(playerIdx, context, items, test)
    if test then return end
    item = ISInventoryPane.getActualItems(items)[1]
    if not item then return end
    local itemType = item:getFullType()
    if not luautils.stringStarts(itemType, "RPDescriptors.Exclamation") and not luautils.stringStarts(itemType, "RPDescriptors.Question") then
        return
    end
    local player = getSpecificPlayer(playerIdx)
    if not player:getInventory():containsRecursive(item) then return end
    local md = item:getModData()
    if md.RPD_Popup then
        context:addOption("Remove Overhead Popup", item, removeItemOverhead)
        context:addOption("Change Overhead Popup", item, promptItemOverheadRange)
    else
        context:addOption("Enable Overhead Popup", item, promptItemOverheadRange)
    end
end

Events.OnTick.Add(onPlayerUpdate)
Events.OnFillInventoryObjectContextMenu.Add(showMenu)