objects = {
  { name = "", type = "TownZone", x = 10086, y = 5929, z = 0, width = 14, height = 14 },
  { name = "", type = "TownZone", x = 10084, y = 5950, z = 0, width = 15, height = 13 },
  { name = "", type = "TownZone", x = 10107, y = 5937, z = 0, width = 12, height = 13 },
  { name = "", type = "TownZone", x = 10106, y = 5954, z = 0, width = 15, height = 16 },
  { name = "", type = "TownZone", x = 10127, y = 5966, z = 0, width = 11, height = 16 },
  { name = "", type = "Vegitation", z = 0, geometry = "polygon", points = { 10200, 6000, 10022, 6000, 10026, 5984, 10024, 5978, 10031, 5963, 10036, 5944, 10046, 5929, 10063, 5923, 10072, 5925, 10073, 5914, 10067, 5899, 10067, 5887, 10067, 5868, 10078, 5866, 10090, 5863, 10104, 5852, 10113, 5848, 10117, 5850, 10125, 5860, 10124, 5851, 10141, 5864, 10158, 5874, 10171, 5886, 10184, 5886, 10195, 5888, 10200, 5890 } },
  { name = "", type = "Vegitation", z = 0, geometry = "polygon", points = { 10200, 5890, 10229, 5908, 10238, 5931, 10232, 5955, 10243, 5979, 10257, 5985, 10267, 5982, 10297, 5994, 10304, 6000, 10200, 6000 } },
  { name = "", type = "TownZone", x = 10078, y = 6009, z = 0, width = 35, height = 41 },
  { name = "", type = "Vegitation", z = 0, geometry = "polygon", points = { 10022, 6000, 10029, 6018, 10062, 6044, 10090, 6065, 10103, 6069, 10125, 6082, 10141, 6095, 10147, 6093, 10156, 6096, 10170, 6090, 10173, 6081, 10169, 6068, 10167, 6057, 10171, 6046, 10167, 6034, 10162, 6029, 10154, 6012, 10150, 6000 } },
  { name = "", type = "Vegitation", z = 0, geometry = "polygon", points = { 10200, 6000, 10157, 6000, 10158, 6005, 10160, 6012, 10161, 6017, 10167, 6027, 10170, 6028, 10178, 6035, 10181, 6046, 10179, 6053, 10177, 6059, 10184, 6073, 10191, 6081, 10200, 6085 } },
  { name = "", type = "Vegitation", z = 0, geometry = "polygon", points = { 10200, 6000, 10200, 6085, 10227, 6097, 10238, 6089, 10254, 6086, 10263, 6085, 10273, 6080, 10276, 6085, 10283, 6092, 10289, 6097, 10285, 6086, 10281, 6080, 10280, 6071, 10288, 6055, 10303, 6041, 10309, 6022, 10310, 6015, 10308, 6009, 10304, 6000 } }
}
