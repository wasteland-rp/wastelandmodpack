module Base
{
	item Hat_SWATNeck
    {
        Type = Clothing,
   	  FabricType = Leather,
        DisplayName = Swat Neck Armour,
        ClothingItem = Hat_SWATNeck,
        BodyLocation = Neck,
        BloodLocation = Neck,
        Icon = Hat_SwatNeck,
	  RunSpeedModifier = 0.9,
	  CanHaveHoles = false,
	  ConditionLowerChanceOneIn = 250,
        ScratchDefense = 30,
	  BiteDefense = 20,
        Insulation = 0.2,
        WindResistance = 0.3,
	  WaterResistance = 0.3,
	  Weight = 3,
    }

    item Hat_SwatGasMask
	{
	  Type = Clothing,
	  FabricType = Leather,
  	  DisplayName = Swat Gas Mask,
	  ClothingItem = Hat_SwatGasMask,
	  BodyLocation = MaskEyes,
	  Icon = Hat_SwatGasMask,
	  CanHaveHoles = false,
	  BloodLocation = Head;Neck,
	  Insulation = 1.0,
	  WindResistance = 1.0,
        ScratchDefense = 30,
	  BiteDefense = 25,
	}

    item SwatElbowPads
	{
	  Type = Clothing,
	  FabricType = Leather,
	  DisplayName = Swat Elbow Pads,
	  ClothingItem = SwatElbowPads,
   	  BodyLocation = Scarf,
	  BloodLocation = UpperArms,
	  Icon = SwatElbowpads,
	  ConditionLowerChanceOneIn = 15,
	  	  CombatSpeedModifier = 0.97,
        ConditionMax = 27,
        RemoveOnBroken = true,
        ScratchDefense = 10,
        BiteDefense = 10,
        WorldStaticModel = BoilerSuit_Ground,
	}


    item SwatKneePads
	{
	  Type = Clothing,
	  FabricType = Leather,
	  DisplayName = Swat Knee Pads,
	  ClothingItem = SwatKneePads,
	  BodyLocation = Sweater,
	  BloodLocation = Trousers,
	  Icon = SwatKneepads,
	  ConditionLowerChanceOneIn = 15,
	  	  RunSpeedModifier = 0.97,
        ConditionMax = 27,
        RemoveOnBroken = true,
        ScratchDefense = 10,
        BiteDefense = 10,
        WorldStaticModel = Hazmat_Ground,
	}

    item SwatShoulderPads
	{
	  Type = Clothing,
	  FabricType = Leather,
	  DisplayName = Swat Shoulder Pads,
   	  ClothingItem = SwatShoulderPads,
	  BodyLocation = Tail,
	  BloodLocation = UpperArms,
	  Icon = SwatShoulderPads,
	  ConditionLowerChanceOneIn = 50,
        ConditionMax = 22,
        RemoveOnBroken = true,
	  CombatSpeedModifier = 0.95,
        ScratchDefense = 10,
        BiteDefense = 10,
	  Weight = 1,
        WorldStaticModel = BoilerSuit_Ground,
	}

    item Trousers_Swat
    	{
        Type = Clothing,
	  FabricType = Leather,
        DisplayName = Swat Combat Pants,
        ClothingItem = Trousers_Swat,
        BodyLocation = Skirt,
        Icon = Trousers_Swat,
        BloodLocation = Trousers,
        RunSpeedModifier = 0.85,
        BiteDefense = 20,
        ScratchDefense = 30,
        Insulation = 0.85,
        WindResistance = 0.85,
        WaterResistance = 0.8,
        WorldStaticModel = Trousers_Ground,
   	}

    item Shoes_RiotBoots
	{
	  Type = Clothing,
	  FabricType = Leather,
	  DisplayName = Riot Boots,
	  ClothingItem = Shoes_RiotBoots,
	  BodyLocation = Shoes,
	  BloodLocation = Shoes,
	  Icon = RiotBoots,
	  StompPower = 2.5,
	  Weight = 1,
	  ConditionLowerChanceOneIn = 15,
        ConditionMax = 27,
        RunSpeedModifier = 0.95,
        RemoveOnBroken = false,
        ScratchDefense = 80,
        BiteDefense = 50,
        Insulation = 0.9,
        WindResistance = 0.65,
        WaterResistance = 0.65,
        WorldStaticModel = Boots_Ground,
	}

    item Shoes_SwatBoots
	{
	  Type = Clothing,
	  FabricType = Leather,
	  DisplayName = Swat Tactical Boots,
	  ClothingItem = Shoes_SwatBoots,
	  BodyLocation = Shoes,
	  BloodLocation = Shoes,
	  Icon = SwatBoots,
  	  StompPower = 2.0,
	  ConditionLowerChanceOneIn = 13,
        ConditionMax = 25,
        RunSpeedModifier = 1.0,
        RemoveOnBroken = false,
        ScratchDefense = 50,
        BiteDefense = 30,
        Insulation = 0.9,
        WindResistance = 0.45,
        WaterResistance = 0.45,
        WorldStaticModel = Boots_Ground,
	}


    item Gloves_SwatGloves
	{
	  Type = Clothing,
	  FabricType = Leather,
	  DisplayName = Swat Fingerless Kevlar Gloves,
	  ClothingItem = Gloves_SwatFingerlessGloves,
	  BodyLocation = Hands,
	  BloodLocation = Hands,
	  Icon = SwatGloves,
	  ScratchDefense = 30,
        BiteDefense = 15,
        Weight = 0.2,
        Insulation = 0.75,
        WindResistance = 0.75,
        WorldStaticModel = Gloves_Ground,
	}

    item Gloves_RiotGloves
	{
	Type = Clothing,
	FabricType = Leather,
	DisplayName = Riot Full Kevlar Gloves,
	ClothingItem = Gloves_RiotGloves,
	BodyLocation = Hands,
	BloodLocation = Hands,
	Icon = RiotGloves,
	ScratchDefense = 30,
        BiteDefense = 15,
	  CombatSpeedModifier = 0.99,
        Weight = 0.3,
        Insulation = 0.75,
        WindResistance = 0.75,
        WorldStaticModel = Gloves_Ground,
	}

    item Hat_SwatHelmet
	{
	  Type = Clothing,
	  DisplayName = SWAT Helmet,
	  ClothingItem = Hat_SwatHelmet,
	  BodyLocation = Hat,
	  Icon = Hat_SwatHelmet,
	  CanHaveHoles = false,
	  BloodLocation = Head,
	  BiteDefense = 100,
	  ScratchDefense = 100,
	  ChanceToFall = 5,
	  Insulation = 0.45,
	  WaterResistance = 0.40,
        Weight = 1.5,
    	}

    item Vest_BulletSwat
   	{
        Type = Clothing,
	  FabricType = Leather,
        DisplayName = Swat Bulletproof Vest,
        ClothingItem = Vest_BulletSwat,
        BodyLocation = TorsoExtra,
        Icon = Vest_BulletSwat,
        BloodLocation = ShirtNoSleeves,
	    BiteDefense = 30,
	    ScratchDefense = 55,
        Insulation = 0.7,
        WindResistance = 0.7,
        Weight = 2.0,
        BulletDefense = 100,
        WorldStaticModel = BulletVest_Ground,
   	}

    item Jacket_Swat
    	{
        Type = Clothing,
	  FabricType = Cotton,
        DisplayName = Swat Jacket,
        ClothingItem = Jacket_Swat,
        BodyLocation = Jacket,
        Icon = JacketSwat,
        BloodLocation = Jacket,
        RunSpeedModifier = 0.9,
        CombatSpeedModifier = 0.9,
        BiteDefense = 50,
        ScratchDefense = 50,
        Insulation = 0.85,
        WindResistance = 0.85,
        WaterResistance = 0.8,
        Weight = 3,
        WorldStaticModel = Jacket_Ground,
   	}

    item Glasses_SwatGoggles
   	{
	Type = Clothing,
	DisplayName = Swat Protection Goggles,
	ClothingItem = Glasses_SwatGoggles,
	BodyLocation = Eyes,
	Icon = SwatGoggles,
	CombatSpeedModifier = 1.1,
	Weight = 0.1,
	ChanceToFall = 10,
	
    	}

    item Hat_Balaclava_Swat
    	{
	Type = Clothing,
	FabricType = Cotton,
	DisplayName = Balaclava Swat,
	ClothingItem = Hat_Balaclava_Swat,
	BodyLocation = Mask,
	Icon = Hat_Balaclava_Swat,
	CanHaveHoles = false,
	BloodLocation = Head;Neck,
	Insulation = 1.0,
	WindResistance = 1.0,
      ScratchDefense = 10,
	}

    item Hat_PoliceRiotHelmet
    	{
	Type = Clothing,
	DisplayName = Police Riot Helmet,
	ClothingItem = Hat_PoliceRiotHelmet,
	BodyLocation = Hat,
	Icon = Hat_PoliceRiotHelmet,
	BloodLocation = FullHelmet,
        NeckProtectionModifier = 0.7,
	CanHaveHoles = false,
	CombatSpeedModifier = 0.95,
	BiteDefense = 100,
      	ScratchDefense = 100,
       	ChanceToFall = 1,
       	Insulation = 0.45,
        WindResistance = 0.65,
        WaterResistance = 1.0,
        Weight = 2,
	}

	item Hat_SWATRiotHelmet
    	{
	Type = Clothing,
	DisplayName = SWAT Riot Helmet (Open),
	ClothingItem = Hat_SWATRiotHelmet,
	BodyLocation = Hat,
	Icon = Hat_SWATRiotHelmet_open,
	BloodLocation = FullHelmet,
	ClothingItemExtra = Hat_SWATRiotHelmet2,
        ClothingItemExtraOption = CloseHelmet,
        NeckProtectionModifier = 0.7,
	CanHaveHoles = false,
	CombatSpeedModifier = 0.95,
	BiteDefense = 80,
      	ScratchDefense = 80,
       	ChanceToFall = 1,
       	Insulation = 0.45,
        WindResistance = 0.45,
        WaterResistance = 0.7,
        Weight = 2,
	}

	item Hat_SWATRiotHelmet2
    	{
	  Type = Clothing,
	  DisplayName = SWAT Riot Helmet (Closed),
	  ClothingItem = Hat_SWATRiotHelmet2,
	  BodyLocation = Hat,
	  Icon = Hat_SWATRiotHelmet_closed,
	  BloodLocation = FullHelmet,
	  ClothingItemExtra = Hat_SWATRiotHelmet,
        ClothingItemExtraOption = OpenHelmet,
        NeckProtectionModifier = 0.7,
	  CanHaveHoles = false,
	  CombatSpeedModifier = 0.95,
	  BiteDefense = 100,
        ScratchDefense = 100,
        ChanceToFall = 1,
        Insulation = 0.45,
        WindResistance = 0.65,
        WaterResistance = 1.0,
        Weight = 2,
	}

    item RiotArmorSuit
    	{
        Type = Clothing,
	  FabricType = Leather,
        DisplayName = Riot Armor Suit,
        ClothingItem = RiotArmorSuit,
        BodyLocation = FullSuit,
        Icon = RiotArmorSuit,
        BloodLocation = Trousers;Jumper,
        Icon = RiotArmorSuit,
        RunSpeedModifier = 0.55,
        ScratchDefense = 40,
        BiteDefense = 40,
        Insulation = 0.65,
        WindResistance = 0.9,
        Weight = 10,
        BulletDefense = 20,
        WorldStaticModel = BoilerSuit_Ground,
    	}

    item Hat_Antibombhelmet
    	{
	  Type = Clothing,
	  DisplayName = EOD Helmet,
	  ClothingItem = Hat_Antibombhelmet,
  	  BodyLocation = Hat,
	  Icon = Hat_Antibombhelmet,
	  BloodLocation = FullHelmet;Neck,
	  CanHaveHoles = false,
        RunSpeedModifier = -0.5,
 	  CombatSpeedModifier = -0.5,
	  BiteDefense = 100,
        ScratchDefense = 100,
        ChanceToFall = 0,
        Insulation = 1.0,
        WindResistance = 1.0,
        WaterResistance = 0.1,
        Weight = 4,
	}

    item AntibombSuit
    	{
        Type = Clothing,
	  FabricType = Leather,
        DisplayName = EOD Suit Part 1,
        ClothingItem = AntibombSuit,
        BodyLocation = FullSuit,
        Icon = AntibombSuit,
        BloodLocation = Trousers;Jumper;Shoes;Hands,
	  ConditionLowerChanceOneIn = 15,
        ConditionMax = 30,
        Icon = AntibombSuit,
        RunSpeedModifier = -0.5,
	  CombatSpeedModifier = -0.5,
        ScratchDefense = 75,
        BiteDefense = 75,
  	  Insulation = 1.0,
	  WindResistance = 1.0,
        WaterResistance = 0.1,
        Weight = 10,
	  StompPower = 3.5,
        BulletDefense = 80,
        WorldStaticModel = BoilerSuit_Ground,
    	}

    item AntibombSuitP2
    	{
        Type = Clothing,
	  FabricType = Leather,
        DisplayName = EOD Suit Part 2,
        ClothingItem = AntibombSuitP2,
        BodyLocation = Underwear,
        Icon = AntibombSuit,
        BloodLocation = Trousers;Jumper;Shoes;Hands,
        Icon = AntibombSuit,
        RunSpeedModifier = -0.5,
	  CombatSpeedModifier = -0.5,
        ScratchDefense = 25,
        BiteDefense = 25,
        Insulation = 1.0,
	  WindResistance = 1.0,
        WaterResistance = 0.1,
        Weight = 10,
        WorldStaticModel = Apron_Ground,
    	}
}