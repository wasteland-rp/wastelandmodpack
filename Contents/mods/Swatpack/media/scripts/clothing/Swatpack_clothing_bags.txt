module Base
{

    item SWATPouch
    {
        Type = Container,
        DisplayName = SWAT Leg Pouch,
        ClothingItem = SWATPouch,
        BodyLocation = Belt421,
        Icon = SPouch,
        WeightReduction	= 50,
		Capacity = 3,
        ClothingItemExtra = SWATPouch,
        ClothingItemExtraOption = WearIt,
        CanBeEquipped = Belt421,
        PutInSound = PutItemInBag,
        Weight = 0.5,
        WorldStaticModel = GolfBag_Ground,
    }

    item Bag_BigSwatBag
    {
        Type = Container,
        DisplayName = Swat Bag,
        ClothingItem = Bag_BigSwatBag,
        CanBeEquipped = Back,
        WeightReduction	= 85,
        Weight	= 3,
        Capacity = 35,
        Icon = Bag_BigSwatBag,
        OpenSound = OpenBag,
        CloseSound = CloseBag,
        PutInSound = PutItemInBag,
        BloodLocation = Bag,
        CanHaveHoles = false,
        AttachmentReplacement = Bag,
        ReplaceInSecondHand = BigSwatBag_LHand holdingbagleft,
        ReplaceInPrimaryHand = BigSwatBag_RHand holdingbagright,
        WorldStaticModel = BigHikingBag_Ground,
    }
    item Bag_PoliceBag
    {
        Type = Container,
        DisplayName = Police Bag,
        ClothingItem = Bag_PoliceBag,
        CanBeEquipped = Back,
        WeightReduction	= 90,
        Weight = 2,
        Capacity = 22,
        Icon = Bag_PoliceBag,
        OpenSound = OpenBag,
        CloseSound = CloseBag,
        PutInSound = PutItemInBag,
        BloodLocation = Bag,
        CanHaveHoles = false,
        AttachmentReplacement = Bag,
        ReplaceInSecondHand = PoliceBag_LHand holdingbagleft,
        ReplaceInPrimaryHand = PoliceBag_RHand holdingbagright,
        WorldStaticModel = ALICE_Pack_Ground,
    }
}