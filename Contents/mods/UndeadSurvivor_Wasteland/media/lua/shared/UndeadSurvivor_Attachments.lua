--***********************************************************
--**                    THE INDIE STONE                    **
--***********************************************************

-- Adds attachment positions on Items

local group = AttachedLocations.getGroup("Human")
group:getOrCreateLocation("PrepperKnife"):setAttachmentName("Prepper_Vest_Knife")
group:getOrCreateLocation("PrepperFlashlight"):setAttachmentName("Prepper_Vest_Flashlight")
group:getOrCreateLocation("PrepperLantern"):setAttachmentName("Prepper_Vest_Lantern")
group:getOrCreateLocation("PrepperHolster"):setAttachmentName("Prepper_Trousers_Pistol")
group:getOrCreateLocation("GunMagazine1"):setAttachmentName("Prepper_Vest_Mag1")
group:getOrCreateLocation("GunMagazine2"):setAttachmentName("Prepper_Vest_Mag2")
group:getOrCreateLocation("GunMagazine3"):setAttachmentName("Prepper_Vest_Mag3")
group:getOrCreateLocation("GunMagazine4"):setAttachmentName("Prepper_Vest_Mag4")
group:getOrCreateLocation("Bounty Letter"):setAttachmentName("bounty_letter")

-- Add Headhunter Scope model to Vanilla and FireamsB41 Weapons

local riflesScope = {
    "Base.VarmintRifle",
    "Base.HuntingRifle",
    "Base.AssaultRifle",
    "Base.AssaultRifle2",
    "Base.HuntingRifle_Sawn",
    "Base.Rugerm7722",
    "Base.M24Rifle",
    "Base.M1Garand",
    "Base.FN_FAL",
    "Base.M16A2",
    "Base.AR15",
    "Base.M60",
    "Base.AK47"
}

for _, rifle in ipairs(riflesScope) do
    local item = ScriptManager.instance:getItem(rifle)
    if item then
        item:DoParam("ModelWeaponPart = HeadhunterScope Base.HeadhunterScope scope scope")
    end
end

local magazines = {
    "Base.9mmClip",
    "Base.45Clip",
    "Base.44Clip",
    "Base.223Clip",
    "Base.308Clip",
    "Base.556Clip",
    "Base.M14Clip",
    "Base.Glock17Mag",
    "Base.MP5Mag",
    "Base.UZIMag",
    "Base.AK_Mag",
    "Base.M60Mag",
    "Base.FN_FAL_Mag",
    "Base.Mac10Mag",
    "Base.22Clip",
    "Base.HandcraftedRifleClip"
}

for _, mag in ipairs(magazines) do
    local item = ScriptManager.instance:getItem(mag)
    if item then
        item:DoParam("AttachmentType = GunMagazine")
    end
end

-- Allow storing boxes of ammo in the vest
local ammoBoxes = {
    "Base.Bullets9mmBox",
    "Base.Bullets45Box",
    "Base.Bullets44Box",
    "Base.Bullets4440Box",
    "Base.Bullets38Box",
    "Base.Bullets357Box",
    "Base.ShotgunShellsBox",
    "Base.223Box",
    "Base.308Box",
    "Base.556Box",
    "Base.762x51Box",
    "Base.762x39Box",
    "Base.Bullets22Box",
    "Base.Bullets3006Box",
    "Base.RubberShellsBox",
    "GreysWLWeaponsFirearms.ScrapShardsBox",
    "GreysWLWeaponsFirearms.ScrapBallsBox",
    "Base.Bullets9mmBox",
    "Base.Bullets45Box",
    "Base.Bullets44Box",
    "Base.Bullets38Box",
    "Base.ShotgunShellsBox",
    "Base.223Box",
    "Base.308Box",
    "Base.556Box",
}

for _, box in ipairs(ammoBoxes) do
    local item = ScriptManager.instance:getItem(box)
    if item then
        item:DoParam("AttachmentType = GunMagazine")
    end
end

-- Add Vanilla and FirearmsB41 Attachments to Headhunter Rifle
local attachments = {
    ["Base.308Silencer"] = "AssaultRifle2; M24Rifle; FN_FAL; SKS; AK47; DeadlyHeadhunterRifle; HeadhunterRifle",
    ["Base.ImprovisedSilencer"] = "Pistol; Pistol2; Glock17; AR15; AssaultRifle2; M24Rifle; FN_FAL; SKS; AK47; VarmintRifle; HuntingRifle; HuntingRifle_Sawn; AssaultRifle; M16A2; M733; MP5; UZI; Mac10; Revolver_Short; Revolver; Revolver_Long; ColtPeacemaker; Shotgun; Mossberg500; Mossberg500Tactical; Remington870Wood; M37; SPAS12; LAW12; Winchester73; Winchester94; Rossi92; ColtSingleAction22; ColtAnaconda; ColtPython; ColtPythonHunter; ColtAce; M1Garand; DeadlyHeadhunterRifle; HeadhunterRifle",
    ["Base.Silencer_PopBottle"] = "Pistol; Pistol2; Glock17; AR15; AssaultRifle2; M24Rifle; FN_FAL; SKS; AK47; VarmintRifle; HuntingRifle; HuntingRifle_Sawn; AssaultRifle; M16A2; M733; MP5; UZI; Mac10; Revolver_Short; Revolver; Revolver_Long; ColtPeacemaker; Shotgun; Mossberg500; Mossberg500Tactical; Remington870Wood; M37; SPAS12; LAW12; Winchester73; Winchester94; Rossi92; ColtSingleAction22; ColtAnaconda; ColtPython; ColtPythonHunter; ColtAce; M1Garand; DeadlyHeadhunterRifle; HeadhunterRifle",
    ["Base.RecoilPad"] = "HuntingRifle; VarmintRifle; AssaultRifle; AssaultRifle2; Base.Winchester94; Base.M24Rifle; Shotgun; Base.Mossberg500; Base.Remington870Wood; Rugerm7722; DeadlyHeadhunterRifle; HeadhunterRifle",
    ["Base.ExtendedRecoilPad"] = "HuntingRifle; VarmintRifle; M24Rifle; DeadlyHeadhunterRifle; HeadhunterRifle",
    ["Base.Rifle_Bipod"] = "HuntingRifle; VarmintRifle; M24Rifle; HuntingRifle_Sawn; AssaultRifle2; FN_FAL; DeadlyHeadhunterRifle; HeadhunterRifle",
    ["Base.Sling"] = "HuntingRifle; VarmintRifle; ShotgunSawnoff; AR15; AK47; SKS; Base.Winchester94; Winchester73; Base.M60; Base.HuntingRifle_Sawn; Base.Remington870Sawnoff; Shotgun; Base.Mossberg500; AssaultRifle; Base.M16A2; M733; AssaultRifle2; Base.M24Rifle; Base.Remington870Wood; Mossberg500Tactical; SPAS12; LAW12; Base.MP5; Base.UZI; Rugerm7722; FN_FAL; M1Garand; DeadlyHeadhunterRifle; HeadhunterRifle",
    ["Base.Sling_Leather"] = "HuntingRifle; VarmintRifle; ShotgunSawnoff; AR15; AK47; SKS; Base.Winchester94; Winchester73; Base.M60; Base.HuntingRifle_Sawn; Base.Remington870Sawnoff; Shotgun; Base.Mossberg500; AssaultRifle; Base.M16A2; M733; AssaultRifle2; Base.M24Rifle; Base.Remington870Wood; Mossberg500Tactical; SPAS12; LAW12; Base.MP5; Base.UZI; Rugerm7722; FN_FAL; M1Garand; DeadlyHeadhunterRifle; HeadhunterRifle",
    ["Base.Sling_Olive"] = "HuntingRifle; VarmintRifle; ShotgunSawnoff; AR15; AK47; SKS; Base.Winchester94; Winchester73; Base.M60; Base.HuntingRifle_Sawn; Base.Remington870Sawnoff; Shotgun; Base.Mossberg500; AssaultRifle; Base.M16A2; M733; AssaultRifle2; Base.M24Rifle; Base.Remington870Wood; Mossberg500Tactical; SPAS12; LAW12; Base.MP5; Base.UZI; Rugerm7722; FN_FAL; M1Garand; DeadlyHeadhunterRifle; HeadhunterRifle",
    ["Base.Sling_Camo"] = "HuntingRifle; VarmintRifle; ShotgunSawnoff; AR15; AK47; SKS; Base.Winchester94; Winchester73; Base.M60; Base.HuntingRifle_Sawn; Base.Remington870Sawnoff; Shotgun; Base.Mossberg500; AssaultRifle; Base.M16A2; M733; AssaultRifle2; Base.M24Rifle; Base.Remington870Wood; Mossberg500Tactical; SPAS12; LAW12; Base.MP5; Base.UZI; Rugerm7722; FN_FAL; M1Garand; DeadlyHeadhunterRifle; HeadhunterRifle",
    ["Base.AmmoStraps"] = "HuntingRifle; VarmintRifle; ShotgunSawnoff; AR15; AK47; SKS; Base.Winchester94; Winchester73; Base.M60; Base.HuntingRifle_Sawn; Base.Remington870Sawnoff; Shotgun; Base.Mossberg500; AssaultRifle; Base.M16A2; M733; AssaultRifle2; Base.M24Rifle; Base.Remington870Wood; Mossberg500Tactical; SPAS12; LAW12; Base.MP5; Base.UZI; Rugerm7722; FN_FAL; M1Garand; DeadlyHeadhunterRifle; HeadhunterRifle",
    ["Base.Laser"] = "Pistol; Base.Glock17; Pistol2; Pistol3; AssaultRifle; AssaultRifle2; FN_FAL; M16A2; M733; AR15; M24Rifle; DeadlyHeadhunterRifle; HeadhunterRifle",
    ["Base.IronSight"] = "HuntingRifle; Base.HuntingRifle_Sawn; VarmintRifle; Pistol; Pistol2; Pistol3; Revolver; Revolver_Long; AssaultRifle; AssaultRifle2; Base.M24Rifle; Rugerm7722; DeadlyHeadhunterRifle; HeadhunterRifle",
    ["Base.RedDot"] = "Pistol; Pistol2; Pistol3; Revolver; Revolver_Long; AssaultRifle; AssaultRifle2; HuntingRifle; VarmintRifle; Base.Glock17; Base.HuntingRifle_Sawn; Shotgun; ShotgunSawnoff; Base.Remington870Sawnoff; Base.Mossberg500; Mossberg500Tactical; LAW12; Base.Remington870Wood; Base.MP5; Base.UZI; Base.M24Rifle; Base.M16A2; M733; Rugerm7722; FN_FAL; DeadlyHeadhunterRifle; HeadhunterRifle",
    ["Base.x2Scope"] = "HuntingRifle; VarmintRifle; Base.HuntingRifle_Sawn; AssaultRifle; Base.M60; Base.M16A2; M733; AssaultRifle2; Base.M24Rifle; Base.MP5; Base.UZI; Mossberg500Tactical; LAW12; Base.Mossberg500; Shotgun; ShotgunSawnoff; Base.Remington870Sawnoff; Base.Remington870Wood; Rugerm7722; FN_FAL; DeadlyHeadhunterRifle; HeadhunterRifle",
    ["Base.x4Scope"] = "HuntingRifle; VarmintRifle; Base.HuntingRifle_Sawn; AssaultRifle; AssaultRifle2; Base.M16A2; M733; AssaultRifle2; Base.M24Rifle; Base.MP5; Mossberg500Tactical; LAW12; Rugerm7722; FN_FAL; DeadlyHeadhunterRifle; HeadhunterRifle",
    ["Base.x8Scope"] = "HuntingRifle; VarmintRifle; Base.HuntingRifle_Sawn; AssaultRifle; AssaultRifle2; Base.M24Rifle; Rugerm7722; DeadlyHeadhunterRifle; HeadhunterRifle",
    ["Base.x4-x12Scope"] = "HuntingRifle; VarmintRifle; HuntingRifle_Sawn; AssaultRifle2; M24Rifle; Rugerm7722; DeadlyHeadhunterRifle; HeadhunterRifle"
}

for attachment, mountOn in pairs(attachments) do
    local item = ScriptManager.instance:getItem(attachment)
    if item then
        item:DoParam("MountOn = " .. mountOn)
    end
end
