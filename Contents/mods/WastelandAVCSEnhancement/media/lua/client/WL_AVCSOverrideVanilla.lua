require "AVCSOverrideVanilla.lua"
require "TimedActions/ISInventoryTransferAction"

if not AVCS.oISInventoryTransferAction then
    AVCS.oISInventoryTransferAction = ISInventoryTransferAction.isValid
end

function ISInventoryTransferAction:isValid()
    local vehicle = nil
    if self.destContainer:getParent() and instanceof(self.destContainer:getParent(), "BaseVehicle") then
        vehicle = self.destContainer:getParent()
    end
    if self.srcContainer:getParent() and instanceof(self.srcContainer:getParent(), "BaseVehicle") then
        vehicle = self.srcContainer:getParent()
    end

    if vehicle then
        local isPublic = AVCS.getPublicPermission(vehicle, "AllowOpeningTrunk")
        local hasPerm =  AVCS.getSimpleBooleanPermission(AVCS.checkPermission(self.character, vehicle))

        if not isPublic and not hasPerm then
            self.character:setHaloNote(getText("IGUI_AVCS_Vehicle_No_Permission"), 250, 250, 250, 300)
            return false
        end
    end

    return AVCS.oISInventoryTransferAction(self)
end

local function claimVehicle(player, button, vehicle)
    if button.internal == "NO" then return end
    if luautils.walkAdj(player, vehicle:getSquare()) then
        ISTimedActionQueue.add(ISAVCSVehicleClaimAction:new(player, vehicle))
    end
end

local function claimCfmDialog(player, vehicle)
    local message = string.format("Confirm", vehicle:getScript():getName())
    local playerNum = player:getPlayerNum()
    local modal = ISModalDialog:new((getCore():getScreenWidth() / 2) - (300 / 2), (getCore():getScreenHeight() / 2) - (150 / 2), 300, 150, message, true, player, claimVehicle, playerNum, vehicle)
    modal:initialise();
    modal:addToUIManager();
end

local function unclaimVehicle(player, button, vehicle)
    if button.internal == "NO" then return end
    if luautils.walkAdj(player, vehicle:getSquare()) then
        ISTimedActionQueue.add(ISAVCSVehicleUnclaimAction:new(player, vehicle))
    end
end

local function unclaimCfmDialog(player, vehicle)
    local message = string.format("Confirm", vehicle:getScript():getName())
    local playerNum = player:getPlayerNum()
    local modal = ISModalDialog:new((getCore():getScreenWidth() / 2) - (300 / 2), (getCore():getScreenHeight() / 2) - (150 / 2), 300, 150, message, true, player, unclaimVehicle, playerNum, vehicle)
    modal:initialise();
    modal:addToUIManager();
end

-- Do not show expiration time
-- Copy and override the vanilla menu to add our context menu in
function AVCS.addOptionToMenuOutsideVehicle(player, context, vehicle)
	-- Ignore wrecks
	if string.match(string.lower(vehicle:getScript():getName()), "burnt") or string.match(string.lower(vehicle:getScript():getName()), "smashed") then
		return
	end

	local checkResult = AVCS.checkPermission(player, vehicle)
	local option
	local toolTip
	toolTip = ISToolTip:new()
	toolTip:initialise()
	toolTip:setVisible(false)

	if type(checkResult) == "boolean" then
		if checkResult == true then
			local playerInv = player:getInventory()
			-- Free car
			option = context:addOption(getText("ContextMenu_AVCS_ClaimVehicle"), player, claimCfmDialog, vehicle)
			option.toolTip = toolTip
			if playerInv:getItemCount("Base.AVCSClaimOrb") < 1 and SandboxVars.AVCS.RequireTicket then
				toolTip.description = getText("Tooltip_AVCS_Needs") .. " <LINE><RGB:1,0.2,0.2>" .. getItemNameFromFullType("Base.AVCSClaimOrb") .. " " .. playerInv:getItemCount("Base.AVCSClaimOrb") .. "/1"
				option.notAvailable = true
			else
				if AVCS.checkMaxClaim(player) then
					if SandboxVars.AVCS.RequireTicket then
						toolTip.description = getText("Tooltip_AVCS_Needs") .. " <LINE><RGB:0.2,1,0.2>" .. getItemNameFromFullType("Base.AVCSClaimOrb") .. " " .. playerInv:getItemCount("Base.AVCSClaimOrb") .. "/1"
					else
						toolTip.description = getText("Tooltip_AVCS_ClaimVehicle")
					end
					option.notAvailable = false
				else
					toolTip.description = "<RGB:0.2,1,0.2>" .. getText("Tooltip_AVCS_ExceedLimit")
					option.notAvailable = true
				end
			end
		elseif checkResult == false then
			-- Not supported vehicle
			option = context:addOption(getText("ContextMenu_AVCS_UnsupportedVehicle"), player, claimCfmDialog, vehicle)
			option.toolTip = toolTip
			toolTip.description = getText("Tooltip_AVCS_Unsupported")
			option.notAvailable = true
		end
	elseif checkResult.permissions == true then
		-- Owned car
		option = context:addOption(getText("ContextMenu_AVCS_UnclaimVehicle"), player, unclaimCfmDialog, vehicle)
		option.toolTip = toolTip
		toolTip.description = getText("Tooltip_AVCS_Owner") .. ": " .. checkResult.ownerid
		option.notAvailable = false
	elseif checkResult.permissions == false then
		-- Owned car
		option = context:addOption(getText("ContextMenu_AVCS_UnclaimVehicle"), player, unclaimCfmDialog, vehicle)
		option.toolTip = toolTip
		toolTip.description = getText("Tooltip_AVCS_Owner") .. ": " .. checkResult.ownerid
		option.notAvailable = true
	end

	-- Must not be towing or towed
	if vehicle:getVehicleTowedBy() ~= nil or vehicle:getVehicleTowing() ~= nil then
		toolTip.description = getText("Tooltip_AVCS_Towed")
		option.notAvailable = true
	end
end