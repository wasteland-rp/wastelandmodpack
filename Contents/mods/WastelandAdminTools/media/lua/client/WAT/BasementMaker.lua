WAT_BasementMaker = WAT_BasementMaker or {
    basementsData = {},
    tps = {}
}

function WAT_BasementMaker.isEnabled()
    return SandboxVars.WastelandAdminTools.BasementsStartPoint ~= "None" and
    SandboxVars.WastelandAdminTools.BasementsTpInPoint ~= "None"  and
    SandboxVars.WastelandAdminTools.BasementsTpOutPoint ~= "None"  and
    SandboxVars.WastelandAdminTools.BasementsSpacing ~= "None"  and
    SandboxVars.WastelandAdminTools.BasementsCopyPasteName ~= "None"
end

function WAT_BasementMaker.removeBasement(key)
    sendClientCommand(getPlayer(), 'WAT', 'removeBasement', {key = key})
end

WAT_BasementMaker.MakeBasementWindow = ISPanel:derive("WAT_BasementMaker_MakeBasementWindow")
function WAT_BasementMaker.MakeBasementWindow:new()
    local w = 200
    local h = 75
    local x = (getCore():getScreenWidth() / 2) - (w / 2)
    local y = (getCore():getScreenHeight() / 2) - (h / 2)
    local o = ISPanel:new(x, y, w, h)
    setmetatable(o, self)
    self.__index = self
    o:initialise()
    o:addToUIManager()
    o:setAlwaysOnTop(true)
    return o
end

function WAT_BasementMaker.MakeBasementWindow:initialise()
    ISPanel.initialise(self)
    self.moveWithMouse = true
    self.backgroundColor = {r=0, g=0, b=0, a=0.8}

    local win = GravyUI.Node(self.width, self.height, self):pad(5)
    local label, buttons = win:rows({0.4, 0.6}, 10)
    local setOutButton, setInButton, goButton, closeButton = buttons:cols(4, 10)
    self.label = label:makeLabel("Make a Basement", UIFont.AutoNormMedium, {r=1, g=1, b=1, a=1}, "center")
    self.outButton = setOutButton:makeButton("To", self, self.setOut)
    self.inButton = setInButton:makeButton("From", self, self.setIn)
    self.goButton = goButton:makeButton("Go", self, self.go)
    self.closeButton = closeButton:makeButton("Close", self, self.close)

    self.goButton:setEnable(false)
end

function WAT_BasementMaker.MakeBasementWindow:setOut()
    local player = getPlayer()
    self.outPoint = {
        x = math.floor(player:getX()),
        y = math.floor(player:getY()),
        z = math.floor(player:getZ())
    }
    self.outButton.textColor = {r=0.5, g=1.0, b=0.5, a=1}
end

function WAT_BasementMaker.MakeBasementWindow:setIn()
    local player = getPlayer()
    self.inPoint = {
        x = math.floor(player:getX()),
        y = math.floor(player:getY()),
        z = math.floor(player:getZ())
    }
    self.inButton.textColor = {r=0.5, g=1.0, b=0.5, a=1}
end

function WAT_BasementMaker.MakeBasementWindow:go()
    self.goButton:setEnable(false)
    self.inButton:setEnable(false)
    self.outButton:setEnable(false)

    self.label:setText("Finding basement position.")
    self.stage = "teleportToBasementPosition"
    self.stageDelay = 10
end

function WAT_BasementMaker.MakeBasementWindow:close()
    self:removeFromUIManager()
end

function getNums(inputString)
    local result = {}
    for word in string.gmatch(inputString, '([^,]+)') do
        table.insert(result, tonumber(word))
    end
    return result
end

function WAT_BasementMaker.MakeBasementWindow:prerender()
    ISPanel.prerender(self)

    if self.outPoint and self.inPoint and not self.goButton.enable then
        self.goButton:setEnable(true)
    end

    if self.stageDelay and self.stageDelay > 0 then
        self.stageDelay = self.stageDelay - 1
        return
    end

    if self.stage == "teleportToBasementPosition" then
        -- find next open position
        local startXY = getNums(SandboxVars.WastelandAdminTools.BasementsStartPoint)
        local spacingXY = getNums(SandboxVars.WastelandAdminTools.BasementsSpacing)
        local maxXY = getNums(SandboxVars.WastelandAdminTools.BasementsMaxSize)

        for y = 0, maxXY[2] do
            for x = 0, maxXY[1] do
                local key = x .. "," .. y
                if not WAT_BasementMaker.basementsData[key] then
                    local rx = startXY[1] + (x * spacingXY[1])
                    local ry = startXY[2] + (y * spacingXY[2])
                    self.basementKey = key
                    self.basementXY = {x = rx, y = ry}
                    WL_Utils.teleportPlayerToCoords(getPlayer(), rx - 1, ry, 0)
                    self.stage = "waitForTeleportFinish"
                    self.label:setText("Teleporting...")
                    return
                end
            end
        end
        self.stage = "failed"
        self.label:setText("Failed to find a basement position.")
    elseif self.stage == "waitForTeleportFinish" then
        local player = getPlayer()
        local square = getCell():getGridSquare(self.basementXY.x, self.basementXY.y, 0)
        if not square then return end
        self.stage = "waitOpenCopyPasteLoaded"
        WAT_CopyPaste:display()
        WAT_CopyPaste.instance.pointSelector:setValue({x = self.basementXY.x, y = self.basementXY.y, z = 0})
        self.label:setText("Loading Copy/Paste, Do Not Click.")
        self.stageDelay = 200
    elseif self.stage == "waitOpenCopyPasteLoaded" then
        if not WAT_CopyPaste.instance then
            self.stage = "failed"
            self.label:setText("Failed to load Copy/Paste")
            return
        end
        local copyPasteName = SandboxVars.WastelandAdminTools.BasementsCopyPasteName
        if WAT_CopyPaste.instance.savedData[copyPasteName] then
            WAT_CopyPaste.instance.comboLoad:select(copyPasteName)
            WAT_CopyPaste.instance:loadSave()
            WAT_CopyPaste.instance:onClear()
            self.stage = "waitCopyPasteClear"
            self.label:setText("Clearing Space")
            self.stageDelay = 20
        end
    elseif self.stage == "waitCopyPasteClear" then
        self.stage = "waitCopyPasteFinish"
        WAT_CopyPaste.instance:onPaste()
        self.label:setText("Pasting Basement")
        self.stageDelay = 20
    elseif self.stage == "waitCopyPasteFinish" then
        if not WAT_CopyPaste.instance.pendingAdds then
            local bounds = WAT_CopyPaste.instance.pasteArea.bounds
            local startX = bounds.x1
            local startY = bounds.y1
            local endX = bounds.x2
            local endY = bounds.y2

            if ISAddSafeZoneUI.instance then
                ISAddSafeZoneUI.instance:close()
            end
            local addSafeZoneUI = ISAddSafeZoneUI:new(getCore():getScreenWidth() / 2 - 210, getCore():getScreenHeight() / 2 - 200, 420, 400, getPlayer())
            addSafeZoneUI:initialise()
            addSafeZoneUI:addToUIManager()
            addSafeZoneUI.startingX = startX
            addSafeZoneUI.startingY = startY
            addSafeZoneUI.X1 = startX
            addSafeZoneUI.Y1 = startY
            addSafeZoneUI.X2 = endX
            addSafeZoneUI.Y2 = endY

            WL_Utils.teleportPlayerToCoords(getPlayer(), endX, endY, 0)

            WAT_CopyPaste.instance:close()
            local inXYZ = getNums(SandboxVars.WastelandAdminTools.BasementsInTpPoint)
            local outXYZ = getNums(SandboxVars.WastelandAdminTools.BasementsOutTpPoint)
            sendClientCommand(getPlayer(), 'WAT', 'finishBasement', {
                key = self.basementKey,
                outX1 = self.outPoint.x,
                outY1 = self.outPoint.y,
                outZ1 = self.outPoint.z,
                inX1 = self.basementXY.x + inXYZ[1],
                inY1 = self.basementXY.y + inXYZ[2],
                inZ1 = inXYZ[3],
                outX2 = self.basementXY.x + outXYZ[1],
                outY2 = self.basementXY.y + outXYZ[2],
                outZ2 = outXYZ[3],
                inX2 = self.inPoint.x,
                inY2 = self.inPoint.y,
                inZ2 = self.inPoint.z
            })
            self.label:setText("Done.")
            self.stage = "done"
        end
    end
end

local function doInstantTp(self, player)
    local sh = SafeHouse.getSafeHouse(player:getSquare())
    if sh and not sh:playerAllowed(player) and not WL_Utils.isStaff(player) then
        return
    end
    WL_Utils.teleportPlayerToCoords(player, self.tpTo.x, self.tpTo.y, self.tpTo.z)
end

function WAT_BasementMaker.updateTeleports()
    for k, v in pairs(WAT_BasementMaker.basementsData) do

        if WAT_BasementMaker.tps[k] then
            WL_TriggerZones.removeZone(WAT_BasementMaker.tps[k][1])
            WAT_BasementMaker.tps[k][1]:delete()
            WL_TriggerZones.removeZone(WAT_BasementMaker.tps[k][2])
            WAT_BasementMaker.tps[k][2]:delete()
            WAT_BasementMaker.tps[k] = nil
        end

        local zone1 = WL_Zone:new(v.outX1, v.outY1, v.outZ1, v.outX1, v.outY1, v.outZ1)
        zone1.mapDisabled = not getDebug()
        zone1.allowGods = true
        zone1.tpTo = {x = v.inX1, y = v.inY1, z = v.inZ1}
        zone1.onPlayerEnteredZone = doInstantTp
        local zone2 = WL_Zone:new(v.outX2, v.outY2, v.outZ2, v.outX2, v.outY2, v.outZ2)
        zone2.mapDisabled = not getDebug()
        zone1.allowGods = true
        zone2.tpTo = {x = v.inX2, y = v.inY2, z = v.inZ2}
        zone2.onPlayerEnteredZone = doInstantTp
        WL_TriggerZones.addZone(zone1)
        WL_TriggerZones.addZone(zone2)
        WAT_BasementMaker.tps[k] = {zone1, zone2}
    end
    for k, _ in pairs(WAT_BasementMaker.tps) do
        if not WAT_BasementMaker.basementsData[k] then
            for _, zone in pairs(WAT_BasementMaker.tps[k]) do
                WL_TriggerZones.removeZone(zone)
                zone:delete()
            end
            WAT_BasementMaker.tps[k] = nil
        end
    end
end

WAT_BasementMaker.EditBasementWindow = ISPanel:derive("WAT_BasementMaker_EditBasementWindow")
function WAT_BasementMaker.EditBasementWindow:new()
    local w = 200
    local h = 130
    local x = (getCore():getScreenWidth() / 2) - (w / 2)
    local y = (getCore():getScreenHeight() / 2) - (h / 2)
    local o = ISPanel:new(x, y, w, h)
    setmetatable(o, self)
    self.__index = self
    o:initialise()
    o:addToUIManager()
    o:setAlwaysOnTop(true)
    return o
end

function WAT_BasementMaker.EditBasementWindow:initialise()
    ISPanel.initialise(self)
    self.data = nil
    self.moveWithMouse = true
    self.backgroundColor = {r=0, g=0, b=0, a=0.8}

    local win = GravyUI.Node(self.width, self.height, self):pad(5)
    local label, selector, buttons1, buttons2 = win:rows(4, 10)
    local setToButton, setFromButton = buttons1:cols(2, 10)
    local removeButton, saveButton = buttons2:cols(2, 10)
    self.label = label:makeLabel("Edit Basement", UIFont.AutoNormMedium, {r=1, g=1, b=1, a=1}, "center")
    self.selector = selector:makeComboBox(self, self.selectBasement)
    self.removeButton = removeButton:makeButton("Remove", self, self.remove)
    self.setToButton = setToButton:makeButton("Set To", self, self.setTo)
    self.setFromButton = setFromButton:makeButton("Set From", self, self.setFrom)
    self.saveButton = saveButton:makeButton("Close", self, self.save)

    self.removeButton:setEnable(false)
    self.setFromButton:setEnable(false)
    self.setToButton:setEnable(false)

    local options = {}
    for k, d in pairs(WAT_BasementMaker.basementsData) do
        table.insert(options, {d.inX1 .. "," .. d.inY1, d})
    end
    table.sort(options, function(a, b) return a[1] < b[1] end)
    self.selector:addOption("")
    for _, d in ipairs(options) do
        self.selector:addOptionWithData(d[1], d[2])
    end
end

function WAT_BasementMaker.EditBasementWindow:selectBasement()
    local data = self.selector:getOptionData(self.selector.selected)
    if not data or type(data) ~= "table" then
        self.data = nil
        self.removeButton:setEnable(false)
        self.setFromButton:setEnable(false)
        self.setToButton:setEnable(false)
        self.saveButton:setTitle("Close")
        return
    end
    self.data = data
    self.removeButton:setEnable(true)
    self.setFromButton:setEnable(true)
    self.setToButton:setEnable(true)
    self.saveButton:setTitle("Save")
end

function WAT_BasementMaker.EditBasementWindow:remove()
    WAT_BasementMaker.basementsData[self.data.key] = nil
    WAT_BasementMaker.removeBasement(self.data.key)
    self.selector:clear()
    self.selector:addOption("")
    for k, d in pairs(WAT_BasementMaker.basementsData) do
        self.selector:addOptionWithData(d.inX1 .. "," .. d.inY1, k)
    end
    self.data = nil
    self.removeButton:setEnable(false)
    self.setFromButton:setEnable(false)
    self.setToButton:setEnable(false)
    self.saveButton:setEnable(false)
end

function WAT_BasementMaker.EditBasementWindow:setTo()
    local player = getPlayer()
    self.data.outX1 = math.floor(player:getX())
    self.data.outY1 = math.floor(player:getY())
    self.data.outZ1 = math.floor(player:getZ())
end

function WAT_BasementMaker.EditBasementWindow:setFrom()
    local player = getPlayer()
    self.data.inX2 = math.floor(player:getX())
    self.data.inY2 = math.floor(player:getY())
    self.data.inZ2 = math.floor(player:getZ())
end

function WAT_BasementMaker.EditBasementWindow:save()
    if not self.data then
        self:removeFromUIManager()
        return
    end
    sendClientCommand(getPlayer(), 'WAT', 'finishBasement', self.data)
    self.selector:select("")
    self:selectBasement()
end

if not WAT_BasementMaker.init then
    Events.OnInitGlobalModData.Add(function()
        ModData.request("WAT_Basements")
    end)

    Events.OnReceiveGlobalModData.Add(function (key, data)
        if key == "WAT_Basements" then
            WAT_BasementMaker.basementsData = data or {}
            WAT_BasementMaker.updateTeleports()
        end
    end)
    WAT_BasementMaker.init = true
end
