local function WAT_moveHordeToPosition(x, y, z, d)
    local sx = math.floor(x) - d
    local sy = math.floor(y) - d
    local ex = sx + d*2
    local ey = sy + d*2
    for cx = sx,ex do for cy = sy,ey do for cz = 0,7 do
        local square = getCell():getGridSquare(cx, cy, cz)
        if square then
            local movingEntities = square:getMovingObjects()
            for i=0,movingEntities:size()-1 do
                local movingEntity = movingEntities:get(i)
                if instanceof(movingEntity, "IsoZombie") then
                    movingEntity:pathToLocationF(x, y, z)
                end
            end
        end
    end end end
end

local BasementData = {}
local CopyPasteData = {}
local function WAT_FinishBasement(args)
    BasementData[args.key] = args
    ModData.add("WAT_Basements", BasementData)
    ModData.transmit("WAT_Basements")
end
local function WAT_RemoveBasement(args)
    BasementData[args.key] = nil
    ModData.add("WAT_Basements", BasementData)
    ModData.transmit("WAT_Basements")
end
local function WAT_AddCopyPaste(args)
    CopyPasteData[args.name] = args.data
    ModData.add("WAT_CopyPaste", CopyPasteData)
end
local function WAT_RemoveCopyPaste(args)
    CopyPasteData[args.name] = nil
    ModData.add("WAT_CopyPaste", CopyPasteData)
end
local function WAT_tileUp(x, y, z, i)
    local square = getCell():getGridSquare(x, y, z)
    if square then
        local arraylist = ArrayList:new()
        local objs = square:getObjects()
        for j=1, objs:size() do
            if j-1 == i-1 then
                arraylist:add(objs:get(j))
            elseif j-1 == i then
                arraylist:add(objs:get(j-2))
            else
                arraylist:add(objs:get(j-1))
            end
        end
        objs:clear()
        for j=1, arraylist:size() do
            objs:add(arraylist:get(j-1))
        end
    else
        print("WAT_tileUp: square is nil: " .. x .. ", " .. y .. ", " .. z)
    end
end

local function WAT_tileDown(x, y, z, i)
    local square = getCell():getGridSquare(x, y, z)
    if square then
        local arraylist = ArrayList:new()
        local objs = square:getObjects()
        for j=1, objs:size() do
            if j-1 == i then
                arraylist:add(objs:get(j))
            elseif j-1 ==i+1 then
                arraylist:add(objs:get(j-2))
            else
                arraylist:add(objs:get(j-1))
            end
        end
        objs:clear()
        for j=1, arraylist:size() do
            objs:add(arraylist:get(j-1))
        end
    else
        print("WAT_tileDown: square is nil: " .. x .. ", " .. y .. ", " .. z)
    end
end

Events.OnClientCommand.Add(function (module, command, player, args)
    if module ~= "WAT" then
        return
    end

    if command == "reboot" then
        PzWebStats.RequestReboot("", "medium")
    elseif command == "simpleRepair" then
        WAT_simpleRepair(args.vehicle)
    elseif command == "moveHordeToPosition" then
        WAT_moveHordeToPosition(args.x, args.y, args.z, args.d)
    elseif command == "finishBasement" then
        WAT_FinishBasement(args)
    elseif command == "removeBasement" then
        WAT_RemoveBasement(args)
    elseif command == "addCopyPaste" then
        WAT_AddCopyPaste(args)
    elseif command == "removeCopyPaste" then
        WAT_RemoveCopyPaste(args)
    elseif command == "tileUp" then
        WAT_tileUp(args.x, args.y, args.z, args.i)
    elseif command == "tileDown" then
        WAT_tileDown(args.x, args.y, args.z, args.i)
    elseif command == "clearCopyPasteSaves" then
        CopyPasteData = {}
        ModData.add("WAT_CopyPaste", CopyPasteData)
    end
end)

Events.OnInitGlobalModData.Add(function()
    BasementData = ModData.getOrCreate("WAT_Basements") or {}
    CopyPasteData = ModData.getOrCreate("WAT_CopyPaste") or {}
end)