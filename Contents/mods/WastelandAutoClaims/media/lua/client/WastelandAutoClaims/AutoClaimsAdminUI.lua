AutoClaimsAdminUI = ISCollapsableWindow:derive("AutoClaimsAdminUI")
AutoClaimsAdminUI.instance = nil

local PLAYERS_PER_PAGE = 14

function AutoClaimsAdminUI:teleportToVehicle(vehicleEntry)
    WL_Utils.teleportPlayerToCoords(getPlayer(), vehicleEntry.lastX, vehicleEntry.lastY, 0)
end

function AutoClaimsAdminUI:new()
    local scale = getTextManager():MeasureStringY(UIFont.Small, "XXX") / 12
    local width = scale * 400  -- Width for two column layout
    local height = scale * 300 -- Height to fit player list and details
    local x = getCore():getScreenWidth() / 2 - width / 2
    local y = getCore():getScreenHeight() / 2 - height / 2
    local o = ISCollapsableWindow:new(x, y, width, height)
    setmetatable(o, self)
    self.__index = self
    o.title = "Vehicle Claims Admin"
    o.scale = scale
    o.players = {}
    o.filteredPlayers = {}
    o.selectedPlayer = nil
    o.currentPage = 1
    o.lastSearchValue = ""
    o:initialise()
    return o
end

function AutoClaimsAdminUI:initialise()
    ISCollapsableWindow.initialise(self)
    self.moveWithMouse = true
    self:setResizable(false)

    -- Main layout using GravyUI
    local win = GravyUI.Node(self.width, self.height, self):pad(2 * self.scale, 15 * self.scale, 2 * self.scale, 2 * self.scale)
    local header, body = win:rows({20 * self.scale, 1.0}, 2 * self.scale)

    -- Search bar in header
    local searchLabel, searchInput = header:cols({0.3, 0.7}, 2 * self.scale)
    searchLabel:makeLabel("Search:", UIFont.Small, {r=1, g=1, b=1, a=1}, "left")
    -- Set up search box
    self.searchEntry = searchInput:makeTextBox("", false)

    -- Split body into player list and details
    local playerList, details = body:cols({0.35, 0.65}, 2 * self.scale)

    -- Player list section
    local listRows = {playerList:rows({
        1.0,           -- Player checkboxes
        15 * self.scale -- Pagination controls
    }, 2 * self.scale)}

    -- Player checkboxes area
    self.playerTickBoxes = listRows[1]:makeTickBox(self, self.onPlayerSelected)
    self.playerTickBoxes.onlyOnePossibility = true

    -- Pagination controls
    local prevBtn, pageLabel, nextBtn = listRows[2]:cols({0.2, 0.6, 0.2}, 5 * self.scale)
    self.prevButton = prevBtn:makeButton("<", self, self.onPrevPage)
    self.pageLabel = pageLabel:makeLabel("Page 1", UIFont.Small, {r=1, g=1, b=1, a=1}, "center")
    self.nextButton = nextBtn:makeButton(">", self, self.onNextPage)

    -- Details section
    local detailsRows = {details:rows({
        15 * self.scale,  -- Player name
        15 * self.scale,  -- Claims allowed
        15 * self.scale,  -- Timeout info
        1.0              -- Vehicle list
    }, 2 * self.scale)}

    -- Player name
    self.playerNameLabel = detailsRows[1]:pad(0.15, 0):makeLabel("Select Player", UIFont.Medium, {r=1, g=1, b=1, a=1}, "left")

    -- Claims controls
    local claimsLabel, claimsControls = detailsRows[2]:pad(0.15, 0):cols({0.4, 0.6}, 2 * self.scale)
    claimsLabel:makeLabel("Claims Allowed:", UIFont.Small, {r=1, g=1, b=1, a=1}, "left")
    local minusBtn, claimsText, plusBtn, resetBtn = claimsControls:cols({0.15, 0.5, 0.15, 0.2}, 2 * self.scale)
    self.minusButton = minusBtn:makeButton("-", self, self.onDecreaseClaims)
    self.claimsLabel = claimsText:makeLabel("0", UIFont.Small, {r=1, g=1, b=1, a=1}, "center")
    self.plusButton = plusBtn:makeButton("+", self, self.onIncreaseClaims)
    self.resetButton = resetBtn:makeButton("Reset", self, self.onResetClaims)

    -- Timeout button
    self.resetTimeoutBtn = detailsRows[3]:pad(0.15, 0):makeButton("Reset Timeout", self, self.onResetTimeout)

    -- Vehicle list with TP buttons
    self.vehicleList = detailsRows[4]:makeScrollingListBox(UIFont.Small)
    self.vehicleList.itemheight = 22 * self.scale
    local parentUI = self
    self.vehicleList.doDrawItem = function(list, y, item, alt)
        if not item or not item.item then return y end
        
        local vehicleEntry = item.item
        
        -- Draw box around the entire row
        if list:isMouseOver() and list:getMouseY() >= y and list:getMouseY() <= y + list.itemheight then
            list:drawRect(0, y, list:getWidth(), list.itemheight + 1, 0.3, 0.7, 0.35, 0.15)
        else
            list:drawRect(0, y, list:getWidth(), list.itemheight + 1, 0.1, 0.1, 0.1, 1.0)
        end
        list:drawRectBorder(0, y, list:getWidth(), list.itemheight + 1, 0.5, 1, 1, 1)
        
        -- Draw vehicle name (left-aligned)
        list:drawText(getText("IGUI_VehicleName" .. vehicleEntry.type), 4, y + (list.itemheight - 14 * self.scale) / 2, 1, 1, 1, 1, UIFont.Small)
        
        -- Draw TP button (right-aligned, full height)
        local buttonText = vehicleEntry.lastX .. "," .. vehicleEntry.lastY
        local buttonWidth = list:getWidth() * 0.3
        if list:isMouseOver() and list:getMouseY() >= y and list:getMouseY() <= y + list.itemheight then
            if list:getMouseX() >= list:getWidth() - buttonWidth and list:getMouseX() <= list:getWidth() then
                list:drawRect(list:getWidth() - buttonWidth, y, buttonWidth, list.itemheight, 0.3, 0.7, 0.35, 0.15)
                if list.mouseDown then
                    parentUI:teleportToVehicle(vehicleEntry)
                end
            end
        end
        list:drawRectBorder(list:getWidth() - buttonWidth, y, buttonWidth, list.itemheight + 1, 0.5, 1, 1, 1)
        list:drawTextCentre(buttonText, list:getWidth() - buttonWidth / 2, y + (list.itemheight - 14 * self.scale) / 2, 1, 1, 1, 1, UIFont.Small)
        
        return y + list.itemheight
    end
    self.vehicleList.onMouseDown = function(list, x, y)
        list.mouseDown = true
        return true
    end
    self.vehicleList.onMouseUp = function(list, x, y)
        list.selected = math.floor(y / list.itemheight) + 1
        list.mouseDown = false
        return true
    end
    self.vehicleList.onMouseDoubleClick = function(list)
        local selected = list.items[self.vehicleList.selected]
        if selected and selected.item then
            local ui = ClaimedVehicleUI:display(selected.item)
            ui:setAlwaysOnTop(true)
        end
    end

    -- Set up tooltips
    self.searchEntry:setTooltip("Type to filter players")
    self.minusButton:setTooltip("Decrease claims adjustment (-1)")
    self.plusButton:setTooltip("Increase claims adjustment (+1)")
    self.resetButton:setTooltip("Reset adjustment to 0")
    self.resetTimeoutBtn:setTooltip("Reset player's timeout")

    -- Set up tooltips
    self.searchEntry:setTooltip("Type to filter players")

    self:refreshPlayers()
    self:updateButtons()
end

function AutoClaimsAdminUI:refreshPlayers()
    self.players = {}
    for username, playerData in pairs(WastelandAutoClaimsCore.publicData.players) do
        table.insert(self.players, {username = username, data = playerData})
    end
    table.sort(self.players, function(a, b) return a.username < b.username end)
    self:applyFilter()
end

function AutoClaimsAdminUI:applyFilter()
    local search = string.lower(self.searchEntry:getText())
    self.filteredPlayers = {}
    for _, player in ipairs(self.players) do
        if search == "" or string.find(string.lower(player.username), search) then
            table.insert(self.filteredPlayers, player)
        end
    end
    self:updatePlayerList()
end

function AutoClaimsAdminUI:updatePlayerList()
    self.playerTickBoxes:clearOptions()
    local startIdx = (self.currentPage - 1) * PLAYERS_PER_PAGE + 1
    for i = startIdx, math.min(startIdx + PLAYERS_PER_PAGE - 1, #self.filteredPlayers) do
        local player = self.filteredPlayers[i]
        self.playerTickBoxes:addOption(player.username)
    end
    self:updateButtons()
    
    -- Auto-select first player if available and none is currently selected
    if #self.filteredPlayers > 0 and not self.selectedPlayer then
        self:onPlayerSelected(1, true)
    end
end

function AutoClaimsAdminUI:updateButtons()
    self.prevButton:setEnabled(self.currentPage > 1)
    self.nextButton:setEnabled(self.currentPage < math.ceil(#self.filteredPlayers / PLAYERS_PER_PAGE))
    self.pageLabel:setText(string.format("Page %d/%d", self.currentPage, math.max(1, math.ceil(#self.filteredPlayers / PLAYERS_PER_PAGE))))
end

function AutoClaimsAdminUI:updateSelectedPlayer()
    if not self.selectedPlayer then
        self.playerNameLabel:setText("Select a player")
        self.claimsLabel:setText("0")
        self.vehicleList:clear()
        self.minusButton:setEnabled(false)
        self.plusButton:setEnabled(false)
        self.resetTimeoutBtn:setEnabled(false)
        self.resetTimeoutBtn:setTitle("Reset Timeout")
        return
    end

    local playerData = self.selectedPlayer.data
    self.playerNameLabel:setText(self.selectedPlayer.username)
    local defaultClaims = SandboxVars.WastelandAutoClaims.DefaultClaims
    local adjustment = playerData.claimsAdjustment
    local total = self.selectedPlayer and WastelandAutoClaimsCore:getEffectiveClaims(playerData) or 0
    local sign = adjustment >= 0 and "+" or ""
    local claimsText = defaultClaims .. " " .. sign .. adjustment .. " = " .. total
    self.claimsLabel:setText(claimsText)
    
    local now = WL_Utils.getTimestampMs()
    local timeAgo = WL_Utils.toHumanReadableTime(now - playerData.lastOnlineTime * 1000)
    self.resetTimeoutBtn:setTitle("Last Online: " .. timeAgo)

    self.minusButton:setEnabled(true)
    self.plusButton:setEnabled(true)
    self.resetTimeoutBtn:setEnabled(true)

    self.vehicleList:clear()
    if #playerData.vehicles == 0 then
        self.vehicleList:addItem("No claimed vehicles", nil)
    else
        for _, vehicleId in ipairs(playerData.vehicles) do
            local vehicleEntry = WastelandAutoClaimsCore:getVehicleEntry(vehicleId)
            if vehicleEntry then
                self.vehicleList:addItem(getText("IGUI_VehicleName" .. vehicleEntry.type), vehicleEntry)
            end
        end
    end
end

function AutoClaimsAdminUI:onPrevPage()
    if self.currentPage > 1 then
        self.currentPage = self.currentPage - 1
        self:updatePlayerList()
    end
end

function AutoClaimsAdminUI:onNextPage()
    local maxPage = math.ceil(#self.filteredPlayers / PLAYERS_PER_PAGE)
    if self.currentPage < maxPage then
        self.currentPage = self.currentPage + 1
        self:updatePlayerList()
    end
end

function AutoClaimsAdminUI:onPlayerSelected(index, selected)
    if selected then
        local playerIndex = (self.currentPage - 1) * PLAYERS_PER_PAGE + index
        self.selectedPlayer = self.filteredPlayers[playerIndex]
        self:updateSelectedPlayer()
    end
end

function AutoClaimsAdminUI:onIncreaseClaims()
    if not self.selectedPlayer then return end
    WastelandAutoClaimsCore:setClaimsAdjustment(
        getPlayer(),
        self.selectedPlayer.username,
        self.selectedPlayer.data.claimsAdjustment + 1
    )
    self.selectedPlayer.data = WastelandAutoClaimsCore:getPlayerEntry(self.selectedPlayer.username)
    self.filteredPlayers[self.selectedPlayer.username] = self.selectedPlayer.data
    self:updateSelectedPlayer()
end

function AutoClaimsAdminUI:onDecreaseClaims()
    if not self.selectedPlayer then return end
    WastelandAutoClaimsCore:setClaimsAdjustment(
        getPlayer(),
        self.selectedPlayer.username,
        self.selectedPlayer.data.claimsAdjustment - 1
    )
    self.selectedPlayer.data = WastelandAutoClaimsCore:getPlayerEntry(self.selectedPlayer.username)
    self.filteredPlayers[self.selectedPlayer.username] = self.selectedPlayer.data
    self:updateSelectedPlayer()
end

function AutoClaimsAdminUI:onResetClaims()
    if not self.selectedPlayer then return end
    WastelandAutoClaimsCore:setClaimsAdjustment(
        getPlayer(),
        self.selectedPlayer.username,
        0  -- Reset adjustment to 0
    )
    self.selectedPlayer.data = WastelandAutoClaimsCore:getPlayerEntry(self.selectedPlayer.username)
    self.filteredPlayers[self.selectedPlayer.username] = self.selectedPlayer.data
    self:updateSelectedPlayer()
end

function AutoClaimsAdminUI:onResetTimeout()
    if not self.selectedPlayer then return end
    WastelandAutoClaimsCore:resetUserTime(getPlayer(), self.selectedPlayer.username)
    self.selectedPlayer.data = WastelandAutoClaimsCore:getPlayerEntry(self.selectedPlayer.username)
    self.filteredPlayers[self.selectedPlayer.username] = self.selectedPlayer.data
    self:updateSelectedPlayer()
end

function AutoClaimsAdminUI:prerender()
    ISCollapsableWindow.prerender(self)
    
    -- Check if search value has changed
    local currentSearchValue = self.searchEntry:getText()
    if currentSearchValue ~= self.lastSearchValue then
        self.lastSearchValue = currentSearchValue
        self.currentPage = 1
        self:applyFilter()
    end
    
    -- Update timeout button text every second
    if not self.lastUpdate or self.lastUpdate + 1000 < getTimestampMs() then
        if self.selectedPlayer then
            local now = WL_Utils.getTimestampMs()
            local timeAgo = WL_Utils.toHumanReadableTime(now - self.selectedPlayer.data.lastOnlineTime * 1000)
            self.resetTimeoutBtn:setTitle("Last Online: " .. timeAgo)
        end
        self.lastUpdate = getTimestampMs()
    end
end

function AutoClaimsAdminUI:close()
    ISCollapsableWindow.close(self)
    self:removeFromUIManager()
    AutoClaimsAdminUI.instance = nil
end

function AutoClaimsAdminUI:display()
    if AutoClaimsAdminUI.instance then
        AutoClaimsAdminUI.instance:close()
    end
    local ui = AutoClaimsAdminUI:new()
    ui:addToUIManager()
    AutoClaimsAdminUI.instance = ui
    return ui
end