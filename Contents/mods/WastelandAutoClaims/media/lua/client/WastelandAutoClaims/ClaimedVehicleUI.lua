ClaimedVehicleUI = ISCollapsableWindow:derive("ClaimedVehicleUI")
ClaimedVehicleUI.instance = nil

function ClaimedVehicleUI:display(vehicleEntry)
    if ClaimedVehicleUI.instance then
        ClaimedVehicleUI.instance:close()
    end
    local player = getPlayer()
    local scale = getTextManager():MeasureStringY(UIFont.Small, "XXX") / 12
    local width = scale * 350  -- Width for single column layout
    local height = scale * 205 -- Height to fit all rows exactly
    if WL_Utils.isStaff(player) then
        height = height + (25 * scale) -- Extra height for staff buttons
    end
    local x = getCore():getScreenWidth() / 2 - width / 2
    local y = getCore():getScreenHeight() / 2 - height / 2
    local o = ISCollapsableWindow:new(x, y, width, height)
    setmetatable(o, self)
    self.__index = self
    o.title = "Vehicle Claim"
    o.scale = scale
    o.vehicleEntry = vehicleEntry
    o:initialise()
    o:addToUIManager()
    ClaimedVehicleUI.instance = o
    return o
end

function ClaimedVehicleUI:initialise()
    ISCollapsableWindow.initialise(self)

    self.moveWithMouse = true
    self:setResizable(false)

    local win = GravyUI.Node(self.width, self.height, self):pad(5 * self.scale, 20 * self.scale, 5 * self.scale, 5 * self.scale)
    local header, body = win:rows({20 * self.scale, 1.0}, 5 * self.scale)

    -- Header with vehicle name and coords
    local headerLeft, headerRight = header:cols({0.7, 0.3}, 2 * self.scale)
    self.header = headerLeft:makeLabel(getText("IGUI_VehicleName" .. self.vehicleEntry.type), UIFont.Large, {r=1, g=1, b=1, a=1}, "left")
    headerRight:makeLabel("(" .. self.vehicleEntry.lastX .. ", " .. self.vehicleEntry.lastY .. ")", UIFont.Medium, {r=1, g=1, b=1, a=1}, "right")

    -- Main content sections
    local player = getPlayer()
    local rowHeights = {
        20 * self.scale,  -- Selection row
        110 * self.scale, -- Permissions section
        20 * self.scale   -- Bottom buttons
    }
    if WL_Utils.isStaff(player) then
        table.insert(rowHeights, 20 * self.scale) -- Staff buttons
    end
    local sections = {body:rows(rowHeights, 2 * self.scale)}

    -- Selection row with dropdown and buttons
    local selectionRow = {sections[1]:cols({0.5, 0.25, 0.25}, 2 * self.scale)}
    self.targetCombo = selectionRow[1]:makeComboBox(self, self.onTargetSelected)
    self.targetCombo:addOption("Public")
    
    self.addUserBtn = selectionRow[2]:makeButton("Add User", self, self.onAddUser)
    self.removeUserBtn = selectionRow[3]:makeButton("Remove User", self, self.onRemoveUser)
    self.removeUserBtn:setEnabled(false)
    
    -- Permissions section with toggles and reset
    local permArea = {sections[2]:cols({0.7, 0.3}, 2 * self.scale)}
    
    -- Permission toggles on left
    self.permToggles = permArea[1]:makeTickBox()
    self:addPermissionToggles(self.permToggles)
    
    -- Reset controls on right
    local resetArea = {permArea[2]:pad(0, 0.3):rows({0.5, 0.5}, 2 * self.scale)}
    self.timeoutStatus = resetArea[1]:makeLabel("", UIFont.Small, {r=1, g=1, b=1, a=1}, "left")
    self.resetCombo = resetArea[2]:makeComboBox(self, self.onResetTimeout)
    self:populateResetCombo(self.resetCombo)

    self:refreshTargetList()
    
    -- Bottom buttons
    local bottomButtons = {sections[3]:cols({0.5, 0.5}, 2 * self.scale)}
    self.transferBtn = bottomButtons[1]:makeButton("Transfer to...", self, self.onTransfer)
    self.unclaimBtn = bottomButtons[2]:makeButton("Unclaim Vehicle", self, self.onUnclaim)
    
    -- Staff buttons
    local player = getPlayer()
    if WL_Utils.isStaff(player) and sections[4] then
        local staffButtons = {sections[4]:cols({0.25, 0.25, 0.25, 0.25}, 2 * self.scale)}
        staffButtons[1]:makeButton("TP", self, self.onTeleportToVehicle)
        staffButtons[2]:makeButton("Angles", self, self.onVehicleAngles)
        staffButtons[3]:makeButton("Colors", self, self.onVehicleColors)
        staffButtons[4]:makeButton("Drive", self, self.onDrive)
    end
end

local function findVehicle(vehicleId)
    local vehicles = getCell():getVehicles()
    for i=1, vehicles:size() do
        local vehicle = vehicles:get(i-1)
        print(vehicle:getScript():getName())
        if vehicle:getModData().WAC_ID == vehicleId then
            return vehicle
        end
    end
    return nil
end

function ClaimedVehicleUI:onTeleportToVehicle()
    WL_Utils.teleportPlayerToCoords(getPlayer(), self.vehicleEntry.lastX, self.vehicleEntry.lastY, 0)
end

function ClaimedVehicleUI:onVehicleAngles()
    local vehicle = findVehicle(self.vehicleEntry.id)
    if not vehicle then
        getPlayer():Say("Vehicle not found")
        return
    end
    debugVehicleAngles(vehicle)
end

function ClaimedVehicleUI:onVehicleColors()
    local vehicle = findVehicle(self.vehicleEntry.id)
    if not vehicle then
        getPlayer():Say("Vehicle not found")
        return
    end
    local player = getPlayer()
    debugVehicleColor(player, vehicle)
end

function ClaimedVehicleUI:onDrive()
    local player = getPlayer()
    local vehicle = findVehicle(self.vehicleEntry.id)
    if not vehicle then
        player:Say("Vehicle not found")
        return
    end
    if player:getVehicle() then
        player:Say("You are already in a vehicle")
        return
    end
    if not vehicle:isSeatInstalled(0) or not vehicle:isSeatInstalled(0) or vehicle:isSeatOccupied(0) then
        player:Say("Driver seat missing or occupied")
        return
    end
    vehicle:enter(0, player)
    vehicle:setCharacterPosition(player, 0, "inside")
	vehicle:transmitCharacterPosition(0, "inside")
	vehicle:playPassengerAnim(0, "idle")
	triggerEvent("OnEnterVehicle", player)
end

function ClaimedVehicleUI:addPermissionToggles(toggles)
    toggles:addOption("Drive")
    toggles:addOption("Passenger")
    toggles:addOption("Access Inventory")
    toggles:addOption("Attach")
    toggles:addOption("Fuel")
    toggles:addOption("Mechanics")
end

function ClaimedVehicleUI:refreshTargetList()
    self.targetCombo:clear()
    self.targetCombo:addOption("Public")
    for username, _ in pairs(self.vehicleEntry.userPermissions) do
        self.targetCombo:addOption(username)
    end
    self:setPermissions("Public")
    self:updateTimeoutStatus()
end

function ClaimedVehicleUI:setPermissions(target)
    local permissions = target == "Public" and self.vehicleEntry.publicPermissions or self.vehicleEntry.userPermissions[target]
    if not permissions then return end
    
    self.permToggles:setSelected(1, permissions.drive)
    self.permToggles:setSelected(2, permissions.passenger)
    self.permToggles:setSelected(3, permissions.accessInventory)
    self.permToggles:setSelected(4, permissions.attach)
    self.permToggles:setSelected(5, permissions.fuel)
    self.permToggles:setSelected(6, permissions.mechanics)
end

function ClaimedVehicleUI:updateTimeoutStatus()
    local target = self.targetCombo:getSelectedText()
    if not target then return end
    
    local permissions = target == "Public" and self.vehicleEntry.publicPermissions or self.vehicleEntry.userPermissions[target]
    if not permissions then
        self.timeoutStatus:setText("")
        return
    end
    
    if permissions.reset then
        local timeLeft = (permissions.reset - WL_Utils.getTimestamp()) * 1000
        self.timeoutStatus:setText("Timeout: " .. WL_Utils.toHumanReadableTime(timeLeft))
    else
        self.timeoutStatus:setText("No timeout set")
    end
end

function ClaimedVehicleUI:onTargetSelected()
    local target = self.targetCombo:getSelectedText()
    if not target then return end
    
    -- Enable/disable user-specific controls
    self.removeUserBtn:setEnabled(target ~= "Public")
    
    -- Update permissions display
    self:setPermissions(target)
    self:updateTimeoutStatus()
end

function ClaimedVehicleUI:onAddUser()
    local s = self
    local excluded = {self.vehicleEntry.owner}
    for username, _ in pairs(self.vehicleEntry.userPermissions) do
        table.insert(excluded, username)
    end
    WL_SelectPlayersPanel:show(self, function (_, username)
        WastelandAutoClaimsCore:addVehiclePermission(getPlayer(), self.vehicleEntry.id, username)
        s.vehicleEntry = WastelandAutoClaimsCore:getVehicleEntry(self.vehicleEntry.id)
        s:refreshTargetList()
        s.targetCombo:select(username) -- Reset to Public
        s:onTargetSelected() -- Call onTargetSelected to properly update UI state
    end, {
        includeSelf = true,
        onlyInLOS = false,
        exclude = excluded,
        allowManual = true,
        staffOverride = false,
    })
end

function ClaimedVehicleUI:onRemoveUser()
    local username = self.targetCombo:getSelectedText()
    if username == "Public" then return end
    
    WastelandAutoClaimsCore:removeVehiclePermission(getPlayer(), self.vehicleEntry.id, username)
    self.vehicleEntry = WastelandAutoClaimsCore:getVehicleEntry(self.vehicleEntry.id)
    self:refreshTargetList()
    self.targetCombo:select("Public") -- Reset to Public
    self:onTargetSelected() -- Call onTargetSelected to properly update UI state
end

function ClaimedVehicleUI:onTransfer()
    local s = self
    WL_SelectPlayersPanel:show(self, function (_, username)
        WastelandAutoClaimsCore:setNewOwner(getPlayer(), self.vehicleEntry.id, username)
        s:close()
    end, {
        includeSelf = true,
        onlyInLOS = false,
        exclude = {self.vehicleEntry.owner},
        allowManual = true,
        staffOverride = false,
    })
end

function ClaimedVehicleUI:onUnclaim()
    local modal = ISModalDialog:new(0, 0, 350, 150, "Unclaim Vehicle?", true, nil, function(target, button)
        if button.internal == "YES" then
            WastelandAutoClaimsCore:unclaimVehicle(getPlayer(), self.vehicleEntry.id)
            self:close()
        end
    end)
    modal:initialise()
    modal:addToUIManager()
end

function ClaimedVehicleUI:populateResetCombo(combo)
    combo:clear()
    combo:addOption("Select duration...")
    combo:addOption("No timeout")
    combo:addOption("15 Minutes")
    combo:addOption("30 Minutes")
    combo:addOption("1 Hour")
    combo:addOption("3 Hours")
    combo:addOption("6 Hours")
    combo:addOption("12 Hours")
    combo:addOption("1 Day")
    combo:addOption("3 Days")
    combo:addOption("1 Week")
end

local function getDuration(selected)
    if selected == "Select duration..." then return nil end
    
    local durations = {
        ["No timeout"] = -1,
        ["15 Minutes"] = 900,
        ["30 Minutes"] = 1800,
        ["1 Hour"] = 3600,
        ["3 Hours"] = 10800,
        ["6 Hours"] = 21600,
        ["12 Hours"] = 43200,
        ["1 Day"] = 86400,
        ["3 Days"] = 259200,
        ["1 Week"] = 604800
    }
    return durations[selected]
end

function ClaimedVehicleUI:onResetTimeout()
    local target = self.targetCombo:getSelectedText()
    if not target then return end
    
    local selected = self.resetCombo:getSelectedText()
    local duration = getDuration(selected)
    if duration == nil then return end
    if target == "Public" then target = nil end
    if duration > 0 then duration = WL_Utils.getTimestamp() + duration
    else duration = nil end
    WastelandAutoClaimsCore:setVehiclePermission(
        getPlayer(),
        self.vehicleEntry.id,
        target,
        "reset",
        duration
    )
    self.vehicleEntry = WastelandAutoClaimsCore:getVehicleEntry(self.vehicleEntry.id)
    self.resetCombo:select("Select duration...")
end

function ClaimedVehicleUI:close()
    ISCollapsableWindow.close(self)
    self:removeFromUIManager()
end

function ClaimedVehicleUI:prerender()
    ISCollapsableWindow.prerender(self)
    local player = getPlayer()

    -- Update timeout status every second
    if not self.lastStatusUpdate or self.lastStatusUpdate + 1000 < getTimestampMs() then
        self:updateTimeoutStatus()
        self.lastStatusUpdate = getTimestampMs()
    end

    -- Update permission states based on toggles
    if self.permToggles then
        local target = self.targetCombo:getSelectedText()
        if target then
            local permissions = {
                drive = self.permToggles:isSelected(1),
                passenger = self.permToggles:isSelected(2),
                accessInventory = self.permToggles:isSelected(3),
                attach = self.permToggles:isSelected(4),
                fuel = self.permToggles:isSelected(5),
                mechanics = self.permToggles:isSelected(6)
            }
            local wasChange = false
            local currentPermissions = target == "Public" and self.vehicleEntry.publicPermissions or self.vehicleEntry.userPermissions[target]
            if target == "Public" then target = nil end
            if currentPermissions then
                for perm, value in pairs(permissions) do
                    if value ~= currentPermissions[perm] then
                        WastelandAutoClaimsCore:setVehiclePermission(
                            player,
                            self.vehicleEntry.id,
                            target,
                            perm,
                            value
                        )
                        wasChange = true
                    end
                end
            end
            if wasChange then
                self.vehicleEntry = WastelandAutoClaimsCore:getVehicleEntry(self.vehicleEntry.id)
            end
        end
    end
end