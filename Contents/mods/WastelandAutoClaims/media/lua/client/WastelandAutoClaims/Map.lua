require "WL_Utils"

-- create the textures and add the button to the map
ISWorldMap.WastelandAutoClaims_original_createChildren = ISWorldMap.WastelandAutoClaims_original_createChildren or ISWorldMap.createChildren;
function ISWorldMap:createChildren()
    ISWorldMap.WastelandAutoClaims_original_createChildren(self)

	local btnSize = self.texViewIsometric and self.texViewIsometric:getWidth() or 48
    local buttons = self.buttonPanel.joypadButtons

    for _, btn in ipairs(buttons) do
        btn:setX(btn.x + btnSize + 20)
    end

    self.onShowVehicleClaims = ISButton:new(buttons[1].x - 20 - btnSize, 0, btnSize, btnSize, "VC", self, ISWorldMap.onShowVehicleClaimsClick)
    self.onShowVehicleClaims:setVisible(true)

    table.insert(buttons, 1, self.onShowVehicleClaims)
    self.buttonPanel:addChild(self.onShowVehicleClaims)
	self.buttonPanel:insertNewListOfButtons(buttons)

    local btnCount = #buttons
    self.buttonPanel:setX(self.width - 20 - (btnSize * btnCount + 20 * (btnCount - 1)))
    self.buttonPanel:setWidth(btnSize * btnCount + 20 * (btnCount - 1))

    self.showVehicleClaims = false
end

function ISWorldMap:onShowVehicleClaimsClick()
    self.showVehicleClaims = not self.showVehicleClaims
end

ISWorldMap.WastelandVehicleClaims_original_render = ISWorldMap.WastelandVehicleClaims_original_render or ISWorldMap.render;
function ISWorldMap:render()
    ISWorldMap.WastelandVehicleClaims_original_render(self)

    if self.onShowVehicleClaims:isVisible() and self.isometric then
        self.showVehicleClaims = false
        self.onShowVehicleClaims:setVisible(false)
    elseif not self.onShowVehicleClaims:isVisible() and not self.isometric then
        self.onShowVehicleClaims:setVisible(true)
    end

    if not self.showVehicleClaims or self.isometric then return end

    local myEntry = WastelandAutoClaimsCore:getPlayerEntry(getPlayer():getUsername())
    if not myEntry then return end

    for _,vehicle in ipairs(myEntry.vehicles) do
        local vehicleEntry = WastelandAutoClaimsCore:getVehicleEntry(vehicle)
        if vehicleEntry then
            local x = vehicleEntry.lastX
            local y = vehicleEntry.lastY

            local tlX = self.mapAPI:worldToUIX(x, y)
            local tlY = self.mapAPI:worldToUIY(x, y)
            local brX = tlX + 5
            local brY = tlY + 5

            self:drawRect(tlX, tlY, brX - tlX, brY - tlY, 1, 0.3, 1.0, 0.3)
            self:drawTextCentre(getText("IGUI_VehicleName" .. vehicleEntry.type), tlX + 2, tlY + 2, 0, 0, 0, 1, UIFont.Small)
        end
    end

    if WL_Utils.isStaff(getPlayer()) then
        for _, vehicleEntry in pairs(WastelandAutoClaimsCore.publicData.vehicles) do
            if vehicleEntry.owner and vehicleEntry.owner ~= getPlayer():getUsername() then
                local x = vehicleEntry.lastX
                local y = vehicleEntry.lastY

                local tlX = self.mapAPI:worldToUIX(x, y)
                local tlY = self.mapAPI:worldToUIY(x, y)
                local brX = tlX + 5
                local brY = tlY + 5

                if tlX < self.width and tlY < self.height and brX > 0 and brY > 0 then
                    self:drawRect(tlX, tlY, brX - tlX, brY - tlY, 1, 1.0, 0.3, 0.3)
                    if self.mapAPI:getZoomF() > 18 then
                        self:drawTextCentre(getText("IGUI_VehicleName" .. vehicleEntry.type) .. " " .. vehicleEntry.owner, tlX + 2, tlY + 2, 0, 0, 0, 1, UIFont.Small)
                    end
                end
            end
        end
    end
end
