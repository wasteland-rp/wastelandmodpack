-- Helper function to add permission request options to a menu
local function addRequestPermissionOptions(menu, player, vehicleEntry)
    if not menu or not player or not vehicleEntry then return end

    local hasAllPermissions = true
    for key, name in pairs(WastelandAutoClaimsCore.VEHICLE_PERMISSIONS) do
        if not WastelandAutoClaimsCore:playerCan(player, vehicleEntry.id, key) then
            hasAllPermissions = false
            break
        end
    end

    if hasAllPermissions then
        menu:addOption("All Permissions Granted")
        return
    else
        menu:addOption("Request All Permissions",
            WastelandAutoClaimsCore,
            WastelandAutoClaimsCore.requestPermission,
            player,
            vehicleEntry.id
        )
    end

    for key, name in pairs(WastelandAutoClaimsCore.VEHICLE_PERMISSIONS) do
        if not WastelandAutoClaimsCore:playerCan(player, vehicleEntry.id, key) then
            menu:addOption(
                "Request " .. name .. " Permission",
                WastelandAutoClaimsCore,
                WastelandAutoClaimsCore.requestPermission,
                player,
                vehicleEntry.id,
                key
            )
        end
    end
end

local function getCharacterName(username)
    if WRC and WRC.Meta and WRC.Meta.GetName then
        return WRC.Meta.GetName(username)
    end
    return username
end

-- Helper function to format vehicle location text
local function formatVehicleLocationText(vehicleEntry)
    return getText("IGUI_VehicleName" .. vehicleEntry.type) ..
           " (" .. vehicleEntry.lastX .. ", " .. vehicleEntry.lastY .. ")"
end

-- Handle menu options for a vehicle owned by the player
local function handleOwnedVehicle(context, vehicleEntry)
    context:addOption(formatVehicleLocationText(vehicleEntry),
                     ClaimedVehicleUI,
                     ClaimedVehicleUI.display,
                     vehicleEntry)
end

-- Handle menu options for a vehicle owned by another player
local function handleOtherVehicle(context, player, vehicleEntry)
    if WL_Utils.canModerate(player) then
        context:addOption("Claimed by " .. getCharacterName(vehicleEntry.owner),
                         ClaimedVehicleUI,
                         ClaimedVehicleUI.display,
                         vehicleEntry)
        return
    end

    local claimedOtherMenu = WL_ContextMenuUtils.getOrCreateSubMenu(
        context,
        "Claimed by " .. getCharacterName(vehicleEntry.owner)
    )
    addRequestPermissionOptions(claimedOtherMenu, player, vehicleEntry)
end

-- Handle menu options for an unclaimed vehicle
local function handleUnclaimedVehicle(context, player, vehicle, myEntry)
    local item
    local effectiveClaims = WastelandAutoClaimsCore:getEffectiveClaims(myEntry)
    local myClaims = #myEntry.vehicles
    if effectiveClaims > myClaims or WL_Utils.isStaff(player) then
        item = context:addOption("Claim (" .. myClaims .. "/".. effectiveClaims .. ")",
            WastelandAutoClaimsCore,
            WastelandAutoClaimsCore.claimVehicle,
            player,
            vehicle
        )
    else
        item = context:addOption("Claim (" .. myClaims .. "/".. effectiveClaims .. ")")
        item.notAvailable = true
    end
end

-- Handle menu options for player's claimed vehicles list
local function handleClaimedVehiclesList(context, myEntry)
    if #myEntry.vehicles == 0 then return end

    local effectiveClaims = WastelandAutoClaimsCore:getEffectiveClaims(myEntry)
    local myClaims = #myEntry.vehicles
    local menu = WL_ContextMenuUtils.getOrCreateSubMenu(context, "Vehicle Claims (" .. myClaims .. "/" .. effectiveClaims .. ")")
    for _, vehicleId in ipairs(myEntry.vehicles) do
        local vehicleEntry = WastelandAutoClaimsCore:getVehicleEntry(vehicleId)
        if vehicleEntry then
            menu:addOption(formatVehicleLocationText(vehicleEntry),
                          ClaimedVehicleUI,
                          ClaimedVehicleUI.display,
                          vehicleEntry)
        else
            local item = menu:addOption("Unknown Vehicle")
            item.notAvailable = true
        end
    end
end

local function showWorldMenu(playerNum, context, worldobjects, test)
    local player = getSpecificPlayer(playerNum)
    local vehicle = player:getVehicle()
    if not vehicle then
        vehicle = IsoObjectPicker.Instance:PickVehicle(getMouseXScaled(), getMouseYScaled())
    end

    local myEntry = WastelandAutoClaimsCore:getPlayerEntry(player:getUsername())

    -- Handle vehicle-specific menu options
    if vehicle then
        local vehicleEntry = WastelandAutoClaimsCore:getVehicleEntry(vehicle)

        if not vehicleEntry then
            handleUnclaimedVehicle(context, player, vehicle, myEntry)
        elseif vehicleEntry.owner == myEntry.username then
            handleOwnedVehicle(context, vehicleEntry)
        else
            handleOtherVehicle(context, player, vehicleEntry)
        end
    end

    -- Handle claimed vehicles list
    handleClaimedVehiclesList(context, myEntry)

    -- Handle admin menu
    if WL_Utils.canModerate(player) then
        local wlMenu = WL_ContextMenuUtils.getOrCreateSubMenu(context, "WL Admin")
        wlMenu:addOption("Vehicle Claims", AutoClaimsAdminUI, AutoClaimsAdminUI.display)
        wlMenu:addOption("Vehicle Search", VehicleSearchUI, VehicleSearchUI.display)
    end
end

Events.OnPreFillWorldObjectContextMenu.Add(showWorldMenu)