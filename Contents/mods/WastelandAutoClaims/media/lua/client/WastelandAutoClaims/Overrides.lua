local function showNoPermissionMessage()
    getPlayer():setHaloNote("No Permission", 255, 0, 0, 300)
end

local function logStaffAction(character, vehicle, action)
    local vehicleName = vehicle:getScript():getName() .. " " .. vehicle:getModData().WAC_ID
    WastelandAutoClaimsCore:logInfo("Staff member " .. character:getUsername() .. " " .. action .. " vehicle " .. vehicleName .. " without permission.")
end

local overrides = {
    enterVehicle = ISEnterVehicle.new,
    switchSeat = ISSwitchVehicleSeat.new,
    attachTrailer = ISAttachTrailerToVehicle.new,
    detachTrailer = ISDetachTrailerFromVehicle.new,
    uninstallPart = ISUninstallVehiclePart.new,
    takeGas = ISTakeGasolineFromVehicle.new,
    takeEngineParts = ISTakeEngineParts.new,
    deflateTire = ISDeflateTire.new,
    smashWindow = ISSmashVehicleWindow.new,
    inventoryTransferValid = ISInventoryTransferAction.isValid,
}

function ISEnterVehicle:new(character, vehicle, seat)
    if seat == 0 and not WastelandAutoClaimsCore:playerCan(character, vehicle, "drive") then
        if WL_Utils.isStaff(character) then
            logStaffAction(character, vehicle, "entered to drive")
            return overrides.enterVehicle(self, character, vehicle, seat)
        end
        showNoPermissionMessage()
        return {ignoreAction = true}
    end

    if seat ~= 0 and not WastelandAutoClaimsCore:playerCan(character, vehicle, "passenger") then
        if WL_Utils.isStaff(character) then
            logStaffAction(character, vehicle, "entered as passenger")
            return overrides.enterVehicle(self, character, vehicle, seat)
        end
        showNoPermissionMessage()
        return {ignoreAction = true}
    end

    return overrides.enterVehicle(self, character, vehicle, seat)
end

function ISSwitchVehicleSeat:new(character, seat)
    local vehicle = character:getVehicle()
    if seat == 0 and not WastelandAutoClaimsCore:playerCan(character, vehicle, "drive") then
        if WL_Utils.isStaff(character) then
            logStaffAction(character, vehicle, "switched to drive")
            return overrides.switchSeat(self, character, seat)
        end
        showNoPermissionMessage()
        return {ignoreAction = true}
    end

    if seat ~= 0 and not WastelandAutoClaimsCore:playerCan(character, vehicle, "passenger") then
        if WL_Utils.isStaff(character) then
            logStaffAction(character, vehicle, "switched as passenger")
            return overrides.switchSeat(self, character, seat)
        end
        showNoPermissionMessage()
        return {ignoreAction = true}
    end

    return overrides.switchSeat(self, character, seat)
end

function ISAttachTrailerToVehicle:new(character, vehicleA, vehicleB, attachmentA, attachmentB)
    if not WastelandAutoClaimsCore:playerCan(character, vehicleA, "attach") then
        if WL_Utils.isStaff(character) then
            logStaffAction(character, vehicleA, "attached trailer")
            return overrides.attachTrailer(self, character, vehicleA, vehicleB, attachmentA, attachmentB)
        end
        showNoPermissionMessage()
        return {ignoreAction = true}
    end
    
    if not WastelandAutoClaimsCore:playerCan(character, vehicleB, "attach") then
        if WL_Utils.isStaff(character) then
            logStaffAction(character, vehicleB, "attached to")
            return overrides.attachTrailer(self, character, vehicleA, vehicleB, attachmentA, attachmentB)
        end
        showNoPermissionMessage()
        return {ignoreAction = true}
    end

    return overrides.attachTrailer(self, character, vehicleA, vehicleB, attachmentA, attachmentB)
end

function ISDetachTrailerFromVehicle:new(character, vehicle, attachment)
    if not WastelandAutoClaimsCore:playerCan(character, vehicle, "attach") then
        if WL_Utils.isStaff(character) then
            logStaffAction(character, vehicle, "detached trailer")
            return overrides.detachTrailer(self, character, vehicle, attachment)
        end
        showNoPermissionMessage()
        return {ignoreAction = true}
    end

    return overrides.detachTrailer(self, character, vehicle, attachment)
end

function ISUninstallVehiclePart:new(character, part, time)
    local vehicle = part:getVehicle()
    if not WastelandAutoClaimsCore:playerCan(character, vehicle, "mechanics") then
        if WL_Utils.isStaff(character) then
            logStaffAction(character, vehicle, "uninstalled part")
            return overrides.uninstallPart(self, character, part, time)
        end
        showNoPermissionMessage()
        return {ignoreAction = true}
    end

    return overrides.uninstallPart(self, character, part, time)
end

function ISTakeGasolineFromVehicle:new(character, part, item, time)
    local vehicle = part:getVehicle()
    if not WastelandAutoClaimsCore:playerCan(character, vehicle, "fuel") then
        if WL_Utils.isStaff(character) then
            logStaffAction(character, vehicle, "took gas")
            return overrides.takeGas(self, character, part, item, time)
        end
        showNoPermissionMessage()
        return {ignoreAction = true}
    end

    return overrides.takeGas(self, character, part, item, time)
end

function ISTakeEngineParts:new(character, part, item, time)
    local vehicle = part:getVehicle()
    if not WastelandAutoClaimsCore:playerCan(character, vehicle, "mechanics") then
        if WL_Utils.isStaff(character) then
            logStaffAction(character, vehicle, "took engine parts")
            return overrides.takeEngineParts(self, character, part, item, time)
        end
        showNoPermissionMessage()
        return {ignoreAction = true}
    end

    return overrides.takeEngineParts(self, character, part, item, time)
end

function ISDeflateTire:new(character, part, psi, time)
    local vehicle = part:getVehicle()
    if not WastelandAutoClaimsCore:playerCan(character, vehicle, "mechanics") then
        if WL_Utils.isStaff(character) then
            logStaffAction(character, vehicle, "deflated tire")
            return overrides.deflateTire(self, character, part, psi, time)
        end
        showNoPermissionMessage()
        return {ignoreAction = true}
    end

    return overrides.deflateTire(self, character, part, psi, time)
end

function ISSmashVehicleWindow:new(character, part, open)
    local vehicle = part:getVehicle()
    if not WastelandAutoClaimsCore:playerCan(character, vehicle, "mechanics") then
        if WL_Utils.isStaff(character) then
            logStaffAction(character, vehicle, "smashed window")
            return overrides.smashWindow(self, character, part, open)
        end
        showNoPermissionMessage()
        return {ignoreAction = true}
    end

    return overrides.smashWindow(self, character, part, open)
end

function ISInventoryTransferAction:isValid()
    if self.destContainer:getParent() and instanceof(self.destContainer:getParent(), "BaseVehicle") and not WastelandAutoClaimsCore:playerCan(self.character, self.destContainer:getParent(), "accessInventory") then
        if WL_Utils.isStaff(self.character) then
            logStaffAction(self.character, self.destContainer:getParent(), "transferred inventory into")
            return overrides.inventoryTransferValid(self)
        end
        showNoPermissionMessage()
        return false
    end
    
    if self.srcContainer:getParent() and instanceof(self.srcContainer:getParent(), "BaseVehicle") and not WastelandAutoClaimsCore:playerCan(self.character, self.srcContainer:getParent(), "accessInventory") then
        if WL_Utils.isStaff(self.character) then
            logStaffAction(self.character, self.srcContainer:getParent(), "transferred inventory out of")
            return overrides.inventoryTransferValid(self)
        end
        showNoPermissionMessage()
        return false
    end

    return overrides.inventoryTransferValid(self)
end

if ISVehicleSalvage then
    overrides.salvage = ISVehicleSalvage.new

    function ISVehicleSalvage:new(character, vehicle, part)
        if WastelandAutoClaimsCore:isClaimed(vehicle) then
            if WL_Utils.isStaff(character) then
                logStaffAction(character, vehicle, "salvaged")
                return overrides.salvage(self, character, vehicle, part)
            end
            showNoPermissionMessage()
            return {ignoreAction = true}
        end

        return overrides.salvage(self, character, vehicle, part)
    end
end

if ISInstallTuningVehiclePart then
    overrides.installTuningVehiclePart = ISInstallTuningVehiclePart.new
    overrides.uninstallTuningVehiclePart = ISUninstallTuningVehiclePart.new

    function ISInstallTuningVehiclePart:new(character, part, time, modelName)
        if not WastelandAutoClaimsCore:playerCan(character, part:getVehicle(), "mechanics") then
            if WL_Utils.isStaff(character) then
                logStaffAction(character, part:getVehicle(), "installed tuning part")
                return overrides.installTuningVehiclePart(self, character, part, time, modelName)
            end
            showNoPermissionMessage()
            return {ignoreAction = true}
        end
        return overrides.installTuningVehiclePart(self, character, part, time, modelName)
    end

    function ISUninstallTuningVehiclePart:new(character, part, time, modelName)
        if not WastelandAutoClaimsCore:playerCan(character, part:getVehicle(), "mechanics") then
            if WL_Utils.isStaff(character) then
                logStaffAction(character, part:getVehicle(), "uninstalled tuning part")
                return overrides.uninstallTuningVehiclePart(self, character, part, time, modelName)
            end
            showNoPermissionMessage()
            return {ignoreAction = true}
        end
        return overrides.uninstallTuningVehiclePart(self, character, part, time, modelName)
    end
end