VehicleSearchUI = ISCollapsableWindow:derive("VehicleSearchUI")
VehicleSearchUI.instance = nil

local VEHICLES_PER_PAGE = 10

function VehicleSearchUI:teleportToVehicle(vehicleEntry)
    WL_Utils.teleportPlayerToCoords(getPlayer(), vehicleEntry.lastX, vehicleEntry.lastY, 0)
end

function VehicleSearchUI:new()
    local scale = getTextManager():MeasureStringY(UIFont.Small, "XXX") / 12
    local width = scale * 400
    local height = scale * 300
    local x = getCore():getScreenWidth() / 2 - width / 2
    local y = getCore():getScreenHeight() / 2 - height / 2
    local o = ISCollapsableWindow:new(x, y, width, height)
    setmetatable(o, self)
    self.__index = self
    o.title = "Vehicle Search"
    o.scale = scale
    o.vehicles = {}
    o.filteredVehicles = {}
    o.currentPage = 1
    o.lastSearchValue = ""
    o:initialise()
    return o
end

function VehicleSearchUI:initialise()
    ISCollapsableWindow.initialise(self)
    self.moveWithMouse = true
    self:setResizable(false)

    -- Main layout using GravyUI
    local win = GravyUI.Node(self.width, self.height, self):pad(2 * self.scale, 15 * self.scale, 2 * self.scale, 2 * self.scale)
    local header, body = win:rows({20 * self.scale, 1.0}, 2 * self.scale)

    -- Search bar in header
    local searchLabel, searchInput = header:cols({0.3, 0.7}, 2 * self.scale)
    searchLabel:makeLabel("Search:", UIFont.Small, {r=1, g=1, b=1, a=1}, "left")
    self.searchEntry = searchInput:makeTextBox("", false)

    -- Vehicle list section with pagination
    local listRows = {body:rows({
        1.0,            -- Vehicle list
        15 * self.scale -- Pagination controls
    }, 2 * self.scale)}

    -- Vehicle list
    self.vehicleList = listRows[1]:makeScrollingListBox(UIFont.Small)
    self.vehicleList.itemheight = 22 * self.scale
    local parentUI = self
    self.vehicleList.doDrawItem = function(list, y, item, alt)
        if not item or not item.item then return y end
        
        local vehicleEntry = item.item
        local nameWidth = list:getWidth() * 0.4
        local ownerWidth = list:getWidth() * 0.3
        local locationWidth = list:getWidth() * 0.3
        
        -- Draw background for each column with hover effects
        local isRowHovered = list:isMouseOver() and list:getMouseY() >= y and list:getMouseY() <= y + list.itemheight
        local mouseX = list:getMouseX()
        
        -- Helper function to check if mouse is over a specific column
        local function isColumnHovered(startX, width)
            return isRowHovered and mouseX >= startX and mouseX <= startX + width
        end
        
        -- Draw column backgrounds with hover effects
        -- Name column
        if isColumnHovered(0, nameWidth) then
            list:drawRect(0, y, nameWidth, list.itemheight + 1, 0.3, 0.7, 0.35, 0.15)
        else
            list:drawRect(0, y, nameWidth, list.itemheight + 1, 0.1, 0.1, 0.1, 1.0)
        end
        
        -- Owner column
        if isColumnHovered(nameWidth, ownerWidth) then
            list:drawRect(nameWidth, y, ownerWidth, list.itemheight + 1, 0.3, 0.7, 0.35, 0.15)
        else
            list:drawRect(nameWidth, y, ownerWidth, list.itemheight + 1, 0.1, 0.1, 0.1, 1.0)
        end
        
        -- Location column
        if isColumnHovered(nameWidth + ownerWidth, locationWidth) then
            list:drawRect(nameWidth + ownerWidth, y, locationWidth, list.itemheight + 1, 0.3, 0.7, 0.35, 0.15)
        else
            list:drawRect(nameWidth + ownerWidth, y, locationWidth, list.itemheight + 1, 0.1, 0.1, 0.1, 1.0)
        end
        
        -- Draw borders
        list:drawRectBorder(0, y, list:getWidth(), list.itemheight + 1, 0.5, 1, 1, 1)
        list:drawRectBorder(nameWidth, y, 1, list.itemheight + 1, 0.5, 1, 1, 1)
        list:drawRectBorder(nameWidth + ownerWidth, y, 1, list.itemheight + 1, 0.5, 1, 1, 1)
        
        -- Draw text content with error handling
        -- Vehicle name
        local vehicleName = vehicleEntry.type and getText("IGUI_VehicleName" .. vehicleEntry.type) or "Unknown"
        list:drawText(vehicleName, 4, y + (list.itemheight - 14 * self.scale) / 2, 1, 1, 1, 1, UIFont.Small)
        
        -- Owner
        local owner = vehicleEntry.owner or "None"
        list:drawTextCentre(owner, nameWidth + ownerWidth/2, y + (list.itemheight - 14 * self.scale) / 2, 1, 1, 1, 1, UIFont.Small)
        
        -- Location
        local location = (vehicleEntry.lastX and vehicleEntry.lastY) and (vehicleEntry.lastX .. "," .. vehicleEntry.lastY) or "Unknown"
        list:drawTextCentre(location, nameWidth + ownerWidth + locationWidth/2, y + (list.itemheight - 14 * self.scale) / 2, 1, 1, 1, 1, UIFont.Small)
        
        -- Handle clicks
        if isRowHovered and list.mouseDown then
            -- Handle column clicks
            if isColumnHovered(0, nameWidth) and vehicleEntry.type then
                -- Click on name - open ClaimedVehicleUI
                local ui = ClaimedVehicleUI:display(vehicleEntry)
                ui:setAlwaysOnTop(true)
            elseif isColumnHovered(nameWidth, ownerWidth) and vehicleEntry.owner then
                -- Click on owner - open AutoClaimsAdminUI
                local ui = AutoClaimsAdminUI:display()
                ui:setAlwaysOnTop(true)
                -- Find and select the owner
                for i, player in ipairs(ui.players) do
                    if player.username == vehicleEntry.owner then
                        ui:onPlayerSelected(i, true)
                        break
                    end
                end
            elseif isColumnHovered(nameWidth + ownerWidth, locationWidth) and vehicleEntry.lastX and vehicleEntry.lastY then
                -- Click on location - teleport
                parentUI:teleportToVehicle(vehicleEntry)
            end
            list.mouseDown = false
        end
        
        return y + list.itemheight
    end
    self.vehicleList.onMouseDown = function(list, x, y)
        list.mouseDown = true
        return true
    end
    self.vehicleList.onMouseUp = function(list, x, y)
        list.mouseDown = false
        return true
    end

    -- Pagination controls
    local prevBtn, pageLabel, nextBtn = listRows[2]:cols({0.2, 0.6, 0.2}, 5 * self.scale)
    self.prevButton = prevBtn:makeButton("<", self, self.onPrevPage)
    self.pageLabel = pageLabel:makeLabel("Page 1", UIFont.Small, {r=1, g=1, b=1, a=1}, "center")
    self.nextButton = nextBtn:makeButton(">", self, self.onNextPage)

    -- Set up tooltips
    self.searchEntry:setTooltip("Type to filter vehicles by type or owner")

    self:refreshVehicles()
    self:updateButtons()
end

function VehicleSearchUI:refreshVehicles()
    self.vehicles = {}
    for vehicleId, vehicleEntry in pairs(WastelandAutoClaimsCore.publicData.vehicles) do
        table.insert(self.vehicles, {id = vehicleId, data = vehicleEntry})
    end
    table.sort(self.vehicles, function(a, b) 
        return getText("IGUI_VehicleName" .. a.data.type) < getText("IGUI_VehicleName" .. b.data.type)
    end)
    self:applyFilter()
end

function VehicleSearchUI:applyFilter()
    local search = string.lower(self.searchEntry:getText())
    self.filteredVehicles = {}
    for _, vehicle in ipairs(self.vehicles) do
        local vehicleName = string.lower(getText("IGUI_VehicleName" .. vehicle.data.type))
        local ownerName = vehicle.data.owner and string.lower(vehicle.data.owner) or ""
        if search == "" or 
           string.find(vehicleName, search) or 
           string.find(ownerName, search) then
            table.insert(self.filteredVehicles, vehicle)
        end
    end
    self:updateVehicleList()
end

function VehicleSearchUI:updateVehicleList()
    self.vehicleList:clear()
    local startIdx = (self.currentPage - 1) * VEHICLES_PER_PAGE + 1
    for i = startIdx, math.min(startIdx + VEHICLES_PER_PAGE - 1, #self.filteredVehicles) do
        local vehicle = self.filteredVehicles[i]
        self.vehicleList:addItem(getText("IGUI_VehicleName" .. vehicle.data.type), vehicle.data)
    end
    self:updateButtons()
end

function VehicleSearchUI:updateButtons()
    self.prevButton:setEnabled(self.currentPage > 1)
    self.nextButton:setEnabled(self.currentPage < math.ceil(#self.filteredVehicles / VEHICLES_PER_PAGE))
    self.pageLabel:setText(string.format("Page %d/%d", self.currentPage, math.max(1, math.ceil(#self.filteredVehicles / VEHICLES_PER_PAGE))))
end

function VehicleSearchUI:onPrevPage()
    if self.currentPage > 1 then
        self.currentPage = self.currentPage - 1
        self:updateVehicleList()
    end
end

function VehicleSearchUI:onNextPage()
    local maxPage = math.ceil(#self.filteredVehicles / VEHICLES_PER_PAGE)
    if self.currentPage < maxPage then
        self.currentPage = self.currentPage + 1
        self:updateVehicleList()
    end
end

function VehicleSearchUI:prerender()
    ISCollapsableWindow.prerender(self)
    
    -- Check if search value has changed
    local currentSearchValue = self.searchEntry:getText()
    if currentSearchValue ~= self.lastSearchValue then
        self.lastSearchValue = currentSearchValue
        self.currentPage = 1
        self:applyFilter()
    end
end

function VehicleSearchUI:close()
    ISCollapsableWindow.close(self)
    self:removeFromUIManager()
    VehicleSearchUI.instance = nil
end

function VehicleSearchUI:display()
    if VehicleSearchUI.instance then
        VehicleSearchUI.instance:close()
    end
    local ui = VehicleSearchUI:new()
    ui:addToUIManager()
    VehicleSearchUI.instance = ui
    return ui
end
