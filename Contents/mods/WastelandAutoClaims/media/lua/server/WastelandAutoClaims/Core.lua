---@class WastelandAutoClaims_VehicleEntry
---@field id number
---@field type string
---@field owner string
---@field lastX number
---@field lastY number
---@field publicPermissions WastelandAutoClaims_Permissions
---@field userPermissions table<string, WastelandAutoClaims_Permissions>

---@class WastelandAutoClaims_PlayerEntry
---@field username string
---@field lastOnlineTime number
---@field vehicles number[]
---@field claimsAdjustment number

---@class WastelandAutoClaims_Permissions
---@field reset number|nil
---@field drive boolean
---@field passenger boolean
---@field accessInventory boolean
---@field attach boolean
---@field fuel boolean
---@field mechanics boolean

---@class WastelandAutoClaimsCore : WL_ClientServerBase
WastelandAutoClaimsCore = WL_ClientServerBase:new("WastelandAutoClaims")
WastelandAutoClaimsCore.needsPublicData = true

WastelandAutoClaimsCore.VEHICLE_PERMISSIONS = {
    ["drive"] = "Drive",
    ["passenger"] = "Passenger",
    ["accessInventory"] = "Access Inventory",
    ["attach"] = "Attach",
    ["fuel"] = "Fuel",
    ["mechanics"] = "Mechanics"
}

function WastelandAutoClaimsCore:onModDataInit()
    self.publicData.vehicles = self.publicData.vehicles or {}
    self.publicData.players = self.publicData.players or {}

    -- migrate from AVCS
    if ModData.exists("AVCSByVehicleSQLID") then
        self:logInfo("Migrating from AVCS")
        local avcs_db = ModData.get("AVCSByVehicleSQLID")
        for vehicleId, avcsDbEntry in pairs(avcs_db) do
            if not self.publicData.vehicles[vehicleId] then
                self:logInfo("\tMigrating vehicle " .. vehicleId .. " (" .. avcsDbEntry.CarModel .. ") for " .. avcsDbEntry.OwnerPlayerID)
                self.publicData.vehicles[vehicleId ] = {
                    id = tonumber(vehicleId),
                    type = string.gsub(avcsDbEntry.CarModel, "Base.", ""),
                    owner = avcsDbEntry.OwnerPlayerID,
                    lastX = avcsDbEntry.LastLocationX,
                    lastY = avcsDbEntry.LastLocationY,
                    userPermissions = {},
                    publicPermissions = {
                        drive = false,
                        passenger = true,
                        accessInventory = false,
                        attach = false,
                        fuel = false,
                        mechanics = false,
                    }
                }
                local playerEntry = self:getPlayerEntry(avcsDbEntry.OwnerPlayerID)
                table.insert(playerEntry.vehicles, vehicleId)
                self.publicData.players[playerEntry.username] = playerEntry
            end
        end
        ModData.remove("AVCSByVehicleSQLID")
        self:savePublicData(false)
    end
end

function WastelandAutoClaimsCore:onPublicDataUpdated()
    local player = getPlayer()
    if player then
        self:fixupPlayer(player)
    end
end

function WastelandAutoClaimsCore:getOrAddId(player, vehicle)
    local id = vehicle:getModData().WAC_ID
    if id then return id end
    
    id = vehicle:getModData().SQLID -- migrate from AVCS
    if not id then
        id = tonumber(vehicle:getSqlId() .. getTimestamp())
    end
    
    vehicle:getModData().WAC_ID = id
    self:syncId(player, vehicle:getId(), id)
    return id
end

local function findVehicle(vehicleId)
    local vehicle = getVehicleById(vehicleId)
    if vehicle then return vehicle end
    local vehicles = getCell():getVehicles()
    for i=1, vehicles:size() do
        local vehicle = vehicles:get(i-1)
        if vehicle:getId() == vehicleId then
            return vehicle
        end
    end
    return nil
end

function WastelandAutoClaimsCore:syncId(player, vehicleId, wacId, fromServer)
    local vehicle = findVehicle(vehicleId)
    if vehicle then
        vehicle:getModData().WAC_ID = wacId
    end
    if isClient() and not fromServer then
        self:sendToServer(player or getPlayer(), "syncId", vehicleId, wacId)
    end
    if isServer() then
        self:sendToAllClients("syncId", vehicleId, wacId, true)
    end
end

function WastelandAutoClaimsCore:updateVehicle(player, vehicleEntry, fromServer)
    if not self.publicData.vehicles then return end -- happens if events are received before init
    
    self.publicData.vehicles[vehicleEntry.id] = vehicleEntry

    if isClient() and not fromServer then
        self:sendToServer(player, "updateVehicle", vehicleEntry)
    end
    if isServer() then
        self:savePublicData(false)
        self:sendToAllClients("updateVehicle", vehicleEntry, true)
    end
end

function WastelandAutoClaimsCore:deleteVehicle(player, vehicleId, fromServer)
    self.publicData.vehicles[vehicleId] = nil

    if isClient() and not fromServer then
        self:sendToServer(player, "deleteVehicle", vehicleId)
    end
    if isServer() then
        self:savePublicData(false)
        self:sendToAllClients("deleteVehicle", vehicleId, true)
    end
end

function WastelandAutoClaimsCore:updatePlayer(player, playerEntry, fromServer)
    self.publicData.players[playerEntry.username] = playerEntry

    if isClient() and not fromServer then
        self:sendToServer(player, "updatePlayer", playerEntry)
    end
    if isServer() then
        self:savePublicData(false)
        self:sendToAllClients("updatePlayer", playerEntry, true)
    end
end

function WastelandAutoClaimsCore:deletePlayer(player, username, fromServer)
    self.publicData.players[username] = nil

    if isClient() and not fromServer then
        self:sendToServer(player, "removePlayer", username)
    end
    if isServer() then
        self:savePublicData(false)
        self:sendToAllClients("removePlayer", username, true)
    end
end

function WastelandAutoClaimsCore:getVehicleEntry(vehicle)
    if type(vehicle) == "number" then
        return self.publicData.vehicles[vehicle]
    end

    local id = self:getOrAddId(nil, vehicle)
    
    return self.publicData.vehicles[id]
end

function WastelandAutoClaimsCore:getPlayerEntry(username)
    return self.publicData.players[username] or {
        username = username,
        lastOnlineTime = WL_Utils.getTimestamp(),
        vehicles = {},
        claimsAdjustment = 0,
    }
end

function WastelandAutoClaimsCore:claimVehicle(player, vehicle)
    local vehicleEntry = self:getVehicleEntry(vehicle)
    if not vehicleEntry then
        vehicleEntry = {
            id = tonumber(vehicle:getSqlId() .. getTimestamp()),
            type = vehicle:getScript():getName()
        }
        self:syncId(player, vehicle:getId(), vehicleEntry.id)
    end

    vehicleEntry.owner = player:getUsername()
    vehicleEntry.lastX = math.floor(vehicle:getX())
    vehicleEntry.lastY = math.floor(vehicle:getY())
    vehicleEntry.publicPermissions = {
        drive = false,
        passenger = true,
        accessInventory = false,
        attach = false,
        fuel = false,
        mechanics = false,
    }
    vehicleEntry.userPermissions = {}

    local playerEntry = self:getPlayerEntry(player:getUsername())
    table.insert(playerEntry.vehicles, vehicleEntry.id)
    self:updateVehicle(player, vehicleEntry)
    self:updatePlayer(player, playerEntry)

    if isClient() and player then
        player:setHaloNote("Vehicle Claimed", 0, 255, 0, 300)
    end

    self:logInfo(player:getUsername() .. " claimed " .. vehicleEntry.type .. " (" .. vehicleEntry.id .. ")", true)
end

function WastelandAutoClaimsCore:unclaimVehicle(player, vehicle)
    local vehicleEntry = self:getVehicleEntry(vehicle)
    if not vehicleEntry then return end
    
    local playerEntry = self.publicData.players[vehicleEntry.owner]
    if playerEntry then
        for i, v in ipairs(playerEntry.vehicles) do
            if v == vehicleEntry.id then
                table.remove(playerEntry.vehicles, i)
                break
            end
        end
    end

    self:deleteVehicle(player, vehicleEntry.id, isServer())
    self:updatePlayer(player, playerEntry)
    if isClient() and player then
        player:setHaloNote("Vehicle Unclaimed", 0, 255, 0, 300)
    end

    self:logInfo(player:getUsername() .. " unclaimed " .. vehicleEntry.type .. " (" .. vehicleEntry.id .. ")", true)
end

function WastelandAutoClaimsCore:checkLoadedVehicle(player, vehicle)
    local vehicleEntry = self:getVehicleEntry(vehicle)
    if not vehicleEntry then return end

    local wasChanged = false

    local x = math.floor(vehicle:getX())
    local y = math.floor(vehicle:getY())
    if vehicleEntry.lastX ~= x or vehicleEntry.lastY ~= y then
        vehicleEntry.lastX = x
        vehicleEntry.lastY = y
        wasChanged = true
    end

    if vehicleEntry.publicPermissions.reset and vehicleEntry.publicPermissions.reset < WL_Utils.getTimestamp() then
        vehicleEntry.publicPermissions.drive = false
        vehicleEntry.publicPermissions.passenger = true
        vehicleEntry.publicPermissions.accessInventory = false
        vehicleEntry.publicPermissions.attach = false
        vehicleEntry.publicPermissions.fuel = false
        vehicleEntry.publicPermissions.mechanics = false
        vehicleEntry.publicPermissions.reset = nil
        wasChanged = true
    end
    
    for _, up in pairs(vehicleEntry.userPermissions) do
        if up.reset and up.reset < WL_Utils.getTimestamp() then
            up.drive = false
            up.passenger = true
            up.accessInventory = false
            up.attach = false
            up.fuel = false
            up.mechanics = false
            up.reset = nil
            wasChanged = true
        end
    end

    if wasChanged then
        self:updateVehicle(player, vehicleEntry)
    end
end

function WastelandAutoClaimsCore:updatePlayerTime(player)
    local playerEntry = self.publicData.players[player:getUsername()]
    if not playerEntry then return end
    playerEntry.lastOnlineTime = WL_Utils.getTimestamp()
    
    -- manual update to avoid sending to clients
    self.publicData.players[player:getUsername()] = playerEntry
end

function WastelandAutoClaimsCore:setVehiclePermission(player, vehicle, username, permission, enabled)
    local vehicleEntry = self:getVehicleEntry(vehicle)
    if not vehicleEntry then return end
    
    if username then
        if not vehicleEntry.userPermissions[username] then
            vehicleEntry.userPermissions[username] = {
                drive = false,
                passenger = true,
                accessInventory = false,
                attach = false,
                fuel = false,
                mechanics = false,
            }
        end
        if permission then
            vehicleEntry.userPermissions[username][permission] = enabled
            self:logInfo(player:getUsername() .. " set permission " .. permission .. " to " .. (enabled and "true" or "false") .. " for " .. username .. " on " .. vehicleEntry.type .. " (" .. vehicleEntry.id .. ")", true)
        else
            vehicleEntry.userPermissions[username].drive = enabled
            vehicleEntry.userPermissions[username].passenger = enabled
            vehicleEntry.userPermissions[username].accessInventory = enabled
            vehicleEntry.userPermissions[username].attach = enabled
            vehicleEntry.userPermissions[username].fuel = enabled
            vehicleEntry.userPermissions[username].mechanics = enabled
            self:logInfo(player:getUsername() .. " set all permissions to " .. (enabled and "true" or "false") .. " for " .. username .. " on " .. vehicleEntry.type .. " (" .. vehicleEntry.id .. ")", true)
        end
    else
        if permission then
            vehicleEntry.publicPermissions[permission] = enabled
            self:logInfo(player:getUsername() .. " set permission " .. permission .. " to " .. (enabled and "true" or "false") .. " for everyone on " .. vehicleEntry.type .. " (" .. vehicleEntry.id .. ")", true)
        else
            vehicleEntry.publicPermissions.drive = enabled
            vehicleEntry.publicPermissions.passenger = enabled
            vehicleEntry.publicPermissions.accessInventory = enabled
            vehicleEntry.publicPermissions.attach = enabled
            vehicleEntry.publicPermissions.fuel = enabled
            vehicleEntry.publicPermissions.mechanics = enabled
            self:logInfo(player:getUsername() .. " set all permissions to " .. (enabled and "true" or "false") .. " for everyone on " .. vehicleEntry.type .. " (" .. vehicleEntry.id .. ")", true)
        end
    end

    self:updateVehicle(player, vehicleEntry)
    
    if isClient() and player then
        player:setHaloNote("Permission " .. (enabled and "Set" or "Unset"), 0, 255, 0, 300)
    end
end

function WastelandAutoClaimsCore:removeVehiclePermission(player, vehicle, username)
    local vehicleEntry = self:getVehicleEntry(vehicle)
    if not vehicleEntry then return end
    
    if username then
        if vehicleEntry.userPermissions[username] then
            vehicleEntry.userPermissions[username] = nil
        end
        self:updateVehicle(player, vehicleEntry)
        self:logInfo(player:getUsername() .. " removed permission for " .. username .. " on " .. vehicleEntry.type .. " (" .. vehicleEntry.id .. ")", true)
    end
    
    if isClient() and player then
        player:setHaloNote("Permission Removed", 0, 255, 0, 300)
    end
end

function WastelandAutoClaimsCore:addVehiclePermission(player, vehicle, username)
    local vehicleEntry = self:getVehicleEntry(vehicle)
    if not vehicleEntry then return end

    vehicleEntry.userPermissions[username] = {
        drive = false,
        passenger = true,
        accessInventory = false,
        attach = false,
        fuel = false,
        mechanics = false,
    }

    self:updateVehicle(player, vehicleEntry)
    
    if isClient() and player then
        player:setHaloNote("Permission Added", 0, 255, 0, 300)
    end

    self:logInfo(player:getUsername() .. " added permission for " .. username .. " on " .. vehicleEntry.type .. " (" .. vehicleEntry.id .. ")", true)
end

function WastelandAutoClaimsCore:isClaimed(vehicle)
    return self:getVehicleEntry(vehicle) ~= nil
end

function WastelandAutoClaimsCore:playerCan(player, vehicle, permission)
    local vehicleEntry = self:getVehicleEntry(vehicle)
    if not vehicleEntry then return true end

    if vehicleEntry.owner == player:getUsername() then
        return true
    end

    if vehicleEntry.userPermissions[player:getUsername()] and vehicleEntry.userPermissions[player:getUsername()][permission] then
        return true
    end

    return vehicleEntry.publicPermissions[permission]
end

function WastelandAutoClaimsCore:getEffectiveClaims(playerEntry)
    return SandboxVars.WastelandAutoClaims.DefaultClaims + playerEntry.claimsAdjustment
end

function WastelandAutoClaimsCore:setNewOwner(player, vehicle, username)
    local vehicleEntry = self:getVehicleEntry(vehicle)
    if not vehicleEntry then return end

    local currentOwnerEntry = self:getPlayerEntry(vehicleEntry.owner)
    if not currentOwnerEntry then return end
    
    local newOwnerEntry = self:getPlayerEntry(username)
    if not newOwnerEntry then return end
    
    if #newOwnerEntry.vehicles >= self:getEffectiveClaims(newOwnerEntry) then
        player:Say("Player " .. username .. " has reached the maximum number of claims.")
        return
    end

    for i, v in ipairs(currentOwnerEntry.vehicles) do
        if v == vehicleEntry.id then
            table.remove(currentOwnerEntry.vehicles, i)
            break
        end
    end

    table.insert(newOwnerEntry.vehicles, vehicleEntry.id)
    vehicleEntry.owner = username
    self:updatePlayer(player, currentOwnerEntry)
    self:updatePlayer(player, newOwnerEntry)
    self:updateVehicle(player, vehicleEntry)

    self:logInfo(player:getUsername() .. " set new owner " .. username .. " on " .. vehicleEntry.type .. " (" .. vehicleEntry.id .. ")", true)
    
    if isClient() and player then
        player:setHaloNote("Owner Changed", 0, 255, 0, 300)
    end
end

function WastelandAutoClaimsCore:fixupPlayer(player)
    local playerEntry = self:getPlayerEntry(player:getUsername())
    if not playerEntry then return end

    for i=#playerEntry.vehicles, 1, -1 do
        local vehicleEntry = self.publicData.vehicles[playerEntry.vehicles[i]]
        if not vehicleEntry or vehicleEntry.owner ~= player:getUsername() then
            table.remove(playerEntry.vehicles, i)
            self:logError("Removing invalid vehicle " .. playerEntry.vehicles[i] .. " from player " .. player:getUsername())
        end
    end
end

function WastelandAutoClaimsCore:setClaimsAdjustment(player, username, adjustment)
    local playerEntry = self:getPlayerEntry(username)
    if not playerEntry then return end

    playerEntry.claimsAdjustment = adjustment
    self:updatePlayer(player, playerEntry)
    self:logInfo(player:getUsername() .. " set claims adjustment to " .. adjustment .. " for " .. username, true)
    
    if isClient() and player then
        player:setHaloNote("Claims Adjustment Set", 0, 255, 0, 300)
    end
end

function WastelandAutoClaimsCore:resetUserTime(player, username)
    local playerEntry = self:getPlayerEntry(username)
    if not playerEntry then return end

    playerEntry.lastOnlineTime = WL_Utils.getTimestamp()
    self:updatePlayer(player, playerEntry)
    self:logInfo(player:getUsername() .. " reset last online time for " .. username, true)
    
    if isClient() and player then
        player:setHaloNote("Last Online Time Reset", 0, 255, 0, 300)
    end
end

function WastelandAutoClaimsCore:requestPermission(player, vehicle, permission)
    local vehicleEntry = self:getVehicleEntry(vehicle)
    if not vehicleEntry then return end
    
    if isClient() then
        if not vehicleEntry then return end
        self:sendToServer(player, "requestPermission", vehicleEntry.id, permission)
        player:setHaloNote("Permission Requested", 0, 255, 0, 300)
    end
    if isServer() then
        local allPlayers = getOnlinePlayers()
        local ownerPlayer = nil
        for i=0, allPlayers:size()-1 do
            local p = allPlayers:get(i)
            if p:getUsername() == vehicleEntry.owner then
                ownerPlayer = p
                break
            end
        end
        self:logInfo(player:getUsername() .. " requested permission " .. (permission or "all") .. " on " .. vehicleEntry.type .. " (" .. vehicleEntry.id .. ")", true)
        if not ownerPlayer then
            self:sendToClient(player, "permissionReply", "Owner " .. vehicleEntry.owner .. " is not online.", false)
            return
        end
        self:sendToClient(ownerPlayer, "permissionRequest", player:getUsername(), vehicleEntry.id, permission)
    end
end

function WastelandAutoClaimsCore:permissionRequest(player, username, vehicleId, permission)
    local vehicleEntry = self.publicData.vehicles[vehicleId]
    if not vehicleEntry then return end
    
    if isClient() then
        local name = permission and WastelandAutoClaimsCore.VEHICLE_PERMISSIONS[permission] or "All"
        local message = username .. " requested " .. name .. " permission on " .. getText("IGUI_VehicleName" .. vehicleEntry.type)
        local s = self
        local modal = ISModalDialog:new(getCore():getScreenWidth() / 2 - 150, getCore():getScreenHeight() / 2 - 100, 300, 200, message, true, nil, function (_, button)
            if button.internal == "YES" then
                if permission then
                    vehicleEntry.userPermissions[username] = vehicleEntry.userPermissions[username] or {
                        drive = false,
                        passenger = false,
                        accessInventory = false,
                        attach = false,
                        fuel = false,
                        mechanics = false,
                    }
                    vehicleEntry.userPermissions[username][permission] = true
                else
                    vehicleEntry.userPermissions[username] = {
                        drive = true,
                        passenger = true,
                        accessInventory = true,
                        attach = true,
                        fuel = true,
                        mechanics = true,
                    }
                end
                s:updateVehicle(player, vehicleEntry)
                s:sendToServer(player, "permissionReply", name .. " permission granted.", username, true)
                s:logInfo(player:getUsername() .. " granted permission " .. name .. " to " .. username .. " on " .. vehicleEntry.type .. " (" .. vehicleEntry.id .. ")", true)
            else
                s:sendToServer(player, "permissionReply", name .. " permission denied.", username, false)
                s:logInfo(player:getUsername() .. " denied permission " .. name .. " to " .. username .. " on " .. vehicleEntry.type .. " (" .. vehicleEntry.id .. ")", true)
            end
        end)
        modal:initialise()
        modal:addToUIManager()
    end
end

function WastelandAutoClaimsCore:permissionReply(player, message, usernameOrSuccess, success)
    if isClient() then
        if usernameOrSuccess == true then
            player:setHaloNote(message, 0, 255, 0, 300)
        else
            player:setHaloNote(message, 255, 0, 0, 300)
        end
    end
    if isServer() then
        local allPlayers = getOnlinePlayers()
        local requesterPlayer = nil
        for i=0, allPlayers:size()-1 do
            local p = allPlayers:get(i)
            if p:getUsername() == usernameOrSuccess then
                requesterPlayer = p
                break
            end
        end
        if requesterPlayer then
            self:sendToClient(requesterPlayer, "permissionReply", message, success)
        end
    end
end