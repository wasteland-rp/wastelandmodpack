if isClient() then return end -- server side only

-- update locations of all vehicles
local function checkLoadedVehicles()
    local vehicles = getCell():getVehicles()
    for i=0, vehicles:size()-1 do
        local vehicle = vehicles:get(i)
        WastelandAutoClaimsCore:checkLoadedVehicle(nil, vehicle)
    end
end

-- update player times
local function updatePlayerTimes()
    local onlinePlayers = getOnlinePlayers()
    for i=0, onlinePlayers:size()-1 do
        local player = onlinePlayers:get(i)
        WastelandAutoClaimsCore:updatePlayerTime(player)
    end

    local cTime = getTimestamp()
    for username, playerEntry in pairs(WastelandAutoClaimsCore.publicData.players) do
        local dTime = cTime - playerEntry.lastOnlineTime
        -- 60sec * 60min * 24hours * days
        local timeoutSeconds = 60 * 60 * 24 * SandboxVars.WastelandAutoClaims.PlayerTimeoutLength
        if dTime > timeoutSeconds then
            WastelandAutoClaimsCore:logInfo("Player " .. username .. " has been offline for " .. SandboxVars.WastelandAutoClaims.PlayerTimeoutLength .. " days, unclaiming vehicles and deleting player entry")
            for i, vehicleId in ipairs(playerEntry.vehicles) do
                WastelandAutoClaimsCore:unclaimVehicle(nil, vehicleId)
            end
            WastelandAutoClaimsCore:deletePlayer(nil, username)
        end
    end
end


Events.EveryTenMinutes.Add(checkLoadedVehicles)
Events.EveryHours.Add(updatePlayerTimes)