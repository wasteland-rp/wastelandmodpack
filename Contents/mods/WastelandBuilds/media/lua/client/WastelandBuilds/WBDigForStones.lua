require "TimedActions/ISBaseTimedAction"

WBDigForStones = ISBaseTimedAction:derive("WBDigForStones")

-- todo: track where you have searched before, so you don't keep finding stones in the same spot

function WBDigForStones:isValid()
    return self.character:getPrimaryHandItem() == self.shovel
end

function WBDigForStones:waitToStart()
    self.character:faceThisObject(self.square:getObjects():get(0))
    return self.character:shouldBeTurning()
end

function WBDigForStones:update()
    self.character:faceThisObject(self.square:getObjects():get(0))
    self.character:setMetabolicTarget(Metabolics.DiggingSpade);
end

function WBDigForStones:start()
    if self.character:isSitOnGround() then
		self.character:setVariable("sitonground", false);
		self.character:setVariable("forceGetUp", true);
	end
    self:setActionAnim(ISFarmingMenu.getShovelAnim(self.shovel));
    self:setOverrideHandModels(self.shovel, nil);
    self.sound = self.character:playSound("Shoveling")
end

function WBDigForStones:stop()
    self.character:stopOrTriggerSound(self.sound)
    ISBaseTimedAction.stop(self)
end

function WBDigForStones:perform()
    self.character:stopOrTriggerSound(self.sound)

    if self.shovel:isUseEndurance() then
        local use = self.shovel:getWeight() * self.shovel:getFatigueMod(self.character) * self.character:getFatigueMod() * self.shovel:getEnduranceMod() * 0.03
        self.character:getStats():setEndurance(self.character:getStats():getEndurance() - use)
    end

    local foragingSkill = self.character:getPerkLevel(Perks.PlantScavenging)
    local chanceToFind = 25 + foragingSkill * 5
    local roll = ZombRand(100)
    if roll <= chanceToFind then
        local numStones = ZombRand(10, 20)
        WBStoning.AddStone(self.square, numStones)
        self.character:getXp():AddXP(Perks.PlantScavenging, 5)
        self.character:addLineChatElement("Found a stone deposit!", 0.8, 0.8, 1.0)
    else
        self.character:addLineChatElement("I didn't find anything.", 0.8, 0.8, 1.0)
    end

    -- needed to remove from queue / start next.
    ISBaseTimedAction.perform(self)
end


function WBDigForStones:new(character, square, shovel)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.character = character
    o.square = square
    o.shovel = shovel
    o.stopOnWalk = true
    o.stopOnRun = true
    o.maxTime = 500
    if o.character:isTimedActionInstant() then o.maxTime = 1; end
    o.caloriesModifier = 5;
    return o
end
