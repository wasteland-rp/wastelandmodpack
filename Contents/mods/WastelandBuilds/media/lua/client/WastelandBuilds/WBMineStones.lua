require "TimedActions/ISBaseTimedAction"

WBMineStones = ISBaseTimedAction:derive("WBMineStones")

WBMineStones.oreChances = {}
table.insert(WBMineStones.oreChances, {ore = "Base.WastelandBuildsPyrite", chance = 2})
table.insert(WBMineStones.oreChances, {ore = "Base.WastelandBuildsSaltpeter", chance = 15})
table.insert(WBMineStones.oreChances, {ore = "Base.WastelandBuildsLimestone", chance = 2})
table.insert(WBMineStones.oreChances, {ore = "Base.WastelandBuildsIronOre", chance = 1})
table.insert(WBMineStones.oreChances, {ore = "Base.WastelandBuildsNahcolite", chance = 1})
table.insert(WBMineStones.oreChances, {ore = "Base.Stone", chance = 80})

function WBMineStones:isValid()
    return self.character:getPrimaryHandItem() == self.pickaxe
end

function WBMineStones:waitToStart()
    self.character:faceThisObject(self.square:getObjects():get(0))
    return self.character:shouldBeTurning()
end

function WBMineStones:update()
    self.character:faceThisObject(self.square:getObjects():get(0))
    self.character:setMetabolicTarget(Metabolics.HeavyWork)
end

function WBMineStones:start()
    if self.character:isSitOnGround() then
		self.character:setVariable("sitonground", false);
		self.character:setVariable("forceGetUp", true);
	end
    self:setActionAnim(CharacterActionAnims.DigPickAxe)
    self:setOverrideHandModels(self.pickaxe, nil)
    self.sound = self.character:playSound("Shoveling")
end

function WBMineStones:stop()
    self.character:stopOrTriggerSound(self.sound)
    ISBaseTimedAction.stop(self)
end

function WBMineStones.rollOre()
    return WL_Utils.weightedRandom(WBMineStones.oreChances, "chance").ore
end

function WBMineStones:perform()
    self.character:stopOrTriggerSound(self.sound)


    local mineable = nil
    local objects = self.square:getObjects()
    for i = 0, objects:size() - 1 do
        local object = objects:get(i)
        if WBStoning.IsMineableTile(object:getTextureName()) then
            mineable = object
            break
        end
    end

    if not mineable then return end

    if self.pickaxe:isUseEndurance() then
        local use = self.pickaxe:getWeight() * self.pickaxe:getFatigueMod(self.character) * self.character:getFatigueMod() * self.pickaxe:getEnduranceMod() * 0.03
        use = use * (self.maxTime / 500)
        self.character:getStats():setEndurance(self.character:getStats():getEndurance() - use)
    end

    local pickLowerChance = (self.pickaxe:getConditionLowerChance() * 2 + self.character:getMaintenanceMod() * 2) / 4
    if ZombRand(pickLowerChance) == 0 then
        self.pickaxe:setCondition(self.pickaxe:getCondition() - 1)
        ISWorldObjectContextMenu.checkWeapon(self.character)
    else
        self.character:getXp():AddXP(Perks.Maintenance, 1)
    end

    if mineable:getModData().WB_NumStones and mineable:getModData().WB_NumStones > 0 then
        local maxStonesToFind = math.floor(1 + self.character:getPerkLevel(Perks.Strength) / 3)
        if self.pickaxe:getFullType() == "Base.PickAxe" then
            maxStonesToFind = maxStonesToFind + 1
        elseif self.pickaxe:hasTag("Sledgehammer") then
            maxStonesToFind = maxStonesToFind + 2
        end
        local stonesToFind = math.min(mineable:getModData().WB_NumStones, ZombRand(1, maxStonesToFind))
        local stonesLeft = mineable:getModData().WB_NumStones - stonesToFind

        print("Stones Left: " .. stonesLeft)

        for i = 1, stonesToFind do
            self.character:getInventory():AddItem(WBMineStones.rollOre())
        end
        if stonesToFind == 1 then
            self.character:addLineChatElement("Mined a stone.", 0.8, 0.8, 1.0)
        else
            self.character:addLineChatElement("Mined " .. stonesToFind .. " stones.", 0.8, 0.8, 1.0)
        end

        self.character:getXp():AddXP(Perks.Fitness, 1)
        self.character:getXp():AddXP(Perks.Strength, 1)

        if stonesLeft <= 0 then
            WBStoning.RemoveStone(self.square)
            self.character:addLineChatElement("There are no more stones here.", 0.8, 0.8, 1.0)
        else
            WBStoning.ReplaceStone(self.square, stonesLeft)
            if self.character:getStats():getEndurance() > 0.2 then
                ISTimedActionQueue.add(WBMineStones:new(self.character, self.square, self.pickaxe))
            end
        end
    end
    -- needed to remove from queue / start next.
    ISBaseTimedAction.perform(self)
end

function WBMineStones:new(character, square, pickaxe)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.character = character
    o.square = square
    o.pickaxe = pickaxe
    o.stopOnWalk = true
    o.stopOnRun = true
    o.maxTime = 500
    if o.pickaxe:hasTag("Sledgehammer") then
        o.maxTime = math.floor(o.maxTime / 2)
    end
    if o.pickaxe:getFullType() == "Base.HammerStone" then
        o.maxTime = o.maxTime * 2
    end
    if o.character:isTimedActionInstant() then o.maxTime = 1 end
    o.caloriesModifier = 5
    return o
end
