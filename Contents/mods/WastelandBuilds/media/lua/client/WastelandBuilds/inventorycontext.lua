
Events.OnFillInventoryObjectContextMenu.Add(function(playerIdx, context, items)
	local firstItem = items[1]
	if firstItem == nil then return end
    if firstItem.items then firstItem = firstItem.items[1] end
    if not firstItem then return end
	if firstItem:getFullType() ~= "Base.Stone" then return end
    local player = getSpecificPlayer(playerIdx)
    local hammer = WBStoning.GetCrushingHammer(player)
    if not hammer then return end
    if WBStoning.GetStonesCount(player) < 10 then return end
    local option = context:addOption("Crush Stones", nil, function ()
        ISTimedActionQueue.add(ISEquipWeaponAction:new(player, hammer, 50, true, false))
        ISTimedActionQueue.add(WBCrushStones:new(player, hammer))
    end)
    local tooltip = ISToolTip:new()
    tooltip:initialise()
    tooltip:setVisible(false)
    tooltip:setName("Crush Stones")
    option.toolTip = tooltip
    tooltip.description = "Crush 10 stones looking for a more valuable stone."
end)