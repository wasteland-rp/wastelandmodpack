local function makeBrickWall(
    type, name, description,
    twall1, twall2, twin1, twin2, twin3, twin4, twin5, twin6, twin7, twin8, twin9, twin10, tdoor1, tdoor2, tpill, tcorner)

    local health = 500
    local xp = { Woodwork = 5 }
    local skills = { Woodwork = 8 }
    local tools = { "farming.HandShovel" }
    local baseBricks = 4

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Brick", type },
        Name = name .. " Wall",
        Description = string.format(description, "wall"),
        Tiles = {
            twall1,
            twall2,
            tpill,
        },
        Type = "wall",
        Xp = xp,
        Health = health,
        BonusHealthSkill = { Woodwork = 50 },
        BonusHealthTrait = { Handy = 200 },
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.WastelandBuildsBrick', Amount = baseBricks },
            { Id = 'Base.BucketPlasterFull', Amount = 1 },
        },
        RequiredTools = tools,
    })

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Brick", type },
        Name = name .. " Wall [NW Corner]",
        Description = string.format(description, "wall"),
        Tiles = {
            tcorner,
            tcorner,
        },
        Type = "pillar",
        Xp = xp,
        Health = health * 2,
        BonusHealthSkill = { Woodwork = 100 },
        BonusHealthTrait = { Handy = 400 },
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.WastelandBuildsBrick', Amount = baseBricks * 2 },
            { Id = 'Base.BucketPlasterFull', Amount = 2 },
        },
        RequiredTools = tools,
    })

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Brick", type },
        Name = name .. " Window Frame",
        Description = string.format(description, "window frame"),
        Tiles = {
            twin1,
            twin2,
            tpill,
        },
        Type = "windowwall",
        Xp = xp,
        Health = health,
        BonusHealthSkill = { Woodwork = 50 },
        BonusHealthTrait = { Handy = 200 },
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.WastelandBuildsBrick', Amount = math.ceil(baseBricks * 0.75) },
            { Id = 'Base.BucketPlasterFull', Amount = 1 },
        },
        RequiredTools = tools,
    })

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Brick", type },
        Name = name .. " Large Window",
        Description = string.format(description, "window frame"),
        Tiles = {
            twin3,
            twin4,
            tpill,
        },
        Type = "windowwall",
        Xp = xp,
        Health = health,
        BonusHealthSkill = { Woodwork = 50 },
        BonusHealthTrait = { Handy = 200 },
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.WastelandBuildsBrick', Amount = math.ceil(baseBricks * 0.75) },
            { Id = 'Base.BucketPlasterFull', Amount = 1 },
            { Id = 'Base.Plank', Amount = 3 },
            { Id = 'Base.Nails', Amount = 6 },
            { Id = 'ImprovisedGlass.GlassPane', Amount = 2 },
        },
        RequiredTools = tools,
    })

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Brick", type },
        Name = name .. " Dark Long Window",
        Description = string.format(description, "window frame"),
        Tiles = {
            twin5,
            twin6,
            tpill,
        },
        Type = "windowwall",
        Xp = xp,
        Health = health,
        BonusHealthSkill = { Woodwork = 50 },
        BonusHealthTrait = { Handy = 200 },
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.WastelandBuildsBrick', Amount = math.ceil(baseBricks * 0.75) },
            { Id = 'Base.BucketPlasterFull', Amount = 1 },
            { Id = 'Base.Plank', Amount = 3 },
            { Id = 'Base.Nails', Amount = 6 },
            { Id = 'ImprovisedGlass.GlassPane', Amount = 2 },
        },
        RequiredTools = tools,
    })

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Brick", type },
        Name = name .. " Light Long Window",
        Description = string.format(description, "window frame"),
        Tiles = {
            twin7,
            twin8,
            tpill,
        },
        Type = "windowwall",
        Xp = xp,
        Health = health,
        BonusHealthSkill = { Woodwork = 50 },
        BonusHealthTrait = { Handy = 200 },
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.WastelandBuildsBrick', Amount = math.ceil(baseBricks * 0.75) },
            { Id = 'Base.BucketPlasterFull', Amount = 1 },
            { Id = 'Base.Plank', Amount = 3 },
            { Id = 'Base.Nails', Amount = 6 },
            { Id = 'ImprovisedGlass.GlassPane', Amount = 2 },
        },
        RequiredTools = tools,
    })
    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Brick", type },
        Name = name .. " Two Pane Window",
        Description = string.format(description, "window frame"),
        Tiles = {
            twin9,
            twin10,
            tpill,
        },
        Type = "windowwall",
        Xp = xp,
        Health = health,
        BonusHealthSkill = { Woodwork = 50 },
        BonusHealthTrait = { Handy = 200 },
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.WastelandBuildsBrick', Amount = math.ceil(baseBricks * 0.75) },
            { Id = 'Base.BucketPlasterFull', Amount = 1 },
            { Id = 'Base.Plank', Amount = 3 },
            { Id = 'Base.Nails', Amount = 6 },
            { Id = 'ImprovisedGlass.GlassPane', Amount = 2 },
        },
        RequiredTools = tools,
    })

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Brick", type },
        Name = name .. " Door Frame",
        Description = string.format(description, "door frame"),
        Tiles = {
            tdoor1,
            tdoor2,
            tpill,
        },
        Type = "doorwall",
        Xp = xp,
        Health = health,
        BonusHealthSkill = { Woodwork = 50 },
        BonusHealthTrait = { Handy = 200 },
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.WastelandBuildsBrick', Amount = math.ceil(baseBricks * 0.5) },
            { Id = 'Base.BucketPlasterFull', Amount = 1 },
        },
        RequiredTools = tools,
    })

    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Brick", type },
        Name = name .. " Pillar",
        Description = string.format(description, "pillar"),
        Tiles = {
            tpill,
            tpill,
        },
        Type = "pillar",
        Xp = xp,
        Health = math.floor(health / 2),
        BonusHealthSkill = { Woodwork = 25 },
        BonusHealthTrait = { Handy = 100 },
        RequiredSkills = skills,
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.WastelandBuildsBrick', Amount = math.ceil(baseBricks * 0.25) },
        },
        RequiredTools = tools,
    })
end

makeBrickWall(
    "Red",
    "Red Brick",
    "A red brick %s.",
    "melos_tiles_walls_brick_02_standard_brown_0", "melos_tiles_walls_brick_02_standard_brown_1",
    "melos_tiles_walls_brick_02_standard_brown_8", "melos_tiles_walls_brick_02_standard_brown_9",
    "wlbuilds_windows_0", "wlbuilds_windows_1",
    "wlbuilds_windows_2", "wlbuilds_windows_3",
    "wlbuilds_windows_4", "wlbuilds_windows_5",
    "wlbuilds_windows_6", "wlbuilds_windows_7",
    "melos_tiles_walls_brick_02_standard_brown_10", "melos_tiles_walls_brick_02_standard_brown_11",
    "melos_tiles_walls_brick_02_standard_brown_3", "melos_tiles_walls_brick_02_standard_brown_2")

makeBrickWall(
    "Dark Brown",
    "Dark Brown Brick",
    "A dark brown brick %s.",
    "melos_tiles_walls_brick_raw_01c_0", "melos_tiles_walls_brick_raw_01c_1",
    "melos_tiles_walls_brick_raw_01c_8", "melos_tiles_walls_brick_raw_01c_9",
    "wlbuilds_windows_8", "wlbuilds_windows_9",
    "wlbuilds_windows_10", "wlbuilds_windows_11",
    "wlbuilds_windows_12", "wlbuilds_windows_13",
    "wlbuilds_windows_14", "wlbuilds_windows_15",
    "melos_tiles_walls_brick_raw_01c_10", "melos_tiles_walls_brick_raw_01c_11",
    "melos_tiles_walls_brick_raw_01c_3", "melos_tiles_walls_brick_raw_01c_2")

makeBrickWall(
    "Red Grey",
    "Red Grey Brick",
    "A red grey brick %s.",
    "melos_tiles_walls_brick_02_standard_light_0", "melos_tiles_walls_brick_02_standard_light_1",
    "melos_tiles_walls_brick_02_standard_light_8", "melos_tiles_walls_brick_02_standard_light_9",
    "wlbuilds_windows_16", "wlbuilds_windows_17",
    "wlbuilds_windows_18", "wlbuilds_windows_19",
    "wlbuilds_windows_20", "wlbuilds_windows_21",
    "wlbuilds_windows_22", "wlbuilds_windows_23",
    "melos_tiles_walls_brick_02_standard_light_10", "melos_tiles_walls_brick_02_standard_light_11",
    "melos_tiles_walls_brick_02_standard_light_3", "melos_tiles_walls_brick_02_standard_light_2")

-- makeBrickWall(
--     "White",
--     "White Brick",
--     "A white brick %s.",
--     "melos_tiles_walls_brick_02_eggshell_0", "melos_tiles_walls_brick_02_eggshell_1",
--     "melos_tiles_walls_brick_02_eggshell_8", "melos_tiles_walls_brick_02_eggshell_9",
--     "wlbuilds_windows_24", "wlbuilds_windows_25",
--     "wlbuilds_windows_26", "wlbuilds_windows_27",
--     "wlbuilds_windows_28", "wlbuilds_windows_29",
--     "wlbuilds_windows_30", "wlbuilds_windows_31",
--     "melos_tiles_walls_brick_02_eggshell_10", "melos_tiles_walls_brick_02_eggshell_11",
--     "melos_tiles_walls_brick_02_eggshell_3", "melos_tiles_walls_brick_02_eggshell_2")

makeBrickWall(
    "Eggshell",
    "Eggshell Brick",
    "An eggshell brick %s.",
    "melos_tiles_walls_brick_raw_01_0", "melos_tiles_walls_brick_raw_01_1",
    "melos_tiles_walls_brick_raw_01_8", "melos_tiles_walls_brick_raw_01_9",
    "wlbuilds_windows_32", "wlbuilds_windows_33",
    "wlbuilds_windows_34", "wlbuilds_windows_35",
    "wlbuilds_windows_36", "wlbuilds_windows_37",
    "wlbuilds_windows_38", "wlbuilds_windows_39",
    "melos_tiles_walls_brick_raw_01_10", "melos_tiles_walls_brick_raw_01_11",
    "melos_tiles_walls_brick_raw_01_3", "melos_tiles_walls_brick_raw_01_2")

makeBrickWall(
    "Brown",
    "Brown Brick",
    "An brown brick %s.",
    "melos_tiles_walls_brick_02_brown_0", "melos_tiles_walls_brick_02_brown_1",
    "melos_tiles_walls_brick_02_brown_8", "melos_tiles_walls_brick_02_brown_9",
    "wlbuilds_windows_40", "wlbuilds_windows_41", 
    "wlbuilds_windows_42", "wlbuilds_windows_43", 
    "wlbuilds_windows_44", "wlbuilds_windows_45", 
    "wlbuilds_windows_46", "wlbuilds_windows_47",
    "melos_tiles_walls_brick_02_brown_10", "melos_tiles_walls_brick_02_brown_11",
    "melos_tiles_walls_brick_02_brown_3", "melos_tiles_walls_brick_02_brown_2")
