local function makeWoodFloorWithKit(
    name, description, tile)

    table.insert(WastelandBuilds.Config, {
        Menu = { "Floors", "Wooden" },
        Name = name,
        Description = description,
        Tiles = { tile },
        Type = "floor",
        Xp = { Woodwork = 1 },
        RequiredSkills = { Woodwork = 1 },
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.WastelandBuildsSuppliesWoodFloor', Amount = 1 },
        },
        RequiredTools = {
            "Tag:Hammer",
        },
    })

    table.insert(WastelandBuilds.Config, {
        Menu = { "Floors", "Wooden" },
        Name = name,
        Description = description,
        Tiles = { tile },
        Type = "floor",
        Xp = { Woodwork = 1 },
        RequiredSkills = { Woodwork = 6 },
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.Plank', Amount = 1 },
            { Id = 'Base.Nails', Amount = 1 },
        },
        RequiredTools = {
            "Tag:Hammer",
            { "Base.Saw", "Base.GardenSaw" },
        },
    })
end

local function makeBrickFloorWithKit(
    type, name, description, tile)

    table.insert(WastelandBuilds.Config, {
        Menu = { "Floors", "Brick", type },
        Name = name,
        Description = description,
        Tiles = { tile },
        Type = "floor",
        Xp = { Woodwork = 2 },
        RequiredSkills = { Woodwork = 4 },
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.WastelandBuildsSuppliesBrickFloor', Amount = 1 },
        },
        RequiredTools = {
            "Base.ClubHammer",
        },
    })

    table.insert(WastelandBuilds.Config, {
        Menu = { "Floors", "Brick", type },
        Name = name,
        Description = description,
        Tiles = { tile },
        Type = "floor",
        Xp = { Woodwork = 4 },
        RequiredSkills = { Woodwork = 8 },
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.WastelandBuildsBrick', Amount = 2 },
            { Id = 'Base.Sandbag', Amount = 1 },
        },
        RequiredTools = {
            "Base.ClubHammer",
            "Base.Screwdriver",
        },
    })
end

makeWoodFloorWithKit(
    "Light Brown [NS]",
    "Wide light brown planks oriented north and south",
    'floors_interior_tilesandwood_01_40')

makeWoodFloorWithKit(
    "Dark Brown [NS]",
    "Wide dark brown planks oriented north and south",
    'floors_interior_tilesandwood_01_46')

makeWoodFloorWithKit(
    "Dark Brown [WE]",
    "Wide dark brown planks oriented west and east",
    'floors_interior_tilesandwood_01_47')

makeWoodFloorWithKit(
    "Light [NS]",
    "Wide Light planks oriented north and south",
    'floors_interior_tilesandwood_01_42')

makeWoodFloorWithKit(
    "Brown [NS]",
    "Wide brown planks oriented north and south",
    'floors_interior_tilesandwood_01_43')

makeWoodFloorWithKit(
    "Brown [WE]",
    "Wide brown planks oriented west and east",
    'floors_interior_tilesandwood_01_44')

makeWoodFloorWithKit(
    "Fine Brown [NS]",
    "Fine brown planks oriented north and south",
    'floors_interior_tilesandwood_01_45')

makeBrickFloorWithKit(
    "Herringbone",
    "Grey Herringbone",
    "Grey herringbone brick floor",
    'melos_tiles_floors_01_27')

makeBrickFloorWithKit(
    "Herringbone",
    "White Herringbone",
    "White herringbone brick floor",
    'melos_tiles_floors_01_28')

makeBrickFloorWithKit(
    "Herringbone",
    "Red-Brown Herringbone",
    "Red-Brown herringbone brick floor",
    'melos_tiles_floors_01_29')

makeBrickFloorWithKit(
    "Herringbone",
    "Sandy Herringbone",
    "Sandy herringbone brick floor",
    'melos_tiles_floors_01_30')

makeBrickFloorWithKit(
    "Herringbone",
    "Red Herringbone",
    "Red herringbone brick floor",
    'melos_tiles_floors_01_31')

makeBrickFloorWithKit(
    "Rough Squaures",
    "Grey Rough Squares",
    "Grey rough squares brick floor",
    'melos_tiles_floors_01_92')

makeBrickFloorWithKit(
    "Rough Squaures",
    "Blue Rough Squares",
    "Blue rough squares brick floor",
    'melos_tiles_floors_01_94')

makeBrickFloorWithKit(
    "Small Rough Squaures",
    "Red Rough Squares",
    "Red rough squares brick floor",
    'melos_tiles_floors_01_95')

makeBrickFloorWithKit(
    "Large Rough Squaures",
    "Red Rough Squares",
    "Red rough squares brick floor",
    'melos_tiles_floors_01_74')

makeBrickFloorWithKit(
    "Large Rough Squaures",
    "Light Red Rough Squares",
    "Light Red rough squares brick floor",
    'melos_tiles_floors_01_72')

makeBrickFloorWithKit(
    "Rough Squaures",
    "Sandy Rough Squares",
    "Sandy rough squares brick floor",
    'melos_tiles_floors_01_93')

makeBrickFloorWithKit(
    "Interlocked",
    "Red Interlocked",
    "Red interlocked brick floor",
    'pert_floors_01_0')

makeBrickFloorWithKit(
    "Interlocked",
    "Dark Interlocked",
    "Dark interlocked brick floor",
    'pert_floors_01_1')

table.insert(WastelandBuilds.Config, {
    Menu = { "Floors", "Metal" },
    Name = "Rough Strips [NS]",
    Description = "Rough metal strips oriented north and south",
    Tiles = {
        'shanty_ddd_01_9',
    },
    Type = "floor",
    Xp = { MetalWelding = 1 },
    RequiredSkills = { MetalWelding = 2 },
    RequiredTraits = {},
    RequiredMaterials = {
        { Id = 'Base.SheetMetal', Amount = 1 },
        { Id = 'Base.Screws', Amount = 2 },
    },
    RequiredTools = {
        "Tag:Hammer",
        "Base.Screwdriver",
    },
})


table.insert(WastelandBuilds.Config, {
    Menu = { "Floors", "Metal" },
    Name = "Rough Strips [WE]",
    Description = "Rough metal strips oriented west and east",
    Tiles = {
        'shanty_ddd_01_8',
    },
    Type = "floor",
    Xp = { MetalWelding = 1 },
    RequiredSkills = { MetalWelding = 2 },
    RequiredTraits = {},
    RequiredMaterials = {
        { Id = 'Base.SheetMetal', Amount = 1 },
        { Id = 'Base.Screws', Amount = 2 },
    },
    RequiredTools = {
        "Tag:Hammer",
        "Base.Screwdriver",
    },
})