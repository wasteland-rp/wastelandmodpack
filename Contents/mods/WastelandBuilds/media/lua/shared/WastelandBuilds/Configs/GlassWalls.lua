
local wallVarient = 0

local function addWindowWall(tile1, tile2, tpill)
    wallVarient = wallVarient + 1
    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Glass", "Wall" },
        Name = "Glass Wall " .. wallVarient,
        Description = "A wall made from small panes of glass.",
        Tiles = {
            tile1,
            tile2,
            tpill,
        },
        Type = "wall",
        Xp = {},
        Health = 100,
        RequiredSkills = {Woodwork = 6},
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.Plank', Amount = 6 },
            { Id = 'Base.ScrapMetal', Amount = 2 },
            { Id = 'Base.Nails', Amount = 12 },
            { Id = 'Base.DuctTape', Amount = 2 },
            { Id = 'ImprovisedGlass.GlassPane', Amount = 3 },
        },
        RequiredTools = {
            "Tag:Hammer",
            "Base.Screwdriver",
            "Base.Scalpel",
            "Tag:Saw"
        },
    })
end

addWindowWall("wlbuilds_windows_152", "wlbuilds_windows_153", "melos_tiles_walls_commercial_04_3")
addWindowWall("wlbuilds_windows_154", "wlbuilds_windows_155", "melos_tiles_walls_commercial_03_3")