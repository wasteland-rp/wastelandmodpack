table.insert(WastelandBuilds.Config, {
    Menu = { "Stairs", "Wooden" },
    Name = "Log Stairs",
    Description = "A set of basic Log stairs",
    Tiles = {
        'edit_ddd_RUS_Forest Survival_01_74', 'edit_ddd_RUS_Forest Survival_01_75', 'edit_ddd_RUS_Forest Survival_01_76',
        'edit_ddd_RUS_Forest Survival_01_79', 'edit_ddd_RUS_Forest Survival_01_78', 'edit_ddd_RUS_Forest Survival_01_77',
        nil, nil
    },
    Type = "stairs",
    Xp = { Woodwork = 10 },
    Health = 1000,
    RequiredSkills = { Woodwork = 6 },
    RequiredTraits = {},
    RequiredMaterials = {
        { Id = 'Base.Log', Amount = 8 },
        { Id = 'Base.Nails', Amount = 10 },
    },
    RequiredTools = {
        "Tag:Hammer",
        "Base.Screwdriver",
        { "Base.Saw", "Base.GardenSaw" },
    },
})

table.insert(WastelandBuilds.Config, {
    Menu = { "Stairs", "Wooden" },
    Name = "Basic Stairs",
    Description = "A set of basic wooden stairs",
    Tiles = {
        'fixtures_stairs_01_19', 'fixtures_stairs_01_20', 'fixtures_stairs_01_21',
        'fixtures_stairs_01_27', 'fixtures_stairs_01_28', 'fixtures_stairs_01_29',
        'fixtures_stairs_01_30', 'fixtures_stairs_01_31'
    },
    Type = "stairs",
    Xp = { Woodwork = 10 },
    Health = 1000,
    RequiredSkills = { Woodwork = 6 },
    RequiredTraits = {},
    RequiredMaterials = {
        { Id = 'Base.Plank', Amount = 15 },
        { Id = 'Base.Nails', Amount = 10 },
    },
    RequiredTools = {
        "Tag:Hammer",
        "Base.Screwdriver",
        { "Base.Saw", "Base.GardenSaw" },
    },
})

table.insert(WastelandBuilds.Config, {
    Menu = { "Stairs", "Metal" },
    Name = "Basic Stairs",
    Description = "A set of basic metal stairs",
    Tiles = {
        'fixtures_stairs_01_3', 'fixtures_stairs_01_4', 'fixtures_stairs_01_5',
        'fixtures_stairs_01_11', 'fixtures_stairs_01_12', 'fixtures_stairs_01_13',
        'fixtures_stairs_01_14', 'fixtures_stairs_01_14'
    },
    Type = "stairs",
    Xp = { MetalWelding = 10 },
    Health = 1000,
    RequiredSkills = { MetalWelding = 6 },
    RequiredTraits = {},
    RequiredMaterials = {
        { Id = 'Base.SheetMetal', Amount = 10 },
        { Id = 'Base.Screws', Amount = 15 },
        { Id = 'Base.BlowTorch', Amount = 5 },
    },
    RequiredTools = {
        "Tag:Hammer",
        "Base.Screwdriver",
    },
})