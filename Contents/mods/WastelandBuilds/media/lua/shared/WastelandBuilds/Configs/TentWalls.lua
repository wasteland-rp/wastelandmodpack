local wallVarient = 0

local function addTentWall(tile1, tile2, tpill)
    wallVarient = wallVarient + 1
    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Tent", "Wall" },
        Name = "Tent Wall " .. wallVarient,
        Description = "A wall made from tarps.",
        Tiles = {
            tile1,
            tile2,
            tpill,
        },
        Type = "wall",
        Xp = {},
        Health = 100,
        RequiredSkills = {Woodwork = 1, PlantScavenging = 4},
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.Tarp', Amount = 2 },
            { Id = 'camping.TentPeg', Amount = 2 },
        },
        RequiredTools = {
            "Tag:Hammer",
        },
    })
end

local windowVarient = 0
local function addTentWindow(tile1, tile2, tpill)
    windowVarient = windowVarient + 1
    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Tent", "Window" },
        Name = "Tent Window Frame " .. windowVarient,
        Description = "A wall with a hole made from tarps.",
        Tiles = {
            tile1,
            tile2,
            tpill,
        },
        Type = "windowwall",
        Xp = {},
        Health = 100,
        RequiredSkills = {Woodwork = 1, PlantScavenging = 4},
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.Tarp', Amount = 2 },
            { Id = 'camping.TentPeg', Amount = 2 },
        },
        RequiredTools = {
            "Tag:Hammer",
        },
    })
end

local doorVarient = 0
local function addTentDoor(tile1, tile2, tpill)
    doorVarient = doorVarient + 1
    table.insert(WastelandBuilds.Config, {
        Menu = { "Walls", "Tent", "Door" },
        Name = "Tent Door Frame " .. doorVarient,
        Description = "A doorframe made of tarps.",
        Tiles = {
            tile1,
            tile2,
            tpill,
        },
        Type = "doorwall",
        Xp = {},
        Health = 100,
        RequiredSkills = {Woodwork = 1, PlantScavenging = 4},
        RequiredTraits = {},
        RequiredMaterials = {
            { Id = 'Base.Tarp', Amount = 2 },
            { Id = 'camping.TentPeg', Amount = 2 },
        },
        RequiredTools = {
            "Tag:Hammer",
        },
    })
end

addTentWall("location_military_tent_01_0", "location_military_tent_01_1", "location_military_tent_01_3")
addTentWall("location_military_tent_01_64", "location_military_tent_01_65", "location_military_tent_01_67")
addTentWall("walls_tents_ddd_01_0", "walls_tents_ddd_01_1", "walls_tents_ddd_01_3")
addTentWall("walls_tents_ddd_01_64", "walls_tents_ddd_01_65", "walls_tents_ddd_01_67")
addTentWall("walls_tents_Simon_MD_0", "walls_tents_Simon_MD_1", "walls_tents_Simon_MD_3")

addTentWindow("location_military_tent_01_8", "location_military_tent_01_9", "location_military_tent_01_3")
addTentWindow("location_military_tent_01_72", "location_military_tent_01_73", "location_military_tent_01_67")
addTentWindow("walls_tents_ddd_01_8", "walls_tents_ddd_01_9", "walls_tents_ddd_01_3")
addTentWindow("walls_tents_ddd_01_72", "walls_tents_ddd_01_73", "walls_tents_ddd_01_67")
addTentWindow("walls_tents_Simon_MD_8", "walls_tents_Simon_MD_9", "walls_tents_Simon_MD_3")

addTentDoor("location_military_tent_01_10", "location_military_tent_01_11", "location_military_tent_01_3")
addTentDoor("location_military_tent_01_74", "location_military_tent_01_75", "location_military_tent_01_67")
addTentDoor("walls_tents_ddd_01_10", "walls_tents_ddd_01_11", "walls_tents_ddd_01_3")
addTentDoor("walls_tents_ddd_01_74", "walls_tents_ddd_01_75", "walls_tents_ddd_01_67")
addTentDoor("walls_tents_Simon_MD_10", "walls_tents_Simon_MD_11", "walls_tents_Simon_MD_3")
