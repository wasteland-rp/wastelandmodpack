WBStoning = {}

local diggableTiles = ArrayList.new()
diggableTiles:add('blends_natural_01_5')

diggableTiles:add('blends_natural_01_16')
diggableTiles:add('blends_natural_01_21')
diggableTiles:add('blends_natural_01_22')
diggableTiles:add('blends_natural_01_23')

diggableTiles:add('blends_natural_01_32')
diggableTiles:add('blends_natural_01_37')
diggableTiles:add('blends_natural_01_38')
diggableTiles:add('blends_natural_01_39')

diggableTiles:add('blends_natural_01_48')
diggableTiles:add('blends_natural_01_53')
diggableTiles:add('blends_natural_01_54')
diggableTiles:add('blends_natural_01_55')

diggableTiles:add('blends_natural_01_64')
diggableTiles:add('blends_natural_01_69')
diggableTiles:add('blends_natural_01_70')
diggableTiles:add('blends_natural_01_71')

diggableTiles:add('blends_natural_01_80')
diggableTiles:add('blends_natural_01_85')
diggableTiles:add('blends_natural_01_86')
diggableTiles:add('blends_natural_01_87')

diggableTiles:add('floors_exterior_natural_01_8')
diggableTiles:add('floors_exterior_natural_01_9')
diggableTiles:add('floors_exterior_natural_01_10')
diggableTiles:add('floors_exterior_natural_01_11')
diggableTiles:add('floors_exterior_natural_01_12')
diggableTiles:add('floors_exterior_natural_01_16')
diggableTiles:add('floors_exterior_natural_01_17')
diggableTiles:add('floors_exterior_natural_01_18')
diggableTiles:add('floors_exterior_natural_01_19')
diggableTiles:add('floors_exterior_natural_01_20')
diggableTiles:add('floors_exterior_natural_01_21')

diggableTiles:add('greys_wasteland_tiles_cavetiles_16')
diggableTiles:add('greys_cavetiles_16')

local stoneTiles = ArrayList.new()
stoneTiles:add("d_rocks_ddd_01_14")
stoneTiles:add("d_rocks_ddd_01_18")
stoneTiles:add("d_rocks_ddd_01_16")
stoneTiles:add("d_rocks_ddd_01_17")
stoneTiles:add("d_rocks_ddd_01_27")
stoneTiles:add("d_rocks_ddd_01_30")
stoneTiles:add("d_rocks_ddd_01_29")
stoneTiles:add("d_rocks_ddd_01_36")
stoneTiles:add("d_rocks_ddd_01_39")
stoneTiles:add("d_rocks_ddd_01_38")

local function predicateNotBroken(item)
    return not item:isBroken()
end

function WBStoning.IsDiggableTile(tileName)
    return diggableTiles:contains(tileName)
end

function WBStoning.GetRandomDiggableTile()
    return diggableTiles:get(ZombRand(diggableTiles:size() - 1))
end

function WBStoning.IsMineableTile(tileName)
    return stoneTiles:contains(tileName)
end

function WBStoning.GetMineableTile(numStones)
    return stoneTiles:get(math.min(math.floor((numStones * stoneTiles:size()) / 20), stoneTiles:size() - 1))
end

function WBStoning.CanDigForStones(square, playerIdx)
    if not WBStoning.IsSuitableSquare(square) then return false end
    if not WBStoning.GetShovel(playerIdx) then return false end
    if not WBStoning.HasEnoughEndurance(playerIdx) then return false end
    if not WBStoning.IsSuitableZone(square) then return false end
    if not WBStoning.IsClear(square) then return false end

    return true
end

function WBStoning.IsSuitableSquare(square)
    local objects = square:getObjects()
    if objects:size() == 0 then return false end

    local object = objects:get(0)
    if not object then return false end

    local objectTextureName = object:getTextureName()

    if not WBStoning.IsDiggableTile(objectTextureName) then
        return false
    end

    return true
end

function WBStoning.GetShovel(playerIdx)
    local player = getSpecificPlayer(playerIdx)
    local inv = player:getInventory()
    local shovel = inv:getFirstTagEvalRecurse("TakeDirt", predicateNotBroken)
    return shovel
end

function WBStoning.HasEnoughEndurance(playerIdx)
    local player = getSpecificPlayer(playerIdx)
    if player:getStats():getEndurance() <= 0.2 then
        return false
    end
    return true
end

function WBStoning.IsSuitableZone(square)
    local zone = forageSystem.getForageZoneAt(square:getX(), square:getY())
    if not zone or zone.name ~= "DeepForest" then
        return false
    end
    return true
end

function WBStoning.IsClear(square)
    local objects = square:getObjects()
    if objects:size() > 1 then
        return false
    end
    return true
end

function WBStoning.GetStonesCount(player)
    return player:getInventory():getCountTypeRecurse("Base.Stone")
end

function WBStoning.GetStone(player)
    return player:getInventory():getFirstTypeRecurse("Base.Stone")
end

function WBStoning.GetCrushingHammer(player)
    local hammer = player:getInventory():getFirstTypeEvalRecurse("HammerStone", predicateNotBroken)
    if not hammer then
        hammer = player:getInventory():getFirstTypeEvalRecurse("ClubHammer", predicateNotBroken)
    end
    return hammer
end


function WBStoning.AddStone(square, numStones)
    if isClient() then
        sendClientCommand(getPlayer(), 'WBStoning', 'AddStone', { x = square:getX(), y = square:getY(), z = square:getZ(), numStones = numStones })
        return
    end
    local tile = WBStoning.GetMineableTile(numStones)
    print('WBStoning.AddStone: ' .. tile)
    local obj = IsoObject.new(square, tile, "MiningStone")
    obj:getModData().WB_NumStones = numStones
    square:AddTileObject(obj)
    if isServer() then
        obj:transmitCompleteItemToClients()
    end
end

function WBStoning.RemoveStone(square)
    if isClient() then
        sendClientCommand(getPlayer(), 'WBStoning', 'RemoveStone', { x = square:getX(), y = square:getY(), z = square:getZ() })
        return
    end
    local objects = square:getObjects()
    for i = 0, objects:size() - 1 do
        local object = objects:get(i)
        if WBStoning.IsMineableTile(object:getTextureName()) then
            square:transmitRemoveItemFromSquareOnServer(object)
            square:RemoveTileObject(object)
            break
        end
    end
end

function WBStoning.ReplaceStone(square, numStones)
    if isClient() then
        sendClientCommand(getPlayer(), 'WBStoning', 'ReplaceStone', { x = square:getX(), y = square:getY(), z = square:getZ(), numStones = numStones })
        return
    end
    WBStoning.RemoveStone(square)
    WBStoning.AddStone(square, numStones)
end

if isServer() then
    function WBStoning.OnClientCommand(module, command, player, args)
        if module ~= 'WBStoning' then return end

        if command == 'AddStone' then
            local square = getCell():getGridSquare(args.x, args.y, args.z)
            if not square then return end
            WBStoning.AddStone(square, args.numStones)
        elseif command == 'RemoveStone' then
            local square = getCell():getGridSquare(args.x, args.y, args.z)
            if not square then return end
            WBStoning.RemoveStone(square)
        elseif command == 'ReplaceStone' then
            local square = getCell():getGridSquare(args.x, args.y, args.z)
            if not square then return end
            WBStoning.ReplaceStone(square, args.numStones)
        end
    end

    if not WBStoning.initialized then
        Events.OnClientCommand.Add(WBStoning.OnClientCommand)
        WBStoning.initialized = true
    end
end