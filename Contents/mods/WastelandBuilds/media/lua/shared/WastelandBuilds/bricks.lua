function WastelandBuildsOnExtractBrick()
    local player = getPlayer()
    if ZombRand(4) == 0 then
        player:Say("The mold broke and is no longer usable.")
    else
        player:getInventory():AddItem("Base.WastelandBuildsBrickMoldEmpty", 1)
    end
end

function WastelandBuildsOnExtractMetalBrick()
    local player = getPlayer()
    if ZombRand(10) == 0 then
        player:Say("The mold broke and is no longer usable.")
    else
        player:getInventory():AddItem("Base.WastelandBuildsMetalBrickMoldEmpty", 1)
    end
end