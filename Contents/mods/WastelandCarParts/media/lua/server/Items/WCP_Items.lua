require 'Items/ProceduralDistributions'

local function registerAsLoot(item, chance, allocation)
  table.insert(ProceduralDistributions.list[allocation].items, item);
  table.insert(ProceduralDistributions.list[allocation].items, chance);
end

registerAsLoot("Base.SuspensionSpring", 2, "CrateMechanics");
registerAsLoot("Base.SuspensionSpring", 4, "CrateMetalwork");
registerAsLoot("Base.SuspensionSpring", 1, "GarageMechanics");
registerAsLoot("Base.SuspensionSpring", 4, "GarageMetalwork");
registerAsLoot("Base.SuspensionSpring", 10, "MechanicShelfSuspension");
registerAsLoot("Base.SuspensionSpring", 1, "MechanicSpecial");
registerAsLoot("Base.SuspensionSpring", 4, "StoreShelfMechanics");
registerAsLoot("Base.SuspensionSpring", 0.1, "CrateRandomJunk");
