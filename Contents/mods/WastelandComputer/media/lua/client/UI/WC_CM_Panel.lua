---
--- WC_CM_Panel.lua
--- 13/10/2024
---

require "GravyUI_WL"
require "WC_CM_Panel_Home"
require "WC_CM_Shared_Output"
require "WC_CM_Shared_UIComponents"
require "WC_CM_Shared_Styling"

WC_CM_Panel = ISPanel:derive("WC_CM_Panel")

local FONT_HGT_SMALL = getTextManager():getFontHeight(UIFont.Small)
local FONT_HGT_LARGE = getTextManager():getFontHeight(UIFont.Large)

local COLOR_GREEN = {r=0.5, g=0.8, b=0.5, a=1}
local COLOR_DARKERGREEN = {r=0.3, g=0.7, b=0.3, a=1}
local COLOR_LIGHTERGREEN = {r=0.7, g=0.9, b=0.7, a=1}
local COLOR_BLACK = {r=0,g=0,b=0,a=1}
local COLOR_GREY = {r=0.5,g=0.5,b=0.5,a=1}
local COLOR_LIGHTERGREY = {r=0.7,g=0.7,b=0.7,a=1}

function WC_CM_Panel.display(player, computer, disk, title, outputMessage)
    if WC_CM_Panel.instance then
        WC_CM_Panel.instance:onClose()
    end
    WC_CM_Panel.instance = WC_CM_Panel:new(player, computer, disk, title, outputMessage)
    WC_CM_Panel.instance:addToUIManager()
end

function WC_CM_Panel:new(player, computer, disk, title, outputMessage)
    local scale = FONT_HGT_SMALL / 12
    local w = 600 * scale
    local h = 500 * scale
    local o = ISPanel:new(getCore():getScreenWidth()/2-w/2,getCore():getScreenHeight()/2-h/2, w, h)
    setmetatable(o, self)
    self.__index = self
    o.character = player
    o.computer = computer:getSquare()
    o.computerObject = computer
    o.disk = disk
    if title then
        o.title = title
    end
    if outputMessage then
        o.outputMessage = outputMessage
    end
    o.skill = player:getPerkLevel(Perks.Electricity)
    o:initialise()
    return o
end

function WC_CM_Panel:initialise()
    ISPanel.initialise(self)
    self.moveWithMouse = true

    local win = GravyUI.Node(self.width, self.height, self)
    win = win:pad(20, 20, 20, 20)

    WC_CM_Shared_Styling.initializePanel(self, "media/textures/ui/WC_CM_Panel.png", "media/textures/ui/WC_CM_Panel_Overlay.png")

    local modData = self.disk:getModData()
	local modDataKey = "ComputerNoteModData"
    local disk = modData[modDataKey]
    
    local dividerArea = win:rows(1,10)

    local title, buttons = win:rows({0.2, 0.8}, 10)
    local titleDivider, subtitleDivider = title:cols({1, 1}, 10)
    local closeButtonNode = win:corner("topRight", FONT_HGT_SMALL + 3, FONT_HGT_SMALL + 3)
    local apTitle, apDividier, apSubtitle, apSubtitleDivider = title:rows({0.25, 0.25, 0.25, 0.25}, 10)
    local leftDivider, middleDivider, rightDivider = buttons:cols({0.10, 0.35, 0.55}, 10)
    WC_CM_Panel.buttonRow = middleDivider:rows({FONT_HGT_LARGE * 15, FONT_HGT_LARGE}, 20)

    local messageStartX = self.width * 0.5
    self.outputElements = WC_CM_Shared_Output.initializeOutputElements(self, messageStartX, COLOR_LIGHTERGREEN)
    
    WC_CM_Shared_Styling.initializeTitleAndSubtitle(self, apTitle, apDividier, apSubtitle, apSubtitleDivider, self.title, "A device in which to have direct control over inserted media")
    
    local _, lowestRow = dividerArea:rows(
        {dividerArea.height - FONT_HGT_LARGE - 20,  FONT_HGT_LARGE }, 20)
    local bottomDivider, _, _ = lowestRow:cols(1,20)
    self.bottomDivider = bottomDivider:makeLabel(dividerText, UIFont.Large, COLOR_GREEN, "left")

    local _, aboveLowest = dividerArea:rows(
        {dividerArea.height - FONT_HGT_LARGE - 40,  FONT_HGT_LARGE }, 20)
    local aboveBottomDivider, _, _ = aboveLowest:cols(1,20)
    WC_CM_Panel.leftSpacer, WC_CM_Panel.leftBottom, WC_CM_Panel.middleBottom, WC_CM_Panel.rightBottom, WC_CM_Panel.rightSpacer = aboveBottomDivider:cols({0.03, 0.1, 0.27, 0.1, 0.6}, 10)
    self.middleBottom:makeLabel("Disk Controls", UIFont.Medium, COLOR_GREEN, "center")

    WC_CM_Shared_Output.loadOutputFromComputer(self.computerObject, self)
    if self.outputMessage and self.outputMessage ~= "new" then 
        WC_CM_Shared_Output.addOutputMessage(self.outputMessage, self, self.computerObject)
    elseif self.outputMessage == "new" and self.title then
        WC_CM_Shared_Output.addOutputMessage("Disk Loaded: " .. self.title .. ".", self, self.computerObject)
    end

    WC_CM_Shared_Styling.changeButton(closeButtonNode:makeButton("X", self, self.onClose))
    self:updateState()
end

function WC_CM_Panel:typeText(targetLabel, fullText, speed)
    local currentIndex = 1
    local typingTimer = nil

    local function typeNextCharacter()
        if currentIndex <= #fullText then
            local currentText = fullText:sub(1, currentIndex)
            targetLabel:setName(currentText)
            currentIndex = currentIndex + 1
        else
            Events.OnTick.Remove(typingTimer)
        end
    end

    typingTimer = function()
        typeNextCharacter()
    end

    Events.OnTick.Add(typingTimer)
end

function WC_CM_Panel:loadButtons(character, disk)
    if self.elementsToClear then
        for _, element in ipairs(self.elementsToClear) do
            if element then
                self:removeChild(element)
            else
                print("Warning: Tried to remove a nil or invalid element.")
            end
        end
    end
    self.elementsToClear = {}

    local maxRows = 10
    local rowsUsed = 0
    local modData = disk

    local buttonList = {
        {name = "ACCESS DISK", partname = "accessButton", action = self.onOpen, condition = nil, remain = true, blockonInfected = true, blockonEmpty = false, elecskill = nil, toolTip1 = "Disk is Corrupted.", toolTip2 = nil},
        {name = "BRUTE FORCE DISK", partname = "bruteButton", action = self.onBrute, condition = modData.password and modData.title, remain = true, blockonInfected = true, blockonEmpty = true, elecskill = 7, toolTip1 = "Disk is Not Encrypted.", toolTip2 = "Not High Enough Electrical skill: " .. self.skill .. "/7."},
        {name = "CLONE DISK", partname = "cloneButton", action = self.onClone, condition = modData.title and WC_CM_Panel.checkDisk(character), remain = true, blockonInfected = true, blockonEmpty = true, elecskill = nil, toolTip1 = "No Empty Disk Found", toolTip2 = "Cannot Copy Data From Infected disk"},
        {name = "DETECT VIRUS ON DISK", partname = "scanButton", action = self.onScan, condition = modData.detected == nil, remain = true, blockonInfected = true, blockonEmpty = false, elecskill = 4, toolTip1 = "Detection Already Run. Disk Clean.", toolTip2 = "Not High Enough Electrical Skill: " .. self.skill .. "/4."},
        {name = "ERASE DATA ON DISK", partname = "eraseButton", action = self.onErase, condition = modData.title or modData.infected, remain = false, blockonInfected = nil, blockonEmpty = false, elecskill = nil, toolTip1 = nil, toolTip2 = nil},
        {name = "ENCRYPT DATA ON DISK", partname = "encryptButton", action = self.onEncrypt, condition = modData.title and modData.password == nil, remain = true, blockonInfected = true, blockonEmpty = true, elecskill = nil, toolTip1 = "Disk Already Encrypted.", toolTip2 = nil},
        {name = "DECRYPT DATA ON DISK", partname = "decryptButton", action = self.onDecrypt, condition = modData.password, remain = false, blockonInfected = true, blockonEmpty = false, elecskill = nil, toolTip1 = nil, toolTip2 = nil},
        {name = "RECOVER DATA FROM DISK", partname = "recoverButton", action = self.onRecover, condition = modData.infected, remain = true, blockonInfected = false, blockonEmpty = false, elecskill = 10, toolTip1 = "Disk Not Corrupted.", toolTip2 = "Not High Enough Electrical Skill: " .. self.skill .. "/10."},
        {name = "REMOVE VIRUS FROM DISK", partname = "uninfectButton", action = self.onUninfect, condition = modData.detected and modData.virus, remain = true, blockonInfected = true, blockonEmpty = false, elecskill = 7, toolTip1 = "No Virus Detected on Disk.", toolTip2 = "Not High Enough Electrical Skill: " .. self.skill .. "/7."},
        {name = "WRITE VIRUS ON DISK", partname = "infectButton", action = self.onInfect, condition = modData.virus == nil and modData.detected, remain = true, blockonInfected = true, blockonEmpty = false, elecskill = 10, toolTip1 = "Virus Detection Not Run.", toolTip2 = "Not High Enough Electrical Skill: " .. self.skill .. "/10."}
    }

    local visibleButtons = {}
    for _, button in ipairs(buttonList) do
        if button.condition or button.remain then
            table.insert(visibleButtons, button)
        end
    end

    if not self.buttonRow then
        print("Error: buttonRow is not defined")
        return
    end

    local buttonRows = {self.buttonRow:rows(maxRows, 10)}
    local topButtonRow = 10 - #visibleButtons
    local totalRows = #buttonRows

    for i, button in ipairs(visibleButtons) do
        if rowsUsed >= maxRows then
            break
        end

        local rowIndex = i + topButtonRow

        if rowIndex > totalRows or rowIndex < 1 then
            print("Error: rowIndex out of bounds for button: " .. button.name)
        end

        local rowNode = buttonRows [rowIndex]
        if not rowNode then
            print("Error: Failed to create row for button: " .. button.name)
        end

        rowsUsed = rowsUsed + 1

        if type(rowNode) ~= "table" then
            print("Error: rowNode is not a table. Got type:", type(rowNode))
        end

        if rowNode then
            local buttonElement = rowNode:makeButton(button.name, self, button.action)
            table.insert(self.elementsToClear, buttonElement)

            if button.condition then
                if button.blockonInfected and modData.infected ~= nil then
                    WC_CM_Shared_Styling.changeButtonGrey(buttonElement, "Option Unavailable Due to Disk Being Corrupted.", self)
                elseif not button.toolTip1 and not button.toolTip2 then
                    WC_CM_Shared_Styling.changeButton(buttonElement)
                elseif button.elecskill and button.elecskill < self.skill then
                    WC_CM_Shared_Styling.changeButton(buttonElement)
                elseif button.elecskill and button.elecskill > self.skill then
                    WC_CM_Shared_Styling.changeButtonGrey(buttonElement, button.toolTip2, self)
                else
                    WC_CM_Shared_Styling.changeButton(buttonElement)
                end
            else
                if button.blockonInfected and modData.infected ~= nil then
                    WC_CM_Shared_Styling.changeButtonGrey(buttonElement, "Option Unavailable Due to Disk Being Corrupted.", self)
                elseif not button.toolTip1 and not button.toolTip2 then
                    WC_CM_Shared_Styling.changeButton(buttonElement)
                elseif button.elecskill and button.elecskill > self.skill then
                    WC_CM_Shared_Styling.changeButtonGrey(buttonElement, button.toolTip2, self)
                elseif button.elecskill and button.elecskill < self.skill then
                    WC_CM_Shared_Styling.changeButtonGrey(buttonElement, button.toolTip1, self)
                elseif not button.elecskill and not button.blockonEmpty then
                    WC_CM_Shared_Styling.changeButton(buttonElement)
                elseif not modData.title and button.blockonEmpty then
                    WC_CM_Shared_Styling.changeButtonGrey(buttonElement, "No Disk Loaded.", self)
                elseif button.blockonEmpty then
                    WC_CM_Shared_Styling.changeButtonGrey(buttonElement, button.toolTip1, self)
                else
                    WC_CM_Shared_Styling.changeButtonGrey(buttonElement, button.toolTip1, self)
                end
            end

        end

    end

    for j = rowsUsed + 1, maxRows do
        local rowNode = buttonRows[j]
        if rowNode then
            table.insert(self.elementsToClear, rowNode)
        end
    end
    local prevButtonLoaded = false

    local prevButton = self.leftBottom:makeButton("Previous", self, function() 
        WC_CM_Shared_Output.saveOutputToComputer(self.computerObject, self)
        WC_CM_Panel_Home.display(self.character, self.computerObject, self.disk)
        self:onClose()
    end)
    WC_CM_Shared_Styling.changeButton(prevButton)
end

local function eraseDiskData(playerObj, disk, panel)
	local modData = disk:getModData()
	local modDataKey = "ComputerNoteModData"

	disk:setName("Floppy Disk")

	if modData[modDataKey] then
		modData[modDataKey] = {
			title = nil,
			text = nil,
			author = nil,
			infected = nil,
			virus = modData[modDataKey].virus,
			detected = nil,
			password = nil
		}
        WC_CM_Shared_Output.addOutputMessage("Disk Data Erased.", panel, panel.computerObject)
	else
	end
end

local function texturePopUp(modal)
    modal.backgroundColor = {r=0, g=0, b=0, a=0.9}
    modal.borderColor = {r=0, g=0, b=0, a=1}

    if modal.yes and modal.no then
        modal.yes:setFont(UIFont.Medium)
        modal.no:setFont(UIFont.Medium)

        modal.yes.backgroundColor = {r=0.3, g=0.7, b=0.3, a=1}
        modal.no.backgroundColor = {r=0.3, g=0.7, b=0.3, a=1}

        modal.yes.backgroundColorMouseOver = {r=0.7, g=0.9, b=0.7, a=1}
        modal.no.backgroundColorMouseOver = {r=0.7, g=0.9, b=0.7, a=1}

        modal.yes.textColor = {r=0, g=0, b=0, a=1}
        modal.no.textColor = {r=0, g=0, b=0, a=1}
    elseif modal.ok then
        modal.ok:setFont(UIFont.Medium)
        modal.ok.backgroundColor = {r=0.3, g=0.7, b=0.3, a=1}
        modal.ok.backgroundColorMouseOver = {r=0.7, g=0.9, b=0.7, a=1}
        modal.ok.textColor = {r=0, g=0, b=0, a=1}
    end
end

local function promptInput(decision, playerObj, disk, panel)
	local scale = getTextManager():getFontHeight(UIFont.Small) / 14
	local width = 300 * scale
	local height = 100 * scale
	local x = (getCore():getScreenWidth() / 2) - (width / 2)
	local y = (getCore():getScreenHeight() / 2) - (height / 2)
	local chosenDisk = disk:getModData()["ComputerNoteModData"].title
	if disk.title == nil then
		chosenDisk = "Floppy Disk"
	end
	if decision == "delete" then
		local modal = ISModalDialog:new(x, y, width, height, "", true, nil,
		function(_, button)
			if button.internal == "YES" then
				eraseDiskData(playerObj, disk, panel)
                if panel then
                    WC_CM_Shared_Output.addOutputMessage("Disk Data Erased.", panel, panel.computerObject)
                    panel:updateState()
                end
			end
		end)

        local originalPrerender = modal.prerender
		modal.prerender = function(self)
	
			originalPrerender(self)
	
			local font = UIFont.Small
			local textColor = {r = 0.5, g = 1.0, b = 0.5, a = 1.0}
			local fontHgt = getTextManager():getFontHeight(font)
			local yPos = (self:getHeight() / 2.5) - fontHgt
			self:drawTextCentre("Are you sure you want to erase the disk: " .. chosenDisk .. "?", self:getWidth() / 2, yPos, textColor.r, textColor.g, textColor.b, textColor.a, font)
		end

		modal:initialise()
        texturePopUp(modal)
		modal.moveWithMouse = true
		modal:addToUIManager()

		local originalDestroy = modal.destroy
		modal.destroy = function(self)
			originalDestroy(self)
		end
	elseif decision == "virus" then
		local modal = ISModalDialog:new(x, y, width, height, "", true, nil,
		function(_, button)
            if button.internal == "YES" then
                local modData = disk:getModData()["ComputerNoteModData"]
                modData.virus = true
                modData.detected = nil
                if panel then
                    panel:updateState()
                    WC_CM_Shared_Output.addOutputMessage("Disk Infected.", panel, panel.computerObject)
                end
            end
		end)

        local originalPrerender = modal.prerender
		modal.prerender = function(self)
	
			originalPrerender(self)
	
			local font = UIFont.Small
			local textColor = {r = 0.5, g = 1.0, b = 0.5, a = 1.0}
			local fontHgt = getTextManager():getFontHeight(font)
			local yPos = (self:getHeight() / 2.5) - fontHgt
			self:drawTextCentre("Are you sure you want to infect the disk: " .. chosenDisk .. "?", self:getWidth() / 2, yPos, textColor.r, textColor.g, textColor.b, textColor.a, font)
		end

		modal:initialise()
        texturePopUp(modal)
		modal.moveWithMouse = true
		modal:addToUIManager()

		local originalDestroy = modal.destroy
		modal.destroy = function(self)
			originalDestroy(self)
		end
	end
end

local function textureModal(modal)
    modal.backgroundColor = {r=0,g=0,b=0,a=0.9}
    modal.borderColor = {r=0,g=0,b=0,a=1}
    modal.entry.borderColor = {r=0,g=0,b=0,a=1}
    modal.entry:setOnlyNumbers(false)
    modal.yes:setFont(UIFont.Medium)
    modal.no:setFont(UIFont.Medium)
    modal.yes.backgroundColor = {r=0.3, g=0.7, b=0.3, a=1}
    modal.no.backgroundColor = {r=0.3, g=0.7, b=0.3, a=1}
    modal.yes.backgroundColorMouseOver = {r=0.7, g=0.9, b=0.7, a=1}
    modal.no.backgroundColorMouseOver = {r=0.7, g=0.9, b=0.7, a=1}
    modal.yes.textColor = {r=0, g=0, b=0, a=1}
    modal.no.textColor = {r=0, g=0, b=0, a=1}

	modal.entry:setOnlyNumbers(false)
end

local function encryptDiskData(playerObj, disk, panel)
	local modData = disk:getModData()
	local modDataKey = "ComputerNoteModData"
	local scale = getTextManager():getFontHeight(UIFont.Small) / 14
	local width = 220 * scale
	local height = 130 * scale
	local x = (getCore():getScreenWidth() / 2) - (width / 2)
	local y = (getCore():getScreenHeight() / 2) - (height / 2)
	local modal = ISTextBox:new(x, y, width, height, "", "", nil, function (_, button)
		if button.internal == "OK" then
			local password = button.target.entry:getText()
            if string.len(password) > 150 then
                if panel then
                    WC_CM_Shared_Output.addOutputMessage("Password Too Long.", panel, panel.computerObject)
                end   
			elseif password and password ~= "" then
                local modal2 = ISTextBox:new(x, y, width, height, "", "", nil, function (_, button)
                    if button.internal == "OK" then
                        local repeatPassword = button.target.entry:getText()
                        if string.len(repeatPassword) > 150 then
                            if panel then
                                WC_CM_Shared_Output.addOutputMessage("Password Too Long.", panel, panel.computerObject)
                            end   
                        elseif repeatPassword == password and repeatPassword ~= "" then
                            modData[modDataKey].password = password
                            if panel then
                                panel:updateState()
                                WC_CM_Shared_Output.addOutputMessage("Disk Encrypted.", panel, panel.computerObject)
                            end
                        else
                            if panel then
                                WC_CM_Shared_Output.addOutputMessage("Passwords Do Not Match.", panel, panel.computerObject)
                            end
                        end
                    end
                end, nil)
                local originalPrerender = modal2.prerender
                modal2.prerender = function(self)

                    originalPrerender(self)

                    local font = UIFont.Small
                    local textColor = {r = 0.5, g = 1.0, b = 0.5, a = 1.0}
                    local fontHgt = getTextManager():getFontFromEnum(font):getLineHeight()
                    self:drawTextCentre("Repeat Password to Encrypt Disk:", self:getWidth() / 2, self.entry:getY() - 8 - fontHgt, textColor.r, textColor.g, textColor.b, textColor.a, font)
                end
                modal2:initialise()

                textureModal(modal2)
                modal2:addToUIManager()
			else
                if panel then
                    WC_CM_Shared_Output.addOutputMessage("Password Cannot be Empty.", panel, panel.computerObject)
                end
			end
		end
	end, nil)
    local originalPrerender = modal.prerender
    modal.prerender = function(self)

        originalPrerender(self)

        local font = UIFont.Small
        local textColor = {r = 0.5, g = 1.0, b = 0.5, a = 1.0}
        local fontHgt = getTextManager():getFontFromEnum(font):getLineHeight()
        self:drawTextCentre("Enter Password to Encrypt Disk:", self:getWidth() / 2, self.entry:getY() - 8 - fontHgt, textColor.r, textColor.g, textColor.b, textColor.a, font)
    end

	modal:initialise()

    textureModal(modal)
	modal:addToUIManager()
end

local function getDiskPassword(playerObj, disk, callback)
	local scale = getTextManager():getFontHeight(UIFont.Small) / 14
	local width = 220 * scale
	local height = 130 * scale
	local x = (getCore():getScreenWidth() / 2) - (width / 2)
	local y = (getCore():getScreenHeight() / 2) - (height / 2)
	local modal
	local success
    local modData = disk:getModData()["ComputerNoteModData"]
	modal = ISTextBox:new(x, y, width, height, "", "", nil, function (_, button)
		if button.internal == "OK" then
			local enteredPassword = button.target.entry:getText()
			if enteredPassword == modData.password then
				modal:removeFromUIManager()
				success = true
				callback(success)
			else
				modal:removeFromUIManager()
				success = false
				callback(success)
			end
		else
			modal:removeFromUIManager()
			success = false
			callback(success)
		end
	end, nil)

    local originalPrerender = modal.prerender
    modal.prerender = function(self)

        originalPrerender(self)

        local font = UIFont.Small
        local textColor = {r = 0.5, g = 1.0, b = 0.5, a = 1.0}
        local fontHgt = getTextManager():getFontFromEnum(font):getLineHeight()
        self:drawTextCentre("Enter Password to Access the Disk:", self:getWidth() / 2, self.entry:getY() - 8 - fontHgt, textColor.r, textColor.g, textColor.b, textColor.a, font)
    end

	modal:initialise()

    modal.backgroundColor = {r=0,g=0,b=0,a=0.9}
    modal.borderColor = {r=0,g=0,b=0,a=1}
    modal.entry.borderColor = {r=0,g=0,b=0,a=1}
    modal.entry:setOnlyNumbers(false)
    modal.yes:setFont(UIFont.Medium)
    modal.no:setFont(UIFont.Medium)
    modal.yes.backgroundColor = {r=0.3, g=0.7, b=0.3, a=1}
    modal.no.backgroundColor = {r=0.3, g=0.7, b=0.3, a=1}
    modal.yes.backgroundColorMouseOver = {r=0.7, g=0.9, b=0.7, a=1}
    modal.no.backgroundColorMouseOver = {r=0.7, g=0.9, b=0.7, a=1}
    modal.yes.textColor = {r=0, g=0, b=0, a=1}
    modal.no.textColor = {r=0, g=0, b=0, a=1}

	modal.entry:setOnlyNumbers(false)
	modal:addToUIManager()
end

function WC_CM_Panel:onOpen()
    if luautils.walkAdj(self.character, self.computer) then
        ISWorldObjectContextMenu.equip(self.character, self.character:getPrimaryHandItem(), self.disk, true)
        ISTimedActionQueue.add(WC_OpenDiskAction:new(self.character, self.computerObject, self.disk, self, function() 
            if self.disk:getModData()["ComputerNoteModData"].virus then
                WC_CM_Shared_Output.addOutputMessage("Undetected Virus Ran. Disks Corrupted.", self, self.computerObject)
            end
            self:onClose() 
            end))
    end
end

function WC_CM_Panel:onBrute()
    if luautils.walkAdj(self.character, self.computer) then
        ISWorldObjectContextMenu.equip(self.character, self.character:getPrimaryHandItem(), self.disk, true)
        ISTimedActionQueue.add(WC_DecryptAction:new(self.character, self.computerObject, self.disk, self))
    end
end

function WC_CM_Panel:onRecover()
    if luautils.walkAdj(self.character, self.computer) then
        ISWorldObjectContextMenu.equip(self.character, self.character:getPrimaryHandItem(), self.disk, true)
        ISTimedActionQueue.add(WC_RecoverAction:new(self.character, self.computerObject, self.disk, self))
    end
end

function WC_CM_Panel:onClone()
local emptyDisk = self.character:getInventory():getFirstTypeEvalRecurse("Base.WC_CM_FloppyDisk", function(emptyDisk)
        local data = emptyDisk:getModData()["ComputerNoteModData"]
        return data.title == nil and data.text == nil
    end)
    if emptyDisk and not self.disk:getModData().infected then
        local modData = self.disk:getModData()["ComputerNoteModData"]
        local newModData = emptyDisk:getModData()["ComputerNoteModData"]
        newModData.title = modData.title
        newModData.text = modData.text
        newModData.author = modData.author
        newModData.password = modData.password
        if modData.infected then
            newModData.infected = true
            emptyDisk:setName("Corrupted: " .. newModData.title)
        else
            newModData.virus = nil
            emptyDisk:setName("Disk: " .. newModData.title)
        end
        WC_CM_Shared_Output.addOutputMessage("Disk Data Copied.", self, self.computerObject)
    end
    self:updateState()
end

function WC_CM_Panel:onScan()
    local modData = self.disk:getModData()["ComputerNoteModData"]
    modData.detected = true
    if modData.virus then
        WC_CM_Shared_Output.addOutputMessage("Virus Detected.", self, self.computerObject)
        if modData.title == nil then
            self.disk:setName("Virus: Floppy Disk")
        else
            self.disk:setName("Virus: " .. modData.title)
        end
    else
        WC_CM_Shared_Output.addOutputMessage("No Virus Detected.", self, self.computerObject)
    end
    self:updateState()
end

function WC_CM_Panel:onErase()
    promptInput("delete", self.character, self.disk, self)
end

function WC_CM_Panel:onEncrypt()
    encryptDiskData(self.character, self.disk, self)
end

function WC_CM_Panel:onDecrypt()
    getDiskPassword(self.character, self.disk, function(success)
        if success then
            self.disk:getModData()["ComputerNoteModData"].password = nil
            WC_CM_Shared_Output.addOutputMessage("Disk Decrypted.", self, self.computerObject)
            self:updateState()
        else
            WC_CM_Shared_Output.addOutputMessage("Failed to Decrypt Disk.", self, self.computerObject)
        end
    end)
end

function WC_CM_Panel:onInfect()
    promptInput("virus", self.character, self.disk, self)
end

function WC_CM_Panel:onUninfect()
    ISWorldObjectContextMenu.equip(self.character, self.character:getPrimaryHandItem(), self.disk, true)
    ISTimedActionQueue.add(WC_RemoveVirusAction:new(self.character, self.computerObject, self.disk, self))
end

function WC_CM_Panel:updateState()
    if not self.disk or not self.disk:getModData() or not self.disk:getModData()["ComputerNoteModData"] then
        print("Error: Disk is invalid or missing mod data")
        return
    end

    local diskData = self.disk:getModData()["ComputerNoteModData"]
    self:loadButtons(self.character, diskData)
end


function WC_CM_Panel:prerender()
    ISPanel.prerender(self)
    local computerModData = self.computer and self.computer:getModData() and (self.computer:getModData()["ComputerModData"] or nil) or nil
    local computerName = computerModData and computerModData.name or "Unknown"
    if self.titleLabel then
        self.titleLabel.name = "Computer: " .. computerName
    end
    local px = self.character:getX()
    local py = self.character:getY()
    local cx = self.computer:getX()
    local cy = self.computer:getY()
    local dx = px - cx
    local dy = py - cy
    if dx * dx + dy * dy > 8 then -- more than two tiles away
      self:onClose()
    end
end

function WC_CM_Panel:render()
    ISPanel.render(self)
    self:drawTextureScaled(self.texture, 0, 0, self.width, self.height, 0.2, 0, 0, 0)
    local moveRight = 38
    local moveDown = 45
    local sizeIncrease = 0.39
    local sizeDecrease = sizeIncrease/2
    local squishFactor = 0.9
    self:drawTextureScaled(self.computerTexture, moveRight + (0 - (self.width * sizeDecrease)), moveDown + (0 - (self.height * sizeDecrease)), self.width * (1 + sizeIncrease), self.height * (1 + sizeIncrease) * squishFactor, 1, 0.85, 0.85, 0.85)
end

function WC_CM_Panel:onClose()
    WC_CM_Shared_Output.saveOutputToComputer(self.computerObject, self)
    self:removeFromUIManager()
end

function WC_CM_Panel:removeFromUIManager()
    ISPanel.removeFromUIManager(self)
    WC_CM_Panel.instance = nil
end

function WC_CM_Panel.checkDisk(player)
    local inventory = player:getInventory()
    local newDisk = inventory:getFirstTypeEvalRecurse("Base.WC_CM_FloppyDisk", function(newDisk)
        local data = newDisk:getModData()["ComputerNoteModData"]
        return data.title == nil and data.txt == nil
        end)
    if newDisk == nil then
        return false
    else
        return true
    end
    return data ~= nil
end