---
--- WC_CM_Panel_Home.lua
--- 15/10/2024
---

require "GravyUI_WL"
require "WC_CM_Panel"
require "WC_CM_Shared_Output"
require "WC_CM_Shared_UIComponents"
require "WC_CM_Shared_Styling"

WC_CM_Panel_Home = ISPanel:derive("WC_CM_Panel_Home")

local FONT_HGT_CODE = getTextManager():getFontHeight(UIFont.Code)
local FONT_HGT_SMALL = getTextManager():getFontHeight(UIFont.Small)
local FONT_HGT_MEDIUM = getTextManager():getFontHeight(UIFont.Medium)
local FONT_HGT_LARGE = getTextManager():getFontHeight(UIFont.Large)
local FONT_HGT_MASSIVE = getTextManager():getFontHeight(UIFont.Massive)

local COLOR_GREEN = {r=0.5, g=0.8, b=0.5, a=1}
local COLOR_DARKERGREEN = {r=0.3, g=0.7, b=0.3, a=1}
local COLOR_LIGHTERGREEN = {r=0.7, g=0.9, b=0.7, a=1}
local COLOR_BLACK = {r=0,g=0,b=0,a=1}
local COLOR_GREY = {r=0.5,g=0.5,b=0.5,a=1}
local COLOR_LIGHTERGREY = {r=0.7,g=0.7,b=0.7,a=1}

function WC_CM_Panel_Home.display(player, computer, disk)
    if WC_CM_Panel_Home.instance then
        WC_CM_Panel_Home.instance:onClose()
    end
    WC_CM_Panel_Home.instance = WC_CM_Panel_Home:new(player, computer, disk)
    WC_CM_Panel_Home.instance:addToUIManager()
end

function WC_CM_Panel_Home:new(player, computer, disk)
    local scale = FONT_HGT_SMALL / 12
    local w = 600 * scale
    local h = 500 * scale
    local o = ISPanel:new(getCore():getScreenWidth()/2-w/2,getCore():getScreenHeight()/2-h/2, w, h)
    setmetatable(o, self)
    self.__index = self
    o.character = player
    o.computer = computer:getSquare()
    o.computerObject = computer
    o.disk = disk
    o:initialise()
    return o
end

function WC_CM_Panel_Home:initialise()
    ISPanel.initialise(self)
    self.moveWithMouse = true

    local win = GravyUI.Node(self.width, self.height, self)
    win = win:pad(20, 20, 20, 20)

    WC_CM_Shared_Styling.initializePanel(self, "media/textures/ui/WC_CM_Panel.png", "media/textures/ui/WC_CM_Panel_Overlay.png")

    local modData = self.disk:getModData()
	local modDataKey = "ComputerNoteModData"
    local disk = modData[modDataKey]
    
    local dividerArea = win:rows(1,10)

    local title, buttons = win:rows({0.2, 0.8}, 10)
    local titleDivider, subtitleDivider = title:cols({1, 1}, 10)
    local closeButtonNode = win:corner("topRight", FONT_HGT_SMALL + 3, FONT_HGT_SMALL + 3)
    local apTitle, apDividier, apSubtitle, apSubtitleDivider = title:rows({0.25, 0.25, 0.25, 0.25}, 10)
    local leftDivider, middleDivider, rightDivider = buttons:cols({0.10, 0.35, 0.55}, 10)
    WC_CM_Panel_Home.buttonRow = middleDivider:rows({FONT_HGT_LARGE * 15, FONT_HGT_LARGE}, 20)


    WC_CM_Panel_Home.currentPage = 1
    WC_CM_Panel_Home.itemsPerPage = 10

    local messageStartX = self.width * 0.5
    self.outputElements = WC_CM_Shared_Output.initializeOutputElements(self, messageStartX, COLOR_LIGHTERGREEN)
    
    WC_CM_Shared_Styling.initializeTitleAndSubtitle(self, apTitle, apDividier, apSubtitle, apSubtitleDivider, nil, "A device in which to have direct control over inserted media")
    
    local _, lowestRow = dividerArea:rows(
        {dividerArea.height - FONT_HGT_LARGE - 20,  FONT_HGT_LARGE }, 20)
    local bottomDivider, _, _ = lowestRow:cols(1,20)
    self.bottomDivider = bottomDivider:makeLabel(dividerText, UIFont.Large, COLOR_GREEN, "left")

    local _, aboveLowest = dividerArea:rows(
        {dividerArea.height - FONT_HGT_LARGE - 40,  FONT_HGT_LARGE }, 20)
    local aboveBottomDivider, _, _ = aboveLowest:cols(1,20)
    WC_CM_Panel_Home.leftSpacer, WC_CM_Panel_Home.leftBottom, WC_CM_Panel_Home.middleBottom, WC_CM_Panel_Home.rightBottom, WC_CM_Panel_Home.rightSpacer = aboveBottomDivider:cols({0.03, 0.1, 0.27, 0.1, 0.6}, 10)
    self.middleBottom:makeLabel("Disk Controls", UIFont.Medium, COLOR_GREEN, "center")

    WC_CM_Shared_Output.loadOutputFromComputer(self.computerObject, self)

    WC_CM_Shared_Styling.changeButton(closeButtonNode:makeButton("X", self, self.onClose))

    self:updateState()
end

function WC_CM_Panel_Home:typeText(targetLabel, fullText, speed)
    local currentIndex = 1
    local typingTimer = nil

    local function typeNextCharacter()
        if currentIndex <= #fullText then
            local currentText = fullText:sub(1, currentIndex)
            targetLabel:setName(currentText)
            currentIndex = currentIndex + 1
        else
            Events.OnTick.Remove(typingTimer)
        end
    end

    typingTimer = function()
        typeNextCharacter()
    end

    Events.OnTick.Add(typingTimer)
end

function WC_CM_Panel_Home:populateMenu()
    local disks = self.character:getInventory():getAllTypeRecurse("Base.WC_CM_FloppyDisk")
    local buttonList = {}
    local titleCount = {}

    if disks and not disks:isEmpty() then
        for i = 0, disks:size() - 1 do
            local disk = disks:get(i)
            local selectedDisk = disk:getModData()["ComputerNoteModData"]
            local diskTitle = selectedDisk.title or "Empty Disk"
            
            if selectedDisk.infected then
                if selectedDisk.title == nil then
                    diskTitle = "Corrupted Disk"
                else
                    diskTitle = "Corrupted: " .. selectedDisk.title
                end
            end

            if titleCount[diskTitle] then
                titleCount[diskTitle] = titleCount[diskTitle] + 1
                diskTitle = diskTitle .. " " .. titleCount[diskTitle]
            else
                titleCount[diskTitle] = 1
            end

            diskPartname = diskTitle:gsub("%s+", "")

            table.insert(buttonList, {name = diskTitle, partname = diskPartname:lower() .. "Button", action = function() 
                WC_CM_Panel.display(self.character, self.computerObject, disk, diskTitle, "new")
                self:onClose()
            end, show = true, infected = selectedDisk.infected, toolTip = nil})
        end
        table.sort(buttonList, function (a, b) return a.name:lower() < b.name:lower() end)
        self:loadButtons(self.character, buttonList)
    end
end

function WC_CM_Panel_Home:loadButtons(character, buttonList)
    if self.elementsToClear then
        for _, element in ipairs(self.elementsToClear) do
            if element then
                self:removeChild(element)
            else
                print("Warning: Tried to remove a nil or invalid element.")
            end
        end
    end
    self.elementsToClear = {}
    if not self.buttonRow then
        print("Error: buttonRow is not defined")
        return
    end

    local startIndex = (self.currentPage - 1) * self.itemsPerPage + 1
    local endIndex = math.min(startIndex + self.itemsPerPage - 1, #buttonList)

    local currentButtons = {}
    for i = startIndex, endIndex do
        if buttonList[i].show then
            table.insert(currentButtons, buttonList[i])
        end
    end

    local buttonRows = {self.buttonRow:rows(10, 10)}
    local topButtonRow = 10 - #currentButtons

    for i, button in ipairs(currentButtons) do
        local rowNode = buttonRows[i + topButtonRow]

        if not rowNode then
            print("Error: Failed to create row for button: " .. button.name)
            return
        end
        
        if button.show then
            local buttonElement = rowNode:makeButton(button.name, self, button.action)
            table.insert(self.elementsToClear, buttonElement)
            WC_CM_Shared_Styling.changeButton(buttonElement)
        end
    end
    if self.currentPage > 1 then
        local prevButton = self.leftBottom:makeButton("Previous", self, function() 
            self.currentPage = self.currentPage - 1
            self:loadButtons(character, buttonList)
        end)
        WC_CM_Shared_Styling.changeButton(prevButton)
    else
        local prevButton = self.leftBottom:makeButton("Previous", self, function() 
            WC_CM_Shared_Output.addOutputMessage("No previous page available", self, self.computerObject)
        end)
        WC_CM_Shared_Styling.changeButtonGrey(prevButton, "No previous page available", self)
    end
    if endIndex < #buttonList then
        local nextButton = self.rightBottom:makeButton("Next", self, function() 
            self.currentPage = self.currentPage + 1
            self:loadButtons(character, buttonList)
        end)
        WC_CM_Shared_Styling.changeButton(nextButton)
    else
        local nextButton = self.rightBottom:makeButton("Next", self, function() 
            WC_CM_Shared_Output.addOutputMessage("No next page available", self, self.computerObject)
        end)
        WC_CM_Shared_Styling.changeButtonGrey(nextButton, "No next page available", self)
    end
end

function WC_CM_Panel_Home:updateState()
    if not self.disk or not self.disk:getModData() or not self.disk:getModData()["ComputerNoteModData"] then
        print("Error: Disk is invalid or missing mod data")
        return
    end
    self:populateMenu()
end


function WC_CM_Panel_Home:prerender()
    ISPanel.prerender(self)
    local computerModData = self.computer and self.computer:getModData() and (self.computer:getModData()["ComputerModData"] or nil) or nil
    local computerName = computerModData and computerModData.name or "Unknown"
    if self.titleLabel then
        self.titleLabel.name = "Computer: " .. computerName
    end
    local px = self.character:getX()
    local py = self.character:getY()
    local cx = self.computer:getX()
    local cy = self.computer:getY()
    local dx = px - cx
    local dy = py - cy
    if dx * dx + dy * dy > 8 then -- more than two tiles away
      self:onClose()
    end
end

function WC_CM_Panel_Home:render()
    ISPanel.render(self)
    self:drawTextureScaled(self.texture, 0, 0, self.width, self.height, 0.2, 0, 0, 0)
    local moveRight = 38
    local moveDown = 45
    local sizeIncrease = 0.39
    local sizeDecrease = sizeIncrease/2
    local squishFactor = 0.9
    self:drawTextureScaled(self.computerTexture, moveRight + (0 - (self.width * sizeDecrease)), moveDown + (0 - (self.height * sizeDecrease)), self.width * (1 + sizeIncrease), self.height * (1 + sizeIncrease) * squishFactor, 1, 0.85, 0.85, 0.85)
end

function WC_CM_Panel_Home:onClose()
    WC_CM_Shared_Output.saveOutputToComputer(self.computerObject, self)
    self:removeFromUIManager()
end

function WC_CM_Panel_Home:removeFromUIManager()
    ISPanel.removeFromUIManager(self)
    WC_CM_Panel_Home.instance = nil
end