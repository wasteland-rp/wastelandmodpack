---
--- WC_CM_Panel_View.lua
--- 16/10/2024
---

require "GravyUI_WL"
require "WC_CM_Panel"
require "WC_CM_Shared_Output"
require "WC_CM_Shared_UIComponents"
require "WC_CM_Shared_Styling"

WC_CM_Panel_View = ISPanel:derive("WC_CM_Panel_View")

local FONT_HGT_SMALL = getTextManager():getFontHeight(UIFont.Small)
local FONT_HGT_LARGE = getTextManager():getFontHeight(UIFont.Large)

local COLOR_GREEN = {r=0.5, g=0.8, b=0.5, a=1}
local COLOR_DARKERGREEN = {r=0.3, g=0.7, b=0.3, a=1}
local COLOR_LIGHTERGREEN = {r=0.7, g=0.9, b=0.7, a=1}
local COLOR_BLACK = {r=0,g=0,b=0,a=1}

function WC_CM_Panel_View.display(player, computer, disk, title)
    if WC_CM_Panel_View.instance then
        WC_CM_Panel_View.instance:onClose()
    end
    WC_CM_Panel_View.instance = WC_CM_Panel_View:new(player, computer, disk, title)
    WC_CM_Panel_View.instance:addToUIManager()
end

function WC_CM_Panel_View:new(player, computer, disk, title)
    local scale = FONT_HGT_SMALL / 12
    local w = 600 * scale
    local h = 500 * scale
    local o = ISPanel:new(getCore():getScreenWidth()/2-w/2,getCore():getScreenHeight()/2-h/2, w, h)
    setmetatable(o, self)
    self.__index = self
    o.character = player
    o.computer = computer:getSquare()
    o.computerObject = computer
    o.disk = disk
    if title then
        o.title = title
    end
    o:initialise()
    return o
end

function WC_CM_Panel_View:initialise()
    ISPanel.initialise(self)
    self.moveWithMouse = true

    local win = GravyUI.Node(self.width, self.height, self)
    win = win:pad(20, 20, 20, 20)

    WC_CM_Shared_Styling.initializePanel(self, "media/textures/ui/WC_CM_Panel.png", "media/textures/ui/WC_CM_Panel_Overlay.png")
    
    local dividerArea = win:rows(1,10)

    local title, buttons = win:rows({0.2, 0.8}, 10)
    local titleDivider, subtitleDivider = title:cols({1, 1}, 10)
    local closeButtonNode = win:corner("topRight", FONT_HGT_SMALL + 3, FONT_HGT_SMALL + 3)
    local apTitle, apDividier, apSubtitle, apSubtitleDivider = title:rows({0.25, 0.25, 0.25, 0.25}, 10)
    WC_CM_Panel_View.middleDivider = buttons:cols(1, 10)
    WC_CM_Panel_View.buttonRow = WC_CM_Panel_View.middleDivider:rows({FONT_HGT_LARGE * 15, FONT_HGT_LARGE}, 20)
    
    WC_CM_Shared_Styling.initializeTitleAndSubtitle(self, apTitle, apDividier, apSubtitle, apSubtitleDivider, self.title, "A device in which to have direct control over inserted media")
    
    local _, lowestRow = dividerArea:rows(
        {dividerArea.height - FONT_HGT_LARGE - 20,  FONT_HGT_LARGE }, 20)
    local bottomDivider, _, _ = lowestRow:cols(1,20)
    self.bottomDivider = bottomDivider:makeLabel(dividerText, UIFont.Large, COLOR_GREEN, "left")

    local _, aboveLowest = dividerArea:rows(
        {dividerArea.height - FONT_HGT_LARGE - 40,  FONT_HGT_LARGE }, 20)
    local aboveBottomDivider, _, _ = aboveLowest:cols(1,20)
    WC_CM_Panel_View.leftSpacer, WC_CM_Panel_View.leftBottom, WC_CM_Panel_View.middleBottom, WC_CM_Panel_View.saveButton, WC_CM_Panel_View.cancelButton = aboveBottomDivider:cols({0.03, 0.1, 0.27, 0.1, 0.1}, 10)
    self.middleBottom:makeLabel("Disk Controls", UIFont.Medium, COLOR_GREEN, "center")

    WC_CM_Shared_Output.loadOutputFromComputer(self.computerObject, self)
    WC_CM_Shared_Styling.changeButton(closeButtonNode:makeButton("X", self, self.onClose))

    self:loadButtons(self.character, self.disk)
    self:AddTextBoxes()
end

function WC_CM_Panel_View:AddTextBoxes()
    local name, author, input, buttons = WC_CM_Panel_View.middleDivider:rows({FONT_HGT_LARGE, FONT_HGT_LARGE, 0.9, FONT_HGT_LARGE}, 5)
    local nameLabel, nameInput = name:cols({0.1,0.9}, 5) --10% / 95%
    local authorLabel, authorInput = author:cols({0.1,0.9}, 5)
    local bodyLabel, bodyInput = input:cols({0.1,0.9}, 5)
    local saveButton, cancelButton = buttons:cols(2, 5)

    self:addChild(nameLabel:pad(0,2,0,0):makeLabel("Title >:", UIFont.Code, COLOR_GREEN, "right"))
    self.titleBox = nameInput:makeTextBox("")
    self:changeTextBox(self.titleBox)
    self:addChild(self.titleBox)
    self.titleBox:setEditable(true)
    self.titleBox:setSelectable(true)

    self:addChild(authorLabel:pad(0,2,0,0):makeLabel("Author >:", UIFont.Code, COLOR_GREEN, "right"))
    self.authorBox = authorInput:makeTextBox("")
    self:changeTextBox(self.authorBox)
    self:addChild(self.authorBox)
    self.authorBox:setEditable(true)
    self.authorBox:setSelectable(true)

    self:addChild(bodyLabel:pad(0,2,0,0):makeLabel("Body >:", UIFont.Code, COLOR_GREEN, "right"))
    self.textBox = bodyInput:makeTextBox("")
    self:changeBigTextBox(self.textBox)
    self:addChild(self.textBox)
    self.textBox:setMultipleLine(true)
    self.textBox.javaObject:setMaxLines(20)
    self.textBox:setEditable(true)
    self.textBox:setSelectable(true)

    local disk = self.disk:getModData()["ComputerNoteModData"]
    local sb = WC_CM_Panel_View.saveButton:makeButton("Save", self, self.onSave)
    self:addChild(sb)
    WC_CM_Shared_Styling.changeButton(sb)

    local cb = WC_CM_Panel_View.cancelButton:makeButton("Cancel", self, function() 
        WC_CM_Panel.display(self.character, self.computerObject, self.disk, disk.title or "Empty Disk", "Disk Not Saved.")
        self:onClose()
    end)
    self:addChild(cb)
    WC_CM_Shared_Styling.changeButton(cb)

    self:populateFields()
end

function WC_CM_Panel_View:changeTextBox(box)
    box.backgroundColor = {r=0.0,g=0.0,b=0.0,a=0}
    local originalPrerender = box.prerender
    box.prerender = function(self)
        self.borderColor = {r=0.0,g=0.0,b=0.0,a=0}
        local lineThickness = 1
        local lineColor = COLOR_GREEN

        self:drawRect(0, self.height - lineThickness, self.width, lineThickness, lineColor.a, lineColor.r, lineColor.g, lineColor.b)
    end
end

function WC_CM_Panel_View:changeBigTextBox(box)
    box.backgroundColor = {r=0.0,g=0.0,b=0.0,a=0}
    local originalPrerender = box.prerender
    box.prerender = function(self)
        self.borderColor = {r=0.0,g=0.0,b=0.0,a=0}
        local lineThickness = 1
        local lineColor = COLOR_GREEN

        self:drawRect(0, self.height - lineThickness, self.width, lineThickness, lineColor.a, lineColor.r, lineColor.g, lineColor.b)
    end
end

function WC_CM_Panel_View:populateFields()
    local noteData = self.disk:getModData()["ComputerNoteModData"]

    if noteData then
        self.titleBox:setText(noteData.title or "")
        self.authorBox:setText(noteData.author or "")
        self.textBox:setText(noteData.text or "")
    else
        self.titleBox:setText("")
        self.authorBox:setText("")
        self.textBox:setText("")
    end
end

function WC_CM_Panel_View:promptInput()
    print("Prompting Input")
	local scale = getTextManager():getFontHeight(UIFont.Small) / 14
	local width = 300 * scale
	local height = 100 * scale
	local x = (getCore():getScreenWidth() / 2) - (width / 2)
	local y = (getCore():getScreenHeight() / 2) - (height / 2)
    local modal = ISModalDialog:new(x, y, width, height, "", false, nil)

    local originalPrerender = modal.prerender
    modal.prerender = function(self)

        originalPrerender(self)

        local font = UIFont.Small
        local textColor = {r = 0.5, g = 1.0, b = 0.5, a = 1.0}
        local fontHgt = getTextManager():getFontHeight(font)
        local yPos = (self:getHeight() / 2.5) - fontHgt
        self:drawTextCentre("Title and body cannot be empty", self:getWidth() / 2, yPos, textColor.r, textColor.g, textColor.b, textColor.a, font)
    end

    modal:initialise()
    self:texturePopUp(modal)
    modal.moveWithMouse = true
    modal:addToUIManager()

    local originalDestroy = modal.destroy
    modal.destroy = function(self)
        originalDestroy(self)
    end
end

function WC_CM_Panel_View:texturePopUp(modal)
    modal.backgroundColor = {r=0, g=0, b=0, a=0.9}
    modal.borderColor = {r=0, g=0, b=0, a=1}

    if modal.yes and modal.no then
        modal.yes:setFont(UIFont.Medium)
        modal.no:setFont(UIFont.Medium)

        modal.yes.backgroundColor = {r=0.3, g=0.7, b=0.3, a=1}
        modal.no.backgroundColor = {r=0.3, g=0.7, b=0.3, a=1}

        modal.yes.backgroundColorMouseOver = {r=0.7, g=0.9, b=0.7, a=1}
        modal.no.backgroundColorMouseOver = {r=0.7, g=0.9, b=0.7, a=1}

        modal.yes.textColor = {r=0, g=0, b=0, a=1}
        modal.no.textColor = {r=0, g=0, b=0, a=1}
    elseif modal.ok then
        modal.ok:setFont(UIFont.Medium)
        modal.ok.backgroundColor = {r=0.3, g=0.7, b=0.3, a=1}
        modal.ok.backgroundColorMouseOver = {r=0.7, g=0.9, b=0.7, a=1}
        modal.ok.textColor = {r=0, g=0, b=0, a=1}
    end
end

function WC_CM_Panel_View:onSave()
    local noteTitle = self.titleBox:getText()
    local noteText = self.textBox:getText()
    local noteAuthor = self.authorBox:getText()
    local titleLength = string.len(noteTitle)
    local textLength = string.len(noteText)
    local authorLength = string.len(noteAuthor)

    if textLength > 2000 or titleLength > 30 or authorLength > 30 then
        print("Title is too long")
        return
    end

    if noteTitle == "" or noteText == "" then
        print("Title or Text cannot be empty")
        if self.panel then
            WC_CM_Shared_Output.addOutputMessage("Title and Text Cannot Be Empty.", self, self.computerObject) --TODO: Add warning message popup
        end
        self:promptInput()
        return
    end

    local modData = self.disk:getModData()
    local modDataKey = "ComputerNoteModData"

    modData[modDataKey] = {
        title = noteTitle,
        text = noteText,
        author = noteAuthor,
        infected = modData[modDataKey].infected,
        virus = modData[modDataKey].virus,
        detected = modData[modDataKey].detected,
        password = modData[modDataKey].password
    }

    if modData[modDataKey].infected then
        self.disk:setName("Corrupted: " .. noteTitle)
    else
        self.disk:setName("Disk: " .. noteTitle)
    end

    WC_CM_Panel.display(self.character, self.computerObject, self.disk, noteTitle, "Disk Successfully Saved.")
    self:onClose()
end

function WC_CM_Panel_View:loadButtons(character, disk)
    local disk = self.disk:getModData()["ComputerNoteModData"]
    local prevButton = self.leftBottom:makeButton("Previous", self, function() 
        WC_CM_Panel.display(self.character, self.computerObject, self.disk, disk.title or "Empty Disk")
        self:onClose()
    end)
    WC_CM_Shared_Styling.changeButton(prevButton)
end

function WC_CM_Panel_View:prerender()
    ISPanel.prerender(self)
    local computerModData = self.computer and self.computer:getModData() and (self.computer:getModData()["ComputerModData"] or nil) or nil
    local computerName = computerModData and computerModData.name or "Unknown"
    if self.titleLabel then
        self.titleLabel.name = "Computer: " .. computerName
    end
    local px = self.character:getX()
    local py = self.character:getY()
    local cx = self.computer:getX()
    local cy = self.computer:getY()
    local dx = px - cx
    local dy = py - cy
    if dx * dx + dy * dy > 8 then -- more than two tiles away
      self:onClose()
    end
end

function WC_CM_Panel_View:render()
    ISPanel.render(self)
    self:drawTextureScaled(self.texture, 0, 0, self.width, self.height, 0.2, 0, 0, 0)
    local moveRight = 38
    local moveDown = 45
    local sizeIncrease = 0.39
    local sizeDecrease = sizeIncrease/2
    local squishFactor = 0.9
    self:drawTextureScaled(self.computerTexture, moveRight + (0 - (self.width * sizeDecrease)), moveDown + (0 - (self.height * sizeDecrease)), self.width * (1 + sizeIncrease), self.height * (1 + sizeIncrease) * squishFactor, 1, 0.85, 0.85, 0.85)
end

function WC_CM_Panel_View:onClose()
    self:removeFromUIManager()
end

function WC_CM_Panel_View:removeFromUIManager()
    ISPanel.removeFromUIManager(self)
    WC_CM_Panel_View.instance = nil
end