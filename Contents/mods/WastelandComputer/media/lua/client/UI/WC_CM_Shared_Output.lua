WC_CM_Shared_Output = {}
local maxMessages = 13
WC_CM_Shared_Output.outputElements = {}

function WC_CM_Shared_Output.initializeOutputElements(panel, messageStartX, color)
    WC_CM_Shared_Output.outputElements[panel] = WC_CM_Shared_Output.outputElements[panel] or {}

    local messageStartY = (500 - 370) * (getTextManager():getFontHeight(UIFont.Small) / 12)
    local messageHeight = getTextManager():getFontHeight(UIFont.Code) + 10

    for i = 1, maxMessages do
        local outputRow = ISLabel:new(
            messageStartX,
            messageStartY + ((i - 1) * messageHeight),
            getTextManager():getFontHeight(UIFont.Code),
            "", 
            color.r, color.g, color.b, color.a, UIFont.Code, true
        )
        outputRow:initialise()
        outputRow:instantiate()
        panel:addChild(outputRow)

        table.insert(WC_CM_Shared_Output.outputElements[panel], outputRow)
    end
end

function WC_CM_Shared_Output.addOutputMessage(newMessage, panel, computer)
    local maxLength = 32
    local formattedMessage = ">: " .. newMessage

    local firstPart, secondPart = WC_CM_Shared_Output.splitMessageOnLength(formattedMessage, maxLength)

    local outputElements = WC_CM_Shared_Output.outputElements[panel]
    if not outputElements then return end

    if secondPart then
        for i = 1, maxMessages - 2 do
            outputElements[i]:setName(outputElements[i + 2]:getName())
        end

        outputElements[maxMessages - 1]:setName(firstPart or "")
        outputElements[maxMessages]:setName(secondPart or "")
    else
        for i = 1, maxMessages - 1 do
            outputElements[i]:setName(outputElements[i + 1]:getName())
        end

        outputElements[maxMessages]:setName(firstPart or "")
    end

    WC_CM_Shared_Output.saveOutputToComputer(computer, panel)
end


function WC_CM_Shared_Output.saveOutputToComputer(computer, panel)
    local computerModData = computer:getModData()
    computerModData.outputMessages = {}

    local outputElements = WC_CM_Shared_Output.outputElements[panel]
    if outputElements then
        for i, outputElement in ipairs(outputElements) do
            local message = outputElement:getName()
            if message and message ~= "" then
                table.insert(computerModData.outputMessages, message)
            end
        end
    end

    computer:transmitModData()
end

function WC_CM_Shared_Output.loadOutputFromComputer(computer, panel)
    local computerModData = computer:getModData()
    local savedMessages = computerModData.outputMessages or {}

    local outputElements = WC_CM_Shared_Output.outputElements[panel]
    if not outputElements then return end

    local startIndex = math.max(#savedMessages - maxMessages + 1, 1)
    local elementIndex = maxMessages

    for i = #savedMessages, startIndex, -1 do
        outputElements[elementIndex]:setName(savedMessages[i])
        elementIndex = elementIndex - 1
    end
end

function WC_CM_Shared_Output.clearAllMessages(computer)
    local computerModData = computer:getModData()
    computerModData.outputMessages = {}
    computer:transmitModData()

    for panelID, outputElements in pairs(WC_CM_Shared_Output.outputElements) do
        for _, outputElement in ipairs(outputElements) do
            outputElement:setName("")
        end
    end
end

function WC_CM_Shared_Output.splitMessageOnLength(message, maxLength)
    if #message <= maxLength then
        return message, nil
    end

    local breakIndex = maxLength
    while breakIndex > 0 and message:sub(breakIndex, breakIndex) ~= " " do
        breakIndex = breakIndex - 1
    end

    if breakIndex == 0 then
        breakIndex = maxLength
    end

    local firstPart = message:sub(1, breakIndex)
    local remainingPart = message:sub(breakIndex + 1):gsub("^%s+", "")

    return firstPart, remainingPart
end

return WC_CM_Shared_Output
