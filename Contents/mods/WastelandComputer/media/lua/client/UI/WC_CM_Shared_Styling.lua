WC_CM_Shared_Styling = {}

local FONT_HGT_CODE = getTextManager():getFontHeight(UIFont.Code)
local FONT_HGT_SMALL = getTextManager():getFontHeight(UIFont.Small)
local FONT_HGT_MEDIUM = getTextManager():getFontHeight(UIFont.Medium)
local FONT_HGT_LARGE = getTextManager():getFontHeight(UIFont.Large)
local FONT_HGT_MASSIVE = getTextManager():getFontHeight(UIFont.Massive)

local COLOR_GREEN = {r=0.5, g=0.8, b=0.5, a=1}
local COLOR_DARKERGREEN = {r=0.3, g=0.7, b=0.3, a=1}
local COLOR_LIGHTERGREEN = {r=0.7, g=0.9, b=0.7, a=1}
local COLOR_BLACK = {r=0,g=0,b=0,a=1}
local COLOR_GREY = {r=0.5,g=0.5,b=0.5,a=1}
local COLOR_LIGHTERGREY = {r=0.7,g=0.7,b=0.7,a=1}

function WC_CM_Shared_Styling.initializePanel(panel, texturePath, computerTexturePath)
    panel.backgroundColor = {r = 0, g = 0, b = 0, a = 0.9}
    panel.borderColor = COLOR_BLACK

    panel.texture = getTexture(texturePath)
    panel.computerTexture = getTexture(computerTexturePath)
end

function WC_CM_Shared_Styling.initializeTitleAndSubtitle(panel, apTitle, apDividier, apSubtitle, apSubtitleDivider, title, subtitle)
    panel.titleLabel = apTitle:makeLabel("Wasteland Computer System", UIFont.Large, COLOR_GREEN, "left")
    panel.titleLabel.name = "Title"

    local dividerText = string.rep("-", math.floor(apDividier.width / getTextManager():MeasureStringX(UIFont.Large, "-")) - 22)
    panel.titleDivider = apDividier:makeLabel(dividerText, UIFont.Large, COLOR_GREEN, "left")

    if title then
        panel.subtitleLabel = apSubtitle:makeLabel("Disk Accessed: " .. title, UIFont.Medium, COLOR_GREEN, "left")
    else
        panel.subtitleLabel = apSubtitle:makeLabel(subtitle, UIFont.Medium, COLOR_GREEN, "left")
    end

    panel.subtitleDivider = apSubtitleDivider:makeLabel(dividerText, UIFont.Large, COLOR_GREEN, "left")
end

function WC_CM_Shared_Styling.changeButton(button)
    button.backgroundColor = COLOR_DARKERGREEN
    if button.backgroundColorMouseOver then
        button.backgroundColorMouseOver = COLOR_LIGHTERGREEN
    end
    button.textColor = COLOR_BLACK
    local originalRender = button.render
    button.render = function(button)
        originalRender(button)
        button:drawRectBorder(0, 0, button.width, button.height, 1, 0, 0, 0)
    end
    button:setFont(UIFont.Medium)
end

function WC_CM_Shared_Styling.changeButtonGrey(button, toolTip, panel)
    button.backgroundColor = COLOR_GREY
    if button.backgroundColorMouseOver then
        button.backgroundColorMouseOver = COLOR_LIGHTERGREY 
    end
    button.textColor = COLOR_BLACK
    button.onClickArgs = {}
    button.onclick = function()
        WC_CM_Shared_Output.addOutputMessage(toolTip, panel, panel.computerObject)
    end
    button:initialise()
    local originalRender = button.render
    button.render = function(button)
        originalRender(button)
        button:drawRectBorder(0, 0, button.width, button.height, 1, 0, 0, 0)
        if button.mouseOver then
            local tooltipText = toolTip or "No tooltip available"
            local tooltipWidth = getTextManager():MeasureStringX(UIFont.Small, tooltipText) + 10
            local tooltipHeight = FONT_HGT_SMALL + 4
            local mouseX, mouseY = getMouseX() - button:getAbsoluteX(), getMouseY() - button:getAbsoluteY()
            button:drawRect(mouseX, mouseY - tooltipHeight, tooltipWidth, tooltipHeight, 0.8, 0, 0, 0)
            button:drawText(tooltipText, mouseX + 5, mouseY - tooltipHeight + 2, 1, 1, 1, 1, UIFont.Small)
        end
    end
    button:setFont(UIFont.Medium)
end

return WC_CM_Shared_Styling
