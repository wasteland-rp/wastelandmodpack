WC_CM_Shared_UIComponents = {}

function WC_CM_Shared_UIComponents.createButton(panel, label, onClickCallback, width, height)
    local button = ISButton:new(0, 0, width or 100, height or 25, label, panel, onClickCallback)
    button:initialise()
    panel:addChild(button)
    return button
end

function WC_CM_Shared_UIComponents.createLabel(panel, text, font, color, alignment)
    local label = ISLabel:new(0, 0, 0, text, color.r, color.g, color.b, color.a, font, alignment or "left")
    label:initialise()
    panel:addChild(label)
    return label
end

function WC_CM_Shared_UIComponents.createTextBox(panel, defaultText, width, height, multiline)
    local textBox = ISTextEntryBox:new(defaultText, 0, 0, width or 100, height or 25)
    if multiline then
        textBox:setMultipleLine(true)
    end
    textBox:initialise()
    panel:addChild(textBox)
    return textBox
end

return WC_CM_Shared_UIComponents
