require "WC_CM_Panel_Home"
require "WC_CM_Shared"

function CM_CreateDisk(disk)
    local modData = disk:getModData()
    local modDataKey = "ComputerNoteModData"
	local roll = ZombRand(1, 100)
    
    if not modData[modDataKey] then
        modData[modDataKey] = {}
    end

    modData[modDataKey].title = modData[modDataKey].title or nil
    modData[modDataKey].text = modData[modDataKey].text or nil
    modData[modDataKey].author = modData[modDataKey].author or nil
    modData[modDataKey].infected = modData[modDataKey].infected or nil
    modData[modDataKey].virus = modData[modDataKey].virus or nil
    modData[modDataKey].detected = modData[modDataKey].detected or nil
    modData[modDataKey].password = modData[modDataKey].password or nil

	if roll <= 2 then
		modData[modDataKey].virus = true
	end
end

if ComputerNoteMod then
	Events.OnFillWorldObjectContextMenu.Remove(ComputerNoteMod.computerNoteWorldContext)
end
ComputerNoteMod = {}

local SpriteComputerOff = {
	S = "appliances_com_01_72",
	E = "appliances_com_01_73",
	N = "appliances_com_01_74",
	W = "appliances_com_01_75",
}

local SpriteComputerOn = {
	S = "appliances_com_01_76",
	E = "appliances_com_01_77",
	N = "appliances_com_01_78",
	W = "appliances_com_01_79",
}

local function getcomputerObject(sq)
	if sq then
		if sq:getObjects() then
			for i=0,sq:getObjects():size() - 1 do
				local obj = sq:getObjects():get(i)
				if obj:getSprite() then
					local spriteName = obj:getSprite():getName() or nil
					if spriteName then
						-- Computer
						if getActivatedMods():contains("Computer") then
							if (spriteName == SpriteComputerOn.S) or (spriteName == SpriteComputerOn.E)
								or (spriteName == SpriteComputerOn.N) or (spriteName == SpriteComputerOn.W) then
								return obj
							end
						else
							if (spriteName == SpriteComputerOff.S) or (spriteName == SpriteComputerOff.E)
								or (spriteName == SpriteComputerOff.N) or (spriteName == SpriteComputerOff.W) then
								return obj
							end
						end
					end
				end
			end
		end
	end
end

function ComputerNoteMod.computerNoteWorldContext(player, context, worldObjects)
	if not worldObjects and not worldObjects[1] then return end
    local square = worldObjects[1]:getSquare()
	local playerObj = getSpecificPlayer(player)
	local computer = getcomputerObject(square)
	local disks = playerObj:getInventory():getAllTypeRecurse("Base.WC_CM_FloppyDisk")

	if not computer then return end

	if disks and not disks:isEmpty() then
		for i = 0, disks:size() - 1 do
			CM_CreateDisk(disks:get(i))
		end
		local submenu = WL_ContextMenuUtils.getOrCreateSubMenuOnTop(context, "Disk Management")
		submenu:addOptionOnTop("Open Root Menu", playerObj, function()
			if luautils.walkAdj(playerObj, computer:getSquare()) then
				ISTimedActionQueue.add(WC_OpenDiskAction:new(playerObj, computer, disks:get(0)))
			end
		end)
		if playerObj:isGodMod() then
			submenu:addOption("Clear Output", playerObj, function()
				WC_CM_Shared_Output.clearAllMessages(computer)
			end)
		end
	else
		WL_ContextMenuUtils.missingRequirement(context, "Disk Management", "No Floppy Disk Found", "top", "media/textures/Item_FloppyDisk.png")
	end
end

Events.OnFillWorldObjectContextMenu.Add(ComputerNoteMod.computerNoteWorldContext)
