WC_DecryptAction = ISBaseTimedAction:derive("WC_DecryptAction")

function WC_DecryptAction:isValid()
    if not self.computer then return false end
    if not self.disk then return false end
    if not self.character:getInventory():contains(self.disk) then return false end
    return true
end

function WC_DecryptAction:start()
    self:setActionAnim("Craft")
    self.character:setPrimaryHandItem(self.disk)
end

function WC_DecryptAction:waitToStart()
    if self.computer then
        self.character:faceLocation(self.computer:getX(), self.computer:getY())
        return self.character:shouldBeTurning()
    end
    return false
end

function increaseUnhappiness(character, amount)
    local damage = character:getBodyDamage()
    local currentUnhappiness = damage:getUnhappynessLevel()
    local newUnhappiness = currentUnhappiness + amount

    if newUnhappiness > 100 then
        newUnhappiness = 100
    elseif newUnhappiness < 0 then
        newUnhappiness = 0
    end

    damage:setUnhappynessLevel(newUnhappiness)
end

function increaseStress(character, amount)
    local stats = character:getStats()
    local currentStress = stats:getStress()
    local newStress = currentStress + amount

    if newStress > 100 then
        newStress = 100
    elseif newStress < 0 then
        newStress = 0
    end

    stats:setStress(newStress)
end

function WC_DecryptAction:perform()
    local chance = 10
    chance = chance + self.player:getPerkLevel(Perks.Electricity) * 1.5
    if self.player:getMoodles():getMoodleLevel(MoodleType.Unhappy) >= 1 then
        chance = chance - 10
    end
    if self.player:getMoodles():getMoodleLevel(MoodleType.Stressed) >= 1 then
        chance = chance - 10
    end

    chance = 100 - chance

    local roll = ZombRand(100)
    local success = roll >= chance

    if success then
        self.disk:getModData()["ComputerNoteModData"].password = nil
        if self.panel then
            self.panel:updateState()
            WC_CM_Shared_Output.addOutputMessage("Disk Successfully Decrypted.", self.panel, self.computerObject)
        end
    else
        if self.panel then
            WC_CM_Shared_Output.addOutputMessage("Failed to Decrypt the Disk.", self.panel, self.computerObject)
        end
        increaseUnhappiness(self.character, 10)
        increaseStress(self.character, 10)
        ISTimedActionQueue.add(WC_DecryptAction:new(self.character, self.computerObject, self.disk, self.panel))
    end
    ISBaseTimedAction.perform(self)
end

function WC_DecryptAction:showEditNotePrompt()
    local action = self
    self.editNotePrompt = WC_CM_Panel_View.display(self.character, self.computerObject, self.disk, title)
    action:forceComplete()
end

function WC_DecryptAction:new(player, computer, disk, panel)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.player = player
    o.character = player -- need this for ISBaseTimedAction
    o.computer = computer:getSquare()
    o.computerObject = computer
    o.disk = disk
    o.panel = panel
    o.stopOnWalk = true
    o.stopOnRun = true
    o.maxTime = 900
    if player:isGodMod() then
        o.maxTime = 10
    end
    if player:isTimedActionInstant() then
        o.maxTime = 1
    end
    return o
end