WC_OpenDiskAction = ISBaseTimedAction:derive("WC_OpenDiskAction")

function WC_OpenDiskAction:isValid()
    if not self.computer then return false end
    --if not self.disk then return false end
    --if not self.character:getInventory():contains(self.disk) then return false end
    return true
end

function WC_OpenDiskAction:start()
    --self.character:setPrimaryHandItem(self.disk)
end

function WC_OpenDiskAction:waitToStart()
    if self.computer then
        self.character:faceLocation(self.computer:getX(), self.computer:getY())
        return self.character:shouldBeTurning()
    end
    return false
end

function WC_OpenDiskAction:update()
    if not self.panel then
        self:showHomeWindow()
    elseif not self.hasPassword then
        if not self.passwordPrompt then
            self:showPasswordPrompt()
        end
    else
        if not self.editNotePrompt then
            self:showEditNotePrompt()
            if panel then
                self.panel:addOutputMessage("Disk Accessed Successfully.")
            end
        end
    end
    if self.disk:getModData()["ComputerNoteModData"].virus then
        self:infectDisks()            
    end
end

function WC_OpenDiskAction:infectDisks()
    local disks = self.character:getInventory():getAllTypeRecurse("Base.WC_CM_FloppyDisk")
    for i = 0, disks:size() - 1 do
        local disk = disks:get(i)
        if self.disk:getModData()["ComputerNoteModData"].virus and not disk:getModData()["ComputerNoteModData"].infected and not disk:getModData()["ComputerNoteModData"].virus then
            self.character:setSecondaryHandItem(disk)
            disk:getModData()["ComputerNoteModData"].infected = true
            local name = disk:getName()
            if disk:getModData()["ComputerNoteModData"].title ~= nil then
                name = disk:getModData()["ComputerNoteModData"].title
            end
            if not name:find("Corrupted: ") then
                disk:setName("Corrupted: " .. name)
            end
            if self.panel then
                self.panel:updateState()
            end
        end
    end
end

function WC_OpenDiskAction:stop()
    ISBaseTimedAction.stop(self)
    if self.passwordPrompt then
        self.passwordPrompt:removeFromUIManager()
    end
    if self.editNotePrompt then
        self.editNotePrompt:removeFromUIManager()
    end
    if self.panel then
        self.panel:updateState()
    end
end

function WC_OpenDiskAction:perform()
    if self.passwordPrompt then
        self.passwordPrompt:removeFromUIManager()
    end
    if self.editNotePrompt then
        self.editNotePrompt:removeFromUIManager()
    end
    if self.panel then
        self.panel:updateState()
    end
    if self.callback_onClose then
        self.callback_onClose()
    end
    ISBaseTimedAction.perform(self)
end

function WC_OpenDiskAction:showPasswordPrompt()
    local scale = getTextManager():getFontHeight(UIFont.Small) / 14
    local width = 220 * scale
    local height = 130 * scale
    local x = (getCore():getScreenWidth() / 2) - (width / 2)
    local y = (getCore():getScreenHeight() / 2) - (height / 2)

    local modal = ISTextBox:new(x, y, width, height, "", "", nil, function (_, button)
        if button.internal == "OK" then
            local enteredPassword = button.target.entry:getText()
            if enteredPassword ~= self.disk:getModData()["ComputerNoteModData"].password then
                if self.panel then
                    WC_CM_Shared_Output.addOutputMessage("Incorrect Password Entered.", self.panel, self.computerObject)
                end
                self:forceStop()
            else
                self.hasPassword = true
                if self.panel then
                    WC_CM_Shared_Output.addOutputMessage("Correct Password Entered.", self.panel, self.computerObject)
                    self.callback_onClose()
                end
            end
        else
            self:forceStop()
        end
    end, nil)

    local originalPrerender = modal.prerender
    modal.prerender = function(self)

        originalPrerender(self)

        local font = UIFont.Small
        local textColor = {r = 0.5, g = 1.0, b = 0.5, a = 1.0}
        local fontHgt = getTextManager():getFontFromEnum(font):getLineHeight()
        self:drawTextCentre("Enter Password to Open the Disk:", self:getWidth() / 2, self.entry:getY() - 8 - fontHgt, textColor.r, textColor.g, textColor.b, textColor.a, font)
    end

    modal:initialise()

    modal.backgroundColor = {r=0,g=0,b=0,a=0.9}
    modal.borderColor = {r=0,g=0,b=0,a=1}
    modal.entry.borderColor = {r=0,g=0,b=0,a=1}
    modal.entry:setOnlyNumbers(false)
    modal.yes:setFont(UIFont.Medium)
    modal.no:setFont(UIFont.Medium)
    modal.yes.backgroundColor = {r=0.3, g=0.7, b=0.3, a=1}
    modal.no.backgroundColor = {r=0.3, g=0.7, b=0.3, a=1}
    modal.yes.backgroundColorMouseOver = {r=0.7, g=0.9, b=0.7, a=1}
    modal.no.backgroundColorMouseOver = {r=0.7, g=0.9, b=0.7, a=1}
    modal.yes.textColor = {r=0, g=0, b=0, a=1}
    modal.no.textColor = {r=0, g=0, b=0, a=1}

    modal:addToUIManager()
    self.passwordPrompt = modal
end

function WC_OpenDiskAction:showEditNotePrompt()
    local title = self.disk:getModData()["ComputerNoteModData"].title or "Empty Disk"
    self.editNotePrompt = WC_CM_Panel_View.display(self.character, self.computerObject, self.disk, title)
    self.callback_onClose()
    self:forceStop()
end

function WC_OpenDiskAction:showHomeWindow()
    WC_CM_Panel_Home.display(self.character, self.computerObject, self.disk)
    self:forceStop()
end

function WC_OpenDiskAction:new(player, computer, disk, panel, onClose)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.character = player -- need this for ISBaseTimedAction
    o.computer = computer:getSquare()
    o.computerObject = computer
    o.disk = disk
    if panel then
        o.panel = panel
    end
    if onClose then
        o.callback_onClose = onClose
    end
    if not disk:getModData()["ComputerNoteModData"] or not disk:getModData()["ComputerNoteModData"].password then
        o.hasPassword = true
    end
    if not disk:getModData()["ComputerNoteModData"] or not disk:getModData()["ComputerNoteModData"].virus then
        o.hasVirus = true
    end
    o.stopOnWalk = true
    o.stopOnRun = true
    o.maxTime = -1
    return o
end