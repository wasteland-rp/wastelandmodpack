local piles = {
    ["WLCustomCases.Log_Pile"] = true,
    ["WLCustomCases.Stone_Pile"] = true,
    ["WLCustomCases.Plank_Pile"] = true,
    ["WLCustomCases.Brick_Pile"] = true,
    ["WLCustomCases.Metal_Pile"] = true,
    ["WLCustomCases.BaggedItems_Pile"] = true,
    ["WLCustomCases.CarParts_Pile"] = true,
}

local original_ISInventoryTransferAction_new = ISInventoryTransferAction.new
function ISInventoryTransferAction:new(character, item, srcContainer, destContainer, time)
    local srcItem = srcContainer:getContainingItem()
    local destItem = destContainer:getContainingItem()
    if (srcItem and piles[srcItem:getFullType()]) or (destItem and piles[destItem:getFullType()]) then
        time = 5
    end
    return original_ISInventoryTransferAction_new(self, character, item, srcContainer, destContainer, time)
end