local alwaysShowingTypes = {
    ["WLCustomCases.Manilla_Folder"] = true,
    ["WLCustomCases.FloppyDisk_Binder"] = true,
    ["WLCustomCases.Empty_Book"] = true,
    ["Base.Wallet"] = true,
    ["Base.Wallet2"] = true,
    ["Base.Wallet3"] = true,
    ["Base.Wallet4"] = true,
}

local freeWeight = {
    ["WLCustomCases.Manilla_Folder"] = 0.1,
    ["WLCustomCases.FloppyDisk_Binder"] = 0.1,
    ["Base.Wallet"] = 0.2,
    ["Base.Wallet2"] = 0.2,
    ["Base.Wallet3"] = 0.2,
    ["Base.Wallet4"] = 0.2,
}

local function GetInternalWeight(bag)
    local weight = 0
    for i = 0, bag:getItems():size()-1 do
        weight = weight + bag:getItems():get(i):getActualWeight()
    end
    return weight
end

local function DoFreeWeights()
    local playerObj = getPlayer()
    local items = playerObj:getInventory():getItems()

    for i = 0, items:size()-1 do
        local item = items:get(i)
        if freeWeight[item:getFullType()] then
            if item:isEquipped() or not playerObj:getInventory():contains(item) then
                item:setActualWeight(GetInternalWeight(item:getInventory()) + freeWeight[item:getFullType()])
            else
                item:setActualWeight(-1 * GetInternalWeight(item:getInventory()) + freeWeight[item:getFullType()])
            end
            item:setCustomWeight(true);
        end
    end
end

local function OnRefreshInventoryWindowContainers(inventoryPage, eventType)
    if eventType == "buttonsAdded" and inventoryPage.onCharacter then
        local playerObj = getSpecificPlayer(inventoryPage.player)
        local items = playerObj:getInventory():getItems()

        for i = 0, items:size()-1 do
            local item = items:get(i)
            if alwaysShowingTypes[item:getFullType()] and not playerObj:isEquipped(item) then
                inventoryPage:addContainerButton(item:getInventory(), item:getTex(), item:getName(), item:getName())
            end
        end
    elseif eventType == "end" then
        DoFreeWeights()
    end
end

local original_ISInventoryPage_refreshWeight = ISInventoryPage.refreshWeight
function ISInventoryPage:refreshWeight()
    original_ISInventoryPage_refreshWeight(self)
    DoFreeWeights()
end

Events.OnRefreshInventoryWindowContainers.Add(OnRefreshInventoryWindowContainers)