WLCustomCases = {
  FolderAllowedTypes = {
    ["Base.SheetPaper2"] = true,
    ["MOTW.SheetMusic"] = true,
  },
  LogPileAllowedTypes = {
    ["Base.Log"] = true,
    ["Base.LogStacks2"] = true,
    ["Base.LogStacks3"] = true,
    ["Base.LogStacks4"] = true,
    ["Base.TreeBranch"] = true,
    ["Base.TreeBranchPackage"] = true,
    ["Base.Twigs"] = true,
  },
  StonePileAllowedTypes = {
    ["Base.Stone"] = true,
    ["Base.WastelandBuildsLimestone"] = true,
    ["Base.WastelandBuildsSaltpeter"] = true,
    ["Base.WastelandBuildsPyrite"] = true,
    ["Base.WastelandBuildsIronOre"] = true,
    ["Base.WastelandBuildsNahcolite"] = true,
  },
  PlankPileAllowedTypes = {
    ["Base.Plank"] = true,
  },
  BrickPileAllowedTypes = {
    ["Base.WastelandBuildsBrick"] = true,
  },
  MetalPileAllowedTypes = {
    ["Base.ScrapMetal"] = true,
    ["Base.MetalPipe"] = true,
    ["Base.MetalBar"] = true,
    ["Base.SheetMetal"] = true,
    ["Base.SmallSheetMetal"] = true,
    ["Base.ScrapMetalPackage"] = true,
    ["Base.SmallSheetMetalPackage"] = true,
    ["Base.LeadPipe"] = true,
    ["TW.MetalParts"] = true,
  },
  BaggedItemsPileAllowedTypes = {
    ["Base.Dirtbag"] = true,
    ["Base.Sandbag"] = true,
    ["Base.Gravelbag"] = true,
    ["Base.Charcoal"] = true,
    ["Base.PlasterPowder"] = true,
    ["Base.ConcretePowder"] = true,
    ["Base.EmptySandbag"] = true,
    ["Base.Fertilizer"] = true,
    ["Base.CompostBag"] = true,
  },
  WalletAllowedTypes = {
    ["Base.CreditCard"] = true,
    ["Base.GoldCurrency"] = true,
    ["Base.GoldCurrencyFive"] = true,
    ["Base.GoldCurrencyTen"] = true,
    ["Base.GoldCurrencyFifty"] = true,
    ["Base.GoldCurrencyHundred"] = true,
    ["Base.Money"] = true,
    ["DrugMod.Cocaine"] = true,
    ["DrugMod.Acid"] = true,
    ["Base.SheetPaper2"] = true,
    ["Base.RubberBand"] = true,
    ["SapphCooking.Fortune_Message1"] = true,
    ["SapphCooking.Fortune_Message2"] = true,
    ["SapphCooking.Fortune_Message3"] = true,
    ["SapphCooking.Fortune_Message4"] = true,
    ["SapphCooking.Fortune_Message5"] = true,
    ["SapphCooking.Fortune_Message6"] = true,
    ["SapphCooking.Fortune_Message7"] = true,
    ["SapphCooking.Fortune_Message8"] = true,
    ["SapphCooking.Fortune_Message9"] = true,
    ["SapphCooking.Fortune_Message10"] = true,
    ["Base.Bandaid"] = true,
    ["Base.AerodactylCoin"] = true,
    ["Base.ChanseyCoin"] = true,
    ["Base.EeveeCoin"] = true,
    ["Base.MeowthCoin"] = true,
    ["Base.MetalPikachuCoin"] = true,
    ["Base.PikachuCoin"] = true,
    ["Base.StarmieCoin"] = true,
    ["Base.VileplumeCoin"] = true,
    ["MoreBrews.BottleCap"] = true,
    ["Base.MonopolyMoney1"] = true,
    ["Base.MonopolyMoney5"] = true,
    ["Base.MonopolyMoney10"] = true,
    ["Base.MonopolyMoney20"] = true,
    ["Base.MonopolyMoney50"] = true,
    ["Base.MonopolyMoney100"] = true,
    ["Base.MonopolyMoney500"] = true,
    ["Base.Bricktoys"] = true,
    ["Base.Paperclip"] = true,
    ["WLPB.Stamp"] = true,
    ["WLPB.StampBook"] = true,

  },
  FloppyDiskBinderAllowedTypes = {
    ["Base.WC_CM_FloppyDisk"] = true,
  }
}

function WLCustomCases.FolderAcceptFunction(_, item)
  local itemId = item:getFullType()
  return WLCustomCases.FolderAllowedTypes[itemId]
end

function WLCustomCases.LogPileAcceptFunction(_, item)
  local itemId = item:getFullType()
  return WLCustomCases.LogPileAllowedTypes[itemId]
end

function WLCustomCases.StonePileAcceptFunction(_, item)
  local itemId = item:getFullType()
  return WLCustomCases.StonePileAllowedTypes[itemId]
end

function WLCustomCases.PlankPileAcceptFunction(_, item)
  local itemId = item:getFullType()
  return WLCustomCases.PlankPileAllowedTypes[itemId]
end

function WLCustomCases.BrickPileAcceptFunction(_, item)
  local itemId = item:getFullType()
  return WLCustomCases.BrickPileAllowedTypes[itemId]
end

function WLCustomCases.MetalPileAcceptFunction(_, item)
  local itemId = item:getFullType()
  return WLCustomCases.MetalPileAllowedTypes[itemId]
end

function WLCustomCases.BaggedItemsPileAcceptFunction(_, item)
  local itemId = item:getFullType()
  return WLCustomCases.BaggedItemsPileAllowedTypes[itemId]
end

function WLCustomCases.CarPartsPileAcceptFunction(_, item)
  local category = item:getDisplayCategory()
  return category == "Mech" or category == "VehicleMaintenance"
end

function WLCustomCases.WalletAcceptFunction(_, item)
  local itemId = item:getFullType()
  return WLCustomCases.WalletAllowedTypes[itemId]
end

function WLCustomCases.FloppyDiskBinderAcceptFunction(_, item)
  local itemId = item:getFullType()
  return WLCustomCases.FloppyDiskBinderAllowedTypes[itemId]
end

function WLCustomCases.CanDestroyPile(item)
  return item:getItemContainer():isEmpty()
end

function WLCustomCases.CanBuildPile(item)
  return item:getContainer():getType() == "floor"
end