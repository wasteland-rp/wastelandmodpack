local fishingNetItems = {
    ["Base.BaitFish"] = 15,
    ["Base.Lobster"] = 2,
    ["Base.FishRoe"] = 2,
    ["Base.Oysters"] = 2,
    ["Base.Shrimp"] = 3,
    ["Base.Crayfish"] = 3,
    ["Base.Squid"] = 1,
}

local fishingNetTrash = {
    ["Base.Bra_Straps_AnimalPrint"] = 1,
    ["Base.Bikini_TINT"] = 1,
    ["Base.EmptyBeerCan"] = 20,
    ["Base.Straw"] = 5,
    ["Base.Plasticbag"] = 40,
    ["WLIF.EmptySyringe"] = 2,
    ["Base.Hat_BonnieHat"] = 1,
    ["Base.Hat_BucketHat"] = 1,
    ["Base.GoldCurrency"] = 1,
    ["Base.Glasses_Aviators"] = 1,
    ["Base.Glasses_Normal"] = 1,
    ["Base.Glasses_Reading"] = 1,
    ["Base.Glasses"] = 1,
    ["Base.Glasses_Shooting"] = 1,
    ["Base.Glasses_Sun "] = 1,
    ["Base.WeddingRing_Man"] = 4,
    ["Base.WeddingRing_Woman"] = 2,
    ["Base.BeerEmpty"] = 15,
    ["Base.Pop2"] = 1,
    ["Base.Pop3"] = 1,
}

fishingNet.checkTrap = function(player, trap, hours)
    -- the fishnet can broke !
    local fishCaught = false;
    if hours > 72 and ZombRand(10) == 0 then
--        getSoundManager():PlayWorldSound("getFish", false, player:getSquare(), 1, 20, 1, false)
        player:playSound("CheckFishingNet");
        trap:getSquare():transmitRemoveItemFromSquare(trap);
        player:getInventory():AddItem("Base.BrokenFishingNet");
        return;
    end
    if hours > 20 then
        hours = 20;
    end
    local lootLookUpTable = {}
    for item, count in pairs(fishingNetItems) do
        for i=1,count do
            table.insert(lootLookUpTable, item);
        end
    end
    local trashLookUpTable = {}
    for item, count in pairs(fishingNetTrash) do
        for i=1,count do
            table.insert(trashLookUpTable, item);
        end
    end
    for i=1,hours do
        if ZombRand(60) == 0 then
            local item = trashLookUpTable[ZombRand(#trashLookUpTable)+1];
            player:getInventory():AddItem(item);
        elseif ZombRand(40) == 0 then
            local item = lootLookUpTable[ZombRand(#lootLookUpTable)+1];
            player:getInventory():AddItem(item);
            fishCaught = true;
        end
    end
    if fishCaught then
        fishCaught = false;
        player:getXp():AddXP(Perks.Fishing, 1);
    end
    fishingNet.doTimestamp(trap);
end