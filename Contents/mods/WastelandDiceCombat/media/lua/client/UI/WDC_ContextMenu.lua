---
--- WDC_ContextMenu.lua
--- 22/04/2024
---

require 'UI/WDC_MainPanel'
require 'UI/WDC_EventPanel'
require "WRC/meta"

local WDC_ContextMenu = {}

if isClient() then -- Multiplayer
	if WRC and WRC.Meta then
		--WRC.Meta.RegisterAction("PvP Dice Combat", WDC_MainPanel.display)
		WRC.Meta.RegisterAction("Event Dice Rolls", WDC_EventPanel.display)
	end
else -- SP Testing
	WDC_ContextMenu.doMenu = function(playerIdx, context)
		context:addOption("PvP Dice Combat", nil, WDC_MainPanel.display)
		context:addOption("Event Dice Rolls", nil, WDC_EventPanel.display)
	end
	Events.OnFillWorldObjectContextMenu.Add(WDC_ContextMenu.doMenu)
end
