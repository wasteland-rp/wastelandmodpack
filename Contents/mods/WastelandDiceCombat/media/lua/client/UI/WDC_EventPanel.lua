---
--- WDC_EventPanel.lua
--- 23/08/2024
---


require "GravyUI_WL"
require "WDC_PlayerState"
require "dicesystem/WDC_Systems"
require "UI/WL_SelectPlayersPanel"
require "UI/WL_TextEntryPanel"

WDC_EventPanel = ISPanel:derive("WDC_EventPanel")

local FONT_HGT_SMALL = getTextManager():getFontHeight(UIFont.Small)
local FONT_HGT_MEDIUM = getTextManager():getFontHeight(UIFont.Medium)
local FONT_HGT_LARGE = getTextManager():getFontHeight(UIFont.Large)

local COLOR_WHITE = {r=1,g=1,b=1,a=1}
local COLOR_ORANGE = {r=1,g=0.64,b=0,a=1}
local COLOR_RED = {r=1,g=0,b=0,a=1}
local COLOR_GREEN = {r=0,g=1,b=0,a=1}

WDC_EventPanel.excludedPerks = {
	[Perks.MAX] = true,
	[Perks.None] = true,
	[Perks.Agility] = true,
	[Perks.Melee] = true,
	[Perks.Crafting] = true,
	[Perks.Passiv] = true,
	[Perks.Firearm] = true,
	[Perks.Survivalist] = true,
	[Perks.Melting] = true,
	[Perks.Blacksmith] = true,
	[Perks.Combat] = true
}

function WDC_EventPanel.display()
	if WDC_EventPanel.instance then
		return
	end
	WDC_EventPanel.instance = WDC_EventPanel:new()
	WDC_EventPanel.instance:addToUIManager()
end

function WDC_EventPanel:new()
	local scale = FONT_HGT_SMALL / 12
	local w = 370 * scale
	local h = 300 * scale
	local o = ISPanel:new(getCore():getScreenWidth()/2-w/2,getCore():getScreenHeight()/2-h/2, w, h)
	setmetatable(o, self)
	self.__index = self
	o:initialise()
	return o
end

function WDC_EventPanel:initialise()
	ISPanel.initialise(self)
	self.moveWithMouse = true

	local playerState = WDC_PlayerState.instance()
	local win = GravyUI.Node(self.width, self.height, self):pad(10, 5, 10, 10)
	local rowPadding = 15
	local header, body = win:rows({FONT_HGT_LARGE, win.height - FONT_HGT_LARGE - rowPadding}, rowPadding)
	self.headerLabel = header:makeLabel("Event Dice Rolls", UIFont.Large, COLOR_WHITE, "center")
	local systemSelectionArea, skillsArea, attacksArea, defenseArea = body:rows({ 2/15, 3/15, 7/15, 3/15, }, 20)

	-- Dice System
	local systemSelectNode, systemDescriptionNode = systemSelectionArea:rows( {FONT_HGT_MEDIUM, systemSelectionArea.height - FONT_HGT_MEDIUM - 10}, 10)
	local sysLabel, systemCombo = systemSelectNode:cols({ 0.3, 0.45}, 5)
	self.systemLabel = sysLabel:makeLabel("Combat System: ", UIFont.Small, COLOR_WHITE, "right")
	self.systemComboBox = systemCombo:makeComboBox(self, self.onSystemChanged)
	for _, system in pairs(WDC_SystemList) do
		self.systemComboBox:addOptionWithData(system.getName() .. " (" .. system.getKey() .. ")", system)
	end
	self.systemComboBox:selectData(WDC_SystemList[playerState.currentSystemKey])
	self.systemDescription = systemDescriptionNode:makeLabel("Description of the dice system here", UIFont.Small, COLOR_WHITE, "left")

	local skillsTitleNode, skillsSelection = skillsArea:rows(2, 10)
	skillsTitleNode:makeLabel("Skills", UIFont.Large, COLOR_WHITE, "left")

	local skillIconNode, skillComboSelection, skillCalcNode, rollSkillButtonNode, halfRollSkillButtonNode = skillsSelection:cols({ 0.1, 0.45, 0.15, 0.15, 0.15 }, 5)
	self.skillIcon = skillIconNode

	local skillComboSelector, _ = skillComboSelection:cols({0.8, 0.2})
	self.skillComboBox = skillComboSelector:makeComboBox(self, self.onSkillChanged)

	local validPerks = {}
	for i = 1, Perks.getMaxIndex() - 1 do
		local perks = Perks.fromIndex(i)
		local perk = PerkFactory.getPerk(perks)
		if perk and not WDC_EventPanel.excludedPerks[perk] then
			table.insert(validPerks, perk)
		end
	end

	table.sort(validPerks, function(a, b) return a:getName() < b:getName() end)
	for _, perk in ipairs(validPerks) do
		self.skillComboBox:addOptionWithData(perk:getName(), perk)
	end
	self.skillCalcLabel = skillCalcNode:makeLabel("", UIFont.Small, COLOR_WHITE, "left")
	self.rollSkillButton = rollSkillButtonNode:makeButton("Roll", self, self.doSkillRoll)

	-- Attack
	local attacksTitleNode, targetingNode, primaryWeaponNode, secondaryWeaponNode = attacksArea:rows(4, 10)
	attacksTitleNode:makeLabel("Attack", UIFont.Large, COLOR_WHITE, "left")
	local targetTextNode, currTargetNode, clearTargetNode, selectTargetNode = targetingNode:cols({ 0.13, 0.57, 0.15, 0.15 }, 5)
	targetTextNode:makeLabel("Target: ", UIFont.Medium, COLOR_WHITE, "left")
	self.currentTargetLabel = currTargetNode:makeLabel("", UIFont.Medium, COLOR_ORANGE, "left", true)
	self.clearTargetButton = clearTargetNode:makeButton("Clear", self, self.doTargetClear)
	self.selectTargetButton = selectTargetNode:makeButton("Change", self, self.doSelectTarget)
	local primaryWeapIconNode, primaryWeapNameNode, primaryWeapHitCalcNode, primaryWeapAttackNode, primaryWeapDamageNode = primaryWeaponNode:cols({ 0.1, 0.45, 0.15, 0.15, 0.15 }, 5)
	self.primaryWeaponNode = primaryWeapIconNode
	self.primaryWeaponName = primaryWeapNameNode:makeLabel("", UIFont.Medium, COLOR_WHITE, "left", true)
	self.primaryWeaponHitCalculation = primaryWeapHitCalcNode:makeLabel("", UIFont.Small, COLOR_WHITE, "left")
	self.primaryAttackLabel = primaryWeapAttackNode:makeLabel("", UIFont.Medium, COLOR_ORANGE, "center")
	self.primaryAttackButton = primaryWeapAttackNode:makeButton("Attack", self, self.doPrimaryAttack)
	self.primaryDamageLabel = primaryWeapDamageNode:makeLabel("", UIFont.Medium, COLOR_RED, "center")
	self.primaryDamageButton = primaryWeapDamageNode:makeButton("Damage", self, self.doPrimaryDamage)
	local secondaryWeapIconNode, secondaryWeapNameNode, secondaryWeapHitCalcNode, secondaryWeapAttackNode, secondaryWeapDamageNode = secondaryWeaponNode:cols({ 0.1, 0.45, 0.15, 0.15, 0.15 }, 5)
	self.secondaryWeaponNode = secondaryWeapIconNode
	self.secondaryWeaponName = secondaryWeapNameNode:makeLabel("", UIFont.Medium, COLOR_WHITE, "left", true)
	self.secondaryWeaponHitCalculation = secondaryWeapHitCalcNode:makeLabel("", UIFont.Small, COLOR_WHITE, "left")
	self.secondaryAttackLabel = secondaryWeapAttackNode:makeLabel("", UIFont.Medium, COLOR_ORANGE, "center")
	self.secondaryAttackButton = secondaryWeapAttackNode:makeButton("Attack", self, self.doSecondaryAttack)
	self.secondaryDamageLabel = secondaryWeapDamageNode:makeLabel("", UIFont.Medium, COLOR_RED, "center")
	self.secondaryDamageButton = secondaryWeapDamageNode:makeButton("Damage", self, self.doSecondaryDamage)

	-- Defense
	local defenseTitleNode, defenseRollsNode = defenseArea:rows(2, 10)
	defenseTitleNode:makeLabel("Defense", UIFont.Large, COLOR_WHITE, "left")
	local defendIconNode, defendLabelNode, defendCalcNode, _, defendButtonNode = defenseRollsNode:cols({ 0.1, 0.45, 0.15, 0.15, 0.15 }, 5)
	self.defendIcon = defendIconNode
	defendLabelNode:makeLabel("Dodge & Block", UIFont.Medium, COLOR_WHITE, "left")
	self.defenseCalculationLabel = defendCalcNode:makeLabel("", UIFont.Small, COLOR_WHITE, "left")
	defendButtonNode:makeButton("Defend", self, self.doDefendRoll)

	-- Add close button last to avoid overlap
	self.closeButton = win:corner("topRight", FONT_HGT_SMALL + 3, FONT_HGT_SMALL + 3):offset(9, -4):makeButton("X", self, self.onClose)
	self:onSystemChanged()
end

--- Refreshes the state of the whole UI
function WDC_EventPanel:updateState()
	self:updateAttacksState()
	self.skillCalcLabel:setText(WDC_Systems.getSkillCalculation(
			self:getCurrentSkillSelected(), self:getCurrentDiceSystem()))
end

function WDC_EventPanel:updateAttacksState()
	local playerState = WDC_PlayerState.instance()
	self.primaryWeapon = getPlayer():getPrimaryHandItem()
	self.secondaryWeapon = getPlayer():getSecondaryHandItem()
	self.primaryWeaponName:setText(WDC_Systems.getWeaponName(self.primaryWeapon))
	self.primaryWeaponHitCalculation:setText(WDC_Systems.getWeaponAttackCalculation(self.primaryWeapon, self:getCurrentDiceSystem()))

	if playerState.currentTarget then
		local displayedName = playerState.currentTarget
		if WRC then displayedName = WRC.Meta.GetName(playerState.currentTarget) end
		self.currentTargetLabel:setText(displayedName)
	else
		self.currentTargetLabel:setText("Nobody")
	end

	if self.primaryWeapon == self.secondaryWeapon then
		self.secondaryWeaponName:setText("")
		self.secondaryWeaponHitCalculation:setText("")
		self.secondaryAttackButton:setVisible(false)
		self.secondaryDamageButton:setVisible(false)
	else
		self.secondaryWeaponName:setText(WDC_Systems.getWeaponName(self.secondaryWeapon))
		self.secondaryWeaponHitCalculation:setText(WDC_Systems.getWeaponAttackCalculation(self.secondaryWeapon, self:getCurrentDiceSystem()))
		self.secondaryAttackButton:setVisible(true)
		self.secondaryDamageButton:setVisible(true)
	end
end

function WDC_EventPanel:getCurrentDiceSystem()
	return self.systemComboBox:getOptionData(self.systemComboBox.selected)
end

function WDC_EventPanel:onSystemChanged()
	local selectedSystem = self:getCurrentDiceSystem()
	WDC_PlayerState.instance().currentSystemKey = selectedSystem.getKey()
	self.systemDescription:setText(selectedSystem.getDescription())
	self.defenseCalculationLabel:setText(WDC_Systems.getDefensiveRollCalculation(getPlayer(), self:getCurrentDiceSystem()))
	self:updateState() -- The modifiers will change here too
end

function WDC_EventPanel:getCurrentSkillSelected()
	return self.skillComboBox:getOptionData(self.skillComboBox.selected)
end

function WDC_EventPanel:onSkillChanged()
	self:updateState()
end

function WDC_EventPanel:doSkillRoll()
	getSoundManager():playUISound("DiceRoll")
	WDC_Systems.rollSkill(self:getCurrentSkillSelected(), self:getCurrentDiceSystem())
end

function WDC_EventPanel:doTargetClear()
	self:onNewTargetSelected(nil)
end

function WDC_EventPanel:doSelectTarget()
	WL_SelectPlayersPanel:show(self, self.onNewTargetSelected, {
		includeSelf = false,
		onlyInLOS = true,
	})
end

function WDC_EventPanel:onNewTargetSelected(username)
	WDC_PlayerState.instance().currentTarget = username
	self:updateState()
end

function WDC_EventPanel:doDefendRoll()
	getSoundManager():playUISound("MetalWooshHit")
	WDC_Systems.rollDefense(self:getCurrentDiceSystem())
	self:updateState()
end

---@param weapon InventoryItem can be nil - this means unarmed
local function doAttackSound(weapon)
	if weapon and weapon:IsWeapon() then
		getPlayer():playSound(weapon:getSwingSound())
	else
		getPlayer():playSound("PunchSwoosh")
	end
end

---@param weapon InventoryItem can be nil - this means unarmed
local function doDamageSound(weapon)
	if weapon and weapon:IsWeapon() then
		getPlayer():playSound(weapon:getZombieHitSound())
	else
		getPlayer():playSound("PunchHitCrunch")
	end
end

function WDC_EventPanel:doPrimaryAttack()
	local playerState = WDC_PlayerState.instance()
	WDC_Systems.rollAttack(self.primaryWeapon, self:getCurrentDiceSystem(), playerState.currentTarget, false)
	doAttackSound(self.primaryWeapon)
	self:updateState()
end

function WDC_EventPanel:doPrimaryDamage()
	local playerState = WDC_PlayerState.instance()
	local isTwoHanded = self.primaryWeapon == self.secondaryWeapon
	WDC_Systems.rollDamage(self.primaryWeapon, isTwoHanded, self:getCurrentDiceSystem(), playerState.currentTarget)
	doDamageSound(self.primaryWeapon)
	self:updateState()
end

function WDC_EventPanel:doSecondaryAttack()
	local playerState = WDC_PlayerState.instance()
	WDC_Systems.rollAttack(self.secondaryWeapon, self:getCurrentDiceSystem(), playerState.currentTarget, false)
	doAttackSound(self.secondaryWeapon)
	self:updateState()
end

function WDC_EventPanel:doSecondaryDamage()
	local playerState = WDC_PlayerState.instance()
	WDC_Systems.rollDamage(self.secondaryWeapon, false, self:getCurrentDiceSystem(), playerState.currentTarget)
	doDamageSound(self.secondaryWeapon)
	self:updateState()
end

function WDC_EventPanel:render()
	self:drawTexture(getTexture("media/ui/Icon_Shield.png"), self.defendIcon.left, self.defendIcon.top, 1, 1.0, 1.0, 1.0)
	self:drawTexture(getTexture("media/ui/initiative_icon.png"), self.skillIcon.left, self.skillIcon.top, 1, 1.0, 1.0, 1.0)

	if self.primaryWeapon then
		self:drawTexture(self.primaryWeapon:getTex(), self.primaryWeaponNode.left, self.primaryWeaponNode.top, 1, 1.0, 1.0, 1.0)
	else
		self:drawTexture(getTexture("media/ui/Icon_Melee_Fist.png"), self.primaryWeaponNode.left, self.primaryWeaponNode.top, 1, 1.0, 1.0, 1.0)
	end

	if self.secondaryWeapon then
		local alpha = 1.0
		if self.secondaryWeapon == self.primaryWeapon then
			alpha = 0.5
		end
		self:drawTexture(self.secondaryWeapon:getTex(), self.secondaryWeaponNode.left, self.secondaryWeaponNode.top, alpha, 1.0, 1.0, 1.0)
	else
		local alpha = 1.0
		if not self.primaryWeapon then
			alpha = 0.5
		end
		self:drawTexture(getTexture("media/ui/Icon_Melee_Fist.png"), self.secondaryWeaponNode.left, self.secondaryWeaponNode.top, alpha, 1.0, 1.0, 1.0)
	end
end


function WDC_EventPanel:onClose()
	self:removeFromUIManager()
	WDC_EventPanel.instance = nil
end

function WDC_EventPanel:removeFromUIManager()
	ISPanelJoypad.removeFromUIManager(self)
end

Events.OnEquipPrimary.Add(function()
	if WDC_EventPanel.instance ~= nil then
		--TODO delay for 4 seconds then check if weapon has changed?
		WDC_EventPanel.instance:updateState()
	end
end)

Events.OnEquipSecondary.Add(function()
	if WDC_EventPanel.instance ~= nil then
		--TODO delay for 4 seconds then check if weapon has changed?
		WDC_EventPanel.instance:updateState()
	end
end)