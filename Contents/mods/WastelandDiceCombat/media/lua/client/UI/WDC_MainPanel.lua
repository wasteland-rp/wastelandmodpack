---
--- WDC_MainPanel.lua
--- 22/04/2024
---

require "GravyUI_WL"
require "WDC_PlayerState"
require "dicesystem/WDC_Systems"
require "UI/WL_SelectPlayersPanel"
require "UI/WL_TextEntryPanel"
require "GroundHightlighter"

WDC_MainPanel = ISPanel:derive("WDC_MainPanel")

local FONT_HGT_SMALL = getTextManager():getFontHeight(UIFont.Small)
local FONT_HGT_MEDIUM = getTextManager():getFontHeight(UIFont.Medium)
local FONT_HGT_LARGE = getTextManager():getFontHeight(UIFont.Large)

local COLOR_WHITE = {r=1,g=1,b=1,a=1}
local COLOR_ORANGE = {r=1,g=0.64,b=0,a=1}
local COLOR_RED = {r=1,g=0,b=0,a=1}
local COLOR_GREEN = {r=0,g=1,b=0,a=1}

function WDC_MainPanel.display()
	if WDC_MainPanel.instance then
		return
	end
	WDC_MainPanel.instance = WDC_MainPanel:new()
	WDC_MainPanel.instance:addToUIManager()
	Events.OnPlayerMove.Add(WDC_MainPanel.trackMovement)
end

function WDC_MainPanel:new()
	local scale = FONT_HGT_SMALL / 12
	local w = 370 * scale
	local h = 430 * scale
	local o = ISPanel:new(getCore():getScreenWidth()/2-w/2,getCore():getScreenHeight()/2-h/2, w, h)
	setmetatable(o, self)
	self.__index = self
	o:initialise()
	return o
end

function WDC_MainPanel:initialise()
	ISPanel.initialise(self)
	self.moveWithMouse = true
	self.groundHighlighter = GroundHightlighter:new()

	local playerState = WDC_PlayerState.instance()
	local win = GravyUI.Node(self.width, self.height, self):pad(10, 5, 10, 10)
	local rowPadding = 15
	local header, body = win:rows({FONT_HGT_LARGE, win.height - FONT_HGT_LARGE - rowPadding}, rowPadding)
	self.headerLabel = header:makeLabel("Dice Combat", UIFont.Large, COLOR_WHITE, "center")
	local systemSelectionArea, turnOrderArea, movementArea, attacksArea, defenseArea, actionsArea = body:rows({ 2/15, 2/15, 2/15, 4/15, 3/15, 2/15 }, 20)

	-- Dice System
	local systemSelectNode, systemDescriptionNode = systemSelectionArea:rows(2, 10)
	local sysLabel, systemCombo = systemSelectNode:cols({ 0.3, 0.45}, 5)
	self.systemLabel = sysLabel:makeLabel("Combat System: ", UIFont.Small, COLOR_WHITE, "right")
	self.systemComboBox = systemCombo:makeComboBox(self, self.onSystemChanged)
	for _, system in pairs(WDC_SystemList) do
		self.systemComboBox:addOptionWithData(system.getName() .. " (" .. system.getKey() .. ")", system)
	end
	self.systemComboBox:selectData(WDC_SystemList[playerState.currentSystemKey])
	self.systemDescription = systemDescriptionNode:makeLabel("Description of the dice system here", UIFont.Small, COLOR_WHITE, "left")

	-- Initiative
	local turnOrderTitleNode, initiativeNode = turnOrderArea:rows(2, 10)
	turnOrderTitleNode:makeLabel("Turn Order", UIFont.Large, COLOR_WHITE, "left")
	local initiativeIconNode, initiativeLabelNode, initiativeCalcNode, rollInitiativeNode, startTurnNode = initiativeNode:cols({ 0.1, 0.45, 0.15, 0.15, 0.15 }, 5)
	self.initiativeIcon = initiativeIconNode
	initiativeLabelNode:makeLabel("Initiative", UIFont.Medium, COLOR_WHITE, "left")
	self.initiativeCalcLabel = initiativeCalcNode:makeLabel("", UIFont.Small, COLOR_WHITE, "left")
	self.lastInitiativeLabel = rollInitiativeNode:makeLabel("", UIFont.Medium, COLOR_ORANGE, "center")
	self.rollInitiativeButton = rollInitiativeNode:makeButton("Roll", self, self.doInitiativeRoll)
	self.startTurnButton = startTurnNode:makeButton("Start Turn", self, self.doStartTurn)

	-- Movement
	local movementTitleNode, movementActionsNode = movementArea:rows(2, 10)
	movementTitleNode:makeLabel("Movement", UIFont.Large, COLOR_WHITE, "left")
	local movementIconNode, movementStaticLabelNode, movementLabelNode, moveCalculationNode, moveStatusNode = movementActionsNode:cols({ 0.1, 0.3, 0.15, 0.15, 0.3 }, 5)
	self.movementIcon = movementIconNode
	movementStaticLabelNode:makeLabel("Move Points: ", UIFont.Medium, COLOR_WHITE, "left")
	self.movePointsLabel = movementLabelNode:makeLabel("", UIFont.Medium, COLOR_GREEN, "left")
	self.moveCalculationLabel = moveCalculationNode:makeLabel("", UIFont.Small, COLOR_WHITE, "left")
	self.moveStatusLabel = moveStatusNode:makeLabel("", UIFont.Medium, COLOR_ORANGE, "left")

	-- Attack
	local attacksTitleNode, targetingNode, primaryWeaponNode, secondaryWeaponNode = attacksArea:rows(4, 10)
	attacksTitleNode:makeLabel("Attack", UIFont.Large, COLOR_WHITE, "left")
	local targetTextNode, currTargetNode, clearTargetNode, selectTargetNode = targetingNode:cols({ 0.13, 0.57, 0.15, 0.15 }, 5)
	targetTextNode:makeLabel("Target: ", UIFont.Medium, COLOR_WHITE, "left")
	self.currentTargetLabel = currTargetNode:makeLabel("", UIFont.Medium, COLOR_ORANGE, "left", true)
	self.clearTargetButton = clearTargetNode:makeButton("Clear", self, self.doTargetClear)
	self.selectTargetButton = selectTargetNode:makeButton("Change", self, self.doSelectTarget)
	local primaryWeapIconNode, primaryWeapNameNode, primaryWeapHitCalcNode, primaryWeapAttackNode, primaryWeapDamageNode = primaryWeaponNode:cols({ 0.1, 0.45, 0.15, 0.15, 0.15 }, 5)
	self.primaryWeaponNode = primaryWeapIconNode
	self.primaryWeaponName = primaryWeapNameNode:makeLabel("", UIFont.Medium, COLOR_WHITE, "left", true)
	self.primaryWeaponHitCalculation = primaryWeapHitCalcNode:makeLabel("", UIFont.Small, COLOR_WHITE, "left")
	self.primaryAttackLabel = primaryWeapAttackNode:makeLabel("", UIFont.Medium, COLOR_ORANGE, "center")
	self.primaryAttackButton = primaryWeapAttackNode:makeButton("Attack", self, self.doPrimaryAttack)
	self.primaryDamageLabel = primaryWeapDamageNode:makeLabel("", UIFont.Medium, COLOR_RED, "center")
	self.primaryDamageButton = primaryWeapDamageNode:makeButton("Damage", self, self.doPrimaryDamage)
	local secondaryWeapIconNode, secondaryWeapNameNode, secondaryWeapHitCalcNode, secondaryWeapAttackNode, secondaryWeapDamageNode = secondaryWeaponNode:cols({ 0.1, 0.45, 0.15, 0.15, 0.15 }, 5)
	self.secondaryWeaponNode = secondaryWeapIconNode
	self.secondaryWeaponName = secondaryWeapNameNode:makeLabel("", UIFont.Medium, COLOR_WHITE, "left", true)
	self.secondaryWeaponHitCalculation = secondaryWeapHitCalcNode:makeLabel("", UIFont.Small, COLOR_WHITE, "left")
	self.secondaryAttackLabel = secondaryWeapAttackNode:makeLabel("", UIFont.Medium, COLOR_ORANGE, "center")
	self.secondaryAttackButton = secondaryWeapAttackNode:makeButton("Attack", self, self.doSecondaryAttack)
	self.secondaryDamageLabel = secondaryWeapDamageNode:makeLabel("", UIFont.Medium, COLOR_RED, "center")
	self.secondaryDamageButton = secondaryWeapDamageNode:makeButton("Damage", self, self.doSecondaryDamage)

	-- Defense
	local defenseTitleNode, defenseRollsNode, hpNode = defenseArea:rows(3, 10)
	defenseTitleNode:makeLabel("Defense", UIFont.Large, COLOR_WHITE, "left")
	local defendIconNode, defendLabelNode, defendCalcNode, _, defendButtonNode = defenseRollsNode:cols({ 0.1, 0.45, 0.15, 0.15, 0.15 }, 5)
	self.defendIcon = defendIconNode
	defendLabelNode:makeLabel("Dodge & Block", UIFont.Medium, COLOR_WHITE, "left")
	self.defenseCalculationLabel = defendCalcNode:makeLabel("", UIFont.Small, COLOR_WHITE, "left")
	defendButtonNode:makeButton("Defend", self, self.doDefendRoll)
	local hpIconNode, hpStaticLabelNode, hpLabelNode, hpCalcNode, reduceHpNode, increaseHpNode = hpNode:cols({ 0.1, 0.3, 0.15, 0.15, 0.15, 0.15 }, 5)
	self.hpIcon = hpIconNode
	hpStaticLabelNode:makeLabel("Hit Points: ", UIFont.Medium, COLOR_WHITE, "left")
	self.hitPointsLabel = hpLabelNode:makeLabel("50/50", UIFont.Medium, COLOR_GREEN, "left") --todo
	self.hpCalculationLabel = hpCalcNode:makeLabel("", UIFont.Small, COLOR_WHITE, "left")
	reduceHpNode:makeButton("Reduce", self, self.doReduceHitPoints)
	increaseHpNode:makeButton("Increase", self, self.doIncreaseHitPoints)

	-- Actions
	local actionsTitleNode, actionsBarNode = actionsArea:rows(2, 10)
	actionsTitleNode:makeLabel("Actions", UIFont.Large, COLOR_WHITE, "left")
	local actionsIconNode, actionsRemainingLabelNode, actionsRemainingNode, doActionNode, voidActionNode, endTurnNode = actionsBarNode:cols({ 0.1, 0.3, 0.15, 0.15, 0.15, 0.15 }, 5)
	self.actionsIcon = actionsIconNode
	actionsRemainingLabelNode:makeLabel("Remaining: ", UIFont.Medium, COLOR_WHITE, "left")
	self.actionsRemainingLabel = actionsRemainingNode:makeLabel("3/3", UIFont.Medium, COLOR_GREEN, "left")
	self.doActionButton = doActionNode:makeButton("Custom", self, self.doCustomAction)
	self.voidActionButton = voidActionNode:makeButton("Void", self, self.doVoidAction)
	self.endTurnButton = endTurnNode:makeButton("End Turn", self, self.doEndTurn)

	-- Add close button last to avoid overlap
	self.closeButton = win:corner("topRight", FONT_HGT_SMALL + 3, FONT_HGT_SMALL + 3):offset(9, -4):makeButton("X", self, self.onClose)
	self:onSystemChanged()
end

--- Refreshes the state of the whole UI except for the calculation labels depending on the player's state, equipped
--- weapons, position in the world etc.
function WDC_MainPanel:updateState()
	self.systemComboBox.disabled = WDC_PlayerState.instance().isTurnInProgress
	self:updateInitiativeState()
	self:updateDefenseState()
	local isInValidPosition = self:updateMovementState()
	self:updateAttacksState(isInValidPosition)
	self:updateActionsState(isInValidPosition)
end

function WDC_MainPanel:updateInitiativeState()
	local playerState = WDC_PlayerState.instance()
	if playerState.lastInitiative then
		self.rollInitiativeButton:setVisible(false)
		self.lastInitiativeLabel:setText(tostring(playerState.lastInitiative))
		self.startTurnButton:setVisible(not playerState.isTurnInProgress)
	else
		self.rollInitiativeButton:setVisible(true)
		self.lastInitiativeLabel:setText("")
		self.startTurnButton:setVisible(false)
	end
end

---@return boolean isInValidPosition true if position is reachable
function WDC_MainPanel:updateMovementState()
	local playerState = WDC_PlayerState.instance()
	local isInValidPosition = true
	local startingX = playerState.positionTurnStartX
	local startingY = playerState.positionTurnStartY
	if startingX and startingY then
		local player = getPlayer()
		local movementUsed = math.abs(math.floor(player:getX()) - startingX) + math.abs(math.floor(player:getY()) - startingY)
		local maxMovement = WDC_Systems.getMovementPerTurn(self:getCurrentDiceSystem())

		if movementUsed > maxMovement then
			WDC_PlayerState.tryToUseActionForMovement()
		else -- Then movement used <= maxMovement
			WDC_PlayerState.tryToReleaseActionUsedForMovement()
			self.moveStatusLabel:setText("")
		end

		if playerState.usingActionForMovement then
			maxMovement = maxMovement * 2
			self.moveStatusLabel:setText("-1 ACTION")
			self.moveStatusLabel.color = COLOR_ORANGE
		end

		local movementRemaining = maxMovement - movementUsed
		self.movePointsLabel:setText(tostring(movementRemaining) .. "/" .. tostring(maxMovement))
		if movementRemaining == maxMovement then
			self.movePointsLabel.color = COLOR_GREEN
		elseif movementRemaining < 1 then
			self.movePointsLabel.color = COLOR_RED
			if movementRemaining < 0 then
				self.moveStatusLabel:setText("INVALID MOVE")
				self.moveStatusLabel.color = COLOR_RED
				isInValidPosition = false
			end
		else
			self.movePointsLabel.color = COLOR_ORANGE
		end
	else
		self.movePointsLabel:setText("N/A")
		self.movePointsLabel.color = COLOR_WHITE
	end

	return isInValidPosition
end

function WDC_MainPanel:updateAttacksState(isInValidPosition)
	local playerState = WDC_PlayerState.instance()
	local remainingActions = WDC_PlayerState.getActionsRemaining()

	self.primaryWeapon = getPlayer():getPrimaryHandItem()
	self.secondaryWeapon = getPlayer():getSecondaryHandItem()
	self.primaryWeaponName:setText(WDC_Systems.getWeaponName(self.primaryWeapon))
	self.primaryWeaponHitCalculation:setText(WDC_Systems.getWeaponAttackCalculation(self.primaryWeapon, self:getCurrentDiceSystem()))
	local isTurnInProgress = playerState.isTurnInProgress
	local hasAttackedThisTurn = playerState.lastAttackRollPrimary ~= nil or playerState.lastAttackRollSecondary ~= nil
	local canAttackNow = ((isTurnInProgress and not hasAttackedThisTurn) or (not isTurnInProgress and not WDC_Systems.isHeavyWeapon(self.primaryWeapon))) and remainingActions > 0
	self.primaryAttackButton:setVisible(canAttackNow)
	self.primaryAttackButton.enable = isInValidPosition
	self.primaryDamageButton:setVisible(playerState.lastAttackRollPrimary ~= nil and playerState.lastDamageRollPrimary == nil and playerState.lastAttackRollPrimaryHitOrMiss ~= false)

	if playerState.currentTarget then
		local displayedName = playerState.currentTarget
		if WRC then displayedName = WRC.Meta.GetName(playerState.currentTarget) end
		self.currentTargetLabel:setText(displayedName)
	else
		self.currentTargetLabel:setText("Nobody")
	end

	if playerState.lastAttackRollPrimary then
		local rollString = tostring(playerState.lastAttackRollPrimary)
		if(playerState.lastAttackRollPrimaryHitOrMiss == nil) then
			self.primaryAttackLabel:setText(rollString)
			self.primaryAttackLabel.color = COLOR_ORANGE
		elseif playerState.lastAttackRollPrimaryHitOrMiss == true then
			self.primaryAttackLabel:setText("HIT (" .. rollString .. ")")
			self.primaryAttackLabel.color = COLOR_GREEN
		else
			self.primaryAttackLabel:setText("MISS (" .. rollString .. ")")
			self.primaryAttackLabel.color = COLOR_RED
		end
	else
		self.primaryAttackLabel:setText("")
	end

	if playerState.lastAttackRollSecondary then
		local rollString = tostring(playerState.lastAttackRollSecondary)
		if(playerState.lastAttackRollSecondaryHitOrMiss == nil) then
			self.secondaryAttackLabel:setText(rollString)
			self.secondaryAttackLabel.color = COLOR_ORANGE
		elseif playerState.lastAttackRollSecondaryHitOrMiss == true then
			self.secondaryAttackLabel:setText("HIT (" .. rollString .. ")")
			self.secondaryAttackLabel.color = COLOR_GREEN
		else
			self.secondaryAttackLabel:setText("MISS (" .. rollString .. ")")
			self.secondaryAttackLabel.color = COLOR_RED
		end
	else
		self.secondaryAttackLabel:setText("")
	end

	if playerState.lastDamageRollPrimary then
		self.primaryDamageLabel:setText("-" .. tostring(playerState.lastDamageRollPrimary) .. " HP")
	else
		self.primaryDamageLabel:setText("")
	end

	if playerState.lastDamageRollSecondary then
		self.secondaryDamageLabel:setText("-" .. tostring(playerState.lastDamageRollSecondary) .. " HP")
	else
		self.secondaryDamageLabel:setText("")
	end

	if self.primaryWeapon == self.secondaryWeapon then
		self.secondaryWeaponName:setText("")
		self.secondaryWeaponHitCalculation:setText("")
		self.secondaryAttackButton:setVisible(false)
		self.secondaryDamageButton:setVisible(false)
	else
		self.secondaryWeaponName:setText(WDC_Systems.getWeaponName(self.secondaryWeapon))
		self.secondaryWeaponHitCalculation:setText(WDC_Systems.getWeaponAttackCalculation(self.secondaryWeapon, self:getCurrentDiceSystem()))
		self.secondaryAttackButton:setVisible(canAttackNow)
		self.secondaryDamageButton:setVisible(playerState.lastAttackRollSecondary ~= nil and playerState.lastDamageRollSecondary == nil and playerState.lastAttackRollSecondaryHitOrMiss ~= false)
	end
	self.secondaryAttackButton.enable = isInValidPosition
end

function WDC_MainPanel:updateDefenseState()
	local playerState = WDC_PlayerState.instance()
	local maxHp = WDC_Systems.getMaxHitPoints(self:getCurrentDiceSystem())
	local hitpointsRemaining = maxHp - playerState.hitPointsLost
	self.hitPointsLabel:setText(tostring(hitpointsRemaining) .. "/" .. tostring(maxHp))
	if(playerState.hitPointsLost == 0) then
		self.hitPointsLabel.color = COLOR_GREEN
	elseif playerState.hitPointsLost >= maxHp then
		self.hitPointsLabel.color = COLOR_RED
	else
		self.hitPointsLabel.color = COLOR_ORANGE
	end
end

function WDC_MainPanel:updateActionsState(isInValidPosition)
	local playerState = WDC_PlayerState.instance()
	local remainingActions = WDC_PlayerState.getActionsRemaining()
	self.actionsRemainingLabel:setText(tostring(remainingActions) .. "/" .. tostring(WDC_PlayerState.MAX_ACTIONS))
	if remainingActions == WDC_PlayerState.MAX_ACTIONS then
		self.actionsRemainingLabel.color = COLOR_GREEN
	elseif remainingActions < 1 then
		self.actionsRemainingLabel.color = COLOR_RED
	else
		self.actionsRemainingLabel.color = COLOR_ORANGE
	end
	self.doActionButton:setVisible(remainingActions > 0 and playerState.isTurnInProgress)
	self.voidActionButton:setVisible(WDC_PlayerState.canRefundNormalAction())
	self.endTurnButton:setVisible(playerState.isTurnInProgress)
	self.endTurnButton.enable = isInValidPosition
end

function WDC_MainPanel:getCurrentDiceSystem()
	return self.systemComboBox:getOptionData(self.systemComboBox.selected)
end

function WDC_MainPanel:onSystemChanged()
	local selectedSystem = self:getCurrentDiceSystem()
	WDC_PlayerState.instance().currentSystemKey = selectedSystem.getKey()
	self.systemDescription:setText(selectedSystem.getDescription())
	self.initiativeCalcLabel:setText(WDC_Systems.getInitiativeDisplayText(selectedSystem))
	self.moveCalculationLabel:setText(WDC_Systems.getMovementPerTurnCalculation(self:getCurrentDiceSystem()))
	self.defenseCalculationLabel:setText(WDC_Systems.getDefensiveRollCalculation(getPlayer(), self:getCurrentDiceSystem()))
	self.hpCalculationLabel:setText(WDC_Systems.getHitPointsCalculation(self:getCurrentDiceSystem()))
	self:updateState() -- The modifiers will change here too
end

function WDC_MainPanel:doInitiativeRoll()
	getSoundManager():playUISound("DiceRoll")
	local playerState = WDC_PlayerState.instance()
	playerState.lastInitiative = WDC_Systems.rollInitiative(self:getCurrentDiceSystem())
	self:updateState()
end

function WDC_MainPanel:doStartTurn()
	getSoundManager():playUISound("BladeDrawQuick")
	local playerState = WDC_PlayerState.instance()
	WDC_Systems.turnStarted(self:getCurrentDiceSystem(), playerState.lastInitiative, playerState.positionTurnEndX, playerState.positionTurnEndY)
	WDC_PlayerState.newTurn()
	self.groundHighlighter:remove()
	self:updateState()
end

function WDC_MainPanel:doEndTurn()
	getSoundManager():playUISound("ButtonClick1")
	WDC_PlayerState.endTurn()
	WDC_Systems.turnEnded(self:getCurrentDiceSystem(),  WDC_PlayerState.getActionsRemaining())
	self.groundHighlighter:remove()
	self:updateState()
end

function WDC_MainPanel:doTargetClear()
	self:onNewTargetSelected(nil)
end

function WDC_MainPanel:doSelectTarget()
	WL_SelectPlayersPanel:show(self, self.onNewTargetSelected, {
		includeSelf = false,
		onlyInLOS = true,
	})
end

function WDC_MainPanel:onNewTargetSelected(username)
	WDC_PlayerState.instance().currentTarget = username
	self:updateState()
end

function WDC_MainPanel:doReduceHitPoints()
	WL_TextEntryPanel:show("Enter number of hit points to reduce by", self, self.onReduceHp)
end

function WDC_MainPanel:doIncreaseHitPoints()
	WL_TextEntryPanel:show("Enter number of hit points to increase by", self, self.onIncreaseHp)
end

function WDC_MainPanel:onReduceHp(hpCount)
	self:onIncreaseHp(-math.abs(hpCount))
end

function WDC_MainPanel:onIncreaseHp(hpCount)
	local number = tonumber(hpCount)
	if number then
		number = math.floor(number)
		local playerState = WDC_PlayerState.instance()
		local currentHpLost = playerState.hitPointsLost
		playerState.hitPointsLost = currentHpLost - number
		if playerState.hitPointsLost < 0 then
			playerState.hitPointsLost = 0
		end

		if currentHpLost ~= playerState.hitPointsLost then
			WDC_Systems.reportChangedHitPoints(self:getCurrentDiceSystem(), currentHpLost, playerState.hitPointsLost)
			self:updateState()
		end
	end
end

function WDC_MainPanel:doDefendRoll()
	getSoundManager():playUISound("MetalWooshHit")
	WDC_Systems.rollDefense(self:getCurrentDiceSystem())
	self:updateState()
end

---@param weapon InventoryItem can be nil - this means unarmed
local function doAttackSound(weapon)
	if weapon and weapon:IsWeapon() then
		getPlayer():playSound(weapon:getSwingSound())
	else
		getPlayer():playSound("PunchSwoosh")
	end
end

---@param weapon InventoryItem can be nil - this means unarmed
local function doDamageSound(weapon)
	if weapon and weapon:IsWeapon() then
		getPlayer():playSound(weapon:getZombieHitSound())
	else
		getPlayer():playSound("PunchHitCrunch")
	end
end

function WDC_MainPanel:doPrimaryAttack()
	local playerState = WDC_PlayerState.instance()
	playerState.lastAttackRollPrimaryHitOrMiss, playerState.lastAttackRollPrimary = WDC_Systems.rollAttack(self.primaryWeapon, self:getCurrentDiceSystem(), playerState.currentTarget, not playerState.isTurnInProgress)
	playerState.lastDamageRollPrimary = nil
	WDC_PlayerState.actionUsed()
	doAttackSound(self.primaryWeapon)
	self:updateState()
end

function WDC_MainPanel:doPrimaryDamage()
	local playerState = WDC_PlayerState.instance()
	local isTwoHanded = self.primaryWeapon == self.secondaryWeapon
	playerState.lastDamageRollPrimary = WDC_Systems.rollDamage(self.primaryWeapon, isTwoHanded, self:getCurrentDiceSystem(), playerState.currentTarget)
	doDamageSound(self.primaryWeapon)
	self:updateState()
end

function WDC_MainPanel:doSecondaryAttack()
	local playerState = WDC_PlayerState.instance()
	playerState.lastAttackRollSecondaryHitOrMiss, playerState.lastAttackRollSecondary = WDC_Systems.rollAttack(self.secondaryWeapon, self:getCurrentDiceSystem(), playerState.currentTarget, not playerState.isTurnInProgress)
	playerState.lastDamageRollSecondary = nil
	WDC_PlayerState.actionUsed()
	doAttackSound(self.secondaryWeapon)
	self:updateState()
end

function WDC_MainPanel:doSecondaryDamage()
	local playerState = WDC_PlayerState.instance()
	playerState.lastDamageRollSecondary = WDC_Systems.rollDamage(self.secondaryWeapon, false, self:getCurrentDiceSystem(), playerState.currentTarget)
	doDamageSound(self.secondaryWeapon)
	self:updateState()
end


function WDC_MainPanel:doCustomAction()
	WL_TextEntryPanel:show("Describe the action you are taking", self, self.onCustomAction)
end

function WDC_MainPanel:onCustomAction(description)
	local actionsBefore = WDC_PlayerState.getActionsRemaining()
	WDC_PlayerState.actionUsed()
	WDC_Systems.performedAction(self:getCurrentDiceSystem(), description, actionsBefore, WDC_PlayerState.getActionsRemaining())
	self:updateState()
end

function WDC_MainPanel:doVoidAction()
	WL_TextEntryPanel:show("Explain the action you are voiding and why", self, self.onVoidAction)
end

function WDC_MainPanel:onVoidAction(description)
	local actionsBefore = WDC_PlayerState.getActionsRemaining()
	WDC_PlayerState.refundAction()
	WDC_Systems.voidedAction(self:getCurrentDiceSystem(), description, actionsBefore, WDC_PlayerState.getActionsRemaining())
	self:updateState()
end

function WDC_MainPanel:prerender()
	ISPanel.prerender(self)
	if self.groundHighlighter.type == "none" then
		self:showHightlight()
	end
end

function WDC_MainPanel:showHightlight()
	local playerState = WDC_PlayerState.instance()
	if playerState.isTurnInProgress then
		local startingX = playerState.positionTurnStartX
		local startingY = playerState.positionTurnStartY
		if startingX and startingY then
			local movementPerTurn = WDC_Systems.getMovementPerTurn(self:getCurrentDiceSystem())
			self.groundHighlighter:resetColor()
			self.groundHighlighter:setColor(0.25, 0.65, 0.15, 0.7)
			self.groundHighlighter:addColorStop(1, 0.3, 0.7, 0.2, 0.5)
			self.groundHighlighter:addColorStop(movementPerTurn+1, 0.81, 0.9, 0.09, 0.5)
			self.groundHighlighter:highlightCircleManhattan(startingX, startingY, movementPerTurn *2, 0, 8)
		end
	else
		local endingX = playerState.positionTurnEndX
		local endingY = playerState.positionTurnEndY
		if endingX and endingY then
			self.groundHighlighter:resetColor()
			self.groundHighlighter:setColor(0.8, 0.1, 0.1, 0.7)
			self.groundHighlighter:highlightCircleManhattan(endingX, endingY, 0, getPlayer():getZ(), getPlayer():getZ())
		end
	end
end

function WDC_MainPanel:render()
	self:drawTexture(getTexture("media/ui/Icon_Shield.png"), self.defendIcon.left, self.defendIcon.top, 1, 1.0, 1.0, 1.0)
	self:drawTexture(getTexture("media/ui/Icon_Grab.png"), self.actionsIcon.left, self.actionsIcon.top, 1, 1.0, 1.0, 1.0)
	self:drawTexture(getTexture("media/ui/Icon_Movement.png"), self.movementIcon.left, self.movementIcon.top, 1, 1.0, 1.0, 1.0)
	self:drawTexture(getTexture("media/ui/Icon_Heart.png"), self.hpIcon.left, self.hpIcon.top, 1, 1.0, 1.0, 1.0)
	self:drawTexture(getTexture("media/ui/initiative_icon.png"), self.initiativeIcon.left, self.initiativeIcon.top, 1, 1.0, 1.0, 1.0)
	if self.primaryWeapon then
		self:drawTexture(self.primaryWeapon:getTex(), self.primaryWeaponNode.left, self.primaryWeaponNode.top, 1, 1.0, 1.0, 1.0)
	else
		self:drawTexture(getTexture("media/ui/Icon_Melee_Fist.png"), self.primaryWeaponNode.left, self.primaryWeaponNode.top, 1, 1.0, 1.0, 1.0)
	end

	if self.secondaryWeapon then
		local alpha = 1.0
		if self.secondaryWeapon == self.primaryWeapon then
			alpha = 0.5
		end
		self:drawTexture(self.secondaryWeapon:getTex(), self.secondaryWeaponNode.left, self.secondaryWeaponNode.top, alpha, 1.0, 1.0, 1.0)
	else
		local alpha = 1.0
		if not self.primaryWeapon then
			alpha = 0.5
		end
		self:drawTexture(getTexture("media/ui/Icon_Melee_Fist.png"), self.secondaryWeaponNode.left, self.secondaryWeaponNode.top, alpha, 1.0, 1.0, 1.0)
	end
end

function WDC_MainPanel.trackMovement()
	local mainPanel = WDC_MainPanel.instance
	if not mainPanel or not WDC_PlayerState.instance().isTurnInProgress then
		return
	end

	local player = getPlayer()
	local x = math.floor(player:getX())
	local y = math.floor(player:getY())
	if x ~= mainPanel.lastX or y ~= mainPanel.lastY then
		mainPanel.lastX = x
		mainPanel.lastY = y
		mainPanel:updateState()
	end
end

function WDC_MainPanel:onClose()
	self:removeFromUIManager()
	WDC_MainPanel.instance = nil
end

function WDC_MainPanel:removeFromUIManager()
	Events.OnPlayerMove.Remove(WDC_MainPanel.trackMovement)
	self.groundHighlighter:remove()
	ISPanelJoypad.removeFromUIManager(self)
end

Events.OnEquipPrimary.Add(function()
	if WDC_MainPanel.instance ~= nil then
		--TODO delay for 4 seconds then check if weapon has changed?
		WDC_MainPanel.instance:updateState()
	end
end)

Events.OnEquipSecondary.Add(function()
	if WDC_MainPanel.instance ~= nil then
		--TODO delay for 4 seconds then check if weapon has changed?
		WDC_MainPanel.instance:updateState()
	end
end)