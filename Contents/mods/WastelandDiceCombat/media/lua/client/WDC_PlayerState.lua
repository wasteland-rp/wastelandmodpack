---
--- WDC_PlayerState.lua
---
--- Singleton. Keeps track of the current state of a player in dice combat, such as their rolls, turn status and actions
---
--- 26/04/2024


WDC_PlayerState = {}
WDC_PlayerState.data = nil

function WDC_PlayerState.instance()
	if not WDC_PlayerState.data then
		WDC_PlayerState.createBlankData()
	end
	return WDC_PlayerState.data
end

function WDC_PlayerState.createBlankData()
	WDC_PlayerState.data = {
		currentSystemKey = "PSH",
		isTurnInProgress = false,
		usingActionForMovement = false,
		hitPointsLost = 0,
		actionsUsed = 0,
		positionTurnStartX = nil,
		positionTurnStartY = nil,
		positionTurnEndX = nil,
		positionTurnEndY = nil,
		lastInitiative = nil,
		lastAttackRollPrimary = nil,
		lastAttackRollPrimaryHitOrMiss = nil,
		lastAttackRollSecondary = nil,
		lastAttackRollSecondaryHitOrMiss = nil,
		lastDamageRollPrimary = nil,
		lastDamageRollSecondary = nil,
		currentTarget = nil,
	}
	return WDC_PlayerState.data
end

WDC_PlayerState.MAX_ACTIONS = 3

function WDC_PlayerState.getActionsRemaining()
	local playerState = WDC_PlayerState.instance()
	return WDC_PlayerState.MAX_ACTIONS - playerState.actionsUsed
end

function WDC_PlayerState.actionUsed()
	local data = WDC_PlayerState.instance()
	if data.actionsUsed < WDC_PlayerState.MAX_ACTIONS then
		data.actionsUsed = data.actionsUsed + 1
	end
end

function WDC_PlayerState.tryToUseActionForMovement()
	local playerState = WDC_PlayerState.instance()
	if not playerState.usingActionForMovement and WDC_PlayerState.getActionsRemaining() > 0 then
		WDC_PlayerState.actionUsed()
		playerState.usingActionForMovement = true
	end
end

function WDC_PlayerState.tryToReleaseActionUsedForMovement()
	local playerState = WDC_PlayerState.instance()
	if playerState.usingActionForMovement then
		WDC_PlayerState.refundAction()
		playerState.usingActionForMovement = false
	end
end

function WDC_PlayerState.canRefundNormalAction()
	local playerState = WDC_PlayerState.instance()
	if playerState.actionsUsed == 0 then return false end
	if playerState.actionsUsed == 1 and playerState.usingActionForMovement then return false end
	return true
end

function WDC_PlayerState.refundAction()
	local playerState = WDC_PlayerState.instance()
	if playerState.actionsUsed > 0 then
		playerState.actionsUsed = playerState.actionsUsed - 1
	end
end

function WDC_PlayerState.newTurn()
	local data = WDC_PlayerState.instance()
	data.isTurnInProgress = true
	data.actionsUsed = 0
	data.usingActionForMovement = false
	data.lastAttackRollPrimary = nil
	data.lastAttackRollPrimaryHitOrMiss = nil
	data.lastAttackRollSecondary = nil
	data.lastAttackRollSecondaryHitOrMiss = nil
	data.lastDamageRollPrimary = nil
	data.lastDamageRollSecondary = nil
	data.positionTurnEndX = nil
	data.positionTurnEndY = nil
	local player = getPlayer()
	data.positionTurnStartX = math.floor(player:getX())
	data.positionTurnStartY = math.floor(player:getY())
end

function WDC_PlayerState.endTurn()
	local data = WDC_PlayerState.instance()
	data.isTurnInProgress = false
	data.lastInitiative = nil
	data.lastAttackRollPrimary = nil
	data.lastAttackRollPrimaryHitOrMiss = nil
	data.lastAttackRollSecondary = nil
	data.lastAttackRollSecondaryHitOrMiss = nil
	data.lastDamageRollPrimary = nil
	data.lastDamageRollSecondary = nil
	data.positionTurnStartX = nil
	data.positionTurnStartY = nil
	local player = getPlayer()
	data.positionTurnEndX = math.floor(player:getX())
	data.positionTurnEndY = math.floor(player:getY())
end
