---
--- WDC_System_Hybrid.lua
--- 22/04/2024
---

require "dicesystem/WDC_Systems"

WDC_System_Hybrid = {}

function WDC_System_Hybrid.getName()
	return "Trait-Skill Hybrid"
end

function WDC_System_Hybrid.getKey()
	return "TSH"
end

function WDC_System_Hybrid.getDescription()
	return "Modifiers are calculated by: 2 x Trait level, +1 for skill levels 4, 6, 8 and 10"
end

function WDC_System_Hybrid.getFullModifier(player, perk)
	local perkLevel, skillLevel = WDC_Systems.getSkillValues(player, perk)
	local modifier = perkLevel * 2

	if perk == Perks.Nimble then
		if skillLevel > 4 then
			modifier = modifier + 4
		elseif skillLevel > 3 then
			modifier = modifier + 3
		elseif skillLevel > 2 then
			modifier = modifier + 2
		elseif skillLevel > 1 then
			modifier = modifier + 1
		end
	else
		if skillLevel > 9 then
			modifier = modifier + 4
		elseif skillLevel > 7 then
			modifier = modifier + 3
		elseif skillLevel > 5 then
			modifier = modifier + 2
		elseif skillLevel > 3 then
			modifier = modifier + 1
		end
	end
	return modifier
end

function WDC_System_Hybrid.getHalfModifier(player, perk)
	local perkLevel, skillLevel = WDC_Systems.getSkillValues(player, perk)
	local modifier = perkLevel

	if perk == Perks.Nimble then
		if skillLevel > 4 then
			modifier = modifier + 2
		elseif skillLevel > 2 then
			modifier = modifier + 1
		end
	else
		if skillLevel > 7 then
			modifier = modifier + 2
		elseif skillLevel > 5 then
			modifier = modifier + 1
		end
	end
	return modifier
end

WDC_Systems.RegisterSystem(WDC_System_Hybrid)