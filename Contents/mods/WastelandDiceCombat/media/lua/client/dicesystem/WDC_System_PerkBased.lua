---
--- WDC_System_PerkBased.lua
--- 26/04/2024
---

require "dicesystem/WDC_Systems"

WDC_System_PerkBased = {}

function WDC_System_PerkBased.getName()
	return "Trait Based"
end

function WDC_System_PerkBased.getKey()
	return "TB"
end

function WDC_System_PerkBased.getDescription()
	return "Modifiers are calculated by: 1 + 3 x Trait level, skill points ignored"
end

function WDC_System_PerkBased.getFullModifier(player, perk)
	local perkLevel = WDC_Systems.getSkillValues(player, perk)
	return 1 + (perkLevel * 3)
end

function WDC_System_PerkBased.getHalfModifier(player, perk)
	local perkLevel = WDC_Systems.getSkillValues(player, perk)
	return math.min(perkLevel * 2, 5)
end

WDC_Systems.RegisterSystem(WDC_System_PerkBased)