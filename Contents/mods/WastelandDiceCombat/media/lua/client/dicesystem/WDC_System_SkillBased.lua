---
--- WDC_System_SkillBased.lua
--- 22/04/2024
---

require "dicesystem/WDC_Systems"

WDC_System_SkillBased = {}

function WDC_System_SkillBased.getName()
	return "Skill Based"
end

function WDC_System_SkillBased.getKey()
	return "SB"
end

function WDC_System_SkillBased.getDescription()
	return "Modifiers are calculated by the value of your current skill level (0-10)"
end

function WDC_System_SkillBased.getFullModifier(player, perk)
	local perkLevel, skillLevel = WDC_Systems.getSkillValues(player, perk)
	if perk == Perks.Nimble then
		local cappedNimble = math.min(skillLevel, 5)
		return cappedNimble * 2
	end
	return skillLevel
end

function WDC_System_SkillBased.getHalfModifier(player, perk)
	local perkLevel, skillLevel = WDC_Systems.getSkillValues(player, perk)
	if perk == Perks.Nimble then
		return math.min(skillLevel, 5)
	end
	return WDC_Systems.integerDivision(skillLevel, 2)
end

WDC_Systems.RegisterSystem(WDC_System_SkillBased)