---
--- WDC_Systems.lua
--- 22/04/2024
---
require "WL_Utils"
require "WRC"

WDC_Systems = {}
WDC_SystemList = {}

function WDC_Systems.RegisterSystem(system)
	WDC_SystemList[system.getKey()] = system
end

local function reportSystemMechanics(string, system)
	local output = "[" .. system.getKey() .. "] " .. string
	if WRC and isClient() then
		WRC.SendLocalOOC(output, "yell")
	else
		print(output) -- Single Player / No WRC fallback
	end
end

local function emoteAction(string)
	if WRC and isClient() then
		WRC.SendLocalChat("/meyell " .. string, "yell")
	else
		getPlayer():Say("* " .. getPlayer():getDisplayName() .. string) -- Single Player / No WRC fallback
	end
end

function WDC_Systems.turnStarted(system, initiativeRoll, positionTurnEndX, positionTurnEndY)
	local playedMovementWarning = ""
	if positionTurnEndX and positionTurnEndY then
		local player = getPlayer()
		local x = math.floor(player:getX())
		local y = math.floor(player:getY())
		local movement = math.abs(x - positionTurnEndX) + math.abs(y - positionTurnEndY)
		if movement > 0 then
			playedMovementWarning = " [POSSIBLE ILLEGAL LOCATION: MOVED " ..  tostring(movement) .. " TILES SINCE END OF LAST TURN!]"
		end
	end

	reportSystemMechanics("Started Turn. Initiative: " .. tostring(initiativeRoll) .. playedMovementWarning, system)
	emoteAction("starts turn (Initiative: " .. tostring(initiativeRoll) .. ")" .. playedMovementWarning)
end

function WDC_Systems.turnEnded(system, actionsRemaining)
	reportSystemMechanics("Ended Turn, actions remaining: " .. tostring(actionsRemaining), system)
	emoteAction("ended turn (" .. tostring(actionsRemaining) .. " actions remain)" )
end

local function describeActionChange(actionsBefore, actionsAfter)
	return " (" .. tostring(actionsBefore) .. " -> " .. tostring(actionsAfter) .. ")"
end

function WDC_Systems.performedAction(system, description, actionsBefore, actionsAfter)
	reportSystemMechanics("Performed Action: " .. description .. describeActionChange(actionsBefore, actionsAfter), system)
	emoteAction(" performs action: " .. description)
end

function WDC_Systems.voidedAction(system, description, actionsBefore, actionsAfter)
	reportSystemMechanics("VOIDED an action: " .. description .. describeActionChange(actionsBefore, actionsAfter), system)
	emoteAction(" VOIDS action: " .. description)
end

--- Gets the perk of a weapon, do not call this with nil or without checking item:IsWeapon() = true
---@param weapon InventoryItem can be nil - this means unarmed / nimble
---@return PerkFactory.Perk perk used for this weapon e.g. Long Blunt, Short Blade
---@see PerkFactory.Perks
function WDC_Systems.getWeaponPerk(weapon)
	if not weapon or not weapon:IsWeapon() then
		return Perks.Nimble
	end

	if weapon:isRanged() then
		return Perks.Aiming
	end

	local categories = weapon:getScriptItem():getCategories()
	if categories:contains("Axe") then
		return Perks.Axe
	end
	if categories:contains("Blunt") then
		return Perks.Blunt
	end
	if categories:contains("Spear") then
		return Perks.Spear
	end
	if categories:contains("LongBlade") then
		return Perks.LongBlade
	end
	if categories:contains("SmallBlade") then
		return Perks.SmallBlade
	end
	if categories:contains("SmallBlunt") then
		return Perks.SmallBlunt
	end
end

---@param weapon InventoryItem can be nil - this means unarmed
---@return string never nil
function WDC_Systems.getWeaponName(weapon)
	if weapon and weapon:IsWeapon() then
		return weapon:getName()
	else
		return "Unarmed"
	end
end

local function getWSKPerkBoosts()
	if not WDC_PerkBoosts then
		WDC_PerkBoosts = WSK_Lib.GetPerkBoosts()
	end
	return WDC_PerkBoosts
end

local function getWSKPerkBoost(perk)
	local boost = getWSKPerkBoosts()[perk:getId()]
	if not boost then return 0 end
	boost = math.max(boost, 0) -- Sometimes skill keeper gives negatives
	return boost
end

---@param perk PerkFactory.Perks value
---@return number, number perkLevel, skillLevel
function WDC_Systems.getSkillValues(player, perk)
	local perkLevel
	if perk == Perks.Strength or perk == Perks.Fitness then
		if player ~= getPlayer() then error("No data for Strength and Fitness values") end
		perkLevel = getWSKPerkBoost(perk)
	else
		perkLevel = player:getXp():getPerkBoost(perk)
	end

	local skillLevel = player:getPerkLevel(perk)
	return perkLevel, skillLevel
end

function WDC_Systems.rollD20()
	return ZombRand(1, 21)
end

function WDC_Systems.rollD6(diceCount)
	local count = diceCount or 1 -- If diceCount is nil, set count to 1
	local total = 0
	for i = 1, count do
		total = total + ZombRand(1, 7) -- Roll a D6 and add it to the total
	end
	return total
end

--- Divides using integer division, always returning a whole number
function WDC_Systems.integerDivision(dividend, divisor)
	return (dividend - dividend % divisor) / divisor
end

---@return string text showing the initiative calculation for this system
function WDC_Systems.getInitiativeDisplayText(system)
	return "D20 + " .. tostring(system.getFullModifier(getPlayer(), Perks.Lightfoot))
end

function WDC_Systems.rollInitiative(system)
	local diceRollSum = WDC_Systems.rollD20()
	local modifierBonus = system.getFullModifier(getPlayer(), Perks.Lightfoot)
	local totalRoll = diceRollSum + modifierBonus
	reportSystemMechanics("Initiative: (D20 = " .. tostring(diceRollSum) .. ") + " .. tostring(modifierBonus) .. " = " .. tostring(totalRoll), system)
	emoteAction("rolls initiative: " .. tostring(totalRoll))
	return totalRoll
end

local function makeD20ResultText(diceRollSum, modifierBonus, totalRoll)
	return "(D20 = " .. tostring(diceRollSum) .. ") + " .. tostring(modifierBonus) .. " = " .. tostring(totalRoll)
end

---@param weapon InventoryItem
function WDC_Systems.isHeavyWeapon(weapon)
	if not weapon or not weapon:IsWeapon() or weapon:isRanged() then
		return false
	end

	local item = ScriptManager.instance:getItem(weapon:getFullType())
	if item and item:getSwingAnim() then
		return item:getSwingAnim() == "Heavy"
	else
		return false
	end
end

local function getDefenseModifier(playerAttacked, system)
	local modifierBonus = system.getFullModifier(playerAttacked, Perks.Nimble)
	local weapon = playerAttacked:getPrimaryHandItem()
	if WDC_Systems.isHeavyWeapon(weapon) then
		modifierBonus = 0
	elseif weapon and weapon:IsWeapon() and not weapon:isRanged() then
		local weaponPerk = WDC_Systems.getWeaponPerk(weapon)
		modifierBonus = modifierBonus + system.getHalfModifier(playerAttacked, weaponPerk)
	end
	return modifierBonus
end

function WDC_Systems.getDefensiveRollCalculation(playerAttacked, system)
	return "D20 + " .. tostring(getDefenseModifier(playerAttacked, system))
end

function WDC_Systems.getDefensiveRoll(playerAttacked, system)
	local diceRollSum = WDC_Systems.rollD20()
	local modifierBonus = getDefenseModifier(playerAttacked, system)
	local totalRoll = diceRollSum + modifierBonus
	local rollDisplayText = makeD20ResultText(diceRollSum, modifierBonus, totalRoll)
	return totalRoll, rollDisplayText
end

function WDC_Systems.rollDefense(system)
	local totalRoll, rollDisplayText = WDC_Systems.getDefensiveRoll(getPlayer(), system)
	emoteAction("rolls defense: " .. rollDisplayText)
end

local function getAttackModifier(weapon, system)
	local weaponPerk = WDC_Systems.getWeaponPerk(weapon)
	local modifier = system.getFullModifier(getPlayer(), weaponPerk) + system.getHalfModifier(getPlayer(), Perks.Strength)
	return modifier
end

---@param weapon InventoryItem can be nil - this means unarmed / nimble
---@return string weaponAttackCalculation
function WDC_Systems.getWeaponAttackCalculation(weapon, system)
	return "D20 + " .. tostring(getAttackModifier(weapon, system))
end

---@param targetPlayerUsername string or nil
function WDC_Systems.rollAttack(weapon, system, targetPlayerUsername, isOppurtunityAttack)
	local diceRollSum = WDC_Systems.rollD20()
	local modifierBonus = getAttackModifier(weapon, system)
	local totalRoll = diceRollSum + modifierBonus
	local playerTarget = WL_Utils.findPlayerFromUsername(targetPlayerUsername)

	local attackTextMechanics = "Attack: "
	local attackTextEmote = "attacks "
	if isOppurtunityAttack then
		attackTextMechanics = "Opportunity Attack: "
		attackTextEmote = "opportunity attacks "
	end

	if not playerTarget then
		emoteAction(attackTextEmote .. "using " .. WDC_Systems.getWeaponName(weapon) .. ": " .. makeD20ResultText(diceRollSum, modifierBonus, totalRoll))
		return nil, totalRoll
	else
		local defensiveRoll, rollDisplayText = WDC_Systems.getDefensiveRoll(playerTarget, system)
		local isHit = totalRoll >= defensiveRoll
		local hitOrMissText = " - MISS!"
		local emoteHitOrMiss = " but misses"
		if isHit then
			hitOrMissText = " - HIT!"
			emoteHitOrMiss = " and hits!"
		end
		local targetName = WL_Utils.getRolePlayChatName(targetPlayerUsername)
		emoteAction(attackTextEmote .. targetName .. " using " .. WDC_Systems.getWeaponName(weapon) .. ": "
				.. makeD20ResultText(diceRollSum, modifierBonus, totalRoll) .. " vs " .. targetName .. " "
				.. rollDisplayText .. hitOrMissText)
		return isHit, totalRoll
	end
end

local function reportDamageRoll(numberOfD6, modifierBonus, system, targetPlayerUsername)
	local diceRollSum = WDC_Systems.rollD6(numberOfD6)
	local totalRoll = diceRollSum + modifierBonus

	local calculationStr = "(" .. tostring(numberOfD6) .. "D6 = " .. tostring(diceRollSum) .. ") + " .. tostring(modifierBonus) .. " = " .. tostring(totalRoll)
	local damageCalc

	if targetPlayerUsername then
		damageCalc = "damages " .. WL_Utils.getRolePlayChatName(targetPlayerUsername) .. " for " .. tostring(totalRoll) .. " hit points! " .. calculationStr
	else
		damageCalc = "inflicted damage: " .. calculationStr
	end
	return totalRoll, damageCalc
end

local function rollWeaponDamage(weapon, isTwoHanded, system, targetPlayerUsername)
	if not weapon then -- Unarmed
		return reportDamageRoll(1, system.getHalfModifier(getPlayer(), Perks.Strength), system, targetPlayerUsername)
	end

	local weaponModifier = system.getFullModifier(getPlayer(), WDC_Systems.getWeaponPerk(weapon))
	if isTwoHanded then
		if WDC_Systems.isHeavyWeapon(weapon) then
			return reportDamageRoll(3, system.getFullModifier(getPlayer(), Perks.Strength) + weaponModifier, system, targetPlayerUsername)
		end
		return reportDamageRoll(3, system.getHalfModifier(getPlayer(), Perks.Strength) + weaponModifier, system, targetPlayerUsername)
	else
		local highestStrFit = math.max(system.getHalfModifier(getPlayer(), Perks.Strength), system.getHalfModifier(getPlayer(), Perks.Fitness))
		return reportDamageRoll(2,  highestStrFit + weaponModifier, system, targetPlayerUsername)
	end
end

function WDC_Systems.rollDamage(weapon, isTwoHanded, system, targetPlayerUsername)
	local damageRoll, damageCalc = rollWeaponDamage(weapon, isTwoHanded, system, targetPlayerUsername)
	emoteAction(damageCalc)
	return damageRoll
end

function WDC_Systems.getMovementPerTurnCalculation(system)
	return "7 + " .. system.getFullModifier(getPlayer(), Perks.Sprinting)
end

function WDC_Systems.getMovementPerTurn(system)
	return 7 + system.getFullModifier(getPlayer(), Perks.Sprinting)
end

function WDC_Systems.getHitPointsCalculation(system)
	return "30 + " .. system.getFullModifier(getPlayer(), Perks.Fitness)
end

function WDC_Systems.getMaxHitPoints(system)
	return 30 + system.getFullModifier(getPlayer(), Perks.Fitness)
end

function WDC_Systems.reportChangedHitPoints(system, oldHitPointsLost, newHitPointsLost)
	local maxHp = WDC_Systems.getMaxHitPoints(system)
	local oldHp = maxHp - oldHitPointsLost
	local newHp = maxHp - newHitPointsLost
	local difference = newHp - oldHp
	local gainedOrLost = "GAINED "
	if oldHp > newHp then
		gainedOrLost = "lost "
	end
	local localHpChangeStr = "(" .. tostring(oldHp) .. "/" .. tostring(maxHp) .. " -> " .. tostring(newHp) .. "/" .. tostring(maxHp)  .. ")"
	reportSystemMechanics("Hit Points: " .. tostring(difference) .. " " ..  localHpChangeStr, system)
	emoteAction(gainedOrLost .. tostring(math.abs(difference)) .. " hit points " .. localHpChangeStr)
end

function WDC_Systems.getSkillCalculation(perk, system)
	return "D20 + " .. tostring(system.getFullModifier(getPlayer(), perk))
end

function WDC_Systems.rollSkill(perk, system)
	local diceRollSum = WDC_Systems.rollD20()
	local modifierBonus = system.getFullModifier(getPlayer(), perk)
	local totalRoll = diceRollSum + modifierBonus
	emoteAction("rolls " .. perk:getName() .. ": " .. makeD20ResultText(diceRollSum, modifierBonus, totalRoll))
end
