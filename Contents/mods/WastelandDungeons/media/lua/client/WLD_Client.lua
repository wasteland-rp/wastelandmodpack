---
--- WLD_Client.lua
--- 01/11/2023
---

if not isClient() then return end

WLD_Client = {}

function WLD_Client.enterDungeon(player, dungeonId)
    checkPositionCooldown = getTimestamp() + 10
    sendClientCommand(player, "WastelandDungeons", "enterDungeon", { dungeonId = dungeonId })
end

function WLD_Client.completeObjective(player, dungeonId, objectiveId)
    sendClientCommand(player, "WastelandDungeons", "completeObjective", { dungeonId = dungeonId, objectiveId = objectiveId })
end

function WLD_Client.leaveDungeon(player, dungeonId)
    sendClientCommand(player, "WastelandDungeons", "leaveDungeon", { dungeonId = dungeonId })
end

function WLD_Client.activateTrigger(player, dungeonId, triggerId)
    sendClientCommand(player, "WastelandDungeons", "activateTrigger", { dungeonId = dungeonId, triggerId = triggerId })
end

function WLD_Client.logLootRewards(player, dungeonId, loot)
    sendClientCommand(player, "WastelandDungeons", "logLootRewards", { dungeonId = dungeonId, loot = loot })
end

local Commands = {}

function Commands.dungeonCompleted(args)
    local dungeon = WLD_All_Dungeons[args.dungeonId]
    if not dungeon then return end
    dungeon:dungeonCompleted(getPlayer(), args.getsRewards)
end

function Commands.objectiveCompleted(args)
    local dungeon = WLD_All_Dungeons[args.dungeonId]
    if not dungeon then return end
    dungeon:objectiveCompleted(getPlayer(), args.objectiveId, args.playerUsername)
end

function Commands.activateTrigger(args)
    local dungeon = WLD_All_Dungeons[args.dungeonId]
    if not dungeon then return end
    dungeon:activateTrigger(getPlayer(), args.triggerId)
end

function Commands.configs(args)
    print("Received configs")
    if args then
        for id, config in pairs(args) do
            WLD_CreateInstance(id, config)
        end
    end
    ModData.request("WLD_DungeonStates")
end

local function processServerCommand(module, command, args)
    if module ~= "WastelandDungeons" then return end
    if not Commands[command] then return end
    Commands[command](args)
end

local function OnRecieveGlobalModData(key, data)
    if key ~= "WLD_DungeonStates" then return end
    if not data then return end
    local player = getPlayer()
    for dungeonId, dungeonData in pairs(data) do
        local dungeon = WLD_All_Dungeons[dungeonId]
        if dungeon then
            WLD_All_Dungeons[dungeonId].serverState = dungeonData
            dungeon:setObjectives(player)
            dungeon:setTriggers(player)
        end
    end
end

local function sendReset(dungeonId)
    sendClientCommand(getPlayer(), "WastelandDungeons", "resetDungeon", {dungeonId = dungeonId})
end
local function clearHistory(dungeonId)
    sendClientCommand(getPlayer(), "WastelandDungeons", "clearHistory", {dungeonId = dungeonId})
end
local function setDungeonStatus(dungeonId, status)
    sendClientCommand(getPlayer(), "WastelandDungeons", "setDungeonStatus", {dungeonId = dungeonId, status = status})
end
local function sendSetDungeonObjectiveChange(dungeonId, objectiveId, change)
    sendClientCommand(getPlayer(), "WastelandDungeons", "setDungeonObjectiveChange", {dungeonId = dungeonId, objectiveId = objectiveId, change = change})
end
local function addAdminCommandsToContextMenu(playerIdx, context)
    if not isAdmin() then return end
    local adminMenu = WL_ContextMenuUtils.getOrCreateSubMenu(context, "WL Admin")
    local dungeonsMenu = WL_ContextMenuUtils.getOrCreateSubMenu(adminMenu, "Dungeons")
    dungeonsMenu:addOption("Reload Configs", nil, function() sendClientCommand(getPlayer(), "WastelandDungeons", "resetConfigs", {}) end)
    for dungeonId, dungeon in pairs(WLD_All_Dungeons) do
        local dungeonMenu = WL_ContextMenuUtils.getOrCreateSubMenu(dungeonsMenu, dungeon.name)
        if dungeon.serverState.status == "open" then
            dungeonMenu:addOption("Close to new players", dungeonId, setDungeonStatus, "running")
        end
        if dungeon.serverState.status == "running" then
            dungeonMenu:addOption("Open to new players", dungeonId, setDungeonStatus, "open")
        end
        if dungeon.serverState.status == "resetting" then
            dungeonMenu:addOption("Force Reset Complete", dungeonId, setDungeonStatus, "idle")
        end
        if dungeon.serverState.status == "open" or dungeon.serverState.status == "running" then
            for objectiveId, objective in pairs(dungeon.objectives) do
                if dungeon.serverState.objectives[objectiveId] then
                    dungeonMenu:addOption("Uncomplete " .. objective.objectiveDescription, dungeonId, sendSetDungeonObjectiveChange, objectiveId, false)
                else
                    dungeonMenu:addOption("Complete " .. objective.objectiveDescription, dungeonId, sendSetDungeonObjectiveChange, objectiveId, true)
                end
            end
        end
        dungeonMenu:addOption("Reset", dungeonId, sendReset)
        dungeonMenu:addOption("Clear History", dungeonId, clearHistory)
    end
end

local timeOutOfDungeon = 0
local timeInDungeonArea = 0
local function checkPlayerPosition()
    local player = getPlayer()
    if not player then return end
    local x,y = player:getX(), player:getY()
    local didATimeout = false
    for _, dungeon in pairs(WLD_All_Dungeons) do
        if dungeon.dungeonZone:isInZone(x, y, 0) and dungeon.exitPortal and dungeon.exitPortal.target then
            if dungeon.serverState.status == "resetting" then
                WL_Utils.addErrorToChat("This dungeon is currently resetting. Do not come back until after the reset.")
                WL_Utils.teleportPlayerToCoords(player, dungeon.exitPortal.target.x, dungeon.exitPortal.target.y, dungeon.exitPortal.target.z)
                WL_Utils.makePlayerSafe(30)
            elseif not dungeon.serverState.currentPlayers[player:getUsername()] and not player:isGodMod() then
                if timeInDungeonArea == 0 then
                    timeInDungeonArea = getTimestamp()
                elseif timeInDungeonArea + 5 < getTimestamp() then -- we give a bit of buffer time to allow for mod data to sync
                    WL_Utils.addErrorToChat("You do not belong here. Goodbye.")
                    WL_Utils.teleportPlayerToCoords(player, dungeon.exitPortal.target.x, dungeon.exitPortal.target.y, dungeon.exitPortal.target.z)
                    WL_Utils.makePlayerSafe(30)
                end
            end
        end
        if dungeon.serverState.currentPlayers[player:getUsername()] and not dungeon.dungeonZone:isInZone(x, y, 0) then
            if timeOutOfDungeon == 0 then
                WL_Utils.addErrorToChat("You left the dungeon area, you will be removed from the dungeon in 5 minutes unless you return.")
                timeOutOfDungeon = getTimestamp()
            elseif getTimestamp() - timeOutOfDungeon >= 300 then
                timeOutOfDungeon = 0
                WLD_Client.leaveDungeon(player, dungeon.id)
                WL_Utils.addErrorToChat("You have been removed from the dungeon for being out of the area for too long.")
            end
            didATimeout = true
        end
    end
    if not didATimeout then
        timeOutOfDungeon = 0
    end
end


Events.OnServerCommand.Add(processServerCommand)
Events.OnReceiveGlobalModData.Add(OnRecieveGlobalModData)
WL_PlayerReady.Add(function ()
    sendClientCommand(getPlayer(), "WastelandDungeons", "getConfigs", {})
end)
Events.OnFillWorldObjectContextMenu.Add(addAdminCommandsToContextMenu)
Events.OnTick.Add(checkPlayerPosition)