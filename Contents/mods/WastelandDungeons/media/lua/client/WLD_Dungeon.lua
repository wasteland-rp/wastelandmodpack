---
--- WLD_Dungeon.lua
--- 19/10/2023
---

require "WLBaseObject"
require "WL_Utils"
require "WL_MapUtils"
require "WL_Zone"
require "WLP_Portal"
require "WLP_PortalRegistry"
require "WLD_DungeonPortalController"
require "WLD_LootTable"
require "WLD_Objectives/WLD_EnterZoneObjective"
require "WLD_Objectives/WLD_DefendObjective"
require "WLD_Triggers/WLD_SpawnHordeTrigger"
require "WLD_Triggers/WLD_SpawnHordeAreaTrigger"
require "WLD_Triggers/WLD_MakeSoundTrigger"
require "WLD_Triggers/WLD_MessageTrigger"

WLD_Dungeon = WLBaseObject:derive("WLD_Dungeon")

-- Map of dungeonId : dungeon
WLD_All_Dungeons = {}

--- Creates a new Dungeon
--- @param id string unique identifier
--- @param name string human readable name
--- @param difficulty number where 1=easy 2=normal 3=hard
--- @param mapCells table in the format { {1, 3}, {2, 3}, {3, 3} } whereby the two numbers are x and y coords of cells
function WLD_Dungeon:new(id, name, difficulty, mapCells)
    local o = WLD_Dungeon.parentClass.new(self)
    o.id = id
    o.name = name
    o.difficulty = difficulty
    o.maxPlayers = 0
    o.repeatMilliseconds = 604800000 -- 7 days
    o.dungeonZone = WL_Zone:new(WL_MapUtils.cellsToMapCoords(mapCells))
    o.objectives = {}
    o.triggers = {}
    o.messages = {}
    o.serverState = {
        status = "invalid",
        currentPlayers = {},
        objectives = {},
        triggers = {},
        playerHistory = {},
    }
    o.lootTable = WLD_LootTable:new(difficulty)
    o.requiredItem = nil
    o.entrancePortal = nil
    o.exitPortal = nil
    o.otherPortals = {}
    WLD_All_Dungeons[id] = o
    return o
end

function WLD_Dungeon:addPortal(portal, isEntrance)
    portal:setPortalController(WLD_DungeonPortalController:new(self, isEntrance))
    if isEntrance then
        portal.homingArrow.color =  { r = 100, g = 255, b = 100, a = 0.8}

        if self.requiredItem and isEntrance then
            portal.requiredItem = self.requiredItem.item
            portal.requiredItemCount = self.requiredItem.count
            if self.requiredItem.name then
                portal.requiredItemName = self.requiredItem.name
            end
            portal.takeRequiredItem = true
        end
        self.entrancePortal = portal
    else
        portal.homingArrow.color =  { r = 200, g = 100, b = 100, a = 0.8}
        self.exitPortal = portal
    end
end

function WLD_Dungeon:addOtherPortal(portal)
    table.insert(self.otherPortals, portal)
end

function WLD_Dungeon:addTrigger(triggerId, trigger)
    self.triggers[triggerId] = trigger
end

function WLD_Dungeon:addMessages(messageType, messages)
    self.messages[messageType] = messages
end

function WLD_Dungeon:addWeatherZone(zone)
    self.weatherZone = zone
end

function WLD_Dungeon:addObjective(objective)
    self.objectives[objective.objectiveId] = objective
end

function WLD_Dungeon:isObjectiveComplete(objectiveId)
    return self.serverState.objectives[objectiveId]
end

--- @param itemType string the full type of the item (Base.Money)
--- @param count number the number of items required (>0)
--- @param name string|nil the custom name of the item if required
function WLD_Dungeon:setCost(itemType, count, name)
    self.requiredItem = itemType
    self.requiredItemCount = count
    self.requiredItemName = name
end

function WLD_Dungeon:getPortalTooltipSprite()
    return self.tooltipSprite
end

--- Helper function, also used by portals
function WLD_Dungeon:getTimeUntilPlayerCanCompleteDungeon()
    if not self.serverState.playerHistory[getPlayer():getUsername()] then
        return 0
    end
    return self.serverState.playerHistory[getPlayer():getUsername()] + self.repeatMilliseconds - getTimestampMs()
end

function WLD_Dungeon:isAlreadyInDungeon(player)
    return self.serverState.currentPlayers[player:getUsername()]
end

function WLD_Dungeon:canPlayerEnter(player)
    if player:isGodMod() then
        return true
    end

    -- Check if player is already in the dungeon, always allow in this case
    if self:isAlreadyInDungeon(player) then
        return true
    end

    -- Check if the dungeon is already running and open
    if self.serverState.status == "open" then
        return true
    end

    -- Check if the dungeon is idle and the player is eligible to enter
    -- If you can get rewards, you can't start the dungeon
    if self.serverState.status == "idle" and self:getTimeUntilPlayerCanCompleteDungeon() == 0 then
        return true
    end

    return false
end

function WLD_Dungeon:sendMessages(type)
    if self.messages[type] then
        WLD_Helper.doMessages(self.messages[type])
    end
end

function WLD_Dungeon:playerEntered(player)
    if player:isGodMod() then return end

    WLD_Client.enterDungeon(player, self.id)
    self:sendMessages("enter")
    WLD_Helper.displayMessage(player, "ooc", "   ")
    WLD_Helper.displayMessage(player, "ooc", "You have entered a WastelandRP Dungeon! These dungeons are special areas with unique objectives and rewards, but beware, they are not easy. By continuing forth you agree to the following:")
    WLD_Helper.displayMessage(player, "ooc", "1. Deaths in the dungeon will not be refunded for any reason, even bugs.")
    WLD_Helper.displayMessage(player, "ooc", "2. Items lost in the dungeon will not be refunded for any reason, even bugs.")
    WLD_Helper.displayMessage(player, "ooc", "3. If you leave the dungeon before completing it, you will not be able to re-enter.")
    WLD_Helper.displayMessage(player, "ooc", "4. If you leave the dungeon before completing it, you will not receive any rewards.")
    WLD_Helper.displayMessage(player, "ooc", "5. If you logout or are disconnected while in the dungeon, you have 30 minutes to rejoin before being removed. If you are logged out or disconnected when the dungeon is completed, you will not received any rewards.")
    WLD_Helper.displayMessage(player, "ooc", "6. Rewards are determined via a random loot table. Staff will not swap any rewards or items.")
    getSoundManager():playUISound(self:getEntranceSoundId())
    self:setObjectives(player)
end

function WLD_Dungeon:setObjectives(player)
    for _, objective in pairs(self.objectives) do
        if not objective:isAbleToTrigger(player) then
            objective:deactivate()
        end
    end
    for _, objective in pairs(self.objectives) do
        if objective:isAbleToTrigger(player) then
            objective:activate()
        end
    end
end

function WLD_Dungeon:setTriggers(player)
    for triggerId, trigger in pairs(self.triggers) do
        if self.serverState.triggers[triggerId] or not self:isAlreadyInDungeon(player) then
            trigger:unset()
        else
            trigger:set()
        end
    end
end

function WLD_Dungeon:getEntranceSoundId()
    if self.difficulty == 3 then -- Hard
        return "EnterDungeon2"
    elseif self.difficulty == 2 then -- Medium
        return "EnterDungeon3"
    else -- Easy/unknown
        return "EnterDungeon1"
    end
end

function WLD_Dungeon:areAllObjectivesComplete()
    for _, objective in pairs(self.serverState.objectives) do
        if not objective then
            return false
        end
    end
    return true
end

function WLD_Dungeon:playerExited(player)
    if player:isGodMod() then return end
    if self:areAllObjectivesComplete() then
        self:sendMessages("leave_finished")
    else
        self:sendMessages("leave_unfinished")
    end
    if self.weatherOverride then
        WLD_Helper.displayMessage(player, "ooc", "   ")
        WLD_Helper.displayMessage(player, "ooc", "It may take a few minutes for the climate values to restore after leaving this dungeon.")
    end
    WLD_Client.leaveDungeon(player, self.id)
end

function WLD_Dungeon:sendObjectiveCompleted(player, objectiveId)
    WLD_Client.completeObjective(player, self.id, objectiveId)
end

function WLD_Dungeon:objectiveCompleted(player, objectiveId, completedByUsername)
    local objective = self.objectives[objectiveId]
    objective:deactivate()
    if not objective.isQuiet then
        local message = "Objective " .. objective.objectiveDescription .. " completed"
        if player:getUsername() ~= completedByUsername then
            message = message .. " by " .. completedByUsername
        end
        message = message .. "!"
        WLD_Helper.displayMessage(player, "success", message)
        getSoundManager():playUISound("GuitarSound3")
    end
    if objective:shouldReceiveMessages(player, player:getUsername() == completedByUsername) then
        self:sendMessages(objectiveId)
    end
end

function WLD_Dungeon:sendTrigger(player, trigger)
    WLD_Client.activateTrigger(player, self.id, trigger.triggerId)
end

function WLD_Dungeon:activateTrigger(player, triggerId)
    local trigger = self.triggers[triggerId]
    trigger:activate(player)
end

function WLD_Dungeon:dungeonCompleted(player, getsRewards)
    getPlayer():setHaloNote("Dungeon Completed", 255, 215, 0, 300.0)
    WLD_Helper.displayMessage(player, "success", "Dungeon Completed!")
    getSoundManager():playUISound("GuitarSound2")

    if getsRewards then
        self:sendMessages("Dungeon Completed!")
        local loot = self.lootTable:rollLoot()
        local lootStr = ""
        for _, item in ipairs(loot) do
            if type(item) == "string" then
                lootStr = lootStr .. "1 " .. item .. ", "
            elseif type(item) == "table" then
                lootStr = lootStr .. tostring(item[2]) .. " " .. item[1] .. ", "
            end
        end
        lootStr = string.sub(lootStr, 1, -3) -- Remove the last comma
        WLD_Client.logLootRewards(player, self.id, lootStr)
    end
end

function WLD_Dungeon:generatePortalEntranceTooltipText(player)
    return "Dungeon: " .. self.name .. " "  -- need the space here or we lose a word
            .. self:getDungeonDifficultyString()
            .. self:getPlayerStatusString(player)
end

function WLD_Dungeon:getDungeonDifficultyString()
    local str = "<LINE><RGB:1,1,1> Difficulty:" .. WL_Utils.MagicSpace
    if self.difficulty == 1 then
        str = str .. "<RGB:0,1,0> Easy "
    elseif self.difficulty == 2 then
        str = str .. "<RGB:1,0.647,0> Moderate "
    else -- Assume = 3
        str = str .. "<RGB:1,0,0> Hard "
    end
    return str
end

function WLD_Dungeon:countCurrentPlayers()
    local count = 0
    for _ in pairs(self.serverState.currentPlayers) do
        count = count + 1
    end
    return count
end

function WLD_Dungeon:getPlayerStatusString(player)
    local timeUntilCanComplete = self:getTimeUntilPlayerCanCompleteDungeon()

    local str = " <LINE> <RGB:1,1,1> Status:" .. WL_Utils.MagicSpace

    if self.serverState.status == "idle" then
        if timeUntilCanComplete == 0 then
            str = str .. "<RGB:0,1,0> Available" .. WL_Utils.MagicSpace -- Player can start a new dungeon
        else
            str = str .. "<RGB:0.6,0.6,0.6> Ineligible" .. WL_Utils.MagicSpace
        end
    elseif self.serverState.status == "open" then
        str = str .. "<RGB:0,1,0> Joinable" .. WL_Utils.MagicSpace -- Player can join a dungeon that has already started
    elseif self.serverState.status == "running" or self.serverState.status == "completed" then
        str = str .. "<RGB:1,0,0> Occupied" .. WL_Utils.MagicSpace
    elseif self.serverState.status == "resetting" then
        str = str .. "<RGB:1,0,0> Resetting" .. WL_Utils.MagicSpace
    else
        str = str .. "<RGB:1,0,0> Closed" .. WL_Utils.MagicSpace
    end

    if self.maxPlayers > 0 then
        str = str .. " <LINE> <RGB:1,1,1> Players: " .. WL_Utils.MagicSpace
        if self.serverState.status == "idle" or self.serverState.status == "open" then
            local playerCount = self:countCurrentPlayers()
            if playerCount < self.maxPlayers then
                str = str .. "<RGB:0,1,0>"
            else
                str = str .. "<RGB:1,0,0>"
            end
            str = str .. tostring(playerCount) .. "/" .. tostring(self.maxPlayers)
        else
            str = str .. tostring(self.maxPlayers)
        end
        str = str .. WL_Utils.MagicSpace
    end

    if timeUntilCanComplete == 0 then
        str = str .. " <LINE> <RGB:0,1,0> You are eligible for rewards"
    else
        str = str .. " <LINE> <RGB:1,0,0> You can not start a new dungeon, but may join others. Expiration: "
                .. WL_Utils.toHumanReadableTime(timeUntilCanComplete)
    end

    return str
end

function WLD_Dungeon:generatePortalExitTooltipText(player)
    return "Leave the dungeon"
end

function WLD_CreateInstance(id, config)
    if WLD_All_Dungeons[id] then
        local dungeon = WLD_All_Dungeons[id]
        if dungeon.entrancePortal then
            WLP_PortalRegistry.removePortalById(dungeon.entrancePortal.modDataId)
        end
        if dungeon.exitPortal then
            WLP_PortalRegistry.removePortalById(dungeon.exitPortal.modDataId)
        end
        for _, portal in pairs(dungeon.otherPortals) do
            WLP_PortalRegistry.removePortalById(portal.modDataId)
        end
        if dungeon.weatherZone then
            WEZ_EventZones[dungeon.weatherZone.id] = nil
            WL_TriggerZones.removeZone(dungeon.weatherZone.id)
        end
        if dungeon.dungeonZone then
            dungeon.dungeonZone:delete()
        end
        for _, objective in pairs(dungeon.objectives) do
            objective:deactivate()
            if objective.zone then
                WL_TriggerZones.removeZone(objective.zone)
                objective.zone:delete()
            end
        end
        for _, trigger in pairs(dungeon.triggers) do
            trigger:unset()
            WL_TriggerZones.removeZone(trigger)
            trigger:delete()
        end
        WLD_All_Dungeons[id] = nil
    end

    print("Creating dungeon: " .. id)

    local dungeon = WLD_Dungeon:new(id, config.name, config.difficulty, config.mapChunks)

    if config.tooltipSprite then
        dungeon.tooltipSprite = config.tooltipSprite
    end

    if config.requiredItem then
        dungeon.requiredItem = config.requiredItem
    end

    if config.hoursToRepeat then
        dungeon.repeatMilliseconds = config.hoursToRepeat * 3600000
    end

    if config.maxPlayers then
        dungeon.maxPlayers = config.maxPlayers
    end

    if config.cost then
        dungeon:setCost(config.cost[1], config.cost[2])
    end

    if config.messages then
        for messageType, messages in pairs(config.messages) do
            dungeon:addMessages(messageType, messages)
        end
    end

    if config.weatherOverride then
        local zone = WEZ_EventZone:loadFrom({
            name = config.name .. " Weather",
            minX = dungeon.dungeonZone.minX,
            minY = dungeon.dungeonZone.minY,
            minZ = 0,
            maxX = dungeon.dungeonZone.maxX,
            maxY = dungeon.dungeonZone.maxY,
            maxZ = 8,
        })

        if config.weatherOverride.wind then
            zone.weatherWind = config.weatherOverride.wind
            zone.weatherWindEnabled = true
        end

        if config.weatherOverride.clouds then
            zone.weatherClouds = config.weatherOverride.clouds
            zone.weatherCloudsEnabled = true
        end

        if config.weatherOverride.fog then
            zone.weatherFog = config.weatherOverride.fog
            zone.weatherFogEnabled = true
        end

        if config.weatherOverride.precipitation then
            zone.weatherPrecipitation = config.weatherOverride.precipitation
            zone.weatherPrecipitationEnabled = true
        end

        if config.weatherOverride.precipitationIsSnow then
            zone.weatherPrecipitationIsSnow = true
        end

        if config.weatherOverride.temperature then
            zone.weatherTemperature = config.weatherOverride.temperature
            zone.weatherTemperatureEnabled = true
        end

        if config.weatherOverride.darkness then
            zone.weatherDarkness = config.weatherOverride.darkness
            zone.weatherDarknessEnabled = true
        end

        if config.weatherOverride.desaturation then
            zone.weatherDesaturation = config.weatherOverride.desaturation
            zone.weatherDesaturationEnabled = true
        end

        if config.weatherOverride.light then
            zone.weatherLightExtR = config.weatherOverride.light.exteriorR
            zone.weatherLightExtG = config.weatherOverride.light.exteriorG
            zone.weatherLightExtB = config.weatherOverride.light.exteriorB
            zone.weatherLightExtA = config.weatherOverride.light.exteriorA
            zone.weatherLightIntR = config.weatherOverride.light.interiorR
            zone.weatherLightIntG = config.weatherOverride.light.interiorG
            zone.weatherLightIntB = config.weatherOverride.light.interiorB
            zone.weatherLightEnabled = true
        end

        zone.name = config.name .. " Weather"
        zone.external = true
        dungeon:addWeatherZone(zone)
    end

    if config.objectives then
        for objectiveId, objectiveConfig in pairs(config.objectives) do
            if objectiveConfig.type == "enter_zone" then
                dungeon:addObjective(
                    WLD_EnterZoneObjective:new(
                        dungeon, objectiveId, objectiveConfig.name,
                        objectiveConfig.nw[1], objectiveConfig.nw[2], objectiveConfig.nw[3],
                        objectiveConfig.se[1], objectiveConfig.se[2], objectiveConfig.se[3]
                    )
                )
            elseif objectiveConfig.type == "defend" then
                dungeon:addObjective(
                    WLD_DefendObjective:new(
                        dungeon, objectiveId, objectiveConfig.name,
                        objectiveConfig.x, objectiveConfig.y, objectiveConfig.z, objectiveConfig.r,
                        objectiveConfig.tx or objectiveConfig.x, objectiveConfig.ty or objectiveConfig.y, objectiveConfig.tz or objectiveConfig.z,
                        objectiveConfig.zombieCountPerSpawn, objectiveConfig.secondsPerSpawn,
                        objectiveConfig.spawnType, objectiveConfig.duration
                    )
                )
            end
            if objectiveConfig.requires then
                for _, requiredObjective in ipairs(objectiveConfig.requires) do
                    dungeon.objectives[objectiveId]:addRequirement(requiredObjective)
                end
            end
            if objectiveConfig.messages then
                dungeon:addMessages(objectiveId, objectiveConfig.messages)
            end
            if objectiveConfig.quiet then
                dungeon.objectives[objectiveId]:setQuiet()
            end
            if objectiveConfig.onlyActivator then
                dungeon.objectives[objectiveId]:setOnlyActivator()
            end
        end
    end

    if config.triggerZones then
        for triggerId, triggerConfig in pairs(config.triggerZones) do
            local trigger = nil
            if triggerConfig.type == "spawn_horde" then
                trigger = WLD_SpawnHordeTrigger:new(
                        triggerId, dungeon,
                        triggerConfig.nw[1], triggerConfig.nw[2], triggerConfig.nw[3],
                        triggerConfig.se[1], triggerConfig.se[2], triggerConfig.se[3],
                        triggerConfig.args
                    )
            elseif triggerConfig.type == "spawn_horde_area" then
                trigger = WLD_SpawnHordeAreaTrigger:new(
                        triggerId, dungeon,
                        triggerConfig.nw[1], triggerConfig.nw[2], triggerConfig.nw[3],
                        triggerConfig.se[1], triggerConfig.se[2], triggerConfig.se[3],
                        triggerConfig.args
                    )
            elseif triggerConfig.type == "make_sound" then
                trigger = WLD_MakeSoundTrigger:new(
                        triggerId, dungeon,
                        triggerConfig.nw[1], triggerConfig.nw[2], triggerConfig.nw[3],
                        triggerConfig.se[1], triggerConfig.se[2], triggerConfig.se[3],
                        triggerConfig.args
                    )
            elseif triggerConfig.type == "message" then
                trigger = WLD_MessageTrigger:new(
                        triggerId, dungeon,
                        triggerConfig.nw[1], triggerConfig.nw[2], triggerConfig.nw[3],
                        triggerConfig.se[1], triggerConfig.se[2], triggerConfig.se[3],
                        triggerConfig.args
                    )
            elseif triggerConfig.type == "unlock_door" then
                trigger = WLD_UnlockDoorTrigger:new(
                        triggerId, dungeon,
                        triggerConfig.nw[1], triggerConfig.nw[2], triggerConfig.nw[3],
                        triggerConfig.se[1], triggerConfig.se[2], triggerConfig.se[3],
                        triggerConfig.args
                    )
            end

            if trigger then
                dungeon:addTrigger(triggerId, trigger)
                if triggerConfig.requiredObjectives then
                    trigger:setRequiredObjectives(triggerConfig.requiredObjectives)
                end
            else
                print("Unknown trigger type: " .. triggerConfig.type)
            end
        end
    end

    if config.worldPortal then
        local portal = WLP_PortalRegistry.createPortal(config.worldPortal.nw, config.worldPortal.se, config.worldPortal.target)
        portal.modDataId = getRandomUUID()
        portal.markerRange = config.worldPortal.markerRange
        portal.fadeoutTime = config.worldPortal.fadeoutTime
        portal.soundName = config.worldPortal.soundName
        dungeon:addPortal(portal, true)
    end

    if config.dungeonPortal then
        local portal = WLP_PortalRegistry.createPortal(config.dungeonPortal.nw, config.dungeonPortal.se, config.dungeonPortal.target)
        portal.modDataId = getRandomUUID()
        portal.markerRange = config.dungeonPortal.markerRange
        portal.fadeoutTime = config.dungeonPortal.fadeoutTime
        portal.soundName = config.dungeonPortal.soundName
        dungeon:addPortal(portal, false)
    end

    if config.otherPortals then
        for _, portalConfig in ipairs(config.otherPortals) do
            local portal = WLP_PortalRegistry.createPortal(portalConfig.nw, portalConfig.se, portalConfig.target, portalConfig.name)
            portal.modDataId = getRandomUUID()
            portal.markerRange = portalConfig.markerRange
            portal.fadeoutTime = portalConfig.fadeoutTime
            portal.soundName = portalConfig.soundName
            dungeon:addOtherPortal(portal)
        end
    end
end