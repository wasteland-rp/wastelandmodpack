---
--- WLD_DungeonPortalController.lua
--- 19/10/2023
---

require "WLP_PortalController"

WLD_DungeonPortalController = WLP_PortalController:derive("WLD_DungeonPortalController")

function WLD_DungeonPortalController:new(dungeon, isEntrance)
    local o = WLD_DungeonPortalController.super(self)
    o.dungeon = dungeon
    o.isEntrance = isEntrance
    return o
end

function WLD_DungeonPortalController:requiresItem(portal)
    if not portal.requiredItem then return false end
    if self.dungeon.serverState.state == "open" or self.dungeon.serverState.state == "running" then
        local username = getPlayer():getUsername()
        if self.dungeon.serverState.currentPlayers[username] then
            return false
        end
    end
    -- maybe add if not able to receive rewards you get free too
    return true
end

function WLD_DungeonPortalController:getEnterText()
    if self.isEntrance then
        return "Enter Dungeon"
    else
        return "Exit Dungeon"
    end
end

function WLD_DungeonPortalController:generateTooltip(player, portal)
    if self.isEntrance then
        if self.dungeon:isAlreadyInDungeon(player) then
            return self.dungeon:generatePortalEntranceTooltipText(player)
        end
        return self.dungeon:generatePortalEntranceTooltipText(player) .. self:getRequiredItemString(portal)
    else
        return self.dungeon:generatePortalExitTooltipText(player)
    end
end

function WLD_DungeonPortalController:canEnter(player, portal)
    if self.dungeon:isAlreadyInDungeon(player) then
        return true
    end
    if not self.parentClass.canEnter(self, player, portal) then
        return false
    end
    if self.isEntrance then
        return self.dungeon:canPlayerEnter(player)
    else
        return true
    end
end

function WLD_DungeonPortalController:getTooltipSprite()
    if self.isEntrance then
        return self.dungeon:getPortalTooltipSprite()
    else
        return nil
    end
end

function WLD_DungeonPortalController:playerTeleported(player, portal)
    if self.isEntrance then
        if not self.dungeon:isAlreadyInDungeon(player) then
            self.parentClass.playerTeleported(self, player, portal)
        end
        self.dungeon:playerEntered(player)
    else
        self.dungeon:playerExited(player)
    end
end