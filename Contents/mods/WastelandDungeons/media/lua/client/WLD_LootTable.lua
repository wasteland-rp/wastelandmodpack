---
--- WLD_LootTable.lua
--- 08/11/2023
---

require "WLBaseObject"
require "WL_Utils"

WLD_LootTable = WLBaseObject:derive("WLD_LootTable")

WLD_LootTable.tables = {
	[1] = {
		{ weight = 5, items = { "Base.Pistol", { "Base.9mmClip", 3 }, "Base.Bullets9mmBox" } },
		{ weight = 2, items = { "Base.ColtPython", { "Base.Bullets357Box", 3 } } },
		{ weight = 2, items = { "Base.Mossberg500Tactical", { "Base.ShotgunShellsBox", 3 } } },
		{ weight = 4, items = { "Base.SKS", { "Base.762x39Box", 3 } } },
		{ weight = 1, items = { "Base.M1Garand", { "Base.M1GarandClip", 3 }, "Base.Bullets3006Box" } },
		{ weight = 3, items = { "Base.Rossi92", { "Base.Bullets357Box", 3 } } },
		{ weight = 2, items = { "Base.M37", { "Base.ShotgunShellsBox", 3 } } },
		{ weight = 2, items = { "Base.HeadhunterRifle", {"Base.M14Clip", 3}, "Base.308Box" } },
		{ weight = 2, items = { "GreysWLWeaponsFirearms.LastResort", {"Base.ShotgunShellsBox", 2} } },
		{ weight = 1, items = { "Base.22Silencer", {"Base.Bullets22Box", 7 } } },
		{ weight = 3, items = { "KCMweapons.KCM_Compound", { "KCMweapons.CrossbowBoltLargeBox", 2 } } },
		{ weight = 3, items = { "Radio.WalkieTalkie5", {"Base.Bullets22Box", 6} } },
		{ weight = 3, items = { "Base.StenMk2", { "Base.StenMk2_Mag", 3 },  {"Base.Bullets9mmBox", 4} } },
	},

	[2] = {
		{ weight = 1, items = { "Base.Glock17", { "Base.Glock17Mag", 3 }, {"Base.Bullets9mmBox", 6}  } },
		{ weight = 1, items = { "Base.FN_FAL", {"Base.FN_FAL_Mag", 3} , {"Base.762x51Box", 6} } },
		{ weight = 2, items = { "Base.AssaultRifle2", {"Base.M14Clip", 3}, {"Base.308Box", 6} } },
		{ weight = 2, items = { "Base.DeadlyHeadhunterRifle", {"Base.M14Clip", 3}, {"Base.308Box", 6} } },
		{ weight = 2, items = { "Base.LAW12", { "Base.ShotgunShellsBox", 6 } } },
		{ weight = 1, items = { "Base.M24Rifle", { "Base.762x51Box", 6 } } },
		{ weight = 3, items = { "Base.ColtPythonHunter", { "Base.Bullets357Box", 6 } } },
		{ weight = 1, items = { "Base.RiotShieldPolice", "Base.Nightstick", { "Base.Molotov", 3}, "ImprovisedGunToolKit" } },
		{ weight = 2, items = { "Base.AR15", { "Base.556Clip", 3 }, {"Base.556Box", 6} } },
		{ weight = 3, items = { "ImprovisedGunToolKit", {"Base.Bullets9mmBox", 12} } },
		{ weight = 3, items = { "Base.KATTAJ1_TacticalFlashlight", {"Base.Bullets45Box", 12} } },
		{ weight = 3, items = { "Base.KATTAJ1_TacticalFlashlight", {"Base.Bullets44Box", 12} } },
		{ weight = 3, items = { "Base.Sling", {"Base.762x39Box", 12} } },
		{ weight = 3, items = { "ImprovisedGunToolKit", {"Base.Bullets357Box", 12} } },
		{ weight = 3, items = { "KCMweapons.KCM_Compound02", { "KCMweapons.WoodenBoltBox", 4 } } },
	},

	[3] = {
		{ weight = 1, items = { "Base.M60", {"Base.M60Mag", 3}, {"Base.762x51Box", 6} } },
		{ weight = 3, items = { "Base.M733", { "Base.556Clip", 3 }, {"Base.556Box", 6} } },
		{ weight = 3, items = { "Base.SPAS12", { "Base.ShotgunShellsBox", 6 } } },
		{ weight = 3, items = { "Base.AK47", {"Base.AK_Mag", 3} , {"Base.762x39Box", 6} } },
		{ weight = 3, items = { "Base.MP5", {"Base.MP5Mag", 3}, {"Base.Bullets9mmBox", 6} } },

		{ weight = 2, items = { "Base.9mmSilencer", {"Base.Bullets9mmBox", 10} } },
		{ weight = 2, items = { "Base.45Silencer", {"Base.Bullets45Box", 10} } },
		{ weight = 2, items = { "Base.223Silencer", {"Base.223Box", 10} } },
		{ weight = 2, items = { "Base.308Silencer", {"Base.308Box", 10} } },
		{ weight = 2, items = { "Base.ShotgunSilencer", {"Base.ShotgunShellsBox", 10} } },
		{ weight = 2, items = { "Base.38Silencer", {"Base.Bullets38Box", 10} } },
		{ weight = 2, items = { "Base.RiotShieldSwat", "Base.Katana", {"Base.762x39Box", 10} } },

		{ weight = 2, items = { "Base.Olive_ElbowPads", "Base.Olive_ShoulderPads", "Base.Olive_KneePads", "Base.Olive_GreenHelmet", {"Base.556Box", 3}, { "Base.ShotgunShellsBox", 3}, {"Base.762x39Box", 3}, {"Base.762x51Box", 3}, {"Base.Bullets9mmBox", 3}, {"Base.223Box", 3}, {"Base.308Box", 3}, {"Base.Bullets38Box", 3} }},
		{ weight = 2, items = { "Base.SwatElbowPads", "Base.SwatShoulderPads", "Base.SwatKneePads", "Base.Hat_SwatHelmet", {"Base.556Box", 3}, { "Base.ShotgunShellsBox", 3}, {"Base.762x39Box", 3}, {"Base.762x51Box", 3}, {"Base.Bullets9mmBox", 3}, {"Base.223Box", 3}, {"Base.308Box", 3}, {"Base.Bullets38Box", 3} }},
		{ weight = 2, items = { "Base.Black_Camo_Elbow_Pads", "Base.Black_Camo_ShoulderPads_Patriot", "Base.Black_Camo_Knee_Pads", "Base.BlackCamo_Helmet_Patriot", {"Base.556Box", 3}, { "Base.ShotgunShellsBox", 3}, {"Base.762x39Box", 3}, {"Base.762x51Box", 3}, {"Base.Bullets9mmBox", 3}, {"Base.223Box", 3}, {"Base.308Box", 3}, {"Base.Bullets38Box", 3} }},
		{ weight = 2, items = { "Base.Desert_Camo_Elbow_Pads", "Base.Desert_Camo_ShoulderPads_Patriot", "Base.Desert_Camo_Knee_Pads", "Base.DesertCamo_Helmet_Patriot", {"Base.556Box", 3}, { "Base.ShotgunShellsBox", 3}, {"Base.762x39Box", 3}, {"Base.762x51Box", 3}, {"Base.Bullets9mmBox", 3}, {"Base.223Box", 3}, {"Base.308Box", 3}, {"Base.Bullets38Box", 3} }},
		{ weight = 2, items = { "Base.EMR_Camo_Elbow_Pads", "Base.EMR_Camo_Vest", "Base.EMR_Camo_Knee_Pads", "Base.EMR_Camo_Helmet", {"Base.556Box", 3}, { "Base.ShotgunShellsBox", 3}, {"Base.762x39Box", 3}, {"Base.762x51Box", 3}, {"Base.Bullets9mmBox", 3}, {"Base.223Box", 3}, {"Base.308Box", 3}, {"Base.Bullets38Box", 3} }},
		{ weight = 2, items = { "Base.Flecktarn_Camo_Elbow_Pads", "Base.Flecktarn_Camo_Vest", "Base.Flecktarn_Camo_Knee_Pads", "Base.Flecktarn_Camo_Helmet", {"Base.556Box", 3}, { "Base.ShotgunShellsBox", 3}, {"Base.762x39Box", 3}, {"Base.762x51Box", 3}, {"Base.Bullets9mmBox", 3}, {"Base.223Box", 3}, {"Base.308Box", 3}, {"Base.Bullets38Box", 3} }},
		{ weight = 2, items = { "Base.Forest_Camo_Elbow_Pads", "Base.Forest_Camo_ShoulderPads_Patriot", "Base.Forest_Camo_Knee_Pads", "Base.ForestCamo_Helmet_Patriot", {"Base.556Box", 3}, { "Base.ShotgunShellsBox", 3}, {"Base.762x39Box", 3}, {"Base.762x51Box", 3}, {"Base.Bullets9mmBox", 3}, {"Base.223Box", 3}, {"Base.308Box", 3}, {"Base.Bullets38Box", 3} }},
		{ weight = 2, items = { "Base.OCP_Camo_Elbow_Pads", "Base.OCP_Camo_Vest", "Base.OCP_Camo_Knee_Pads", "Base.OCP_Camo_Helmet", {"Base.556Box", 3}, { "Base.ShotgunShellsBox", 3}, {"Base.762x39Box", 3}, {"Base.762x51Box", 3}, {"Base.Bullets9mmBox", 3}, {"Base.223Box", 3}, {"Base.308Box", 3}, {"Base.Bullets38Box", 3} }},
		{ weight = 2, items = { "Base.Alpine_Camo_Elbow_Pads", "Base.Alpine_Camo_ShoulderPads_Patriot", "Base.SnowAlpine_Camo_Knee_Pads", "Base.SnowCamo_Helmet_Patriot", {"Base.556Box", 3}, { "Base.ShotgunShellsBox", 3}, {"Base.762x39Box", 3}, {"Base.762x51Box", 3}, {"Base.Bullets9mmBox", 3}, {"Base.223Box", 3}, {"Base.308Box", 3}, {"Base.Bullets38Box", 3} }},
		{ weight = 2, items = { "Base.Black_Camo_KneePads_Defender", "Base.BlackCamo_Helmet_Defender", {"Base.556Box", 5}, { "Base.ShotgunShellsBox", 5}, {"Base.762x39Box", 5}, {"Base.762x51Box", 5}, {"Base.Bullets9mmBox", 5}, {"Base.223Box", 5}, {"Base.308Box", 5}, {"Base.Bullets38Box", 5} }},
		{ weight = 2, items = { "Base.Alpine_Camo_KneePads_Defender", "Base.SnowCamo_Helmet_Defender", {"Base.556Box", 5}, { "Base.ShotgunShellsBox", 5}, {"Base.762x39Box", 5}, {"Base.762x51Box", 5}, {"Base.Bullets9mmBox", 5}, {"Base.223Box", 5}, {"Base.308Box", 5}, {"Base.Bullets38Box", 5} }},
		{ weight = 2, items = { "Base.Forest_Camo_KneePads_Defender", "Base.ForestCamo_Helmet_Defender", {"Base.556Box", 5}, { "Base.ShotgunShellsBox", 5}, {"Base.762x39Box", 5}, {"Base.762x51Box", 5}, {"Base.Bullets9mmBox", 5}, {"Base.223Box", 5}, {"Base.308Box", 5}, {"Base.Bullets38Box", 5} }},
		{ weight = 2, items = { "Base.Desert_Camo_KneePads_Defender", "Base.DesertCamo_Helmet_Defender", {"Base.556Box", 5}, { "Base.ShotgunShellsBox", 5}, {"Base.762x39Box", 5}, {"Base.762x51Box", 5}, {"Base.Bullets9mmBox", 5}, {"Base.223Box", 5}, {"Base.308Box", 5}, {"Base.Bullets38Box", 5} }},
	},
}

---@param lootTier number from 1-3 (currently lines up with the dungeon difficulties)
function WLD_LootTable:new(lootTier)
	local o = WLD_LootTable.parentClass.new(self)
	o.lootTier = lootTier
	return o
end

--- Rolls loot and gives it to the player
function WLD_LootTable:rollLoot()
	local lootTable = WLD_LootTable.tables[self.lootTier]

	if not lootTable then
		WL_Utils.addErrorToChat("ERROR - No loot table for tier: " .. tostring(self.lootTier))
		return
	end

	local lootItems = self:rollOnLootTable(lootTable)

	if not lootItems then
		WL_Utils.addErrorToChat("ERROR - No loot produced from roll. Tier: " .. tostring(self.lootTier))
		return
	end

	self:addItemsToInventory(lootItems)

	return lootItems
end

function WLD_LootTable:rollOnLootTable(lootTable)
	local totalWeight = 0
	for _, entry in ipairs(lootTable) do
		totalWeight = totalWeight + entry.weight
	end

	local randomValue = ZombRand(0, totalWeight) + 1  -- Random number between 1 and totalWeight

	for _, entry in ipairs(lootTable) do 	-- Find the item that corresponds to the random value
		randomValue = randomValue - entry.weight
		if randomValue <= 0 then
			return entry.items
		end
	end
	return nil -- Shouldn't happen
end

function WLD_LootTable:addItemsToInventory(items)
	for _, item in ipairs(items) do
		if type(item) == "string" then
			WL_Utils.addItemToInventory(item)
		elseif type(item) == "table" then
			WL_Utils.addItemToInventory(item[1], item[2])
		end
	end
end

--- Testing and debugging function
function WLD_testLoot(tier)
	local lootTable = WLD_LootTable:new(tier)
	lootTable:rollLoot(getPlayer())
end