require "WL_Zone"
require "WL_TriggerZones"

WLD_BaseObjective = WLBaseObject:derive("WLD_BaseObjective")

function WLD_BaseObjective:new(dungeon, objectiveId, objectiveDescription)
    local o = WLD_BaseObjective.super(self)  -- call inherited constructor
    o.dungeon = dungeon
    o.objectiveId = objectiveId
    o.objectiveDescription = objectiveDescription
    o.otherObjectives = {}
    o.activated = false
    o.onlyActivator = false
    o.isQuiet = false
    return o
end

function WLD_BaseObjective:addRequirement(objectiveId)
    table.insert(self.otherObjectives, objectiveId)
end

function WLD_BaseObjective:shouldReceiveMessages(player, didTrigger)
    return (not self.onlyActivator) or didTrigger
end

function WLD_BaseObjective:isAbleToTrigger(player)
    if not self.dungeon:isAlreadyInDungeon(player) then return false end
    if self.dungeon:isObjectiveComplete(self.objectiveId) then return false end
    for _, objective in ipairs(self.otherObjectives) do
        if not self.dungeon:isObjectiveComplete(objective) then
            return false
        end
    end
    return true
end

function WLD_BaseObjective:activate()
    if self.activated then return end
    self:onActivate()
    self.activated = true
    WLD_Helper.displayMessage(getPlayer(), "info", "Current Objective: " .. self.objectiveDescription)
end

function WLD_BaseObjective:deactivate()
    if not self.activated then return end
    self:onDeactivate()
    self.activated = false
end

function WLD_BaseObjective:onActivate()
end

function WLD_BaseObjective:onDeactivate()
end

function WLD_BaseObjective:setOnlyActivator()
    self.onlyActivator = true
end

function WLD_BaseObjective:setQuiet()
    self.isQuiet = true
end