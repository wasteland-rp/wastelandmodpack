WLD_DefendObjective = WLD_BaseObjective:derive("WLD_DefendObjective")

function WLD_DefendObjective:new(dungeon, objectiveId, objectiveDescription, x, y, z, r, tx, ty, tz, zombieCountPerSpawn, secondsPerSpawn, type, duration)
    local o = WLD_DefendObjective:super(dungeon, objectiveId, objectiveDescription)
    o.x = x
    o.y = y
    o.z = z
    o.r = r
    o.tx = tx
    o.ty = ty
    o.tz = tz
    o.zombieCountPerSpawn = zombieCountPerSpawn
    o.secondsPerSpawn = secondsPerSpawn
    o.type = type
    o.duration = duration
    return o
end

function WLD_DefendObjective:onActivate()
    local player = getPlayer()
    sendClientCommand(player, "WastelandDungeons", "startDefender", {
        self.dungeon.id,
        self.objectiveId,
        self.x,
        self.y,
        self.z,
        self.r,
        self.tx or self.x,
        self.ty or self.y,
        self.tz or self.z,
        self.zombieCountPerSpawn,
        self.secondsPerSpawn,
        self.type,
        self.duration
    })

    -- alert players about the objective
    WLD_Helper.displayMessage(player, "ooc", "   ")
    WLD_Helper.displayMessage(player, "ooc", "This is a Defend Objective!")
    WLD_Helper.displayMessage(player, "ooc", "1. You must stay in this area to complete the objective.")
    WLD_Helper.displayMessage(player, "ooc", "2. If you flee, you will fail the objective and be removed from the dungeon.")
    WLD_Helper.displayMessage(player, "ooc", "3. You must survive for " .. WL_Utils.toHumanReadableTime(self.duration * 1000) .. "!")

    local ob = self

    self.mustStayInZone = WL_Zone:new(self.x - self.r, self.y - self.r, 0, self.x + self.r, self.y + self.r, 7)
    self.mustStayInZone.mapDisabled = false
    self.mustStayInZone.mapType = "Dungeon Defend Area"
    self.mustStayInZone.onPlayerExitedZone = function ()
        ob:onLeave(player)
    end
    WL_TriggerZones.addZone(self.mustStayInZone)

    self.warnZone = WL_Zone:new(self.x - (self.r * 0.85), self.y - (self.r * 0.85), 0, self.x + (self.r * 0.85), self.y + (self.r * 0.85), 7)
    self.warnZone.mapDisabled = true
    self.warnZone.onPlayerExitedZone = function ()
        ob:warnLeaving(player)
    end
    self.warnZone.onPlayerEnteredZone = function ()
        ob:inZone(player)
    end
    WL_TriggerZones.addZone(self.warnZone)
end

function WLD_DefendObjective:onDeactivate()
    if self.mustStayInZone then
        self.mustStayInZone.onPlayerExitedZone = function() end
        WL_TriggerZones.removeZone(self.mustStayInZone)
        self.mustStayInZone:delete()
        self.mustStayInZone = nil
    end
    if self.warnZone then
        self.warnZone.onPlayerExitedZone = function() end
        self.warnZone.onPlayerEnteredZone = function() end
        WL_TriggerZones.removeZone(self.warnZone)
        self.warnZone:delete()
        self.warnZone = nil
    end
end

function WLD_DefendObjective:onLeave(player)
    local px = player:getX()
    local py = player:getY()
    -- If the player is not in the dungeon zone, then they probably died and respawned outside the dungeon
    -- Give them a chance to tp to their bodies
    if not self.dungeon.dungeonZone:isInZone(px, py, 0) then
        return
    end

    WLD_Helper.displayMessage(player, "error", "You have failed the objective!")
    WLD_Helper.displayMessage(player, "error", "You have been removed from the dungeon.")
    self.dungeon:playerExited(player)
    WLP_Next_Teleport = {
        character = player,
        x = self.dungeon.exitPortal.target.x,
        y = self.dungeon.exitPortal.target.y,
        z = self.dungeon.exitPortal.target.z,
    }
end

function WLD_DefendObjective:warnLeaving(player)
    player:addLineChatElement("You are leaving the defend area!", 1, 0.3, 0.3)
    WLD_Helper.displayMessage(player, "error", "You are leaving the defend area!")
    WLD_Helper.displayMessage(player, "error", "Return to the defend area or you will fail the objective!")
end

function WLD_DefendObjective:inZone(player)
    WLD_Helper.displayMessage(player, "success", "You are in the defend area!")
end