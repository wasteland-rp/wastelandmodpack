---
--- WLD_EnterZoneObjective.lua
--- 30/10/2023
---
---
require "WL_Zone"
require "WL_TriggerZones"
require "WLD_Objectives/WLD_BaseObjective"

WLD_EnterZoneObjective = WLD_BaseObjective:derive("WLD_EnterZoneObjective")

function WLD_EnterZoneObjective:new(dungeon, objectiveId, objectiveDescription, x1, y1, z1, x2, y2, z2)
    local o = WLD_EnterZoneObjective:super(dungeon, objectiveId, objectiveDescription)
    o.zone = WL_Zone:new(x1, y1, z1, x2, y2, z2)
    o.zone.onPlayerEnteredZone = function(_, player) o:onPlayerEnteredZone(player) end
    o.zone.mapDisabled = false
    o.zone.mapType = "Dungeon Objective"
    return o
end

-- function WLD_EnterZoneObjective:shouldReceiveMessages(player, didTrigger)
--     if didTrigger then return true end
--     return self.zone:isPlayerNearZone(player, 10)
-- end

function WLD_EnterZoneObjective:onActivate()
    WL_TriggerZones.addZone(self.zone)
    local center = self.zone:getCenterPoint()
    self.homingArrow = WL_HomingArrows.addArrow(center.x, center.y, center.z)
    self.homingArrow.color = { r = 255, g = 215, b = 0, a = 0.8}
    self.homingArrow.range = 35
end

function WLD_EnterZoneObjective:onDeactivate()
    WL_HomingArrows.removeArrow(self.homingArrow)
    WL_TriggerZones.removeZone(self.zone)
end

function WLD_EnterZoneObjective:onPlayerEnteredZone(player)
    self.dungeon:sendObjectiveCompleted(player, self.objectiveId)
    if not self.isQuiet then
        addSound(player, player:getX(), player:getY(), player:getZ(), 150, 1)
    end
end

