WLD_BaseTrigger = WL_Zone:derive("WLD_BaseTrigger")

--- @class WLD_BaseTrigger : WL_Zone
function WLD_BaseTrigger:new(id, dungeon, x1, y1, z1, x2, y2, z2, args)
    local o = WLD_BaseTrigger.super(self, x1, y1, z1, x2, y2, z2)
    o:init(args)
    o.triggerId = id
    o.dungeon = dungeon
    o.isSet = false
    o.mapType = "Dungeon Trigger"
    o.mapColor = {0.3, 0.3, 1.0}
    return o
end

function WLD_BaseTrigger:setRequiredObjectives(objectives)
    self.requiredObjectives = objectives
end

function WLD_BaseTrigger:set()
    if self.isSet then return end
    self.isSet = true
    WL_TriggerZones.addZone(self)
end

function WLD_BaseTrigger:unset()
    if not self.isSet then return end
    self.isSet = false
    WL_TriggerZones.removeZone(self)
end

function WLD_BaseTrigger:onPlayerEnteredZone(player)
    if self.requiredObjectives then
        for _, objectiveId in pairs(self.requiredObjectives) do
            if self.dungeon:isObjectiveComplete(objectiveId) then
                return
            end
        end
    end
    self.dungeon:sendTrigger(player, self)
end

function WLD_BaseTrigger:init(args)
end

function WLD_BaseTrigger:activate(player)
end