require "WLD_Triggers/WLD_BaseTrigger"

WLD_MakeSoundTrigger = WLD_BaseTrigger:derive("WLD_MakeSoundTrigger")

function WLD_MakeSoundTrigger:init(args)
    self.position = {x = args.x, y = args.y, args.z}
    self.radius = args.radius or 1
end

function WLD_MakeSoundTrigger:activate(player)
    WLD_Helper.makeSound(self.position.x, self.position.y, self.position.z, self.radius)
end