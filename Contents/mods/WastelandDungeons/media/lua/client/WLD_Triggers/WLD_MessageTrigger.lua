require "WLD_Triggers/WLD_BaseTrigger"

WLD_MessageTrigger = WLD_BaseTrigger:derive("WLD_MessageTrigger")

function WLD_MessageTrigger:init(args)
    self.messages = args.messages
end

function WLD_MessageTrigger:activate(player)
    WLD_Helper.doMessages(self.messages)
end