require "WLD_Triggers/WLD_BaseTrigger"

WLD_SpawnHordeAreaTrigger = WLD_BaseTrigger:derive("WLD_SpawnHordeAreaTrigger")

function WLD_SpawnHordeAreaTrigger:init(args)
    self.count = args.count
    self.perPerson = args.perPerson or false
    self.area = {x1 = args.x1, y1 = args.y1, x2 = args.x2, y2 = args.y2, z = args.z}
end

function WLD_SpawnHordeAreaTrigger:activate(player)
    local count = self.perPerson and (self.count * self.dungeon:countCurrentPlayers()) or self.count
    WLD_Helper.spawnHordeArea(count, self.area.x1, self.area.y1, self.area.x2, self.area.y2, self.area.z)
end