require "WLD_Triggers/WLD_BaseTrigger"

WLD_SpawnHordeTrigger = WLD_BaseTrigger:derive("WLD_SpawnHordeTrigger")

function WLD_SpawnHordeTrigger:init(args)
    self.count = args.count
    self.position = {x = args.x, y = args.y, z = args.z}
    self.radius = args.radius or 1
end

function WLD_SpawnHordeTrigger:activate(player)
    WLD_Helper.spawnHorde(self.count, self.position.x, self.position.y, self.position.z, self.radius)
end