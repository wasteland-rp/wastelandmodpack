require "WLD_Triggers/WLD_BaseTrigger"

WLD_UnlockDoorTrigger = WLD_BaseTrigger:derive("WLD_UnlockDoorTrigger")

function WLD_UnlockDoorTrigger:init(args)
    self.x = args.x
    self.y = args.y
    self.z = args.z
end

function WLD_UnlockDoorTrigger:activate(player)
    local square = getCell():getGridSquare(self.x, self.y, self.z)
    if square then
        local objects = square:getObjects()
        for i=0, objects:size()-1 do
            local object = objects:get(i)
            if object and (instanceof(object, "IsoDoor") or (instanceof(object, "IsoThumpable") and object:isDoor())) then
                object:setLockedByKey(false)
                object:setIsLocked(false)
                if instanceof(object, "IsoDoor") then
                    object:setLocked(false)
                end
            end
        end
    end
end