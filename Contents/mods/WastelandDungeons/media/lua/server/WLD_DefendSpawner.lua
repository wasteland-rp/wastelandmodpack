if not isServer() then return end

WLD_DefendSpawner = {}
local defendSpawners = {}

local function logIt(message)
    writeLog("Dungeons", message)
end

local nextId = 1
function WLD_DefendSpawner.startNew(x, y, z, r, tx, ty, tz, zombieCountPerSpawn, secondsPerSpawn, type)
    local id = nextId
    nextId = nextId + 1
    defendSpawners[id] = {
        x = x,
        y = y,
        z = z,
        r = r,
        tx = tx,
        ty = ty,
        tz = tz,
        zombieCountPerSpawn = zombieCountPerSpawn,
        secondsPerSpawn = secondsPerSpawn,
        type = type,
        lastSpawn = 0,
        lastNoise = 0,
        offset = 0,
    }
    logIt("Defend Spawner started: " .. id)
    return id
end

function WLD_DefendSpawner.stop(id)
    logIt("Defend Spawner stopped: " .. id)
    defendSpawners[id] = nil
end

function WLD_DefendSpawner.spawnCirlce(data)
    local z = data.z
    local amount = data.zombieCountPerSpawn / 8
    data.offset = (data.offset or 0) + 15
    for i = 1, 8 do
        local x = math.floor(data.x + math.cos(math.rad((i * 45) + data.offset)) * data.r)
        local y = math.floor(data.y + math.sin(math.rad((i * 45) + data.offset)) * data.r)
        WLD_Helper.spawnHorde(amount, x, y, z, 8)
    end
end

function WLD_DefendSpawner.OnTick()
    local ts = getTimestamp()
    for id, data in pairs(defendSpawners) do
        if ts - data.lastSpawn > data.secondsPerSpawn then
            if data.type == "circle" then
                WLD_DefendSpawner.spawnCirlce(data)
            end
            data.lastSpawn = ts
        end

        if ts - data.lastNoise > 5 then
            local x = data.tx or data.x
            local y = data.ty or data.y
            local z = data.tz or data.z
            getWorldSoundManager():addSound(nil, x, y, z, data.r + 20, 600, false, data.r * 0.1, 0)
            data.lastNoise = ts
        end
    end
end

Events.OnTick.Add(WLD_DefendSpawner.OnTick)