---
--- WLD_Server.lua
--- 01/11/2023
---
if not isServer() then return end

require("WLD_ServerData")

local function logIt(message)
    writeLog("Dungeons", message)
end

--- @type table<string, WLD_ServerData>
WLD_KnownDungeons = {}
WLD_DungeonConfigs = {}

local Database = {
    Private = {},
    Public = {},
}

local function SavePrivateData()
    ModData.add("WLD_DungeonStates:Private", Database.Private)
end

local function SavePublicData()
    ModData.add("WLD_DungeonStates", Database.Public)
    ModData.transmit("WLD_DungeonStates")
end

local function CheckSaves()
    local wasPrivateUpdate = false
    local wasPublicUpdate = false
    for dungeonId, dungeonData in pairs(WLD_KnownDungeons) do
        if dungeonData.needsPrivateUpdate then
            Database.Private[dungeonId] = dungeonData:getPrivateData()
            wasPrivateUpdate = true
        end
        if dungeonData.needsPublicUpdate then
            Database.Public[dungeonId] = dungeonData:getPublicData()
            wasPublicUpdate = true
        end
    end
    if wasPrivateUpdate then
        SavePrivateData()
    end
    if wasPublicUpdate then
        SavePublicData()
    end
end

local function InitDungeon(dungeonId, config, publicData, privateData)
    WLD_KnownDungeons[dungeonId] = WLD_ServerData:new(dungeonId, config, publicData, privateData)
end

local function SendNeededCellsForReset()
    for dungeonId, dungeon in pairs(WLD_KnownDungeons) do
        if dungeon:needsCellsReset() then
            local cells = dungeon.config.mapChunks
            local file = getFileWriter("WLD_ToReset.txt", true, true)
            if file then
                for i=1, #cells do
                    file:writeln(string.format("%d,%d", cells[i][1], cells[i][2]))
                end
                file:close()
                dungeon:resetStarted()
            else
                logIt(string.format("ERROR | WLD_ToReset.txt not writable | %s", dungeonId))
            end
        end
    end
end

local function CheckResetCells()
    local file = getFileReader("WLD_ResetComplete.txt", true)
    if not file then return end
    local line = file:readLine()
    while line do
        local parts = string.split(line, ",")
        local cell = {tonumber(parts[1]), tonumber(parts[2])}
        local didConsume = false
        for dungeonId, dungeon in pairs(WLD_KnownDungeons) do
            if dungeon:cellWasReset(cell) then
                didConsume = true
            end
        end
        if not didConsume then
            logIt(string.format("ERROR | Cell Reset Unknown | %d,%d", cell[1], cell[2]))
        end
        line = file:readLine()
    end
    file:close()
    file = getFileWriter("WLD_ResetComplete.txt", true, false)
    file:write("")
    file:close()
end

local Commands = {}

function Commands.getConfigs(player)
    sendServerCommand(player, "WastelandDungeons", "configs", WLD_DungeonConfigs)
end

function Commands.resetConfigs(player)
    WLD_DungeonConfigs = {}
    Commands.readConfig(Database.Private, Database.Public)
    sendServerCommand("WastelandDungeons", "configs", WLD_DungeonConfigs)
end

function Commands.enterDungeon(player, args)
    local dungeonId = args.dungeonId
    local username = player:getUsername()
    local dungeon = WLD_KnownDungeons[dungeonId]
    if not dungeon then
        logIt(string.format("ERROR | enterDungeon | Unknown Dungeon | %s", dungeonId))
        return
    end
    dungeon:playerJoined(username)
    CheckSaves()
end

function Commands.completeObjective(player, args)
    local dungeonId = args.dungeonId
    local objectiveId = args.objectiveId
    local username = player:getUsername()
    local dungeon = WLD_KnownDungeons[dungeonId]
    if not dungeon then
        logIt(string.format("ERROR | completeObjective | Unknown Dungeon | %s", dungeonId))
        return
    end
    dungeon:completeObjective(objectiveId, username)
    CheckSaves()
end

function Commands.leaveDungeon(player, args)
    local dungeonId = args.dungeonId
    local username = player:getUsername()
    local dungeon = WLD_KnownDungeons[dungeonId]
    if not dungeon then
        logIt(string.format("ERROR | leaveDungeon | Unknown Dungeon | %s", dungeonId))
        return
    end
    dungeon:playerLeft(username)
    CheckSaves()
end

function Commands.resetDungeon(player, args)
    local dungeonId = args.dungeonId
    local username = player:getUsername()
    local dungeon = WLD_KnownDungeons[dungeonId]
    if not dungeon then
        logIt(string.format("ERROR | resetDungeon | Unknown Dungeon | %s", dungeonId))
        return
    end
    logIt(string.format("%s | Admin Reset | %s", dungeonId, username))
    dungeon:startReset()
    CheckSaves()
end

function Commands.clearHistory(player, args)
    local dungeonId = args.dungeonId
    local username = player:getUsername()
    local dungeon = WLD_KnownDungeons[dungeonId]
    if not dungeon then
        logIt(string.format("ERROR | clearHistory | Unknown Dungeon | %s", dungeonId))
        return
    end
    logIt(string.format("%s | Admin Clear History | %s", dungeonId, username))
    dungeon:clearHistory()
    CheckSaves()
end

function Commands.setDungeonStatus(player, args)
    local dungeonId = args.dungeonId
    local status = args.status
    local username = player:getUsername()
    local dungeon = WLD_KnownDungeons[dungeonId]
    if not dungeon then
        logIt(string.format("ERROR | setDungeonStatus | Unknown Dungeon | %s", dungeonId))
        return
    end
    logIt(string.format("%s | Admin Set Status | %s | %s", dungeonId, username, status))
    dungeon:forceStatus(status)
    CheckSaves()
end

function Commands.setDungeonObjectiveChange(player, args)
    local dungeonId = args.dungeonId
    local objectiveId = args.objectiveId
    local change = args.change
    local username = player:getUsername()
    local dungeon = WLD_KnownDungeons[dungeonId]
    if not dungeon then
        logIt(string.format("ERROR | setDungeonObjectiveChange | Unknown Dungeon | %s", dungeonId))
        return
    end
    logIt(string.format("%s | Admin Set Objective | %s | %s | %s", dungeonId, objectiveId, username, tostring(change)))
    dungeon:forceObjective(objectiveId, username, change)
    CheckSaves()
end

function Commands.activateTrigger(player, args)
    local dungeonId = args.dungeonId
    local triggerId = args.triggerId
    local username = player:getUsername()
    local dungeon = WLD_KnownDungeons[dungeonId]
    if not dungeon then
        logIt(string.format("ERROR | activateTrigger | Unknown Dungeon | %s", dungeonId))
        return
    end
    dungeon:activateTrigger(username, triggerId)
    CheckSaves()
end

function Commands.logLootRewards(player, args)
    local dungeonId = args.dungeonId
    local username = player:getUsername()
    local loot = args.loot
    logIt(string.format("%s | Loot Rewards | %s | %s", dungeonId, username, loot))
    CheckSaves()
end

local defenders = {}
function Commands.startDefender(player, args)
    local dungeonId = args[1]
    local dungeon = WLD_KnownDungeons[dungeonId]
    if not dungeon then
        logIt(string.format("ERROR: Unknown dungeon %s", dungeonId))
        return
    end
    local objectiveId = args[2]
    local defenderKey = dungeonId .. ":" .. objectiveId
    if defenders[defenderKey] then
        defenders[defenderKey].players[player:getUsername()] = true
        return
    end

    local x = args[3]
    local y = args[4]
    local z = args[5]
    local r = args[6]
    local tx = args[7]
    local ty = args[8]
    local tz = args[9]
    local zps = args[10] * dungeon:countCurrentPlayers()
    local sps = args[11]
    local type = args[12]
    local dur = args[13]

    local defenderId = WLD_DefendSpawner.startNew(x, y, z, r, tx, ty, tz, zps, sps, type)
    defenders[defenderKey] = {
        dungeonId = dungeonId,
        objectiveId = objectiveId,
        defenderId = defenderId,
        startTime = getTimestamp(),
        duration = dur,
        players = {}
    }
    defenders[defenderKey].players[player:getUsername()] = true
end

function Commands.readConfig(privateData, publicData)
    local file = getFileReader("WLD_DungeonsConfig.lua", true)
    local data = {}
    local line = file:readLine()
    while line do
        table.insert(data, line)
        line = file:readLine()
    end
    file:close()
    local full = table.concat(data, "\n")
    local func, err = loadstring(full)
    if not func then
        logIt(string.format("ERROR: %s", err))
        return
    end
    local success, result = pcall(func)
    if not success then
        logIt(string.format("ERROR: %s", result))
        return
    end

    WLD_DungeonConfigs = result
    for dungeonId, config in pairs(result) do
        local privateDungeonData = privateData[dungeonId] or {}
        local publicDungeonData = publicData[dungeonId] or {}
        InitDungeon(dungeonId, config, publicDungeonData, privateDungeonData)
    end

    CheckSaves()
end

local function processClientCommand(module, command, player, args)
    if module ~= "WastelandDungeons" then return end
    if not Commands[command] then
        logIt(string.format("ERROR: Unknown command %s", command))
        return
    end
    Commands[command](player, args)
end

local function initModData()
    local privateData = ModData.getOrCreate("WLD_DungeonStates:Private")
    local publicData = ModData.getOrCreate("WLD_DungeonStates")

    Commands.readConfig(privateData, publicData)
end

local function checkDungeons()
    for dungeonId, dungeon in pairs(WLD_KnownDungeons) do
        dungeon:checkEmpty()
        dungeon:checkClose()
        dungeon:checkExpirations()
        dungeon:checkPlayers()
        dungeon:checkPendingCompletionEvents()
    end
    CheckSaves()
end

local function CheckDefenders()
    for id, data in pairs(defenders) do
        local dungeon = WLD_KnownDungeons[data.dungeonId]
        if not dungeon or (dungeon.status ~= "running" and dungeon.status ~= "open") or dungeon.objectives[data.objectiveId] then
            WLD_DefendSpawner.stop(data.defenderId)
            defenders[id] = nil
            return
        end
        local seconds = getTimestamp() - data.startTime
        if seconds >= data.duration then
            WLD_DefendSpawner.stop(data.defenderId)
            if WLD_KnownDungeons[data.dungeonId] then
                WLD_KnownDungeons[data.dungeonId]:completeObjective(data.objectiveId, "Defenders", true)
                CheckSaves()
            end
            defenders[id] = nil
        end
    end
end

local function everyOneMinute()
    SendNeededCellsForReset()
    CheckResetCells()
    CheckSaves()
    CheckDefenders()
end

Events.OnClientCommand.Add(processClientCommand)
Events.OnInitGlobalModData.Add(initModData)
Events.EveryOneMinute.Add(everyOneMinute)
Events.EveryTenMinutes.Add(checkDungeons)