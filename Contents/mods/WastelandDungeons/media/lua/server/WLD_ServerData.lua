local WLD_COOP_JOIN_WINDOW_MILLIS = 600000 -- 10 minutes
local WLD_RESET_DELAY_MILLIS = 60000 -- 1 minute
local WLD_TIMEOUT_PLAYER_MILLIS = 1800000 -- 30 minutes
local WLD_BOOT_FREE_TIME = 1800000 -- 30 minutes

--- @class WLD_ServerData
--- @field dungeonId string
--- @field config table
--- @field status string
--- @field currentPlayers table
--- @field playersInternal table
--- @field objectives table
--- @field playerHistory table
--- @field openTime number
--- @field resetTime number
--- @field cellsPending table
--- @field needsPublicUpdate boolean
--- @field needsPrivateUpdate boolean
--- @field pendingCompletionEvents table
WLD_ServerData = {}

local function logIt(message)
    writeLog("Dungeons", message)
end

local function getPlayerFrom(username)
    local players = getOnlinePlayers()
    for i=0, players:size()-1 do
        local player = players:get(i)
        if player:getUsername() == username then
            return player
        end
    end
    return nil
end

function WLD_ServerData:new(dungeonId, config, publicData, privateData)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o:init(dungeonId, config, publicData, privateData)
    return o
end

function WLD_ServerData:init(dungeonId, config, publicData, privateData)
    self.dungeonId = dungeonId
    self.config = config

    self.status = publicData.status or "idle"
    self.currentPlayers = publicData.currentPlayers or {}
    self.objectives = publicData.objectives or {}
    self.playerHistory = publicData.playerHistory or {}
    self.triggers = publicData.triggers or {}

    self.playersInternal = privateData.playersInternal or {}
    self.openTime = privateData.openTime or 0
    self.resetTime = privateData.resetTime or 0
    self.cellsPending = privateData.cellsPending or {}
    self.pendingCompletionEvents = privateData.pendingCompletionEvents or {}

    self.needsPublicUpdate = true
    self.needsPrivateUpdate = true

    for _, player in pairs(self.currentPlayers) do
        player.lastSeen = getTimestampMs() + WLD_BOOT_FREE_TIME
        player.isMissing = true
    end

    logIt(string.format("%s | Initialized | %s", self.dungeonId, self.status))
end

function WLD_ServerData:getPublicData()
    self.needsPublicUpdate = false
    return {
        currentPlayers = self.currentPlayers,
        objectives = self.objectives,
        status = self.status,
        playerHistory = self.playerHistory,
        triggers = self.triggers,
        weatherOverride = self.weatherOverride,
    }
end

function WLD_ServerData:getPrivateData()
    self.needsPrivateUpdate = false
    return {
        openTime = self.openTime,
        resetTime = self.resetTime,
        cellsPending = self.cellsPending,
        playersInternal = self.playersInternal,
        pendingCompletionEvents = self.pendingCompletionEvents,
    }
end

function WLD_ServerData:open()
    logIt(string.format("%s | Opened", self.dungeonId))
    self.status = "open"
    self.openTime = getTimestampMs()
    self.currentPlayers = {}
    self.playersInternal = {}
    self.objectives = {}
    self.triggers = {}

    for objectiveId, _ in pairs(self.config.objectives) do
        self.objectives[objectiveId] = false
    end

    if self.config.triggers then
        for triggerId, _ in pairs(self.config.triggers) do
            self.triggers[triggerId] = false
        end
    end

    self.needsPublicUpdate = true
    self.needsPrivateUpdate = true
end

function WLD_ServerData:completeObjective(objectiveId, username, wasAdmin)
    logIt(string.format("%s | Objective | %s | %s", self.dungeonId, objectiveId, username))

    if not self.currentPlayers[username] and not wasAdmin then
        logIt(string.format("%s | ERROR: Player %s not found in dungeon", self.dungeonId, username))
        return
    end

    if self.objectives[objectiveId] then
        logIt(string.format("%s | ERROR: Objective %s already completed", self.dungeonId, objectiveId))
        return
    end

    self.objectives[objectiveId] = true

    for playerUsername, _ in pairs(self.currentPlayers) do
        local playerObj = getPlayerFrom(playerUsername)
        if playerObj then
            sendServerCommand(playerObj, "WastelandDungeons", "objectiveCompleted", { dungeonId = self.dungeonId, objectiveId = objectiveId, playerUsername = username})
        end
    end

    self:checkObjectivesComplete()
    self.needsPublicUpdate = true
end

function WLD_ServerData:activateTrigger(username, triggerId)
    logIt(string.format("%s | Trigger | %s | %s", self.dungeonId, triggerId, username))

    if not self.currentPlayers[username] then
        logIt(string.format("%s | ERROR: Player %s not found in dungeon", self.dungeonId, username))
        return
    end

    if self.triggers[triggerId] then
        logIt(string.format("%s | ERROR: Trigger %s already activated", self.dungeonId, triggerId))
        return
    end

    self.triggers[triggerId] = true

    if self.config.triggerZones and self.config.triggerZones[triggerId] then
        local triggerData = self.config.triggerZones[triggerId]
        if triggerData.players == "all" then
            for playerUsername, _ in pairs(self.currentPlayers) do
                local playerObj = getPlayerFrom(playerUsername)
                if playerObj then
                    sendServerCommand(playerObj, "WastelandDungeons", "activateTrigger", { dungeonId = self.dungeonId, triggerId = triggerId})
                end
            end
        elseif triggerData.players == "trigger" then
            local playerObj = getPlayerFrom(username)
            if playerObj then
                sendServerCommand(playerObj, "WastelandDungeons", "activateTrigger", { dungeonId = self.dungeonId, triggerId = triggerId})
            end
        end
    end

    self.needsPublicUpdate = true
end

function WLD_ServerData:checkObjectivesComplete()
    for _, complete in pairs(self.objectives) do
        if not complete then
            return
        end
    end
    logIt(string.format("%s | Completed", self.dungeonId))
    self.status = "completed"
    for playerUsername, playerData in pairs(self.currentPlayers) do
        local playerObj = getPlayerFrom(playerUsername)
        if playerObj then
            sendServerCommand(playerObj, "WastelandDungeons", "dungeonCompleted", { dungeonId = self.dungeonId, getsRewards = playerData.isEligableForRewards })
        end
        if playerData.isEligableForRewards then
            self.playerHistory[playerUsername] = getTimestampMs()
        end
    end
    if self.config.completionEvents then
        local ts = getTimestampMs()
        for name, eventData in pairs(self.config.completionEvents) do
            logIt(string.format("%s | Completion Event | %s | Scheduled | %d Minutes", self.dungeonId, name, eventData.delayMinutes or 0))
            self.pendingCompletionEvents[name] = ts + (eventData.delayMinutes or 0) * 60000
        end
        self.needsPrivateUpdate = true
    end
    self.needsPublicUpdate = true
end

function WLD_ServerData:countCurrentPlayers()
    local count = 0
    for _ in pairs(self.currentPlayers) do
        count = count + 1
    end
    return count
end

function WLD_ServerData:playerJoined(username)

    if self.status == "idle" then
        self:open()
    end

    if self.status ~= "open" then
        logIt(string.format("%s | Decline Join | %s | ERROR: Dungeon not open", self.dungeonId, username))
        return
    end

    logIt(string.format("%s | Join | %s", self.dungeonId, username))

    if not self.currentPlayers[username] then
        self.currentPlayers[username] = {
            isEligableForRewards = not self.playerHistory[username] or (getTimestampMs() - self.playerHistory[username]) > self.config.hoursToRepeat * 3600000
        }
        self.playersInternal[username] = {
            lastSeen = getTimestampMs(),
            isMissing = false,
        }
        self.needsPublicUpdate = true
        self.needsPrivateUpdate = true
    end

    if self:countCurrentPlayers() >= self.config.maxPlayers then
        logIt(string.format("%s | Running | Full", self.dungeonId))
        self.status = "running"
        self.needsPublicUpdate = true
        return
    end
end

function WLD_ServerData:playerLeft(username)
    logIt(string.format("%s | Left | %s", self.dungeonId, username))
    if not self.currentPlayers[username] then
        logIt(string.format("%s | ERROR: Player %s not found in dungeon", self.dungeonId, username))
    else
        self.currentPlayers[username] = nil
        self.needsPublicUpdate = true
    end
    if not self.playersInternal[username] then
        logIt(string.format("%s | ERROR: Player %s not found in internal data", self.dungeonId, username))
    else
        self.playersInternal[username] = nil
        self.needsPrivateUpdate = true
    end
    self:checkEmpty()
end

function WLD_ServerData:checkEmpty()
    if self.status ~= "open" and self.status ~= "running" and self.status ~= "completed" then
        return
    end

    if self:countCurrentPlayers() == 0 then
        logIt(string.format("%s | Reset | Empty", self.dungeonId))
        self:startReset()
    end
end

function WLD_ServerData:checkClose()
    if self.status ~= "open" then
        return
    end

    if getTimestampMs() - self.openTime > WLD_COOP_JOIN_WINDOW_MILLIS then
        logIt(string.format("%s | Running | Max Open Time", self.dungeonId))
        self.status = "running"
        self.needsPublicUpdate = true
    end
end

function WLD_ServerData:checkPlayers()
    local ts = getTimestampMs()
    for username, _ in pairs(self.currentPlayers) do
        local playerData = self.playersInternal[username]
        if not playerData then
            logIt(string.format("%s | Left | %s | ERROR: interal data is missing", self.dungeonId, username))
            self.currentPlayers[username] = nil
            self.needsPublicUpdate = true
            self:checkEmpty()
        else
            if getPlayerFrom(username) then
                playerData.lastSeen = ts
                if playerData.isMissing then
                    logIt(string.format("%s | Player Found | %s", self.dungeonId, username))
                    playerData.isMissing = false
                    self.needsPrivateUpdate = true
                    self.needsPublicUpdate = true
                end
            else
                if not playerData.isMissing then
                    logIt(string.format("%s | Player Missing | %s", self.dungeonId, username))
                    playerData.isMissing = true
                    self.needsPrivateUpdate = true
                end
                if ts - playerData.lastSeen > WLD_TIMEOUT_PLAYER_MILLIS then
                    logIt(string.format("%s | Offline Timeout | %s | Player timed out", self.dungeonId, username))
                    self.currentPlayers[username] = nil
                    self.playersInternal[username] = nil
                    self.needsPublicUpdate = true
                    self.needsPrivateUpdate = true
                    self:checkEmpty()
                end
            end
        end
    end
    self:checkEmpty()
end

function WLD_ServerData:checkExpirations()
    local currentTsMs = getTimestampMs()
    local time = self.config.hoursToRepeat * 3600000
    for username, expiration in pairs(self.playerHistory) do
        if (currentTsMs - expiration) > time then
            self.playerHistory[username] = nil
            self.needsPublicUpdate = true
        end
    end
end

function WLD_ServerData:checkPendingCompletionEvents()
    local ts = getTimestampMs()
    for name, eventTs in pairs(self.pendingCompletionEvents) do
        if ts > eventTs then
            logIt(string.format("%s | Completion Event | %s | Trigger", self.dungeonId, name))
            self.pendingCompletionEvents[name] = nil
            if self.config.completionEvents[name] then
                local eventData = self.config.completionEvents[name]
                if eventData.type == "world_power" then
                    WLD_WorldPowerEvent:new(self, eventData.args):trigger()
                end
            end
        end
    end

end

function WLD_ServerData:startReset()
    logIt(string.format("%s | Resetting", self.dungeonId))
    self.status = "resetting"
    self.currentPlayers = {}
    self.playersInternal = {}
    self.currentObjectives = {}
    self.resetTime = getTimestampMs() + WLD_RESET_DELAY_MILLIS
    self.needsPublicUpdate = true
    self.needsPrivateUpdate = true
end

function WLD_ServerData:needsCellsReset()
    if self.status ~= "resetting" then
        return false
    end

    if self.resetTime == 0 then
        return false
    end

    if getTimestampMs() < self.resetTime then
        return false
    end

    return true
end

function WLD_ServerData:resetStarted()
    self.resetTime = 0
    self.cellsPending = {}
    for _, cell in ipairs(self.config.mapChunks) do
        logIt(string.format("%s | Cell Queued | %d,%d", self.dungeonId, cell[1], cell[2]))
        local cellId = string.format("%d,%d", cell[1], cell[2])
        self.cellsPending[cellId] = true
    end
    self.needsPrivateUpdate = true
end

function WLD_ServerData:cellWasReset(cell)
    local cellId = string.format("%d,%d", cell[1], cell[2])
    if not self.cellsPending[cellId] then
        return false
    end
    logIt(string.format("%s | Cell Complete | %d,%d", self.dungeonId, cell[1], cell[2]))
    self.cellsPending[cellId] = nil

    local areAllComplete = true
    for _, _ in pairs(self.cellsPending) do
        areAllComplete = false
        break
    end
    if areAllComplete then
        logIt(string.format("%s | Reset Complete", self.dungeonId))
        self.status = "idle"
        self.currentPlayers = {}
        self.playersInternal = {}
        self.objectives = {}
        self.openTime = 0
        self.resetTime = 0
        self.needsPublicUpdate = true
        self.needsPrivateUpdate = true
    end
    return true
end

function WLD_ServerData:clearHistory()
    self.playerHistory = {}
    self.needsPublicUpdate = true
end

function WLD_ServerData:forceStatus(status)
    self.status = status
    if status == "open" then
        self.openTime = getTimestampMs()
    end
    if status == "idle" then
        self.cellsPending = {}
    end
    self.needsPublicUpdate = true
end

function WLD_ServerData:forceObjective(objectiveId, username, complete)
   if complete then
        self:completeObjective(objectiveId, username, true)
   else
        self.objectives[objectiveId] = false
        self.needsPublicUpdate = true
   end
end