WLD_WorldPowerEvent = {}

function WLD_WorldPowerEvent:new(dungeon, args)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.state = args.state
    return o
end

function WLD_WorldPowerEvent:trigger()
    WLD_Helper.setWorldPower(self.state)
end