WLD_Helper = {}

local function setSandbox(var, value)
    local sandbox = getSandboxOptions()
    local option = sandbox:getOptionByName(var)
    option:setValue(value)
    sandbox:applySettings()
    sandbox:toLua()
    if isServer() then
        sandbox:saveServerLuaFile(getServerName())
        sendServerCommand("WLD_Helper", "setSandbox", {var = var, value = value})
    end
end

local pendingMessages = {}
local nextMessageTime = 0
local function sendNextMessage()
    if #pendingMessages == 0 then return end
    if nextMessageTime > getTimestamp() then return end
    local message = table.remove(pendingMessages, 1)
    local fakeMessage = WL_FakeMessage:new("[npc][UN:NPC]" .. message)
    ISChat.addLineInChat(fakeMessage)
    if #pendingMessages > 0 then
        nextMessageTime = getTimestamp() + 3
    else
        nextMessageTime = 0
        Events.OnTick.Remove(sendNextMessage)
    end
end

function WLD_Helper.setWorldPower(state)
    print("Setting world power to: " .. state)
    local value = -1
    if state == "on" then
        value = 2147483647
    end
    setSandbox("ElecShutModifier", value)
end

function WLD_Helper.setWorldWater(state)
    print("Setting world water to: " .. state)
    local value = -1
    if state == "on" then
        value = 2147483647
    end
    setSandbox("WaterShutModifier", value)
end

function WLD_Helper.spawnHorde(count, x, y, z, radius)
    if isClient() then
        sendClientCommand(getPlayer(), "WLD_Helper", "spawnHorde", {count = count, x = x, y = y, z = z, radius = radius})
        return
    end
    addZombiesInOutfitArea(x - radius, y - radius, x + radius, y + radius, z, count, nil, nil)
end

function WLD_Helper.spawnHordeArea(count, x1, y1, x2, y2, z)
    if isClient() then
        sendClientCommand(getPlayer(), "WLD_Helper", "spawnHordeArea", {count = count, x1 = x1, y1 = y1, x2 = x2, y2 = y2, z = z})
        return
    end
    print("Spawning horde in area: " .. x1 .. ", " .. y1 .. " to " .. x2 .. ", " .. y2 .. " at " .. z)
    addZombiesInOutfitArea(x1, y1, x2, y2, z, count, nil, nil)
end

function WLD_Helper.makeSound(x, y, z, radius)
    if isClient() then
        sendClientCommand(getPlayer(), "WLD_Helper", "makeSound", {x = x, y = y, z = z, radius = radius})
        return
    end
    getWorldSoundManager():addSound(nil, x, y, z, radius, 1)
end

function WLD_Helper.doMessages(messages)
    if isServer() then return end
    for _, message in ipairs(messages) do
        table.insert(pendingMessages, message)
    end
    if nextMessageTime == 0 then
        Events.OnTick.Add(sendNextMessage)
    end
end

--- @param player IsoPlayer The player to display the message to
--- @param type "success"|"info"|"ooc"|"error"
--- @param message string The message to display
function WLD_Helper.displayMessage(player, type, message)
    if isServer() then
        if player then
            sendServerCommand(player, "WLD_Helper", "displayMessage", {type = type, message = message})
        end
        return
    end

    if type == "success" then
        WL_Utils.addToChat(message, {color = "0.3,1,0.3", chatId = WRC and WRC.OocTabId or nil})
        player:addLineChatElement(message, 0.3, 1, 0.3)
    elseif type == "info" then
        WL_Utils.addToChat(message, {color = "1,0.6,0.3", chatId = WRC and WRC.OocTabId or nil})
        player:addLineChatElement(message, 0.3, 0.6, 1)
    elseif type == "ooc" then
        WL_Utils.addToChat(message, {color = "1,1,1", chatId = WRC and WRC.OocTabId or nil})
    elseif type == "error" then
        WL_Utils.addToChat(message, {color = "1,0.3,0.3", chatId = WRC and WRC.OocTabId or nil})
    end
end

if isClient() then
    Events.OnServerCommand.Add(function(module, command, args)
        if module ~= "WLD_Helper" then return end

        if command == "setSandbox" then
            setSandbox(args.var, args.value)
        elseif command == "displayMessage" then
            WLD_Helper.displayMessage(args.type, args.message, getPlayer())
        end
    end)
end

if isServer() then
    Events.OnClientCommand.Add(function(module, command, player, args)
        if module ~= "WLD_Helper" then return end

        if command == "spawnHorde" then
            WLD_Helper.spawnHorde(args.count, args.x, args.y, args.z, args.radius)
        elseif command == "spawnHordeArea" then
            WLD_Helper.spawnHordeArea(args.count, args.x1, args.y1, args.x2, args.y2, args.z)
        elseif command == "makeSound" then
            WLD_Helper.makeSound(args.x, args.y, args.z, args.radius)
        end
    end)
end