if not isClient() then return end

require "UI/WL_CreateZonePanel"
require "WEZ_EventZone"
require "UI/WEZ_ManageZone"
require "UI/WEZ_ListZones"
require "WL_ContextMenuUtils"
require "WL_Utils"

local WEZ_WorldMenu = {}

WEZ_WorldMenu.doMenu = function(playerIdx, context)
    if not isClient() or WL_Utils.canModerate(getPlayer()) then
        local player = getPlayer(playerIdx)
        local x, y = ISCoordConversion.ToWorld(getMouseXScaled(), getMouseYScaled(), player:getZ())
        local zones = WEZ_EventZone.getZonesAt(x, y, player:getZ())

        for i=1,#zones do
            local zone = zones[i]
            if not zone.external then
                context:addOption("Manage: " .. zone.name, zone, WEZ_WorldMenu.manageZone)
            end
        end

        local submenu = WL_ContextMenuUtils.getOrCreateSubMenu(context, "Zones")
        submenu:addOption("List Event Zones" , nil, WEZ_WorldMenu.listZones)
        local startingCoordinates = {
            startX = math.floor(player:getX() - 5),
            startY = math.floor(player:getY() - 5),
            endX = math.floor(player:getX() + 5),
            endY = math.floor(player:getY() + 5),
        }
        submenu:addOption("New Event Zone", startingCoordinates, WEZ_WorldMenu.createZone)
    end
end

function WEZ_WorldMenu.listZones()
    WEZ_ListZones:show()
end

local function createEventZone(name, startX, startY, endX, endY, startZ, endZ)
    local newZone = WEZ_EventZone:new(name, startX, startY, startZ, endX, endY, endZ)
    WEZ_ManageZone:show(newZone)
end

function WEZ_WorldMenu.createZone(startingCoordinates)
    if WEZ_ManageZone.instance then
        WEZ_ManageZone.instance:onClose()
    end
    if WEZ_ListZones.instance then
        WEZ_ListZones.instance:onClose()
    end

    WL_CreateZonePanel:show("Event Zone", startingCoordinates, createEventZone, true)
end

--- @param zone WEZ_EventZone
function WEZ_WorldMenu.manageZone(zone)
    WEZ_ManageZone:show(zone)
end

Events.OnFillWorldObjectContextMenu.Add(WEZ_WorldMenu.doMenu)
