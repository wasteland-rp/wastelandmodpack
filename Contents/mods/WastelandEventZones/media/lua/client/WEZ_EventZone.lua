if not isClient() then return end

require "WL_Zone"

--- @class WEZ_EventZone : WL_Zone
WEZ_EventZone = WEZ_EventZone or WL_Zone:derive("WEZ_EventZone")
--- @type WEZ_EventZone[]
WEZ_EventZones = {}

function WEZ_EventZone.getZonesAt(x, y, z)
    local zones = {}
    for _, zone in pairs(WEZ_EventZones) do
        if zone:isInZone(x, y, z) then
            table.insert(zones, zone)
        end
    end
    return zones
end

function WEZ_EventZone.getWarningZonesAt(x, y)
    local zones = {}
    for _, zone in pairs(WEZ_EventZones) do
        if zone:isInWarningZone(x, y) then
            table.insert(zones, zone)
        end
    end
    return zones
end

function WEZ_EventZone.getInCarZonesAt(x, y)
    local zones = {}
    for _, zone in pairs(WEZ_EventZones) do
        if zone.inCars and zone:isInZone(x, y, 0) then
            table.insert(zones, zone)
        end
    end
    return zones
end

function WEZ_EventZone.getIsScrapZone(x, y, z)
    for _, zone in pairs(WEZ_EventZones) do
        if zone:isInZone(x, y, z) and zone.isScrapZone then
            return true
        end
    end
    return false
end

function WEZ_EventZone.getZone(zoneId)
    return WEZ_EventZones[zoneId]
end

function WEZ_EventZone.dump()
    print(WEZ_tabledump(WEZ_EventZones))
end

function WEZ_EventZone:new(name, x1, y1, z1, x2, y2, z2)
    --- @type WEZ_EventZone
    local o = WEZ_EventZone.parentClass.new(self, x1, y1, z1, x2, y2, z2)  -- call inherited constructor
    o:init()
    o.name = name
    WEZ_EventZones[o.id] = o
    o:save()
    return o
end

function WEZ_EventZone:loadFrom(data)
    local o = WEZ_EventZone.parentClass.new(self, data.minX, data.minY, data.minZ, data.maxX, data.maxY, data.maxZ)
    o:init()

    for key, value in pairs(data) do
        o[key] = value
    end
    WEZ_EventZones[o.id] = o
    return o
end

function WEZ_EventZone:init()
    --- @type string
    self.id = getRandomUUID()
    self.mapType = "Event Zone"
    self.mapColor = {0.3, 0.8, 0.8}

    -- general
    self.name = ""
    self.teleportX = 0
    self.teleportY = 0
    self.teleportZ = 0

    -- zombies
    self.preventZombies = false
    self.percentageSprinters = 0
    self.percentageFastShamblers = 0
    self.percentageSlowShamblers = 0
    self.spawnInterval = 0
    self.spawnCount = 0
    self.spawnMax = 0
    self.spawnRange = 0
    self.spawnCatchup = false
    self.spawnCheckPlayers = false
    self.spawnPlayerRange = 0
    self.noThump = false

    -- players
    self.isAdminOnly = false
    self.isJail = false
    self.isRpZone = false
    self.noDamage = false
    self.isQuiet = false
    self.isScrapZone = false
    self.freeDeathZone = false
    self.damageRate = 0
    self.damagePreventItems = ""
    self.moodleIncrease = 1
    self.moodleIncreaseRate = 0

    -- messages
    self.warningBuffer = 0
    self.warningMessage = ""
    self.enterMessage = ""
    self.exitMessage = ""
    self.inCars = false
    self.inCarsMessage = ""
    self.rpText = ""

    -- weather
    self.weatherWind = 0
    self.weatherWindEnabled = false
    self.weatherClouds = 0
    self.weatherCloudsEnabled = false
    self.weatherFog = 0
    self.weatherFogEnabled = false
    self.weatherPrecipitation = 0
    self.weatherPrecipitationEnabled = false
    self.weatherPrecipitationIsSnow = false
    self.weatherTemperature = 0
    self.weatherTemperatureEnabled = false
    self.weatherDarkness = 0
    self.weatherDarknessEnabled = false
    self.weatherDesaturation = 0
    self.weatherDesaturationEnabled = false
    self.weatherLightExtR = 0
    self.weatherLightExtG = 0
    self.weatherLightExtB = 0
    self.weatherLightExtA = 0
    self.weatherLightIntR = 0
    self.weatherLightIntG = 0
    self.weatherLightIntB = 0
    self.weatherLightIntA = 0
    self.weatherLightEnabled = false

    self.isNonThumpable = false
    self.carTime = 1440
end

function WEZ_EventZone:save()
    if self.external then return end
    sendClientCommand(getPlayer(), "WastelandEventZones", "SetZone", {
        id = self.id,
        minX = self.minX,
        minY = self.minY,
        maxX = self.maxX,
        maxY = self.maxY,
        minZ = self.minZ,
        maxZ = self.maxZ,

        -- general
        name = self.name,
        teleportX = self.teleportX,
        teleportY = self.teleportY,
        teleportZ = self.teleportZ,

        -- zombies
        preventZombies = self.preventZombies,
        percentageSprinters = self.percentageSprinters,
        percentageFastShamblers = self.percentageFastShamblers,
        percentageSlowShamblers = self.percentageSlowShamblers,
        spawnInterval = self.spawnInterval,
        spawnCount = self.spawnCount,
        spawnMax = self.spawnMax,
        spawnRange = self.spawnRange,
        spawnCatchup = self.spawnCatchup,
        spawnCheckPlayers = self.spawnCheckPlayers,
        spawnPlayerRange = self.spawnPlayerRange,
        noThump = self.noThump,

        -- players
        isAdminOnly = self.isAdminOnly,
        isJail = self.isJail,
        isRpZone = self.isRpZone,
        noDamage = self.noDamage,
        isQuiet = self.isQuiet,
        isScrapZone = self.isScrapZone,
        freeDeathZone = self.freeDeathZone,
        damageRate = self.damageRate,
        damagePreventItems = self.damagePreventItems,
        moodleIncrease = self.moodleIncrease,
        moodleIncreaseRate = self.moodleIncreaseRate,

        -- messages
        warningBuffer = self.warningBuffer,
        warningMessage = self.warningMessage,
        enterMessage = self.enterMessage,
        exitMessage = self.exitMessage,
        inCars = self.inCars,
        inCarsMessage = self.inCarsMessage,
        rpText = self.rpText,

        -- weather
        weatherWind = self.weatherWind,
        weatherWindEnabled = self.weatherWindEnabled,
        weatherClouds = self.weatherClouds,
        weatherCloudsEnabled = self.weatherCloudsEnabled,
        weatherFog = self.weatherFog,
        weatherFogEnabled = self.weatherFogEnabled,
        weatherPrecipitation = self.weatherPrecipitation,
        weatherPrecipitationEnabled = self.weatherPrecipitationEnabled,
        weatherPrecipitationIsSnow = self.weatherPrecipitationIsSnow,
        weatherTemperature = self.weatherTemperature,
        weatherTemperatureEnabled = self.weatherTemperatureEnabled,
        weatherDarkness = self.weatherDarkness,
        weatherDarknessEnabled = self.weatherDarknessEnabled,
        weatherDesaturation = self.weatherDesaturation,
        weatherDesaturationEnabled = self.weatherDesaturationEnabled,
        weatherLightExtR = self.weatherLightExtR,
        weatherLightExtG = self.weatherLightExtG,
        weatherLightExtB = self.weatherLightExtB,
        weatherLightExtA = self.weatherLightExtA,
        weatherLightIntR = self.weatherLightIntR,
        weatherLightIntG = self.weatherLightIntG,
        weatherLightIntB = self.weatherLightIntB,
        weatherLightIntA = self.weatherLightIntA,
        weatherLightEnabled = self.weatherLightEnabled
    });
end



function WEZ_EventZone:getMapName()
    return self.name or "Unamed Event Zone"
end

function WEZ_EventZone:delete()
    sendClientCommand(getPlayer(), "WastelandEventZones", "DeleteZone", {id = self.id})
end

function WEZ_EventZone:isInWarningZone(x, y)
    if self.warningBuffer == 0 then
        return false
    end
    if x >= (self.minX-self.warningBuffer) and x <= (self.maxX+1+self.warningBuffer) and y >= (self.minY-self.warningBuffer) and y <= (self.maxY+1+self.warningBuffer) then
        return true
    end
    return false
end

function WEZ_EventZone:isNoThump(toggle)
    self.noThump = toggle
end

function WEZ_EventZone.isQuietZone(x, y, z)
    for _, zone in pairs(WEZ_EventZones) do
        if zone:isInZone(x, y, z) and zone.isQuiet then
            return true
        end
    end
    return false
end

function WEZ_EventZone:isPlayerInWarningZone(player)
    return self:isInWarningZone(player:getX(), player:getY(), player:getZ())
end

function WEZ_EventZone:isPlayerAllowed(player)
    if self.isAdminOnly and not player:isAccessLevel("admin") then
        return false, "You must be an admin to enter this zone."
    end

    return true
end
