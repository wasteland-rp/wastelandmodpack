if not isClient() then return end

require "WL_Utils"

if WEZ_MonitorPlayer then
    Events.OnTick.Remove(WEZ_MonitorPlayer.Check)
    Events.OnClimateTick.Remove(WEZ_MonitorPlayer.OnClimateTick)
else
    WEZ_MonitorPlayer = {}
    WEZ_MonitorPlayer.checkTimeout = 0
    WEZ_MonitorPlayer.checkInterval = 20
    WEZ_MonitorPlayer.zonesIn = {}
    WEZ_MonitorPlayer.zonesWarned = {}
    WEZ_MonitorPlayer.inCarWarned = {}
    WEZ_MonitorPlayer.jailZoneId = false
    WEZ_MonitorPlayer.stillInJail = false
    WEZ_MonitorPlayer.cancelRun = false
    WEZ_MonitorPlayer.healthInfoOnEnter = {}
    WEZ_MonitorPlayer.changedWeather = {}
    WEZ_MonitorPlayer.LastInventoryLog = {}
end

WEZ_MonitorPlayer.SkipClimateUpdate = false

function WEZ_MonitorPlayer.Check()
    local currentlyInZones = {}
    local currentlyWarnedZones = {}
    local currentlyCarWarnedZones = {}
    local player = getPlayer()
    local changedWeather = {}
    if player then
        if player:isGodMod() then
            -- HACK AS FUCK, BUT I AM TIRED AND JUST WANT TO SEE THE GOD DAMN WEATHER
            local x, y, z = player:getX(), player:getY(), player:getZ()
            local zones = WEZ_EventZone.getZonesAt(x, y, z)
            for _, zone in pairs(zones) do
                local w = WEZ_MonitorPlayer.checkWeather(zone)
                for _, change in pairs(w) do
                    changedWeather[change] = true
                end
            end
            for change, _ in pairs(WEZ_MonitorPlayer.changedWeather) do
                if not changedWeather[change] then
                    WEZ_MonitorPlayer.clearWeather(change)
                end
            end
            WEZ_MonitorPlayer.changedWeather = changedWeather
            return
        end
        WEZ_MonitorPlayer.stillInJail = false
        WEZ_MonitorPlayer.cancelRun = false
        local x, y, z = player:getX(), player:getY(), player:getZ()
        local zones = WEZ_EventZone.getZonesAt(x, y, z)
        for _, zone in pairs(zones) do
            currentlyInZones[zone.id] = true
            if not WEZ_MonitorPlayer.cancelRun then
                local weather = WEZ_MonitorPlayer.CheckZone(player, zone)
                for _, change in pairs(weather) do
                    changedWeather[change] = true
                end
            end
        end
        WEZ_MonitorPlayer.CheckTPBackToJail(player)
        local warnedZones = WEZ_EventZone.getWarningZonesAt(x, y)
        for _, zone in pairs(warnedZones) do
            if not WEZ_MonitorPlayer.zonesWarned[zone.id] then
                WEZ_MonitorPlayer.zonesWarned[zone.id] = true
                WEZ_MonitorPlayer.showWarning(player, zone)
            end
            currentlyWarnedZones[zone.id] = true
        end

        if WEZ_MonitorPlayer.isInCar(player) then
            local inCarWarnedZones = WEZ_EventZone.getInCarZonesAt(x, y)
            for _, zone in pairs(inCarWarnedZones) do
                if not WEZ_MonitorPlayer.inCarWarned[zone.id] then
                    WEZ_MonitorPlayer.inCarWarned[zone.id] = true
                    WEZ_MonitorPlayer.showInCarWarning(player, zone)
                end
                currentlyCarWarnedZones[zone.id] = true
            end
        end

        WEZ_MonitorPlayer.LogInventory(player, zones[1])
    end

    for change, _ in pairs(WEZ_MonitorPlayer.changedWeather) do
        if not changedWeather[change] then
            WEZ_MonitorPlayer.clearWeather(change)
        end
    end
    WEZ_MonitorPlayer.changedWeather = changedWeather

    for zoneId, _ in pairs(WEZ_MonitorPlayer.zonesIn) do
        if not currentlyInZones[zoneId] then
            WEZ_MonitorPlayer.showExitZone(player, WEZ_EventZone.getZone(zoneId))
            WEZ_MonitorPlayer.zonesIn[zoneId] = nil
        end
    end
    for zoneId, _ in pairs(WEZ_MonitorPlayer.zonesWarned) do
        if not currentlyWarnedZones[zoneId] then
            WEZ_MonitorPlayer.zonesWarned[zoneId] = nil
        end
    end
    for zoneId, _ in pairs(WEZ_MonitorPlayer.inCarWarned) do
        if not currentlyCarWarnedZones[zoneId] then
            WEZ_MonitorPlayer.inCarWarned[zoneId] = nil
        end
    end
end

function WEZ_MonitorPlayer.CheckZone(player, zone)
    if WEZ_MonitorPlayer.CheckBoot(player, zone) then
        return {}
    end

    if not WEZ_MonitorPlayer.zonesIn[zone.id] then
        WEZ_MonitorPlayer.zonesIn[zone.id] = true
        if zone.noDamage then
            WEZ_MonitorPlayer.CheckAndTagHealth(player)
        end
        WEZ_MonitorPlayer.showEnterZone(player, zone)
    end

    if zone.noThump then
        zone:isNoThump(true)
    else
        zone:isNoThump(false)
    end

    if zone.noDamage and WEZ_MonitorPlayer.healthInfoOnEnter then
        WEZ_MonitorPlayer.EnsureGoodHealth(player)
    end
    WEZ_MonitorPlayer.CheckJail(player, zone)
    WEZ_MonitorPlayer.CheckTeleport(player, zone)
    WEZ_MonitorPlayer.CheckDamage(player, zone)
    WEZ_MonitorPlayer.CheckRP(player, zone)
    WEZ_MonitorPlayer.CheckMoodle(player, zone)
    return WEZ_MonitorPlayer.checkWeather(zone)
end

function WEZ_MonitorPlayer.increaseMoodle(player, moodle, amount)
    if amount >= 100 then amount = 100 end
    if amount < 0 then amount = 0 end
    if moodle == 1 then return end
    if moodle == 2 then
        local boredom = player:getBodyDamage():getBoredomLevel()
        if boredom < 76 then
            player:getBodyDamage():setBoredomLevel(boredom + 0.001)
        end
    elseif moodle == 3 then
        local hunger = player:getStats():getHunger()
        if hunger < 0.55 then
            player:getStats():setHunger(hunger + (0.00000005*amount))
        end
    elseif moodle == 4 then
        local pain = player:getStats():getPain()
        if amount >= 1 and amount <= 15 then
            player:getStats():setPain(pain + amount)
        elseif amount >= 16 and amount <= 25 then
            player:getStats():setPain(pain + amount)
        elseif amount >= 26 and amount <= 30 then
            player:getStats():setPain(pain + amount)
        elseif amount >= 31 then
            player:getStats():setPain(pain + amount)
        end
    elseif moodle == 5 then
        local panic = player:getStats():getPanic()
        if panic < 70 then
            player:getStats():setPanic(panic + (0.000085*amount))
        end
    elseif moodle == 6 then
        local stress = player:getStats():getStress()
        if stress < 0.80 then
            player:getStats():setStress(stress + (0.0000001*amount))
        end
    elseif moodle == 7 then
        local thirst = player:getStats():getThirst()
        if thirst < 0.71 then
            player:getStats():setThirst(thirst + (0.00000005*amount))
        end
    elseif moodle == 8 then
        local unhappyness = player:getBodyDamage():getUnhappynessLevel()
        if unhappyness < 70 then
            player:getBodyDamage():setUnhappynessLevel(unhappyness + (0.0000025*amount))
        end
    end
end

function WEZ_MonitorPlayer.CheckMoodle(player, zone)
    WEZ_MonitorPlayer.increaseMoodle(player, zone.moodleIncrease, zone.moodleIncreaseRate)
end

function WEZ_MonitorPlayer.CheckAndTagHealth(player)
    local bodyDamage = player:getBodyDamage()
    local bodyParts = bodyDamage:getBodyParts()
    local hasInjury = false

    for i=0, bodyParts:size()-1 do
        local bodyPart = bodyParts:get(i)
        if bodyPart:HasInjury() then
            hasInjury = true
            break
        end
    end
    if hasInjury then
        player:setHaloNote("Injured, Event zone will not protect!!!", 255, 0, 0, 60.0)
        WEZ_MonitorPlayer.wasHealthyOnEnter = false
    else
        player:setHaloNote("No Damage Zone", 0, 255, 0, 60.0)
        WEZ_MonitorPlayer.wasHealthyOnEnter = true
    end
end

function WEZ_MonitorPlayer.EnsureGoodHealth(player)
    local bodyDamage = player:getBodyDamage()
    bodyDamage:setOverallBodyHealth(100)
    local bodyParts = bodyDamage:getBodyParts()
    for i=0, bodyParts:size()-1 do
        local bodyPart = bodyParts:get(i)
        bodyPart:SetHealth(100)
        bodyPart:setBurnTime(0)
        bodyPart:SetBitten(false)
        bodyPart:setBleedingTime(0)
        bodyPart:setScratched(false, true)
        bodyPart:setScratchTime(0)
        bodyPart:setCut(false, true)
        bodyPart:setCutTime(0)
        bodyPart:setDeepWounded(false)
        bodyPart:setDeepWoundTime(0)
        bodyPart:setInfectedWound(false)
        bodyPart:SetInfected(false)
        bodyPart:setHaveBullet(false, 0)
        bodyPart:setHaveGlass(false)
        bodyPart:setFractureTime(0)
    end
end

function WEZ_MonitorPlayer.CheckTPBackToJail(player)
    if WEZ_MonitorPlayer.jailZoneId and not WEZ_MonitorPlayer.stillInJail then
        local targetZone = WEZ_EventZone.getZone(WEZ_MonitorPlayer.jailZoneId)
        if targetZone and targetZone.isJail then
            local x, y = targetZone:getClosestPointInsideZone(player:getX(), player:getY(), player:getZ())
            WL_Utils.teleportPlayerToCoords(player, x, y, 0)
        else
            WEZ_MonitorPlayer.jailZoneId = false
        end
    end
end

function WEZ_MonitorPlayer.CheckJail(player, zone)
    if zone.isJail then
        WEZ_MonitorPlayer.jailZoneId = zone.id
        WEZ_MonitorPlayer.stillInJail = true
    end
end

function WEZ_MonitorPlayer.CheckBoot(player, zone)
    local isPlayerAllowed, reason = zone:isPlayerAllowed(player)
    if not isPlayerAllowed then
        local x, y, z = player:getX(), player:getY(), player:getZ()
        local newX, newY, newZ = zone:getClosestPointOutsideZone(x, y, z)
        WEZ_MonitorPlayer.jailZoneId = false
        WEZ_MonitorPlayer.cancelRun = true
        WL_Utils.teleportPlayerToCoords(player, newX, newY, newZ)
        player:setHaloNote(reason, 255, 0, 0, 60.0)
        return true
    end
    return false
end

function WEZ_MonitorPlayer.CheckTeleport(player, zone)
    if zone.teleportX ~= 0 and zone.teleportY ~= 0 then
        if math.floor(zone.teleportX) == math.floor(player:getX()) and math.floor(zone.teleportY) == math.floor(player:getY()) then
            return
        end
        WEZ_MonitorPlayer.jailZoneId = false
        WEZ_MonitorPlayer.cancelRun = true
        WL_Utils.teleportPlayerToCoords(player, zone.teleportX, zone.teleportY, zone.teleportZ)
        player:setHaloNote("Whoosh", 0, 255, 0, 60.0)
    end
end

function WEZ_MonitorPlayer.CheckDamage(player, zone)
    if zone.damageRate > 0 then
        local items = {}
        for item in string.gmatch(zone.damagePreventItems, "([^;]+)") do
            table.insert(items, item)
        end
        local wornItems = WEZ_MonitorPlayer.getWornItems(player)
        for _, item in ipairs(items) do
            for _, wornItem in ipairs(wornItems) do
                if item == wornItem then
                    return
                end
            end
        end
        player:getBodyDamage():ReduceGeneralHealth(zone.damageRate/100)
    end
end

function WEZ_MonitorPlayer.getWornItems(player)
    local wornItems = {}
    local playerItems = player:getInventory():getItems()
    for i=0, playerItems:size()-1 do
        local item = playerItems:get(i)
        if player:isEquippedClothing(item) then
            table.insert(wornItems, item:getFullType())
        end
    end
    return wornItems
end

function WEZ_MonitorPlayer.isInCar(player)
    local vehicle = player:getVehicle()
    if vehicle and vehicle:getDriver() == player then
        return true
    end
    return false
end

--- @param player IsoPlayer
--- @param zone WEZ_EventZone
function WEZ_MonitorPlayer.CheckRP(player, zone)
    if zone.isRpZone then
        player:getStats():setHunger(0.0)
        player:getStats():setThirst(0.0)
        player:getStats():setFatigue(0.0)
        player:getStats():setThirst(0.0)
        player:getBodyDamage():setBoredomLevel(0)
        player:getBodyDamage():setUnhappynessLevel(0)
        player:getBodyDamage():getThermoregulator():reset()
    end
end

--- @param player IsoPlayer
--- @param zone WEZ_EventZone
function WEZ_MonitorPlayer.showWarning(player, zone)
    if zone.warningMessage == "" then
        return
    end
    player:addLineChatElement(zone.warningMessage, 255, 0, 0)
end

--- @param player IsoPlayer
--- @param zone WEZ_EventZone
function WEZ_MonitorPlayer.showInCarWarning(player, zone)
    local vehicle = player:getVehicle()
    vehicle:getModData().WEZ_ZoneEnterTime = getTimestamp()
    player:addLineChatElement(zone.inCarsMessage, 255, 0, 0)
end

--- @param player IsoPlayer
--- @param zone WEZ_EventZone
function WEZ_MonitorPlayer.showEnterZone(player, zone)
    if not zone or zone.enterMessage == "" then
        return
    end
    player:addLineChatElement(zone.enterMessage, 255, 0, 0)
end

local function formatItemList(itemTable)
    local formattedList = {}
    for item, count in pairs(itemTable) do
        table.insert(formattedList, count > 1 and string.format("%s x %d", item, count) or item)
    end
    return #formattedList > 0 and table.concat(formattedList, ", ") or "None"
end

--- @param player IsoPlayer
--- @param zone WEZ_EventZone
function WEZ_MonitorPlayer.LogInventory(player, zone)
    if not zone then return end

    local currentTime = getTimestamp()
    local username = player:getUsername()

    WEZ_MonitorPlayer.LastInventoryLog = WEZ_MonitorPlayer.LastInventoryLog or {}
    if WEZ_MonitorPlayer.LastInventoryLog[username] and 
       (currentTime - WEZ_MonitorPlayer.LastInventoryLog[username]) <= 1800 then
        return
    end
    WEZ_MonitorPlayer.LastInventoryLog[username] = currentTime

    local inventoryItems = {}, {}

    local playerInventory = player:getInventory()
    local allInventoryItems = WEZ_MonitorPlayer.GetContainerItems(playerInventory)
    for itemType, count in pairs(allInventoryItems) do
        inventoryItems[itemType] = (inventoryItems[itemType] or 0) + count
    end

    local logMessage = string.format("Username: %s | Inventory Items: %s",
        username,
        formatItemList(inventoryItems)
    )

    sendClientCommand(getPlayer(), "WEZ_InventoryLog", "log", { log = logMessage })
end

--- @param inventory ItemContainer
function WEZ_MonitorPlayer.GetContainerItems(inventory)
    local items = {}

    for i = 0, inventory:getItems():size() - 1 do
        local item = inventory:getItems():get(i)
        if item then
            local itemType = item:getFullType()
            items[itemType] = (items[itemType] or 0) + 1
            if item:getCategory() == "Container" and item:getInventory() then
                local subItems = WEZ_MonitorPlayer.GetContainerItems(item:getInventory())
                for subItem, count in pairs(subItems) do
                    items[subItem] = (items[subItem] or 0) + count
                end
            end
        end
    end

    return items
end

--- @param player IsoPlayer
--- @param zone WEZ_EventZone
function WEZ_MonitorPlayer.showExitZone(player, zone)
    if not zone or zone.exitMessage == "" then
        return
    end
    player:addLineChatElement(zone.exitMessage, 255, 0, 0)
end

local FLOAT_DESATURATION = 0
local FLOAT_GLOBAL_LIGHT_INTENSITY = 1
local FLOAT_NIGHT_STRENGTH = 2
local FLOAT_PRECIPITATION_INTENSITY = 3
local FLOAT_TEMPERATURE = 4
local FLOAT_FOG_INTENSITY = 5
local FLOAT_WIND_INTENSITY = 6
local FLOAT_WIND_ANGLE_INTENSITY = 7
local FLOAT_CLOUD_INTENSITY = 8
local FLOAT_AMBIENT = 9
local FLOAT_VIEW_DISTANCE = 10
local FLOAT_DAYLIGHT_STRENGTH = 11
local FLOAT_HUMIDITY = 12
local COLOR_GLOBAL_LIGHT = 0
local COLOR_NEW_FOG = 1
local BOOL_IS_SNOW = 0

--- @param zone WEZ_EventZone
function WEZ_MonitorPlayer.checkWeather(zone)
    local cm = getClimateManager()
    local weatherChanges = {}
    if zone.weatherWindEnabled then
        table.insert(weatherChanges, "Wind")
        cm:getClimateFloat(FLOAT_WIND_INTENSITY):setModdedValue((zone.weatherWind or 0) / cm:getMaxWindspeedKph())
        cm:getClimateFloat(FLOAT_WIND_INTENSITY):setModdedInterpolate(1.0)
        cm:getClimateFloat(FLOAT_WIND_INTENSITY):setEnableModded(true)
        cm:getClimateFloat(FLOAT_WIND_INTENSITY):setEnableOverride(false)
    end
    if zone.weatherCloudsEnabled then
        table.insert(weatherChanges, "Clouds")
        cm:getClimateFloat(FLOAT_CLOUD_INTENSITY):setModdedValue(zone.weatherClouds or 0)
        cm:getClimateFloat(FLOAT_CLOUD_INTENSITY):setModdedInterpolate(1.0)
        cm:getClimateFloat(FLOAT_CLOUD_INTENSITY):setEnableModded(true)
        cm:getClimateFloat(FLOAT_CLOUD_INTENSITY):setEnableOverride(false)
    end
    if zone.weatherFogEnabled then
        table.insert(weatherChanges, "Fog")
        cm:getClimateFloat(FLOAT_FOG_INTENSITY):setModdedValue(zone.weatherFog or 0)
        cm:getClimateFloat(FLOAT_FOG_INTENSITY):setModdedInterpolate(1.0)
        cm:getClimateFloat(FLOAT_FOG_INTENSITY):setEnableModded(true)
        cm:getClimateFloat(FLOAT_FOG_INTENSITY):setEnableOverride(false)
    end
    if zone.weatherPrecipitationEnabled then
        table.insert(weatherChanges, "Precipitation")
        cm:getClimateFloat(FLOAT_PRECIPITATION_INTENSITY):setModdedValue(zone.weatherPrecipitation or 0)
        cm:getClimateFloat(FLOAT_PRECIPITATION_INTENSITY):setModdedInterpolate(1.0)
        cm:getClimateFloat(FLOAT_PRECIPITATION_INTENSITY):setEnableModded(true)
        cm:getClimateFloat(FLOAT_PRECIPITATION_INTENSITY):setEnableOverride(false)
        cm:getClimateBool(BOOL_IS_SNOW):setModdedValue(zone.weatherPrecipitationIsSnow or false)
        cm:getClimateBool(BOOL_IS_SNOW):setEnableModded(true)
        cm:getClimateBool(BOOL_IS_SNOW):setEnableOverride(false)
    end
    if zone.weatherTemperatureEnabled then
        table.insert(weatherChanges, "Temperature")
        cm:getClimateFloat(FLOAT_TEMPERATURE):setModdedValue((zone.weatherTemperature or 0) - 40)
        cm:getClimateFloat(FLOAT_TEMPERATURE):setModdedInterpolate(1.0)
        cm:getClimateFloat(FLOAT_TEMPERATURE):setEnableModded(true)
        cm:getClimateFloat(FLOAT_TEMPERATURE):setEnableOverride(false)
    end
    if zone.weatherDarknessEnabled then
        table.insert(weatherChanges, "Darkness")
        cm:getClimateFloat(FLOAT_DAYLIGHT_STRENGTH):setModdedValue(1.0 - zone.weatherDarkness)
        cm:getClimateFloat(FLOAT_DAYLIGHT_STRENGTH):setModdedInterpolate(1.0)
        cm:getClimateFloat(FLOAT_DAYLIGHT_STRENGTH):setEnableModded(true)
        cm:getClimateFloat(FLOAT_DAYLIGHT_STRENGTH):setEnableOverride(false)
        cm:getClimateFloat(FLOAT_NIGHT_STRENGTH):setModdedValue(zone.weatherDarkness)
        cm:getClimateFloat(FLOAT_NIGHT_STRENGTH):setModdedInterpolate(1.0)
        cm:getClimateFloat(FLOAT_NIGHT_STRENGTH):setEnableModded(true)
        cm:getClimateFloat(FLOAT_NIGHT_STRENGTH):setEnableOverride(false)
        cm:getClimateFloat(FLOAT_AMBIENT):setModdedValue(1.0 - zone.weatherDarkness)
        cm:getClimateFloat(FLOAT_AMBIENT):setModdedInterpolate(1.0)
        cm:getClimateFloat(FLOAT_AMBIENT):setEnableModded(true)
        cm:getClimateFloat(FLOAT_AMBIENT):setEnableOverride(false)
    end
    if zone.weatherDesaturationEnabled then
        table.insert(weatherChanges, "Desaturation")
        cm:getClimateFloat(FLOAT_DESATURATION):setModdedValue(zone.weatherDesaturation or 0)
        cm:getClimateFloat(FLOAT_DESATURATION):setModdedInterpolate(1.0)
        cm:getClimateFloat(FLOAT_DESATURATION):setEnableModded(true)
        cm:getClimateFloat(FLOAT_DESATURATION):setEnableOverride(false)
    end
    if zone.weatherLightEnabled then
        table.insert(weatherChanges, "Light")
        local overrideColor = ClimateColorInfo.new(
            zone.weatherLightIntR/255, zone.weatherLightIntG/255, zone.weatherLightIntB/255, zone.weatherLightIntA/255,
            zone.weatherLightExtR/255, zone.weatherLightExtG/255, zone.weatherLightExtB/255, zone.weatherLightExtA/255
        )
        cm:getClimateColor(COLOR_GLOBAL_LIGHT):setModdedValue(overrideColor)
        cm:getClimateColor(COLOR_GLOBAL_LIGHT):setModdedInterpolate(1.0)
        cm:getClimateColor(COLOR_GLOBAL_LIGHT):setEnableModded(true)
        cm:getClimateColor(COLOR_GLOBAL_LIGHT):setEnableOverride(false)
    end
    return weatherChanges
end

local function resetWeatherFloat(type)
    getClimateManager():getClimateFloat(type):setEnableModded(false)
    getClimateManager():getClimateFloat(type):setEnableOverride(true)
end

local function resetWeatherColor(type)
    getClimateManager():getClimateColor(type):setEnableModded(false)
    getClimateManager():getClimateColor(type):setEnableOverride(true)
end

function WEZ_MonitorPlayer.clearWeather(type)
    if type == "Wind" then
        resetWeatherFloat(FLOAT_WIND_INTENSITY)
    elseif type == "Clouds" then
        resetWeatherFloat(FLOAT_CLOUD_INTENSITY)
    elseif type == "Fog" then
        resetWeatherFloat(FLOAT_FOG_INTENSITY)
    elseif type == "Precipitation" then
        resetWeatherFloat(FLOAT_PRECIPITATION_INTENSITY)
        getClimateManager():getClimateBool(BOOL_IS_SNOW):setEnableModded(false)
    elseif type == "Temperature" then
        resetWeatherFloat(FLOAT_TEMPERATURE)
    elseif type == "Darkness" then
        resetWeatherFloat(FLOAT_DAYLIGHT_STRENGTH)
        resetWeatherFloat(FLOAT_NIGHT_STRENGTH)
        resetWeatherFloat(FLOAT_AMBIENT)
    elseif type == "Desaturation" then
        resetWeatherFloat(FLOAT_DESATURATION)
    elseif type == "Light" then
        resetWeatherColor(COLOR_GLOBAL_LIGHT)
    end
end

Events.OnTick.Add(WEZ_MonitorPlayer.Check)

-- force the changed weather to reapply on climate ticks
function WEZ_MonitorPlayer.OnClimateTick()
    local player = getPlayer()
    if player:isGodMod() then return end
    local zones = WEZ_EventZone.getZonesAt(player:getX(), player:getY(), player:getZ())
    for _, zone in pairs(zones) do
        local weather = WEZ_MonitorPlayer.checkWeather(zone)
        for _, change in pairs(weather) do
            WEZ_MonitorPlayer.changedWeather[change] = true
        end
    end
end

Events.OnClimateTick.Add(WEZ_MonitorPlayer.OnClimateTick)


-- RP Hints

local RPHintsSystem = {}
RPHintsSystem.name = "EventZones"
function RPHintsSystem.GetHints()
    local hints = {}
    for zoneId, _ in pairs(WEZ_MonitorPlayer.zonesIn) do
        local zone = WEZ_EventZone.getZone(zoneId)
        if zone and zone.rpText and zone.rpText ~= "" then
            table.insert(hints, zone.rpText)
        end
    end
    return hints
end

if getActivatedMods():contains("WastelandRpHints") then
    WRH.RegisterSystem(RPHintsSystem)
end
