---
--- WLF_FC4UseTreadmill.lua
--- 23/09/2024
---

if not getActivatedMods():contains("FC4WT") then return end

require 'TimedActions/FC4UseTreadmill'

local function applyStiffness(stiffness)
	local player = getPlayer()
	if player:getBodyDamage():getBodyPart(BodyPartType.UpperLeg_L):getStiffness() >= stiffness then
		return
	end
	player:getBodyDamage():getBodyPart(BodyPartType.UpperLeg_L):setStiffness(stiffness)
	player:getBodyDamage():getBodyPart(BodyPartType.UpperLeg_R):setStiffness(stiffness)
	player:getBodyDamage():getBodyPart(BodyPartType.LowerLeg_L):setStiffness(stiffness)
	player:getBodyDamage():getBodyPart(BodyPartType.LowerLeg_R):setStiffness(stiffness)
end


local function reportExercisePoints(isTooTired)
	local exercisePointsReport = WLF_PlayerStats.getPointsRemainingString(getPlayer())
	if isTooTired then
		getPlayer():setHaloNote("Too sore to run again right now\n" .. exercisePointsReport, 224, 63, 63, 300.0)
	else
		getPlayer():setHaloNote(exercisePointsReport, 99, 224, 99, 150.0)
	end
end

local treadmillStart = FC4UseTreadmill.start

function FC4UseTreadmill:start()
	self.exercisePointsCache = WLF_PlayerStats.getCurrentPoints(getPlayer())
	if self.exercisePointsCache < 2 then
		self.wasTooTired = true
		self:forceStop()
	else
		reportExercisePoints(false)
		treadmillStart(self)
	end
end

local treadmillUpdate = FC4UseTreadmill.update

function FC4UseTreadmill:update()
	treadmillUpdate(self)

	local pointsUsed = math.ceil(self:getJobDelta() * WLF_PlayerStats.TREADMILL_COST)
	if pointsUsed >= self.exercisePointsCache then
		self:forceStop()
		return
	end
end

local treadmillStop = FC4UseTreadmill.stop

function FC4UseTreadmill:stop()
	if self.wasTooTired then
		reportExercisePoints(true)
		ISBaseTimedAction.stop(self);
	else
		local pointsUsed = math.ceil(self:getJobDelta() * WLF_PlayerStats.TREADMILL_COST)
		WLF_PlayerStats.deductPoints(getPlayer(), pointsUsed)
		reportExercisePoints(false)
		local stiffness = self:getJobDelta() *  WLF_PlayerStats.TREADMILL_STIFFNESS
		applyStiffness(stiffness)
		treadmillStop(self)
	end
end

local treadmillPerform = FC4UseTreadmill.perform

function FC4UseTreadmill:perform()
	WLF_PlayerStats.deductPoints(getPlayer(), WLF_PlayerStats.TREADMILL_COST)
	reportExercisePoints(false)
	applyStiffness(WLF_PlayerStats.TREADMILL_COST)
	treadmillPerform(self)
end