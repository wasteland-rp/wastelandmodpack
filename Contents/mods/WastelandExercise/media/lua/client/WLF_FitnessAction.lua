---
--- WLF_FitnessAction.lua
--- 13/09/2024
---

require 'WLF_PlayerStats'

--- For some reason stiffness values below ~9 are rejected. Max stiffness tends to be around 37 here.
local function applyStiffness(stiffness, bodyAreas)
	stiffness = math.max(stiffness, 10)
	local player = getPlayer()
	if bodyAreas then
		local stiffnessTable = luautils.split(bodyAreas, ",");
		for i,v in ipairs(stiffnessTable) do
			if v == "legs" then
				if player:getBodyDamage():getBodyPart(BodyPartType.UpperLeg_L):getStiffness() < stiffness then
					player:getBodyDamage():getBodyPart(BodyPartType.UpperLeg_L):setStiffness(stiffness)
					player:getBodyDamage():getBodyPart(BodyPartType.UpperLeg_R):setStiffness(stiffness)
					player:getBodyDamage():getBodyPart(BodyPartType.LowerLeg_L):setStiffness(stiffness)
					player:getBodyDamage():getBodyPart(BodyPartType.LowerLeg_R):setStiffness(stiffness)
				end
			elseif v == "arms" then
				if player:getBodyDamage():getBodyPart(BodyPartType.ForeArm_L):getStiffness() < stiffness then
					player:getBodyDamage():getBodyPart(BodyPartType.ForeArm_L):setStiffness(stiffness)
					player:getBodyDamage():getBodyPart(BodyPartType.ForeArm_R):setStiffness(stiffness)
					player:getBodyDamage():getBodyPart(BodyPartType.UpperArm_L):setStiffness(stiffness)
					player:getBodyDamage():getBodyPart(BodyPartType.UpperArm_R):setStiffness(stiffness)
				end
			elseif v == "abs" then
				if player:getBodyDamage():getBodyPart(BodyPartType.Torso_Lower):getStiffness() < stiffness then
					player:getBodyDamage():getBodyPart(BodyPartType.Torso_Lower):setStiffness(stiffness)
				end
			elseif v == "chest" then
				if player:getBodyDamage():getBodyPart(BodyPartType.Torso_Upper):getStiffness() < stiffness then
					player:getBodyDamage():getBodyPart(BodyPartType.Torso_Upper):setStiffness(stiffness)
				end
			end
		end
	end
	getPlayer():getFitness():resetValues() -- Stop the vanilla exercise stiffness from building up
end

local function reportExercisePoints(xpMod, isTooTired)
	local exercisePointsReport = WLF_PlayerStats.getPointsRemainingString(getPlayer())
	if isTooTired then
		getPlayer():setHaloNote("Too sore to exercise anymore\n" .. exercisePointsReport, 224, 63, 63, 300.0)
	else
		getPlayer():setHaloNote("Experience Modifier: " .. tostring(xpMod) .. "\n" .. exercisePointsReport, 99, 224, 99, 150.0)
	end
end

local ISFitnessStart = ISFitnessAction.start

function ISFitnessAction:start()
	ISFitnessStart(self)
	self.exercisePointsCache = WLF_PlayerStats.getCurrentPoints(getPlayer())
	self.pointsAtStart = self.exercisePointsCache
	reportExercisePoints(self.exeData.xpMod, self.exercisePointsCache <= 0)
end

local ISFitnessStop = ISFitnessAction.stop

function ISFitnessAction:stop()
	ISFitnessStop(self)
	local pointsUsed = self.pointsAtStart - self.exercisePointsCache
	WLF_PlayerStats.deductPoints(getPlayer(), pointsUsed)
	if self.repnb > 10 then
		applyStiffness(self.repnb, self.exeData.stiffness)
	end
	reportExercisePoints(self.exeData.xpMod,self.exercisePointsCache <= 0)
end

local ISFitnessPerform = ISFitnessAction.perform

-- This doesn't seem to really be called in practice but it's here just in case
function ISFitnessAction:perform()
	ISFitnessPerform(self)

	local pointsUsed = self.pointsAtStart - self.exercisePointsCache
	WLF_PlayerStats.deductPoints(getPlayer(), pointsUsed)
	if self.repnb > 10 then
		applyStiffness(self.repnb, self.exeData.stiffness)
	end
	reportExercisePoints(self.exeData.xpMod,self.exercisePointsCache <= 0)
end

local ISexeLooped = ISFitnessAction.exeLooped

function ISFitnessAction:exeLooped()
	if self.exercisePointsCache <= 0 then
		self:forceStop()
	else
		ISexeLooped(self)
		self.exercisePointsCache = self.exercisePointsCache - 1
	end
end