---
--- WLF_PlayerStats.lua
--- 23/09/2024
---


WLF_PlayerStats = {}
WLF_PlayerStats.MAX_FITNESS_POINTS = 500
WLF_PlayerStats.TREADMILL_STIFFNESS = 35
WLF_PlayerStats.TREADMILL_COST = 50

WLF_PlayerStats.MILLIS_TO_FULL_RESTORE = 259200000   -- 72 hours
WLF_PlayerStats.MIN_CHECK_TIME_MS = 30000
WLF_PlayerStats.cachedData = {}

--- Checks if player has enough exercise points for a task
function WLF_PlayerStats.hasPointsAvailable(player, pointsNeeded)
	local data = WLF_PlayerStats.getPlayerData(player)
	if not data then return false end
	return (data.pointsRemaining - pointsNeeded) >= 0
end

function WLF_PlayerStats.getCurrentPoints(player)
	local data = WLF_PlayerStats.getPlayerData(player)
	if not data then return false end
	return data.pointsRemaining
end

function WLF_PlayerStats.deductPoints(player, amount)
	local data = WLF_PlayerStats.getPlayerData(player)
	data.pointsRemaining = math.max(0, data.pointsRemaining - amount)
	WL_UserData.Set("WLF_PlayerStats", data)
end

--- Get a human readable string showing how many points remain
function WLF_PlayerStats.getPointsRemainingString(player)
	return "Exercise Remaining: " .. tostring(WLF_PlayerStats.getCurrentPoints(player)) .. "/" .. tostring(WLF_PlayerStats.MAX_FITNESS_POINTS)
end

---@param player IsoPlayer to check data for
---@return table|nil with pointsRemaining and lastCheckedAt keys
function WLF_PlayerStats.getPlayerData(player)
	if not player then return end
	local username = player:getUsername()
	if not WLF_PlayerStats.cachedData[username] then return end
	WLF_PlayerStats.updatePoints(WLF_PlayerStats.cachedData[username])
	return WLF_PlayerStats.cachedData[username]
end

function WLF_PlayerStats.updatePoints(data)
	local currentTimeMillis = getTimestampMs()
	if data.pointsRemaining == WLF_PlayerStats.MAX_FITNESS_POINTS then
		data.lastCheckedAt = currentTimeMillis
		return
	end
	if currentTimeMillis - data.lastCheckedAt < WLF_PlayerStats.MIN_CHECK_TIME_MS then return end
	local timeSinceLastCheck = currentTimeMillis - data.lastCheckedAt
	local pointsRestored = (timeSinceLastCheck / WLF_PlayerStats.MILLIS_TO_FULL_RESTORE) * WLF_PlayerStats.MAX_FITNESS_POINTS
	local newPoints = math.floor(math.min(data.pointsRemaining + pointsRestored, WLF_PlayerStats.MAX_FITNESS_POINTS))
	if newPoints == data.pointsRemaining then return end
	data.pointsRemaining = newPoints
	data.lastCheckedAt = currentTimeMillis
	WL_UserData.Set("WLF_PlayerStats", data)
end

local function receiveData(data, username)
	if data.pointsRemaining == nil then
		data.pointsRemaining = WLF_PlayerStats.MAX_FITNESS_POINTS
	end
	if data.lastCheckedAt == nil then
		data.lastCheckedAt = getTimestampMs()
	end
	WLF_PlayerStats.cachedData[username] = data
end

WL_PlayerReady.Add(function(pIdx, player)
	local username = player:getUsername()
	WL_UserData.Listen("WLF_PlayerStats", username, receiveData)
	WL_UserData.Fetch("WLF_PlayerStats")
end)

Events.OnPlayerDeath.Add(function(player)
	local username = player:getUsername()
	WL_UserData.StopListening("WLF_PlayerStats", username, receiveData)
end)