---
--- WLF_FitnessExercises.lua
--- 13/09/2024
---

FitnessExercises.exercisesType.squats.xpMod = 5
FitnessExercises.exercisesType.pushups.xpMod = 5
FitnessExercises.exercisesType.situp.xpMod = 5
FitnessExercises.exercisesType.burpees.xpMod = 4
FitnessExercises.exercisesType.barbellcurl.xpMod = 6
FitnessExercises.exercisesType.dumbbellpress.xpMod = 9
FitnessExercises.exercisesType.bicepscurl.xpMod = 9