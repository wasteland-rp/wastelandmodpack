---
--- WastelandReloadWeaponAction.lua
--- Overrides for ISReloadWeaponAction functions
--- 11/10/2023
---

-- Remove the original function stored in the Events
Events.OnWeaponSwingHitPoint.Remove(ISReloadWeaponAction.onShoot);

-- Save a version of the original function
local onShootWeapon = ISReloadWeaponAction.onShoot

ISReloadWeaponAction.onShoot = function(player, weapon)
	onShootWeapon(player, weapon)
	if weapon:isJammed() then
		local conditionPercent = weapon:getCondition() / weapon:getConditionMax()
		if conditionPercent > 0.35 then -- if we just jammed but still have a decent weapon condition, remove the jam
			weapon:setJammed(false)
		end
	end
end

-- Add back the new version of the function with our additions
Events.OnWeaponSwingHitPoint.Add(ISReloadWeaponAction.onShoot);

