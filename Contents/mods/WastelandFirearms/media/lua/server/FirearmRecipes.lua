---
--- FirearmRecipes.lua
--- 02/07/2023
---
require "recipecode"

function Recipe.OnCreate.craftPneumaticRifle(items, result, player)
	for i=0,items:size() - 1 do
		if items:get(i):getType() == "Extinguisher" then
			player:getInventory():Remove(items:get(i))
		end
	end
end

function Recipe.OnCreate.craftRedDot(items, result, player)
	for i=0,items:size() - 1 do
		if items:get(i):getType() == "Torch" then
			player:getInventory():Remove(items:get(i))
		end
	end
end

function Recipe.GetItemTypes.HandmadeRevolver(scriptItems)
	scriptItems:add(getScriptManager():getItem("Base.HandcraftedRevolver"))
end

function Recipe.GetItemTypes.HandmadeRifle(scriptItems)
	scriptItems:add(getScriptManager():getItem("Base.HandcraftedRifle"))
	scriptItems:add(getScriptManager():getItem("Base.HandcraftedLeverActionRifle"))
end

function Recipe.GetItemTypes.ThompsonSMG(scriptItems)
	scriptItems:add(getScriptManager():getItem("GreysWLWeaponsFirearms.ScrapThompson45v2"))
end

---@param result InventoryItem
---@param player IsoPlayer
function Recipe.OnCreate.disassembleRifle(items, result, player)
	if ZombRand(0, 3) > 0 then -- 2/3 chance we get it back as ZombRand returns 0, 1, or 2
		player:getInventory():AddItem("Base.LongGunBarrel");
	end

	if ZombRand(0, 3) > 0 then  -- 2/3 chance we get it back as ZombRand returns 0, 1, or 2
		player:getInventory():AddItem("Base.HandCraftedRifleStock");
	end

	local springs = ZombRand(0, 4)  -- Give back 0 to 3 springs
	for i = 1, springs do
		player:getInventory():AddItem("Base.ClipSpring");
	end
end

function Recipe.GetItemTypes.PneumaticPistol(scriptItems)
	scriptItems:add(getScriptManager():getItem("GreysWLWeaponsFirearms.PneumaticPistol"))
end

---@param result InventoryItem
---@param player IsoPlayer
function Recipe.OnCreate.disassemblePneumaticPistol(items, result, player)
	if ZombRand(0, 3) > 0 then  -- 2/3 chance we get it back as ZombRand returns 0, 1, or 2
		player:getInventory():AddItem("Base.Hairspray");
	end

	if ZombRand(0, 3) > 1 then -- 1/3 chance we get it back as ZombRand returns 0, 1, or 2
		player:getInventory():AddItem("Base.MetalPipe");
	end

	local springs = ZombRand(0, 3)  -- Give back 0 to 2 springs
	for i = 1, springs do
		player:getInventory():AddItem("Base.ClipSpring");
	end
end

---@param result InventoryItem
---@param player IsoPlayer
function Recipe.OnCreate.disassembleThompson(items, result, player)
	if ZombRand(0, 3) > 1 then  -- 1/3 chance we get it back as ZombRand returns 0, 1, or 2
		player:getInventory():AddItem("Base.MachinedClipSteel");
	end

	if ZombRand(0, 3) > 1 then -- 1/3 chance we get it back as ZombRand returns 0, 1, or 2
		player:getInventory():AddItem("Base.MetalPipe");
	end

	local springs = ZombRand(0, 3)  -- Give back 0 to 2 springs
	for i = 1, springs do
		player:getInventory():AddItem("Base.ClipSpring");
	end
end

function Recipe.GetItemTypes.PneumaticRifle(scriptItems)
	scriptItems:add(getScriptManager():getItem("GreysWLWeaponsFirearms.ScrapSpas15"))
end

---@param result InventoryItem
---@param player IsoPlayer
function Recipe.OnCreate.disassemblePneumaticRifle(items, result, player)
	if ZombRand(0, 3) > 0 then -- 2/3 chance we get it back as ZombRand returns 0, 1, or 2
		player:getInventory():AddItem("Base.LongGunBarrel");
	end

	if ZombRand(0, 3) > 0 then  -- 2/3 chance we get it back as ZombRand returns 0, 1, or 2
		local extinguisher = player:getInventory():AddItem("Base.Extinguisher");
		if extinguisher then
			extinguisher:setUsedDelta(0)
		end
	end

	if ZombRand(0, 3) > 1 then -- 1/3 chance we get it back as ZombRand returns 0, 1, or 2
		player:getInventory():AddItem("Base.MachinedFirearmComponents");
	end

	local springs = ZombRand(0, 3)  -- Give back 0 to 2 springs
	for i = 1, springs do
		player:getInventory():AddItem("Base.ClipSpring");
	end
end

function Recipe.GetItemTypes.LowTierCraftedPistol(scriptItems)
	scriptItems:add(getScriptManager():getItem("GreysWLWeaponsFirearms.BoltActionScrapPistol"))
	scriptItems:add(getScriptManager():getItem("GreysWLWeaponsFirearms.LastResort"))
end

---@param result InventoryItem
---@param player IsoPlayer
function Recipe.OnCreate.disassembleLowTierCraftedPistol(items, result, player)
	if ZombRand(0, 3) > 0 then -- 2/3 chance we get it back as ZombRand returns 0, 1, or 2
		player:getInventory():AddItem("Base.ScrapMetal");
	end

	if ZombRand(0, 3) > 0 then -- 2/3 chance we get it back as ZombRand returns 0, 1, or 2
		player:getInventory():AddItem("Base.ScrapMetal");
	end

	local springs = ZombRand(0, 3)  -- Give back 0 to 2 springs
	for i = 1, springs do
		player:getInventory():AddItem("Base.ClipSpring");
	end
end

---@param result InventoryItem
---@param player IsoPlayer
function Recipe.OnCreate.gunpowderFromBullets(items, result, player)
	result:setUsedDelta(0.1)

	if ZombRand(0, 5) > 0 then -- 80% chance of getting the casing back
		local shellCasing = InventoryItemFactory.CreateItem("Base.ShellCasings");
		player:getInventory():AddItem(shellCasing)
	end
end

---@param result InventoryItem
---@param player IsoPlayer
function Recipe.OnCreate.gunpowderFromTwoBullets(items, result, player)
	result:setUsedDelta(0.1)

	-- Always get one casing
	local shellCasing = InventoryItemFactory.CreateItem("Base.ShellCasings");
	player:getInventory():AddItem(shellCasing)

	if ZombRand(0, 5) > 0 then -- 80% chance of getting the second casing back
		shellCasing = InventoryItemFactory.CreateItem("Base.ShellCasings");
		player:getInventory():AddItem(shellCasing)
	end
end

---@param result InventoryItem
---@param player IsoPlayer
function Recipe.OnCreate.cutShellCasings(items, result, player)
	for i = 1, 5 do
		player:getInventory():AddItem("Base.ShellCasings");
	end

	for i = 1, 4 do
		local shellCasing = InventoryItemFactory.CreateItem("Base.ShellCasings");
		player:getInventory():AddItem(shellCasing)
	end
end

---@param result InventoryItem
---@param player IsoPlayer
function Recipe.OnCreate.shredToLeadFilings(items, result, player)
	player:getInventory():AddItem("Base.LeadFilings");
	player:getInventory():AddItem("Base.LeadFilings");
	for i=0,items:size() - 1 do
		if instanceof (items:get(i), "HandWeapon") and (items:get(i):getCategories():contains("SmallBlade")
				or items:get(i):getCategories():contains("LongBlade")
				or items:get(i):getCategories():contains("Axe")) then
			items:get(i):setCondition(items:get(i):getCondition() - 1);
		end
		if items:get(i):getType() == "SharpedStone" and ZombRand(3) == 0 then
			player:getInventory():Remove(items:get(i))
		end
	end
end

---@param result InventoryItem
---@param player IsoPlayer
function Recipe.OnCreate.createPrimingCompound(items, result, player)
	player:getInventory():AddItem("Base.TinCanEmpty");
	local usedDelta = 0.7
	local bonusPrimerUnits = 3 * player:getPerkLevel(Perks.Doctor);
	usedDelta = usedDelta + (bonusPrimerUnits / 100)
	result:setUsedDelta(usedDelta)
end


function Recipe.OnTest.False(items, result, player)
	return false
end

function Recipe.OnTest.FirearmsNotImportantAndTools(item)
	-- Allows Tools
	if item:getScriptItem() and item:getScriptItem():getTags():contains("Screwdriver") then return true end
	if item:getFullType() == "Base.BallPeenHammer" then return true end
	if item:getFullType() == "Base.Wrench" then return true end

	-- Nothing equipped, attached, or favorited
    if item:isEquipped() then return false end
    if item:getAttachedSlot() >= 0 then return false end
    if item:isFavorite() then return false end

    return true
end