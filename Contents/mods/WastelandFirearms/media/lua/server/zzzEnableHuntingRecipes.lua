---
--- zzzEnableHuntingRecipes.lua
---
--- This is needed to re enable the firearms mod recipes because we use a custom wasteland version of it now.
---
--- 30/12/2024
---

local function fixRecipes()
	local recipes = getScriptManager():getAllRecipes()

	for i=0,recipes:size()-1 do
		local recipe = recipes:get(i)
		if recipe:getName() == "Hunt Rabbit with M1Garand Rifle" then
			recipe:setIsHidden(false)
		end
		if recipe:getName() == "Hunt Birds with M1Garand Rifle" then
			recipe:setIsHidden(false)
		end
		if recipe:getName() == "Hunt Deer with M1Garand Rifle" then
			recipe:setIsHidden(false)
		end
		if recipe:getName() == "Hunt Pig with M1Garand Rifle" then
			recipe:setIsHidden(false)
		end
		if recipe:getName() == "Hunt Rabbit with M24 Rifle" then
			recipe:setIsHidden(false)
		end
		if recipe:getName() == "Hunt Birds with M24 Rifle" then
			recipe:setIsHidden(false)
		end
		if recipe:getName() == "Hunt Deer with M24 Rifle" then
			recipe:setIsHidden(false)
		end
		if recipe:getName() == "Hunt Pig with M24 Rifle" then
			recipe:setIsHidden(false)
		end
		if recipe:getName() == "Hunt Rabbit with Rugerm7722 Rifle" then
			recipe:setIsHidden(false)
		end
		if recipe:getName() == "Hunt Birds with Rugerm7722 Rifle" then
			recipe:setIsHidden(false)
		end
		if recipe:getName() == "Hunt Deer with Rugerm7722 Rifle" then
			recipe:setIsHidden(false)
		end
		if recipe:getName() == "Hunt Pig with Rugerm7722 Rifle" then
			recipe:setIsHidden(false)
		end
		if recipe:getName() == "Hunt Rabbit with Shotgun (B41)" then
			recipe:setIsHidden(false)
		end
		if recipe:getName() == "Hunt Birds with Shotgun (B41)" then
			recipe:setIsHidden(false)
		end
		if recipe:getName() == "Hunt Deer with Shotgun (B41)" then
			recipe:setIsHidden(false)
		end
		if recipe:getName() == "Hunt Pig with Shotgun (B41)" then
			recipe:setIsHidden(false)
		end
	end
end

if getActivatedMods():contains("firearmmod_WL") then
	fixRecipes()
end
