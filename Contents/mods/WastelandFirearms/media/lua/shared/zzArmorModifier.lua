---
--- zzArmorModifier.lua
--- 16/08/2024
---

require "WL_Utils"

WL_Utils.setItemProperties("Base.Makeshift_Arms_LeatherProtectionsLEFT", {
	["BiteDefense"] = 10,
	["ScratchDefense"] = 10,
	["BulletDefense"] = 0,
	["Weight"] = 0.6,
})

WL_Utils.setItemProperties("Base.Makeshift_Arms_LeatherProtectionsRIGHT", {
	["BiteDefense"] = 10,
	["ScratchDefense"] = 10,
	["BulletDefense"] = 0,
	["Weight"] = 0.6,
})

WL_Utils.setItemProperties("Base.Makeshift_Vest_LeatherChestPlate", {
	["BiteDefense"] = 10,
	["ScratchDefense"] = 10,
	["BulletDefense"] = 0,
	["Weight"] = 0.6,
})

WL_Utils.setItemProperties("Base.Makeshift_Shoulders_LeatherShoulderPads", {
	["BiteDefense"] = 10,
	["ScratchDefense"] = 10,
	["BulletDefense"] = 0,
	["Weight"] = 0.6,
	["NeckProtectionModifier"] = 1,
})

WL_Utils.setItemProperties("Base.Makeshift_LegPad_TireRIGHT", {
	["BiteDefense"] = 6,
	["ScratchDefense"] = 6,
	["BulletDefense"] = 0,
	["Weight"] = 1,
})

WL_Utils.setItemProperties("Base.Makeshift_LegPad_TireLEFT", {
	["BiteDefense"] = 6,
	["ScratchDefense"] = 6,
	["BulletDefense"] = 0,
	["Weight"] = 1,
})

WL_Utils.setItemProperties("Base.Makeshift_Shoulders_TireShoulderPads", {
	["BiteDefense"] = 13,
	["ScratchDefense"] = 13,
	["BulletDefense"] = 0,
	["Weight"] = 1,
	["NeckProtectionModifier"] = 1,
})

WL_Utils.setItemProperties("Base.Makeshift_Shoulders_JunkerShoulderPads", {
	["BiteDefense"] = 15,
	["ScratchDefense"] = 15,
	["BulletDefense"] = 5,
	["Weight"] = 1.3,
	["NeckProtectionModifier"] = 1,
})

WL_Utils.setItemProperties("Base.Makeshift_Shoulders_JunkerShoulderPadsSkull", {
	["BiteDefense"] = 15,
	["ScratchDefense"] = 15,
	["BulletDefense"] = 5,
	["Weight"] = 1.3,
	["NeckProtectionModifier"] = 1,
})

WL_Utils.setItemProperties("Base.SwatElbowPads", {
	["BiteDefense"] = 18,
	["ScratchDefense"] = 18,
	["BulletDefense"] = 12,
	["Weight"] = 1,
})

WL_Utils.setItemProperties("Base.SwatKneePads", {
	["BiteDefense"] = 18,
	["ScratchDefense"] = 18,
	["BulletDefense"] = 12,
	["Weight"] = 1,
})

WL_Utils.setItemProperties("Base.SwatShoulderPads", {
	["BiteDefense"] = 18,
	["ScratchDefense"] = 18,
	["BulletDefense"] = 12,
	["Weight"] = 1,
	["NeckProtectionModifier"] = 1,
})