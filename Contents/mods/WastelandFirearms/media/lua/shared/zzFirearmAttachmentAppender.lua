---
--- zzFirearmAttachmentAppender.lua
---
--- We have a dumb name starting with zz because TIS calls Collections.sort to load lua files, and we need to go
--- last to overwrite other mods messing with the attachments.
---
--- 02/07/2023
---

require "WL_Utils"

local function addAttachmentSlots(weapon, attachment)
	local item = ScriptManager.instance:getItem(attachment)
	if item then
		local itemInstance = item:InstanceItem(nil)
		if itemInstance and instanceof(itemInstance, "WeaponPart") and itemInstance:getMountOn() then
			local mountOnOptions = {}
			for i=0, itemInstance:getMountOn():size()-1 do
				table.insert(mountOnOptions, itemInstance:getMountOn():get(i))
			end
			table.insert(mountOnOptions, weapon)
			local mountOnString = table.concat(mountOnOptions, "; ")
			item:DoParam("MountOn = " .. mountOnString)
		end
	end
end

local function removeAttachmentSlots(weapon, attachment)
	local item = ScriptManager.instance:getItem(attachment)
	if item then
		local itemInstance = item:InstanceItem(nil)
		if itemInstance and instanceof(itemInstance, "WeaponPart") and itemInstance:getMountOn() then
			local mountOnOptions = {}

			for i=0, itemInstance:getMountOn():size()-1 do
				local weaponType = itemInstance:getMountOn():get(i)
				if(weaponType ~= weapon) then
					table.insert(mountOnOptions, weaponType)
				end
			end
			local mountOnString = table.concat(mountOnOptions, "; ")
			item:DoParam("MountOn = " .. mountOnString)
		end
	end
end

local function setValidWeaponsForAttachment(attachment, weaponsAllowed)
	local item = ScriptManager.instance:getItem(attachment)
	if item then
		local itemInstance = item:InstanceItem(nil)
		if itemInstance and instanceof(itemInstance, "WeaponPart") and itemInstance:getMountOn() then
			local mountOnString = table.concat(weaponsAllowed, "; ")
			item:DoParam("MountOn = " .. mountOnString)
		end
	end
end

local function addAttachments(weaponId, ...)
	local attachmentIds = {...}
	for _, attachment in ipairs(attachmentIds) do
		addAttachmentSlots(weaponId, attachment)
	end
end

local function removeAttachments(weaponId, ...)
	local attachmentIds = {...}
	for _, attachment in ipairs(attachmentIds) do
		removeAttachmentSlots(weaponId, attachment)
	end
end

-- Remove long range scope from m16A1
removeAttachments("Base.AssaultRifle", "Base.x8Scope")

-- Remove better scopes from the M14
removeAttachments("Base.AssaultRifle2", "Base.x8Scope", "Base.x4-x12Scope")

-- Change how makeshift suppressors work
WL_Utils.setItemProperties("Base.Silencer_PopBottle", {
	["Tooltip"] = "A crude homemade suppressor, can only be attached to equally crude weapons",
})

WL_Utils.setItemProperties("Base.ImprovisedSilencer", {
	["Tooltip"] = "A crude homemade suppressor, can only be attached to equally crude weapons",
})

local makeshiftWeapons = {
	"Base.HandcraftedRifle",
	"Base.HandcraftedLeverActionRifle",
	"GreysWLWeaponsFirearms.BoltActionScrapPistol",
}
setValidWeaponsForAttachment("Base.Silencer_PopBottle", makeshiftWeapons)
setValidWeaponsForAttachment("Base.ImprovisedSilencer", makeshiftWeapons)


-- Add attachments for Wasteland Custom Weapons

addAttachments("Base.HandcraftedRifle", "Base.Sling",
		"Base.Sling_Leather", "Base.Sling_Olive", "Base.Sling_Camo", "Base.IronSight")

addAttachments("Base.HandcraftedLeverActionRifle", "Base.RecoilPad", "Base.RedDot",
		"Base.IronSight")

addAttachments("GreysWLWeaponsFirearms.ScrapM4", "Base.IronSight",  "Base.x2Scope",  "Base.x4Scope",
		"Base.Laser", "Base.RedDot", "Base.Sling", "Base.Sling_Leather", "Base.Sling_Olive", "Base.Sling_Camo")

addAttachments("GreysWLWeaponsFirearms.ScrapThompson45v2", "Base.IronSight",
		"Base.45Silencer", "Base.Sling", "Base.Sling_Leather", "Base.Sling_Olive", "Base.Sling_Camo")

addAttachments("GreysWLWeaponsFirearms.ScrapSpas15",
		"Base.Sling", "Base.Sling_Leather", "Base.Sling_Olive", "Base.Sling_Camo")

addAttachments("Base.454CasullAuto_Silver", "Base.Laser", "Base.RedDot")
addAttachments("Base.454CasullAuto_Black", "Base.Laser", "Base.RedDot")
