---
--- zzWeaponModifier.lua
--- 12/07/2023
---

require "WL_Utils"

---@param weaponType string e.g. Base.AssaultRifle
---@param maxHit number to set max hit to
local function setWeaponMaxHitCount(weaponType, maxHit)
	local item = ScriptManager.instance:getItem(weaponType)
	if item then
		item:setMaxHitCount(maxHit)
	end
end

-- Long Blade

WL_Utils.setItemProperties("Base.Katana", {
	["MaxHitCount"] = 2,
	["EnduranceMod"] = 2,
	["MinDamage"] = 3,
	["MaxDamage"] = 4,
})

WL_Utils.setItemProperties("Base.Machete", {
	["AttachmentType"] = "Knife",
	["MaxHitCount"] = 1,
	["MinDamage"] = 1.4,
	["MaxDamage"] = 2.1,
	["Weight"] = 1.6,
})

if getActivatedMods():contains("RidingMower") then
	WL_Utils.setItemProperties("Base.MowerBladeWeaponSharp", {
		["MaxHitCount"] = 1,
		["MinDamage"] = 0.8,
		["MaxDamage"] = 1.3,
		["ConditionLowerChanceOneIn"] = 15,
	})
	WL_Utils.setItemProperties("Base.MowerBladeWeaponBlunt", {
		["MaxHitCount"] = 1,
		["MinDamage"] = 0.7,
		["MaxDamage"] = 1.1,
		["ConditionLowerChanceOneIn"] = 15,
	})
end

-- Axe

WL_Utils.setItemProperties("Base.Axe", {
	["MinDamage"] = 0.6,
	["MaxDamage"] = 1.6,
	["CriticalChance"] = 15,
	["ConditionLowerChanceOneIn"] = 25,
})

if getActivatedMods():contains("PertsPartyTiles") then
	WL_Utils.setItemProperties("Base.OverlookFireAxe", {
		["EnduranceMod"] = 1.25,
	})
end

WL_Utils.setItemProperties("Base.PickAxe", {
	["MaxHitCount"] = 2,
	["EnduranceMod"] = 1.25,
})

WL_Utils.setItemProperties("Base.HandAxe", {
	["MaxHitCount"] = 1,
	["ConditionLowerChanceOneIn"] = 25,
	["Weight"] = 1.8, -- From 2
})

WL_Utils.setItemProperties("Base.AxeStone", {
	["MaxHitCount"] = 2,
	["ConditionMax"] = 10,
	["EnduranceMod"] = 1.25,
})

-- Long Blunt

WL_Utils.setItemProperties("Base.BaseballBat", {
	["EnduranceMod"] = 1.2,
})

WL_Utils.setItemProperties("Base.BaseballBatNails", {
	["EnduranceMod"] = 1.2,
})

if getActivatedMods():contains("JordansExtraStuff") then
	WL_Utils.setItemProperties("Base.Metal_baseballbat", {
		["EnduranceMod"] = 1.2,
		["MaxHitCount"] = 2,
	})

	WL_Utils.setItemProperties("Base.Staff", {
		["Weight"] = 2,
		["EnduranceMod"] = 1.2,
		["ConditionLowerChanceOneIn"] = 20,
	})

	WL_Utils.setItemProperties("Base.SharpenedStaffBat", {
		["Weight"] = 2,
		["EnduranceMod"] = 1.2,
		["ConditionLowerChanceOneIn"] = 20,
	})

	WL_Utils.setItemProperties("Base.SharpenedStaffSpear", {
		["Weight"] = 2,
		["ConditionLowerChanceOneIn"] = 20,
	})
end

if getActivatedMods():contains("AmputationsRP") then
	WL_Utils.setItemProperties("AmputationsRP.AmpWalkingCane", {
		["ConditionMax"] = 5,
	})
end

WL_Utils.setItemProperties("Base.SnowShovel", {
	["EnduranceMod"] = 1.2,
	["MaxHitCount"] = 2,
})

WL_Utils.setItemProperties("Base.Shovel", {
	["EnduranceMod"] = 1.2,
	["MaxHitCount"] = 2,
})

WL_Utils.setItemProperties("Base.Shovel2", {
	["EnduranceMod"] = 1.2,
	["MaxHitCount"] = 2,
})

WL_Utils.setItemProperties("Base.CanoePadelX2", {
	["EnduranceMod"] = 1.2,
	["MaxHitCount"] = 2,
})

WL_Utils.setItemProperties("Base.GardenHoe", {
	["EnduranceMod"] = 1.1,
	["MaxHitCount"] = 2,
})

WL_Utils.setItemProperties("Base.GuitarElectricBassBlack", {
	["EnduranceMod"] = 1.1,
	["MaxHitCount"] = 2,
})

WL_Utils.setItemProperties("Base.GuitarElectricBassBlue", {
	["EnduranceMod"] = 1.1,
	["MaxHitCount"] = 2,
})

WL_Utils.setItemProperties("Base.GuitarElectricBassRed", {
	["EnduranceMod"] = 1.1,
	["MaxHitCount"] = 2,
})

WL_Utils.setItemProperties("Base.GuitarElectricBlack", {
	["EnduranceMod"] = 1.1,
	["MaxHitCount"] = 2,
})

WL_Utils.setItemProperties("Base.GuitarElectricBlue", {
	["EnduranceMod"] = 1.1,
	["MaxHitCount"] = 2,
})

WL_Utils.setItemProperties("Base.GuitarElectricRed", {
	["EnduranceMod"] = 1.1,
	["MaxHitCount"] = 2,
})

WL_Utils.setItemProperties("Base.GuitarAcoustic", {
	["MaxHitCount"] = 2,
})

WL_Utils.setItemProperties("Base.HockeyStick", {
	["MaxHitCount"] = 2,
})

WL_Utils.setItemProperties("Base.IceHockeyStick", {
	["MaxHitCount"] = 2,
})

WL_Utils.setItemProperties("Base.LaCrosseStick", {
	["MaxHitCount"] = 2,
})

WL_Utils.setItemProperties("Base.CanoePadel", {
	["EnduranceMod"] = 1.1,
	["MaxHitCount"] = 2,
})

WL_Utils.setItemProperties("Base.Crowbar", {
	["MaxHitCount"] = 2,
	["EnduranceMod"] = 1.1,
})

WL_Utils.setItemProperties("Base.PickAxeHandleSpiked", {
	["EnduranceMod"] = 1.2,
})

WL_Utils.setItemProperties("Base.Golfclub", {
	["MaxHitCount"] = 2,
})

WL_Utils.setItemProperties("Base.Banjo", {
	["MaxHitCount"] = 2,
})

WL_Utils.setItemProperties("Base.Broom", {
	["MaxHitCount"] = 2,
})

WL_Utils.setItemProperties("Base.LeafRake", {
	["MaxHitCount"] = 2,
})

WL_Utils.setItemProperties("Base.Rake", {
	["MaxHitCount"] = 2,
})

-- Spears

WL_Utils.setItemProperties("Base.SpearHuntingKnife", {
	["EnduranceMod"] = 1.7,
})

WL_Utils.setItemProperties("Base.SpearMachete", {
	["EnduranceMod"] = 1.3,
})

if getActivatedMods():contains("sapphcooking") then
	WL_Utils.setItemProperties("Base.SpearSpork", {
		["EnduranceMod"] = 1.7,
	})

	WL_Utils.setItemProperties("Base.SpearChefKnife3", {
		["EnduranceMod"] = 1.7,
	})

	WL_Utils.setItemProperties("Base.SpearChefKnife2", {
		["EnduranceMod"] = 1.7,
	})

	WL_Utils.setItemProperties("Base.SpearChefKnife1", {
		["EnduranceMod"] = 1.7,
	})
end

WL_Utils.setItemProperties("Base.SpearBreadKnife", {
	["EnduranceMod"] = 1.7,
})

WL_Utils.setItemProperties("Base.SpearButterKnife", {
	["EnduranceMod"] = 1.7,
})

WL_Utils.setItemProperties("Base.SpearFork", {
	["EnduranceMod"] = 1.7,
})

WL_Utils.setItemProperties("Base.SpearLetterOpener", {
	["EnduranceMod"] = 1.7,
})

WL_Utils.setItemProperties("Base.SpearScalpel", {
	["EnduranceMod"] = 1.7,
})

WL_Utils.setItemProperties("Base.SpearSpoon", {
	["EnduranceMod"] = 1.7,
})

WL_Utils.setItemProperties("Base.SpearScissors", {
	["EnduranceMod"] = 1.7,
})

WL_Utils.setItemProperties("Base.SpearHandFork", {
	["EnduranceMod"] = 1.7,
})

WL_Utils.setItemProperties("Base.SpearScrewdriver", {
	["EnduranceMod"] = 1.7,
})

WL_Utils.setItemProperties("Base.SpearIcePick", {
	["EnduranceMod"] = 1.7,
})

WL_Utils.setItemProperties("Base.SpearKnife", {
	["EnduranceMod"] = 1.7,
})

WL_Utils.setItemProperties("Base.SpearCrafted", {
	["EnduranceMod"] = 1.7,
})

WL_Utils.setItemProperties("Base.GardenFork", {
	["EnduranceMod"] = 1.7,
})

WL_Utils.setItemProperties("Base.WoodenLance", {
	["EnduranceMod"] = 1.7,
})

-- Short Blunt

WL_Utils.setItemProperties("Base.Nightstick", {
	["MinDamage"] = 0.8,   -- From 0.6
	["MaxHitCount"] = 1,
	["ConditionLowerChanceOneIn"] = 30,  -- From 20
})

WL_Utils.setItemProperties("Base.LeadPipe", {
	["MaxHitCount"] = 1,
})

WL_Utils.setItemProperties("Base.MetalBar", {
	["MaxHitCount"] = 1,
})

WL_Utils.setItemProperties("Base.MetalPipe", {
	["MaxHitCount"] = 1,
})

WL_Utils.setItemProperties("Base.PipeWrench", {
	["MaxHitCount"] = 1,
})

WL_Utils.setItemProperties("Base.Wrench", {
	["MaxHitCount"] = 1,
})

WL_Utils.setItemProperties("Base.TableLeg", {
	["MaxHitCount"] = 1,
})

WL_Utils.setItemProperties("Base.Saucepan", {
	["MaxHitCount"] = 1,
})

-- Short Blade

WL_Utils.setItemProperties("Base.HuntingKnife", {
	["ConditionMax"] = 30,
})

WL_Utils.setItemProperties("Base.KitchenKnife", {
	["ConditionMax"] = 15,
})

WL_Utils.setItemProperties("Base.MeatCleaver", {
	["ConditionMax"] = 30,
	["MaxHitCount"] = 1,
})

WL_Utils.setItemProperties("Base.HandScythe", {
	["ConditionMax"] = 10,
	["ConditionLowerChanceOneIn"] = 10,
	["MaxHitCount"] = 1,
})

if getActivatedMods():contains("firearmmod_WL") then

	-- Shotguns
	setWeaponMaxHitCount("Base.DoubleBarrelShotgunSawnoff", 5)

	-- Lever Action Rifles

	WL_Utils.setItemProperties("Base.Winchester94", {
		["RecoilDelay"] = 35, -- From 0
		["RackAfterShoot"] = "FALSE",
		["StopPower"] = 10, -- From N/A
	})

	WL_Utils.setItemProperties("Base.Winchester73", {
		["RecoilDelay"] = 35, -- From 0
		["RackAfterShoot"] = "FALSE",
		["StopPower"] = 10, -- From N/A
	})

	WL_Utils.setItemProperties("Base.Rossi92", {
		["RecoilDelay"] = 35, -- From 0
		["RackAfterShoot"] = "FALSE",
		["StopPower"] = 10, -- From N/A
	})

	-- Shotguns
	WL_Utils.setItemProperties("Base.SPAS12", {
		["RecoilDelay"] = 40, -- From 60
		["ReloadTime"] = 35, -- From 25
		["FireModePossibilities"] = "Single",
		["RackAfterShoot"] = "FALSE",
		["StopPower"] = 7, -- From 30
	})

	-- Bolt Action Rifles

	-- Range: 55 with 12x scope
	WL_Utils.setItemProperties("Base.HuntingRifle", {
		["MaxRange"] = 15, -- From 10
		["AimingPerkRangeModifier"] = 2, -- From 3
		["HitChance"] = 60,     -- From 25
		["AimingPerkHitChanceModifier"] = 10, -- From 15
		["MinDamage"] = 3,
		["MaxDamage"] = 3.5,
	})

	-- Remington Model 700
	WL_Utils.setItemProperties("Base.VarmintRifle", {
		["MinDamage"] = 3,
		["MaxDamage"] = 3.5,
	})

	WL_Utils.setItemProperties("Base.HuntingRifle_Sawn", {
		["MinDamage"] = 3,
		["MaxDamage"] = 3.5,
	})

	-- Range: 62 with 12x scope
	WL_Utils.setItemProperties("Base.M24Rifle", {
		["AimingPerkRangeModifier"] = 3, -- From 2
		["MaxRange"] = 12, -- From 14
		["MinDamage"] = 3,
		["MaxDamage"] = 3.5,
	})

	-- Changes to .22 LR weapons to make them quieter and friendly to new shooters but lower damage

	WL_Utils.setItemProperties("Base.ColtAce", {
		["MinDamage"] = 0.45,  -- From 0.75
		["MaxDamage"] = 1.4,  -- From 2.5
		["RecoilDelay"] = 20,  -- From 25
		["AimingTime"] = 5,    -- From 20
		["SoundRadius"] = 35,  -- From 70
		["CritDmgMultiplier"] = 2, -- From 4
		["AimingPerkCritModifier"] = 5, -- From 10
		["HitChance"] = 60,     -- From 40
		["AimingPerkHitChanceModifier"] = 7, -- From 10
		["Tooltip"] = "A .22 version of the M1911 designed for sub-caliber training",
		["SwingSound"] = "Gun22Shoot",
	})

	WL_Utils.setItemProperties("Base.Rugerm7722", {
		["AimingPerkRangeModifier"] = 2, -- From 3
		["RecoilDelay"] = 85,  -- From 100
		["MinDamage"] = 2.5,
		["MaxDamage"] = 3,  -- From 2
		["AimingPerkCritModifier"] = 8, -- From 15
		["HitChance"] = 60,     -- From 25
		["AimingPerkHitChanceModifier"] = 10, -- From 15
		["SoundRadius"] = 45,  -- From 60
		["SwingSound"] = "Gun22Shoot",
		["PiercingBullets"] = "FALSE",  -- From TRUE
		["Weight"] = 3,  -- From 4
	})

	WL_Utils.setItemProperties("Base.ColtSingleAction22", {
		["RecoilDelay"] = 20,  -- From 5
		["CritDmgMultiplier"] = 2, -- From 4
		["MaxDamage"] = 1.4,  -- From 2
		["HitChance"] = 50,     -- From 35
		["AimingPerkHitChanceModifier"] = 10, -- From 12
		["SoundRadius"] = 35,  -- From 55
		["CritDmgMultiplier"] = 3, -- From 4
		["SwingSound"] = "Gun22Shoot",
	})

	-- SMGs

	-- Range: 28 at 10 with x4 Scope
	WL_Utils.setItemProperties("Base.MP5", {
		["AimingPerkRangeModifier"] = 0.7,  -- From 3
		["AimingTime"] = 20, -- From 25
	})

	-- Range: 24 at 10 with x2 Scope
	WL_Utils.setItemProperties("Base.UZI", {
		["AimingPerkRangeModifier"] = 1,  -- From 2
	})

	-- Range: No Scopes. 20 at 10
	WL_Utils.setItemProperties("Base.Mac10", {
		["MaxRange"] = 10, -- From 7
		["AimingPerkRangeModifier"] = 1,  -- From 2
		["AttachmentType"] = "Holster",  -- From RIFLE
		["HitChance"] = 20,    -- From 30
		["AimingPerkHitChanceModifier"] = 4, -- From 5
		["AimingPerkCritModifier"] = 8, -- From 10
	})

	WL_Utils.setItemProperties("Base.Mac10Mag", {
		["MaxAmmo"] = 15, -- From 30
	})

	WL_Utils.setItemProperties("Base.UZIMag", {
		["MaxAmmo"] = 30, -- From 20
	})

	-- Semi-auto rifles

	-- Range: 48 with x4 scope
	-- Accuracy: 70% at aim 5, 110% at aim 10
	WL_Utils.setItemProperties("Base.FN_FAL", {
		["AimingPerkHitChanceModifier"] = 8, -- From 5
		["AimingPerkRangeModifier"] = 2.2,  -- From 3
		["MaxRange"] = 13, -- From 10
		["Weight"] = 3.5, -- From 4
		["AimingPerkCritModifier"] = 1, -- From 0
		["MinDamage"] = 1.3,  -- From 1
	})

	-- M14
	-- Range: 44 with x4 scope
	-- Accuracy: 75% at aim 5, 100% at aim 10
	WL_Utils.setItemProperties("Base.AssaultRifle2", {
		["AimingPerkRangeModifier"] = 2,  -- From 3
		["MaxRange"] = 11, -- From 10
		["StopPower"] = 10, -- From 30
	})

	-- Range: no scopes. 23.5 at 5 aim, 33 at 10
	-- Hit %: 80% at aim 5, 110% at aim 10
	WL_Utils.setItemProperties("Base.AR15", {
		["AimingPerkRangeModifier"] = 1.7,  -- From 3
		["MaxRange"] = 16, -- From 11
		["HitChance"] = 50,    -- From 40
		["AimingPerkHitChanceModifier"] = 6, -- From 7
		["CriticalChance"] = 40, -- From 30
		["Weight"] = 3, -- From 4
	})

	-- Range: no scopes. 35 at aim 10
	WL_Utils.setItemProperties("Base.SKS", {
		["MaxRange"] = 13, -- From 10
		["StopPower"] = 18, -- From 30
		["AimingPerkRangeModifier"] = 2.2,  -- From 3
	})

	-- Range: no scopes. 37 at aim 10
	WL_Utils.setItemProperties("Base.M1Garand", {
		["MinDamage"] = 1.85,  -- From 1
		["MaxRange"] = 14, -- From 10
		["AimingPerkRangeModifier"] = 2.3,  -- From 3
		["StopPower"] = 35, -- From 30
		["AimingPerkCritModifier"] = 4,  -- From 0
		["CritDmgMultiplier"] = 3,  -- From Default
	})

	-- Fully Automatic Rifles

	-- Range: 40 with x4 scope
	WL_Utils.setItemProperties("Base.AssaultRifle", {
		["AimingPerkRangeModifier"] = 1.6,  -- From 3
		["Weight"] = 3.6, -- From 4
	})

	-- Range: 40 with x4 scope
	WL_Utils.setItemProperties("Base.M16A2", {
		["AimingPerkRangeModifier"] = 1.6,  -- From 3
	})

	-- Colt Commando
	-- Range: 37 with x4 scope
	WL_Utils.setItemProperties("Base.M733", {
		["AimingPerkRangeModifier"] = 1.5,  -- From 3
		["Weight"] = 3, -- From 4
		["AimingTime"] = 15, -- From 20
		["ReloadTime"] = 25, -- From 30
		["AimingPerkCritModifier"] = 2, -- From 0
	})

	-- Range: no scopes.  22.5 at aim 5, 35 at 10,
	-- Hit %: 60% at aim 5, 100% at aim 10
	WL_Utils.setItemProperties("Base.AK47", {
		["MinDamage"] = 1.5,  -- From 1
		["MaxDamage"] = 3.5,  -- From 3
		["AimingPerkRangeModifier"] = 2.5,  -- From 3
		["MaxRange"] = 10, -- From 11
		["AimingPerkHitChanceModifier"] = 8, -- From 7
		["StopPower"] = 8, -- From 7
		["AimingPerkCritModifier"] = 1, -- From 0
		["Weight"] = 4.5, -- From 4
	})
end