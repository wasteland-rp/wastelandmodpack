require "TimedActions/FollowPlayerAction"
require "TimedActions/ISTimedActionQueue"

local function FollowPlayer(player, otherPlayer)
    ISTimedActionQueue.add(FollowPlayerAction:new(player, otherPlayer))
end

local function AddFollowPlayerOption(playerIdx, context, worldObjects)
    local player = getPlayer(playerIdx)
    local square
    for _, v in ipairs(worldObjects) do
        square = v:getSquare()
        if square then
            break
        end
    end
    if not square then
        return
    end
    for x=-1,1 do
    for y=-1,1 do
        local sq = getCell():getGridSquare(square:getX() + x, square:getY() + y, square:getZ())
        if sq then
            local movingObjects = sq:getMovingObjects()
            for i = 0, movingObjects:size() - 1 do
                local object = movingObjects:get(i)
                if instanceof(object, "IsoPlayer") and player ~= object then
                    context:addOption("Follow " .. object:getDescriptor():getForename(), player, FollowPlayer, object)
                end
            end
        end
    end
    end
end

Events.OnFillWorldObjectContextMenu.Add(AddFollowPlayerOption)