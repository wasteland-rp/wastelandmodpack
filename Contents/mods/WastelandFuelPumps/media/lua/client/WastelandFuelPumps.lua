local function isWorldFuelPump(item)
    return item and instanceof(item, "IsoObject") and item:getPipedFuelAmount() > 0
    -- todo maybe add in the tiles
end

local original_ISWorldObjectContextMenu_doFillFuelMenu = ISWorldObjectContextMenu.doFillFuelMenu
function ISWorldObjectContextMenu.doFillFuelMenu(fuelSource, player, context)
    if isWorldFuelPump(fuelSource) then
        fuelSource:setPipedFuelAmount(0)
        fuelSource:transmitCompleteItemToServer()
    end
    original_ISWorldObjectContextMenu_doFillFuelMenu(fuelSource, player, context)
end

local original_ISVehiclePartMenu_getNearbyFuelPump = ISVehiclePartMenu.getNearbyFuelPump
function ISVehiclePartMenu.getNearbyFuelPump(vehicle)
    local pump = original_ISVehiclePartMenu_getNearbyFuelPump(vehicle)
    if isWorldFuelPump(pump) then
        pump:setPipedFuelAmount(0)
        pump:transmitCompleteItemToServer()
    end
    return pump
end