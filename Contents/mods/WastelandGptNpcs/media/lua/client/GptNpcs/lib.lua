
WGNClient = WGNClient or {}
WGNClient._currentNPC = nil
WGNClient._globalModData = {}

function WGNClient.SendCommand(command, args)
    if args == nil then
        args = {}
    end
    sendClientCommand(getPlayer(), "WGN", command, args)
end

function WGNClient.OnReceiveGlobalModData(key, data)
    if key == "WGN" then
        WGNClient._globalModData = data
        if not WGNClient._globalModData.PendingPlayerNPCs then WGNClient._globalModData.PendingPlayerNPCs = {} end
        if not WGNClient._globalModData.PendingWorldNPCs then WGNClient._globalModData.PendingWorldNPCs = {} end
        if not WGNClient._globalModData.NPCTypes then WGNClient._globalModData.NPCTypes = {} end
    end
end

function WGNClient.LoreMessage(message)
    WL_Utils.addToChat(message, {
        color = "0.3,1.0,0.3",
        chatId = 1,
    })
end

function WGNClient.OOCMessage(message)
    WL_Utils.addToChat(message, {
        color = "0.8,0.8,0.8",
        chatId = WRC.OocTabId,
    })
end

function WGNClient.GetAvailableNPC(username, frequency)
    if WGNClient._globalModData.PendingPlayerNPCs
       and WGNClient._globalModData.PendingPlayerNPCs[username] then
        local npc = WGNClient._globalModData.PendingPlayerNPCs[username]
        if npc.frequency == frequency then
            return npc
        end
    end

    if WGNClient._globalModData.PendingWorldNPCs and WGNClient._globalModData.PendingWorldNPCs[frequency] then
        return WGNClient._globalModData.PendingWorldNPCs[frequency]
    end
end

function WGNClient.SetActiveNPC(name, frequency)
    WGNClient._currentNPC = {
        name = name,
        frequency = frequency
    }
end

function WGNClient.GetCurrentNPCFrequency()
    return WGNClient._currentNPC and WGNClient._currentNPC.frequency
end

function WGNClient.GetCurrentNPCName()
    return WGNClient._currentNPC and WGNClient._currentNPC.name
end

function WGNClient.ClearCurrentNPC()
    WGNClient._currentNPC = nil
end

function WGNClient.GetNPCTypes()
    return WGNClient._globalModData.NPCTypes
end