require "GptNpcs/lib"
require "Chat/WRC"
require "UserData"
require "PlayerReady"
require "WL_Utils"

local function OnChatMessage(parsedMessage)
    local currentFrequency = WGNClient.GetCurrentNPCFrequency()
    if not currentFrequency then
        return
    end

    if currentFrequency then
        local radios = WRU_Utils.getPlayerRadios(getPlayer(), true, true)
        local hasRadioOn = false
        for _, radio in ipairs(radios) do
            if WRU_Utils.getRadioFrequency(radio) == currentFrequency then
                hasRadioOn = true
                break
            end
        end
        if not hasRadioOn then
            return
        end
    end

    if (parsedMessage.chatModifier == nil or parsedMessage.chatModifier == "me") then
        local text = "/me"
        for _, part in ipairs(parsedMessage.parts) do
            if part.type == "emote" then
                text = text .. " " .. part.text
            elseif part.type == "text" then
                text = text .. " \"" .. part.text .. "\""
            end
        end
        WGNClient.SendCommand("ChatMessage", {text})
    end
end

local Commands = {}
function Commands.ChatMessage(args)
    local message = args[1]
    local frequency = args[2]

    local radios = WRU_Utils.getPlayerRadios(getPlayer(), true, false)
    local hasRadioOn = false
    for _, radio in ipairs(radios) do
        if WRU_Utils.getRadioFrequency(radio) == frequency then
            hasRadioOn = true
            break
        end
    end
    if not hasRadioOn then
        return
    end

    WRC.Handlers.AddLineInChat(WL_FakeMessage:new(message, {
        radioChannel = frequency,
    }), 1)
end

function Commands.NewConversation(args)
    local npc = args[1]
    local frequency = args[2]
    WGNClient.SetActiveNPC(npc, frequency)
    WGNClient.OOCMessage("Conversation with " .. npc .. " has been initialized.")
end

function Commands.EndConversation()
    local npcName = WGNClient.GetCurrentNPCName()
    WGNClient.ClearCurrentNPC()
    WGNClient.LoreMessage("The radio goes quiet.")
    WGNClient.OOCMessage("Conversation with " .. (npcName or "an npc") .. " has ended.")
end

function Commands.AlertNPCInitializing()
    WGNClient.OOCMessage("NPC initializing... Please wait...")
end

function Commands.AlertNPCAvailable(args)
    local type = args[1]
    local frequency = args[2]

    local radiosOn = WRU_Utils.getPlayerRadios(getPlayer(), true, false)
    local radiosOverall = WRU_Utils.getPlayerRadios(getPlayer(), false, false)
    if #radiosOn > 0 then
        WGNClient.LoreMessage("Static crackles on your radio, indicating someone might be transmitting on a different frequency.")
    elseif #radiosOverall > 0 then
        WGNClient.LoreMessage("Your walkie-talkie is off; you might be missing important messages.")
    else
        WGNClient.LoreMessage("You realize you've lost your walkie-talkie. You'll need to find a new one.")
    end
    WGNClient.OOCMessage("There is an NPC available on frequency " .. (frequency/1000) .. ".")
    WGNClient.OOCMessage("To engage, make sure your walkie is turned on and tuned in to that frequency.")
    WGNClient.OOCMessage("You must use a walkie. In-world radios will not work.")
end

function OnServerCommand(module, command, args)
    if module ~= "WGN" then return end

    if Commands[command] then
        Commands[command](args)
    else
        print("WGN:OnServerCommand Unknown " .. command)
    end
end

table.insert(WRC.CustomChatCallbacks, OnChatMessage)
Events.OnServerCommand.Add(OnServerCommand)
Events.OnReceiveGlobalModData.Add(WGNClient.OnReceiveGlobalModData)
Events.OnConnected.Add(function () ModData.request("WGN") end)

-- Disabled for S5
-- WL_PlayerReady.Add(function()
--     WL_UserData.Fetch("WGN", function(data)
--         local player = getPlayer()
--         if not data.triggeredNewPlayerNpc then
--             if player:getHoursSurvived() < 1 then
--                 local frequency = ZombRand(250, 1000) * 200
--                 WGNClient.SendCommand("AddPlayerNPC", {player:getUsername(), frequency, "NewPlayer", "random"})
--             end
--             WL_UserData.Append("WGN", {triggeredNewPlayerNpc = true})
--         end
--     end)
-- end)