
function ISRadioAction:checkForNPC()
    if WGNClient.GetCurrentNPCFrequency() then return end

    if  not self.device
        or not instanceof(self.device, "InventoryItem")
        or self.device:getType():sub(1, 12) ~= "WalkieTalkie"           -- Is not a walkie
        or not self.deviceData:getIsTurnedOn()      -- Is not turned on
        or self.deviceData:getPower() <= 0          -- No power
        or self.deviceData:getDeviceVolume() <= 0   -- Volume is muted
    then return end

    local channel = self.deviceData:getChannel()
    if WGNClient.GetAvailableNPC(self.character:getUsername(), channel) then
        WGNClient.SendCommand("InitializeNPC", {channel})
        return
    end
end

local original_ISRadioAction_perform = ISRadioAction.perform
function ISRadioAction:perform()
    original_ISRadioAction_perform(self)
    if not self.device or not self.deviceData then return end
    if self.mode == "ToggleOnOff"
       or self.mode == "SetChannel"
       or self.mode == "MuteMicrophone"
    then
        self:checkForNPC()
    end
end