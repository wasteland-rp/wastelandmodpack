local function Prompt(message, callback)
    local scale = getTextManager():MeasureStringY(UIFont.Small, "XXX") / 12

    local width = 200 * scale
    local height = 130 * scale
    local x = (getCore():getScreenWidth() / 2) - (width / 2)
    local y = (getCore():getScreenHeight() / 2) - (height / 2)
    local modal = ISTextBox:new(x, y, width, height, message, "", nil, function (_, button)
        if callback and button.internal == "OK" then
            callback(button.parent.entry:getText())
        end
    end, nil)
    modal:initialise()
    modal:addToUIManager()
end

local function PromptFrequency(message, callback)
    Prompt(message, function(text)
        if text == "" then
            frequency = (ZombRand(250, 1000) * 2) / 10
        else
            frequency = tonumber(text)
        end
        if not frequency then
            WGNClient.OOCMessage("Frequency must be a number")
            return
        end
        frequency = frequency*1000
        if math.floor(frequency/200) ~= frequency/200 then
            WGNClient.OOCMessage("Frequency must be an even number with 1 decimal place")
            return
        end
        if frequency < 10000 or frequency > 600000 then
            WGNClient.OOCMessage("Frequency must be between 10.0 and 600.0")
            return
        end
        callback(frequency)
    end)
end

local function PromptUses(message, callback)
    Prompt(message, function(text)
        local uses = tonumber(text)
        if not uses then
            callback(0)
            return
        end
        callback(uses)
    end)
end

local function AddWorldNPC(type, specific)
    PromptFrequency("Frequency? (Blank for Random)", function(frequency)
        PromptUses("Uses? (Blank for unlimited)", function(uses)
            WGNClient.SendCommand("AddWorldNPC", {frequency, uses, type, specific})
            WGNClient.OOCMessage("World NPC added to frequency " .. (frequency/1000) .. ".")
        end)
    end)
end

local function AddPlayerNPC(username, type, specific)
    PromptFrequency("Frequency? (Blank for Random)", function(frequency)
        WGNClient.SendCommand("AddPlayerNPC", {username, frequency, type, specific})
        WGNClient.OOCMessage("Player NPC added to frequency " .. (frequency/1000) .. " for player " .. username .. ".")
    end)
end

local function RemovePlayerNPC(username)
    WGNClient.SendCommand("RemovePlayerNPC", {username})
    WGNClient.OOCMessage("Player NPC removed for player " .. username .. ".")
end

local function RemoveWorldNPC(frequency)
    WGNClient.SendCommand("RemoveWorldNPC", {frequency})
    WGNClient.OOCMessage("World NPC removed from frequency " .. (frequency/1000) .. ".")
end

local function EndConversation()
    WGNClient.SendCommand("EndConversation")
    WGNClient.ClearCurrentNPC()
end

local function worldMenu(playerIdx, context)
    if WGNClient.GetCurrentNPCName() then
        context:addOption("Force End NPC Interaction", nil, EndConversation)
    end

    if not isAdmin() then
        return
    end

    local wlContext = WL_ContextMenuUtils.getOrCreateSubMenu(context, "WL Admin")
    local npcContext = WL_ContextMenuUtils.getOrCreateSubMenu(wlContext, "NPCs")

    local worldNPCContext = WL_ContextMenuUtils.getOrCreateSubMenu(npcContext, "World")
    local playerNPCContext = WL_ContextMenuUtils.getOrCreateSubMenu(npcContext, "Player")

    local allPlayers = getOnlinePlayers()
    local playerNames = {}
    for i=0, allPlayers:size()-1 do
        local p = allPlayers:get(i)
        local name = p:getUsername()
        table.insert(playerNames, name)
    end
    table.sort(playerNames)

    local removeWorldNPCContext = WL_ContextMenuUtils.getOrCreateSubMenu(worldNPCContext, "Remove")
    for frequency, npcData in pairs(WGNClient._globalModData.PendingWorldNPCs) do
        local fs = frequency/1000
        local title = fs .. " [" .. npcData.uses .. "] " .. npcData.type .. " - " .. (npcData.specific or "random")
        removeWorldNPCContext:addOption(title, frequency, RemoveWorldNPC)
    end
    for type, specifics in pairs(WGNClient._globalModData.NPCTypes) do
        local c = WL_ContextMenuUtils.getOrCreateSubMenu(worldNPCContext, type)
        for _, specific in ipairs(specifics) do
            c:addOption(specific, type, AddWorldNPC, specific)
        end
    end

    local removePlayerNPCContext = WL_ContextMenuUtils.getOrCreateSubMenu(playerNPCContext, "Remove")
    for username, data in pairs(WGNClient._globalModData.PendingPlayerNPCs) do
        local fs = data.frequency/1000
        local title = username .. " - " .. fs .. " - " .. data.type .. " - " .. (data.specific or "random")
        removePlayerNPCContext:addOption(title, username, RemovePlayerNPC)
    end
    for type, specifics in pairs(WGNClient._globalModData.NPCTypes) do
        local c = WL_ContextMenuUtils.getOrCreateSubMenu(playerNPCContext, type)
        for _, specific in ipairs(specifics) do
            local d = WL_ContextMenuUtils.getOrCreateSubMenu(c, specific)
            for _, name in ipairs(playerNames) do
                d:addOption(name, name, AddPlayerNPC, type, specific)
            end
        end
    end
end


Events.OnFillWorldObjectContextMenu.Add(worldMenu)