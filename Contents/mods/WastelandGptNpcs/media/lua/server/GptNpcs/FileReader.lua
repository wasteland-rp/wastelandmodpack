if isClient() then return end

local FileReader = {}

function FileReader:new(filePath)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o:Init(filePath)
    return o
end

function FileReader:Init(filePath)
    self.filePath = filePath
end

function FileReader:ReadLines()
    local file = getFileReader(self.filePath, false)
    if not file then return {} end
    local lines = {}
    local line = file:readLine()
    while line ~= nil do
        table.insert(lines, line)
        line = file:readLine()
    end
    file:close()
    self:ClearFile()
    return lines
end

function FileReader:ClearFile()
    local file = getFileWriter(self.filePath, false, false)
    file:write("")
    file:close()
end

return FileReader