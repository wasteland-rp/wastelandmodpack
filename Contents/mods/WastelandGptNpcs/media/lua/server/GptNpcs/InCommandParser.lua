if isClient() then return end

local InCommands = require "GptNpcs/InCommands"

local LineReader = {}
function LineReader:new(lines)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.currentLine = 1
    o.lines = lines
    o.lastLine = #lines
    return o
end

function LineReader:ReadString()
    if self.currentLine > self.lastLine then
        return nil
    end
    local line = self.lines[self.currentLine]
    self.currentLine = self.currentLine + 1
    return line
end

function LineReader:ReadNumber()
    if self.currentLine > self.lastLine then
        return nil
    end
    local line = self.lines[self.currentLine]
    self.currentLine = self.currentLine + 1
    return tonumber(line)
end

local InCommandParser = {}

function InCommandParser:new(lines)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o:Init(lines)
    return o
end

function InCommandParser:Init(lines)
    self.commands = {}
    self.lines = lines
    self.currentLine = 1
end

function InCommandParser:Parse()
    local reader = LineReader:new(self.lines)
    while reader.currentLine <= reader.lastLine do
        local command = reader:ReadString()
        if InCommands[command] then
            local cmd = InCommands[command]:new()
            cmd:Parse(reader)
            cmd:Execute()
        else
            print("WGN:InCommandParser:Unknown command: " .. command)
        end
    end
end

function InCommandParser:parseChatMessage()
    local username = self.lines[self.currentLine + 1]
    local message = self.lines[self.currentLine + 2]
    table.insert(self.commands, InCommands.ChatMessage:new(username, message))
    self.currentLine = self.currentLine + 2
end

function InCommandParser:parseNewConversation()
    local username = self.lines[self.currentLine + 1]
    local npcName = self.lines[self.currentLine + 2]
    table.insert(self.commands, InCommands.NewConversation:new(username, npcName))
    self.currentLine = self.currentLine + 2
end

function InCommandParser:parseEndConversation()
    local username = self.lines[self.currentLine + 1]
    table.insert(self.commands, InCommands.EndConversation:new(username))
    self.currentLine = self.currentLine + 1
end

function InCommandParser:parseNPCTypeSpecifics()
    local type = self.lines[self.currentLine + 1]
    local count = tonumber(self.lines[self.currentLine + 2])
    local specifics = {}
    for i = 1, count do
        table.insert(specifics, self.lines[self.currentLine + 2 + i])
    end
    table.insert(self.commands, InCommands.NPCTypes:new(type, specifics))
    self.currentLine = self.currentLine + 2 + count
end

function InCommandParser:ExecuteCommands()
    for _, command in ipairs(self.commands) do
        command:Execute()
    end
end

return InCommandParser