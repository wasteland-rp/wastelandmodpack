if isClient() then return end

local InCommand = WLBaseObject:derive("InCommand")

function InCommand:Execute()
    print(self._type .. " is not implemented")
end

local ChatMessage = InCommand:derive("ChatMessage")

function ChatMessage:Parse(reader)
    self.username = reader:ReadString()
    self.message = reader:ReadString()
    self.npc = WGNServer.GetRunningNPC(self.username)
end

function ChatMessage:Execute()
    if not self.npc then return end
    WGNServer.SendGlobal("ChatMessage", {"[UN:NPC " .. self.npc.name .. "]" .. self.message, self.npc.frequency})
end

local NewConversation = InCommand:derive("NewConversation")

function NewConversation:Parse(reader)
    self.username = reader:ReadString()
    self.npcName = reader:ReadString()
end

function NewConversation:Execute()
    WGNServer.SetRunningNPCName(self.username, self.npcName)
    local frequency = WGNServer.GetRunningNPC(self.username).frequency
    WGNServer.SendUser(self.username, "NewConversation", {self.npcName, frequency})
end

local EndConversation = InCommand:derive("EndConversation")

function EndConversation:Parse(reader)
    self.username = reader:ReadString()
end

function EndConversation:Execute()
    WGNServer.ClearRunningNPC(self.username)
    WGNServer.SendUser(self.username, "EndConversation")
end

local Initialized = InCommand:derive("Initialized")

function Initialized:Parse(reader)
    self.types = {}
    local typesCount = reader:ReadNumber()
    for i = 1, typesCount do
        local type = reader:ReadString()
        self.types[type] = {}
        local specificCount = reader:ReadNumber()
        for j = 1, specificCount do
            local specific = reader:ReadString()
            table.insert(self.types[type], specific)
        end
    end
end

function Initialized:Execute()
    WGNServer.SetNPCTypes(self.types)
    WGNServer.ClearRunningNPCs()
end

return {
    ChatMessage = ChatMessage,
    NewConversation = NewConversation,
    EndConversation = EndConversation,
    Initialized = Initialized
}
