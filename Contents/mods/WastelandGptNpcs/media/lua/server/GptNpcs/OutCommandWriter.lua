if isClient() then return end

local OutCommandWriter = {}

--- OutCommandFormatter
--- @param directory string the directory to write the files
function OutCommandWriter:new(directory)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o:Init(directory)
    return o
end

function OutCommandWriter:Init(directory)
    self.directory = directory
    self.fileIndex = 0
end

--- WriteCommand
--- @param command OutCommand the command to write
function OutCommandWriter:WriteCommand(command)
    local lines = command:GetLines()
    local file = getFileWriter(self.directory .. "/" .. self.fileIndex .. ".txt", true, false)
    for _, v in ipairs(lines) do
        file:writeln(v)
    end
    file:close()
    self.fileIndex = self.fileIndex + 1
end

return OutCommandWriter