if isClient() then return end

--- @class OutCommand
local OutCommand = WLBaseObject:derive("OutCommand")
function OutCommand:GetLines()
    print(self._type .. " is not implemented")
    return {}
end

local NewConversation = OutCommand:derive("NewConversation")

--- NewConversation
--- @param player IsoPlayer The player
--- @param type string The type of conversation
--- @param specific string The specific npc to use
function NewConversation:new(player, type, specific)
    local o = self:super()
    o.player = player
    o.type = type
    o.specific = specific
    return o
end

function NewConversation:GetLines()
    local lines = {}
    table.insert(lines, "NewConversation")
    table.insert(lines, self.player:getUsername())
    table.insert(lines, self.player:getX() .. "," .. self.player:getY() .. "," .. self.player:getZ())
    table.insert(lines, self.type)
    table.insert(lines, self.specific)
    return lines
end

local ChatMessage = OutCommand:derive("ChatMessage")

--- OutChatMessage
--- @param fromPlayer string The player Username
--- @param message string the message
function ChatMessage:new(fromPlayer, message)
    local o = self:super()
    o.fromPlayer = fromPlayer
    o.message = message
    return o
end

function ChatMessage:GetLines()
    local lines = {}
    table.insert(lines, "ChatMessage")
    table.insert(lines, self.fromPlayer)
    table.insert(lines, self.message)
    return lines
end

local EndConversation = OutCommand:derive("EndConversation")

--- EndConversation
--- @param player string The player Username
function EndConversation:new(player)
    local o = self:super()
    o.player = player
    return o
end

function EndConversation:GetLines()
    local lines = {}
    table.insert(lines, "EndConversation")
    table.insert(lines, self.player)
    return lines
end

local Initialize = OutCommand:derive("Initialize")

--- RequestTypes
function Initialize:new()
    local o = self:super()
    return o
end

function Initialize:GetLines()
    local lines = {}
    table.insert(lines, "Initialize")
    return lines
end

return {
    NewConversation = NewConversation,
    ChatMessage = ChatMessage,
    EndConversation = EndConversation,
    Initialize = Initialize
}