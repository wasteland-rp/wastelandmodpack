if isClient() then return end

WGNServer = WGNServer or {}
WGNServer._publicData = {}
WGNServer._privateData = {}

function WGNServer.SendGlobal(command, options)
    if options == nil then
        options = {}
    end
    sendServerCommand("WGN", command, options)
end

function WGNServer.SendUser(username, command, options)
    if options == nil then
        options = {}
    end

    local player = WGNServer.GetPlayerByUserName(username)
    if player then
        sendServerCommand(player, "WGN", command, options)
        return
    end

    print("WGN:SendUser:Player not found: " .. username)
end

function WGNServer.GetPlayerByUserName(username)
    local player = getPlayerFromUsername(username)
    if player then
        return player
    end
    local players = getOnlinePlayers()
    for i=0, players:size()-1 do
        local player = players:get(i)
        if player:getUsername() == username then
            return player
        end
    end
    return nil
end

local dbVersion = 5
function WGNServer.InitGlobalModData()
    WGNServer._publicData = ModData.getOrCreate("WGN")
    if not WGNServer._publicData.v or WGNServer._publicData.v ~= dbVersion then WGNServer._publicData = {} end
    if not WGNServer._publicData.PendingPlayerNPCs then WGNServer._publicData.PendingPlayerNPCs = {} end
    if not WGNServer._publicData.PendingWorldNPCs then WGNServer._publicData.PendingWorldNPCs = {} end
    if not WGNServer._publicData.NPCTypes then WGNServer._publicData.NPCTypes = {} end
    WGNServer._publicData.v = dbVersion
    WGNServer.SavePublicData()

    WGNServer._privateData = ModData.getOrCreate("WGNPrivate")
    if not WGNServer._privateData.v or WGNServer._privateData.v ~= dbVersion then WGNServer._privateData = {} end
    if not WGNServer._privateData.RunningNPCs then WGNServer._privateData.RunningNPCs = {} end
    if not WGNServer._privateData.UsedWorldNPCs then WGNServer._privateData.UsedWorldNPCs = {} end
    WGNServer._privateData.v = dbVersion
    WGNServer.SavePrivateData()
end

function WGNServer.SavePublicData()
    ModData.add("WGN", WGNServer._publicData)
    ModData.transmit("WGN")
end

function WGNServer.SavePrivateData()
    ModData.add("WGNPrivate", WGNServer._privateData)
end

function WGNServer.SetNPCTypes(types)
    WGNServer._publicData.NPCTypes = types
    WGNServer.SavePublicData()
end

function WGNServer.AddPlayerNPC(username, frequency, type, specific)
    WGNServer._publicData.PendingPlayerNPCs[username] = {
        frequency = frequency,
        type = type,
        specific = specific
    }
    WGNServer.SavePublicData()
end

function WGNServer.AddWorldNPC(frequency, uses, type, specific)
    WGNServer._publicData.PendingWorldNPCs[frequency] = {
        uses = uses,
        type = type,
        specific = specific
    }
    WGNServer.SavePublicData()
end

function WGNServer.UpdateWorldNPCUses(frequency, uses)
    WGNServer._publicData.PendingWorldNPCs[frequency].uses = uses
    WGNServer.SavePublicData()
end

function WGNServer.GetPendingPlayerNPC(username)
    return WGNServer._publicData.PendingPlayerNPCs[username]
end

function WGNServer.GetPendingWorldNPC(frequency)
    return WGNServer._publicData.PendingWorldNPCs[frequency]
end

function WGNServer.RemovePendingPlayerNPC(username)
    WGNServer._publicData.PendingPlayerNPCs[username] = nil
    WGNServer.SavePublicData()
end

function WGNServer.RemovePendingWorldNPC(frequency)
    WGNServer._publicData.PendingWorldNPCs[frequency] = nil
    WGNServer.ClearUsedNPC(frequency)
    WGNServer.SavePublicData()
end

function WGNServer.InitializeRunningNPC(username, frequency)
    WGNServer._privateData.RunningNPCs[username] = {
        frequency = frequency,
        lastMessage = getTimestamp(),
        name = ""
    }
    WGNServer.SendUser(username, "AlertNPCInitializing")
    WGNServer.SavePrivateData()
end

function WGNServer.SetRunningNPCName(username, npcName)
    WGNServer._privateData.RunningNPCs[username].name = npcName
    WGNServer.SavePrivateData()
end

function WGNServer.GetRunningNPC(username)
    return WGNServer._privateData.RunningNPCs[username]
end

function WGNServer.ClearRunningNPC(username)
    WGNServer._privateData.RunningNPCs[username] = nil
    WGNServer.SavePrivateData()
end

function WGNServer.ClearRunningNPCs()
    WGNServer._privateData.RunningNPCs = {}
    WGNServer.SavePrivateData()
end

function WGNServer.AddUsedNPC(username, frequency)
    if not WGNServer._privateData.UsedWorldNPCs[frequency] then WGNServer._privateData.UsedWorldNPCs[frequency] = {} end
    WGNServer._privateData.UsedWorldNPCs[frequency][username] = true
    WGNServer.SavePrivateData()
end

function WGNServer.IsNPCUsed(username, frequency)
    return WGNServer._privateData.UsedWorldNPCs[frequency] and WGNServer._privateData.UsedWorldNPCs[frequency][username]
end

function WGNServer.ClearUsedNPC(frequency)
    WGNServer._privateData.UsedWorldNPCs[frequency] = nil
    WGNServer.SavePrivateData()
end

function WGNServer.ClearUsedNPCs()
    WGNServer._privateData.UsedWorldNPCs = {}
    WGNServer.SavePrivateData()
end

function WGNServer.Log(message)
    writeLog("WGN", message)
end

function WGNServer.CheckForOfflinePlayers()
    for username, _ in pairs(WGNServer._privateData.RunningNPCs) do
        if not WGNServer.GetPlayerByUserName(username) then
            WGNServer.ClearRunningNPC(username)
        end
    end
    for username, _ in pairs(WGNServer._publicData.PendingPlayerNPCs) do
        if not WGNServer.GetPlayerByUserName(username) then
            WGNServer.RemovePendingPlayerNPC(username)
        end
    end
end