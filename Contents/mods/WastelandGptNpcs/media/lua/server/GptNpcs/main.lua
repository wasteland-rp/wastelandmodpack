if isClient() then return end

require "GtpNpcs/lib"

local FileMonitor = require "GptNpcs/FileMonitor"
local InCommandParser = require "GptNpcs/InCommandParser"
local OutCommandWriter = require "GptNpcs/OutCommandWriter"
local OutCommands = require "GptNpcs/OutCommands"

local ClientCommands = {}

local out = OutCommandWriter:new("npc-in")
local fileMonitor = FileMonitor:new("npc-out.txt", InCommandParser)

function ClientCommands.InitializeNPC(player, args)
    local username = player:getUsername()

    if WGNServer.GetRunningNPC(username) then
        print("WGN:InitializeNPC:Already running: " .. username)
        return
    end

    local frequency = args[1]

    local playerNPC = WGNServer.GetPendingPlayerNPC(username)
    if playerNPC and playerNPC.frequency == frequency then
        WGNServer.RemovePendingPlayerNPC(username)
        WGNServer.InitializeRunningNPC(username, frequency)
        out:WriteCommand(OutCommands.NewConversation:new(player, playerNPC.type, playerNPC.specific))
        return
    end


    if WGNServer.IsNPCUsed(username, frequency) then
        print("WGN:InitializeNPC:Already used: " .. username)
        return
    end

    local worldNPC = WGNServer.GetPendingWorldNPC(frequency)
    if worldNPC then
        if worldNPC.uses > 0 then
            if worldNPC.uses == 1 then
                WGNServer.RemovePendingWorldNPC(frequency)
            else
                WGNServer.UpdateWorldNPCUses(frequency, worldNPC.uses - 1)
            end
        end
        WGNServer.AddUsedNPC(username, frequency)
        WGNServer.InitializeRunningNPC(username, frequency)
        out:WriteCommand(OutCommands.NewConversation:new(player, worldNPC.type, worldNPC.specific))
        return
    end
end

function ClientCommands.EndConversation(player)
    local username = player:getUsername()
    out:WriteCommand(OutCommands.EndConversation:new(username))
    WGNServer.ClearRunningNPC(username)
end

function ClientCommands.ChatMessage(player, args)
    local username = player:getUsername()
    local message = args[1]

    out:WriteCommand(OutCommands.ChatMessage:new(username, message))
end

function ClientCommands.AddPlayerNPC(player, args)
    local username = args[1]
    local frequency = args[2]
    local type = args[3]
    local specific = args[4]

    WGNServer.AddPlayerNPC(username, frequency, type, specific)
    WGNServer.SendUser(username, "AlertNPCAvailable", {type, frequency})
end

function ClientCommands.AddWorldNPC(player, args)
    local frequency = args[1]
    local uses = args[2]
    local type = args[3]
    local specific = args[4]

    WGNServer.AddWorldNPC(frequency, uses, type, specific)
end

function ClientCommands.RemovePlayerNPC(player, args)
    local username = args[1]

    WGNServer.RemovePendingPlayerNPC(username)
end

function ClientCommands.RemoveWorldNPC(player, args)
    local frequency = args[1]

    WGNServer.RemovePendingWorldNPC(frequency)
end

local function OnClientCommand(module, command, player, args)
    if module ~= "WGN" then return end

    if ClientCommands[command] then
        ClientCommands[command](player, args)
    else
        print("WGN:OnClientCommand Unknown " .. command)
    end
end

fileMonitor:Start()
Events.OnClientCommand.Add(OnClientCommand)
Events.OnInitGlobalModData.Add(WGNServer.InitGlobalModData)
Events.EveryHours.Add(WGNServer.CheckForOfflinePlayers)