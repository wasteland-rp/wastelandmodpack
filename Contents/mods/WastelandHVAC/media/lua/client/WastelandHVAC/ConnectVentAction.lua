WLHVAC_ConnectVentAction = ISBaseTimedAction:derive("WLHVAC_ConnectVentAction")

function WLHVAC_ConnectVentAction.getRequirements(furnace, vent)
    local dist = IsoUtils.DistanceTo(furnace.x, furnace.y, furnace.z, vent.x, vent.y, vent.z)
    return {
        distance = math.ceil(dist),
        sheets = math.ceil(dist / 2),
        screws = math.floor(dist * 2),
        ductTape = math.ceil(dist),
        time = math.ceil(dist * 30)
    }
end

function WLHVAC_ConnectVentAction:isValid()
    return true
end

function WLHVAC_ConnectVentAction:waitToStart()
    local square = getCell():getGridSquare(self.vent.x, self.vent.y, self.vent.z)
    if not square then
        self:forceStop()
        return false
    end
    local vent = WastelandHVACSystem:getVentObject(square)
    if not vent then
        self:forceStop()
        return false
    end
    self.character:faceThisObject(vent)
    return self.character:shouldBeTurning()
end

function WLHVAC_ConnectVentAction:start()
    self:setActionAnim("Loot")
    self.character:reportEvent("EventLootItem")
end

function WLHVAC_ConnectVentAction:stop()
    ISBaseTimedAction.stop(self)
end

function WLHVAC_ConnectVentAction:perform()
    local playerObj = self.character
    local furnace = self.furnace
    local vent = self.vent
    local cost = self.cost
    local playerInv = playerObj:getInventory()

    for i=1, cost.sheets do
        playerInv:RemoveOneOf("Base.SheetMetal")
    end

    for i=1, cost.screws do
        playerInv:RemoveOneOf("Base.Screws")
    end

    for i=1, cost.ductTape do
        local dt = playerObj:getInventory():getFirstType("Base.DuctTape")
        if dt then
            dt:Use()
        end
    end

    WastelandHVACSystem:setVentFurnace(playerObj, vent.x, vent.y, vent.z, furnace.x, furnace.y, furnace.z)

    -- needed to remove from queue / start next.
    ISBaseTimedAction.perform(self)
end

function WLHVAC_ConnectVentAction:new(playerObj, furnace, vent)
    local o = ISBaseTimedAction.new(self, playerObj)
    o.cost = WLHVAC_ConnectVentAction.getRequirements(furnace, vent)
    o.character = playerObj
    o.furnace = furnace
    o.vent = vent
    o.maxTime = o.cost.time
    return o
end