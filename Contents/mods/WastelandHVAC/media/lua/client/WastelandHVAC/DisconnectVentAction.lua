WLHVAC_DisconnectVentAction = ISBaseTimedAction:derive("WLHVAC_DisconnectVentAction")

function WLHVAC_DisconnectVentAction:isValid()
    return true
end

function WLHVAC_DisconnectVentAction:waitToStart()
    self.character:faceLocationF(self.vent.x, self.vent.y)
    return self.character:shouldBeTurning()
end

function WLHVAC_DisconnectVentAction:start()
    self:setActionAnim("Loot")
    self.character:reportEvent("EventLootItem")
end

function WLHVAC_DisconnectVentAction:stop()
    ISBaseTimedAction.stop(self)
end

function WLHVAC_DisconnectVentAction:perform()
    local playerObj = self.character
    local vent = self.vent

    local furnace = WastelandHVACSystem:getFurnace(vent.furnace[1], vent.furnace[2], vent.furnace[3])
    if furnace then
        local cost = WLHVAC_ConnectVentAction.getRequirements(furnace, vent)
        local sheetsToReturn = math.ceil(cost.sheets * (ZombRand(20, 80)/100))
        local screwsToReturn = math.ceil(cost.screws * (ZombRand(20, 80)/100))
        for i=1, sheetsToReturn do
            playerObj:getInventory():AddItem("Base.SheetMetal")
        end
        for i=1, screwsToReturn do
            playerObj:getInventory():AddItem("Base.Screws")
        end
    end
    WastelandHVACSystem:removeVentFurnace(playerObj, vent.x, vent.y, vent.z)

    -- needed to remove from queue / start next.
    ISBaseTimedAction.perform(self)
end

function WLHVAC_DisconnectVentAction:new(playerObj, vent)
    local o = ISBaseTimedAction.new(self, playerObj)
    o.character = playerObj
    o.vent = vent
    o.maxTime = 100
    return o
end