WLHVAC_FuelFurnaceAction = ISBaseTimedAction:derive("WLHVAC_FuelFurnaceAction")

function WLHVAC_FuelFurnaceAction:isValid()
    return true
end

function WLHVAC_FuelFurnaceAction:waitToStart()
    local square = getCell():getGridSquare(self.furnace.x, self.furnace.y, self.furnace.z)
    if not square then
        self:forceStop()
        return false
    end
    local furnace = WastelandHVACSystem:getFurnaceObject(square)
    if not furnace then
        self:forceStop()
        return false
    end
    self.character:faceThisObject(furnace)
    return self.character:shouldBeTurning()
end

function WLHVAC_FuelFurnaceAction:getNextFuelItem()
    self.fuelItem = self.character:getInventory():getFirstType("Base.Charcoal")
    if not self.fuelItem then
        self.fuelItem = self.character:getInventory():getFirstType("Base.Log")
    end
end

function WLHVAC_FuelFurnaceAction:start()
    self:setActionAnim("Loot")
    self.character:reportEvent("EventLootItem")
    self:getNextFuelItem()
    if not self.fuelItem then
        self:forceStop()
    end
end

function WLHVAC_FuelFurnaceAction:stop()
    ISBaseTimedAction.stop(self)
end

function WLHVAC_FuelFurnaceAction:perform()
    local playerObj = self.character
    local furnace = self.furnace

    -- shouldn't happen
    if not self.fuelItem then
        ISBaseTimedAction.perform(self)
        return
    end

    if self.fuelItem:getFullType() == "Base.Charcoal" then
        self.fuelItem:Use()
        WastelandHVACSystem:addFuelFurnace(playerObj, furnace.x, furnace.y, furnace.z, 10)
        self.furnace.fuel = self.furnace.fuel + 10
    elseif self.fuelItem:getFullType() == "Base.Log" then
        playerObj:getInventory():RemoveOneOf(self.fuelItem:getType())
        WastelandHVACSystem:addFuelFurnace(playerObj, furnace.x, furnace.y, furnace.z, 5)
        self.furnace.fuel = self.furnace.fuel + 5
    end

    if self.furnace.fuel >= 300 then
        ISBaseTimedAction.perform(self)
        return
    end

    playerObj:addLineChatElement("Fuel: " .. self.furnace.fuel .. " / 300")
    self:getNextFuelItem()

    if not self.fuelItem then
        ISBaseTimedAction.perform(self)
        return
    end

    self:resetJobDelta()
    self:setActionAnim("Loot")
    self.character:reportEvent("EventLootItem")
end

function WLHVAC_FuelFurnaceAction:new(playerObj, furnace)
    local o = ISBaseTimedAction.new(self, playerObj)
    o.character = playerObj
    o.furnace = furnace
    o.maxTime = 50
    return o
end