WLHVAC_PickupFurnaceAction = ISBaseTimedAction:derive("WLHVAC_PickupFurnaceAction")

function WLHVAC_PickupFurnaceAction:isValid()
    return true
end

function WLHVAC_PickupFurnaceAction:waitToStart()
    local square = getCell():getGridSquare(self.furnace.x, self.furnace.y, self.furnace.z)
    if not square then
        self:forceStop()
        return false
    end
    local furnace = WastelandHVACSystem:getFurnaceObject(square)
    if not furnace then
        self:forceStop()
        return false
    end
    self.character:faceThisObject(furnace)
    return self.character:shouldBeTurning()
end

function WLHVAC_PickupFurnaceAction:start()
    self:setActionAnim("Loot")
    self.character:reportEvent("EventLootItem")
end

function WLHVAC_PickupFurnaceAction:stop()
    ISBaseTimedAction.stop(self)
end

function WLHVAC_PickupFurnaceAction:perform()
    local playerObj = self.character
    local furnace = self.furnace
    -- todo add in furnace cost

    WastelandHVACSystem:removeFurnace(playerObj, furnace.x, furnace.y, furnace.z)

    local square = getCell():getGridSquare(furnace.x, furnace.y, furnace.z)
    local objects = square:getObjects()
    for i=1, objects:size() do
        local object = objects:get(i-1)
        local sprite = object:getSprite()
        if sprite then
            local spriteName = sprite:getName()
            if spriteName == WastelandHVACSystem.tiles.FurnaceOff or
               spriteName == WastelandHVACSystem.tiles.FurnaceOn or
               spriteName == WastelandHVACSystem.tiles.FurnaceOffNorth or
               spriteName == WastelandHVACSystem.tiles.FurnaceOnNorth then
                square:transmitRemoveItemFromSquare(object)
                break
            end
        end
    end

    local item = playerObj:getInventory():AddItem("WLHVAC.Furnace")
    -- todo force into hands

    -- needed to remove from queue / start next.
    ISBaseTimedAction.perform(self)
end

function WLHVAC_PickupFurnaceAction:new(playerObj, furnace)
    local o = ISBaseTimedAction.new(self, playerObj)
    o.character = playerObj
    o.furnace = furnace
    o.maxTime = 100
    return o
end