WLHVAC_PickupVentAction = ISBaseTimedAction:derive("WLHVAC_PickupVentAction")

function WLHVAC_PickupVentAction:isValid()
    return true
end

function WLHVAC_PickupVentAction:waitToStart()
    self.character:faceLocationF(self.vent.x, self.vent.y)
    return self.character:shouldBeTurning()
end

function WLHVAC_PickupVentAction:start()
    self:setActionAnim("Loot")
    self.character:reportEvent("EventLootItem")
end

function WLHVAC_PickupVentAction:stop()
    ISBaseTimedAction.stop(self)
end

function WLHVAC_PickupVentAction:perform()
    local playerObj = self.character
    local vent = self.vent
    -- todo add in furnace cost

    WastelandHVACSystem:removeVent(playerObj, vent.x, vent.y, vent.z)

    local square = getCell():getGridSquare(vent.x, vent.y, vent.z)
    local objects = square:getObjects()
    for i=1, objects:size() do
        local object = objects:get(i-1)
        local sprite = object:getSprite()
        if sprite then
            local spriteName = sprite:getName()
            if spriteName == WastelandHVACSystem.tiles.Vent or
               spriteName == WastelandHVACSystem.tiles.VentNorth then
                square:transmitRemoveItemFromSquare(object)
                break
            end
        end
    end

    playerObj:getInventory():AddItem("WLHVAC.Vent")

    -- needed to remove from queue / start next.
    ISBaseTimedAction.perform(self)
end

function WLHVAC_PickupVentAction:new(playerObj, vent)
    local o = ISBaseTimedAction.new(self, playerObj)
    o.character = playerObj
    o.vent = vent
    o.maxTime = 100
    return o
end