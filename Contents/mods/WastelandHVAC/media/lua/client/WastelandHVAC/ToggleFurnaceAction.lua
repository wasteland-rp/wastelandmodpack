WLHVAC_ToggleFurnaceAction = ISBaseTimedAction:derive("WLHVAC_ToggleFurnaceAction")

function WLHVAC_ToggleFurnaceAction:isValid()
    return true
end

function WLHVAC_ToggleFurnaceAction:waitToStart()
    local square = getCell():getGridSquare(self.furnace.x, self.furnace.y, self.furnace.z)
    if not square then
        self:forceStop()
        return false
    end
    local furnace = WastelandHVACSystem:getFurnaceObject(square)
    if not furnace then
        self:forceStop()
        return false
    end
    self.character:faceThisObject(furnace)
    return self.character:shouldBeTurning()
end

function WLHVAC_ToggleFurnaceAction:start()
    self:setActionAnim("Loot")
    self.character:reportEvent("EventLootItem")
end

function WLHVAC_ToggleFurnaceAction:stop()
    ISBaseTimedAction.stop(self)
end

function WLHVAC_ToggleFurnaceAction:perform()
    local playerObj = self.character
    local furnace = self.furnace

    if furnace.enabled then
        WastelandHVACSystem:disableFurnace(playerObj, furnace.x, furnace.y, furnace.z)
    else
        WastelandHVACSystem:enableFurnace(playerObj, furnace.x, furnace.y, furnace.z)
    end

    -- needed to remove from queue / start next.
    ISBaseTimedAction.perform(self)
end

function WLHVAC_ToggleFurnaceAction:new(playerObj, furnace)
    local o = ISBaseTimedAction.new(self, playerObj)
    o.character = playerObj
    o.furnace = furnace
    o.maxTime = 50
    return o
end