local toolTips = {
    toolTipsAvailable = {},
    toolTipUsed = {},
}

function getTooltip()
    local pool = toolTips.toolTipsAvailable
    if #pool == 0 then
        local tooltip = ISToolTip:new()
        tooltip:initialise()
        table.insert(toolTips.toolTipUsed, tooltip)
        return tooltip
    end
    local tooltip = table.remove(pool, #pool)
    tooltip:reset()
    table.insert(toolTips.toolTipUsed, tooltip)
    return tooltip
end

function resetTooltips()
    for _,tooltip in ipairs(toolTips.toolTipUsed) do
        table.insert(toolTips.toolTipsAvailable, tooltip)
    end
    table.wipe(toolTips.toolTipUsed)
end

local function checkForVent(playerIdx, option)
    local player = getSpecificPlayer(playerIdx)
    if not player then return false end
    local tooltip = getTooltip()
    tooltip:setName("Attach Vent")
    tooltip.description = "Attach a vent to a wall"
    if not player:getInventory():containsType("WLHVAC.Vent") then
        tooltip.description = tooltip.description .. "\n<RGB:1,0,0>1 Vent"
        option.notAvailable = true
    else
        tooltip.description = tooltip.description .. "\n<RGB:0,1,0>1 Vent"
    end
    if player:getInventory():getCountType("Base.Nails") < 4 then
        tooltip.description = tooltip.description .. "\n<RGB:1,0,0>4 Nails"
        option.notAvailable = true
    else
        tooltip.description = tooltip.description .. "\n<RGB:0,1,0>4 Nails"
    end
    if not player:getInventory():containsTag("Hammer") then
        tooltip.description = tooltip.description .. "\n<RGB:1,0,0>Hammer"
        option.notAvailable = true
    else
        tooltip.description = tooltip.description .. "\n<RGB:0,1,0>Hammer"
    end
    if player:getPerkLevel(Perks.Woodwork) < 2 then
        tooltip.description = tooltip.description .. "\n<RGB:1,0,0>Carpentry 2"
        option.notAvailable = true
    else
        tooltip.description = tooltip.description .. "\n<RGB:0,1,0>Carpentry 2"
    end
    if player:getPerkLevel(Perks.MetalWelding) < 2 then
        tooltip.description = tooltip.description .. "\n<RGB:1,0,0>Metalworking 2"
        option.notAvailable = true
    else
        tooltip.description = tooltip.description .. "\n<RGB:0,1,0>Metalworking 2"
    end
    option.toolTip = tooltip
end

local function checkForFurnace(playerIdx, option)
    local player = getSpecificPlayer(playerIdx)
    if not player then return false end
    local tooltip = getTooltip()
    tooltip:setName("Place Furnace")
    tooltip.description = "Place a furnace"
    if not player:getInventory():containsType("WLHVAC.Furnace") then
        tooltip.description = tooltip.description .. "\n<RGB:1,0,0>1 Furnace"
        option.notAvailable = true
    else
        tooltip.description = tooltip.description .. "\n<RGB:0,1,0>1 Furnace"
    end
    if not player:getInventory():containsTag("Hammer") then
        tooltip.description = tooltip.description .. "\n<RGB:1,0,0>Hammer"
        option.notAvailable = true
    else
        tooltip.description = tooltip.description .. "\n<RGB:0,1,0>Hammer"
    end
    if player:getPerkLevel(Perks.Woodwork) < 10 then
        tooltip.description = tooltip.description .. "\n<RGB:1,0,0>Carpentry 10"
        option.notAvailable = true
    else
        tooltip.description = tooltip.description .. "\n<RGB:0,1,0>Carpentry 10"
    end
    option.toolTip = tooltip
end

local function checkForFuel(playerIdx, option, furnace)
    local player = getSpecificPlayer(playerIdx)
    if not player then return false end
    local tooltip = getTooltip()
    tooltip:setName("Add Fuel")
    tooltip.description = "Add fuel to the furnace"
    if furnace.fuel >= 300 then
        tooltip.description = tooltip.description .. "\n<RGB:1,0,0>Furnace is full"
        option.notAvailable = true
    end
    if not player:getInventory():containsType("Base.Charcoal") and not player:getInventory():containsType("Base.Log") then
        tooltip.description = tooltip.description .. "\n<RGB:1,0,0>Charcoal or Log"
        option.notAvailable = true
    else
        tooltip.description = tooltip.description .. "\n<RGB:0,1,0>Charcoal or Log"
    end
    option.toolTip = tooltip
end

local function checkConnectVent(playerIdx, option, furnace, vent)
    local cost = WLHVAC_ConnectVentAction.getRequirements(furnace, vent)
    local player = getSpecificPlayer(playerIdx)
    if not player then return false end
    local inventory = player:getInventory()
    local tooltip = getTooltip()
    tooltip:setName("Connect Vent")
    tooltip.description = "Connect vent to a furnace"
    tooltip.description = tooltip.description .. "\n<RGB:1,1,1>Distance: " .. cost.distance
    if inventory:getCountType("Base.SheetMetal") < cost.sheets then
        tooltip.description = tooltip.description .. "\n<RGB:1,0,0>" .. cost.sheets .. " Metal Sheet"
        option.notAvailable = true
    else
        tooltip.description = tooltip.description .. "\n<RGB:0,1,0>" .. cost.sheets .. " Metal Sheet"
    end
    if inventory:getCountType("Base.Screws") < cost.screws then
        tooltip.description = tooltip.description .. "\n<RGB:1,0,0>" .. cost.screws .. " Screws"
        option.notAvailable = true
    else
        tooltip.description = tooltip.description .. "\n<RGB:0,1,0>" .. cost.screws .. " Screws"
    end
    if inventory:getUsesType("Base.DuctTape") < cost.ductTape then
        tooltip.description = tooltip.description .. "\n<RGB:1,0,0>" .. cost.ductTape .. " Duct Tape"
        option.notAvailable = true
    else
        tooltip.description = tooltip.description .. "\n<RGB:0,1,0>" .. cost.ductTape .. " Duct Tape"
    end
    if not inventory:containsTag("Hammer") then
        tooltip.description = tooltip.description .. "\n<RGB:1,0,0>Hammer"
        option.notAvailable = true
    else
        tooltip.description = tooltip.description .. "\n<RGB:0,1,0>Hammer"
    end
    if not inventory:containsTag("Screwdriver") then
        tooltip.description = tooltip.description .. "\n<RGB:1,0,0>Screwdriver"
        option.notAvailable = true
    else
        tooltip.description = tooltip.description .. "\n<RGB:0,1,0>Screwdriver"
    end
    option.toolTip = tooltip
end

local function checkForFurnacePickup(playerIdx, option)
    local player = getSpecificPlayer(playerIdx)
    if not player then return false end
    local tooltip = getTooltip()
    tooltip:setName("Pickup Furnace")
    tooltip.description = "Pickup a furnace"
    if not player:getInventory():containsTag("Hammer") then
        tooltip.description = tooltip.description .. "\n<RGB:1,0,0>Hammer"
        option.notAvailable = true
    else
        tooltip.description = tooltip.description .. "\n<RGB:0,1,0>Hammer"
    end
    option.toolTip = tooltip
end

local function checkForVentPickup(playerIdx, option)
    local player = getSpecificPlayer(playerIdx)
    if not player then return false end
    local tooltip = getTooltip()
    tooltip:setName("Pickup Vent")
    tooltip.description = "Pickup a vent"
    if not player:getInventory():containsTag("Screwdriver") then
        tooltip.description = tooltip.description .. "\n<RGB:1,0,0>Screwdriver"
        option.notAvailable = true
    else
        tooltip.description = tooltip.description .. "\n<RGB:0,1,0>Screwdriver"
    end
    option.toolTip = tooltip
end

local function checkDisconnectVent(playerIdx, option)
    local player = getSpecificPlayer(playerIdx)
    if not player then return false end
    local tooltip = getTooltip()
    tooltip:setName("Disconnect Vent")
    tooltip.description = "Disconnect a vent"
    if not player:getInventory():containsTag("Screwdriver") then
        tooltip.description = tooltip.description .. "\n<RGB:1,0,0>Screwdriver"
        option.notAvailable = true
    else
        tooltip.description = tooltip.description .. "\n<RGB:0,1,0>Screwdriver"
    end
    if not player:getInventory():containsTag("Hammer") then
        tooltip.description = tooltip.description .. "\n<RGB:1,0,0>Hammer"
        option.notAvailable = true
    else
        tooltip.description = tooltip.description .. "\n<RGB:0,1,0>Hammer"
    end
    option.toolTip = tooltip
end

local function checkStartFurnace(playerIdx, option, square)
    local player = getSpecificPlayer(playerIdx)
    if not player then return false end
    local tooltip = getTooltip()
    tooltip:setName("Start Furnace")
    tooltip.description = "Start up the furnace"
    if not square:haveElectricity() then
        tooltip.description = tooltip.description .. "\n<RGB:1,0,0>No Electricity Available"
        option.notAvailable = true
    else
        tooltip.description = tooltip.description .. "\n<RGB:0,1,0>Electricity Available"
    end
    option.toolTip = tooltip
end

local function doPlaceMenu(playerIdx, context)
    local player = getSpecificPlayer(playerIdx)
    if not player then return end

    local vent = player:getInventory():containsTypeRecurse("WLHVAC.Vent")
    local furnace = player:getInventory():containsTypeRecurse("WLHVAC.Furnace")

    if vent then
        local option = context:addOption("Place Vent", player, function()
            local ventBuild = WLHVAC_VentBuildObject:new(playerIdx)
            getCell():setDrag(ventBuild, playerIdx)
        end)
        checkForVent(playerIdx, option)
    end
    if furnace then
        local option = context:addOption("Place Furnace", player, function()
            local furnaceBuild = WLHVAC_FurnaceBuildObject:new(playerIdx)
            getCell():setDrag(furnaceBuild, playerIdx)
        end)
        checkForFurnace(playerIdx, option)
    end
end

local function doVentConnectMenu(playerIdx, square, context)
    local vent = WastelandHVACSystem:getVent(square:getX(), square:getY(), square:getZ())
    if not vent then print("No Vent") return end
    if vent.furnace then
        local option = context:addOption("Disconnect Vent", nil, function()
            luautils.walkAdj(getSpecificPlayer(playerIdx), square)
            ISTimedActionQueue.add(WLHVAC_DisconnectVentAction:new(getSpecificPlayer(playerIdx), vent))
        end)
        checkDisconnectVent(playerIdx, option)
    else
        local option = context:addOption("Pickup Vent", nil, function()
            luautils.walkAdj(getSpecificPlayer(playerIdx), square)
            ISTimedActionQueue.add(WLHVAC_PickupVentAction:new(getSpecificPlayer(playerIdx), vent))
        end)
        checkForVentPickup(playerIdx, option)
        local furnaces = WastelandHVACSystem:getFurnacesNear(square:getX(), square:getY(), square:getZ())
        if #furnaces == 0 then print("No Furnace Near") return end
        local subMenu = WL_ContextMenuUtils.getOrCreateSubMenu(context, "Run Ducting")
        for _, furnace in ipairs(furnaces) do
            option = subMenu:addOption("to furnace at " .. furnace.x .. ", " .. furnace.y .. ", " .. furnace.z, nil, function()
                luautils.walkAdj(getSpecificPlayer(playerIdx), square)
                ISTimedActionQueue.add(WLHVAC_ConnectVentAction:new(getSpecificPlayer(playerIdx), furnace, vent))
            end)
            checkConnectVent(playerIdx, option, furnace, vent)
        end
    end
end


local function doFurnaceMenu(playerIdx, square, context)
    local furnace = WastelandHVACSystem:getFurnace(square:getX(), square:getY(), square:getZ())
    if not furnace then return end

    local infoContext = context:addOption("Furnance Info")
    furnaceInfoTooltip = getTooltip()
    furnaceInfoTooltip:setName("Furnace Info")
    furnaceInfoTooltip.description = "Fuel Stored: " .. furnace.fuel .. " / 300" ..
        "\nLit: " .. (furnace.enabled and "Yes" or "No") ..
        "\nConnected Vents: " .. #furnace.vents ..
        "\nFuel Consumption per Hour: " .. WastelandHVACSystem:getBurnRate(furnace)
    infoContext.toolTip = furnaceInfoTooltip

    if furnace.enabled then
        context:addOption("Shut Down Furnace", nil, function()
            luautils.walkAdj(getSpecificPlayer(playerIdx), square)
            ISTimedActionQueue.add(WLHVAC_ToggleFurnaceAction:new(getSpecificPlayer(playerIdx), furnace))
        end)
    else
        local option = context:addOption("Start Up Furnace", nil, function()
            luautils.walkAdj(getSpecificPlayer(playerIdx), square)
            ISTimedActionQueue.add(WLHVAC_ToggleFurnaceAction:new(getSpecificPlayer(playerIdx), furnace))
        end)
        checkStartFurnace(playerIdx, option, square)
    end
    local option = context:addOption("Pickup Furnace", nil, function()
        luautils.walkAdj(getSpecificPlayer(playerIdx), square)
        ISTimedActionQueue.add(WLHVAC_PickupFurnaceAction:new(getSpecificPlayer(playerIdx), furnace))
    end)
    checkForFurnacePickup(playerIdx, option)
    option = context:addOption("Add Fuel", nil, function()
        luautils.walkAdj(getSpecificPlayer(playerIdx), square)
        ISTimedActionQueue.add(WLHVAC_FuelFurnaceAction:new(getSpecificPlayer(playerIdx), furnace))
    end)
    checkForFuel(playerIdx, option, furnace)
end

local function WorldMenu(playerIdx, context, worldobjects, test)
    if test and ISWorldObjectContextMenu.Test then return true end

    resetTooltips()

    doPlaceMenu(playerIdx, context)

    if #worldobjects == 0 then return end

    local square = worldobjects[1]:getSquare()
    doVentConnectMenu(playerIdx, square, context)
    doFurnaceMenu(playerIdx, square, context)
end

Events.OnPreFillWorldObjectContextMenu.Add(WorldMenu)