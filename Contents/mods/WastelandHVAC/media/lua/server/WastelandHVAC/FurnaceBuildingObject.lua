WLHVAC_FurnaceBuildObject = ISBuildingObject:derive('WLHVAC_FurnaceBuildObject')

function WLHVAC_FurnaceBuildObject:create(x, y, z, north, sprite)
  local cell = getWorld():getCell()
  self.sq = cell:getGridSquare(x, y, z)
  self.javaObject = IsoObject.new(getCell(), self.sq, getSprite(sprite))
  self.sq:AddTileObject(self.javaObject)
  buildUtil.consumeMaterial(self)
  self.javaObject:transmitCompleteItemToServer()
  WastelandHVACSystem:addFurnace(getSpecificPlayer(self.player), x, y, z)
end

function WLHVAC_FurnaceBuildObject:isValid(square)
  if not ISBuildingObject.isValid(self, square) then
    return false
  end
  return true
end

function WLHVAC_FurnaceBuildObject:render(x, y, z, square)
  ISBuildingObject.render(self, x, y, z, square)
end

function WLHVAC_FurnaceBuildObject:new(player)
  local o = {}
  setmetatable(o, self)
  self.__index = self
  o:init()
  o:setSprite(WastelandHVACSystem.tiles.FurnaceOff)
  o:setNorthSprite(WastelandHVACSystem.tiles.FurnaceOffNorth)
  o.player = player
  o.dismantable = false
  o.name = "Furnace"
  o.stopOnWalk = true
  o.stopOnRun = true
  o.maxTime = 100
  o.modData["need:WLHVAC.Furnace"] = 1
  return o
end