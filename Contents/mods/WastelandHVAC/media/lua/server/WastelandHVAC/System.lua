--- @class WastelandHVACSystem : WL_ClientServerBase
WastelandHVACSystem = WastelandHVACSystem or WL_ClientServerBase:new("WastelandHVACSystem")
WastelandHVACSystem.needsPrivateData = true
WastelandHVACSystem.needsPublicData = true
WastelandHVACSystem.defaultPrivateData = {pendingTileUpdates = {}}
WastelandHVACSystem.defaultPublicData = {furnaces = {}, vents = {}}
WastelandHVACSystem.currentState = WastelandHVACSystem.currentState or false
WastelandHVACSystem.tiles = {
    FurnaceOffNorth = "edit_ddd_RUS_Production_Furnace_26",
    FurnaceOnNorth = "edit_ddd_RUS_Production_Furnace_27",
    FurnaceOff = "edit_ddd_RUS_Production_Furnace_24",
    FurnaceOn = "edit_ddd_RUS_Production_Furnace_25",
    VentNorth = "melos_tiles_walls_vanilla_add_03_93",
    Vent = "melos_tiles_walls_vanilla_add_03_92",
}

if WastelandHVACSystem.didBindServerEvents then
    Events.EveryHours.Remove(WastelandHVACSystem.EveryHours)
end

if WastelandHVACSystem.didBindClientEvents then
    Events.OnTick.Remove(WastelandHVACSystem.OnTick)
end

function WastelandHVACSystem:onPublicDataUpdated()
    if not self.publicData.furnaces then
        self.publicData.furnaces = {}
    end
    if not self.publicData.vents then
        self.publicData.vents = {}
    end
end

function WastelandHVACSystem:onModDataInit()
    if not self.publicData.furnaces then
        self.publicData.furnaces = {}
    end
    if not self.publicData.vents then
        self.publicData.vents = {}
    end
    if not self.privateData.pendingTileUpdates then
        self.privateData.pendingTileUpdates = {}
    end
end

function WastelandHVACSystem:getFurnace(x, y, z)
    for _, furnace in ipairs(self.publicData.furnaces) do
        if furnace.x == x and furnace.y == y and furnace.z == z then
            return furnace
        end
    end
    return nil
end

function WastelandHVACSystem:getFurnacesNear(x, y, z)
    local furnaces = {}
    for _, furnace in ipairs(self.publicData.furnaces) do
        local dist = IsoUtils.DistanceTo(x, y, z, furnace.x, furnace.y, furnace.z)
        if dist < 20 then
            table.insert(furnaces, furnace)
        end
    end
    return furnaces
end

function WastelandHVACSystem:getVent(x, y, z)
    for _, vent in ipairs(self.publicData.vents) do
        if vent.x == x and vent.y == y and vent.z == z then
            return vent
        end
    end
    return nil
end

function WastelandHVACSystem:addFurnace(player, x, y, z)
    if isClient() then
        self:sendToServer(player, "addFurnace", x, y, z)
        return
    end
    table.insert(self.publicData.furnaces, {x = x, y = y, z = z, fuel = 0, enabled = false, vents = {}})
    self:savePublicData()
end

function WastelandHVACSystem:addVent(player, x, y, z, roomDef)
    if isClient() then
        self:sendToServer(player, "addVent", x, y, z, roomDef)
        return
    end
    table.insert(self.publicData.vents, {
        x = x, y = y, z = z,
        roomDef = roomDef,
        furnace = nil,
    })
    self:savePublicData()
end

function WastelandHVACSystem:setVentFurnace(player, ventX, ventY, ventZ, furnaceX, furnaceY, furnaceZ)
    if isClient() then
        self:sendToServer(player, "setVentFurnace", ventX, ventY, ventZ, furnaceX, furnaceY, furnaceZ)
        return
    end
    local vent = self:getVent(ventX, ventY, ventZ)
    if not vent then return end
    local furnace = self:getFurnace(furnaceX, furnaceY, furnaceZ)
    if not furnace then return end
    vent.furnace = {furnaceX, furnaceY, furnaceZ}
    table.insert(furnace.vents, {ventX, ventY, ventZ})
    self:savePublicData()
end

function WastelandHVACSystem:removeFurnace(player, x, y, z)
    if isClient() then
        self:sendToServer(player, "removeFurnace", x, y, z)
        return
    end
    local furnace = self:getFurnace(x, y, z)
    if not furnace then return end
    for _, vent in ipairs(furnace.vents) do
        self:removeVentFurnace(player, vent[1], vent[2], vent[3], true)
    end
    for i, f in ipairs(self.publicData.furnaces) do
        if f.x == x and f.y == y and f.z == z then
            table.remove(self.publicData.furnaces, i)
            break
        end
    end
    self:savePublicData()
end

function WastelandHVACSystem:removeVent(player, x, y, z)
    if isClient() then
        self:sendToServer(player, "removeVent", x, y, z)
        return
    end
    local vent = self:getVent(x, y, z)
    if not vent then return end
    if vent.furnace then
        self:removeVentFurnace(player, x, y, z, true)
    end
    for i, f in ipairs(self.publicData.vents) do
        if f.x == x and f.y == y and f.z == z then
            table.remove(self.publicData.vents, i)
            break
        end
    end
    self:savePublicData()
end

function WastelandHVACSystem:removeVentFurnace(player, x, y, z, skipSave)
    if isClient() then
        self:sendToServer(player, "removeVentFurnace", x, y, z)
        return
    end
    local vent = self:getVent(x, y, z)
    if not vent then return end
    local furnace = self:getFurnace(vent.furnace[1], vent.furnace[2], vent.furnace[3])
    if not furnace then return end
    for i, f in ipairs(furnace.vents) do
        if f[1] == x and f[2] == y and f[3] == z then
            table.remove(furnace.vents, i)
            break
        end
    end
    vent.furnace = nil
    if not skipSave then
        self:savePublicData()
    end
end

function WastelandHVACSystem:getBurnRate(furnace)
    local squares = 0
    for _, ventData in ipairs(furnace.vents) do
        local vent = self:getVent(ventData[1], ventData[2], ventData[3])
        squares = squares + #vent.roomDef.squares
    end
    return math.ceil(squares / 50)
end

function WastelandHVACSystem:enableFurnace(player, x, y, z)
    if isClient() then
        self:sendToServer(player, "enableFurnace", x, y, z)
        return
    end
    local furnace = self:getFurnace(x, y, z)
    if not furnace then return end
    furnace.enabled = true
    self:savePublicData()
    table.insert(self.privateData.pendingTileUpdates, {x = x, y = y, z = z, type = "furnance", status = "on"})
    self:savePrivateData()
    self:attemptSpriteReplacements()
end

function WastelandHVACSystem:disableFurnace(player, x, y, z, skipSave)
    if isClient() then
        self:sendToServer(player, "disableFurnace", x, y, z)
        return
    end
    local furnace = self:getFurnace(x, y, z)
    if not furnace then return end
    furnace.enabled = false
    if not skipSave then
        self:savePublicData()
    end
    table.insert(self.privateData.pendingTileUpdates, {x = x, y = y, z = z, type = "furnance", status = "off"})
    self:savePrivateData()
    self:attemptSpriteReplacements()
end

function WastelandHVACSystem:addFuelFurnace(player, x, y, z, amount)
    if isClient() then
        self:sendToServer(player, "addFuelFurnace", x, y, z, amount)
        return
    end
    local furnace = self:getFurnace(x, y, z)
    if not furnace then return end
    furnace.fuel = math.min(furnace.fuel + amount, 300)
    self:savePublicData()
end

function WastelandHVACSystem:getFurnaceObject(square)
    if not square then return end
    local objects = square:getObjects()
    for i = 0, objects:size() - 1 do
        local object = objects:get(i)
        if object:getSprite() and
            (
                object:getSprite():getName() == self.tiles.FurnaceOff or
                object:getSprite():getName() == self.tiles.FurnaceOn or
                object:getSprite():getName() == self.tiles.FurnaceOffNorth or
                object:getSprite():getName() == self.tiles.FurnaceOnNorth
            ) then
            return object
        end
    end
end

function WastelandHVACSystem:getVentObject(square)
    if not square then return end
    local objects = square:getObjects()
    for i = 0, objects:size() - 1 do
        local object = objects:get(i)
        if object:getSprite() and
            (
                object:getSprite():getName() == self.tiles.Vent or
                object:getSprite():getName() == self.tiles.VentNorth
            ) then
            return object
        end
    end
end

function WastelandHVACSystem:attemptSpriteReplacements()
    local updatedIndexes = {}
    for i, update in ipairs(self.privateData.pendingTileUpdates) do
        local square = getCell():getGridSquare(update.x, update.y, update.z)
        if square then
            if update.type == "furnance" then
                local object = self:getFurnaceObject(square)
                if object then
                    if update.status == "on" then
                        if object:getSprite():getName() == self.tiles.FurnaceOff then
                            object:setSprite(getSprite(self.tiles.FurnaceOn))
                        elseif object:getSprite():getName() == self.tiles.FurnaceOffNorth then
                            object:setSprite(getSprite(self.tiles.FurnaceOnNorth))
                        end
                    elseif update.status == "off" then
                        if object:getSprite():getName() == self.tiles.FurnaceOn then
                            object:setSprite(getSprite(self.tiles.FurnaceOff))
                        elseif object:getSprite():getName() == self.tiles.FurnaceOnNorth then
                            object:setSprite(getSprite(self.tiles.FurnaceOffNorth))
                        end
                    end
                    object:transmitUpdatedSpriteToClients()
                end
            end
            table.insert(updatedIndexes, i)
        end
    end
    for i = #updatedIndexes, 1, -1 do
        table.remove(self.privateData.pendingTileUpdates, updatedIndexes[i])
    end
    self:savePrivateData()
end

function WastelandHVACSystem:runDevices()
    if isClient() then return end
    for _, furnace in ipairs(self.publicData.furnaces) do
        if furnace.enabled then
            furnace.fuel = math.max(0, furnace.fuel - self:getBurnRate(furnace))
        end
        if furnace.fuel == 0 then
            self:disableFurnace(nil, furnace.x, furnace.y, furnace.z, true)
        end
    end
    self:attemptSpriteReplacements()
    self:savePublicData()
end

local FLOAT_TEMPERATURE = 4
function WastelandHVACSystem:checkPlayer()
    if isServer() then return end
    local player = getPlayer()
    if not player then return end
    local x, y, z = math.floor(player:getX()), math.floor(player:getY()), math.floor(player:getZ())
    local isInRoom = false
    for _, vent in ipairs(self.publicData.vents) do
        if vent.roomDef and vent.furnace then
            local roomDef = vent.roomDef
            if x >= roomDef.minX and x <= roomDef.maxX and y >= roomDef.minY and y <= roomDef.maxY and z == roomDef.z then
                for _, ventSquare in ipairs(roomDef.squares) do
                    if ventSquare[1] == x and ventSquare[2] == y then
                        local furnace = self:getFurnace(vent.furnace[1], vent.furnace[2], vent.furnace[3])
                        if furnace and furnace.enabled then
                            isInRoom = true
                            break
                        end
                    end
                end
            end
            if isInRoom then break end
        end
    end
    local cm = getClimateManager()
    if isInRoom and cm:getClimateFloat(FLOAT_TEMPERATURE):getInternalValue() <= 20 then
        self.currentState = true
        local cmftemp = cm:getClimateFloat(FLOAT_TEMPERATURE)
        cmftemp:setModdedValue(20)
        cmftemp:setModdedInterpolate(1.0)
        cmftemp:setEnableModded(true)
        cmftemp:setEnableOverride(false)
    elseif self.currentState then
        self.currentState = false
        local cm = getClimateManager()
        local cmftemp = cm:getClimateFloat(FLOAT_TEMPERATURE)
        cmftemp:setEnableModded(false)
        cmftemp:setEnableOverride(true)
    end
end

function WastelandHVACSystem.EveryHours()
    WastelandHVACSystem:runDevices()
end

function WastelandHVACSystem.OnTick()
    WastelandHVACSystem:checkPlayer()
end

if not isClient() then
    WastelandHVACSystem.didBindServerEvents = true
    Events.EveryHours.Add(WastelandHVACSystem.EveryHours)
end

if not isServer() then
    WastelandHVACSystem.didBindClientEvents = true
    Events.OnTick.Add(WastelandHVACSystem.OnTick)
end

--- @class WLHVAC_Position
--- @field x number
--- @field y number

--- @class WLHVAC_RoomDef
--- @field minX number
--- @field minY number
--- @field maxX number
--- @field maxY number
--- @field z number
--- @field squares WLHVAC_Position[]
WLHVAC_RoomDef = WLHVAC_RoomDef or {}

--- @param square IsoGridSquare
--- @return WLHVAC_RoomDef
function WLHVAC_RoomDef:new(square)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.minX = square:getX()
    o.minY = square:getY()
    o.maxX = square:getX()
    o.maxY = square:getY()
    o.z = square:getZ()
    o.squares = {{square:getX(), square:getY()}}
    return o
end

--- @param square IsoGridSquare
function WLHVAC_RoomDef:addSquare(square)
    if square:getX() < self.minX then
        self.minX = square:getX()
    end
    if square:getY() < self.minY then
        self.minY = square:getY()
    end
    if square:getX() > self.maxX then
        self.maxX = square:getX()
    end
    if square:getY() > self.maxY then
        self.maxY = square:getY()
    end
    table.insert(self.squares, {square:getX(), square:getY()})
end

function WLHVAC_RoomDef:getData()
    return {
        minX = self.minX,
        minY = self.minY,
        maxX = self.maxX,
        maxY = self.maxY,
        z = self.z,
        squares = self.squares,
    }
end