WLHVAC_VentBuildObject = ISBuildingObject:derive('WLHVAC_VentBuildObject')

function WLHVAC_VentBuildObject:create(x, y, z, north, sprite)
  local cell = getWorld():getCell()
  self.sq = cell:getGridSquare(x, y, z)
  local room = self:getRoom(self.sq)
  if not room then return end
  self.javaObject = IsoObject.new(getCell(), self.sq, getSprite(sprite))
  self.sq:AddTileObject(self.javaObject)
  buildUtil.consumeMaterial(self)
  self.javaObject:transmitCompleteItemToServer()
  WastelandHVACSystem:addVent(getSpecificPlayer(self.player), x, y, z, room)
end

function WLHVAC_VentBuildObject.isRoom(square)
  local room = square:getRoom()
  if room then
    return true
  end

  local region = square:getIsoWorldRegion()
  if region and region:isEnclosed() and region:isFullyRoofed() then
    return true
  end

  return false
end

local function getSquaresInRegion(startSquare, targetRegion)
    local connectedSquares = {}
    local visited = {}
    local queue = {startSquare}
    local targetRegionID = targetRegion:getID()
    local cell = getCell()

    while #queue > 0 do
        local square = table.remove(queue, 1)
        if not visited[square] then
            visited[square] = true
            local squareRegion = square:getIsoWorldRegion()
            if squareRegion and squareRegion:getID() == targetRegionID then
                table.insert(connectedSquares, square)
                for _, neighbor in ipairs({
                    cell:getGridSquare(square:getX() - 1, square:getY(), square:getZ()), -- West
                    cell:getGridSquare(square:getX() + 1, square:getY(), square:getZ()), -- East
                    cell:getGridSquare(square:getX(), square:getY() - 1, square:getZ()), -- North
                    cell:getGridSquare(square:getX(), square:getY() + 1, square:getZ()), -- South
                }) do
                    if neighbor and not visited[neighbor] then
                        table.insert(queue, neighbor)
                    end
                end
            end
        end
    end

    return connectedSquares
end

function WLHVAC_VentBuildObject:getRoom(square)
  local room = square:getRoom()

  if room then
    local squares = room:getSquares()
    local roomDef = WLHVAC_RoomDef:new(squares:get(0))
    for i=1,squares:size()-1 do
        roomDef:addSquare(squares:get(i))
    end
    return roomDef:getData()
  end

  local region = square:getIsoWorldRegion()
  if region and region:isEnclosed() and region:isFullyRoofed() then
    local squares = getSquaresInRegion(square, region)
    local roomDef = WLHVAC_RoomDef:new(squares[1])
    for i=2,#squares do
        roomDef:addSquare(squares[i])
    end
    return roomDef:getData()
  end

  return nil
end

function WLHVAC_VentBuildObject:isValid(square)
    if not ISBuildingObject.isValid(self, square) then
        return false
    end
    --ensures that the vent is placed against a wall
    local hasNorthWall = false
    local hasWestWall = false
    for i = 0, square:getObjects():size() - 1 do
        local obj = square:getObjects():get(i)
        if self:getSprite() == obj:getTextureName() then
            return false
        end
        if obj:getProperties():Is('WallN') then
            hasNorthWall = true
        end
        if obj:getProperties():Is('WallW') then
            hasWestWall = true
        end
    end
    if self.north and not hasNorthWall then
        return false
    end
    if not self.north and not hasWestWall then
        return false
    end
    if not self.isRoom(square) then
        return false
    end
    return true
end

function WLHVAC_VentBuildObject:render(x, y, z, square)
  ISBuildingObject.render(self, x, y, z, square)
end

function WLHVAC_VentBuildObject:new(player)
  local o = {}
  setmetatable(o, self)
  self.__index = self
  o:init()
  o:setSprite(WastelandHVACSystem.tiles.Vent)
  o:setNorthSprite(WastelandHVACSystem.tiles.VentNorth)
  o.player = player
  o.dismantable = false
  o.name = "Vent"
  o.stopOnWalk = true
  o.stopOnRun = true
  o.maxTime = 100
  o.canBeAlwaysPlaced = true
  o.modData["need:WLHVAC.Vent"] = 1
  o.modData["need:Base.Nails"] = 4
  return o
end
