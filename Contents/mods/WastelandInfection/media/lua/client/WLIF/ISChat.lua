local WHR_System = {}
WHR_System.name = "Infection"
function WHR_System.GetHints()
    local hints = {}
    local player = getPlayer()
    if not player then return hints end
    local username = player:getUsername()
    if WLIF_InfectionSystem:isInfected(username) then
        local amount = WLIF_InfectionSystem:getInfectionAmount(username)
        if amount >= 125 then
            player:Kill(player)
        elseif amount >= 100 then
            table.insert(hints, "Character Succumbed to Infection. You MUST graveyard asap.")
        elseif amount >= 75 then
            table.insert(hints, "Severe Infection! You must RP the following effects:\n   You have sudden outbursts of violence and aggression.\n   You can't trust anyone, even those closest to you.\n   It's only a matter of time until they hurt you. ")
        elseif amount >= 50 then
            table.insert(hints, "Moderate Infection! You must RP the following effects:\n   You're easily irritated.\n   You feel paranoid about strangers and acquaintances.\n   Your muscles feel sore.")
        elseif amount >= 25 then
            table.insert(hints, "Low Infection! There's a slight fever.\n   Mild headaches.\n   You begin to see the worst in people.")
        else
            table.insert(hints, "Slight Infection! No noticeable effects.")
        end
    end
    return hints
end
WRH.RegisterSystem(WHR_System)

local function startInfection(player)
    WL_Dialogs.showConfirmationDialog("Are you sure you want to become infected?\nThis has RP requirements and will likely lead to your charcaters death.\nSee Discord for more information.", function()
        WLIF_InfectionSystem:startInfection(player)
    end)
end

ISChat.WLIF_oGearClick = ISChat.WLIF_oGearClick or ISChat.onGearButtonClick
function ISChat:onGearButtonClick()
    ISChat.WLIF_oGearClick(self)
    local context = getPlayerContextMenu(0)
    if context then
        if not WLIF_InfectionSystem:isInfected(getPlayer():getUsername()) then
            context:addOption("Become Infected", getPlayer(), startInfection)
            return
        end
    end
end