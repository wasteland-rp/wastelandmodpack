function WLIF_InventoryMenu(playerIdx, context, items)
    local player = getSpecificPlayer(playerIdx)

    if not WL_Utils.isAtLeastGM(player) then return end

    local actualItems = ISInventoryPane.getActualItems(items)
    for _, item in ipairs(actualItems) do
        if item:getFullType() == "WLIF.BloodSample" or item:getFullType() == "WLIF.ScanResult" or item:getFullType() == "Base.WLBloodSample" then
            local username = item:getModData().WLIF_Username or "Unknown"
            local amount = item:getModData().WLIF_InfectionLevel and tostring(item:getModData().WLIF_InfectionLevel) or "??"
            context:addOption(username .. " (" .. amount .. "%)")
        end
    end
end

Events.OnPreFillInventoryObjectContextMenu.Add(WLIF_InventoryMenu)