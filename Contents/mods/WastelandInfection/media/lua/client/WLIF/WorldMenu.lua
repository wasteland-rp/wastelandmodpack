function WLIF_WorldMenu(playerIdx, context, worldobjects, test)
    local playerObj = getSpecificPlayer(playerIdx)
    if not WL_Utils.isAtLeastGM(playerObj) then return end

    local nearPlayers = {}
    local square = worldobjects[1]:getSquare()
    local sx, sy, sz = square:getX(), square:getY(), square:getZ()
    for x=sx-1,sx+1 do
        for y=sy-1,sy+1 do
            local sq = getCell():getGridSquare(x, y, sz)
            if sq then
                for i=0,sq:getMovingObjects():size()-1 do
                    local obj = sq:getMovingObjects():get(i)
                    if instanceof(obj, "IsoPlayer") then
                        table.insert(nearPlayers, obj)
                    end
                end
            end
        end
    end

    if #nearPlayers == 0 then return end

    local subMenu = WL_ContextMenuUtils.getOrCreateSubMenu(context, "Infection")
    for _, player in ipairs(nearPlayers) do
        local username = player:getUsername()
        if not WLIF_InfectionSystem:isInfected(username) then
            subMenu:addOption("Infect " .. username, WLIF_InfectionSystem, WLIF_InfectionSystem.startInfection, playerObj, username)
        else
            local playerAddMenu = WL_ContextMenuUtils.getOrCreateSubMenu(subMenu, username .. " (" .. tostring(WLIF_InfectionSystem:getInfectionAmount(username)) .. "%)")
            playerAddMenu:addOption("Remove 50%", WLIF_InfectionSystem, WLIF_InfectionSystem.adminAddInfection, playerObj, username, -50)
            playerAddMenu:addOption("Remove 25%", WLIF_InfectionSystem, WLIF_InfectionSystem.adminAddInfection, playerObj, username, -25)
            playerAddMenu:addOption("Remove 10%", WLIF_InfectionSystem, WLIF_InfectionSystem.adminAddInfection, playerObj, username, -10)
            playerAddMenu:addOption("Remove 5%", WLIF_InfectionSystem, WLIF_InfectionSystem.adminAddInfection, playerObj, username, -5)
            playerAddMenu:addOption("Remove 1%", WLIF_InfectionSystem, WLIF_InfectionSystem.adminAddInfection, playerObj, username, -1)
            playerAddMenu:addOption("Clear", WLIF_InfectionSystem, WLIF_InfectionSystem.removeInfection, playerObj, username)
            playerAddMenu:addOption("Add 1%", WLIF_InfectionSystem, WLIF_InfectionSystem.adminAddInfection, playerObj, username, 1)
            playerAddMenu:addOption("Add 5%", WLIF_InfectionSystem, WLIF_InfectionSystem.adminAddInfection, playerObj, username, 5)
            playerAddMenu:addOption("Add 10%", WLIF_InfectionSystem, WLIF_InfectionSystem.adminAddInfection, playerObj, username, 10)
            playerAddMenu:addOption("Add 25%", WLIF_InfectionSystem, WLIF_InfectionSystem.adminAddInfection, playerObj, username, 25)
            playerAddMenu:addOption("Add 50%", WLIF_InfectionSystem, WLIF_InfectionSystem.adminAddInfection, playerObj, username, 50)
        end
    end
end

Events.OnFillWorldObjectContextMenu.Add(WLIF_WorldMenu)