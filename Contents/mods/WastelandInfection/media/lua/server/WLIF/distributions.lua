require 'Items/ProceduralDistributions'

local function addItemToDist(category, item, chance)
    table.insert(ProceduralDistributions.list[category].items, item)
    table.insert(ProceduralDistributions.list[category].items, chance)
end

-- Removed due to this mod using WLSyringe now instead
