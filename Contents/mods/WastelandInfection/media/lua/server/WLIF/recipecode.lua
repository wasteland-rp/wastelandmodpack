function WLIF_OnEat_InfectionBlocker(item, player)
    if isClient() then return end
    WLIF_InfectionSystem:takeBlocker(player)
end

function WLIF_OnEat_InfectionCure(item, player)
    if isClient() then return end
    WLIF_InfectionSystem:removeInfection(player)
end

function WLIF_OnCreate_SetAge(items, result, player)
    if result then
        result:setAge(0)
    end
end

function WLIF_OnCreate_TakeBloodSample(items, result, player)
    if result then
        local playerUsername = player:getUsername()
        local amount = WLIF_InfectionSystem:getInfectionAmount(playerUsername)
        result:getModData().WLIF_InfectionLevel = amount
        result:getModData().WLIF_Username = playerUsername
        result:setAge(0)
    end
end

local function djb2_hash(str)
    local hash = 5381
    for i = 1, #str do
        local byte = str:byte(i)
        hash = (hash * 33 + byte) % 2^32  -- Simulating 32-bit overflow
    end
    return hash
end

local function to_hex(num)
    local hex = string.format("%X", num)
    -- Ensure the hex string is of fixed length, e.g., 6 characters
    return string.sub(hex, -6)
end

local function blood_fingerprint(input)
    local hash_value = djb2_hash(input)
    return to_hex(hash_value)
end

function WLIF_OnCreate_ScanBloodSample(items, result, player)
    for i=0,items:size() - 1 do
        local item = items:get(i)
        if item:getFullType() == "WLIF.BloodSample" or item:getFullType() == "Base.WLBloodSample" then
            local fingerprint = blood_fingerprint(item:getModData().WLIF_Username)
            result:setName("Scan: " .. fingerprint .. " - " .. tostring(item:getModData().WLIF_InfectionLevel) .. "%")
            result:setCustomName(true)
            result:getModData().WLIF_InfectionLevel = item:getModData().WLIF_InfectionLevel
            result:getModData().WLIF_Username = item:getModData().WLIF_Username
            return
        end
    end
end