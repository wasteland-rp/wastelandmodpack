WLIF_InfectionSystem = WL_ClientServerBase:new("WLIF_InfectionSystem")
WLIF_InfectionSystem.needsPublicData = true

local TIME_TOTAL_INFECTION = 60 * 60 * 24 * 14 -- 14 days
local TIME_TO_EXPIRE = 60 * 60 * 24 * 28 -- 28 days
local TIME_PER_DOSE = TIME_TOTAL_INFECTION / 4

local function ensurePublicData(data)
    if not data.players then
        data.players = {}
    end
end

--- Runs on Server
function WLIF_InfectionSystem:onModDataInit()
    ensurePublicData(self.publicData)
end

-- Runs on Client
function WLIF_InfectionSystem:onPublicDataUpdated()
    ensurePublicData(self.publicData)
end

function WLIF_InfectionSystem:isInfected(username)
    return self.publicData.players[username] ~= nil
end

function WLIF_InfectionSystem:getInfectionAmount(username)
    if not self:isInfected(username) then return 0 end
    return math.min(100, math.floor(((WL_Utils.getTimestamp() - self.publicData.players[username].startedAt)/TIME_TOTAL_INFECTION) * 100))
end

function WLIF_InfectionSystem:startInfection(player, username)
    if isClient() then
        self:sendToServer(player, "startInfection", username)
        return
    end
    username = username or player:getUsername()
    self.publicData.players[username] = {
        lastSeen = WL_Utils.getTimestamp(),
        startedAt =  WL_Utils.getTimestamp()
    }
    self:savePublicData()
    self:doLog(player, username, "started infection")
end

function WLIF_InfectionSystem:takeBlocker(player, username)
    if isClient() then
        self:sendToServer(player, "takeBlocker", username)
        return
    end
    username = username or player:getUsername()
    if not self:isInfected(username) then return end
    local data = self.publicData.players[username]
    data.startedAt = math.min(WL_Utils.getTimestamp(), data.startedAt + TIME_PER_DOSE)
    self:savePublicData()
    self:doLog(player, username, "took blocker")
end

function WLIF_InfectionSystem:removeInfection(player, username)
    if isClient() then
        self:sendToServer(player, "removeInfection", username)
        return
    end
    username = username or player:getUsername()
    self.publicData.players[username] = nil
    self:savePublicData()
    self:doLog(player, username, "removed infection")
end

function WLIF_InfectionSystem:adminAddInfection(player, username, amount)
    if isClient() then
        self:sendToServer(player, "adminAddInfection", username, amount)
        return
    end
    username = username or player:getUsername()
    if not self:isInfected(username) then return end
    local data = self.publicData.players[username]
    data.startedAt = math.min(math.max(data.startedAt - (TIME_TOTAL_INFECTION * amount / 100), WL_Utils.getTimestamp() - TIME_TOTAL_INFECTION), WL_Utils.getTimestamp())
    self:savePublicData()
    self:doLog(player, username, string.format("admin added %s%% infection", amount))
end

function WLIF_InfectionSystem:doLog(player, username, change)
    if isClient() then
        self:sendToServer(player, "doLog", username)
        return
    end
    if player:getUsername() ~= username then
        writeLog("WastelandInfection", string.format("%s %s | Trigger: %s", username, change, player:getUsername()))
    else
        writeLog("WastelandInfection", string.format("%s %s", username, change))
    end
end

if not isClient() then
    Events.EveryHours.Add(function()
        local timestamp = WL_Utils.getTimestamp()
        local onlinePlayers = getOnlinePlayers()
        for i=0, onlinePlayers:size()-1 do
            local player = onlinePlayers:get(i)
            local username = player:getUsername()
            if WLIF_InfectionSystem:isInfected(username) then
                WLIF_InfectionSystem.publicData.players[username].lastSeen = timestamp
            end
        end
        for username, data in pairs(WLIF_InfectionSystem.publicData.players) do
            if timestamp - data.lastSeen > TIME_TO_EXPIRE then
                WLIF_InfectionSystem.publicData.players[username] = nil
            end
        end
    end)
    Events.EveryDays.Add(function()
        local infected = {}
        local dead = {}
        for username, data in pairs(WLIF_InfectionSystem.publicData.players) do
            local infectionAmount = WLIF_InfectionSystem:getInfectionAmount(username)
            if infectionAmount >= 100 then
                table.insert(dead, username)
            elseif infectionAmount > 0 then
                table.insert(infected, string.format("%s: %s%%", username, infectionAmount))
            end
        end
        if #infected > 1 then
            writeLog("WastelandInfection", "Current Infections: " .. table.concat(infected, ", "))
        end
        if #dead > 1 then
            writeLog("WastelandInfection", "Dead: " .. table.concat(dead, ", "))
        end
    end)
end