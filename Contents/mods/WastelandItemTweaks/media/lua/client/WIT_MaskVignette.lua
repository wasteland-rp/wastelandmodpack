---
--- WIT_MaskVignette.lua
--- Does a vignette effect and plays sounds when a mask is put on or taken off
--- 06/10/2024
---

WIT_MaskVignette = {}

local fullMaskTexture = getTexture("media/textures/GUI/MaskVignette.png");

local width;
local height;
local hasMaskOn = false

local function drawMaskTexture()
	if hasMaskOn then
		UIManager.DrawTexture( fullMaskTexture, 0, 0, width, height, 1.0)
	end
end

local function setWidthAndHeight()
	width = getCore():getScreenWidth();
	height = getCore():getScreenHeight();
end

local function resolutionChanged(_ox, _oy, x, y)
	width = x;
	height = y;
end

Events.OnGameBoot.Add(setWidthAndHeight);
Events.OnResolutionChange.Add(resolutionChanged);
Events.OnPreUIDraw.Add(drawMaskTexture);

function WIT_MaskVignette.setMaskOn(_hasMaskOn)
	if _hasMaskOn ~= hasMaskOn then
		hasMaskOn = _hasMaskOn;
		if hasMaskOn then
			getSoundManager():playUISound("GasMaskBreathing")
		else
			getSoundManager():playUISound("ExhaleMaskOff")
		end
	end
end