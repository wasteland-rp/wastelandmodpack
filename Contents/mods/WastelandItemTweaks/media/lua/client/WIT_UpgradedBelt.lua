local SmallBeltFrontLeft = {
	type = "SmallBeltFrontLeft",
	name = "Belt Front Left", -- Name shown in the slot icon
	animset = "belt left",
	attachments = { -- list of possible item category and their modelAttachement group, the item category is defined in the item script
		Knife = "Belt Front Left Upside", -- defined in AttachedLocations.lua
		Hammer = "Belt Front Left",
		HammerRotated = "Belt Rotated Front Left",
		Nightstick = "Nightstick Front Left",
		Screwdriver  = "Belt Front Left Screwdriver",
		Wrench = "Wrench Front Left",
		MeatCleaver = "MeatCleaver Belt Front Left",
		Walkie = "Walkie Belt Front Left",
	},
}
table.insert(ISHotbarAttachDefinition, SmallBeltFrontLeft);

local SmallBeltFrontRight = {
	type = "SmallBeltFrontRight",
	name = "Belt front Right",
	animset = "belt right",
	attachments = {
		Knife = "Belt Front Right Upside",
		Hammer = "Belt Front Right",
		HammerRotated = "Belt Rotated Front Right",
		Nightstick = "Nightstick Front Right",
		Screwdriver  = "Belt Front Right Screwdriver",
		Wrench = "Wrench Front Right",
		MeatCleaver = "MeatCleaver Belt Front Right",
		Walkie = "Walkie Belt Front Right",
	},
}
table.insert(ISHotbarAttachDefinition, SmallBeltFrontRight);

local group = AttachedLocations.getGroup("Human")
group:getOrCreateLocation("Belt Front Right"):setAttachmentName("belt_front_right")
group:getOrCreateLocation("Belt Front Left"):setAttachmentName("belt_front_left")
group:getOrCreateLocation("Belt Front Right Upside"):setAttachmentName("belt_front_right_upside")
group:getOrCreateLocation("Belt Front Left Upside"):setAttachmentName("belt_front_left_upside")
group:getOrCreateLocation("Walkie Belt Front Right"):setAttachmentName("walkie_belt_front_right")
group:getOrCreateLocation("Walkie Belt Front Left"):setAttachmentName("walkie_belt_front_left")
group:getOrCreateLocation("MeatCleaver Belt Front Right"):setAttachmentName("meatcleaver_front_right")
group:getOrCreateLocation("MeatCleaver Belt Front Left"):setAttachmentName("meatcleaver_front_left")
group:getOrCreateLocation("Belt Rotated Front Right"):setAttachmentName("belt_rotated_front_right")
group:getOrCreateLocation("Belt Rotated Front Left"):setAttachmentName("belt_rotated_front_left")
group:getOrCreateLocation("Nightstick Front Right"):setAttachmentName("nightstick_front_right")
group:getOrCreateLocation("Nightstick Front Left"):setAttachmentName("nightstick_front_left")
group:getOrCreateLocation("Belt Front Right Screwdriver"):setAttachmentName("belt_front_right_screwdriver")
group:getOrCreateLocation("Belt Front Left Screwdriver"):setAttachmentName("belt_front_left_screwdriver")
group:getOrCreateLocation("Wrench Front Right"):setAttachmentName("wrench_front_right")
group:getOrCreateLocation("Wrench Front Left"):setAttachmentName("wrench_front_left")