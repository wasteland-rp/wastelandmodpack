local ToDisable = {};

local function doDisable()
    for i=0, getAllRecipes():size()-1 do
        local recipe = getAllRecipes():get(i);
        if recipe ~= nil then
            for _,v in pairs(ToDisable) do
                if recipe:getName() == v then
                    recipe:setNeedToBeLearn(true);
                    recipe:setIsHidden(true);
                    recipe:setLuaTest("WIT_recipes.OnTest_False");
                end
            end
        end
    end
end

local function DisableRecipe(recipe)
    table.insert(ToDisable, recipe);
end
Events.OnGameBoot.Add(doDisable);

if not getActivatedMods():contains("MoreBrews") then
    DisableRecipe("Exchange for Alternate Currency")
end

-- Disables Churn Butter 

if getActivatedMods():contains("sapphcooking") then
    DisableRecipe("Churn Butter")
end

-- Disables Spinning Thread and Spinning yarn in Favour of our own.

if getActivatedMods():contains("SGarden-Homestead") then
    DisableRecipe("Spin Thread")
end

if getActivatedMods():contains("SGarden-Homestead") then
    DisableRecipe("Spin Yarn")
end


