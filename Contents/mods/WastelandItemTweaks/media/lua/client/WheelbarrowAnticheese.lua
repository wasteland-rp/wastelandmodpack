if not getActivatedMods():contains("WheelbarrowUndeadRelent") then return end

require "TimedActions/PFGPushAction"

local original_ISUnequipAction_perform = ISUnequipAction.perform
function ISUnequipAction:perform()
    if self.item:getFullType() == "Wheelbarrow.HCWoodenwheelbarrow" and not self.item:getInventory():isEmpty() then
        ISTimedActionQueue.addAfter(self, ISDropItemAction:new(self.character, self.item, 1))
    end
    original_ISUnequipAction_perform(self)
end

local original_ISInventoryTransferAction_isValid = ISInventoryTransferAction.isValid
function ISInventoryTransferAction:isValid()
    local val = original_ISInventoryTransferAction_isValid(self)
    if not val then return false end
    if self.item:getFullType() == "Wheelbarrow.HCWoodenwheelbarrow" and not self.item:getInventory():isEmpty() and not self.destContainer:getType()=="floor" then
        return false
    end
    return val
end

local original_ISGrabItemAction_isValid = ISGrabItemAction.isValid
function ISGrabItemAction:isValid()
    local val = original_ISGrabItemAction_isValid(self)
    if not val then return false end
    if self.item and self.item:getItem():getFullType() == "Wheelbarrow.HCWoodenwheelbarrow" and not self.item:getItem():getInventory():isEmpty() then
        return false
    end
    return val
end

local original_ISEquipHeavyItem_isValid = ISEquipHeavyItem.isValid
function ISEquipHeavyItem:isValid()
    if self.item:getFullType() == "Wheelbarrow.HCWoodenwheelbarrow" and not self.item:getInventory():isEmpty() then
        return false
    end
    return original_ISEquipHeavyItem_isValid(self)
end

function PFGPushAction:isValid()
	for i,v in ipairs(PFGMenu.typesTable) do
		if self.character:getInventory():contains(tostring(v)) then
			return false
		end
	end

    if not self.sq then return false end

    local objects = self.sq:getWorldObjects()
    for i=0, objects:size()-1 do
        local object = objects:get(i)
        if object == self.item then
            return true
        end
    end

    return false
end

local original_PFGPushAction_new = PFGPushAction.new
function PFGPushAction:new( item, player)
    local o = original_PFGPushAction_new(self, item, player)
    o.sq = item:getSquare()
    return o
end

local original_isForceDropHeavyItem = isForceDropHeavyItem
function isForceDropHeavyItem(item)
    return original_isForceDropHeavyItem(item) or (item ~= nil and item:getFullType() == "Wheelbarrow.HCWoodenwheelbarrow" and not item:getInventory():isEmpty())
end