---
--- WIT_OverrideRecipe.lua
--- 14/04/2024
---
require 'recipecode'

local cutAnimalsRecipe = Recipe.OnCreate.CutAnimal

function Recipe.OnCreate.CutAnimal(items, result, player)
	cutAnimalsRecipe(items, result, player)
	local foundBirdie
	for i = 0, items:size() - 1 do
		local item = items:get(i)
		if item:getFullType() == "Base.DeadBird" then
			foundBirdie = true
			break
		end
	end

	if not foundBirdie then return end

	local feathers = ZombRand(13, 22)
	for i = 1, feathers do
		player:getInventory():AddItem("Base.WLFeather")
	end
end

--[[ This would work but Food Preservation Plus fucks with the recipe (removes it and adds it's own one)
WIT_OverrideRecipes = {}

function WIT_OverrideRecipes.ButcherBird(items, result, player)
	Recipe.OnCreate.CutAnimal(items, result, player)

	local feathers = ZombRand(13, 22)
	for i = 1, feathers do
		player:getInventory():AddItem("Base.WLFeather")
	end
end

local function setLuaOnCreate(recipeName, luaOnCreate)
	for i=0, getAllRecipes():size()-1 do
		local recipe = getAllRecipes():get(i);
		if recipe ~= nil and recipe:getName() == recipeName then
			recipe:setLuaCreate(luaOnCreate)
		end
	end
end

setLuaOnCreate("Butcher Bird", "WIT_OverrideRecipes.ButcherBird")
--]]

