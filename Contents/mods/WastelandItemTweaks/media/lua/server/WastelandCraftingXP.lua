---
--- WastelandCraftingXP.lua
---
--- For XP Reference: A wooden spear takes 1 plank, 100 time and gives 5 Carpentry XP
---
--- 16/06/2023
---
function Recipe.OnGiveXP.Woodwork10(recipe, ingredients, result, player)
	player:getXp():AddXP(Perks.Woodwork, 10);
end

function Recipe.OnGiveXP.Woodwork15(recipe, ingredients, result, player)
	player:getXp():AddXP(Perks.Woodwork, 15);
end

function Recipe.OnGiveXP.Woodwork20(recipe, ingredients, result, player)
	player:getXp():AddXP(Perks.Woodwork, 20);
end

function Recipe.OnGiveXP.Woodwork30(recipe, ingredients, result, player)
	player:getXp():AddXP(Perks.Woodwork, 30);
end

function Recipe.OnGiveXP.Tailoring5(recipe, ingredients, result, player)
	player:getXp():AddXP(Perks.Tailoring, 5);
end

Give10WoodworkXP = Recipe.OnGiveXP.Woodwork10
Give15WoodworkXP = Recipe.OnGiveXP.Woodwork15
Give20WoodworkXP = Recipe.OnGiveXP.Woodwork20
Give30WoodworkXP = Recipe.OnGiveXP.Woodwork30


--Tailoring functions

function Recipe.OnGiveXP.Tailoring1(recipe, ingredients, result, player)
	player:getXp():AddXP(Perks.Tailoring, 1);
end

function Recipe.OnGiveXP.Tailoring2(recipe, ingredients, result, player)
	player:getXp():AddXP(Perks.Tailoring, 2);
end

function Recipe.OnGiveXP.Tailoring4(recipe, ingredients, result, player)
	player:getXp():AddXP(Perks.Tailoring, 4);
end

function Recipe.OnGiveXP.Tailoring5(recipe, ingredients, result, player)
	player:getXp():AddXP(Perks.Tailoring, 5);
end

function Recipe.OnGiveXP.Tailoring7(recipe, ingredients, result, player)
	player:getXp():AddXP(Perks.Tailoring, 7);
end

function Recipe.OnGiveXP.Tailoring10(recipe, ingredients, result, player)
	player:getXp():AddXP(Perks.Tailoring, 10);
end

-- Tailoring Call on threads.
Get1TailoringXP = Recipe.OnGiveXP.Tailoring1
Get2TailoringXP = Recipe.OnGiveXP.Tailoring2
Get4TailoringXP = Recipe.OnGiveXP.Tailoring4
Get5TailoringXP = Recipe.OnGiveXP.Tailoring5
Get7TailoringXP = Recipe.OnGiveXP.Tailoring7
Get10TailoringXP = Recipe.OnGiveXP.Tailoring10