RecMedia = RecMedia or {}

RecMedia["c2d1a7e8-35af-44e5-a639-fd88b7200f01"] = {
    itemDisplayName = "RM_c2d1a7e8-35af-44e5-a639-fd88b7200f01",
    title = "RM_c2d1a7e8-35af-44e5-a639-fd88b7200f01",
    subtitle = nil,
    author = "Peter Jackson",
    extra = nil,
    spawning = 0,
    category = "Retail-VHS",
    lines = {
        { text = "[Gandalf arrives in the Shire.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "A wizard is never late, Frodo Baggins.", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-1" },
        { text = "He arrives precisely when he means to.", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-1" },
        { text = "[Bilbo at his birthday party.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "I regret to announce, this is the end! I'm going now.", r = 0.2, g = 0.6, b = 0.4, codes = "BOR-1" },
        { text = "[Bilbo prepares to leave the Shire.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "I think I'm quite ready for another adventure.", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-1" },
        { text = "[Frodo inherits the Ring.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "Keep it secret, keep it safe.", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-1" },
        { text = "[The Ringwraiths search for the Ring.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "They are the Nazgul. Neither living nor dead.", r = 0.2, g = 0.6, b = 0.4, codes = "BOR-1" },
        { text = "They will never stop hunting you.", r = 0.6, g = 0.2, b = 0.4, codes = "STS+1" },
        { text = "[At the Council of Elrond.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "One does not simply walk into Mordor.", r = 0.2, g = 0.6, b = 0.4, codes = "BOR-1" },
        { text = "I will take the Ring, though I do not know the way.", r = 0.2, g = 0.6, b = 0.4, codes = "BOR-1" },
        { text = "[The Fellowship sets out from Rivendell.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "[Boromir struggles with temptation.]", r = 0.0, g = 0.7, b = 0.3, codes = "STS+1" },
        { text = "It is a strange fate that we should suffer so much fear and doubt over so small a thing.", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-1" },
        { text = "[The Fellowship continues through Moria.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "I wish the Ring had never come to me.", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-1" },
        { text = "So do all who live to see such times. But that is not for them to decide.", r = 0.2, g = 0.6, b = 0.4, codes = "BOR-1" },
        { text = "All we have to decide is what to do with the time that is given to us.", r = 0.2, g = 0.6, b = 0.4, codes = "BOR-1" },
        { text = "[Pippin drops a bucket, and a fight with Orcs and a Troll takes place!]", r = 0.0, g = 0.7, b = 0.3, codes = "STS+1" },
        { text = "[Frodo is stabbed by the troll with a spear, and falls!]", r = 0.0, g = 0.7, b = 0.3, codes = "STS+1" },
        { text = "[The Fellowship defeats the troll and Frodo survives by his Mithril shirt!]", r = 0.0, g = 0.7, b = 0.3, codes = "STS-2" },
        { text = "[A Running Battle in Moria occurs and then .. the Balrog comes!]", r = 0.0, g = 0.7, b = 0.3, codes = "STS+1" },
        { text = "This is beyond any of you. Run!", r = 0.6, g = 0.2, b = 0.4, codes = "PAN+1" },
        { text = "[Through a harrowing hail of arrows and falling starways, the Fellowship flees!]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "[Gandalf stands alone at the Bridge of Khazad Dum!]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "You cannot pass! I am a servant of the Secret Fire, wielder of the flame of Anor!", r = 0.6, g = 0.2, b = 0.4, codes = "PAN+1" },
        { text = "[The Balrog howls in anger as its fiery whip is brandished from its shadow and strikes at Gandalf]", r = 0.6, g = 0.2, b = 0.4, codes = "PAN+1" },
        { text = "[Gandalf absorbs the strike and defies its might!]", r = 0.6, g = 0.2, b = 0.4, codes = "PAN+3" },
        { text = "YOU! SHALL NOT! PASS!", r = 0.6, g = 0.2, b = 0.4, codes = "PAN+3" },
        { text = "[Gandalf strikes the bridge and the Balrog of Morgoth falls, but so does Gandalf!]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+3" },
        { text = "[Gandalf falls into the abyss.]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+1" },
        { text = "Fly, you fools!", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-1" },
        { text = "[Aragorn leads the Fellowship after Gandalf's fall.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "[The Fellowship arrives in Lothlorien.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "[Frodo offers the ring to Galadriel, Lady of Light, and she refuses the temptation.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "I pass the test, I will diminish, and go into the West, and remain Galadriel.", r = 0.2, g = 0.6, b = 0.4, codes = "BOR-1" },
        { text = "Even the smallest person can change the course of the future.", r = 0.2, g = 0.6, b = 0.4, codes = "BOR-1" },
        { text = "[The Fellowship leave Lothlorien with gifts, Gimli with three strands of Galadriel's hair, a kingly gift.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "[Boromir tries to take the ring but Frodo gets away!]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+1" },
        { text = "What have I done? Frodo! Please! I'm Sorry!", r = 0.2, g = 0.6, b = 0.4, codes = "PAN+1" },
        { text = "[The Uruk-hai of Isengard guard and the Fellowship is broken!]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+1" },
        { text = "[Boromir falls trying to save Merry and Pippin, and begins to die in front of Aragorn.]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN-1" },
        { text = "I would have followed you, my brother, my captain, my king.", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-1" },
        { text = "[Frodo leaves the Fellowship.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "[Sam follows Frodo across the river.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "I made a promise, Mr. Frodo. 'Don't you leave him, Samwise Gamgee.'", r = 0.6, g = 0.2, b = 0.4, codes = "PAN-1" },
        { text = "And I don't mean to.", r = 0.2, g = 0.6, b = 0.4, codes = "BOR-1" },
        { text = "[Frodo and Sam continue toward Mordor.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "[Aragorn, Gimli and Legolas honor Boromir, with Aragorn turning to the Three Hunters.]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN-1" },
        { text = "Let's hunt some Orc.", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-1" },
    },
};

RecMedia["d2f4b7d9-42c1-49b2-a29a-1ad08c120f01"] = {
    itemDisplayName = "RM_d2f4b7d9-42c1-49b2-a29a-1ad08c120f01",
    title = "RM_d2f4b7d9-42c1-49b2-a29a-1ad08c120f01",
    subtitle = nil,
    author = "Peter Jackson",
    extra = nil,
    spawning = 0,
    category = "Retail-VHS",
    lines = {
        { text = "[Gollum leads Frodo and Sam.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "We swears, to do what you wants. We swears on the Precious.", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-1" },
        { text = "You know the way to Mordor?", r = 0.2, g = 0.6, b = 0.4, codes = "BOR-1" },
        { text = "[Frodo and Sam debate Gollum's trust.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "[The Three Hunters pursue the Uruk-hai.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "They're taking the hobbits to Isengard!", r = 0.6, g = 0.2, b = 0.4, codes = "STS+1" },
        { text = "[Aragorn meets Eomer and his riders.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "What business does an elf, a man, and a dwarf have in the Riddermark?", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-1" },
        { text = "I am Aragorn, son of Arathorn. This is Gimli, son of Gloin, and Legolas, of the woodland realm.", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-1" },
        { text = "We are friends of Rohan, and of Theoden, your king.", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-1" },
        { text = "[Gollum catches rabbits.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "They are Tender they are nice!", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-1" },
        { text = "[Sam makes a stew out of the Rabbits.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "What we needs is a few good taters!", r = 0.6, g = 0.2, b = 0.4, codes = "COO+1" },
        { text = "What's taters Precious?", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-1" },
        { text = "PO-TA-TOES! Boil em, Mash em, Stick em in a stew!", r = 0.6, g = 0.2, b = 0.4, codes = "COO+3" },
        { text = "[Saruman watches from Orthanc.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "The world is changing. Who now has the strength to stand against the armies of Isengard?", r = 0.6, g = 0.2, b = 0.4, codes = "STS+1" },
        { text = "[Saruman shows Grima the army of Uruk-Hai.]", r = 0.0, g = 0.7, b = 0.3, codes = "STS+3" },
        { text = "[Faramir captures Frodo and Sam.]", r = 0.0, g = 0.7, b = 0.3, codes = "STS+1" },
        { text = "The Ring will go to Gondor.", r = 0.2, g = 0.6, b = 0.4, codes = "STS+1" },
        { text = "[The Ents hold council in Fangorn with Merry and Pippin.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "We have decided. You are not Orcs.", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-3" },
        { text = "[Helm's Deep is under siege, the ten thousand Uruk-Hai attack in a dark storm!]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+3" },
        { text = "So it begins.", r = 0.6, g = 0.2, b = 0.4, codes = "STS+1" },
        { text = "[Gimli and Legolas start keeping score of their kills.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "Legolas! Two Already!", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-1" },
        { text = "I'm on Seventeen!", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-1" },
        { text = "WHA?!? I'll have no Pointy Ear Outscoring me!", r = 0.6, g = 0.2, b = 0.4, codes = "PAN-5" },
        { text = "[The Deepening Wall is breached by explosives with a cataclysmic BOOM and THUNDER!]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+3" },
        { text = "Aragorn! Fall back to the Keep! Get your Men out of there!", r = 0.6, g = 0.2, b = 0.4, codes = "STS+1" },
        { text = "[The Hornburg is about to be breached. Theoden and Aragorn speak as the gates are attacked.]", r = 0.0, g = 0.7, b = 0.3, codes = "STS+1" },
        { text = "So much Death . . . What can men do against such reckless hate?", r = 0.6, g = 0.2, b = 0.4, codes = "STS+1" },
        { text = "Ride out with me. Ride out and meet them.", r = 0.6, g = 0.2, b = 0.4, codes = "STS-1" },
        { text = "For Death and Glory?", r = 0.6, g = 0.2, b = 0.4, codes = "STS-1" },
        { text = "For Rohan, for your people.", r = 0.6, g = 0.2, b = 0.4, codes = "STS-1" },
        { text = "Yes ... Yes. The horn of Helm Hammerhand shall sound in the deep! One last time!", r = 0.6, g = 0.2, b = 0.4, codes = "STS+1" },
        { text = "Let this hour, that we draw swords together!", r = 0.2, g = 0.6, b = 0.4, codes = "PAN-1" },
        { text = "Fell deeds awake. Now for wrath, now for ruin, and the Red Dawn!", r = 0.2, g = 0.6, b = 0.4, codes = "PAN-2" },
        { text = "[The gates of the Hornburg fall to the Uruk-Hai's battering ram!]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+4" },
        { text = "[The King and the last of his guard with Aragorn charge out of the gates!]", r = 0.0, g = 0.7, b = 0.3, codes = "STS-4" },
        { text = "Forth, Eorlingas!", r = 0.6, g = 0.2, b = 0.4, codes = "PAN-5" },
        { text = "[Gandalf arrives with the Rohirrim, and the Uruk-hai route!]", r = 0.0, g = 0.7, b = 0.3, codes = "STS-5" },
        { text = "[The battle at Helm's Deep ends.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-5" },
        { text = "Victory! We have victory!", r = 0.6, g = 0.2, b = 0.4, codes = "STS-5" },
        { text = "The battle for Helm's Deep is over; the battle for Middle-earth is about to begin.", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-1" },
        { text = "[The Ents and Merry and Pippin march on Isengard.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "Break the dam. Release the river!", r = 0.2, g = 0.6, b = 0.4, codes = "BOR-1" },
        { text = "[Treebeard guides the ents in their fury to break the dam!]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "[Saruman watches from Isengard helpless as Isengard is flooded.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "[In Osgiliath, Frodo is almost taken by a winged Nazgul]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+10" },
        { text = "[Sam comforts Frodo after he saves Frodo.]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN-1" },
        { text = "I can't do this Sam.", r = 0.6, g = 0.2, b = 0.4, codes = "PAN-1" },
        { text = "I know. It's all wrong. By rights we shouldn't even be here. But we are.", r = 0.6, g = 0.2, b = 0.4, codes = "PAN-1" },
        { text = "It's like in the great stories, Mr. Frodo. The ones that really mattered.", r = 0.6, g = 0.2, b = 0.4, codes = "PAN-1" },
        { text = "How could the world go back to the way it was when so much bad had happened?", r = 0.6, g = 0.2, b = 0.4, codes = "PAN-1" },
        { text = "But in the end, it's only a passing thing, this shadow. Even darkness must pass.", r = 0.6, g = 0.2, b = 0.4, codes = "PAN-1" },
        { text = "Those were the stories that stayed with you. That meant something, even if you were too small to understand why.", r = 0.6, g = 0.2, b = 0.4, codes = "PAN-1" },
        { text = "Folk in those stories had lots of chances of turning back, only they didn't.", r = 0.6, g = 0.2, b = 0.4, codes = "PAN-1" },
        { text = "They kept going, because they were holding on to something.", r = 0.6, g = 0.2, b = 0.4, codes = "PAN-1" },
        { text = "What are we holding onto Sam?", r = 0.6, g = 0.2, b = 0.4, codes = "STR-1" },
        { text = "There is some good in this world, Mr. Frodo, and it's worth fighting for.", r = 0.2, g = 0.6, b = 0.4, codes = "BOR-5 PAN-5 STS-5" },
        { text = "[Frodo debates with Faramir.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "I think at last, we understand one another, Frodo Baggins.", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-1" },
        { text = "[One of Faramir's officers speaks up]", r = 0.0, g = 0.7, b = 0.3, codes = "STS+1" },
        { text = "You know the laws of our country, the laws of your father. If you let them go your life will be forfeit.", r = 0.6, g = 0.2, b = 0.4, codes = "STS+1" },
        { text = "Then it is forfeit. Release them.", r = 0.6, g = 0.2, b = 0.4, codes = "STS-2" },
        { text = "[Gollum plots his revenge.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "They stole it from us. Sneaky little hobbitses.", r = 0.6, g = 0.2, b = 0.4, codes = "STS+1" },
        { text = "[Gollum speaks to himself.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "We could let her do it ... This way Hobbitses!", r = 0.6, g = 0.2, b = 0.4, codes = "STS+1" },
    },
};

RecMedia["e3b6a7c3-43d5-4a28-bf3a-3ab9e9b3b7c7"] = {
    itemDisplayName = "RM_e3b6a7c3-43d5-4a28-bf3a-3ab9e9b3b7c7",
    title = "RM_e3b6a7c3-43d5-4a28-bf3a-3ab9e9b3b7c7",
    subtitle = nil,
    author = "Peter Jackson",
    extra = nil,
    spawning = 0,
    category = "Retail-VHS",
    lines = {
        { text = "[Gandalf, Theoden, Aragorn, Legolas, and Gimli ride into the ruins of Isengard through Fanghorn forest, towards laughter.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "[Pippin and Merry relax on the walls of the ruined Numenorean fort, smoking.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "I feel like I am back at the Green Dragon. A mug in my hand, putting my feet up on a settle after a hard day's work.", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-1" },
        { text = "Only, you've never done a hard day's work.", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-1" },
        { text = "[Merry and Pippin share a laugh and chuckle as they smoke and munch on salted pork, before warmly welcoming Gandalf and company.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "Welcome, my lords! To Isengard!", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-1" },
        { text = "[Gimli then calls out to them from the horse of Legolas.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "You young rascals! A merry hunt you've led us on, and now we find ya feasting . . . and smoking!", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-1" },
        { text = "We are sitting on a field of victory, enjoying a few well-earned comforts. The salted pork is -particularly- good.", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-1" },
        { text = "[Gimli then pauses and merely repeats two words.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "Salted pork?", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-1" },
        { text = "[Gandalf sighs and shakes his head.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "Hobbits . . .", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-1" },
        { text = "[After meeting with Treebeard, they speak to the traitor Saruman, who is killed by Grima. Legolas shoots Grima, and Saruman falls to his death.]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+5" },
        { text = "[Treebeard the Ent shakes his head.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "The filth of Saruman is washing away . . .", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-1" },
        { text = "[Pippin sees a Palantir, a lost seeing stone in the waters of Isengard. Gandalf quickly takes it from Pippin before he can be affected.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "[The victors of Helm's Deep hold a feast to celebrate their victory. King Theoden raises a toast to all present.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "Tonight we remember those who gave their blood to defend this country. Hail the Victorious Dead!", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-1" },
        { text = "[The hall hails their king, and merriment commences throughout the halls, with calls for 'Victory' and then drinking games, in which Gimli and Legolas partake.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "[Gimli may have won the competition for the most kills at Helm's Deep, but he falters and stumbles over drunk as Legolas remains standing.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "Game over.", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-1" },
        { text = "[Pippin sneaks a look into the Palantir as Gandalf sleeps after the feast, and the Eye of Sauron sees him. Aragorn briefly holds it too before Gandalf hides it.]", r = 0.0, g = 0.7, b = 0.3, codes = "STS+10" },
        { text = "[Pippin and Merry are separated for the first time, as Merry stays in Rohan and Pippin, now sought by the Great Enemy, is taken by Gandalf on Shadowfax to Minas Tirith.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "[Gandalf and Pippin journey through dark forests and shallow streams, while white mountains tower in the background.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "We just passed into the realm of Gondor!", r = 0.2, g = 0.6, b = 0.4, codes = "BOR-1" },
        { text = "[Finally, they behold the ringed capital city of Gondor, with its White Spire jutting from the center like a white cliff.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "Minas Tirith, city of Kings.", r = 0.2, g = 0.6, b = 0.4, codes = "BOR-1" },
        { text = "[The duo race up the cobbled streets of the seven rings of Minas Tirith, reaching the top of the city to meet the Steward of Gondor, Denethor.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "[Gandalf and Pippin enter the great hall of Denethor.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "[Denethor, father of Boromir, holds his eldest son's broken horn, cleaved in half, in grief. Pippin offers his servitude, while Gandalf tries to focus on the matter at hand.]", r = 0.0, g = 0.7, b = 0.3, codes = "STS+1" },
        { text = "[Pippin pledges his loyalty to Gondor as repayment, kneeling before Denethor.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "You are here to take my son's place. To guard my city.", r = 0.6, g = 0.2, b = 0.4, codes = "STS+1" },
        { text = "[Gandalf tries to focus on the matter at hand.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "My lord, there will be a time to grieve for Boromir, but it is not now. War is coming; the enemy is on your doorstep. Where are Gondor's armies?", r = 0.2, g = 0.6, b = 0.4, codes = "PAN+1" },
        { text = "You still have friends; you are not alone in this fight. Send word to Theoden of Rohan; light the beacons.", r = 0.2, g = 0.6, b = 0.4, codes = "STS+2" },
        { text = "[Denethor glares at Gandalf and responds coldly.]", r = 0.0, g = 0.7, b = 0.3, codes = "STS+2" },
        { text = "You think you are wise, Mithrandir? Yet for all your subtleties, you have not wisdom.", r = 0.2, g = 0.6, b = 0.4, codes = "PAN+2" },
        { text = "With your left hand, you would use me as a shield against Mordor, and with your right, you seek to supplant me. I know who rides with Theoden of Rohan.", r = 0.2, g = 0.6, b = 0.4, codes = "PAN+2" },
        { text = "Oh yes, word has reached my ears of this Aragorn, son of Arathorn. And I will tell you now: I will -not- bow to this Ranger from the North!", r = 0.2, g = 0.6, b = 0.4, codes = "STS+3" },
        { text = "[Gandalf then speaks firmly.]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+2" },
        { text = "Authority is not given to you to deny the Return of the King, Steward of Gondor.", r = 0.6, g = 0.2, b = 0.4, codes = "PAN+2" },
        { text = "[Denethor glares coldly.]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+2" },
        { text = "The rule of Gondor is mine! And NO others!", r = 0.2, g = 0.6, b = 0.4, codes = "STS+4" },
        { text = "[Gandalf leaves with Pippin in disgust.]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+2" },
        { text = "All has turned to vain ambition; he would even use his grief as a cloak!", r = 0.2, g = 0.6, b = 0.4, codes = "BOR-1" },
        { text = "[Gandalf helps Pippin to light the beacons of Minas Tirith, setting off a chorus as the beacons are lit across all of Gondor.]", r = 0.0, g = 0.7, b = 0.3, codes = "STS-5" },
        { text = "Hope is kindled . . .", r = 0.2, g = 0.6, b = 0.4, codes = "BOR-5" },
        { text = "[Aragorn, Legolas, and Gimli rally with King Theoden upon seeing the beacons lit. Elrond approaches.]", r = 0.0, g = 0.7, b = 0.3, codes = "STS+1" },
        { text = "The beacons are lit. Gondor calls for aid.", r = 0.2, g = 0.6, b = 0.4, codes = "STS-1" },
        { text = "And Rohan will answer! Muster the Rohirrim!", r = 0.2, g = 0.6, b = 0.4, codes = "BOR-1" },
        { text = "[Elrond arrives and offers Aragorn Andúril, Flame of the West, reforged from the shards of Narsil.]", r = 0.0, g = 0.7, b = 0.3, codes = "STS+1" },
        { text = "Become the man you were born to be.", r = 0.6, g = 0.2, b = 0.4, codes = "STS+1" },
        { text = "Become the man who you were born to be.", r = 0.6, g = 0.2, b = 0.4, codes = "STS+1" },
        { text = "[Pippin and Gandalf watch as the orcs march towards Minas Tirith.]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+2" },
        { text = "It's so quiet ...", r = 0.6, g = 0.2, b = 0.4, codes = "PAN+2" },
        { text = "It's the deep breath before the plunge", r = 0.6, g = 0.2, b = 0.4, codes = "PAN+2" },
        { text = "Is there any hope, Gandalf? For Frodo and Sam?", r = 0.2, g = 0.6, b = 0.4, codes = "BOR-1" },
        { text = "There never was much hope, just a fool's hope.", r = 0.2, g = 0.6, b = 0.4, codes = "BOR-1" },
        { text = "[The Siege begins, with heads of slain Gondorians flung into the city, followed by large stones! Denethor, seeing his fallen son Faramir, is brought low by Nazgul shrieks!]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+2" },
        { text = "Abandon your posts! FLEE! FLEE FOR YOUR LIVES!", r = 0.6, g = 0.2, b = 0.4, codes = "PAN+2" },
        { text = "[The people of Minas Tirith panic as soldiers begin to flee. Denethor staggers at the sight of Mordor's hosts and quivers in fear.]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+3" },
        { text = "[Gandalf promptly confronts the Steward of Gondor, knocking him out and rallying the soldiers of Minas Tirith!]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "Prepare for battle! Courage is the best defense that you have now.", r = 0.6, g = 0.2, b = 0.4, codes = "PAN+1" },
        { text = "[The first assault on the city begins as orcs launch stones and fire.]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+3" },
        { text = "Hold them back! Do not give in to fear!", r = 0.6, g = 0.2, b = 0.4, codes = "PAN+2" },
        { text = "[The battering ram Grond is brought forth to breach the city gate.]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+3" },
        { text = "Grond! Grond!", r = 0.6, g = 0.2, b = 0.4, codes = "PAN+3" },
        { text = "[The gate falls, and the city begins to crumble!]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+3" },
        { text = "[Gandalf and Pippin fall back with the defenders to one of the upper levels as the lower city of Minas Tirith burns.]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+3" },
        { text = "I didn't think it would end this way.", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-3" },
        { text = "End? No, the journey doesn't end here. Death is just another path, one that we all must take.", r = 0.6, g = 0.2, b = 0.4, codes = "PAN-3" },
        { text = "The grey rain-curtain of this world rolls back, and all turns to silver glass, and then you see it.", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-3" },
        { text = "What? Gandalf? See what?", r = 0.6, g = 0.2, b = 0.4, codes = "PAN-3" },
        { text = "White shores, and beyond . . . a far green country, under a swift sunrise.", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-3" },
        { text = "Well . . . that isn't so bad.", r = 0.6, g = 0.2, b = 0.4, codes = "PAN-3" },
        { text = "No . . . No, it isn't.", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-3" },
        { text = "[The gate buckles from a troll's hammer, and the defenders make ready for what comes.]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+3" },
        { text = "[But then at dawn, the Rohirrim arrive at Minas Tirith at dawn, horns blaring across the plains with Theoden at its head as he gives commands!]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+3" },
        { text = "Arise! Arise, riders of Theoden!", r = 0.6, g = 0.2, b = 0.4, codes = "PAN+3" },
        { text = "Spears shall be shaken! Shields shall be splintered!", r = 0.6, g = 0.2, b = 0.4, codes = "PAN+3" },
        { text = "A Sword Day! A Red Day! Ere the Sun Rises!", r = 0.6, g = 0.2, b = 0.4, codes = "PAN+3" },
        { text = "[Merry, hidden with Eowyn, clings to her for the coming storm.]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+3" },
        { text = "Whatever happens, stay with me. I'll look after you.", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-3" },
        { text = "[The Riders lower their lances, and Theoden, now like his forebears, rides on Snowmane, clanking his sword along their spears.]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+3" },
        { text = "RIDE NOW! RIDE NOW! RIIIDE! RIDE FOR RUIN! AND THE WORLD'S ENDING!", r = 0.6, g = 0.2, b = 0.4, codes = "PAN+3" },
        { text = "[Three times, Theoden called 'Death!' and the call of 'DEATH!' erupted from the lips of the Rohirrim, and then with the dawn, they charged forth!]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+3" },
        { text = "[The orcs fire a few volleys, but the Rohirrim, driven by their aged and yet glorious king on Snowmane, crash upon the orcs like a wave on a shore.]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+3" },
        { text = "[Theoden, with the battle-fury of his fathers and new fire in his veins, is borne on Snowmane like a god of old.]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+3" },
        { text = "[His golden shield is uncovered, shining like an image of the sun, and the grass flamed into green beneath Snowmane's white hooves.]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+3" },
        { text = "[All the host of Rohan burst into song as they slew, for the joy of battle was on them, and the sound of their singing was fair and terrible.]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+3" },
        { text = "[Darkness is removed, and the hosts of Mordor wail as terror takes them; they flee, and the hoofs of wrath ride over them.]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+3" },
        { text = "[Despite the valor of the Rohirrim, King Theoden is struck down by the Witch-king, his horse slain atop him.]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+4" },
        { text = "[Only Eowyn faces the Witch-king on the battlefield, with Merry close by.]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+4" },
        { text = "You fool. No man can kill me.", r = 0.6, g = 0.2, b = 0.4, codes = "PAN+4" },
        { text = "[The Witch-king smashes Eowyn's shield, breaking her arm, but Merry stabs him in the heel, making the vile servant of Sauron kneel before Eowyn.]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+4" },
        { text = "I am no man!", r = 0.2, g = 0.6, b = 0.4, codes = "PAN+4" },
        { text = "[Eowyn strikes the Witch-king, killing him, and then moves to her uncle, the king.]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+4" },
        { text = "My body is broken. I go to my fathers. And even in their mighty company, I shall not now be ashamed.", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+4" },
        { text = "[Eowyn cries for the passing of her uncle and lays beside him with Merry nearby.]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+4" },
        { text = "[Aragorn arrives with the Army of the Dead from the Paths of the Dead, turning the tide of the battle.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-5" },
        { text = "Release us.", r = 0.2, g = 0.6, b = 0.4, codes = "BOR-5" },
        { text = "[Aragorn does so, and the Army of the Dead are released from their oath, now fulfilled, and the siege of Minas Tirith is over.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-5" },
        { text = "[Meanwhile, Frodo and Sam struggle toward Mount Doom, weary and broken.]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+2" },
        { text = "Do you remember the Shire, Mister Frodo? It'll be spring soon; the orchards will be in blossom, and the birds will be nesting in the hazel thicket.", r = 0.2, g = 0.6, b = 0.4, codes = "PAN-1" },
        { text = "Do you remember the taste of strawberries?", r = 0.6, g = 0.2, b = 0.4, codes = "PAN-1" },
        { text = "Oh Sam, I cannot recall the taste of food, or water, or touch of grass. All I see is the wheel of fire! I can see him with my waking eyes!", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+3" },
        { text = "Then let us be rid of it! Once and for all!", r = 0.2, g = 0.6, b = 0.4, codes = "PAN-1" },
        { text = "Come on, Mr. Frodo! I can't carry it for you, but I can carry you!", r = 0.2, g = 0.6, b = 0.4, codes = "PAN-1" },
        { text = "[And thus, Sam carries Frodo into Mount Doom, alone.]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+20" },
        { text = "[Frodo reaches Mount Doom but is overcome by the Ring's power.]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+5" },
        { text = "The Ring is mine.", r = 0.6, g = 0.2, b = 0.4, codes = "PAN+5" },
        { text = "[Gollum attacks Sam, knocking him out, then leaps at Frodo, biting off his finger to take the Ring.]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+5" },
        { text = "[In a struggle, Gollum falls into the fires of Mount Doom with the Ring.]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN+5" },
        { text = "[Sauron's tower crumbles as the Ring is destroyed, signaling victory.]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN-20" },
        { text = "It's over. The Shadow is lifted.", r = 0.2, g = 0.6, b = 0.4, codes = "PAN-10" },
        { text = "[The hobbits are honored at Aragorn's coronation.]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN-10" },
        { text = "My friends, you bow to no one.", r = 0.6, g = 0.2, b = 0.4, codes = "PAN-10" },
        { text = "[The hobbits return to the Shire, forever changed.]", r = 0.0, g = 0.7, b = 0.3, codes = "PAN-10" },
        { text = "How do you pick up the threads of an old life?", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-10" },
        { text = "It's gone. There's no going back.", r = 0.6, g = 0.2, b = 0.4, codes = "PAN-10" },
        { text = "[Frodo writes his tale in Bilbo's book before departing for the Grey Havens.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "I tried to save the Shire, and it has been saved, but not for me.", r = 0.6, g = 0.2, b = 0.4, codes = "PAN-10" },
        { text = "[At the Grey Havens, the hobbits bid Frodo, Bilbo, Gandalf, Elrond, and Galadriel farewell as he departs Middle-earth.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR-1" },
        { text = "I will not say: do not weep; for not all tears are an evil.", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-1" },
        { text = "[Sam watches as Frodo's ship sails into the distance, lost to the horizon.]", r = 0.0, g = 0.7, b = 0.3, codes = "BOR+1" },
        { text = "Well, I'm back.", r = 0.6, g = 0.2, b = 0.4, codes = "BOR-1" },        
    },
};
