IG_UI_EN = {
    IGUI_CantEatFoodMasked = "I can't eat food while masked.",
    IGUI_CantSmokeMasked = "I can't smoke while masked.",
    IGUI_CantDrinkMasked = "I can't drink while masked.",
}