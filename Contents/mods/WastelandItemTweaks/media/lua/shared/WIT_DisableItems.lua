local function RemoveDist(name)
    print("Removing dist: " .. name)
    for category, list in pairs(ProceduralDistributions["list"]) do
        for i, v in ipairs(list.items) do
            if v == name then
                print("Removing dist: " .. name .. " from " .. category)
                table.remove(list.items, i)
                table.remove(list.items, i)
                break
            end
        end
    end
end

local itemsToRemove = {}

local function DisableItem(itemId)
    print("Disabling item: " .. itemId)
    local item = ScriptManager.instance:getItem(itemId)
	if not item then
		print("ERROR: Item not found to modify: " .. itemId)
		return
	end
    item:DoParam("Weight = 999") -- to make any that happen to exist somehow be unmovable
    item:DoParam("OBSOLETE = TRUE")
    if itemId:contains(".") then
        local split = itemId:split(".")
        table.insert(itemsToRemove, split[2])
    end
    table.insert(itemsToRemove, itemId)

end

Events.OnDistributionMerge.Add(function()
    for _, v in ipairs(itemsToRemove) do
        RemoveDist(v)
    end
end)

--- Disable items

if getActivatedMods():contains("PertsPartyTiles") then
    DisableItem("Base.OverlookFireAxe")
    DisableItem("Base.BatLeth")
    DisableItem("Base.MekLeth")
    DisableItem("Base.AZZK_pistol")
end

if getActivatedMods():contains("Daisy County") then
    DisableItem("Base.SuperMushroom")
    DisableItem("Base.556ClipofBill")
    DisableItem("Base.Boomerbileofjar")
    DisableItem("Base.VinifanBagSatchel")
    DisableItem("Base.SuperMugSpiffo")
    DisableItem("Base.SuperWaterMugSpiffo")
    DisableItem("Base.GoldenFertilizer")
    DisableItem("Base.PillsCrudestimulant")
    DisableItem("Base.Shisanxiang")
    DisableItem("Base.L4D2acousticequipment1")
    DisableItem("Base.L4D2acousticequipment2")
    DisableItem("Base.L4D2acousticequipment3")
    DisableItem("Base.vanillaalarmacousticequipment")
    DisableItem("Base.ScavengeSandwich")
    DisableItem("Base.BagofCatfood")
    DisableItem("Base.SpiffoPancakes")
    DisableItem("Base.SuperFlashlight")
    DisableItem("Base.DeveloperRevolverLong")
    DisableItem("Base.OneBarrelShotgun")
    DisableItem("Base.SuperTableLeg")
    DisableItem("Base.AssaultRifleofBill")
    DisableItem("Base.SuperWoodAxe")
    DisableItem("Base.SteelPickAxe")
    DisableItem("Base.ChurchPistol")
    DisableItem("Base.YZstick")
end