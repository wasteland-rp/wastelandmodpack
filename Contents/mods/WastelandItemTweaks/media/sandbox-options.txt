VERSION = 1,

option WastelandItemTweaks.CurrentZombieEarEvent
{
  type = string,
  default = None,
  page = WastelandItemTweaks,
  translation = WastelandItemTweaks.CurrentZombieEarEvent,
}

option WastelandItemTweaks.MaxZombieEarsPerPlayer
{
  type = integer,
  min = 1,max=100000,
  default = 1000,
  page = WastelandItemTweaks,
  translation = WastelandItemTweaks.MaxZombieEarsPerPlayer,
}

option WastelandItemTweaks.EarsFullDrop
{
  type = integer,
  min = 1,max=100000,
  default = 500,
  page = WastelandItemTweaks,
  translation = WastelandItemTweaks.EarsFullDrop,
}

option WastelandItemTweaks.SuperBagLifespan
{
  type = integer,
  default = 3600, min = 0, max = 99999999,

  page = WastelandItemTweaks,
  translation = WastelandItemTweaks.SuperBagLifespan,
}