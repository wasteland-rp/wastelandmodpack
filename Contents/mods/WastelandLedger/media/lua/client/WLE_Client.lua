---
--- WLE_Client.lua
--- 13/06/2024
---
if not isClient() then return end

require 'WLBaseObject'

WLE_Client = WLBaseObject:derive("WLE_Client")
WLE_Clients = {}

--- Fetches or creates the WLE_Client instance for an account type
--- @param accountType string a category for the account, usually the modID. Makes up part of the account UIDs.
--- @return table WLE_Client never nil
function WLE_Client.getClient(accountType)
	local client = WLE_Clients[accountType]
	if not client then
		client = WLE_Client:new(accountType)
		WLE_Clients[accountType] = client
	end
	return client
end

--- Don't call this directly, use getClient(accountType)
function WLE_Client:new(accountType)
	local o = WLE_Client.parentClass.new(self)
	o.accountType = accountType
	o.transactions = {}
	o.overdraft = 0
	return o
end

--- Fetches the balance of an account and calls back with the result
--- @param accountId string to identify the account, is used with the accountType to make a UID
--- @param callback function optional callback used when this operation is completed and server replies. The callback
--- is always called with parameters: target [nil/table], successful [boolean], newBalance [number]
--- @param target table typically an object the callback is on for instance functions. e.g. target:callback(params)
function WLE_Client:fetchBalance(accountId, callback, target)
	if not accountId then error("accountId is missing") end
	if callback ~= nil and type(callback) ~= "function" then error("Callback must be a function or nil") end
	local transaction = getRandomUUID()
	self.transactions[transaction] = {target = target, callback = callback}
	sendClientCommand(getPlayer(), "WastelandLedger", "fetchBalance",
			{ transactionUUID = transaction, accountType = self.accountType, accountId = accountId})
end

--- Fetches N months of history for an account, where 0 means the current month only. The server aggregates the data
--- by summing up the amounts for each category type across the specified months.
--- @param accountId string to identify the account, is used with the accountType to make a UID
--- @param monthsBack number how many months back. e.g. 1 means this month and last month too
--- @param callback function optional callback used when this operation is completed and server replies. The callback
--- is always called with parameters: target [nil/table], successful [boolean], result [table]
--- The result table format resembles this: {deposit = { tax=4, other=3 }, withdrawal = { salary=5 } }
--- @param target table typically an object the callback is on for instance functions. e.g. target:callback(params)
function WLE_Client:fetchLedger(accountId, monthsBack, callback, target)
	if not accountId then error("accountId is missing") end
	if type(monthsBack) ~= "number" then
		error("monthsBack must be a number")
	elseif monthsBack % 1 ~= 0 then
		error("monthsBack must be an integer")
	end
	if callback ~= nil and type(callback) ~= "function" then error("Callback must be a function or nil") end
	local transaction = getRandomUUID()
	self.transactions[transaction] = {target = target, callback = callback}
	sendClientCommand(getPlayer(), "WastelandLedger", "fetchLedger",
	{ transactionUUID = transaction, accountType = self.accountType, accountId = accountId, monthsBack = monthsBack})
end

--- Fetches the balance of an account and calls back with the result. Also checks balance at the same time.
--- @param accountId string to identify the account, is used with the accountType to make a UID
--- @param category string this should NOT be unique but a classification key, e.g. "incomeTax" or "salaries", it is
--- used to group together transactions for reporting as to how money entered or left an account
--- @param callback function optional callback used when this operation is completed and server replies. The callback
--- is always called with parameters: target [nil/table], successful [boolean], newBalance [number]
--- @param target table typically an object the callback is on for instance functions. e.g. target:callback(params)
function WLE_Client:attemptWithdrawal(accountId, amount, category, callback, target)
	if not accountId then error("accountId is missing") end
	if type(amount) ~= "number" then
		error("amount must be a number")
	elseif amount % 1 ~= 0 then
		error("amount must be an integer")
	end
	if amount <= 0 then return end
	if callback ~= nil and type(callback) ~= "function" then error("Callback must be a function or nil") end
	local transaction = getRandomUUID()
	self.transactions[transaction] = {target = target, callback = callback}
	sendClientCommand(getPlayer(), "WastelandLedger", "attemptWithdrawal",
			{ transactionUUID = transaction, accountType = self.accountType, accountId = accountId,
			  amount = amount, category = category, overdraft = self.overdraft})
end

--- Makes a deposit and returns with the balance value afterwards
--- @param accountId string to identify the account, is used with the accountType to make a UID
--- @param category string this should NOT be unique but a classification key, e.g. "incomeTax" or "salaries", it is
--- used to group together transactions for reporting as to how money entered or left an account
--- @param callback function optional callback used when this operation is completed and server replies. The callback
--- is always called with parameters: target [nil/table], successful [boolean], newBalance [number]
--- @param target table typically an object the callback is on for instance functions. e.g. target:callback(params)
function WLE_Client:makeDeposit(accountId, amount, category, callback, target)
	if not accountId then error("accountId is missing") end
	if type(amount) ~= "number" then
		error("amount must be a number")
	elseif amount % 1 ~= 0 then
		error("amount must be an integer")
	end
	if amount <= 0 then return end
	if callback ~= nil and type(callback) ~= "function" then error("Callback must be a function or nil") end
	local transaction = getRandomUUID()
	if callback then
		self.transactions[transaction] = {target = target, callback = callback}
	end
	sendClientCommand(getPlayer(), "WastelandLedger", "makeDeposit",
			{ transactionUUID = transaction, accountType = self.accountType, accountId = accountId,
			  amount = amount, category = category})
end

--- Sets the monthly transactions for an account.
---@param accountId string to identify the account, is used with the accountType to make a UID
---@param monthlyTransactions table a table with two keys, withdrawals and deposits. Each key is a table of strings to
--- whole numbers. The strings are the category of the transaction, e.g. "buses" or "stipend". The integers
--- are the amount of the transaction each month.
function WLE_Client:setMonthlyTransactions(accountId, monthlyTransactions)
	if not accountId then error("accountId is missing") end
	if type(monthlyTransactions) ~= "table" then error("monthlyTransactions must be a table") end

	local withdrawals = monthlyTransactions.withdrawals
	if not withdrawals then withdrawals = {} end

	-- Validate that withdrawals is a table of strings to whole numbers
	for category, amount in pairs(withdrawals) do
		if type(category) ~= "string" then error("withdrawals category must be a string") end
		if type(amount) ~= "number" then error("withdrawals amount must be a number") end
		if amount % 1 ~= 0 then error("withdrawals amount must be an integer") end
	end

	local deposits = monthlyTransactions.deposits
	if not deposits then deposits = {} end

	-- Validate that deposits is a table of strings to whole numbers
	for category, amount in pairs(deposits) do
		if type(category) ~= "string" then error("deposits category must be a string") end
		if type(amount) ~= "number" then error("deposits amount must be a number") end
		if amount % 1 ~= 0 then error("deposits amount must be an integer") end
	end

	sendClientCommand(getPlayer(), "WastelandLedger", "setMonthlyTransactions",
			{ accountType = self.accountType, accountId = accountId, withdrawals = withdrawals,
			  deposits = deposits})
end

function WLE_Client:transactionResponse(args)
	local transaction = self.transactions[args.transactionUUID]
	if transaction then
		if transaction.callback then
			transaction.callback(transaction.target, args.successful, args.result)
		end
		self.transactions[args.transactionUUID] = nil
	end
end

local function processServerCommand(module, command, args)
	if module ~= "WastelandLedger" then return end
	local client = WLE_Client.getClient(args.accountType)
	client[command](client, args)
end

Events.OnServerCommand.Add(processServerCommand)