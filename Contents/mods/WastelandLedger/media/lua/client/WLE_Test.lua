---
--- WLE_Test.lua
---
--- Contains console commands for testing the mod
---
--- Use in combination with WL_Utils.setGamedate() to test the ledger over time
---
--- 15/06/2024
---

if not isClient() then return end

require 'WLE_Client'

WLE_Test = {}

function WLE_Test.setupMonthlyTransactions()
	local client = WLE_Client.getClient("WastelandLedger")
	client:setMonthlyTransactions("test", { deposits = { stipend = 2 }, withdrawals = { buses = 15, ferries = 10 } })
	print("Monthly transactions set to test values")
end

function WLE_Test.clearMonthlyTransactions()
	local client = WLE_Client.getClient("WastelandLedger")
	client:setMonthlyTransactions("test", {})
	print("Monthly transactions cleared")
end

function WLE_Test.check()
	local client = WLE_Client.getClient("WastelandLedger")
	client:fetchBalance("test", function(_, success, newBalance)
		print("fetchBalance - success: " .. tostring(success) .. " balance: " .. tostring(newBalance))
	end, nil)
end

function WLE_Test.ledger(months)
	local gameTime = getGameTime()
	print("Current Date: " .. tostring(gameTime:getYear()) .. "-" .. tostring(gameTime:getMonth()))
	local monthsToCheck = months or 3
	print("Getting the past " .. tostring(monthsToCheck) .. " months")
	local client = WLE_Client.getClient("WastelandLedger")
	client:fetchLedger("test", monthsToCheck, function(_, success, result)
		print("fetchLedger - success: " .. tostring(success) .. " result: " .. WL_Utils.tableToString(result))
	end)
end

function WLE_Test.deposit(amount, category)
	local cat = category or "testDeposit"
	local client = WLE_Client.getClient("WastelandLedger")
	client:makeDeposit("test", amount, cat, function(_, success, newBalance)
		print("makeDeposit - success: " .. tostring(success) .. " balance: " .. tostring(newBalance))
	end)
end

function WLE_Test.withdraw(amount, category)
	local cat = category or "testWithdraw"
	local client = WLE_Client.getClient("WastelandLedger")
	client:attemptWithdrawal("test", amount, cat, function(_, success, newBalance)
		print("attemptWithdrawal - success: " .. tostring(success) .. " balance: " .. tostring(newBalance))
	end)
end

