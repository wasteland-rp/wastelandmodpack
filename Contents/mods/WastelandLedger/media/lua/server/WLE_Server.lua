---
--- WLE_Server.lua
--- 13/06/2024
---

if isClient() then return end

WLE_Accounts = WLE_Accounts or nil
WLE_Ledgers = WLE_Ledgers or nil
WLE_MonthlyFees = WLE_MonthlyFees or nil
WLE_FeePaymentStatus = WLE_FeePaymentStatus or nil
WLE_ServerDataIsDirty = WLE_ServerDataIsDirty or false

local function onInitGlobalModData()
	WLE_Accounts = ModData.getOrCreate("WLE_Accounts")
	WLE_Ledgers = ModData.getOrCreate("WLE_Ledgers")
	WLE_MonthlyFees = ModData.getOrCreate("WLE_MonthlyFees")
	WLE_FeePaymentStatus = ModData.getOrCreate("WLE_FeePaymentStatus")
end

local function saveModData()
	ModData.add("WLE_Accounts", WLE_Accounts)
	ModData.add("WLE_Ledgers", WLE_Ledgers)
	ModData.add("WLE_MonthlyFees", WLE_MonthlyFees)
	ModData.add("WLE_FeePaymentStatus", WLE_FeePaymentStatus)
end

local function onMinutePassed()
	if WLE_ServerDataIsDirty then
		saveModData()
		WLE_ServerDataIsDirty = false
	end
end

local function getYearMonth()
	local gameTime = getGameTime()
	return tostring(gameTime:getYear()) .. "-" .. tostring(gameTime:getMonth())
end

local function populateTransactionsForMonth(fullAccountId, yearMonth, result)
	if WLE_Ledgers[fullAccountId] and WLE_Ledgers[fullAccountId][yearMonth] then
		local monthData = WLE_Ledgers[fullAccountId][yearMonth]
		for transactionType, categories in pairs(monthData) do
			result[transactionType] = result[transactionType] or {}
			for category, amount in pairs(categories) do
				result[transactionType][category] = (result[transactionType][category] or 0) + amount
			end
		end
	end
end

local function getRecentTransactions(fullAccountId, monthsBack)
	local result = {}
	local currentGameTime = getGameTime()
	local currentYear, currentMonth = currentGameTime:getYear(), currentGameTime:getMonth()

	for i = 0, monthsBack do
		local yearMonth = tostring(currentYear) .. "-" .. tostring(currentMonth)
		populateTransactionsForMonth(fullAccountId, yearMonth, result)

		if i < monthsBack then
			currentMonth = currentMonth - 1
			if currentMonth < 0 then
				currentMonth = 11
				currentYear = currentYear - 1
			end
		end
	end

	return result
end

local function recordTransaction(fullAccountId, transactionType, category, amount)
	WLE_Ledgers[fullAccountId] = WLE_Ledgers[fullAccountId] or {}
	local yearMonth = getYearMonth()
	WLE_Ledgers[fullAccountId][yearMonth] = WLE_Ledgers[fullAccountId][yearMonth] or {}
	WLE_Ledgers[fullAccountId][yearMonth][transactionType] = WLE_Ledgers[fullAccountId][yearMonth][transactionType] or {}
	WLE_Ledgers[fullAccountId][yearMonth][transactionType][category] = (WLE_Ledgers[fullAccountId][yearMonth][transactionType][category] or 0) + amount
end

-- WLE_FeePaymentStatus table example:
-- WLE_FeePaymentStatus = {  fullAccountId = {
-- withdrawals = { category = { yearMonth = true, yearMonth2 = false }  },
-- deposits = { category = { yearMonth = true, yearMonth2 = false } }
-- } }
local function reviewFeePaymentStatus()
	--print("Reviewing fee payment status")

	local yearMonth = getYearMonth()

	for fullAccountId, accountFees in pairs(WLE_MonthlyFees) do
		local feePaymentStatus = WLE_FeePaymentStatus[fullAccountId]
		if not feePaymentStatus then
			feePaymentStatus = {}
			feePaymentStatus.withdrawals = {}
			feePaymentStatus.deposits = {}
			WLE_FeePaymentStatus[fullAccountId] = feePaymentStatus
		end

		for category, amount in pairs(accountFees.withdrawals) do
			local categoryRecords = feePaymentStatus.withdrawals[category]
			if not categoryRecords then
				categoryRecords = {}
				feePaymentStatus.withdrawals[category] = categoryRecords
			end

			local monthPayment = categoryRecords[yearMonth]
			if not monthPayment then
				--print("Category " .. category .. " for account " .. fullAccountId .. " has not paid this month, paying now")
				local balance = WLE_Accounts[fullAccountId]
				if not balance then
					WLE_Accounts[fullAccountId] = -amount
				else
					WLE_Accounts[fullAccountId] = balance - amount
				end
				recordTransaction(fullAccountId, "withdrawal", category, amount)
				categoryRecords[yearMonth] = true
			end
		end

		for category, amount in pairs(accountFees.deposits) do
			local categoryRecords = feePaymentStatus.deposits[category]
			if not categoryRecords then
				categoryRecords = {}
				feePaymentStatus.deposits[category] = categoryRecords
			end

			local monthPayment = categoryRecords[yearMonth]
			if not monthPayment then
				--print("Category " .. category .. " for account " .. fullAccountId .. " has not been given this month, paying now")
				local balance = WLE_Accounts[fullAccountId]
				if not balance then
					WLE_Accounts[fullAccountId] = amount
				else
					WLE_Accounts[fullAccountId] = balance + amount
				end
				recordTransaction(fullAccountId, "deposit", category, amount)
				categoryRecords[yearMonth] = true
			end
		end
	end

	saveModData()
end

local Commands = {}

function Commands.fetchBalance(player, args)
	local fullAccountId = args.accountType .. "-" .. args.accountId
	local balance = WLE_Accounts[fullAccountId]
	if balance then
		args.successful = true
		args.result = balance
	else
		args.successful = false
		args.result = 0
	end
	sendServerCommand(player, "WastelandLedger", "transactionResponse", args)
end

function Commands.fetchLedger(player, args)
	local fullAccountId = args.accountType .. "-" .. args.accountId
	args.result = getRecentTransactions(fullAccountId, args.monthsBack)
	args.successful = true
	sendServerCommand(player, "WastelandLedger", "transactionResponse", args)
end

function Commands.attemptWithdrawal(player, args)
	local fullAccountId = args.accountType .. "-" .. args.accountId
	local balance = WLE_Accounts[fullAccountId]
	if balance then
		local allowedOverdraft = args.overdraft or 0
		local newBalance = balance - args.amount
		if newBalance >= -allowedOverdraft then
			WLE_Accounts[fullAccountId] = newBalance
			args.successful = true
			args.result = newBalance
			recordTransaction(fullAccountId, "withdrawal", args.category, args.amount)
			WLE_ServerDataIsDirty = true
		else
			args.successful = false
			args.result = balance
		end
	else
		args.successful = false
		args.result = 0
	end
	sendServerCommand(player, "WastelandLedger", "transactionResponse", args)
end

function Commands.makeDeposit(player, args)
	local fullAccountId = args.accountType .. "-" .. args.accountId
	local balance = WLE_Accounts[fullAccountId]
	if not balance then
		WLE_Accounts[fullAccountId] = args.amount
		args.result = args.amount
	else
		balance = balance + args.amount
		WLE_Accounts[fullAccountId] = balance
		args.result = balance
	end
	recordTransaction(fullAccountId, "deposit", args.category, args.amount)
	WLE_ServerDataIsDirty = true
	args.successful = true
	sendServerCommand(player, "WastelandLedger", "transactionResponse", args)
end

function Commands.setMonthlyTransactions(player, args)
	local fullAccountId = args.accountType .. "-" .. args.accountId
	local accountFees = WLE_MonthlyFees[fullAccountId]
	if not accountFees then
		accountFees = {}
		WLE_MonthlyFees[fullAccountId] = accountFees
	end
	accountFees.withdrawals = args.withdrawals or {}
	accountFees.deposits = args.deposits or {}
	reviewFeePaymentStatus()
end

local function onClientCommand(module, command, player, args)
	if module ~= "WastelandLedger" then return end
	if not Commands[command] then return end
	if not WLE_Accounts then return end
	Commands[command](player, args)
end

Events.OnInitGlobalModData.Add(onInitGlobalModData)
Events.OnClientCommand.Add(onClientCommand)
Events.EveryOneMinute.Add(onMinutePassed)
Events.EveryDays.Add(reviewFeePaymentStatus)