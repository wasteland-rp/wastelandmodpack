---
--- WL_SkillTooltips.lua
--- 12/10/2024
---

WL_SkillTooltips = {}
WL_SkillTooltips.tooltips = {}

function WL_SkillTooltips.addTooltip(itemID, tooltip, perk, minLevel)
	WL_SkillTooltips.tooltips[itemID] = {
		text = tooltip,
		perk = perk,
		minLevel = minLevel
	}
end

function WL_SkillTooltips.getTooltip(itemID)
	local tooltip = WL_SkillTooltips.tooltips[itemID]
	if not tooltip then return nil end
	local playerLevel = getPlayer():getPerkLevel(tooltip.perk)
	if playerLevel < tooltip.minLevel then return nil end
	local text = "<RGB:0,1,0> " .. tooltip.perk:getName() .. " " .. tostring(tooltip.minLevel) .. ": <RGB:1,1,1> " .. WL_Utils.MagicSpace .. tooltip.text
	return text, tooltip.perk
end

-- Test/Example
--WL_SkillTooltips.addTooltip("Axe", "This is a fire axe made from steel and can be broken down for scrap", Perks.MetalWelding, 2)

local original_ISToolTipInv_removeFromUIManager = ISToolTipInv.removeFromUIManager
function ISToolTipInv:removeFromUIManager()
	original_ISToolTipInv_removeFromUIManager(self)
	if self.Skill_Tooltip then
		self.Skill_Tooltip:removeFromUIManager()
		self.Skill_Tooltip = nil
	end
end

local original_ISToolTipInv_setVisible = ISToolTipInv.setVisible
function ISToolTipInv:setVisible(visible)
	original_ISToolTipInv_setVisible(self, visible)
	if self.Skill_Tooltip and not visible then
		self.Skill_Tooltip:setVisible(false)
		self.Skill_Tooltip = nil
	end
end

local function getTextureName(perk)
	if perk == Perks.Doctor then
		return "profession_doctor2"
	end
	-- We can add more here, maybe using other profession icons

	return "books&misc_02_0" -- Default
end

local original_ISToolTipInv_render = ISToolTipInv.render
function ISToolTipInv:render()
	original_ISToolTipInv_render(self)
	local x = self.tooltip:getX() - 11
	local y = self.tooltip:getY() + self.tooltip:getHeight()

	if self.item then
		local tooltip, perk = WL_SkillTooltips.getTooltip(self.item:getType())
		if tooltip then
			if not self.Skill_Tooltip then
				self.Skill_Tooltip = ISToolTip:new()
				self.Skill_Tooltip:initialise()
				self.Skill_Tooltip:addToUIManager()
			end
			self.Skill_Tooltip.description = tooltip
			self.Skill_Tooltip:setTexture(getTextureName(perk))
			self.Skill_Tooltip:setVisible(true)
			self.Skill_Tooltip:setDesiredPosition(x, y)
		elseif self.Skill_Tooltip then
			self.Skill_Tooltip:setVisible(false)
		end
	end
end