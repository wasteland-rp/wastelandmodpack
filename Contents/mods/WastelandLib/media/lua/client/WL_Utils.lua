---
--- WL_Utils.lua
---
--- Utility functions for Wasteland RP
---
--- 17/10/2023
---

-- quick fix for ISFastTeleportMove breaking Z sometimes

local original_ISFastTeleportMove_moveZ = ISFastTeleportMove.moveZ
ISFastTeleportMove.moveZ = function(self, z)
    original_ISFastTeleportMove_moveZ(self, z)
    ISFastTeleportMove.currentZ = math.max(0, math.floor(ISFastTeleportMove.currentZ))
end

WL_Utils = WL_Utils or {}
WL_Utils_Privates = WL_Utils_Privates or {}

--- Add a WL_FakeMessage to the chat window
--- @param message WL_FakeMessage
function WL_Utils.addFakeMessageToChatWindow(message)
    if not ISChat or not ISChat.instance or not ISChat.instance.chatText then return end
    local line = message:getTextWithPrefix()
    local chatText = ISChat.instance.chatText
    if message:getChatID() then
        for _,v in ipairs(ISChat.instance.tabs) do
            if v.tabID == message:getChatID() then
                chatText = v
                break
            end
        end
    end
    if chatText.tabTitle ~= ISChat.instance.chatText.tabTitle then
        local alreadyExist = false;
        for i,blinkedTab in ipairs(ISChat.instance.panel.blinkTabs) do
            if blinkedTab == chatText.tabTitle then
                alreadyExist = true;
                break;
            end
        end
        if alreadyExist == false then
            table.insert(ISChat.instance.panel.blinkTabs, chatText.tabTitle);
        end
    end
    local vscroll = chatText.vscroll
    local scrolledToBottom = (chatText:getScrollHeight() <= chatText:getHeight()) or (vscroll and vscroll.pos == 1)
    if #chatText.chatTextLines > ISChat.maxLine then
        local newLines = {}
        for i,v in ipairs(chatText.chatTextLines) do
            if i ~= 1 then
                table.insert(newLines, v)
            end
        end
        table.insert(newLines, line .. " <LINE> ")
        chatText.chatTextLines = newLines
    else
        table.insert(chatText.chatTextLines, line .. " <LINE> ")
    end
    chatText.text = ""
    local newText = ""
    for i,v in ipairs(chatText.chatTextLines) do
        if i == #chatText.chatTextLines then
            v = string.gsub(v, " <LINE> $", "")
        end
        newText = newText .. v
    end
    chatText.text = newText
    table.insert(chatText.chatMessages, message)
    chatText:paginate()
    if scrolledToBottom then
        chatText:setYScroll(-100000)
    end
end

--- Add a message to the chat window
--- @param text string
--- @param options WL_ChatOptions|nil
function WL_Utils.addToChat(text, options)
    local message = WL_FakeMessage:new(text, options)
    WL_Utils.addFakeMessageToChatWindow(message)
end

--- Add a message to the chat window with a red color
--- @param text string
--- @param options WL_ChatOptions|nil
function WL_Utils.addErrorToChat(text, options)
    options = options or {}
    options.color = "1.0,0.2,0.2"
    WL_Utils.addToChat(text, options)
end

--- Add a message to the chat window with a blue color
--- @param text string
--- @param options WL_ChatOptions|nil
function WL_Utils.addInfoToChat(text, options)
    options = options or {}
    options.color = "0.2,0.2,1.0"
    WL_Utils.addToChat(text, options)
end

--- Local utility function to write chat messages for item spawning
--- @return string the translated name of the item that was added or nil if no such item existed
local function addItemGainedMessage(itemID, newItem, quantity)
    if not newItem then
        WL_Utils.addToChat("ERROR item not found: " .. itemID, { color = "1.0,0,0", })
        return nil
    end

    local numberString
    if quantity and quantity > 1 then
        numberString =  " (" .. quantity .. ")"
    else
        numberString = ""
    end

    WL_Utils.addToChat("Gained Item: " .. newItem:getName() .. numberString, { color = "1.0,0.8,0.2", })
    return newItem:getName()
end

--- Adds one or more instances of an item to the player's inventory and sends a chat window message informing them
--- @param itemID string of the item's type, e.g Base.Pistol
--- @param quantity number|nil optional parameter defining how many of this item to add
--- @return string the translated name of the item that was added or nil if no such item existed
function WL_Utils.addItemToInventory(itemID, quantity)
    local inventory = getPlayer():getInventory()
    local newItem

    if quantity then
        for _ = 1, quantity do
            newItem = inventory:AddItem(itemID)
        end
        return addItemGainedMessage(itemID, newItem, quantity)
    else
        newItem = inventory:AddItem(itemID)
        return addItemGainedMessage(itemID, newItem)
    end
end

---@param playerFrom IsoPlayer to take from
---@param itemID string full ID of the item
---@param amount number number of items to take
---@return boolean true if successful taking the items, otherwise false
function WL_Utils.attemptTakeItems(playerFrom, itemID, amount)
    if not amount then amount = 1 end
    if amount < 1 then return true end
    if playerFrom:getInventory():getItemCountFromTypeRecurse(itemID) < amount then
        return false
    end
    for i = 1, amount do
        local item = playerFrom:getInventory():getFirstTypeRecurse(itemID)
        if item then
            item:getContainer():Remove(item)
        end
    end
    return true
end

--- Gives XP to a player and sends a chat window message informing them
--- Does not factor in perk bonuses, adding the XP value exactly as it is, which makes the message reporting
--- how much XP the player gained be correct at all times (Though this isn't what is wanted in many cases)
--- @param perk PerkFactory.Perks object NOT the string ID e.g Perks.Cooking
--- @param amount number how much XP to give the player
function WL_Utils.gainXP(perk, amount)
    getPlayer():getXp():AddXP(perk, amount, false, false, false);
    WL_Utils.addToChat("Gained " .. perk:getName() .. ": " .. tostring(amount) .. "XP",
            { color = "0.85,0.5,1.0", })
end

--- Reduces XP from a player silently. Unlike gainXP, this function does not send a chat message
--- If needed, this function will drop them down to the previous level.
--- WARNING: This function does not work properly for fitness XP over level 6 when the player is underweight as it
--- can't add XP back due to AddXP being blocked by vanilla Zomboid. It will drop the entire level down instead.
--- @param perk PerkFactory.Perks object NOT the string ID e.g Perks.Cooking
--- @param amount number how much XP to take from the player
function WL_Utils.reduceXP(perk, amount)
    local player = getPlayer()
    local currentLevel = player:getPerkLevel(perk)
    local currentLevelStartXp = perk:getTotalXpForLevel(currentLevel)
    local xp = player:getXp()
    local targetXP = xp:getXP(perk) - amount
    local targetLevel = currentLevel
    if (targetXP < currentLevelStartXp) and currentLevel > 0 then
        targetLevel = targetLevel - 1
        player:LoseLevel(perk)
    end

    local xpToRestore = targetXP - perk:getTotalXpForLevel(targetLevel)
    xp:setXPToLevel(perk, targetLevel);
    if xpToRestore > 0 then
        xp:AddXP(perk, xpToRestore, false, false, false)
    end
    SyncXp(player)
end

--- Makes the player safe from zombies for a short time
--- @param time number how long to make the player safe for in seconds
function WL_Utils.makePlayerSafe(moveTime, stillTime)
    local player = getPlayer()
    if not player then return end
    if WL_Utils.isStaff(player) then return end -- Admins can choose to be safe on their own
    stillTime = stillTime or moveTime * 10
    WL_SafePlayer:start(player, moveTime, stillTime)
end


local function scanItem(holder, item, foundAt)
    table.insert(holder, {item = item, foundAt = foundAt})
    if instanceof(item, "InventoryContainer") then
        local container = item:getItemContainer()
        if container then
            local items = container:getItems()
            if items then
                for i = 0, items:size() - 1 do
                    local innerItem = items:get(i)
                    if innerItem then
                        scanItem(holder, innerItem, "bag")
                    end
                end
            end
        end
    end
end

-- Scans a gridSquare for all items, and returns them in a table
function WL_Utils.scanGridSquare(x, y, z)
    if not instanceof(x, "IsoGridSquare") then
        if not x or not y or not z then return {} end
        x = getCell():getGridSquare(x, y, z)
    end
    if not x then return {} end
    local items = {}

    worldObjects = x:getWorldObjects()
    if worldObjects then
        for j = 0, worldObjects:size() - 1 do
            object = worldObjects:get(j):getItem()
            if object then
                scanItem(items, object, "ground")
            end
        end
    end

    objects = x:getObjects()
    if objects then
        for j = 0, objects:size() - 1 do
            object = objects:get(j)
            if object then
                container = object:getContainer()
                if container then
                    for k = 0, container:getItems():size() - 1 do
                        item = container:getItems():get(k)
                        if item then
                            scanItem(items, item, "container")
                        end
                    end
                end
            end
        end
    end

    movingObjects = x:getMovingObjects()
    if movingObjects then
        for j = 0, movingObjects:size() - 1 do
            object = movingObjects:get(j)
            if instanceof(object, "BaseVehicle") then
                for k = 0, object:getPartCount() - 1 do
                    local part = object:getPartByIndex(k)
                    if part then
                        container = part:getItemContainer()
                        if container then
                            for l = 0, container:getItems():size() - 1 do
                                item = container:getItems():get(l)
                                if item then
                                    scanItem(items, item, "vehicle")
                                end
                            end
                        end
                    end
                end
            end
        end
    end

    return items
end

WL_Utils_Privates.currentMoveBoosts = WL_Utils_Privates.currentMoveBoosts or {}
WL_Utils_Privates.currentMoveBoostAdds = WL_Utils_Privates.currentMoveBoostAdds or {}
function WL_Utils_Privates.handleMoveBoost(player)
    local boost = WL_Utils_Privates.currentMoveBoosts[player:getPlayerNum()]
    if not boost then return end
    if player:getVehicle() then return end
    if player:getPathFindBehavior2():isMovingUsingPathFind() then return end -- disable for auto-walks
    local dX = player:getX() - player:getLx()
    local dY = player:getY() - player:getLy()
    if dX ~= 0 or dY ~= 0 then
        player:setX(player:getX() + dX + dX * boost)
        player:setY(player:getY() + dY + dY * boost)
    end
end

--- Get the current total move boost for a player
--- @param player IsoPlayer
--- @return number the current move boost
function WL_Utils.getMoveBoostTotal(player)
    return WL_Utils_Privates.currentMoveBoosts[player:getPlayerNum()] or 0
end

--- Set the total move boost for a player
--- @param player IsoPlayer
--- @param boost number the new move boost to set
function WL_Utils.setMoveBoostTotal(player, boost)
    if boost == 0 then
        WL_Utils_Privates.currentMoveBoosts[player:getPlayerNum()] = nil
        return
    end
    WL_Utils_Privates.currentMoveBoosts[player:getPlayerNum()] = boost
end

--- Get the current move boost for a player/type
--- @param player IsoPlayer
--- @param boostType string the type of boost to get
function WL_Utils.getMoveBoostType(player, boostType)
    return WL_Utils_Privates.currentMoveBoostAdds[player:getPlayerNum()] and WL_Utils_Privates.currentMoveBoostAdds[player:getPlayerNum()][boostType] or 0
end

--- Add a move boost for a player/type
--- @param player IsoPlayer
--- @param boostType string the type of boost to add
--- @param boost number the amount of boost to add (-1 to 1)
function WL_Utils.setMoveBoostType(player, boostType, boost)
    if not WL_Utils_Privates.currentMoveBoostAdds[player:getPlayerNum()] then
        WL_Utils_Privates.currentMoveBoostAdds[player:getPlayerNum()] = {}
    end
    if boost == 0 then
        WL_Utils_Privates.currentMoveBoostAdds[player:getPlayerNum()][boostType] = nil
    else
        WL_Utils_Privates.currentMoveBoostAdds[player:getPlayerNum()][boostType] = boost
    end
    local totalBoost = 0
    for _,v in pairs(WL_Utils_Privates.currentMoveBoostAdds[player:getPlayerNum()]) do
        totalBoost = totalBoost + v
    end
    totalBoost = math.min(1, math.max(-1, totalBoost))
    WL_Utils.setMoveBoostTotal(player, totalBoost)
end

--- Remove a move boost from a player/type
--- @param player IsoPlayer
--- @param boostType string the type of boost to remove
function WL_Utils.removeMoveBoostType(player, boostType)
    WL_Utils.setMoveBoostType(player, boostType, 0)
end


WL_Utils_Privates.currentActionBoosts = WL_Utils_Privates.currentActionBoosts or {}
WL_Utils_Privates.currentActionBoostAdds = WL_Utils_Privates.currentActionBoostAdds or {}
WL_Utils_Privates.original_ISBaseTimedAction_adjustMaxTime = WL_Utils_Privates.original_ISBaseTimedAction_adjustMaxTime or ISBaseTimedAction.adjustMaxTime
function ISBaseTimedAction:adjustMaxTime(maxTime)
    maxTime = WL_Utils_Privates.original_ISBaseTimedAction_adjustMaxTime(self, maxTime)
    local player = self.character
    if not player then player = getPlayer() end
    local boost = WL_Utils_Privates.currentActionBoosts[player:getPlayerNum()] or 0
    if boost == 0 or maxTime <= 1 then return maxTime end
    if boost < 0 then
        maxTime = maxTime / (1 - -boost)
    else
        maxTime = maxTime * (1 - boost)
    end
    return math.max(1, math.floor(maxTime))
end

function WL_Utils.setActionBoostTotal(player, boost)
    if boost == 0 then
        WL_Utils_Privates.currentActionBoosts[player:getPlayerNum()] = nil
        return
    end
    WL_Utils_Privates.currentActionBoosts[player:getPlayerNum()] = boost
end

function WL_Utils.getActionBoostTotal(player)
    return WL_Utils_Privates.currentActionBoosts[player:getPlayerNum()] or 0
end

function WL_Utils.setActionBoostType(player, boostType, boost)
    if not WL_Utils_Privates.currentActionBoostAdds[player:getPlayerNum()] then
        WL_Utils_Privates.currentActionBoostAdds[player:getPlayerNum()] = {}
    end
    if boost == 0 then
        WL_Utils_Privates.currentActionBoostAdds[player:getPlayerNum()][boostType] = nil
    else
        WL_Utils_Privates.currentActionBoostAdds[player:getPlayerNum()][boostType] = boost
    end
    local totalBoost = 0
    for _,v in pairs(WL_Utils_Privates.currentActionBoostAdds[player:getPlayerNum()]) do
        totalBoost = totalBoost + v
    end
    totalBoost = math.min(1, math.max(-1, totalBoost))
    WL_Utils.setActionBoostTotal(player, totalBoost)
end

function WL_Utils.getActionBoostType(player, boostType)
    return WL_Utils_Privates.currentActionBoostAdds[player:getPlayerNum()] and WL_Utils_Privates.currentActionBoostAdds[player:getPlayerNum()][boostType] or 0
end

if WL_Utils_Privates.didBindPlayerMove then
    Events.OnPlayerMove.Remove(WL_Utils_Privates.handleMoveBoost)
end
Events.OnPlayerMove.Add(WL_Utils_Privates.handleMoveBoost)
WL_Utils_Privates.didBindPlayerMove = true