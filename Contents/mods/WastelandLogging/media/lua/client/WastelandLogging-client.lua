require "TimedActions/ISInventoryTransferAction"
require "TimedActions/ISFinalizeDealAction"
require "TimedActions/ISDropItemAction"
require "TimedActions/ISDropWorldItemAction"
require "TimedActions/ISUninstallVehiclePart"
require "TimedActions/ISTakeGasolineFromVehicle"
require "TimedActions/ISAttachTrailerToVehicle"
require "TimedActions/ISDetachTrailerFromVehicle"

local function getInventoryName(container, player)
    if container:getType() == "floor" then
        return "Ground"
    end
    if instanceof(container:getParent(), "IsoDeadBody") then
        return "Corpse"
    end
    if instanceof(container:getParent(), 'IsoMannequin') then
        return "Mannequin"
    end
    if instanceof(container:getParent(), "BaseVehicle") then
        if AVCS then
            local vehicleID = AVCS.getVehicleID(container:getParent())
            if vehicleID then
                return container:getParent():getScript():getName() .. "[" .. vehicleID .. "]-" .. container:getType()
            end
        end
        return container:getParent():getScript():getName() .. "-" .. container:getType()
    end

    if container:isInCharacterInventory(player) then
        if instanceof(container:getParent(), "IsoPlayer") then
            return "Hands"
        else
            return "player-" .. container:getType()
        end
    end

    return container:getType() or "unknown"
end

function ISInventoryTransferAction:WL_SendItem()
    if not self.WL_currentPendingItem then
        return
    end
    sendClientCommand(self.character, "WastelandLogging", "itemTransfer", self.WL_currentPendingItem)
    self.WL_currentPendingItem = nil
end

local ISInventoryTransferAction_transferItem = ISInventoryTransferAction.transferItem
function ISInventoryTransferAction:transferItem(item)
	if self:isAlreadyTransferred(item) then
		return
	end

    ISInventoryTransferAction_transferItem(self, item)

    if self.destContainer:getType() == "TradeUI" or self.srcContainer:getType() == "TradeUI" then
        return
    end

    if self.WL_currentPendingItem then
        if self.WL_currentPendingItem.item == item:getType() then
            self.WL_currentPendingItem.count = self.WL_currentPendingItem.count + 1
            return
        else
            self:WL_SendItem()
        end
    end

    local source = getInventoryName(self.srcContainer, self.character)
    local dest = getInventoryName(self.destContainer, self.character)
    local x, y, z = self.character:getX(), self.character:getY(), self.character:getZ()
    local item = item:getType() or "unknown"
    local args = { x = x, y = y, z = z, item = item, source = source, dest = dest, count = 1 }
    self.WL_currentPendingItem = args
end

local ISInventoryTransferAction_perform = ISInventoryTransferAction.perform
function ISInventoryTransferAction:perform()
    ISInventoryTransferAction_perform(self)
    self:WL_SendItem()
end

local ISDropItemAction_perform = ISDropItemAction.perform
function ISDropItemAction:perform()
    local container = self.item:getContainer()
    ISDropItemAction_perform(self)
    local source = getInventoryName(container, self.character)
    local dest = "Ground"
    local x, y, z = self.character:getX(), self.character:getY(), self.character:getZ()
    local item = self.item:getType() or "unknown"
    local args = { x = x, y = y, z = z, item = item, source = source, dest = dest, count = 1 }
    sendClientCommand(self.character, "WastelandLogging", "itemTransfer", args)
end

local ISDropWorldItemAction_perform = ISDropWorldItemAction.perform
function ISDropWorldItemAction:perform()
    local container = self.item:getContainer()
    ISDropWorldItemAction_perform(self)
    local source = getInventoryName(container, self.character)
    local dest = "Ground"
    local x, y, z = self.character:getX(), self.character:getY(), self.character:getZ()
    local item = self.item:getType() or "unknown"
    local args = { x = x, y = y, z = z, item = item, source = source, dest = dest, count = 1 }
    sendClientCommand(self.character, "WastelandLogging", "itemTransfer", args)
end

local ISUninstallVehiclePart_perform = ISUninstallVehiclePart.perform
function ISUninstallVehiclePart:perform()
    local args = { vehicle = self.vehicle:getId(), part = self.part:getId() }
	sendClientCommand(self.character, 'WastelandLogging', 'uninstallPart', args)
    ISUninstallVehiclePart_perform(self)
end


local lastEnteredVehicle = nil
-- VehicleEnter adds callback for OnEnterVehicle event.
local function enterVehicle(player)
    if player and instanceof(player, 'IsoPlayer') and player:isLocalPlayer() then
        lastEnteredVehicle = player:getVehicle()
        local args = { vehicle = lastEnteredVehicle:getId() }
        sendClientCommand(player, 'WastelandLogging', 'enterVehicle', args)
    end
end

local function exitVehicle(player)
    if player and instanceof(player, 'IsoPlayer') and player:isLocalPlayer() and lastEnteredVehicle then
        local args = { vehicle = lastEnteredVehicle:getId() }
        sendClientCommand(player, 'WastelandLogging', 'exitVehicle', args)
        lastEnteredVehicle = nil
    end
end

local ISTakeGasolineFromVehicle_perform = ISTakeGasolineFromVehicle.perform
function ISTakeGasolineFromVehicle:perform()
    ISTakeGasolineFromVehicle_perform(self)
    local args = { vehicle = self.vehicle:getId(), amount = self.tankStart - self.tankTarget }
    sendClientCommand(self.character, 'WastelandLogging', 'tookGas', args)
end

local ISAddGasolineToVehicle_perform = ISAddGasolineToVehicle.perform
function ISAddGasolineToVehicle:perform()
    ISAddGasolineToVehicle_perform(self)
    local args = { vehicle = self.vehicle:getId(), amount = self.tankTarget - self.tankStart }
    sendClientCommand(self.character, 'WastelandLogging', 'addGas', args)
end

local ISAttachTrailerToVehicle_perform = ISAttachTrailerToVehicle.perform
ISAttachTrailerToVehicle.perform = function(self)
    ISAttachTrailerToVehicle_perform(self)

    if self.character then
        local args = { vehicleA = self.vehicleA:getId(), vehicleB = self.vehicleB:getId() }
        sendClientCommand(self.character, 'WastelandLogging', 'attachTrailer', args)
    end
end

local ISDetachTrailerFromVehicle_perform = ISDetachTrailerFromVehicle.perform
ISDetachTrailerFromVehicle.perform = function(self)
    ISDetachTrailerFromVehicle_perform(self)
    if self.character and self.vehicle:getVehicleTowing() then
        local args = { vehicleA = self.vehicle:getId(), vehicleB = self.vehicle:getVehicleTowing():getId() }
        sendClientCommand(self.character, 'WastelandLogging', 'detachTrailer', args)
    end
end

local ISFinalizeDealAction_perform = ISFinalizeDealAction.perform
function ISFinalizeDealAction:perform()
    ISFinalizeDealAction_perform(self)
    local args = {}
    table.insert(args, self.otherPlayer:getUsername())
    table.insert(args, #self.itemsToGive)
    for _,v in ipairs(self.itemsToGive) do
        table.insert(args, v:getFullType())
    end
    table.insert(args, #self.itemsToReceive)
    for _,v in ipairs(self.itemsToReceive) do
        table.insert(args, v:getFullType())
    end
    sendClientCommand(self.character, "WastelandLogging", "trade", args)
end

local ISPlowAction_perform = ISPlowAction.perform
function ISPlowAction:perform()
    ISPlowAction_perform(self)
	local sq = self.gridSquare
	local args = { x = sq:getX(), y = sq:getY(), z = sq:getZ() }
    sendClientCommand(self.character, "WastelandLogging", "plow", args)
end

local ISHarvestPlantAction_perform = ISHarvestPlantAction.perform
function ISHarvestPlantAction:perform()
    ISHarvestPlantAction_perform(self)
	local sq = self.plant:getSquare()
	local args = { x = sq:getX(), y = sq:getY(), z = sq:getZ() }
    sendClientCommand(self.character, "WastelandLogging", "harvest", args)
end

local ISShovelAction_perform = ISShovelAction.perform
function ISShovelAction:perform()
    ISShovelAction_perform(self)
    local sq = self.plant:getSquare()
    local args = { x = sq:getX(), y = sq:getY(), z = sq:getZ() }
    sendClientCommand(self.character, "WastelandLogging", "shovel", args)
end

local ISSeedAction_perform = ISSeedAction.perform
function ISSeedAction:perform()
    ISSeedAction_perform(self)
    local sq = self.plant:getSquare()
    local args = { x = sq:getX(), y = sq:getY(), z = sq:getZ() }
    sendClientCommand(self.character, "WastelandLogging", "seed", args)
end

if PFGPushAction then
    local original_PFGPushAction_perform = PFGPushAction.perform
    function PFGPushAction:perform()
        original_PFGPushAction_perform(self)
        local args = {
            x = self.character:getX(),
            y = self.character:getY(),
            z = self.character:getZ(),
            item = self.item:getType() or "unknown",
            source = "Ground",
            dest = "Hands",
            count = 1
        }
        sendClientCommand(self.character, "WastelandLogging", "itemTransfer", args)
    end
end

local oISItemsListTableaddItem = ISItemsListTable.addItem
local buffer = {}
function ISItemsListTable:addItem(item)
    oISItemsListTableaddItem(self, item)
    if not WL_Utils.isStaff(getPlayer()) then
        local itemType = item:getFullName()
        if not buffer[itemType] then
            buffer[itemType] = 0
        end
        buffer[itemType] = buffer[itemType] + 1
    end
end

local function CheckCheats()
    local player = getPlayer()

    local isItemSpawn = false
    for k,v in pairs(buffer) do
        isItemSpawn = true
        break
    end

    if isItemSpawn then
        local items = {}
        for k,v in pairs(buffer) do
            table.insert(items, tostring(v) .. "x " .. k)
        end
        sendClientCommand(player, "WastelandLogging", "cheatItemSpawn", {table.concat(items, ", ")})
        buffer = {}
    end

    if not WL_Utils.isStaff(player) then
        local enabledPerms, disabledPerms = WLLogging_CheckPlayer(player)

        if #enabledPerms > 0 then
            sendClientCommand(player, "WastelandLogging", "cheats", {"Enabled", table.concat(enabledPerms, ", ")})
        end
        if #disabledPerms > 0 then
            sendClientCommand(player, "WastelandLogging", "cheats", {"Disabled", table.concat(disabledPerms, ", ")})
        end
    end
end


local lastCurrencyLog = 0
local function LogCurrencyOnHand()
    local player = getPlayer()
    if not player then return end
    local currency = WIT_Gold.amountOnPlayer(player)
    if currency == lastCurrencyLog then return end
    lastCurrencyLog = currency
    sendClientCommand(player, "WastelandLogging", "currency", {currency})
end

Events.OnEnterVehicle.Add(enterVehicle)
Events.OnExitVehicle.Add(exitVehicle)
Events.EveryOneMinute.Add(CheckCheats)
Events.EveryOneMinute.Add(LogCurrencyOnHand)