-- Do not run on MP Client
if isClient() then return end

local WastelandLoggingCommands = {}

local function getVehicleCondition(vehicle)
    local totalParts = 0
    local totalCondition = 0
    for i=1,vehicle:getPartCount() do
		local part = vehicle:getPartByIndex(i-1)
		local category = part:getCategory() or "Other";
		if category ~= "nodisplay" then
            totalParts = totalParts + 1
            totalCondition = totalCondition + part:getCondition()
        end
    end
    return math.floor(totalCondition / totalParts, 2)
end

local function getPartName(part)
    local invItem = part:getInventoryItem()
    if invItem then return invItem:getName() end
    local type = part:getItemType()
    if type:size() > 0 then return type:get(0) end
    return part:getCategory() .. ":" .. part:getArea()
end

local function getVehiclePersistnetID(vehicle)
    if WastelandAutoClaimsCore then
        return WastelandAutoClaimsCore:getOrAddId(nil, vehicle)
    end
    if AVCS then
        local vehicleID = AVCS.getVehicleID(vehicle)
        -- If no ID, we create one
        if not vehicleID then
            vehicle:getModData().SQLID = tonumber(getTimestamp() .. vehicle:getSqlId())
            vehicleID = vehicle:getModData().SQLID
            sendServerCommand("AVCS", "registerClientVehicleSQLID", {vehicle:getId(), vehicle:getModData().SQLID})
        end
        return vehicleID
    end
    return vehicle:getSqlId()
end

local function roundGas(amount)
    return math.floor(amount * 100 + 0.5) / 100
end


function WastelandLoggingCommands.VehicleEngineParts(playerObj, vehicle)
    local x, y, z = math.floor(vehicle:getX()), math.floor(vehicle:getY()), math.floor(vehicle:getZ())
    local username = playerObj:getUsername()
    local vehicleId = getVehiclePersistnetID(vehicle)
    local vehicleName = vehicle:getScript():getName()
    local vehicleCondition = getVehicleCondition(vehicle)
    local logMessage = string.format("%s took engine parts from %s [%s] (%s%%) at %s,%s,%s", username, vehicleName, vehicleId, vehicleCondition, x, y, z)
    writeLog("VehicleParts", logMessage)
end

function WastelandLoggingCommands.VehicleParts(playerObj, vehicle, part, action)
    local x, y, z = math.floor(vehicle:getX()), math.floor(vehicle:getY()), math.floor(vehicle:getZ())
    local username = playerObj:getUsername()
    local partName = getPartName(part)
    local vehicleId = getVehiclePersistnetID(vehicle)
    local vehicleName = vehicle:getScript():getName()
    local vehicleCondition = getVehicleCondition(vehicle)
    local partCondition = part:getCondition()
    local logMessage = string.format("%s %s %s (%s%%) on %s [%s] (%s%%) at %s,%s,%s", username, action, partName, partCondition, vehicleName, vehicleId, vehicleCondition, x, y, z)
    writeLog("VehicleParts", logMessage)
end

function WastelandLoggingCommands.EnterExitVehicle(playerObj, vehicle, action)
    local x, y, z = math.floor(vehicle:getX()), math.floor(vehicle:getY()), math.floor(vehicle:getZ())
    local username = playerObj:getUsername()
    local vehicleId = getVehiclePersistnetID(vehicle)
    local vehicleName = vehicle:getScript():getName()
    local logMessage = string.format("%s %s %s [%s] at %s,%s,%s", username, action, vehicleName, vehicleId, x, y, z)
    writeLog("VehicleParts", logMessage)
end

function WastelandLoggingCommands.AttachDetachVehicles(playerObj, vehicleA, vehicleB, action)
    local x, y, z = math.floor(vehicleA:getX()), math.floor(vehicleA:getY()), math.floor(vehicleA:getZ())
    local username = playerObj:getUsername()
    local vehicleIdA = getVehiclePersistnetID(vehicleA)
    local vehicleIdB = getVehiclePersistnetID(vehicleB)
    local vehicleNameA = vehicleA:getScript():getName()
    local vehicleNameB = vehicleB:getScript():getName()
    local logMessage = string.format("%s %s %s [%s] to %s [%s] at %s,%s,%s", username, action, vehicleNameA, vehicleIdA, vehicleNameB, vehicleIdB, x, y, z)
    writeLog("VehicleParts", logMessage)
end

function WastelandLoggingCommands.TookGas(player, vehicle, amount)
    local x, y, z = math.floor(vehicle:getX()), math.floor(vehicle:getY()), math.floor(vehicle:getZ())
    local username = player:getUsername()
    local vehicleId = getVehiclePersistnetID(vehicle)
    local vehicleName = vehicle:getScript():getName()
    local logMessage = string.format("%s took %s gas from %s [%s] at %s,%s,%s", username, roundGas(amount), vehicleName, vehicleId, x, y, z)
    writeLog("VehicleParts", logMessage)
end

function WastelandLoggingCommands.AddGas(player, vehicle, amount)
    local x, y, z = math.floor(vehicle:getX()), math.floor(vehicle:getY()), math.floor(vehicle:getZ())
    local username = player:getUsername()
    local vehicleId = getVehiclePersistnetID(vehicle)
    local vehicleName = vehicle:getScript():getName()
    local logMessage = string.format("%s added %s gas to %s [%s] at %s,%s,%s", username, roundGas(amount), vehicleName, vehicleId, x, y, z)
    writeLog("VehicleParts", logMessage)
end

function WastelandLoggingCommands.ItemTransfer(playerObj, x, y, z, item, count, source, dest)
    x, y, z = math.floor(x), math.floor(y), math.floor(z)
    local username = playerObj:getUsername()
    local logMessage = string.format("%s transferred %s %s from %s to %s at %s,%s,%s", username or "unknown", count or "unknown", item or "unknown", source or "unknown", dest or "unknown", x, y, z)
    writeLog("ItemTransfer", logMessage)
end

function WastelandLoggingCommands.Trade(player, otherPlayer, itemsGiven, itemsReceived)
    local x, y, z = math.floor(player:getX()), math.floor(player:getY()), math.floor(player:getZ())
    local username = player:getUsername()
    local receivedItems, givenItems
    if #itemsGiven > 0 then
        givenItems = table.concat(itemsGiven, ", ")
    else
        givenItems = "nothing"
    end
    if #itemsReceived > 0 then
        receivedItems = table.concat(itemsReceived, ", ")
    else
        receivedItems = "nothing"
    end
    local logMessage = string.format("%s traded %s for %s with %s at %s,%s,%s", username, givenItems, receivedItems, otherPlayer, x, y, z)
    writeLog("ItemTransfer", logMessage)
end

function WastelandLoggingCommands.Map(player, type, x, y, z)
    local username = player:getUsername()
    local logMessage = string.format("[Farming] %s %s at %s,%s,%s", username, type, x, y, z)
    writeLog("map", logMessage)
end

function WastelandLoggingCommands.OnClientCommand(module, command, player, args)
    if module == 'vehicle' then
        if command == "installPart" then
            local vehicle = getVehicleById(args.vehicle)
            local part = vehicle:getPartById(args.part)
            WastelandLoggingCommands.VehicleParts(player, vehicle, part, "Installed")
        elseif command == "takeEngineParts" then
            local vehicle = getVehicleById(args.vehicle)
            WastelandLoggingCommands.VehicleEngineParts(player, vehicle)
        end
    elseif module == 'WastelandLogging' then
        if command == "itemTransfer" then
            WastelandLoggingCommands.ItemTransfer(player, args.x, args.y, args.z, args.item, args.count, args.source, args.dest)
        elseif command == "uninstallPart" then
            local vehicle = getVehicleById(args.vehicle)
            local part = vehicle:getPartById(args.part)
            WastelandLoggingCommands.VehicleParts(player, vehicle, part, "Uninstalled")
        elseif command == "tookGas" then
            local vehicle = getVehicleById(args.vehicle)
            WastelandLoggingCommands.TookGas(player, vehicle, args.amount)
        elseif command == "addGas" then
            local vehicle = getVehicleById(args.vehicle)
            WastelandLoggingCommands.AddGas(player, vehicle, args.amount)
        elseif command == "enterVehicle" then
            local vehicle = getVehicleById(args.vehicle)
            WastelandLoggingCommands.EnterExitVehicle(player, vehicle, "Entered")
        elseif command == "exitVehicle" then
            local vehicle = getVehicleById(args.vehicle)
            WastelandLoggingCommands.EnterExitVehicle(player, vehicle, "Exited")
        elseif command == "attachTrailer" then
            local vehicleA = getVehicleById(args.vehicleA)
            local vehicleB = getVehicleById(args.vehicleB)
            WastelandLoggingCommands.AttachDetachVehicles(player, vehicleA, vehicleB, "Attached")
        elseif command == "detachTrailer" then
            local vehicleA = getVehicleById(args.vehicleA)
            local vehicleB = getVehicleById(args.vehicleB)
            WastelandLoggingCommands.AttachDetachVehicles(player, vehicleA, vehicleB, "Detached")
        elseif command == "trade" then
            local otherPlayer = args[1]
            local countItemsGiven = args[2]
            local itemsGiven = {}
            for i=1,countItemsGiven do
                table.insert(itemsGiven, args[2+i])
            end
            local countItemsReceived = args[2+countItemsGiven+1]
            local itemsReceived = {}
            for i=1,countItemsReceived do
                table.insert(itemsReceived, args[2+countItemsGiven+1+i])
            end
            WastelandLoggingCommands.Trade(player, otherPlayer, itemsGiven, itemsReceived)
        elseif command == "plow" then
            WastelandLoggingCommands.Map(player, "plowed", args.x, args.y, args.z)
        elseif command == "harvest" then
            WastelandLoggingCommands.Map(player, "harvested", args.x, args.y, args.z)
        elseif command == "shovel" then
            WastelandLoggingCommands.Map(player, "shoveled", args.x, args.y, args.z)
        elseif command == "seed" then
            WastelandLoggingCommands.Map(player, "seeded", args.x, args.y, args.z)
        elseif command == "currency" then
            writeLog("Currency", string.format("%s | %s", player:getUsername(), args[1]))
        elseif command == "cheats" then
            writeLog("Cheaters", string.format("Client | %s | %s: %s", args[1], player:getUsername(), args[2]))
        elseif command == "cheatItemSpawn" then
            writeLog("Cheaters", string.format("Client | Items | %s: %s", player:getUsername(), args[1]))
        end
    end
end


Events.OnClientCommand.Add(WastelandLoggingCommands.OnClientCommand)

local function WastelandCheckPlayers()
    local onlinePlayers = getOnlinePlayers();
    for i=0, onlinePlayers:size()-1 do
        local player = onlinePlayers:get(i)
        if not WL_Utils.isStaff(player) then
            local enabledPerms, disabledPerms = WLLogging_CheckPlayer(player)

            if #enabledPerms > 0 then
                local logMessage = string.format("Server | Enabled | %s: %s", player:getUsername(), table.concat(enabledPerms, ", "))
                writeLog("Cheaters", logMessage)
            end

            if #disabledPerms > 0 then
                local logMessage = string.format("Server | Disabled | %s: %s", player:getUsername(), table.concat(disabledPerms, ", "))
                writeLog("Cheaters", logMessage)
            end
        end
    end
end

Events.EveryOneMinute.Add(WastelandCheckPlayers)