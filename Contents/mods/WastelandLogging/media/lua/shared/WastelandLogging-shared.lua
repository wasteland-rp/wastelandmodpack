local playerEnabledCheats = {}

local function doCheatCheck(username, perm, enabled, ep, dp)
    if enabled then
        if not playerEnabledCheats[username][perm] then
            playerEnabledCheats[username][perm] = true
            table.insert(ep, perm)
        end
    else
        if playerEnabledCheats[username][perm] then
            playerEnabledCheats[username][perm] = false
            table.insert(dp, perm)
        end
    end
end

function WLLogging_CheckPlayer(player)
    local username = player:getUsername()
    if not playerEnabledCheats[username] then
        playerEnabledCheats[username] = {}
    end

    local onClient = not isServer()
    local enabledPerms = {}
    local disabledPerms = {}
    doCheatCheck(username, "BuildCheat", player:isBuildCheat() or (onClient and ISBuildMenu.cheat), enabledPerms, disabledPerms)
    doCheatCheck(username, "CanHearAll", player:isCanHearAll(), enabledPerms, disabledPerms)
    doCheatCheck(username, "CanSeeAll", player:isCanSeeAll(), enabledPerms, disabledPerms)
    doCheatCheck(username, "FarmingCheat", player:isFarmingCheat() or (onClient and ISFarmingMenu.cheat), enabledPerms, disabledPerms)
    doCheatCheck(username, "GodMod", player:isGodMod(), enabledPerms, disabledPerms)
    doCheatCheck(username, "HealthCheat", player:isHealthCheat() or (onClient and ISHealthPanel.cheat), enabledPerms, disabledPerms)
    doCheatCheck(username, "MechanicsCheat", player:isMechanicsCheat() or (onClient and ISVehicleMechanics.cheat), enabledPerms, disabledPerms)
    doCheatCheck(username, "MovablesCheat", player:isMovablesCheat() or (onClient and ISMoveableDefinitions.cheat), enabledPerms, disabledPerms)
    doCheatCheck(username, "ShowMPInfos", player:isShowMPInfos(), enabledPerms, disabledPerms)
    doCheatCheck(username, "TimedActionInstant", player:isTimedActionInstant(), enabledPerms, disabledPerms)
    doCheatCheck(username, "UnlimitedCarry", player:isUnlimitedCarry(), enabledPerms, disabledPerms)
    doCheatCheck(username, "UnlimitedEndurance", player:isUnlimitedEndurance(), enabledPerms, disabledPerms)
    doCheatCheck(username, "ZombiesDontAttack", player:isZombiesDontAttack(), enabledPerms, disabledPerms)

    -- disabled because of trueactions
    -- doCheatCheck(username, "NoClip", player:isNoClip(), enabledPerms, disabledPerms)

    if onClient then
        doCheatCheck(username, "FastTeleportMove", ISFastTeleportMove.cheat, enabledPerms, disabledPerms)
        doCheatCheck(username, "BrushToolManager", BrushToolManager.cheat, enabledPerms, disabledPerms)
        doCheatCheck(username, "UnlimitedAmmo", getDebugOptions():getBoolean("Cheat.Player.UnlimitedAmmo"), enabledPerms, disabledPerms)
        doCheatCheck(username, "KnowAllRecipe", getDebugOptions():getBoolean("Cheat.Recipe.KnowAll"), enabledPerms, disabledPerms)

        -- We do this only on client because WL_SafePlayer is client side only
        doCheatCheck(username, "Invisible", (player:isInvisible() or player:isGhostMode()) and (not WL_SafePlayer or not WL_SafePlayer.instance), enabledPerms, disabledPerms)
    end

    return enabledPerms, disabledPerms
end