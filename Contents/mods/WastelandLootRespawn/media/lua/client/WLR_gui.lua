require "GravyUI"

WLR_gui = ISPanelJoypad:derive("WLR_gui")
WLR_gui.instance = nil

function WLR_gui:new()
    if WLR_gui.instance ~= nil then
        WLR_gui.instance:close()
    end
    local fontHgt = getTextManager():MeasureFont(UIFont.Small)
    local fontRatio = fontHgt / 10
    print("fontRatio: " .. tostring(fontRatio) .. ", fontHgt: " .. tostring(fontHgt))
    local o = ISPanelJoypad.new(self, 200, 200, 400 * fontRatio, 200 * fontRatio)
    o:setDisplayCategories()
    o.moveWithMouse = true
    WLR_gui.instance = o
    sendClientCommand(getPlayer(), "WLR", "GetConfigs", {})
    return o
end

function WLR_gui:setDisplayCategories()
    local items = getAllItems()
    local seenCategories = {}
    self.categories = {}
    self.categoriesStatus = {}
    for i=1,items:size() do
        local item = items:get(i-1)
        if item:getDisplayCategory() ~= "" and seenCategories[item:getDisplayCategory()] == nil then
            seenCategories[item:getDisplayCategory()] = true
            table.insert(self.categories, item:getDisplayCategory())
            table.insert(self.categoriesStatus, true)
        end
    end
    table.sort(self.categories)
end

function WLR_gui:initialise()
    ISPanelJoypad.initialise(self)
    self:addToUIManager()
    self:setAlwaysOnTop(true)
    self.borderColor = {r=0.4, g=0.4, b=0.4, a=1}
    self.backgroundColor = {r=0, g=0, b=0, a=1}
    self.rangeX = 100
    self.rangeY = 100
    self.rangeMax = 500
    self.rangeMin = 5
    self.checkLimit = 15
    self.containerRatio = 100
    self.fullnessRatio = 100
    self.allConfigs = {}
    self.is_running = false

    local window = GravyUI.Node(self.width, self.height):pad(2)
    local top, footer = window:rows({1.0, 45}, 5)
    local mainLeft, mainRight = top:cols({0.6, 0.4}, 20)
    local header, leftBody = mainLeft:rows({30, 1}, 5)
    local catFilterLabel, catFilterBody = mainRight:rows({15, 1}, 5)

    local rangeX, rangeY, checkLimit, onlyExplored, container, fullness = leftBody:grid(6, {0.6, 0.4}, 2, 2)
    local footerButtons, footerConfigs = footer:rows({1, 20}, 2)
    local startSlot, closeSlot = footerButtons:cols(2, 5)
    local dropdown, input, saveBtn, deleteBtn = footerConfigs:resize(footerConfigs.width-20, footerConfigs.height):cols({0.5, 0.3, 0.1, 0.1}, 5)

    self.headerLabel = header
    self.catFilterLabel = catFilterLabel
    self.rangeXLabel = rangeX[1]
    self.rangeYLabel = rangeY[1]
    self.checkLimitLabel = checkLimit[1]
    self.onlyExploredLabel = onlyExplored[1]
    self.containerLabel = container[1]
    self.fullnessLabel = fullness[1]

    self:initCategoriesSelector(catFilterBody)
    self.rangeXInput = rangeX[2]:resize(container[2].width, 12):makeSlider(self, self.rangeXInputChange)
    self.rangeYInput = rangeY[2]:resize(container[2].width, 12):makeSlider(self, self.rangeYInputChange)
    self.checkLimitInput = checkLimit[2]:makeTextBox(tostring(self.checkLimit))
    self.onlyExploredInput = onlyExplored[2]:makeTickBox()
    self.containerInput = container[2]:resize(container[2].width, 12):makeSlider(self, self.containerRatioChange)
    self.fullnessInput = fullness[2]:resize(fullness[2].width, 12):makeSlider(self, self.fullnessRatioChange)

    self.toggleButton = startSlot:makeButton("Start", self, self.toggleRunning)
    -- self.pauseButton = pauseSlot:makeButton("Pause", self, self.togglePaused)
    self.closeButton = closeSlot:makeButton("Close", self, self.close)

    self.configComboBox = dropdown:makeComboBox(self, self.configSelected)
    self.configInput = input:makeTextBox("")
    self.configSaveButton = saveBtn:makeButton("Save", self, self.configSave)
    self.configDeleteButton = deleteBtn:makeButton("Delete", self, self.configDelete)

    self:addChild(self.rangeXInput)
    self:addChild(self.rangeYInput)
    self:addChild(self.checkLimitInput)
    self:addChild(self.onlyExploredInput)
    self:addChild(self.containerInput)

    self:addChild(self.fullnessInput)
    self:addChild(self.toggleButton)
    -- self:addChild(self.pauseButton)
    self:addChild(self.closeButton)

    self:addChild(self.configComboBox)
    self:addChild(self.configInput)
    self:addChild(self.configSaveButton)
    self:addChild(self.configDeleteButton)


    self.onlyExploredInput:addOption("")
    self.onlyExploredInput:setSelected(1, true)

    self.rangeXInput:setCurrentValue(self.rangeX)
    self.rangeXInput:setValues(self.rangeMin, self.rangeMax, 1, 5, false)
    self.rangeYInput:setCurrentValue(self.rangeY)
    self.rangeYInput:setValues(self.rangeMin, self.rangeMax, 1, 5, false)
    self.containerInput:setCurrentValue(self.containerRatio)
    self.fullnessInput:setCurrentValue(self.fullnessRatio)

    self.checkLimitInput:setOnlyNumbers(true)
end

function WLR_gui:setConfigs(configs)
    self.allConfigs = {}
    self.configComboBox.options = {}
    self.configComboBox.selected = 0
    self.configComboBox:addOptionWithData("", nil)
    for _,config in pairs(configs) do
        self.configComboBox:addOptionWithData(config.name, config)
        self.allConfigs[config.name] = config
    end
end

function WLR_gui:configSelected()
    if self.configComboBox.selected == 1 then
        return
    end
    local selectedConfig = self.configComboBox:getOptionData(self.configComboBox.selected)
    self:setConfig(selectedConfig)
    self.configInput:setText(selectedConfig.name)
end

-- todo make this smarter, only send new/updated config
function WLR_gui:configSave()
    local currentConfig = self:getCurrentConfig()
    currentConfig.name = self.configInput:getText()
    self.allConfigs[currentConfig.name] = currentConfig
    self.configComboBox:addOptionWithData(currentConfig.name, currentConfig)
    self.configComboBox.selected = #self.configComboBox.options
    sendClientCommand(getPlayer(), "WLR", "StoreConfigs", self.allConfigs)
end

-- todo make this smarter, only send new/updated config
function WLR_gui:configDelete()
    self.allConfigs[self.configInput:getText()] = nil
    self.configComboBox.options = {}
    self.configComboBox.selected = 0
    self.configComboBox:addOptionWithData("", nil)
    for name, config in pairs(self.allConfigs) do
        self.configComboBox:addOptionWithData(name, config)
    end
    sendClientCommand(getPlayer(), "WLR", "StoreConfigs", self.allConfigs)
end


function WLR_gui:getRangeFromRatio(ratio)
    return math.floor(self.rangeMin + (self.rangeMax - self.rangeMin) * ratio / 100)
end

function WLR_gui:getRatioFromRange(range)
    return math.floor((range - self.rangeMin) * 100 / (self.rangeMax - self.rangeMin))
end

function WLR_gui:rangeXInputChange()
    self.rangeX = self.rangeXInput:getCurrentValue()
end

function WLR_gui:rangeYInputChange()
    self.rangeY = self.rangeYInput:getCurrentValue()
end

function WLR_gui:initCategoriesSelector(body)
    self.categoryLabels = {}
    self.categoryCheckboxes = {}

    self.categoryPanel = ISPanel:new(body.left, body.top, body.width, body.height)
    self.categoryPanel.anchorLeft = true
    self.categoryPanel.anchorTop = true
    self.categoryPanel.clip = true
    self.categoryPanel.background = false
    self.categoryPanel.autosetheight = false
    self.categoryPanel:initialise()
    self.categoryPanel:addScrollBars()
    self:addChild(self.categoryPanel)

    self.displayCategoryTickbox = ISTickBox:new(0, 0, body.width - 10, body.height, "", self, self.tickBoxChange)
    self.displayCategoryTickbox:addOption("All", nil)
    self.displayCategoryTickbox:setSelected(1, true)
    for i,category in ipairs(self.categories) do
        self.displayCategoryTickbox:addOption(category, nil)
        self.displayCategoryTickbox:setSelected(i+1, true)
    end
    self.categoryPanel:addChild(self.displayCategoryTickbox)
    self.categoryPanel:setScrollChildren(true)
    self.categoryPanel:setScrollHeight(self.displayCategoryTickbox:getHeight() + self.displayCategoryTickbox.itemHgt)

    self.categoryPanel.onMouseWheel = function(self, d)
        self:setYScroll(self:getYScroll() - d * 16.0)
        return true
    end

    self.displayCategoryTickbox.render = function(self)
        local y = 0
        local c = 1
        local totalHgt = #self.options * self.itemHgt
        y = y + (self.height - totalHgt) / 2
        local textDY = (self.itemHgt - self.fontHgt) / 2
        local boxDY = (self.itemHgt - self.boxSize) / 2
        self._textColor = self._textColor or { r = 1, g = 1, b = 1, a = 1 }

        local catPanel = self:getParent():getParent().categoryPanel
        local scrollY = catPanel:getYScroll()*-1
        local startPos = math.ceil(scrollY / self.itemHgt) + 1
        local endPos = math.min(#self.options, startPos + math.ceil(catPanel.height / self.itemHgt) - 2)

        for i,v in ipairs(self.options) do
            if i >= startPos and i <= endPos then
                if self:isMouseOver() and (self.mouseOverOption == c) and not self.disabledOptions[self.optionsIndex[self.mouseOverOption]] then
                    self:drawRect(self.leftMargin, y+boxDY, self.boxSize, self.boxSize, 1.0, 0.3, 0.3, 0.3)
                else
                    self:drawRectBorder(self.leftMargin, y+boxDY, self.boxSize, self.boxSize, self.borderColor.a, self.borderColor.r, self.borderColor.g, self.borderColor.b)
                end

                if self.joypadFocused and self.joypadIndex == c then
                    self:drawRectBorder(self.leftMargin - 2, y+boxDY - 2, self.width + 2, self.boxSize + 4, 1.0, 0.6, 0.6, 0.6)
                    self:drawRect(self.leftMargin, y+boxDY, self.boxSize, self.boxSize, 1.0, 0.3, 0.3, 0.3)
                end

                if self.selected[c] == true then
                    self:drawTexture(self.tickTexture, self.leftMargin + 3, y+boxDY+2, 1, 1, 1, 1)
                end

                local textColor = self._textColor
                self:getTextColor(i, textColor)

                if self.textures[i] then
                    local imgW = 20
                    local imgH = 20
                    if self.textures[i]:getWidth() < 32 then
                        imgW = imgW / (32/self.textures[i]:getWidth())
                    end
                    if self.textures[i]:getHeight() < 32 then
                        imgH = imgH / (32/self.textures[i]:getHeight())
                    end

                    self:drawTextureScaled(self.textures[i], self.leftMargin + self.boxSize + self.textGap, y+boxDY, imgW,imgH, 1, 1, 1, 1)
                    self:drawText(v, self.leftMargin + self.boxSize + self.textGap + 25, y+textDY, textColor.r, textColor.g, textColor.b, textColor.a, self.font)
                else
                    self:drawText(v, self.leftMargin + self.boxSize + self.textGap, y+textDY, textColor.r, textColor.g, textColor.b, textColor.a, self.font)
                end
            end
            y = y + self.itemHgt
            c = c + 1
        end
    end
end

function WLR_gui:prerender()
    self:checkButtons()

    self:drawRect(0, 0, self.width, self.height, self.backgroundColor.a, self.backgroundColor.r, self.backgroundColor.g, self.backgroundColor.b)
    self:drawRectBorder(0, 0, self.width, self.height, self.borderColor.a, self.borderColor.r, self.borderColor.g, self.borderColor.b)

    self:drawText("Loot Refill GUI", self.headerLabel.left, self.headerLabel.top, 1, 1, 1, 1, UIFont.Medium)

    self:drawText("Allowed Categories", self.catFilterLabel.left, self.catFilterLabel.top, 1, 1, 1, 1, UIFont.Small)
    self:drawText("Range West/East", self.rangeXLabel.left, self.rangeXLabel.top, 1, 1, 1, 1, UIFont.Small)
    self:drawText("Range North/South", self.rangeYLabel.left, self.rangeYLabel.top, 1, 1, 1, 1, UIFont.Small)
    self:drawText("Skip containers > than", self.checkLimitLabel.left, self.checkLimitLabel.top, 1, 1, 1, 1, UIFont.Small)
    self:drawText("Ignore Container Chance", self.containerLabel.left, self.containerLabel.top, 1, 1, 1, 1, UIFont.Small)
    self:drawText("Only Explored Containers", self.onlyExploredLabel.left, self.onlyExploredLabel.top, 1, 1, 1, 1, UIFont.Small)
    self:drawText("Keep Item Chance", self.fullnessLabel.left, self.fullnessLabel.top, 1, 1, 1, 1, UIFont.Small)
end

function WLR_gui:render()
	ISPanelJoypad.render(self)
end

function WLR_gui:checkButtons()
    if self.is_running then
        if self.toggleButton.title ~= "Stop" then
            self.toggleButton:setTitle("Stop")
        end
    elseif not self.is_running then
        if self.toggleButton.title ~= "Start" then
            self.toggleButton:setTitle("Start")
        end
    end
end

function WLR_gui:tickBoxChange(option, value)
    if option == 1 then
        for i,_ in ipairs(self.categories) do
            self.categoriesStatus[i] = value
            self.displayCategoryTickbox:setSelected(i+1, value)
        end
    else
        self.categoriesStatus[option - 1] = value
    end
end

function WLR_gui:setConfig(config)
    self.rangeX = config.rangeX
    self.rangeY = config.rangeY
    self.containerRatio = config.containerRatio
    self.fullnessRatio = config.fillRatio

    self.rangeXInput:setCurrentValue(self.rangeX)
    self.rangeYInput:setCurrentValue(self.rangeY)
    self.containerInput:setCurrentValue(self.containerRatio)
    self.fullnessInput:setCurrentValue(self.fullnessRatio)

    self.checkLimitInput:setText(tostring(config.checkLimit))
    self.onlyExploredInput:setSelected(1, config.onlyExplored)

    for i, category in ipairs(self.categories) do
        if config.allowedCategories[category] == nil then
            config.allowedCategories[category] = true
        end
        self.categoriesStatus[i] = config.allowedCategories[category]
        self.displayCategoryTickbox:setSelected(i+1, config.allowedCategories[category])
    end
end

function WLR_gui:getCurrentConfig()
    local allowedCategories = {}
    for i, category in ipairs(self.categories) do
        allowedCategories[category] = self.categoriesStatus[i]
    end
    local square = getPlayer():getSquare()
    return {
        centerX = square:getX(),
        centerY = square:getY(),
        rangeX = tonumber(self.rangeX),
        rangeY = tonumber(self.rangeY),
        containerRatio = self.containerInput:getCurrentValue(),
        fillRatio = self.fullnessInput:getCurrentValue(),
        checkLimit = tonumber(self.checkLimitInput:getText()),
        onlyExplored = self.onlyExploredInput:isSelected(1),
        allowedCategories = allowedCategories
    }
end

function WLR_gui:toggleRunning()
    if self.is_running then
        sendClientCommand(getPlayer(), 'WLR', 'Stop', {})
        return
    end
    local currentConfig = WLR_gui.instance:getCurrentConfig()
    self.start_x = currentConfig.centerX
    self.start_y = currentConfig.centerY
    self.start_z = getPlayer():getSquare():getZ()
    self.range_x = currentConfig.rangeX
    self.range_y = currentConfig.rangeY
    print("Starting WLR")
    sendClientCommand(getPlayer(), 'WLR', 'Start', currentConfig)
end

function WLR_gui:close()
    if self.is_running then
        sendClientCommand(getPlayer(), 'WLR', 'Stop', {})
    end
    self:setVisible(false)
    self:removeFromUIManager()
    WLR_gui.instance = nil
end

local function map_override(self)
    if not WLR_gui.instance or self.isometric then
        return
    end
    local sTlX = 0
    local sTlY = 0
    local sBrX = 0
    local sBrY = 0
    if WLR_gui.instance.is_running then
        sTlX = WLR_gui.instance.start_x - WLR_gui.instance.range_x
        sTlY = WLR_gui.instance.start_y - WLR_gui.instance.range_y
        sBrX = WLR_gui.instance.start_x + WLR_gui.instance.range_x
        sBrY = WLR_gui.instance.start_y + WLR_gui.instance.range_y
    else
        local square = getPlayer():getSquare()
        if not square then
            return
        end
        local start_x = square:getX()
        local start_y = square:getY()
        local range_x = WLR_gui.instance.rangeX
        local range_y = WLR_gui.instance.rangeY
        sTlX = start_x - range_x
        sTlY = start_y - range_y
        sBrX = start_x + range_x
        sBrY = start_y + range_y
    end
    local tlX = self.mapAPI:worldToUIX(sTlX, sTlY)
    local tlY = self.mapAPI:worldToUIY(sTlX, sTlY)
    local brX = self.mapAPI:worldToUIX(sBrX, sBrY)
    local brY = self.mapAPI:worldToUIY(sBrX, sBrY)
    self:drawRect(tlX, tlY, brX - tlX, brY - tlY, 0.5, 0, 1, 0)

    if WLR_gui.instance.is_running then
        local sq = getPlayer():getSquare()
        if sq then
            local playerX = sq:getX()
            local playerY = sq:getY()
            local range_x = WLR_gui.instance.sizeX or 1
            local range_y = WLR_gui.instance.sizeY or 1
            local tlX = self.mapAPI:worldToUIX(playerX - range_x/2, playerY - range_y/2)
            local tlY = self.mapAPI:worldToUIY(playerX - range_x/2, playerY - range_y/2)
            local brX = self.mapAPI:worldToUIX(playerX + range_x/2, playerY + range_y/2)
            local brY = self.mapAPI:worldToUIY(playerX + range_x/2, playerY + range_y/2)
            self:drawRect(tlX, tlY, brX - tlX, brY - tlY, 0.25, 0, 0, 1)
        end
    end
end

-- local original_ISWorldMapSymbolTool_render = ISWorldMapSymbolTool.render
-- function ISWorldMapSymbolTool:render()
--     original_ISWorldMapSymbolTool_render(self)
--     map_override(self)
-- end

local original_ISWorldMap_render = ISWorldMap.render
function ISWorldMap:render()
    original_ISWorldMap_render(self)
    map_override(self)
    if WLR_gui.instance then WLR_gui.instance:render() end
end

local function OnServerCommand(module, command, args)
    if module ~= "WLR" then return end
    if not WLR_gui.instance then return end

    if command == "GetConfigs" then
        WLR_gui.instance:setConfigs(args or {})
    end
    if command == "Start" then
        WLR_gui.instance.is_running = true
    end
    if command == "Done" then
        WLR_gui.instance.is_running = false
        local player = getPlayer()
        player:setX(WLR_gui.instance.start_x)
        player:setY(WLR_gui.instance.start_y)
        player:setZ(WLR_gui.instance.start_z)
        player:setLx(WLR_gui.instance.start_x)
        player:setLy(WLR_gui.instance.start_y)
        player:setLz(WLR_gui.instance.start_z)
    end
    if command == "Teleport" then
        local player = getPlayer()
        player:setX(args.x)
        player:setY(args.y)
        player:setZ(0)
        player:setLx(args.x)
        player:setLy(args.y)
        player:setLz(0)
        WLR_gui.instance.sizeX = args.sizeX
        WLR_gui.instance.sizeY = args.sizeY
    end
end
Events.OnServerCommand.Add(OnServerCommand)