if isClient() then return end

local Json = require "WLR_Auto_json"

WLR_Auto = WLR_Auto or {}

--- @class ChunkCache
--- @field definitionId string
--- @field lastRespawn number
--- @field ready boolean

--- @class WLR_Auto.Data
--- @field definitions table<string, WLR_Auto.Definition>
--- @field chunkCache table<string, ChunkCache>
WLR_Auto.Data = WLR_Auto.Data or WLBaseObject:derive("Data")

--- @return WLR_Auto.Data
function WLR_Auto.Data:new()
    local o = self:super()
    o.definitions = {}
    o.chunkCache = {}
    return o
end

--- @param self WLR_Auto.Data
function WLR_Auto.Data:loadDefinitions()
    WLR_Auto.DebugLog("WLR_Auto.Data:loadDefinitions() - Loading definitions")

    local fileReaderObj = getFileReader("WastelandAutoLootRespawnConfig.json", false)
    local json = ""
    if fileReaderObj then
        local line = fileReaderObj:readLine()
        while line ~= nil do
            json = json .. line
            line = fileReaderObj:readLine()
        end
        fileReaderObj:close()
    end

    if json and json ~= "" then
        WLR_Auto.DebugLog("WLR_Auto.Data:loadDefinitions() - Parsing definitions")
        local decoded = Json.Decode(json)
        if decoded then
            for _, definitionData in ipairs(decoded) do
                local definition = WLR_Auto.Definition:new(definitionData)
                if definition.enabled then
                    self.definitions[definition.id] = definition
                    WLR_Auto.DebugLog("WLR_Auto.Data:loadDefinitions() - Loaded definition: " .. definition.id)
                end
            end
        end
    else
        WLR_Auto.DebugLog("WLR_Auto.Data:loadDefinitions() - No definitions found, writing default")
        local defaultConfig = WLR_Auto.Definition.GetDefaultConfig()
        local fileWriterObj = getFileWriter("WastelandAutoLootRespawnConfig.json", true, false)
        fileWriterObj:write(Json.Encode(defaultConfig))
        fileWriterObj:close()
    end
end

--- @param self WLR_Auto.Data
--- @param rawData table<string, table<string, ChunkCache>>
function WLR_Auto.Data:loadChunkCache(rawData)
    self.chunkCache = rawData
    if WLR_Auto.Config.Trace then
        local count = 0
        local readyCount = 0
        for _, chunk in pairs(self.chunkCache) do
            count = count + 1
            if chunk.ready then
                readyCount = readyCount + 1
            end
        end
        WLR_Auto.TraceLog("WLR_Auto.Data:loadChunkCache() - Loaded chunk cache: " .. count .. " chunks, " .. readyCount .. " ready")
    end
end

--- @param self WLR_Auto.Data
function WLR_Auto.Data:forceAll()
    for _, chunk in pairs(self.chunkCache) do
        chunk.ready = true
    end
end

--- @param self WLR_Auto.Data
--- @param range WLR_Auto.Range
--- @return WLR_Auto.Definition|nil
function WLR_Auto.Data:getDefinitionsReadyInChunk(range)
    local chunkCache = self:_getChunkCache(range)
    if chunkCache and ((chunkCache.ready and ZombRand(2) == 0) or WLR_Auto.Config.AlwaysRespawn) then
        local definition = self.definitions[chunkCache.definitionId]
        if definition then
            WLR_Auto.DebugLog("WLR_Auto.Data:getDefinitionsReadyInChunk() - (" .. tostring(range) .. ") respawn is ready: " .. definition.id)
            chunkCache.ready = false
            chunkCache.lastRespawn = getTimestamp()
            return definition
        end
    end
    return nil
end

--- @param self WLR_Auto.Data
function WLR_Auto.Data:checkForNeededRespawn()
    local currentTime = getTimestamp()
    for chunk, chunkCache in pairs(self.chunkCache) do
        if not chunkCache.ready then
            local definition = self.definitions[chunkCache.definitionId]
            if not definition then
                WLR_Auto.TraceLog("WLR_Auto.Data:checkForNeededRespawn() - Removing chunkCache: " .. tostring(chunk))
                self.chunkCache[chunk] = nil
            else
                local timeSinceLastRespawn = currentTime - chunkCache.lastRespawn
                if timeSinceLastRespawn >= definition.frequencyHours * 60 * 60 then
                    WLR_Auto.TraceLog("WLR_Auto.Data:checkForNeededRespawn() - Chunk is due for respawn: " .. tostring(chunk))
                    chunkCache.definitionId = definition.id
                    chunkCache.ready = true
                end
            end
        end
    end
end

--- @param self WLR_Auto.Data
--- @param range WLR_Auto.Range
--- @return ChunkCache|nil
function WLR_Auto.Data:_getChunkCache(range)
    local key = self:_getChunkKey(range.x1, range.y1)
    if self.chunkCache[key] then
        return self.chunkCache[key]
    end

    local definition = self:_getFirstDefinitionInChunk(range)
    if definition then
        WLR_Auto.TraceLog("WLR_Auto.Data:_getCachedChunk() - Creating new chunkCache: " .. key)
        local cache = {
            definitionId = definition.id,
            lastRespawn = getTimestamp(), -- If never respawned, assume now
            ready = false,
        }
        self.chunkCache[key] = cache
        return cache
    else
        WLR_Auto.TraceLog("WLR_Auto.Data:_getCachedChunk() - No definition found for chunk: " .. key)
    end
    return nil
end

--- @param self WLR_Auto.Data
--- @param range WLR_Auto.Range
--- @return WLR_Auto.Definition|nil
function WLR_Auto.Data:_getFirstDefinitionInChunk(range)
    for _, definition in pairs(self.definitions) do
        if definition:intersects(range) then
            return definition
        end
    end
    return nil
end

--- @param self WLR_Auto.Data
function WLR_Auto.Data:_getChunkKey(x, y)
    return tostring(x) .. "," .. tostring(y)
end

