if isClient() then return end
WLR_Auto = WLR_Auto or {}

--- @class WLR_Auto.Instance
--- @field definition WLR_Auto.Definition
--- @field range WLR_Auto.Range
--- @field stage string
--- @field parts table<number, WLR_Auto.Range>
WLR_Auto.Instance = WLR_Auto.Instance or WLBaseObject:derive("Instance")

--- @param definition WLR_Auto.Definition
--- @param range WLR_Auto.Range
--- @return WLR_Auto.Instance
function WLR_Auto.Instance:new(definition, range)
    local o = self:super()
    o.definition = definition
    o.range = range
    o:_init()
    return o
end

local function checkIsUnclaimed(vehicle)
    if WastelandAutoClaimsCore then
        return not WastelandAutoClaimsCore:isClaimed(vehicle)
    end
    if AVCS then
        local vehicleSQL = nil
        if type(vehicle) ~= "number" then
            vehicleSQL = AVCS.getVehicleID(vehicle)
        else
            vehicleSQL = vehicle
        end
        if vehicleSQL == nil then
            return true
        end
        if AVCS.dbByVehicleSQLID[vehicleSQL] == nil then
            return true
        end
        return false
    end
    return false
end

--- @param self WLR_Auto.Instance
--- @return boolean isDone
function WLR_Auto.Instance:run()
    if not self.parts[1] then
        return true
    end

    --- @type WLR_Auto.Range
    local part = table.remove(self.parts, 1)
    local containers, vehicles = self:_getContainersAndVehiclesInPart(part)
    if containers[1] then -- are containers to respawn
        local containerCompare = self.definition.containerChance * 100
        WLR_Auto.DebugLog("WLR_Auto.Instance:_stageRespawnNextPart() - Respawn " .. #containers .. " containers in : " .. tostring(part))
        for _, container in ipairs(containers) do
            if ZombRand(0, 100) <= containerCompare then
                self:_respawnContainer(container)
                self:_pruneContainer(container)
                local parent = container:getParent()
                if parent then
                    if parent:getModData().WLL_PadLockKeyId then
                        parent:getModData().WLL_PadLockKeyId = nil
                        parent:transmitModData()
                    end
                    ItemPicker.updateOverlaySprite(parent)
                    sendItemsInContainer(parent, container)
                    if WLR_Auto.Config.Trace then
                        local x = parent:getX()
                        local y = parent:getY()
                        local z = parent:getZ()
                        WLR_Auto.TraceLog("WLR_Auto.Instance:_stageRespawnNextPart() - Respawned Container: " .. tostring(x) .. ", " .. tostring(y) .. ", " .. tostring(z))
                    end
                end
            end
        end
        if getActivatedMods():contains("WastelandContainerLocks") and self.definition.chanceLocked > 0 then
            local anyLocked = false
            for _, container in ipairs(containers) do
                local parent = container:getParent()
                if parent and parent:getModData().WLL_PadLockKeyId then
                    anyLocked = true
                    break
                end
            end
            if not anyLocked then
                WLR_Auto.TraceLog("WLR_Auto.Instance:_stageRespawnNextPart() - No Locked Containers: " .. tostring(part))
                for _, container in ipairs(containers) do
                    if ZombRand(self.definition.chanceLocked) == 0 then
                        local parent = container:getParent()
                        if parent then
                            parent:getModData().WLL_PadLockKeyId = ZombRand(1, 2000000000)
                            self:_respawnLockedLoot(container)
                            WLR_Auto.DebugLog("WLR_Auto.Instance:_stageRespawnNextPart() - Locked Container: " .. tostring(parent:getX()) .. ", " .. tostring(parent:getY()) .. ", " .. tostring(parent:getZ()))
                            ItemPicker.updateOverlaySprite(parent)
                            sendItemsInContainer(parent, container)
                            parent:transmitModData()
                        else
                            WLR_Auto.DebugLog("WLR_Auto.Instance:_stageRespawnNextPart() - Missing Parent")
                        end
                    end
                end
            end
        end
    end

    for _, vehicle in ipairs(vehicles) do
        for i=1,vehicle:getPartCount() do
            local vPart = vehicle:getPartByIndex(i-1)
            if not vehicle:isEngineStarted() and vPart:isContainer() and vPart:getContainerContentType() == "Gasoline" and vPart:getContainerContentAmount() <= 2 then
                if ZombRand(100) <= self.definition.gasFillChance then
                    local amount = ZombRand(self.definition.gasFillRange[1], self.definition.gasFillRange[2])
                    vPart:setContainerContentAmount(amount)
                    vehicle:transmitPartModData(vPart)
                    WLR_Auto.TraceLog("WLR_Auto.Instance:_stageRespawnNextPart() - Filled Gasoline: " .. tostring(vehicle:getX()) .. ", " .. tostring(vehicle:getY()) .. ", " .. tostring(vehicle:getZ()) .. " - " .. tostring(amount))
                end
            end
        end
    end

    return not self.parts[1]
end

--- @param self WLR_Auto.Instance
function WLR_Auto.Instance:_init()
    self.parts = {}
    local partSize = 10
    for x = self.range.x1, self.range.x2, partSize do
        for y = self.range.y1, self.range.y2, partSize do
            table.insert(self.parts, WLR_Auto.Range:new(x, y, x + partSize - 1, y + partSize - 1))
        end
    end
    WLR_Auto.TraceLog("WLR_Auto.Instance:_stageInit() - Parts: " .. #self.parts)
end

--- @param self WLR_Auto.Instance
--- @param part WLR_Auto.Range
--- @return table<number, IsoObject>, table<number, BaseVehicle>
function WLR_Auto.Instance:_getContainersAndVehiclesInPart(part)
    local containers = {}
    local vehicles = {}
    for x = part.x1, part.x2, 1 do
    for y = part.y1, part.y2, 1 do
    for z = 0, 7, 1 do
        local square = getSquare(x, y, z)
        if square then
            self:_getContainersInSquare(square, containers)
            self:_getVehiclesInSquare(square, vehicles)
        end
    end
    end
    end
    WLR_Auto.TraceLog("WLR_Auto.Instance:_getContainersInPart() - Containers: " .. #containers .. ", Vehicles: " .. #vehicles)
    return containers, vehicles
end

--- @param self WLR_Auto.Instance
--- @param square IsoGridSquare
--- @param containers table<number, IsoObject>
function WLR_Auto.Instance:_getContainersInSquare(square, containers)
    if WWP_Server and WWP_Server.IsWorkplace(square:getX(), square:getY(), square:getZ()) then
        return
    end

    if  square:getRoom() and
        square:getRoom():getRoomDef() and
        square:getRoom():getRoomDef():getProceduralSpawnedContainer() and
        not SafeHouse.getSafeHouse(square)
    then
        local objects = square:getObjects()
        for j = 0, objects:size() - 1, 1 do
            local object = objects:get(j)
            local cnt = object:getContainerCount()-1
            for k=0, cnt do
                local container = object:getContainerByIndex(k)
                if container then
                    local isLocked = false
                    local parent = container:getParent()
                    if parent then
                        local modData = parent:getModData()
                        if modData.WLL_StaffLockedBy or modData.WLL_FrozenBy then
                            isLocked = true
                        end
                    end
                    if  not isLocked and
                        container:isExplored() and
                        container:getItems():size() < self.definition.itemCountToIgnore
                    then
                        table.insert(containers, container)
                    end
                end
            end
        end
    end
end

--- @param self WLR_Auto.Instance
--- @param square IsoGridSquare
--- @param containers table<number, BaseVehicle>
function WLR_Auto.Instance:_getVehiclesInSquare(square, containers)
    if WWP_Server and WWP_Server.IsWorkplace(square:getX(), square:getY(), square:getZ()) then
        return
    end

    local movingObjects = square:getMovingObjects()
    for i = 0, movingObjects:size() - 1, 1 do
        local object = movingObjects:get(i)
        if instanceof(object, "BaseVehicle") then
            if checkIsUnclaimed(object) then
                table.insert(containers, object)
            end
        end
    end

    return containers
end

--- @param self WLR_Auto.Instance
--- @param container IsoObject
function WLR_Auto.Instance:_respawnContainer(container)
    container:getSourceGrid():getRoom():getRoomDef():getProceduralSpawnedContainer():clear()
    container:removeItemsFromProcessItems()
    container:clear()
    ItemPicker.fillContainer(container, nil)
    container:setExplored(true)
end

local itemsForLockedContainers = {
    {item = "Base.Bullets9mm", chance = 10},
    {item = "Base.Bullets45", chance = 10},
    {item = "Base.Bullets44", chance = 10},
    {item = "Base.Bullets38", chance = 10},
    {item = "Base.ShotgunShells", chance = 10},
    {item = "Base.223Bullets", chance = 10},
    {item = "Base.308Bullets", chance = 10},
    {item = "Base.556Bullets", chance = 10},
    {item = "Base.Bullets4440", chance = 10},
    {item = "Base.Bullets357", chance = 10},
    {item = "Base.Bullets3006", chance = 10},

    {item = "Base.CBX_ANAT", chance = 2}, -- Assult Backpack
    {item = "Base.CBX_Ras_amry", chance = 2}, -- Bank Robber Chest Rig
    {item = "Base.BackSlingBackpack", chance = 2}, -- Black Backpack
    {item = "Base.CBX_Sumk_7_L", chance = 2}, -- Drop Leg Pouch
    {item = "Base.CBX_HR", chance = 2}, -- Field Backpack
    {item = "Base.CBX_RUKSAK2", chance = 2}, -- SPOSN Tortilla Pack

    {item = "AuthenticZClothing.Vest_BulletBlack", chance = 1}, -- Black Bulletproof Vest
    {item = "Base.Vest_BulletArmy_Urban", chance = 1}, -- Urban Camo Bulletproof Vest
    {item = "Base.Vest_BulletSwap", chance = 1}, -- Swat Vest

    {item = "Base.22Silencer", chance = 1},
    {item = "Base.Katana", chance = 1},
    {item = "UndeadSurvivor.StalkerKnife", chance = 1},

    {item = "Base.GoldCurrencyTen", chance = 10},
    {item = "Base.GoldCurrencyFifty", chance = 1},
}

function WLR_Auto.Instance:_respawnLockedLoot(container)
    container:getSourceGrid():getRoom():getRoomDef():getProceduralSpawnedContainer():clear()
    container:removeItemsFromProcessItems()
    container:clear()
    container:setExplored(true)
    for i = 1, ZombRand(1, 4) do
        local item = WL_Utils.weightedRandom(itemsForLockedContainers).item
        container:AddItem(item)
    end
    if ZombRand(100) == 0 then
        local item = container:AddItem("Base.PetrolCan")
        item:setUsedDelta(ZombRand(10, 90) / 100)
    end
end

--- @param self WLR_Auto.Instance
--- @param container IsoObject
function WLR_Auto.Instance:_pruneContainer(container)
    local toRemove = {}
    local items = container:getItems()
    local numItems = items:size()
    local fillCompare = self.definition.itemChance * 100
    if items and numItems > 0 then
        for i=0, numItems-1 do
            local item = items:get(i)
            if self.definition.ignoredCategories[item:getDisplayCategory()] or ZombRand(0, 100) > fillCompare then
                table.insert(toRemove, item)
            end
        end
    end
    for _, v in ipairs(items) do
        container:DoRemoveItem(v)
    end
end