local Json = require "json"

local function GetConfigs(player)
    local fileReaderObj = getFileReader("lootrespawn_configs.json", true)
    local json = ""
    local line = fileReaderObj:readLine()
    while line ~= nil do
        json = json .. line
        line = fileReaderObj:readLine()
    end
    fileReaderObj:close()
    if json and json ~= "" then
        local configs = Json.Decode(json)
        if configs then
            sendServerCommand(player, "WLR", "StoredConfigs", configs)
            return
        end
    end
    sendServerCommand(player, "WLR", "StoredConfigs", {})
end

local function StoreConfigs(player, configs)
    local fileWriterObj = getFileWriter("lootrespawn_configs.json", true, false)
    local json = Json.Encode(configs)
    fileWriterObj:write(json)
    fileWriterObj:close()
end

local function Start(player, config)
    WLR_SpawnSystem:init(player, config)
end

local function Stop(player)
    if WLR_SpawnSystem.instances[player] then
        WLR_SpawnSystem.instances[player]:stop()
    end
end

-- todo make better by having AddConfig, EditConfig, and RemoveConfig
Events.OnClientCommand.Add(function (module, command, player, args)
    if module ~= "WLR" then return end

    if command == "GetConfigs" then
        GetConfigs(player)
    end

    if command == "StoreConfigs" then
        StoreConfigs(player, args)
    end

    if command == "Start" then
        Start(player, args)
    end

    if command == "Stop" then
        Stop(player)
    end
end)