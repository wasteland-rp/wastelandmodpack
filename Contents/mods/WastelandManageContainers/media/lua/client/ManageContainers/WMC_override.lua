if not getActivatedMods():contains("manageContainers") then return end

-- require "ManageContainers/placeInContainers"

Events.OnFillInventoryObjectContextMenu.Remove(ISInventoryPaneContextMenu.DetectContainers)
ISInventoryPaneContextMenu.DetectContainers = function(player, context, items)
	local firstItem = items[1]
	if firstItem == nil then
		return
	end
	local playerObj = getSpecificPlayer(player)
	local validContainer = ISInventoryPaneContextMenu.ValidNearbyContainers(playerObj)

	if validContainer and not ISInventoryPaneContextMenu.isAllFav(items) then
		context:addOption(getText("ContextMenu_Put_Assigned_Containers"), items, ISInventoryPaneContextMenu.TransferItems, player, true)
	end
end
Events.OnFillInventoryObjectContextMenu.Add(ISInventoryPaneContextMenu.DetectContainers)