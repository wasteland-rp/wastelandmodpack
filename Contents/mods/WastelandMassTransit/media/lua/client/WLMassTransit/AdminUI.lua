WLMassTransitAdminPanel = ISCollapsableWindow:derive("WLMassTransitAdminPanel")
WLMassTransitAdminPanel.instance = nil

function WLMassTransitAdminPanel:show(routeId)
    if self.instance then
        self.instance:close()
    end
    local scale = getTextManager():MeasureStringY(UIFont.Small, "XXX") / 12
    local w = 600 * scale
    local h = 400 * scale
    local o = WLMassTransitAdminPanel:new(getCore():getScreenWidth()/2-w/2,getCore():getScreenHeight()/2-h/2, w, h)
    o.scale = scale
    setmetatable(o, self)
    self.__index = self
    o:initialise(routeId)
    o:addToUIManager()
    self.instance = o
    return o
end

function WLMassTransitAdminPanel:initialise(routeId)
    ISCollapsableWindow.initialise(self)
    self.page = 0
    self.moveWithMouse = true
    self:setResizable(false)

    self.routeId = routeId
    if routeId then
        self.route = WLMassTransitSystem:getRoute(routeId)
    else
        self.route = nil
    end
    local win = GravyUI.Node(self.width, self.height, self):pad(5, 21, 5, 16)

    local routeSection, stopsSection, pageSection = win:rows({0.4, 0.6, 16 * self.scale}, 10 * self.scale)
    local routeHeaderSection, routeConfigSecion = routeSection:rows({0.3, 0.7}, 10)
    local routeNameSection, routeAreaSection, routeEntranceSection = routeHeaderSection:cols({0.3, 0.4, 0.3}, 20 * self.scale)

    local routeNameLabel, routeNameInput = routeNameSection:rows({0.2, 0.8}, 15)
    self.routeNameLabel = routeNameLabel:makeLabel("Route Name", UIFont.Small, {r=1, g=1, b=1, a=1}, "left")
    self.routeNameInput = routeNameInput:makeTextBox("")

    local routeAreaLabel, routeAreaInput = routeAreaSection:rows({0.2, 0.8}, 5)
    self.routeAreaLabel = routeAreaLabel:makeLabel("Area", UIFont.Small, {r=1, g=1, b=1, a=1}, "left")
    self.routeAreaInput = routeAreaInput:makeAreaPicker()
    self.routeAreaInput.showAlways = false

    local routeEntranceLabel, routeEntranceInput = routeEntranceSection:rows({0.2, 0.8}, 5)
    self.routeEntranceLabel = routeEntranceLabel:makeLabel("Entrance", UIFont.Small, {r=1, g=1, b=1, a=1}, "left")
    self.routeEntranceInput = routeEntranceInput:makePointPicker()
    self.routeEntranceInput.showAlways = false

    local timesCol, weightCol, buttonsCol = routeConfigSecion:cols({0.4, 0.4, 0.2}, 5)
    local typeItem, costItem, timeStop, timeTransit = timesCol:rows(4, 5)

    local typeLabel, typeInput = typeItem:cols({0.3, 0.7}, 5)
    local costItemLabel, costItemInput = costItem:cols({0.3, 0.7}, 5)
    local timeStopLabel, timeStopInput = timeStop:cols({0.3, 0.7}, 5)
    local timeTransitLabel, timeTransitInput = timeTransit:cols({0.3, 0.7}, 5)

    self.typeLabel = typeLabel:makeLabel("Type", UIFont.Small, {r=1, g=1, b=1, a=1}, "right")
    self.typeInput = typeInput:makeComboBox()
    self.typeInput:addOption("Bus")
    self.typeInput:addOption("Train")
    self.typeInput:addOption("Boat")
    self.typeInput:addOption("Plane")

    self.costItemLabel = costItemLabel:makeLabel("Cost Item", UIFont.Small, {r=1, g=1, b=1, a=1}, "right")
    self.costItemInput = costItemInput:makeTextBox("")

    self.timeStopLabel = timeStopLabel:makeLabel("Time Stop", UIFont.Small, {r=1, g=1, b=1, a=1}, "right")
    self.timeStopInput = timeStopInput:makeTextBox("")
    self.timeStopInput:setOnlyNumbers(true)

    self.timeTransitLabel = timeTransitLabel:makeLabel("Time Transit", UIFont.Small, {r=1, g=1, b=1, a=1}, "right")
    self.timeTransitInput = timeTransitInput:makeTextBox("")
    self.timeTransitInput:setOnlyNumbers(true)

    local weightMax, weightFree, weightUnit, weightCost = weightCol:rows(4, 5)
    local weightMaxLabel, weightMaxInput = weightMax:cols({0.5, 0.5}, 5)
    local weightFreeLabel, weightFreeInput = weightFree:cols({0.5, 0.5}, 5)
    local weightUnitLabel, weightUnitInput = weightUnit:cols({0.5, 0.5}, 5)
    local weightCostLabel, weightCostInput = weightCost:cols({0.5, 0.5}, 5)

    self.weightMaxLabel = weightMaxLabel:makeLabel("Weight Max", UIFont.Small, {r=1, g=1, b=1, a=1}, "right")
    self.weightMaxInput = weightMaxInput:makeTextBox("")
    self.weightMaxInput:setOnlyNumbers(true)

    self.weightFreeLabel = weightFreeLabel:makeLabel("Weight Free", UIFont.Small, {r=1, g=1, b=1, a=1}, "right")
    self.weightFreeInput = weightFreeInput:makeTextBox("")
    self.weightFreeInput:setOnlyNumbers(true)

    self.weightUnitLabel = weightUnitLabel:makeLabel("Weight Unit", UIFont.Small, {r=1, g=1, b=1, a=1}, "right")
    self.weightUnitInput = weightUnitInput:makeTextBox("")
    self.weightUnitInput:setOnlyNumbers(true)

    self.weightCostLabel = weightCostLabel:makeLabel("Weight Cost", UIFont.Small, {r=1, g=1, b=1, a=1}, "right")
    self.weightCostInput = weightCostInput:makeTextBox("")
    self.weightCostInput:setOnlyNumbers(true)

    local saveButton, cancelButton, deleteButton = buttonsCol:rows(3, 5)
    self.saveButton = saveButton:resize(50 * self.scale, 20 * self.scale):makeButton("Save", self, self.save)
    self.cancelButton = cancelButton:resize(50 * self.scale, 20 * self.scale):makeButton("Cancel", self, self.close)
    self.deleteButton = deleteButton:resize(50 * self.scale, 20 * self.scale):makeButton("Delete", self, self.delete)

    self.stopsList = {}
    local stopsListRows = {stopsSection:grid({12 * self.scale, 0.2, 0.2, 0.2, 0.2, 0.2}, {0.1, 0.2, 0.1, 0.3, 0.2, 0.1}, 5, 20 * self.scale)}

    stopsListRows[1][1]:makeLabel("Move", UIFont.Small, {r=1, g=1, b=1, a=1}, "center")
    stopsListRows[1][2]:makeLabel("Name", UIFont.Small, {r=1, g=1, b=1, a=1}, "center")
    stopsListRows[1][3]:makeLabel("Cost", UIFont.Small, {r=1, g=1, b=1, a=1}, "center")
    stopsListRows[1][4]:makeLabel("Area", UIFont.Small, {r=1, g=1, b=1, a=1}, "center")
    stopsListRows[1][5]:makeLabel("Exit", UIFont.Small, {r=1, g=1, b=1, a=1}, "center")
    stopsListRows[1][6]:makeLabel("Delete", UIFont.Small, {r=1, g=1, b=1, a=1}, "center")

    for i=2,6 do
        local upButton, downButton = stopsListRows[i][1]:rows(2, 2)
        local areaPicker = stopsListRows[i][4]:makeAreaPicker()
        local pointPicker = stopsListRows[i][5]:makePointPicker()
        areaPicker.showAlways = false
        pointPicker.showAlways = false
        local deleteButton, saveButton = stopsListRows[i][6]:rows(2, 2)
        local costInput = stopsListRows[i][3]:resize(stopsListRows[i][3].width, stopsListRows[i][3].height/2):makeTextBox("")
        costInput:setOnlyNumbers(true)
        table.insert(self.stopsList, {
            moveUpButton = upButton:makeButton("Up", self, self.moveStop, {i-1, -1}),
            moveDownButton = downButton:makeButton("Down", self, self.moveStop, {i-1, 1}),
            nameInput = stopsListRows[i][2]:resize(stopsListRows[i][2].width, stopsListRows[i][2].height/2):makeTextBox(""),
            costInput = costInput,
            areaInput = areaPicker,
            exitInput = pointPicker,
            deleteButton = deleteButton:makeButton("Delete", self, self.deleteStop, {i-1}),
            saveButton = saveButton:makeButton("Save", self, self.saveStop, {i-1})
        })
    end

    local prevPage, addStop, nextPage = pageSection:cols(3, 5)
    self.prevPageButton = prevPage:resize(50 * self.scale, 12 * self.scale):makeButton("Prev", self, self.prevPage)
    self.addStopButton = addStop:resize(50 * self.scale, 12 * self.scale):makeButton("Add Stop", self, self.addStop)
    self.nextPageButton = nextPage:resize(50 * self.scale, 12 * self.scale):makeButton("Next", self, self.nextPage)

    self:updateUi()
end

function WLMassTransitAdminPanel:updateUi()
    if not self.route then
        local player = getPlayer()
        self:setHeight(200 * self.scale)
        self.routeAreaInput:setValue({
            x1 = math.floor(player:getX())-5,
            y1 = math.floor(player:getY())-5,
            z1 = math.floor(player:getZ()),
            x2 = math.floor(player:getX())+5,
            y2 = math.floor(player:getY())+5,
            z2 = math.floor(player:getZ())
        })
        self.routeEntranceInput:setValue({
            x = math.floor(player:getX()),
            y = math.floor(player:getY()),
            z = math.floor(player:getZ())
        })
        self.typeInput:select("Bus")
        self.costItemInput:setText("")
        self.timeStopInput:setText("15")
        self.timeTransitInput:setText("30")
        self.weightMaxInput:setText("0")
        self.weightFreeInput:setText("0")
        self.weightUnitInput:setText("0")
        self.weightCostInput:setText("0")
        self.saveButton:setTitle("Create")
        self.deleteButton:setVisible(false)
        self.prevPageButton:setVisible(false)
        self.addStopButton:setVisible(false)
        self.nextPageButton:setVisible(false)
        for i=1,5 do
            self.stopsList[i].moveUpButton:setVisible(false)
            self.stopsList[i].moveDownButton:setVisible(false)
            self.stopsList[i].nameInput:setVisible(false)
            self.stopsList[i].costInput:setVisible(false)
            self.stopsList[i].areaInput:setVisible(false)
            self.stopsList[i].exitInput:setVisible(false)
            self.stopsList[i].deleteButton:setVisible(false)
            self.stopsList[i].saveButton:setVisible(false)
        end
    else
        self:setHeight(400 * self.scale)
        self.costItemInput:setText(self.route.costItem)
        self.typeInput:select(self.route.type)
        self.routeNameInput:setText(self.route.name)
        self.routeAreaInput:setValue(self.route.area)
        self.routeEntranceInput:setValue(self.route.entrance)
        self.timeStopInput:setText(tostring(self.route.timeAtStops))
        self.timeTransitInput:setText(tostring(self.route.timeInTransit))
        self.weightMaxInput:setText(tostring(self.route.weightMax))
        self.weightFreeInput:setText(tostring(self.route.weightFree))
        self.weightUnitInput:setText(tostring(self.route.weightPerUnit))
        self.weightCostInput:setText(tostring(self.route.weightCostPerUnit))
        self.saveButton:setTitle("Save")
        self.deleteButton:setVisible(true)
        local hasNewStop = false
        for _,stop in pairs(self.route.stops) do
            if stop.isNew then
                hasNewStop = true
                break
            end
        end
        self.addStopButton:setVisible(not hasNewStop)

        local totalStops = 0
        for _,_ in pairs(self.route.stops) do
            totalStops = totalStops + 1
        end

        for i=1,5 do
            local stopIdx = i + self.page * 5
            local stopName = self.route.order[stopIdx]
            local stop = self.route.stops[stopName]
            if stop then
                self.stopsList[i].moveUpButton:setVisible(not stop.isNew and stopIdx > 1)
                self.stopsList[i].moveDownButton:setVisible(not stop.isNew and stopIdx < totalStops)
                self.stopsList[i].nameInput:setVisible(true)
                self.stopsList[i].nameInput:setText(stop.name)
                self.stopsList[i].costInput:setVisible(true)
                if stop.cost then
                    self.stopsList[i].costInput:setText(tostring(stop.cost))
                end
                self.stopsList[i].areaInput:setVisible(true)
                if stop.area then
                    self.stopsList[i].areaInput:setValue(stop.area)
                end
                self.stopsList[i].exitInput:setVisible(true)
                if stop.exit then
                    self.stopsList[i].exitInput:setValue(stop.exit)
                end
                self.stopsList[i].deleteButton:setVisible(true)
                self.stopsList[i].saveButton:setVisible(true)
            else
                self.stopsList[i].moveUpButton:setVisible(false)
                self.stopsList[i].moveDownButton:setVisible(false)
                self.stopsList[i].nameInput:setVisible(false)
                self.stopsList[i].costInput:setVisible(false)
                self.stopsList[i].areaInput:setVisible(false)
                self.stopsList[i].exitInput:setVisible(false)
                self.stopsList[i].deleteButton:setVisible(false)
                self.stopsList[i].saveButton:setVisible(false)
            end
        end
        if totalStops > self.page * 5 + 5 then
            self.nextPageButton:setEnable(true)
        else
            self.nextPageButton:setEnable(false)
        end
        if self.page > 0 then
            self.prevPageButton:setEnable(true)
        else
            self.prevPageButton:setEnable(false)
        end
    end
end

function WLMassTransitAdminPanel:close()
    self:setVisible(false)
    self:removeFromUIManager()
    WLMassTransitAdminPanel.instance = nil
end

function WLMassTransitAdminPanel:save()
    if not self.routeId then
        self.routeId = getRandomUUID()
        WLMassTransitSystem:addTransitRoute(getPlayer(), self.routeId, {
            name = self.routeNameInput:getText(),
            area = self.routeAreaInput:getValue(),
            entrance = self.routeEntranceInput:getValue(),
            type = self.typeInput:getSelectedText(),
            costItem = self.costItemInput:getText(),
            timeAtStops = tonumber(self.timeStopInput:getText()),
            timeInTransit = tonumber(self.timeTransitInput:getText()),
            weightMax = tonumber(self.weightMaxInput:getText()),
            weightFree = tonumber(self.weightFreeInput:getText()),
            weightPerUnit = tonumber(self.weightUnitInput:getText()),
            weightCostPerUnit = tonumber(self.weightCostInput:getText())
        })
    else
        WLMassTransitSystem:updateTransitRoute(getPlayer(), self.routeId, {
            name = self.routeNameInput:getText(),
            area = self.routeAreaInput:getValue(),
            entrance = self.routeEntranceInput:getValue(),
            type = self.typeInput:getSelectedText(),
            costItem = self.costItemInput:getText(),
            timeAtStops = tonumber(self.timeStopInput:getText()),
            timeInTransit = tonumber(self.timeTransitInput:getText()),
            weightMax = tonumber(self.weightMaxInput:getText()),
            weightFree = tonumber(self.weightFreeInput:getText()),
            weightPerUnit = tonumber(self.weightUnitInput:getText()),
            weightCostPerUnit = tonumber(self.weightCostInput:getText())
        })
    end
end

function WLMassTransitAdminPanel:delete(btn)
    if not btn then
        local modal = ISModalDialog:new(getCore():getScreenWidth()/2-100,getCore():getScreenHeight()/2-50,200,100, "Delete this Route?", true, self, self.delete)
        modal:initialise()
        modal:addToUIManager()
        return
    end
    if btn.internal == "YES" and self.route then
        WLMassTransitSystem:deleteTransitRoute(getPlayer(), self.routeId)
    end
end

local newStopI = 1
function WLMassTransitAdminPanel:addStop()
    if not self.route then return end
    local player = getPlayer()
    table.insert(self.route.order, "New Stop " .. newStopI)
    self.route.stops["New Stop " .. newStopI] = {
        isNew = true,
        name = "New Stop " .. newStopI,
        area = {
            x1 = math.floor(player:getX())-5,
            y1 = math.floor(player:getY())-5,
            z1 = math.floor(player:getZ()),
            x2 = math.floor(player:getX())+5,
            y2 = math.floor(player:getY())+5,
            z2 = math.floor(player:getZ())
        },
        exit = {
            x = math.floor(player:getX()),
            y = math.floor(player:getY()),
            z = math.floor(player:getZ())
        },
        cost = 0
    }
    self:updateUi()
    newStopI = newStopI + 1
end

function WLMassTransitAdminPanel:prevPage()
    self.page = self.page - 1
    self:updateUi()
end

function WLMassTransitAdminPanel:nextPage()
    self.page = self.page + 1
    self:updateUi()
end

function WLMassTransitAdminPanel:moveStop(_, idx, dir)
    if not self.route then return end
    local stopIdx = idx + self.page * 5
    local stopName = self.route.order[stopIdx]
    local stop = self.route.stops[stopName]
    if not stop or stop.isNew then return end
    local newIdx = stopIdx + dir
    if newIdx < 1 or newIdx > #self.route.order then return end
    WLMassTransitSystem:moveTransitStop(getPlayer(), self.routeId, stopName, newIdx)
end

function WLMassTransitAdminPanel:saveStop(_, idx)
    if not self.route then return end
    local stopIdx = idx + self.page * 5
    local stopName = self.route.order[stopIdx]
    local stop = self.route.stops[stopName]
    if not stop then return end
    if stop.isNew then
        WLMassTransitSystem:addTransitStop(getPlayer(), self.routeId, self.stopsList[idx].nameInput:getText(), {
            area = self.stopsList[idx].areaInput:getValue(),
            exit = self.stopsList[idx].exitInput:getValue(),
            cost = tonumber(self.stopsList[idx].costInput:getText())
        })
    else
        WLMassTransitSystem:updateTransitStop(getPlayer(), self.routeId, stopName, {
            name = self.stopsList[idx].nameInput:getText(),
            area = self.stopsList[idx].areaInput:getValue(),
            exit = self.stopsList[idx].exitInput:getValue(),
            cost = tonumber(self.stopsList[idx].costInput:getText())
        })
    end
end

function WLMassTransitAdminPanel:deleteStop(_, idx)
    if not self.route then return end
    local stopIdx = idx + self.page * 5
    local stopName = self.route.order[stopIdx]
    local stop = self.route.stops[stopName]
    if not stop then return end
    if stop.isNew then
        table.remove(self.route.order, stopIdx)
        self.route.stops[stopName] = nil
        self:updateUi()
    else
        WLMassTransitSystem:deleteTransitStop(getPlayer(), self.routeId, stopName)
    end
end