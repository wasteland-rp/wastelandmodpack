WLMassTransitEmbarkUI = ISPanel:derive("WLMassTransitEmbarkUI")
WLMassTransitEmbarkUI.panels = {}

function WLMassTransitEmbarkUI.closePanel(routeId, stopName)
    local panelKey = WLMassTransitSystem:getPanelKey(routeId, stopName)
    if WLMassTransitEmbarkUI.panels[panelKey] then
        WLMassTransitEmbarkUI.panels[panelKey]:close()
    end
end

--- @param routeId string
--- @param stopName string|nil if nil, will behave as if player is on the route
function WLMassTransitEmbarkUI:show(routeId, stopName)
    local route = WLMassTransitSystem:getRoute(routeId)
    if not route then
        return
    end
    local routeTitle = route.name .. " - " .. (stopName or "In Route")
    local tm = getTextManager()
    local scale = tm:MeasureStringY(UIFont.Small, "XXX") / 12
    local width = scale * 100
    local headWidth = tm:MeasureStringX(UIFont.Large, routeTitle) + 10
    if headWidth > width then
        width = headWidth
    end
    for _, s in ipairs(route.order) do
        local stopWidth = tm:MeasureStringX(UIFont.Small, s) + 2 + (30 * scale)
        if stopWidth > width then
            width = stopWidth
        end
    end
    width = width + 10
    local height = scale * (44 + 18 * #route.order) + 10
    local x = getCore():getScreenWidth() / 2 - width / 2
    local y = getCore():getScreenHeight() / 2 - height / 2
    local o = ISPanel:new(x, y, width, height)

    setmetatable(o, self)
    self.__index = self
    o.scale = scale
    o:initialise(routeTitle, routeId, stopName, route)
    o:addToUIManager()

    local panelKey = o:getPanelKey()
    if WLMassTransitEmbarkUI.panels[panelKey] then
        WLMassTransitEmbarkUI.panels[panelKey]:close()
    end
    ISLayoutManager.RegisterWindow("WLMassTransitEmbarkUI:" .. panelKey, WLMassTransitEmbarkUI, o)
    WLMassTransitEmbarkUI.panels[panelKey] = o;

    return o
end

function WLMassTransitEmbarkUI:getPanelKey()
    return WLMassTransitSystem:getPanelKey(self.routeId, self.stopName)
end

function WLMassTransitEmbarkUI:refresh()
    local route = WLMassTransitSystem:getRoute(self.routeId)
    if not route then
        return
    end
    local routeTitle = route.name .. " - " .. (self.stopName or "In Route")
    local tm = getTextManager()
    local scale = tm:MeasureStringY(UIFont.Small, "XXX") / 12
    local width = scale * 100
    local headWidth = tm:MeasureStringX(UIFont.Large, routeTitle) + 10
    if headWidth > width then
        width = headWidth
    end
    for _, s in ipairs(route.order) do
        local stopWidth = tm:MeasureStringX(UIFont.Small, s) + 2 + (30 * scale)
        if stopWidth > width then
            width = stopWidth
        end
    end
    width = width + 10
    local height = scale * (44 + 18 * #route.order) + 10
    self:clearChildren()
    self:setWidth(width)
    self:setHeight(height)
    self:stopSound()
    self:initialise(routeTitle, self.routeId, self.stopName, route)
end

function WLMassTransitEmbarkUI:initialise(winTitle, routeId, stopName, route)
    ISPanel.initialise(self)
    self.winTitle = winTitle
    self.routeId = routeId
    self.route = route
    self.stopName = stopName
    self.tickDelay = 0
    self.moveWithMouse = true
    self.background = false
    self.currentSound = nil
    -- self.borderColor = {r=0, g=0, b=0, a=0}
    -- self.backgroundColor = {r=0, g=0, b=0, a=0.2}

    local win = GravyUI.Node(self.width, self.height, self):pad(5)

    local header, rows, footer = win:rows({20 * self.scale, 1.0, 20 * self.scale}, 2)
    local rows = {rows:grid(#route.order, {1.0, 30 * self.scale, (not stopName) and (12 * self.scale) or nil}, 2 * self.scale, 4 * self.scale)}

    self.header = header:makeLabel(winTitle, UIFont.Large, {r=1, g=1, b=1, a=1}, "center")
    self.footer = footer:resize(footer.width + 12, footer.height):makeButton("???", self, self.onEmbark)
    self.footer.backgroundColor.a = 0.5
    self.footer.borderColor.a = 0.3
    self.alarm = nil
    self.rowsData = {}
    for i,row in ipairs(rows) do
        local nameLabel = row[1]:makeLabel("???", UIFont.Small, {r=1, g=1, b=1, a=1}, "left")
        local timeLabel = row[2]:makeLabel("???", UIFont.Small, {r=1, g=1, b=1, a=1}, "right")
        self.rowsData[i] = {
            timeLabel = timeLabel,
            nameLabel = nameLabel
        }
        if not stopName then
            local check = row[3]:pad(0, -3, 0, 0):makeTickBox(self, self.onCheckbox)
            check:addOption("")
            check.changeOptionArgs = {i}
            self.rowsData[i].check = check
        end
    end
    self:updateUi()
end

function WLMassTransitEmbarkUI:onCheckbox(_, checked, idx)
    if checked then
        self.alarm = self.rowsData[idx].nameLabel:getText()
    else
        self.alarm = nil
    end
end

function WLMassTransitEmbarkUI:updateUi()
    local currentLeg, timeInLeg = WLMassTransitSystem:getCurrentLeg(self.routeId)
    currentLeg = currentLeg and currentLeg or 1
    timeInLeg = timeInLeg and timeInLeg or 0

    local topIdx = currentLeg
    if timeInLeg > self.route.timeAtStops then
        topIdx = topIdx + 1
    end

    for i, data in ipairs(self.rowsData) do
        local idx = i + topIdx - 1
        if idx > #self.route.order then
            idx = idx - #self.route.order
        end

        local stopName = self.route.order[idx]
        data.nameLabel:setText(stopName)

        if currentLeg == idx and timeInLeg <= self.route.timeAtStops then
            data.timeLabel:setText("<<<")
            if self.alarm and stopName == self.alarm then
                getSoundManager():playUISound("BLMT_Bell")
                self.alarm = nil
            end
        else
            local time = WLMassTransitSystem:getNextArrivalTimeForStop(self.routeId, stopName)
            data.timeLabel:setText(tostring(time))
        end

        if self.stopName and stopName == self.stopName then
            data.nameLabel:setHoverText("Free", {r=0.3, g=1, b=0.3, a=1})

            data.timeLabel.color = {r=0.6, g=0.8, b=1, a=1}
            data.nameLabel.color = {r=0.6, g=0.8, b=1, a=1}
        elseif WLMassTransitSystem.currentStop and stopName == WLMassTransitSystem.currentStop then
            data.nameLabel:setHoverText("Free", {r=0.3, g=1, b=0.3, a=1})

            data.timeLabel.color = {r=0.6, g=1.0, b=0.6, a=1}
            data.nameLabel.color = {r=0.6, g=1.0, b=0.6, a=1}
        else
            local player = getPlayer()
            data.nameLabel:setHoverText(
                WLMassTransitSystem:getCostString(player, self.routeId, stopName),
                WLMassTransitSystem:canExit(player, self.routeId, stopName) and {r=0.3, g=1, b=0.3, a=1} or {r=1, g=0.3, b=0.3, a=1}
            )

            data.timeLabel.color = {r=1, g=1, b=1, a=1}
            data.nameLabel.color = {r=1, g=1, b=1, a=1}
        end

        if not self.stopName then
            if stopName and self.alarm and stopName == self.alarm then
                data.check:setSelected(1, true)
            else
                data.check:setSelected(1, false)
            end
        end
        
    end
    local currentStopName = self.route.order[currentLeg]
    if not self.stopName then
        if timeInLeg > self.route.timeAtStops then
            self.footer:setEnable(false)
            self.footer:setTitle("Disembark")
        elseif WLMassTransitSystem:canExit(getPlayer(), self.routeId, currentStopName) then
            self.footer:setEnable(true)
            self.footer:setTitle("Disembark for " .. WLMassTransitSystem:getCostString(getPlayer(), self.routeId, currentStopName) .. " (" .. self.route.timeAtStops - timeInLeg .. ")")
        else
            self.footer:setEnable(false)
            self.footer:setTitle("Missing " .. WLMassTransitSystem:getCostString(getPlayer(), self.routeId, currentStopName) .. " (" .. self.route.timeAtStops - timeInLeg .. ")")
        end
    elseif currentStopName == self.stopName and timeInLeg < self.route.timeAtStops then
        self.footer:setEnable(true)
        self.footer:setTitle("Embark" .. " (" .. self.route.timeAtStops - timeInLeg .. ")")
    else
        local timeToMyStop = WLMassTransitSystem:getNextArrivalTimeForStop(self.routeId, self.stopName)
        self.footer:setEnable(false)
        self.footer:setTitle("Arrives in " .. timeToMyStop)
    end
end

function WLMassTransitEmbarkUI:stopSound()
    if self.currentSound then
        if self.currentSound.needsStopped then
            getSoundManager():stopUISound(self.currentSound.audio)
        end
        self.currentSound = nil
    end
end

function WLMassTransitEmbarkUI:playSound(desiredSound, needsStopped)
    self.currentSound = {
        name = desiredSound,
        audio = getSoundManager():playUISound(desiredSound),
        needsStopped = needsStopped
    }
end

function WLMassTransitEmbarkUI:prerender()
    self:drawRect(0, 0, self.width, self.height - (22 * self.scale) - 5, 0.2, 0.0, 0.0, 0.0)
    ISPanel.prerender(self)

    if self.tickDelay == 0 then
        self:updateUi()
        self.tickDelay = 5
    else
        self.tickDelay = self.tickDelay - 1
    end
    local desiredSound, needsStopped = WLMassTransitSystem:getCurrentSoundName(self.routeId, self.stopName)
    if desiredSound then
        if self.currentSound and self.currentSound.name ~= desiredSound then
            self:stopSound()
        end
        if not self.currentSound then
            self:playSound(desiredSound, needsStopped)
        end
    elseif not desiredSound and self.currentSound then
        self:stopSound()
    end
end

function WLMassTransitEmbarkUI:onEmbark()
    if self.isTeleporting then
        return
    end
    self.isTeleporting = true
    if self.stopName then
        WLMassTransitSystem:enterTransitRoute(getPlayer(), self.routeId, self.stopName)
    else
        WLMassTransitSystem:exitTransitRoute(getPlayer(), self.routeId, WLMassTransitSystem:getCurrentStop(self.routeId))
    end
end

function WLMassTransitEmbarkUI:close()
    self:setVisible(false)
    self:removeFromUIManager()
    self:stopSound()
    local panelKey = self:getPanelKey()
    WLMassTransitEmbarkUI.panels[panelKey] = nil
end

function WLMassTransitEmbarkUI:onMouseUp(x, y)
    ISPanel.onMouseUp(self, x, y)
    ISLayoutManager.OnPostSave()
end

function WLMassTransitEmbarkUI:onMouseUpOutside(x, y)
    ISPanel.onMouseUpOutside(self, x, y)
    ISLayoutManager.OnPostSave()
end

function WLMassTransitEmbarkUI:RestoreLayout(name, layout)
	local x = tonumber(layout.x)
	local y = tonumber(layout.y)
    if x ~= nil and y ~= nil then
		self:setX(x)
		self:setY(y)
	end
end

function WLMassTransitEmbarkUI:SaveLayout(name, layout)
	layout.x = self:getX()
	layout.y = self:getY()
end