local function openRoute(player, routeId)
    WLMassTransitSystem:openRoute(player, routeId)
end

local function closeRoute(player, routeId, force)
    WLMassTransitSystem:closeRoute(player, routeId, force)
end

local function editRoute(player, routeId)
    WLMassTransitAdminPanel:show(routeId)
end

local function deleteRoute(player, routeId)
    WLMassTransitSystem:deleteTransitRoute(player, routeId)
end

local function addRoute(player)
    WLMassTransitAdminPanel:show()
end

local function teleportToRoute(player, routeId)
    local route = WLMassTransitSystem:getRoute(routeId)
    if not route then return end
    WL_Utils.teleportPlayerToCoords(player, route.entrance.x, route.entrance.y, route.entrance.z)
end

local function WorldMenu(playerIdx, context)
    local player = getPlayer(playerIdx)
    if not player then return end
    if isClient() and not WL_Utils.canModerate(player) then return end

    local wlAdmin = WL_ContextMenuUtils.getOrCreateSubMenu(context, "WL Admin")
    local submenu = WL_ContextMenuUtils.getOrCreateSubMenu(wlAdmin, "Mass Transit")

    local currentRoutes = WLMassTransitSystem:getRoutes()
    local currentMenu = WL_ContextMenuUtils.getOrCreateSubMenu(submenu, "Current Routes")
    for routeId, route in pairs(currentRoutes) do
        local routeMenu = WL_ContextMenuUtils.getOrCreateSubMenu(currentMenu, route.name .. " (" .. route.status .. ")")
        routeMenu:addOption("TP", player, teleportToRoute, routeId)
        if route.status == "closed" then
            routeMenu:addOption("Open", player, openRoute, routeId)
        elseif route.status == "open" then
            routeMenu:addOption("Close", player, closeRoute, routeId)
        end
        if route.status == "closing" then
            routeMenu:addOption("Force Close", player, closeRoute, routeId, true)
        end
        if route.status == "closed" then
            routeMenu:addOption("Edit", player, editRoute, routeId)

            local deleteSubMenu = WL_ContextMenuUtils.getOrCreateSubMenu(routeMenu, "Delete")
            deleteSubMenu:addOption("Confirm", player, deleteRoute, routeId)
        end
    end

    submenu:addOption("Add Route", player, addRoute)
end

Events.OnFillWorldObjectContextMenu.Add(WorldMenu)