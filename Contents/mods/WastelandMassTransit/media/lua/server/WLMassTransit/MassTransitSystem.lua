local function indexOf(array, value)
    for i, v in ipairs(array) do
        if v == value then
            return i
        end
    end
    return nil
end

--- @class WLMassTransitSystem_PublicData
--- @field routes table<string, WLMassTransitSystem_Route>

--- @class WLMassTransitSystem_PrivateData
--- @field routes table<string, any>

--- @class WLMassTransitSystem_Passenger
--- @field username string
--- @field routeId string
--- @field enterStopName string

--- @class WLMassTransitSystem_Route
--- @field id string
--- @field status "open"|"closing"|"closed"
--- @field name string
--- @field area {x1: number, y1: number, z1: number, x2: number, y2: number, z2: number}
--- @field entrance {x: number, y: number, z: number}
--- @field costItem string
--- @field type "Bus"|"Ferry"|"Train"|"Plane"
--- @field timeAtStops number
--- @field timeInTransit number
--- @field weightMax number
--- @field weightFree number
--- @field weightPerUnit number
--- @field weightCostPerUnit number
--- @field stops table<string, WLMassTransitSystem_Stops>
--- @field order string[]

--- @class WLMassTransitSystem_Stops
--- @field name string
--- @field area {x1: number, y1: number, z1: number, x2: number, y2: number, z2: number}
--- @field exit {x: number, y: number, z: number}
--- @field cost number

--- @class WLMassTransitSystem : WL_ClientServerBase
--- @field publicData WLMassTransitSystem_PublicData
--- @field privateData WLMassTransitSystem_PrivateData
WLMassTransitSystem = WLMassTransitSystem or WL_ClientServerBase:new("WLMassTransitSystem")
WLMassTransitSystem.needsPrivateData = true
WLMassTransitSystem.needsPublicData = true

--- @return table<string, WLMassTransitSystem_Route>
function WLMassTransitSystem:getRoutes()
    return self.publicData.routes or {}
end

--- @param routeId string
--- @return WLMassTransitSystem_Route
function WLMassTransitSystem:getRoute(routeId)
    return self.publicData.routes[routeId]
end

function WLMassTransitSystem:onModDataInit()
    if not self.publicData.routes then self.publicData.routes = {} end
    if not self.privateData.routes then self.privateData.routes = {} end
end

function WLMassTransitSystem:addTransitRoute(player, id, data)
    if isClient() then
        self:sendToServer(player, "addTransitRoute", id, data)
        return
    end
    local name = data.name or ("Route " .. id)
    self.publicData.routes[id] = {
        id = id,
        status = "closed", -- open, closing, closed
        name = name,
        area = data.area,
        entrance = data.entrance,
        costItem = data.costItem,
        type = data.type,
        timeAtStops = data.timeAtStops,
        timeInTransit = data.timeInTransit, -- 1 minute default
        weightMax = data.weightMax,
        weightFree = data.weightFree,
        weightPerUnit = data.weightPerUnit,
        weightCostPerUnit = data.weightCostPerUnit,
        stops = {},
        order = {},
    }
    self.privateData.routes[id] = {
        passengers = {},
    }
    self:savePublicData(true)
    self:savePrivateData()
    self:logInfo(player:getUsername() .. " added route: " .. name .. " (" .. id .. ")")
end

function WLMassTransitSystem:updateTransitRoute(player, id, data)
    if isClient() then
        self:sendToServer(player, "updateTransitRoute", id, data)
        return
    end
    if not self.publicData.routes[id] then
        self:logError(player:getUsername() .. " tried to update non-existent route: " .. id)
        self:showPlayerError("There was an error: Route not found.")
        return
    end
    if self.publicData.routes[id].status ~= "closed" then
        self:logError(player:getUsername() .. " tried to update open route: " .. id)
        self:showPlayerError("There was an error: Route is not closed.")
        return
    end
    local route = self.publicData.routes[id]
    route.name = data.name
    route.area = data.area
    route.entrance = data.entrance
    route.costItem = data.costItem
    route.type = data.type
    route.timeAtStops = data.timeAtStops
    route.timeInTransit = data.timeInTransit
    route.weightMax = data.weightMax
    route.weightFree = data.weightFree
    route.weightPerUnit = data.weightPerUnit
    route.weightCostPerUnit = data.weightCostPerUnit
    self:savePublicData(true)
    self:logInfo(player:getUsername() .. " updated route: " .. route.name .. " (" .. id .. ")")
end

function WLMassTransitSystem:deleteTransitRoute(player, id)
    if isClient() then
        self:sendToServer(player, "deleteTransitRoute", id)
        return
    end
    self.publicData.routes[id] = nil
    self.privateData.routes[id] = nil
    self:savePublicData(true)
    self:savePrivateData()
    self:logInfo(player:getUsername() .. " deleted route: " .. id)
end

function WLMassTransitSystem:addTransitStop(player, routeId, stopName, data)
    if isClient() then
        self:sendToServer(player, "addTransitStop", routeId, stopName, data)
        return
    end
    if not self.publicData.routes[routeId] then
        self:logError(player:getUsername() .. " tried to add stop to non-existent route: " .. routeId)
        self:showPlayerError("There was an error: Route not found.")
        return
    end
    self.publicData.routes[routeId].stops[stopName] = {
        name = stopName,
        area = data.area,
        exit = data.exit,
        cost = data.cost
    }
    table.insert(self.publicData.routes[routeId].order, stopName)
    self:savePublicData(true)
    self:logInfo(player:getUsername() .. " added stop: " .. stopName .. " to route: " .. self.publicData.routes[routeId].name)
end

function WLMassTransitSystem:updateTransitStop(player, routeId, stopName, data)
    if isClient() then
        self:sendToServer(player, "updateTransitStop", routeId, stopName, data)
        return
    end
    if not self.publicData.routes[routeId] then
        self:logError(player:getUsername() .. " tried to update stop on non-existent route: " .. routeId)
        self:showPlayerError("There was an error: Route not found.")
        return
    end
    if not self.publicData.routes[routeId].stops[stopName] then
        self:logError(player:getUsername() .. " tried to update non-existent stop: " .. stopName)
        self:showPlayerError("There was an error: Stop not found.")
        return
    end
    local stop = self.publicData.routes[routeId].stops[stopName]
    stop.area = data.area
    stop.exit = data.exit
    stop.cost = data.cost
    if stop.name ~= data.name then
        self.publicData.routes[routeId].stops[data.name] = stop
        self.publicData.routes[routeId].stops[stop.name] = nil
        local orderIdx = indexOf(self.publicData.routes[routeId].order, stop.name)
        if orderIdx then
            self.publicData.routes[routeId].order[orderIdx] = data.name
        end
        stop.name = data.name
    end
    self:savePublicData(true)
    self:logInfo(player:getUsername() .. " updated stop: " .. stopName .. " on route: " .. self.publicData.routes[routeId].name)
end

function WLMassTransitSystem:deleteTransitStop(player, routeId, stopName)
    if isClient() then
        self:sendToServer(player, "deleteTransitStop", routeId, stopName)
        return
    end
    if not self.publicData.routes[routeId] then
        self:logError(player:getUsername() .. " tried to delete stop on non-existent route: " .. routeId)
        self:showPlayerError("There was an error: Route not found.")
        return
    end
    self.publicData.routes[routeId].stops[stopName] = nil
    local orderIdx = indexOf(self.publicData.routes[routeId].order, stopName)
    if orderIdx then
        table.remove(self.publicData.routes[routeId].order, orderIdx)
    end
    self:savePublicData(true)
    self:logInfo(player:getUsername() .. " deleted stop: " .. stopName .. " from route: " .. self.publicData.routes[routeId].name)
end

function WLMassTransitSystem:moveTransitStop(player, routeId, stopName, newIndex)
    if isClient() then
        self:sendToServer(player, "moveTransitStop", routeId, stopName, newIndex)
        return
    end
    if not self.publicData.routes[routeId] then
        self:logError(player:getUsername() .. " tried to move stop on non-existent route: " .. routeId)
        self:showPlayerError("There was an error: Route not found.")
        return
    end
    local order = self.publicData.routes[routeId].order
    local oldIndex = indexOf(order, stopName)
    if oldIndex then
        table.remove(order, oldIndex)
        table.insert(order, newIndex, stopName)
        self:savePublicData(true)
        self:logInfo(player:getUsername() .. " moved stop: " .. stopName .. " on route: " .. self.publicData.routes[routeId].name)
    else
        self:logError(player:getUsername() .. " tried to move a stop without an order: " .. stopName)
        self:showPlayerError("There was an error: Route in invalid state.")
    end
end

function WLMassTransitSystem:openRoute(player, routeId)
    if isClient() then
        self:sendToServer(player, "openRoute", routeId)
        return
    end
    if not self.publicData.routes[routeId] then
        self:logError(player:getUsername() .. " tried to open non-existent route: " .. routeId)
        self:showPlayerError("There was an error: Route not found.")
        return
    end
    if #self.publicData.routes[routeId].order < 2 then
        self:logError(player:getUsername() .. " tried to open route with less than 2 stops: " .. routeId)
        self:showPlayerError("There was an error: Route must have at least 2 stops.")
        return
    end

    self.publicData.routes[routeId].status = "open"
    self:savePublicData(true)
    self:logInfo(player:getUsername() .. " opened route: " .. self.publicData.routes[routeId].name)
end

function WLMassTransitSystem:hasPassengers(routeId)
    for _, _ in pairs(self.privateData.routes[routeId].passengers) do
        return true
    end
    return false
end

function WLMassTransitSystem:closeRoute(player, routeId, force)
    if isClient() then
        self:sendToServer(player, "closeRoute", routeId, force)
        return
    end
    if not self.publicData.routes[routeId] then
        self:logError(player:getUsername() .. " tried to close non-existent route: " .. routeId)
        self:showPlayerError("There was an error: Route not found.")
        return
    end
    if force then
        self.privateData.routes[routeId].passengers = {}
        self:savePrivateData()
    end
    if not self:hasPassengers(routeId) then
        self.publicData.routes[routeId].status = "closed"
        self:savePublicData(true)
        self:logInfo(player:getUsername() .. " closed route: " .. self.publicData.routes[routeId].name)
        return
    end
    self.publicData.routes[routeId].status = "closing"
    self:savePublicData(true)
    self:logInfo(player:getUsername() .. " started closing route: " .. self.publicData.routes[routeId].name)
end

local function getRouteTotalTime(route)
    return (route.timeAtStops + route.timeInTransit) * #route.order
end

local function getCurrentRouteStartTimeTs(route, timestamp)
    local totalTime = getRouteTotalTime(route)
    return math.floor(timestamp / totalTime) * totalTime
end

--- @return number, number
function WLMassTransitSystem:getCurrentLeg(routeId)
    local route = self.publicData.routes[routeId]
    local ts = WL_Utils.getTimestamp()

    local currentRouteStart = getCurrentRouteStartTimeTs(route, ts)
    local timePerLeg = route.timeAtStops + route.timeInTransit
    local currentIndex = math.floor((ts - currentRouteStart) / timePerLeg)
    local timeInLeg = ts - (currentRouteStart + (currentIndex * timePerLeg))
    return currentIndex + 1, timeInLeg
end

--- Gets the current stop for the given route
--- @param routeId string Route ID
--- @return string|nil stopName The current stop name or nil if the route is not running or in transit
function WLMassTransitSystem:getCurrentStop(routeId)
    if not self.publicData.routes[routeId] then
        return
    end
    local route = self.publicData.routes[routeId]
    local ts = WL_Utils.getTimestamp()
    local currentRouteStart = getCurrentRouteStartTimeTs(route, ts)
    local timeInRoute = ts - currentRouteStart
    local timePerStop = route.timeAtStops + route.timeInTransit
    local stopIndex = math.floor(timeInRoute / timePerStop)
    local timeInStop = timeInRoute - (stopIndex * timePerStop)
    if timeInStop < route.timeAtStops then
        return route.order[stopIndex + 1]
    else
        return nil
    end
end

--- Calculates the number of seconds until the next arrival at the given stop
--- @param routeId string
--- @param stopName string
--- @param ts number|nil Timestamp to calculate the arrival time from. Defaults to current time.
--- @return number Number of seconds until the next arrival
function WLMassTransitSystem:getNextArrivalTimeForStop(routeId, stopName, ts)
    if not self.publicData.routes[routeId] then
        self:logError("Route not found: " .. routeId)
        return -1
    end
    if not self.publicData.routes[routeId].stops[stopName] then
        self:logError("Stop not found: " .. stopName)
        return -1
    end
    local route = self.publicData.routes[routeId]
    ts = ts or WL_Utils.getTimestamp()
    local currentRouteStart = getCurrentRouteStartTimeTs(route, ts)
    local timePerStop = route.timeAtStops + route.timeInTransit
    local currentIndex = math.floor((ts - currentRouteStart) / timePerStop)
    local stopIndex = indexOf(route.order, stopName) - 1
    if currentIndex < stopIndex then
        return (currentRouteStart + (stopIndex * timePerStop)) - ts
    else
        return (currentRouteStart + (stopIndex * timePerStop) + getRouteTotalTime(route)) - ts
    end
end

function WLMassTransitSystem:getLeaveTimeForCurrentStop(routeId)
    if not self.publicData.routes[routeId] then
        self:logError("Route not found: " .. routeId)
        return -1
    end
    local route = self.publicData.routes[routeId]
    local ts = WL_Utils.getTimestamp()
    local currentRouteStart = getCurrentRouteStartTimeTs(route, ts)
    local timePerStop = route.timeAtStops + route.timeInTransit
    local currentIndex = math.floor((ts - currentRouteStart) / timePerStop)
    return currentRouteStart + (currentIndex * timePerStop) + route.timeAtStops
end

function WLMassTransitSystem:doEnter(player, routeId, stopName)
    self.currentRoute = routeId
    self.currentStop = stopName
    local route = self.publicData.routes[routeId]
    WL_Utils.teleportPlayerToCoords(player, route.entrance.x, route.entrance.y, route.entrance.z)
end

function WLMassTransitSystem:enterTransitRoute(player, routeId, stopName)
    if isClient() then
        self:sendToServer(player, "enterTransitRoute", routeId, stopName)
        return
    end

    if not self.publicData.routes[routeId] then
        self:logError(player:getUsername() .. " tried to enter non-existent route: " .. routeId)
        self:showPlayerError(player, "There was an error: Route not found.")
        return
    end
    local route = self.publicData.routes[routeId]
    if route.status ~= "open" then
        self:logError(player:getUsername() .. " tried to enter non-open route: " .. routeId)
        self:showPlayerError(player, "There was an error: Route is not open.")
        return
    end
    local passenger = {
        username = player:getUsername(),
        routeId = routeId,
        enterStopName = stopName,
    }
    self.privateData.routes[routeId].passengers[player:getUsername()] = passenger
    self:savePrivateData()
    self:sendToClient(player, "doEnter", routeId, stopName, true)
    self:logInfo(player:getUsername() .. " entered route: " .. self.publicData.routes[routeId].name .. " at stop: " .. stopName)
end

local function getPlayerWeight(player)
    local weight = 0
    for i = 0, player:getInventory():getItems():size() - 1 do
        local item = player:getInventory():getItems():get(i)
        if (not player:isEquipped(item) and not player:isEquippedClothing(item)) or player:isHandItem(item) then
            weight = weight + item:getUnequippedWeight()
        elseif instanceof(item, "InventoryContainer") then
            weight = weight + item:getContentsWeight()
        end
    end
    return weight
end

local function getExtraWeightUnits(playerWeight, weightFree, weightPerUnit, weightCostPerUnit)
    local extraUnits = playerWeight - weightFree
    if extraUnits <= 0 then
        return 0
    end
    extraUnits = math.ceil(extraUnits / weightPerUnit)
    return extraUnits * weightCostPerUnit
end

function WLMassTransitSystem:getCostString(player, routeId, stopName)
    if isServer() then
        return
    end
    local route = self.publicData.routes[routeId]
    if not route then
        return
    end
    local stop = route.stops[stopName]
    if not stop then
        return
    end
    local cost = stop.cost
    if self.currentStop and stopName == self.currentStop then
        return "Free"
    end
    if route.weightFree > 0 then
        local weight = getPlayerWeight(player)
        cost = cost + getExtraWeightUnits(weight, route.weightFree, route.weightPerUnit, route.weightCostPerUnit)
    end
    return cost > 0 and (tostring(cost) .. " " .. getItemNameFromFullType(route.costItem)) or "Free"
end

function WLMassTransitSystem:canExit(player, routeId, stopName)
    if isServer() then
        return
    end

    if player:isGodMod() then
        return true
    end

    if self.currentRoute == routeId and self.currentStop == stopName then
        return true
    end

    local route = self.publicData.routes[routeId]
    if not route then
        return false
    end

    local stop = route.stops[stopName]
    if not stop then
        return false
    end

    local cost = stop.cost

    if route.weightFree or route.weightMax then
        weight = getPlayerWeight(player)
        if route.weightMax > 0 and weight > route.weightMax then
            return false
        end
        if route.weightFree > 0 then
            cost = cost + getExtraWeightUnits(weight, route.weightFree, route.weightPerUnit, route.weightCostPerUnit)
        end
    end

    if route.costItem and cost > 0 then
        if route.costItem == "Base.GoldCurrency" then
            if WIT_Gold.amountOnPlayer(player) < cost then
                return false
            end
        elseif player:getInventory():getItemCountFromTypeRecurse(route.costItem) < cost then
            return false
        end
    end

    return true
end

function WLMassTransitSystem:takeCost(player, routeId, stopName)
    if isServer() then
        return
    end

    if player:isGodMod() then
        return
    end

    local route = self.publicData.routes[routeId]
    if not route then
        return
    end
    if not route.costItem then
        return
    end

    local stop = route.stops[stopName]
    if not stop then
        return
    end

    local cost = stop.cost
    if route.weightFree > 0 then
        local weight = getPlayerWeight(player)
        cost = cost + getExtraWeightUnits(weight, route.weightFree, route.weightPerUnit, route.weightCostPerUnit)
    end
    if cost > 0 then
        if route.costItem == "Base.GoldCurrency" then
            WIT_Gold.removeAmountFromPlayer(player, cost)
        else
            for i = 1, cost do
                local item = player:getInventory():getFirstTypeRecurse(route.costItem)
                if item then
                    item:getContainer():Remove(item)
                end
            end
        end
        self:logCost(player, routeId, stopName, cost)
    end
end

function WLMassTransitSystem:logCost(player, routeId, stopName, cost)
    if isClient() then
        self:sendToServer(player, "logCost", routeId, stopName, cost)
        return
    end
    self:logInfo(player:getUsername() .. " paid " .. cost .. " " .. getItemNameFromFullType(self.publicData.routes[routeId].costItem) .. " to exit route: " .. self.publicData.routes[routeId].name .. " at stop: " .. stopName)
end

function WLMassTransitSystem:doExit(player, routeId, stopName)
    self:takeCost(player, routeId, stopName)
    local stop = self.publicData.routes[routeId].stops[stopName]
    if not stop then
        self:logError(player:getUsername() .. " tried to exit non-existent stop: " .. stopName)
        self:showPlayerError(player, "There was an error: Stop not found.")
        return
    end
    self.currentRoute = nil
    self.currentStop = nil
    WL_Utils.teleportPlayerToCoords(player, stop.exit.x, stop.exit.y, stop.exit.z)
    WL_Utils.makePlayerSafe(30)
end

function WLMassTransitSystem:exitTransitRoute(player, routeId, stopName)
    if isClient() then
        self:sendToServer(player, "exitTransitRoute", routeId, stopName)
        return
    end

    if not self.publicData.routes[routeId] then
        self:logError(player:getUsername() .. " tried to exit non-existent route: " .. routeId)
        self:showPlayerError(player, "There was an error: Route not found.")
        return
    end

    self.privateData.routes[routeId].passengers[player:getUsername()] = nil
    self:savePrivateData()
    self:sendToClient(player, "doExit", routeId, stopName)
    self:logInfo(player:getUsername() .. " exited route: " .. self.publicData.routes[routeId].name .. " at stop: " .. stopName)

    if self.publicData.routes[routeId].status == "closing" then
        if not self:hasPassengers(routeId) then
            self.publicData.routes[routeId].status = "closed"
            self:savePublicData(true)
            self:logInfo("Route: " .. self.publicData.routes[routeId].name .. " closed completed")
        end
    end
end

WLMassTransitSystem.soundTimings = {
    ["Bus"] = {
        Arrive = 9,
        Leave = 6,
    },
    ["Boat"] = {
        Arrive = 5,
        Leave = 9,
    }
}

function WLMassTransitSystem:getCurrentSoundName(routeId, stopName)
    local route = self.publicData.routes[routeId]
    if not route or not self.soundTimings[route.type] then
        return nil
    end

    local stopIndex = stopName and indexOf(route.order, stopName)
    local legIndex, legTimeElapsed = self:getCurrentLeg(routeId)

    if legTimeElapsed <= route.timeAtStops then
        if stopName and stopIndex ~= legIndex then
            return nil
        end
        return route.type .. "Stopped", true
    end

    if legTimeElapsed <= route.timeAtStops + self.soundTimings[route.type].Leave then
        if stopName then
            if stopIndex == legIndex then
                return route.type .. "LeaveFade"
            end
            return nil
        end
        return route.type .. "LeaveFull"
    end

    if legTimeElapsed >= route.timeAtStops + route.timeInTransit - self.soundTimings[route.type].Arrive then
        if stopName then
            local nextIndex = legIndex == #route.order and 1 or legIndex + 1
            if nextIndex == stopIndex then
                return route.type .. "ArriveFade"
            end
            return nil
        end
        return route.type .. "ArriveFull"
    end

    if not stopName then
        return route.type .. "Moving", true
    end
end

function WLMassTransitSystem:getPanelKey(routeId, stopName)
    return routeId .. ":" .. (stopName or "_transporter")
end

-- Client Only Stuff
if not isServer() then
    WLMassTransitSystem.zones = WLMassTransitSystem.zones or {}

    local function onEnterTransporterZone(zone)
        local routeId = zone.massTranRouteId
        WLMassTransitEmbarkUI:show(routeId)
    end

    local function onLeaveTransporterZone(zone)
        local routeId = zone.massTranRouteId
        WLMassTransitEmbarkUI.closePanel(routeId)
    end

    local function onEnterEmbarkZone(zone)
        local routeId = zone.massTranRouteId
        local stopName = zone.massTranStopName
        WLMassTransitEmbarkUI:show(routeId, stopName)
    end

    local function onLeaveEmbarkZone(zone)
        local routeId = zone.massTranRouteId
        local stopName = zone.massTranStopName
        WLMassTransitEmbarkUI.closePanel(routeId, stopName)
    end

    function WLMassTransitSystem:onPublicDataUpdated()
        if not self.publicData.routes then self.publicData.routes = {} end

        if WLMassTransitAdminPanel.instance then
            WLMassTransitAdminPanel.instance.route = self:getRoute(WLMassTransitAdminPanel.instance.routeId)
            WLMassTransitAdminPanel.instance:updateUi()
        end

        for routeId, route in pairs(self.publicData.routes) do
            if not self.zones[routeId] then self.zones[routeId] = {} end

            -- zone for transporter
            if not self.zones[routeId]["_transporter"] then
                local zone = WL_Zone:new(route.area.x1, route.area.y1, route.area.z1, route.area.x2, route.area.y2, route.area.z2)
                zone.mapType = "Mass Transit Transporter"
                zone.mapColor = {1.0, 0.3, 1.0}
                zone.getMapName = function(s) return route.name end
                zone.massTranRouteId = routeId
                zone.onPlayerEnteredZone = onEnterTransporterZone
                zone.onPlayerExitedZone = onLeaveTransporterZone
                WL_TriggerZones.addZone(zone, true)
                self.zones[routeId]["_transporter"] = zone
            else
                local zone = self.zones[routeId]["_transporter"]
                if zone:isPlayerInZone(getPlayer()) then
                    onLeaveTransporterZone(zone)
                end
                zone:setArea(route.area.x1, route.area.y1, route.area.x2, route.area.y2, route.area.z1, route.area.z2)
                if zone:isPlayerInZone(getPlayer()) then
                    onEnterTransporterZone(zone)
                end
            end

            if route.status == "open" then
                -- zone for each stop
                for stopName, stop in pairs(route.stops) do
                    if not self.zones[routeId][stopName] then
                        local zone = WL_Zone:new(stop.area.x1, stop.area.y1, stop.area.z1, stop.area.x2, stop.area.y2, stop.area.z2)
                        zone.mapType = "Mass Transit Stop"
                        zone.mapColor = {1.0, 0.3, 1.0}
                        zone.getMapName = function(s) return s.stopName end
                        zone.massTranRouteId = routeId
                        zone.massTranStopName = stopName
                        zone.onPlayerEnteredZone = onEnterEmbarkZone
                        zone.onPlayerExitedZone = onLeaveEmbarkZone
                        local cp = zone:getCenterPoint()
                        zone.arrow = WL_HomingArrows.addArrow(cp.x, cp.y, cp.z)
                        WL_TriggerZones.addZone(zone, true)
                        self.zones[routeId][stopName] = zone
                    else
                        local zone = self.zones[routeId][stopName]
                        if zone:isPlayerInZone(getPlayer()) then
                            onLeaveEmbarkZone(zone)
                        end
                        zone:setArea(stop.area.x1, stop.area.y1, stop.area.x2, stop.area.y2, stop.area.z1, stop.area.z2)
                        local cp = zone:getCenterPoint()
                        WL_HomingArrows.removeArrow(zone.arrow)
                        zone.arrow = WL_HomingArrows.addArrow(cp.x, cp.y, cp.z)
                        if zone:isPlayerInZone(getPlayer()) then
                            onEnterEmbarkZone(zone)
                        end
                    end
                end
            else
                for zoneName, zone in pairs(self.zones[routeId]) do
                    if zoneName ~= "_transporter" then
                        if zone:isPlayerInZone(getPlayer()) then
                            onLeaveEmbarkZone(zone)
                        end
                        WL_HomingArrows.removeArrow(zone.arrow)
                        WL_TriggerZones.removeZone(zone)
                        zone:delete()
                        self.zones[routeId][zoneName] = nil
                    end
                end
            end
        end
    end
end