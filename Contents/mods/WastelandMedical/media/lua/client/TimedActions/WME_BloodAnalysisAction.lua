---
--- WME_BloodAnalysisAction.lua
--- 02/01/2025
---
require "TimedActions/ISBaseTimedAction"

---@class WME_BloodAnalysisAction : ISBaseTimedAction
WME_BloodAnalysisAction = ISBaseTimedAction:derive("WME_BloodAnalysisAction")

function WME_BloodAnalysisAction:isValid()
	return true
end

function WME_BloodAnalysisAction:update()
	self.character:PlayAnim("Idle")
	self.character:faceThisObject(self.isoSpectrometerObject)
end

function WME_BloodAnalysisAction:start()
	self.sound = self.character:playSound("MassSpectrometer");
end

function WME_BloodAnalysisAction:stop()
	if self.sound and self.sound ~= 0 then
		self.character:getEmitter():stopOrTriggerSound(self.sound)
	end
	ISBaseTimedAction.stop(self)
end

function WME_BloodAnalysisAction:perform()
	if self.sound and self.sound ~= 0 then
		self.character:getEmitter():stopOrTriggerSound(self.sound)
	end

	local sampleName = self.bloodSampleItem:getName()
	local name = sampleName:match("Blood Sample:%s*(.-)%s*%((.-)%)")
	if name then sampleName = name end

	local bloodTestReportText = WME_BloodAnalysis.getFullBloodTestReportString(self.bloodSampleItem)
	local report = InventoryItemFactory.CreateItem("Base.SheetPaper2")
	report:setName("Blood Analysis: " .. sampleName)
	report:setCustomName(true)
	report:setLockedBy("____")
	report:setWeight(0)
	report:setCustomWeight(true)
	report:addPage(1, bloodTestReportText)
	self.character:getInventory():AddItem(report)
	self.character:setHaloNote("Blood Analysis Complete", 50, 255, 50, 250.0)
	self.character:getInventory():Remove(self.bloodSampleItem)
	ISBaseTimedAction.perform(self)
end

function WME_BloodAnalysisAction:new (character, bloodSampleItem, isoSpectrometerObject)
	local o = {}
	setmetatable(o, self)
	self.__index = self
	o.character = character
	o.bloodSampleItem = bloodSampleItem
	o.isoSpectrometerObject = isoSpectrometerObject
	o.maxTime = 760
	o.caloriesModifier = 1;
	return o
end
