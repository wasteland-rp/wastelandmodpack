---
--- WME_DirtyWaterDrinking.lua
--- 01/11/2024
---

require "TimedActions/ISTakeWaterAction"
require "TimedActions/ISDrinkFromBottle"
require "WME_ConditionHandler"

local function rollForDysentery()
	getPlayer():setHaloNote("The water tastes funny", 255, 0, 0, 250.0)

	local chance = 6
	if getPlayer():HasTrait("IronGut") then
		chance = 1
	elseif getPlayer():HasTrait("WeakStomach") then
		chance = 12
	end

	if(ZombRand(100) < chance) then
		WME_ConditionHandler.increaseCondition(WME_Condition.DYSENTERY, 1)
	end
end


local originalISTakeWaterAction_perform = ISTakeWaterAction.perform
function ISTakeWaterAction:perform()
	-- If not filling up a container and the water source is tainted then roll for dysentery as player is drinking it
	if self.item == nil and self.waterObject and self.waterObject:isTaintedWater() then
		rollForDysentery()
	end
	originalISTakeWaterAction_perform(self)
end

local originalISDrinkFromBottle_perform = ISDrinkFromBottle.perform

function ISDrinkFromBottle:perform()
	if self.item and self.item:isTaintedWater() then
		rollForDysentery()
	end
	originalISDrinkFromBottle_perform(self)
end