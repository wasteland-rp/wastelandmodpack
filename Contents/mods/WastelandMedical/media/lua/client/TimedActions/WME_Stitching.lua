---
--- WME_Stitching.lua
--- 04/11/2024
---
require "WME_ConditionHandler"
require "TimedActions/ISStitch"

local originalStitching_perform = ISStitch.perform
function ISStitch:perform()
	local isPerformedOnSelf = self.doctor:getUsername() == self.otherPlayer:getUsername()

	if self.doIt then
		local successChance = 50 + (self.doctorLevel * 7)
		if not isPerformedOnSelf then
			successChance = successChance + 22
		end

		if successChance < 100 and ZombRand(100) > successChance then
			self.character:setHaloNote("Stitching Fumbled. Wound has worsened!", 255, 0, 0, 250.0)
			local deepWoundTime = self.bodyPart:getDeepWoundTime()
			if(deepWoundTime > 0) then -- Presumably always true but check anyway
				deepWoundTime = math.min(100, deepWoundTime + 10)
				self.bodyPart:setDeepWoundTime(deepWoundTime)
			end

			-- Don't to the real perform
			ISBaseTimedAction.perform(self);
			if self.item then
				self.item:setJobDelta(0.0);
			end
			if self.item then
				self.item:Use();
			end
			ISHealthPanel.setBodyPartActionForPlayer(self.otherPlayer, self.bodyPart, nil, nil, nil)
			return
		end
	end

	originalStitching_perform(self)

	if self.doIt and isPerformedOnSelf then -- Player stitching themselves
		if self.doctorLevel < 8 and self.bodyPart:isInfectedWound() then
			if ZombRand(self.doctorLevel + 1) == 0 then
				if ZombRand(10) == 0 then
					WME_ConditionHandler.increaseCondition(WME_Condition.FUNGAL_INFECTION, 1)
				else
					WME_ConditionHandler.increaseCondition(WME_Condition.BACTERIAL_INFECTION, 1)
				end
			end
		end
	end
end