---
--- WME_TakeDrugsAction.lua
--- 29/10/2024
---

require "WME_ConditionHandler"

local ISTakePillActionPerform = ISTakePillAction.perform
function ISTakePillAction:perform()
	if self.item:getType() == "PillsVitamins" then
		WME_ConditionHandler.increaseCondition(WME_Condition.VITAMINS, 24)
	end

	if self.item:getType() == "PillsChloroquine" then
		WME_ConditionHandler.increaseCondition(WME_Condition.MALARIA_MEDICINE, 100)
		self.item:Use()
	end

	if self.item:getType() == "PillsOxycodone" then
		WME_ConditionHandler.increaseCondition(WME_Condition.OPIOIDS, 40)
		self.item:Use()
	end

	if self.item:getType() == "PillsXanax" then
		WME_ConditionHandler.increaseCondition(WME_Condition.BENZODIAZEPINE, 35)
		self.item:Use()
	end

	if self.item:getType() == "PillsFluCure" then
		WME_ConditionHandler.increaseCondition(WME_Condition.INFLUENZA_CURE, 100)
		self.item:Use()
	end

	if self.item:getType() == "PillsIvermectin" then
		WME_ConditionHandler.increaseCondition(WME_Condition.IVERMECTIN, 50)
		self.item:Use()
	end

	ISTakePillActionPerform(self)

end

local ISEatFoodActionPerform = ISEatFoodAction.perform
function ISEatFoodAction:perform()
	ISEatFoodActionPerform(self)

	--print("food Item type: " .. self.item:getType())

	if self.item:getType() == "Antibiotics" then
		WME_ConditionHandler.increaseCondition(WME_Condition.ANTIBIOTICS, 100)
		WME_ConditionHandler.increaseCondition(WME_Condition.MALNUTRITION, 10) -- Killing gut bacteria
	end

	if self.item:getType() == "MentholCream" then
		WME_ConditionHandler.increaseCondition(WME_Condition.MUSCLE_HEALING, 80)
	end

	if self.item:getType() == "BurnCream" then
		WME_ConditionHandler.increaseCondition(WME_Condition.BURN_HEALING, 100)
	end

	if self.item:getType() == "PotassiumIodide" then
		WME_ConditionHandler.increaseCondition(WME_Condition.POTASSIUM_IODIDE, 70)
	end
end

local ISEatFoodActionStart = ISEatFoodAction.start
function ISEatFoodAction:start()
	ISEatFoodActionStart(self)

	if (self.item:getType() == "MentholCream") or (self.item:getType() == "BurnCream") then
		self:setActionAnim("MedicalCheck")
	end
end

local ISEatFoodActionAdjustMaxTime = ISEatFoodAction.adjustMaxTime
function ISEatFoodAction:adjustMaxTime(maxtime)
	if self.item:getEatType() and self.item:getEatType() == "WME_Inject" then
		return 100
	else
		return ISEatFoodActionAdjustMaxTime(self, maxtime)
	end
end

function WME_DoCocaine(food, character, percent)
	WME_ConditionHandler.increaseCondition(WME_Condition.COCAINE, 40)
	WME_ConditionHandler.updateSymptoms() -- Immediate effect
end

function WME_DoCrack(food, character, percent)
	WME_ConditionHandler.increaseCondition(WME_Condition.COCAINE, 50)
	WME_ConditionHandler.updateSymptoms() -- Immediate effect
end

function WME_MedicalMorphine(food, character, percent)
	WME_ConditionHandler.increaseCondition(WME_Condition.OPIOIDS, 30)
	WME_ConditionHandler.updateSymptoms() -- Immediate effect
end

function WME_EstimatedMorphine(food, character, percent)
	local severity = ZombRand(26, 37)
	WME_ConditionHandler.increaseCondition(WME_Condition.OPIOIDS, severity)
	WME_ConditionHandler.updateSymptoms() -- Immediate effect
end

function WME_TakeHeroin(food, character, percent)
	local severity = ZombRand(36, 55)
	WME_ConditionHandler.increaseCondition(WME_Condition.OPIOIDS, severity)
	WME_ConditionHandler.updateSymptoms() -- Immediate effect
end

function WME_DoSpeed(food, character, percent)
	local severity = ZombRand(41, 47)
	WME_ConditionHandler.increaseCondition(WME_Condition.AMPHETAMINE, severity)
	WME_ConditionHandler.updateSymptoms() -- Immediate effect
end

function WME_DoMeth(food, character, percent)
	local severity = ZombRand(61, 67)
	WME_ConditionHandler.increaseCondition(WME_Condition.AMPHETAMINE, severity)
	WME_ConditionHandler.updateSymptoms() -- Immediate effect
end

function WME_UseSedative(food, character, percent)
	WME_ConditionHandler.increaseCondition(WME_Condition.SEDATIVE, 40)
	WME_ConditionHandler.updateSymptoms() -- Immediate effect
end

function WME_EstimatedSedative(food, character, percent)
	local severity = ZombRand(30,95)
	WME_ConditionHandler.increaseCondition(WME_Condition.SEDATIVE, severity)
	WME_ConditionHandler.updateSymptoms() -- Immediate effect
end

function WME_UseAdrenaline(food, character, percent)
	WME_ConditionHandler.increaseCondition(WME_Condition.ADRENALINE, 60)
	WME_ConditionHandler.updateSymptoms() -- Immediate effect
end

function WME_EstimatedAdrenaline(food, character, percent)
	local severity = ZombRand(60,100)
	WME_ConditionHandler.increaseCondition(WME_Condition.ADRENALINE, severity)
	WME_ConditionHandler.updateSymptoms() -- Immediate effect
end

function WME_UseOpioidsCure(food, character, percent)
	WME_ConditionHandler.increaseCondition(WME_Condition.OPIOIDS_CURE, 25)
end

function WME_UseEstimatedOpioidsCure(food, character, percent)
	local severity = ZombRand(25,100)
	WME_ConditionHandler.increaseCondition(WME_Condition.OPIOIDS_CURE, severity)
end