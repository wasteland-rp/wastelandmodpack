---
--- WME_ConditionsWindow.lua
--- 20/10/2024
---
---
require "GravyUI_WL"
require "WME_ConditionHandler"

WME_ConditionsWindow = ISPanel:derive("WME_ConditionsWindow")

local FONT_HGT_SMALL = getTextManager():getFontHeight(UIFont.Small)
local FONT_HGT_MEDIUM = getTextManager():getFontHeight(UIFont.Medium)
local FONT_HGT_LARGE = getTextManager():getFontHeight(UIFont.Large)
local FONT_HGT_MASSIVE = getTextManager():getFontHeight(UIFont.Massive)

---@param playerMedicalState table of WME_PlayerMedicalState
function WME_ConditionsWindow.show(playerMedicalState, isLocalPlayer)
	if WME_ConditionsWindow.instance then
		WME_ConditionsWindow.instance:onClose()
	end
	WME_ConditionsWindow.instance = WME_ConditionsWindow:new(playerMedicalState, isLocalPlayer)
	WME_ConditionsWindow.instance:addToUIManager()
end

function WME_ConditionsWindow:new(playerMedicalState, isLocalPlayer)
	local scale = FONT_HGT_SMALL / 12
	local w = 600 * scale
	local h = 400 * scale
	local o = ISPanel:new(getCore():getScreenWidth()/2-w/2,getCore():getScreenHeight()/2-h/2, w, h)
	setmetatable(o, self)
	self.__index = self
	o.playerMedicalState = playerMedicalState
	o.isLocalPlayer = isLocalPlayer
	o:initialise()
	return o
end

function WME_ConditionsWindow:initialise()
	ISPanel.initialise(self)
	self.moveWithMouse = true
	local win =  GravyUI.Node(self.width, self.height, self)
	local closeButtonNode = win:corner("topRight", FONT_HGT_SMALL + 3, FONT_HGT_SMALL + 3)
	win = win:pad(10, 10, 10, 10)
	local tablesArea, controlsArea = win:rows( { 0.85, 0.25}, 10)
	local addictionsArea, conditionsArea, symptomsArea = tablesArea:cols({ 1/3, 1/3, 1/3 }, 20)

	local addictionsTitle, addictionsListArea = addictionsArea:rows(
			{ FONT_HGT_LARGE, addictionsArea.height - FONT_HGT_LARGE - 10 }, 10)
	addictionsTitle:makeLabel("Addictions", UIFont.Large, COLOR_WHITE, "center")
	local addictListArea, addictionsListControls = addictionsListArea:rows({ addictionsListArea.height - FONT_HGT_LARGE - 10, FONT_HGT_LARGE }, 10)
	self.addictionsList = addictListArea:makeScrollingListBox()

	local addAddiction, changeAddiction, removeAddiction = addictionsListControls:cols( { 0.25, 0.4, 0.35 }, 20)
	self.addAddictionButton = addAddiction:makeButton("Add", self, self.addAddiction)
	self.changeAddictionButton = changeAddiction:makeButton("Swap State", self, self.changeAddiction)
	self.removeAddictionButton = removeAddiction:makeButton("Remove", self, self.removeAddiction)

	local conditionsTitle, conditionListArea = conditionsArea:rows(
			{ FONT_HGT_LARGE, conditionsArea.height - FONT_HGT_LARGE - 10 }, 10)
	conditionsTitle:makeLabel("Conditions", UIFont.Large, COLOR_WHITE, "center")
	local conditionsListArea, conditionsListControls = conditionListArea:rows({ conditionListArea.height - FONT_HGT_LARGE - 10, FONT_HGT_LARGE }, 10)
	self.conditionsList = conditionsListArea:makeScrollingListBox()

	local addCondition, changeSeverity, removeCondition = conditionsListControls:cols( { 0.25, 0.4, 0.35 }, 20)
	self.addConditionButton = addCondition:makeButton("Add", self, self.addCondition)
	self.changeSeverityButton = changeSeverity:makeButton("Set Severity", self, self.setSeverity)
	self.removeConditionButton = removeCondition:makeButton("Remove", self, self.removeCondition)

	local symptomsTitle, sympArea = symptomsArea:rows(
			{ FONT_HGT_LARGE, symptomsArea.height - FONT_HGT_LARGE - 10 }, 10)
	symptomsTitle:makeLabel("Symptoms", UIFont.Large, COLOR_WHITE, "center")

	local symptomsListArea, symptomsNoteArea = sympArea:rows(
			{ sympArea.height - FONT_HGT_LARGE - 10, FONT_HGT_LARGE }, 10)
	self.symptomsList = symptomsListArea:makeScrollingListBox()
	self.symptomsNoteLabel = symptomsNoteArea:makeLabel("", UIFont.Small, COLOR_WHITE, "left")
	if not self.isLocalPlayer then
		self.symptomsNoteLabel:setText("Symptoms will not be shown")
	end

	local _, buttonRow = controlsArea:rows( { FONT_HGT_LARGE, FONT_HGT_LARGE }, 10)
	self.closeButton = closeButtonNode:makeButton("X", self, self.onClose)

	local progressConditions, refresh = buttonRow:cols({ 0.25, 0.15, 0.75 }, 20)
	self.progressConditionsButton = progressConditions:makeButton("Progress Conditions (1 Hour)", self, self.progressConditions)
	self.refreshButton = refresh:makeButton("Refresh", self, self.refreshWindow)

	if not self.isLocalPlayer then
		self:setButtonsEnabled(false) -- Turned off until we get the data from the server
		self.progressConditionsButton.enable = false -- Permanently turned off, only works for the local player
	end

	self:refreshWindow()
end

function WME_ConditionsWindow:setButtonsEnabled(enabled)
	self.addConditionButton.enable = enabled
	self.changeSeverityButton.enable = enabled
	self.removeConditionButton.enable = enabled
	self.addAddictionButton.enable = enabled
	self.changeAddictionButton.enable = enabled
	self.removeAddictionButton.enable = enabled
	self.refreshButton.enable = enabled
end

function WME_ConditionsWindow:refreshWindow()
	if self.isLocalPlayer then
		self:updateLists()
	else
		WME_Client.fetchMedicalData(self.playerMedicalState.username)
	end
end

function WME_ConditionsWindow:receiveMedicalDataFromServer(medicalData)
	self.playerMedicalState:loadSerialisedData(medicalData)
	self:setButtonsEnabled(true)
	self:updateLists()
end

function WME_ConditionsWindow:updateLists()
	self.conditionsList:clear()
	self.symptomsList:clear()
	self.addictionsList:clear()

	if self.playerMedicalState.conditions then
		for condition, severity in pairs(self.playerMedicalState.conditions) do
			local text = condition.key .. " (" .. tostring(severity) .. ")"
			local item = self.conditionsList:addItem(text, condition)

			local symptomNames = {}
			if condition.symptoms then
				for _, symptomEntry in ipairs(condition.symptoms) do
					table.insert(symptomNames, symptomEntry.symptom.name)
				end

			end

			if condition.sideEffects then
				for _, sideEffectEntry in ipairs(condition.sideEffects) do
					table.insert(symptomNames, sideEffectEntry.symptom.name)
				end
			end

			item.tooltip = "Symptoms/Side Effects: " .. table.concat(symptomNames, ", ")
		end
	end

	if self.isLocalPlayer then
		for symptom, severity in pairs(WME_SymptomHandler.currentSymptoms) do
			local text = symptom.name .. " (" .. tostring(severity) .. ")"
			self.symptomsList:addItem(text, symptom)
		end
	end

	if self.playerMedicalState.addictions then
		for addiction, worsening in pairs(self.playerMedicalState.addictions) do
			local status = worsening and " (Reliant)" or " (Recovering)"
			local text = addiction .. status
			self.addictionsList:addItem(text, addiction)
		end
	end
end


function WME_ConditionsWindow:onClose()
	self:removeFromUIManager()
end

function WME_ConditionsWindow:addCondition()
	local comboPanel = WL_ComboEntryPanel:new("Select new Condition to add")
	for _, condition in pairs(WME_Condition) do
		comboPanel:addOption(condition.key, condition)
	end
	comboPanel:setInitialSelection(WME_Condition.NO2_POISONING)
	comboPanel:getUserSelection(nil, function(_, condition)
		self.playerMedicalState:increaseCondition(condition, 50)
		self.playerMedicalState:sendToServer()
		if self.isLocalPlayer then
			WME_ConditionHandler.updateSymptoms()
		end
		self:updateLists()
	end)
end

function WME_ConditionsWindow:setSeverity()
	local selectedCondition = self.conditionsList.items[self.conditionsList.selected]
	if not selectedCondition then return end

	WL_TextEntryPanel:show("Enter the new severity", nil,
			function(_, amountString)
				local severity = tonumber(amountString)
				if not severity or severity < 1 then return end
				self.playerMedicalState:setSeverity(selectedCondition.item, severity)
				self.playerMedicalState:sendToServer()
				if self.isLocalPlayer then
					WME_ConditionHandler.updateSymptoms()
				end
				self:updateLists()
			end, nil, true, true)
end

function WME_ConditionsWindow:removeCondition()
	local selectedCondition = self.conditionsList.items[self.conditionsList.selected]
	if not selectedCondition then return end
	self.playerMedicalState:removeCondition(selectedCondition.item)
	self.playerMedicalState:sendToServer()
	if self.isLocalPlayer then
		WME_ConditionHandler.updateSymptoms()
	end
	self:updateLists()
end

function WME_ConditionsWindow:progressConditions()
	WME_ConditionHandler.progressConditions()
	self:updateLists()
end

function WME_ConditionsWindow:addAddiction()
	local comboPanel = WL_ComboEntryPanel:new("Select new Addiction to add")
	for _, condition in pairs(WME_Condition) do
		if condition.addiction then
			comboPanel:addOption(condition.addiction.type, condition.addiction.type)
		end
	end
	comboPanel:getUserSelection(nil, function(_, addiction)
		self.playerMedicalState.addictions[addiction] = true
		self.playerMedicalState:sendToServer()
		self:updateLists()
	end)
end

function WME_ConditionsWindow:changeAddiction()
	local selectedAddiction = self.addictionsList.items[self.addictionsList.selected]
	if not selectedAddiction then return end
	local state = self.playerMedicalState.addictions[selectedAddiction.item]
	if state == nil then return end
	self.playerMedicalState.addictions[selectedAddiction.item] = not state
	self.playerMedicalState:sendToServer()
	self:updateLists()
end

function WME_ConditionsWindow:removeAddiction()
	local selectedAddiction = self.addictionsList.items[self.addictionsList.selected]
	if not selectedAddiction then return end
	self.playerMedicalState.addictions[selectedAddiction.item] = nil
	local condition = WME_ConditionUtils.getConditionFromKey(selectedAddiction.item)
	if condition then
		self.playerMedicalState:removeCondition(condition) -- Should remove the withdrawal condition too
	end
	self.playerMedicalState:sendToServer()
	self:updateLists()
end