---
--- WME_ContextMenu.lua
--- 20/10/2024
---


local WME_ContextMenu = {}

local function getNearPlayers(worldobjects)
	local nearPlayers = {}
	local square = worldobjects[1]:getSquare()
	local sx, sy, sz = square:getX(), square:getY(), square:getZ()
	for x=sx-1,sx+1 do
		for y=sy-1,sy+1 do
			local sq = getCell():getGridSquare(x, y, sz)
			if sq then
				for i=0,sq:getMovingObjects():size()-1 do
					local obj = sq:getMovingObjects():get(i)
					if instanceof(obj, "IsoPlayer") then
						table.insert(nearPlayers, obj)
					end
				end
			end
		end
	end
	return nearPlayers
end


WME_ContextMenu.doMenu = function(playerIdx, context, worldobjects, test)
	local localPlayer = getSpecificPlayer(playerIdx)
	if not WL_Utils.isAtLeastGM(localPlayer) then return end
	local nearPlayers = getNearPlayers(worldobjects)
	if #nearPlayers == 0 then return end

	local medicalConditionsMenu = WL_ContextMenuUtils.getOrCreateSubMenu(context, "Medical Conditions")

	for _, player in ipairs(nearPlayers) do
		local username = player:getUsername()
		if username == localPlayer:getUsername() then
			medicalConditionsMenu:addOption(username, WME_ConditionHandler.currentPlayerMedicalState, WME_ConditionsWindow.show, true)
		else
			medicalConditionsMenu:addOption(username, WME_PlayerMedicalState:new(username), WME_ConditionsWindow.show, false)
		end
	end
end

Events.OnFillWorldObjectContextMenu.Add(WME_ContextMenu.doMenu)
