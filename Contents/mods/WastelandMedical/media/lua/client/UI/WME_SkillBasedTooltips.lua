---
--- WME_SkillBasedTooltips.lua
--- 29/10/2024
---

local addTooltip = WL_SkillTooltips.addTooltip

addTooltip("PillsVitamins", "Can be useful for patients with vitamin deficiencies. Those suffering malnutrition should take one tablet a day.", Perks.Doctor, 5)
addTooltip("Antibiotics", "Useful for several conditions, including dysentery, sepsis and cellulitis.  Overuse may disrupt gut microbiome, risking Malnutrition symptoms.", Perks.Doctor, 5)
addTooltip("MentholCreamPot", "Can be rubbed onto sore muscles to soothe them, making it ideal for countering exercise fatigue.", Perks.Doctor, 5)
addTooltip("BurnCreamTube", "Can be applied to burns to improve healing speed, should be used once a day.", Perks.Doctor, 5)
addTooltip("PotassiumIodide", "Can be used to treat fungal infections such as sporotrichosis and radiation poisoning. Causes gastrointestinal problems if taken needlessly.", Perks.Doctor, 7)
addTooltip("PillsChloroquine", "Used for the treatment or prevention of malaria. Causes a variety of side effects if taken needlessly, including nausea, dizziness, headaches, stomach ache and diarrhea.", Perks.Doctor, 7)
addTooltip("CocaineBaggie", "A stimulant that temporarily increases energy and euphoria, but carries risks of addiction, cardiovascular stress, and severe health complications.", Perks.Doctor, 5)
addTooltip("CocaineBrick", "A stimulant that temporarily increases energy and euphoria, but carries risks of addiction, cardiovascular stress, and severe health complications.", Perks.Doctor, 5)
addTooltip("SpeedBaggie", "A stimulant that enhances focus, energy, and activity levels for an extended period. Prolonged use can lead to addiction and severe mental and physical health risks.", Perks.Doctor, 6)
addTooltip("SpeedBox", "A stimulant that enhances focus, energy, and activity levels for an extended period. Prolonged use can lead to addiction and severe mental and physical health risks.", Perks.Doctor, 6)
addTooltip("MorphineSyringeMedical", "A precisely measured syringe of morphine. Pain relief with no risk of addiction.", Perks.Doctor, 5)
addTooltip("MorphineSyringeEstimated", "A dubiously estimated syringe of morphine. May have little effect or cause addiction.", Perks.Doctor, 5)
addTooltip("MorphineVial", "Can provide pain relief, though causes dizziness and overdosing can kill. Inject a precise dose to avoid risk of addiction.", Perks.Doctor, 5)
addTooltip("BlackTarHeroin", "Highly addictive substance. Causes a euphoria high and blocks pain. Needs to be dissolved in water before injection.", Perks.Doctor, 5)
addTooltip("HeroinSyringe", "Contains dissolved Heroin. Highly addictive substance. Causes a euphoria high and blocks pain.", Perks.Doctor, 5)
addTooltip("PillsOxycodone", "Oral opioid used to relieve pain. Slower release than injections. Chance of addiction from use.", Perks.Doctor, 6)
addTooltip("CrackCocaine", "A highly addictive stimulant that provides a short, intense euphoria and burst of energy.", Perks.Doctor, 6);
addTooltip("Methamphetamine", "A powerful stimulant that increases energy, alertness, and euphoria. Long-term use can lead to severe addiction.", Perks.Doctor, 7);
addTooltip("AdrenalineVial", "Adrenaline. Boosts energy and speed. Use a precise dose or risk various negative effects such as dizziness and headaches.", Perks.Doctor, 5)
addTooltip("SedativeVial", "A sedative. Non-addictive. Provides pain relief and suppresses anxiety and panic. Slows reactions and reduces energy. Use two precise doses for extended duration.", Perks.Doctor, 5)
addTooltip("SedativeSyringe", "A precisely measured syringe of Lorazepam, a sedative.", Perks.Doctor, 5)
addTooltip("SedativeSyringeEstimated", "A dubiously estimated syringe of Lorazepam, a sedative. Effects may vary.", Perks.Doctor, 5)
addTooltip("AdrenalineSyringe", "A precisely measured syringe of Epinephrine (Adrenaline)", Perks.Doctor, 5)
addTooltip("AdrenalineSyringeEstimated", "A dubiously estimated syringe of Epinephrine (Adrenaline). May cause adverse effects such as dizziness and chest pain.", Perks.Doctor, 5)
addTooltip("PillsXanax", "A single pill each day can be used to relieve anxiety and panic disorders. Exceeding this dosage can cause euphoria, addiction and negative side effects.", Perks.Doctor, 6)
addTooltip("OpioidsCureVial", "Used to counteract opioid overdoses. Only a small precise dose is required. Side effects when taken needlessly include chest pains and nausea.", Perks.Doctor, 5)
addTooltip("OpioidsCureSyringe", "A precisely measured syringe of Naloxone. Reverses opioid overdoses.", Perks.Doctor, 5)
addTooltip("OpioidsCureSyringeEstimated", "A dubiously estimated syringe of Naloxone. Reverses opioid overdoses.", Perks.Doctor, 5)
addTooltip("PillsFluCure", "Helps to fight off influenza, often indicated by coughing and sneezing.", Perks.Doctor, 4)
addTooltip("PillsIvermectin", "An antiparasitic drug effective against various worm infections. Improper use can cause side effects such as dizziness and nausea", Perks.Doctor, 7)
addTooltip("WLSyringeDirty", "This syringe requires decontamination before use. This can be done with disinfectant, alcohol or boiling water.", Perks.Doctor, 3)
addTooltip("WLBloodSample", "This blood sample can be tested using liquid chromatography mass spectrometry (LC-MS) to determine any conditions affecting the owner. It can also be used for DNA matching.", Perks.Doctor, 7)
addTooltip("Mov_MassSpectrometer", "A machine used for analyzing blood samples to detect infections, toxins, or genetic markers. Essential for medical diagnostics and forensic investigations.", Perks.Doctor, 7)
addTooltip("MassAnalyzer", "A key component in the creation of a liquid chromatography mass spectrometry (LC-MS) system", Perks.Electricity, 7)
