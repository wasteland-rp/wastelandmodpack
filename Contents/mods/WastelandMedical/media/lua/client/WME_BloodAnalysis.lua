---
--- WME_BloodAnalysis.lua
--- 01/01/2025
---

WME_BloodAnalysis = WME_BloodAnalysis or {}

local function djb2_hash(str)
	local hash = 5381
	for i = 1, #str do
		local byte = str:byte(i)
		hash = (hash * 33 + byte) % 2^32  -- Simulating 32-bit overflow
	end
	return hash
end

local function to_hex(num)
	local hex = string.format("%X", num)
	-- Ensure the hex string is of fixed length, e.g., 6 characters
	return string.sub(hex, -6)
end

--- Stolen from WastelandInfection code
function WME_BloodAnalysis.getBloodFingerprint(username)
	local hash_value = djb2_hash(username)
	return to_hex(hash_value)
end

local function severityToAdjective(severity)
	if severity < 25 then return "Traces of " end
	if severity < 60 then return "Moderate levels of " end
	return "High levels of "
end

-- Blood type distribution table (cumulative percentages)
local bloodTypeDistribution = {
	{ type = "O+", percentage = 45 },
	{ type = "A+", percentage = 75 },
	{ type = "B+", percentage = 85 },
	{ type = "AB+", percentage = 90 },
	{ type = "O-", percentage = 95 },
	{ type = "A-", percentage = 98 },
	{ type = "B-", percentage = 99 },
	{ type = "AB-", percentage = 100 }
}

local function hashName(name)
	local hash = 0
	local prime = 31 -- Prime multiplier to improve distribution
	for i = 1, #name do
		hash = (hash * prime + string.byte(name, i)) % 100
	end
	return hash
end

-- Function to determine blood type based on a hashed value
function WME_BloodAnalysis.getBloodType(username)
	local hashValue = hashName(username) -- Improved hash function
	for _, bloodType in ipairs(bloodTypeDistribution) do
		if hashValue < bloodType.percentage then
			return bloodType.type
		end
	end
end

local function getBACSummary(bacValue)
	if bacValue < 0.08 then
		return "Mild alcohol levels, causing slight relaxation and no major impairment."
	elseif bacValue < 0.14 then
		return "Moderate alcohol levels, leading to minor judgment impairment and lowered inhibitions."
	elseif bacValue < 0.23 then
		return "Significant alcohol levels detected, impairing motor skills and coordination."
	else
		return "High alcohol levels detected, causing confusion, memory loss, and severe impairment."
	end
end

function WME_BloodAnalysis.deserialiseConditions(serialisedConditions)
	local conditions = {}
	for key, severity in pairs(serialisedConditions) do
		local condition = WME_ConditionUtils.getConditionFromKey(key)
		if condition then
			conditions[condition] = severity
		end
	end
	return conditions
end

function WME_BloodAnalysis.getFullBloodTestReportTable(bloodSampleItem)
	local reportTable = { }
	if not bloodSampleItem then return reportTable end
	local gameTime = getGameTime()
	local dateString = string.format("%d/%d/%d", gameTime:getYear(), (gameTime:getMonth() + 1),
			(gameTime:getDay() + 1))
	reportTable.date = dateString
	local isRotten = bloodSampleItem:isRotten();
	reportTable.isDamaged = isRotten

	local playerName = bloodSampleItem:getModData().WME_Username
	if playerName then
		reportTable.dnaFingerprint = WME_BloodAnalysis.getBloodFingerprint(playerName)
		if isRotten then
			reportTable.bloodType = "Unknown (Damaged sample)"
		else
			reportTable.bloodType = WME_BloodAnalysis.getBloodType(playerName)
		end
	end

	if not isRotten then
		local serialisedConditions = bloodSampleItem:getModData().WME_Conditions
		if serialisedConditions then
			local conditions = WME_BloodAnalysis.deserialiseConditions(serialisedConditions)
			for condition, severity in pairs(conditions) do
				local adjective = severityToAdjective(severity)
				local message = adjective .. condition.bloodTest
				if not reportTable.conditions then reportTable.conditions = { } end
				table.insert(reportTable.conditions, message)
			end
		end
	end

	if bloodSampleItem:getModData().WME_BloodAlcohol then
		local alcoholLevel =  bloodSampleItem:getModData().WME_BloodAlcohol
		if alcoholLevel > 0 then
			alcoholLevel = (alcoholLevel / 100) * 0.3 -- Convert to real world BAC
			reportTable.bloodAlcoholLevel = string.format("%.2f%%", alcoholLevel)
			reportTable.bloodAlcoholSummary = getBACSummary(alcoholLevel)
		end
	end

	return reportTable
end

function WME_BloodAnalysis.getFullBloodTestReportString(bloodSampleItem)
	if not bloodSampleItem then return "Sample corrupted" end
	local reportTable = WME_BloodAnalysis.getFullBloodTestReportTable(bloodSampleItem)
	local reportText = "Blood sample analysis - " .. reportTable.date  .. "\n"

	if reportTable.dnaFingerprint then
		reportText = reportText .. "DNA Fingerprint: " .. reportTable.dnaFingerprint .. "\n"
	end

	if reportTable.bloodType then
		reportText = reportText .. "Blood Type: " .. reportTable.bloodType .. "\n"
	end

	reportText = reportText .. "Observations:\n"
	if reportTable.isDamaged then
		reportText = reportText .. "Sample has degraded, leading to hemolysis and bacterial growth. Results inconclusive.\n"
	elseif reportTable.conditions then
		for _, conditionMessage in ipairs(reportTable.conditions) do
			reportText = reportText .. conditionMessage .. "\n"
		end
	end

	if reportTable.bloodAlcoholLevel then
		reportText = reportText .. "Blood Alcohol Level: " .. reportTable.bloodAlcoholLevel .. "\n"
		reportText = reportText .. reportTable.bloodAlcoholSummary .. "\n"
	end
	return reportText
end
