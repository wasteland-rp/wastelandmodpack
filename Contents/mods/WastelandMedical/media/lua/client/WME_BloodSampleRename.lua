---
--- WME_BloodSampleRename.lua
--- 02/01/2025
---
require "ISInventoryPaneContextMenu"

WME_BloodSampleRename = {};

WME_BloodSampleRename.createMenu = function(playerID, context, items)
	local bloodSampleItem = nil;
	for i, v in ipairs(items) do
		local item = v;

		if not instanceof(v, "InventoryItem") then
			item = v.items[1];
		end

		if item:getFullType() == "Base.WLBloodSample" then
			bloodSampleItem = item;
		end
	end

	if bloodSampleItem then
		local option = context:addOption("Label Sample", bloodSampleItem, WME_BloodSampleRename.onLabelSample, playerID);

		local player = getSpecificPlayer(playerID)
		local hasMarker = player:getInventory():containsTypeRecurse("Pencil") or
				player:getInventory():containsTypeRecurse("Pen") or
				player:getInventory():containsTypeRecurse("RedPen") or
				player:getInventory():containsTypeRecurse("BluePen");

		if hasMarker then
			if option.toolTip then
				option.toolTip:setVisible(false)
			end
		else
			option.notAvailable = true
			local tooltip = ISToolTip:new()
			tooltip:initialise()
			tooltip:setVisible(false)
			tooltip.description = "You need a pen or pencil to do this"
			option.toolTip = tooltip
		end
	end
end

WME_BloodSampleRename.onLabelSample = function(item, player)
	local modal = ISTextBox:new(0, 0, 280, 180, getText("Enter the label"), "", nil, WME_BloodSampleRename.onConfirmNewName, player, getSpecificPlayer(player), item);
	modal:initialise();
	modal:addToUIManager();
end

function WME_BloodSampleRename:onConfirmNewName(button, player, item)
	if button.internal == "OK" then
		if button.parent.entry:getText() and button.parent.entry:getText() ~= "" then
			item:setName("Blood Sample: " .. button.parent.entry:getText());
			local pdata = getPlayerData(player:getPlayerNum());
			pdata.playerInventory:refreshBackpacks();
			pdata.lootInventory:refreshBackpacks();
		end
	end
end

Events.OnPreFillInventoryObjectContextMenu.Add(WME_BloodSampleRename.createMenu);