---
--- WME_Client.lua
--- 07/01/2025
---
if not isClient() then return end

WME_Client = {}

function WME_Client.fetchMedicalData(username)
	if not username then error("username is missing") end
	sendClientCommand(getPlayer(), "WastelandMedical", "fetchPlayerMedicalData",{ username = username })
end

function WME_Client.setPlayerMedicalData(medicalData)
	if not medicalData then error("medicalData is missing") end
	if not medicalData.username then error("medicalData.username is missing") end
	sendClientCommand(getPlayer(), "WastelandMedical", "setPlayerMedicalData", medicalData)
end

function WME_Client.setPlayerMedicalDataAndUpdate(medicalData)
	if not medicalData then error("medicalData is missing") end
	if not medicalData.username then error("medicalData.username is missing") end
	sendClientCommand(getPlayer(), "WastelandMedical", "setPlayerMedicalDataAndUpdate", medicalData)
end

local serverCommands = {}

function serverCommands.receivePlayerMedicalData(medicalData)
	if medicalData.username == getPlayer():getUsername() then
		if not WME_ConditionHandler.currentPlayerMedicalState then -- Just connected
			WME_ConditionHandler.currentPlayerMedicalState = WME_PlayerMedicalState:new(medicalData.username)
		end
		WME_ConditionHandler.currentPlayerMedicalState:loadSerialisedData(medicalData)
		WME_ConditionHandler.updateSymptoms()
	end

	if WME_ConditionsWindow.instance and WME_ConditionsWindow.instance.playerMedicalState.username == medicalData.username then
		WME_ConditionsWindow.instance:receiveMedicalDataFromServer(medicalData)
	end
end

local function processServerCommand(module, command, args)
	if module ~= "WastelandMedical" then return end
	if not serverCommands[command] then return end
	serverCommands[command](args)
end

Events.OnServerCommand.Add(processServerCommand)