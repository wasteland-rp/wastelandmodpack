---
--- WME_ConditionHandler.lua
--- 20/10/2024
---

require "WME_Condition"
require "WME_SymptomHandler"
require "WME_Symptom"

WME_ConditionHandler = WME_ConditionHandler or {}

---@class WME_PlayerMedicalState
WME_ConditionHandler.currentPlayerMedicalState = nil

function WME_ConditionHandler.increaseCondition(condition, severityIncrease)
	if not WME_ConditionHandler.currentPlayerMedicalState then return end
	WME_ConditionHandler.currentPlayerMedicalState:increaseCondition(condition, severityIncrease)
	WME_ConditionHandler.currentPlayerMedicalState:sendToServer()
end

--- Finds all the conditions countered by the curingCondition that are also in the table of allConditions
---@param curingCondition table the condition that is curing the other conditions
---@param allConditions table of all the conditions that are active, conditions not in this table are not considered
---@return table { n x { condition, reduction, removedBy } }, can be empty
local function getActiveConditionsCounteredBy(curingCondition, allConditions)
	local counteredConditions = {}
	if not curingCondition.removeConditions then return counteredConditions end

	for _, condition in pairs(curingCondition.removeConditions) do
		local counteredCondition = WME_ConditionUtils.getConditionFromKey(condition.key)
		if counteredCondition and allConditions[counteredCondition] then
			table.insert(counteredConditions, {
				condition = counteredCondition,
				reduction = condition.speed,
				removedBy = curingCondition
			})
		end
	end

	return counteredConditions
end

--- Returns the symptoms of a condition, including side effects if it's a medicine ingested without the condition it cures
local function getActiveSymptoms(condition)
	local symptoms = {}

	if condition.symptoms then
		for _, symptomEntry in ipairs(condition.symptoms) do
			table.insert(symptoms, symptomEntry)
		end
	end

	if condition.sideEffects then
		local counteredConditions = getActiveConditionsCounteredBy(condition, WME_ConditionHandler.currentPlayerMedicalState.conditions)
		if #counteredConditions == 0 then
			for _, sideEffectEntry in ipairs(condition.sideEffects) do
				table.insert(symptoms, sideEffectEntry)
			end
		end
	end
	return symptoms
end

local function getSymptomSeverity(condition, conditionSeverity, symptomEntry)
	local start = symptomEntry.start or 0
	if conditionSeverity < start then return 0 end
	local maxFromCondition = condition.max or WME_ConditionUtils.DEFAULT_MAX_SEVERITY
	local finalMax = symptomEntry.max or maxFromCondition
	local multiplier = symptomEntry.multiplier or 1
	local rawSeverityPoints = math.min(conditionSeverity, finalMax) - start
	return rawSeverityPoints * multiplier
end

--- Updates the current symptoms table based on the current conditions and random symptoms.
--- This can be safely called at any time to refresh the symptoms.
function WME_ConditionHandler.updateSymptoms()
	if not WME_ConditionHandler.currentPlayerMedicalState then return end
	local player = getPlayer()
	if not player then return end
	if player:isGodMod() then
		WME_SymptomHandler.setCurrentSymptoms({})
		return
	end

	local newSymptoms = {}

	for condition, conditionSeverity in pairs(WME_ConditionHandler.currentPlayerMedicalState.conditions) do
		local symptoms = getActiveSymptoms(condition)
		for _, symptomEntry in ipairs(symptoms) do

			-- Random symptoms are covered separately from this loop because this function can be called at any time
			if not symptomEntry.chance then
				local symptom = symptomEntry.symptom
				local symptomSeverity = getSymptomSeverity(condition, conditionSeverity, symptomEntry)
				if symptomSeverity > 0 then
					if newSymptoms[symptom] then
						newSymptoms[symptom] = math.min(newSymptoms[symptom] + symptomSeverity, WME_SymptomHandler.MAX_SEVERITY)
					else
						newSymptoms[symptom] = math.min(symptomSeverity, WME_SymptomHandler.MAX_SEVERITY)
					end
				end
			end
		end
	end

	for symptom, severity in pairs(WME_ConditionHandler.currentPlayerMedicalState.randomSymptoms) do
		if newSymptoms[symptom] then
			newSymptoms[symptom] = math.min(newSymptoms[symptom] + severity, WME_SymptomHandler.MAX_SEVERITY)
		else
			newSymptoms[symptom] = math.min(severity, WME_SymptomHandler.MAX_SEVERITY)
		end
	end

	local clashingSymptoms = {
		{WME_Symptom.MOVEMENT_PENALTY, WME_Symptom.MOVEMENT_BOOST},
		{WME_Symptom.ACTION_BOOST, WME_Symptom.ACTION_PENALTY},
	}

	for _, clashingPair in ipairs(clashingSymptoms) do
		local symptom1, symptom2 = clashingPair[1], clashingPair[2]
		local severity1 = newSymptoms[symptom1] or 0
		local severity2 = newSymptoms[symptom2] or 0

		if severity1 > 0 and severity2 > 0 then
			if severity1 > severity2 then
				newSymptoms[symptom1] = severity1 - severity2
				newSymptoms[symptom2] = nil
			elseif severity2 > severity1 then
				newSymptoms[symptom2] = severity2 - severity1
				newSymptoms[symptom1] = nil
			else -- If severities are equal, remove both
				newSymptoms[symptom1] = nil
				newSymptoms[symptom2] = nil
			end
		end
	end

	WME_SymptomHandler.setCurrentSymptoms(newSymptoms)
	WME_RPHints.updateRpHints() -- We do this here so that hints synchronize with the symptoms and moodles
end

--- Determine the random symptoms that are active. This is only called hourly, do not call it more often or the random
--- symptoms will occur too frequently.
function WME_ConditionHandler.rollForRandomSymptoms()
	local newRandomSymptoms = {} -- We reset the random symptoms each hour and re-roll them

	for condition, conditionSeverity in pairs(WME_ConditionHandler.currentPlayerMedicalState.conditions) do
		local symptoms = getActiveSymptoms(condition)

		for _, symptomEntry in ipairs(symptoms) do
			local symptomActive = symptomEntry.chance and ZombRand(1,100) < symptomEntry.chance
			if symptomActive then
				local symptom = symptomEntry.symptom
				local symptomSeverity = getSymptomSeverity(condition, conditionSeverity, symptomEntry)
				if symptomSeverity > 0 then
					if newRandomSymptoms[symptom] then
						newRandomSymptoms[symptom] = newRandomSymptoms[symptom] + symptomSeverity
					else
						newRandomSymptoms[symptom] = symptomSeverity
					end
				end
			end
		end
	end
	return newRandomSymptoms
end

local function updateMalnutrition()
	local weight = getPlayer():getNutrition():getWeight()
	if weight <= 50 then
		WME_ConditionHandler.currentPlayerMedicalState:increaseCondition(WME_Condition.MALNUTRITION, 2)
	elseif weight < 56 then
		WME_ConditionHandler.currentPlayerMedicalState:increaseCondition(WME_Condition.MALNUTRITION, 1)
	else
		WME_ConditionHandler.currentPlayerMedicalState:reduceCondition(WME_Condition.MALNUTRITION, 1)
	end
end

local function hasInfectedWounds(player)
	local bodyParts = player:getBodyDamage():getBodyParts()
	for i=0, bodyParts:size()-1 do
		local bodyPart = bodyParts:get(i)
		if bodyPart:isInfectedWound() then
			return true
		end
	end
	return false
end

local function updateInfections()
	local player = getPlayer()
	if hasInfectedWounds(player) then
		WME_ConditionHandler.currentPlayerMedicalState:increaseConditionIfPresent(WME_Condition.BACTERIAL_INFECTION, 10)
		WME_ConditionHandler.currentPlayerMedicalState:increaseConditionIfPresent(WME_Condition.FUNGAL_INFECTION, 10)
	end
end

local function updateAlcoholic()
	local player = getPlayer()
	if player:HasTrait("Alcoholic") then
		if player:getStats():getDrunkenness() == 0 then
			WME_ConditionHandler.currentPlayerMedicalState:increaseCondition(WME_Condition.ALCOHOLIC_WITHDRAWAL, 1)
			WME_ConditionHandler.currentPlayerMedicalState:removeCondition(WME_Condition.DRUNK_ALCOHOLIC)
		end
	end
end

local function removeAlcoholicWithdrawal()
	if not WME_ConditionHandler.currentPlayerMedicalState then return end
	local player = getPlayer()
	if player:HasTrait("Alcoholic") then
		if player:getStats():getDrunkenness() > 9 then
			local wasInWithdrawal = WME_ConditionHandler.currentPlayerMedicalState.conditions[WME_Condition.ALCOHOLIC_WITHDRAWAL]
			WME_ConditionHandler.currentPlayerMedicalState:removeCondition(WME_Condition.ALCOHOLIC_WITHDRAWAL)
			WME_ConditionHandler.currentPlayerMedicalState:increaseCondition(WME_Condition.DRUNK_ALCOHOLIC, 20)
			if wasInWithdrawal then
				WME_ConditionHandler.updateSymptoms()
			end
		end
	end
end

Events.EveryTenMinutes.Add(removeAlcoholicWithdrawal)

local function updateAddictions()
	local satedAddictions = {}
	local currentConditions = WME_ConditionHandler.currentPlayerMedicalState.conditions
	local addictions = WME_ConditionHandler.currentPlayerMedicalState.addictions

	for condition, severity in pairs(currentConditions) do
		local addiction = condition.addiction
		if addiction then
			satedAddictions[addiction.type] = true
			if addictions[addiction.type] == nil then
				local addictionChance = math.min(severity, addiction.max) - addiction.start
				if addictionChance > 0 then
					if ZombRand(addiction.chanceIn) < addictionChance then --  Roll for a chance of addiction
						addictions[addiction.type] = true
					end
				end
			else
				if not addictions[addiction.type] then -- If recovering addict
					addictions[addiction.type] = true -- they are back to "getting worse"
				end
			end
		end
	end

	local withdrawalToReverse = {}
	local addictionsToRemove = {}
	for addictionType, worsening in pairs(addictions) do
		local withdrawalCondition = WME_ConditionUtils.getConditionFromKey(addictionType)
		if satedAddictions[addictionType] then
			WME_ConditionHandler.currentPlayerMedicalState:removeCondition(withdrawalCondition)
		else
			if worsening then
				WME_ConditionHandler.currentPlayerMedicalState:increaseCondition(withdrawalCondition, 1)
				local conditionMax = withdrawalCondition.max or WME_ConditionUtils.DEFAULT_MAX_SEVERITY
				if currentConditions[withdrawalCondition] >= conditionMax then
					table.insert(withdrawalToReverse, addictionType)
				end
			else
				WME_ConditionHandler.currentPlayerMedicalState:reduceCondition(withdrawalCondition, 1)
				if currentConditions[withdrawalCondition] == nil then
					table.insert(addictionsToRemove, addictionType)
				end
			end
		end
	end

	for _, addictionType in ipairs(withdrawalToReverse) do
		addictions[addictionType] = false -- Player begins recovering from withdrawal
	end

	for _, addictionType in ipairs(addictionsToRemove) do
		addictions[addictionType] = nil
	end
end

local function applyConditionReductions(conditionReductions)
	local currentConditions = WME_ConditionHandler.currentPlayerMedicalState.conditions

	for _, reductionData in ipairs(conditionReductions) do
		local condition = reductionData.condition
		local reduction = reductionData.reduction
		local removedBy = reductionData.removedBy

		if condition.applyHealSpeedTraits then
			if getPlayer():HasTrait("SlowHealer") then
				reduction = math.ceil(reduction * 0.7)
			elseif getPlayer():HasTrait("FastHealer") then
				reduction = math.ceil(reduction * 1.3)
			end
		end

		if currentConditions[condition] then
			currentConditions[condition] = currentConditions[condition] - reduction
			if currentConditions[condition] <= 0 then
				currentConditions[condition] = nil

				-- This stops the side effects of the cure from being applied after it has done it's job
				if removedBy.sideEffects then
					local conditionsBeingCountered = getActiveConditionsCounteredBy(removedBy, currentConditions)
					if #conditionsBeingCountered == 0 then
						currentConditions[removedBy] = nil
					end
				end
			end
		end
	end
end

function WME_ConditionHandler.progressConditions()
	if not WME_ConditionHandler.currentPlayerMedicalState then return end
	local newConditionsState = {}
	local conditionReductions = {}
	local currentConditions = WME_ConditionHandler.currentPlayerMedicalState.conditions

	for condition, severity in pairs(currentConditions) do
		local severityChange = condition.hourlyChange or 0
		local newSeverity = severity + severityChange
		if newSeverity > 0 then
			local maxValue = condition.max or WME_ConditionUtils.DEFAULT_MAX_SEVERITY
			newConditionsState[condition] = math.min(newSeverity, maxValue)
		end

		local counteredConditions = getActiveConditionsCounteredBy(condition, currentConditions)
		for _, counter in ipairs(counteredConditions) do
			table.insert(conditionReductions, counter)
		end
	end

	WME_ConditionHandler.currentPlayerMedicalState.conditions = newConditionsState
	applyConditionReductions(conditionReductions)
	updateMalnutrition()
	updateInfections()
	updateAlcoholic()
	updateAddictions()
	WME_ConditionHandler.currentPlayerMedicalState.randomSymptoms = WME_ConditionHandler.rollForRandomSymptoms()
	WME_ConditionHandler.currentPlayerMedicalState:sendToServer()
	WME_ConditionHandler.updateSymptoms()
	WME_SymptomHandler.checkHourlySymptoms() -- This ensures the function runs after the conditions have been updated
end
Events.EveryHours.Add(WME_ConditionHandler.progressConditions)

WL_PlayerReady.Add(function(pIdx, player)
	local username = player:getUsername()
	WME_Client.fetchMedicalData(username)
end)

Events.OnPlayerDeath.Add(function(player)
	if WME_ConditionHandler.currentPlayerMedicalState then
		WME_ConditionHandler.currentPlayerMedicalState:resetForPlayerDeath()
		WME_ConditionHandler.currentPlayerMedicalState:sendToServer()
	end
end)