---
--- WME_ContextMenu.lua
--- 31/12/2024
---

local function makeBloodTestReportItem(player, bloodSampleItem, spectrometerSquare, isoSpectrometerObject)
	if not AdjacentFreeTileFinder.isTileOrAdjacent(player:getCurrentSquare(), spectrometerSquare) then
		local adjacent = AdjacentFreeTileFinder.Find(spectrometerSquare, player)
		if adjacent then
			ISTimedActionQueue.add(ISWalkToTimedAction:new(player, adjacent))
			ISTimedActionQueue.add(WME_BloodAnalysisAction:new(player, bloodSampleItem, isoSpectrometerObject))
		end
	else
		ISTimedActionQueue.add(WME_BloodAnalysisAction:new(player, bloodSampleItem, isoSpectrometerObject))
	end
end

local function checkForSpectrometer(playerID, context, worldobjects, test)
	local player = getSpecificPlayer(playerID)

	local spectrometerSquare = nil
	for i,v in ipairs(worldobjects) do
		local square = v:getSquare();
		if square then
			spectrometerSquare = square
			break
		end
	end

	if spectrometerSquare then
		local objs = spectrometerSquare:getObjects()
		for i=0, objs:size()-1 do
			local isoObject = objs:get(i)
			if isoObject:getSprite() ~= nil then
				local spriteName = isoObject:getSprite():getName()
				if spriteName ~= nil then
					if string.match(spriteName, "Zoomies_Med_Device_0") then
						local playerItems = player:getInventory():getItems()
						for i=playerItems:size()-1,0,-1  do
							local item = playerItems:get(i)
							if item:getFullType() == "Base.WLBloodSample" then
								local optionName = item:getName()
								local name, status = optionName:match("Blood Sample:%s*(.-)%s*%((.-)%)")
								if name and status then
									optionName = name .. " (" .. status .. ")"
								end

								local powered = spectrometerSquare:haveElectricity()
								local canOperate = player:getPerkLevel(Perks.Doctor) >= 7
								local submenu = WL_ContextMenuUtils.getOrCreateSubMenu(context, "Insert Sample")
								local option = submenu:addOption(optionName, player, makeBloodTestReportItem, item, spectrometerSquare, isoObject)

								if powered and canOperate then
									if option.toolTip then
										option.toolTip:setVisible(false)
									end
								else
									option.notAvailable = true
									local tooltip = ISToolTip:new()
									tooltip:initialise()
									tooltip:setVisible(false)
									tooltip.description = ""

									if not powered then
										tooltip.description = "The Mass Spectrometer is not powered "
									end

									if not canOperate then
										if not powered then
											tooltip.description = tooltip.description .. "<LINE>"
										end
										tooltip.description = tooltip.description .. "You need a Medical level of 7 to run blood tests"
									end
									option.toolTip = tooltip
								end
							end
						end
						return
					end
				end
			end
		end
	end
end


Events.OnFillWorldObjectContextMenu.Add(checkForSpectrometer);