---
--- WME_Moodle.lua
--- 12/10/2024
---

require "MF_ISMoodle"
require "WME_Symptom"
require "WME_SymptomHandler"

WME_Moodle = WME_Moodle or {}
WME_Moodle.isPlayerValid = false

for _, symptom in pairs(WME_Symptom) do
	if symptom.moodle then
		MF.createMoodle(symptom.moodle)
	end
end

local function calculateMoodleValue(symptom, severity)
	local value = symptom.isGood and 0.0 or 1.0
	if severity >= WME_SymptomHandler.MIN_THRESHOLD then
		value = severity / WME_SymptomHandler.MAX_SEVERITY
		value = symptom.isGood and value or 1.0 - value
	end
	return value
end

function WME_Moodle.updateMoodleState()
	if not WME_Moodle.isPlayerValid then return end

	local updatedChecklist = {}
	for symptom, severity in pairs(WME_SymptomHandler.currentSymptoms) do
		if symptom.moodle then
			local value = calculateMoodleValue(symptom, severity)
			if MF.getMoodle(symptom.moodle):getValue() ~= value then
				MF.getMoodle(symptom.moodle):setValue(value)
			end
			updatedChecklist[symptom] = true
		end
	end

	for _, symptom in pairs(WME_Symptom) do
		if symptom.moodle then
			if not updatedChecklist[symptom] then
				MF.getMoodle(symptom.moodle):setValue(symptom.isGood and 0.0 or 1.0)
			end
		end
	end
end

local function validatePlayer(playerIndex, player)
	if player == getPlayer() then
		WME_Moodle.isPlayerValid = true
		for _, symptom in pairs(WME_Symptom) do
			if symptom.moodle then
				if symptom.isGood then
					MF.getMoodle(symptom.moodle):setThresholds(nil, nil, nil, nil, 0.1, 0.3, 0.55, 0.8)
				else
					MF.getMoodle(symptom.moodle):setThresholds(0.2, 0.45, 0.7, 0.9, nil, nil, nil, nil)
				end
			end
		end
	end
end

Events.OnCreatePlayer.Add(validatePlayer)


Events.OnGameStart.Add(function()
	Events.OnPlayerUpdate.Add(function(player)
		WME_Moodle.updateMoodleState()
	end)
end)

--[[ Vanilla Moodle thresholds for reference. Note that these are reversed from the above. i.e. the 0.1 = 0.9
Stress: 0.25
Anger: 0.1
Drunk: 0.1
Unhappy: 0.2
Sickness: 0.25
Pain: 0.15
Panic: 0.06
Boredom: 0.25
--]]