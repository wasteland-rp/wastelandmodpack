---
--- WME_OverlayManager.lua
--- 17/10/2024
---

WME_OverlayManager = WME_OverlayManager or {}

local MODE_NONE = 0
local MODE_BLURRED = 1
local MODE_DIZZY_SPELL = 2
local currentMode = MODE_NONE
local targetBlur = 0
local currentBlur = 0

function WME_OverlayManager.clearAllEffects()
	local player = getPlayer()
	if not player then return end
	local playerNumber = player:getPlayerNum()
	local sm = getSearchMode();
	sm:setEnabled(playerNumber, false);
	sm:setOverride(playerNumber, false);
	currentBlur = 0
	targetBlur = 0
	currentMode = MODE_NONE
end

function WME_OverlayManager.updateBlurLevel(severity)
	if getPlayer():isGodMod() then
		if currentMode ~= MODE_NONE then
			WME_OverlayManager.clearAllEffects()
		end
		return
	end

	if currentMode == MODE_DIZZY_SPELL then
		return  -- Don't override dizziness
	end

	if severity < 10 then -- Moodle for drunk disappears below this level
		if currentMode == MODE_BLURRED then
			WME_OverlayManager.clearAllEffects()
		end
	else
		targetBlur = (math.min(severity, 100) / WME_SymptomHandler.MAX_SEVERITY) * 0.8
		currentMode = MODE_BLURRED
	end
end

---@param blurLevel number between 0 and 1
local function setBlurLevel(blurLevel)
	local player = getPlayer()
	if not player then return end
	local playerNumber = player:getPlayerNum()
	local sm = getSearchMode();
	sm:setEnabled(playerNumber, true);
	sm:setOverride(playerNumber, true);
	local psm = sm:getSearchModeForPlayer(playerNumber);
	psm:getRadius():setExterior(0)
	psm:getRadius():setInterior(0)
	psm:getDesat():setExterior(0)
	psm:getDesat():setInterior(0)
	psm:getDarkness():setExterior(0)
	psm:getDarkness():setInterior(0)
	psm:getRadius():setExterior(0)
	psm:getRadius():setInterior(0)
	psm:getGradientWidth():setExterior(0)
	psm:getGradientWidth():setInterior(0)
	psm:getBlur():setExterior(blurLevel)
	psm:getBlur():setInterior(blurLevel)
	currentBlur = blurLevel
end

function WME_OverlayManager.restoreOverlayEffects()
	if currentMode == MODE_BLURRED then
		setBlurLevel(targetBlur)
	end
end

function WME_OverlayManager.attemptDizzySpell()
	if currentMode ~= MODE_NONE then return false end
	getSoundManager():playUISound("DizzyTones")
	setBlurLevel(0.1)
	targetBlur = 1.0
	currentMode = MODE_DIZZY_SPELL
	return true
end

local DIZZY_SPELL_UPDATE_TICKS = 1
local BLUR_UPDATE_TICKS = 30
local ticksToWait = DIZZY_SPELL_UPDATE_TICKS


local function adjustBlurLevels()
	if currentMode == MODE_NONE then return end

	if ticksToWait > 0 then
		ticksToWait = ticksToWait - 1
		return
	else
		if currentMode == MODE_DIZZY_SPELL then
			ticksToWait = DIZZY_SPELL_UPDATE_TICKS
		else
			ticksToWait = BLUR_UPDATE_TICKS
		end
	end

	if targetBlur == 0 and currentBlur < 0.05 and currentMode == MODE_DIZZY_SPELL then
		WME_OverlayManager.clearAllEffects()
		return
	end

	if currentBlur < targetBlur then
		setBlurLevel(currentBlur + 0.01)
	else
		if currentMode == MODE_BLURRED and currentBlur > targetBlur then
			setBlurLevel(currentBlur - 0.01)
		end

		if currentMode == MODE_DIZZY_SPELL then
			setBlurLevel(currentBlur - 0.01)
			targetBlur = 0
		end
	end
end

Events.OnTick.Add(adjustBlurLevels)