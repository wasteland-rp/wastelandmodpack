---
--- WME_PlayerMedicalState.lua
--- 04/01/2025
---

WME_PlayerMedicalState = {}

function WME_PlayerMedicalState:new(username, conditions, addictions, randomSymptoms)
	local o = {}
	setmetatable(o, self)
	self.__index = self
	o.username = username
	o.conditions = conditions or {}
	o.addictions = addictions or {}
	o.randomSymptoms = randomSymptoms or {}
	return o
end

function WME_PlayerMedicalState:increaseCondition(condition, severityIncrease)
	if not condition then error("condition is missing") end
	if not severityIncrease then error("severityIncrease is missing") end
	local maxValue = condition.max or WME_ConditionUtils.DEFAULT_MAX_SEVERITY
	if not self.conditions[condition] then
		self.conditions[condition] = math.min(severityIncrease, maxValue)
	else
		self.conditions[condition] = math.min(self.conditions[condition] + severityIncrease, maxValue)
	end
end

function WME_PlayerMedicalState:increaseConditionIfPresent(condition, severityIncrease)
	if self.conditions[condition] then
		self:increaseCondition(condition, severityIncrease)
	end
end

function WME_PlayerMedicalState:reduceCondition(condition, severityDecrease)
	if not condition then error("condition is missing") end
	if not severityDecrease then error("severityDecrease is missing") end
	if self.conditions[condition] then
		self.conditions[condition] = self.conditions[condition] - severityDecrease
		if self.conditions[condition] <= 0 then
			self.conditions[condition] = nil
		end
	end
end

function WME_PlayerMedicalState:setSeverity(condition, severity)
	if not condition then error("condition is missing") end
	if not severity then error("severity is missing") end
	local maxValue = condition.max or WME_ConditionUtils.DEFAULT_MAX_SEVERITY
	self.conditions[condition] = math.min(severity, maxValue)
end

function WME_PlayerMedicalState:removeCondition(condition)
	if not condition then error("condition is missing") end
	self.conditions[condition] = nil
end

function WME_PlayerMedicalState:serialise()
	local serialisedData = {}
	serialisedData.username = self.username
	serialisedData.conditions = {}
	for condition, severity in pairs(self.conditions) do
		serialisedData.conditions[condition.key] = severity
	end
	serialisedData.addictions = self.addictions -- This is just a table of string -> boolean
	serialisedData.randomSymptoms = {}
	for symptom, severity in pairs(self.randomSymptoms) do
		serialisedData.randomSymptoms[symptom.key] = severity
	end
	return serialisedData
end

function WME_PlayerMedicalState:resetForPlayerDeath()
	self.conditions = {}
	self.randomSymptoms = {}
end

function WME_PlayerMedicalState:sendToServer()
	local serialisedData = self:serialise()
	if self.username == getPlayer():getUsername() then
		WME_Client.setPlayerMedicalData(serialisedData)
	else
		WME_Client.setPlayerMedicalDataAndUpdate(serialisedData)
	end
end

function WME_PlayerMedicalState:loadSerialisedData(data)
	if not data.username == self.username then
		error("Wrong username in serialised data. Expected: " .. self.username .. ", sent: " .. data.username)
	end

	self.conditions = {}
	if data.conditions then
		for conditionKey, severity in pairs(data.conditions) do
			local condition = WME_ConditionUtils.getConditionFromKey(conditionKey)
			if condition then
				self.conditions[condition] = severity
			end
		end
	end

	self.addictions = data.addictions or {}

	self.randomSymptoms = {}
	if data.randomSymptoms then
		for symptomKey, severity in pairs(data.randomSymptoms) do
			local symptom = WME_ConditionUtils.getSymptomFromKey(symptomKey)
			if symptom then
				self.randomSymptoms[symptom] = severity
			end
		end
	end
end