---
--- WME_RPHints.lua
--- 10/11/2024
---

require "WME_ConditionHandler"
WME_RPHints = WME_RPHints or {}
WME_RPHints.cachedHints = {}

function WME_RPHints.updateRpHints()
	WME_RPHints.cachedHints = {}
	if not WME_ConditionHandler.currentPlayerMedicalState then return end

	for condition, conditionSeverity in pairs(WME_ConditionHandler.currentPlayerMedicalState.conditions) do
		if condition.rpHints then
			local highestHint = nil
			local highestSeverity = -1
			for _, rpHint in ipairs(condition.rpHints) do
				if conditionSeverity >= rpHint.severity and rpHint.severity > highestSeverity then
					highestHint = rpHint.text
					highestSeverity = rpHint.severity
				end
			end

			if highestHint then
				table.insert(WME_RPHints.cachedHints, highestHint)
			end
		end
	end
end


local rpHintsSystem = {}
rpHintsSystem.name = "WastelandMedical"

function rpHintsSystem.GetHints()
	return WME_RPHints.cachedHints
end

if getActivatedMods():contains("WastelandRpHints") then
	WRH.RegisterSystem(rpHintsSystem)
end

--[[
-- For testing when RP hints isn't loaded
Events.EveryTenMinutes.Add(function()
	print("Current hints:")
	local hints = rpHintsSystem.getHints()
	for _, hint in ipairs(hints) do
		print(hint)
	end
end)--]]
