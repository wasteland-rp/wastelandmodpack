---
--- WME_SymptomHandler.lua
--- 12/10/2024
---

require "WME_Symptom"

WME_SymptomHandler = WME_SymptomHandler or {}
WME_SymptomHandler.MIN_THRESHOLD = 10 -- The minimum severity for a symptom to do anything and create a moodle
WME_SymptomHandler.MAX_SEVERITY = 100
WME_SymptomHandler.currentSymptoms = {}

function WME_SymptomHandler.setCurrentSymptoms(symptoms)
	WME_SymptomHandler.currentSymptoms = symptoms
	WME_SymptomHandler.applyFixedEffects()
end

function WME_SymptomHandler.onTick()
	local player = getPlayer()
	if not player then return end
	if player:isGodMod() then return end

	local shortnessOfBreath = WME_SymptomHandler.currentSymptoms[WME_Symptom.SHORT_BREATH]
	if shortnessOfBreath and shortnessOfBreath >= WME_SymptomHandler.MIN_THRESHOLD then
		local maxEndurance = 1.0 - 0.25 * (shortnessOfBreath / WME_SymptomHandler.MAX_SEVERITY)
		local currentEndurance = player:getStats():getEndurance()
		if currentEndurance > maxEndurance then
			player:getStats():setEndurance(maxEndurance)
		end
	end

	local healthDamage = WME_SymptomHandler.currentSymptoms[WME_Symptom.HEALTH_DAMAGE]
	if healthDamage then
		if healthDamage > 0 then
			local targetHealth = 100 - healthDamage
			if player:getBodyDamage():getOverallBodyHealth() > targetHealth then
				player:getBodyDamage():ReduceGeneralHealth(0.03)
			end
		end
	end

	-- Panic decreases quickly so had to be moved up here
	local panic = WME_SymptomHandler.currentSymptoms[WME_Symptom.PANIC]
	local suppressPanic = WME_SymptomHandler.currentSymptoms[WME_Symptom.SUPPRESS_PANIC]
	local isPanicSuppressed = suppressPanic and suppressPanic >= WME_SymptomHandler.MIN_THRESHOLD

	if isPanicSuppressed then
		local panicLevel = player:getStats():getPanic()
		if panicLevel > 0 then
			player:getStats():setPanic(0)
		end
	else
		if panic and panic >= WME_SymptomHandler.MIN_THRESHOLD then
			local panicLevel = player:getStats():getPanic()
			if panicLevel < panic then
				player:getStats():setPanic(panic)
			end
		end
	end

	-- We used to do this for endurance boost symptom but it seemed dodgy the way it went up, so ditched that symptom
	-- endurance = endurance + (0.0000001 * enduranceBoost)
end

local function tripChance(player)
	local isRunning = player:IsRunning()
	local isSprinting = player:isSprinting()
	if not isRunning and not isSprinting then return end

	local unsteadiness = player:getStats():getDrunkenness()
	local dizziness = WME_SymptomHandler.currentSymptoms[WME_Symptom.DIZZY]
	if dizziness and dizziness > unsteadiness then
		unsteadiness = dizziness
	end
	if unsteadiness < WME_SymptomHandler.MIN_THRESHOLD then return end

	local chance = isSprinting and 50000 or 250000
	chance = chance * (player:HasTrait("Graceful") and 1.3 or 1)
	chance = chance * (player:HasTrait("Clumsy") and 0.7 or 1)
	local tripped = ZombRand(chance) < (unsteadiness - 10)
	if tripped then
		local side = ZombRand(2) == 0 and "left" or "right"
		player:setBumpFallType("FallForward");
		player:setBumpType(side);
		player:setBumpDone(false);
		player:setBumpFall(true);
		player:reportEvent("wasBumped");
	end
end

local function applyPainSymptom(symptom, bodyPartType)
	local pain = WME_SymptomHandler.currentSymptoms[symptom]
	if pain and pain >= 15 then -- Min threshold for pain moodle
		local bodyPart = getPlayer():getBodyDamage():getBodyPart(bodyPartType)
		if bodyPart:getAdditionalPain() < pain then
			bodyPart:setAdditionalPain(pain)
		end
	end
end

function WME_SymptomHandler.checkHourlySymptoms()
	local player = getPlayer()
	if not player then return end

	local fluHealing = WME_SymptomHandler.currentSymptoms[WME_Symptom.FLU_HEALING]
	if fluHealing and fluHealing >= WME_SymptomHandler.MIN_THRESHOLD then
		local bodyDamage = player:getBodyDamage()
		if bodyDamage:isHasACold() then
			if bodyDamage:getColdStrength() < 15 then
				bodyDamage:setColdStrength(0)
				bodyDamage:setHasACold(false)
				bodyDamage:setCatchACold(0)
			else
				bodyDamage:setColdStrength(bodyDamage:getColdStrength() - 15);
			end
		end
	end

	local burnHealing = WME_SymptomHandler.currentSymptoms[WME_Symptom.BURN_HEALING]
	if burnHealing and burnHealing >= WME_SymptomHandler.MIN_THRESHOLD then
		local bodyParts = player:getBodyDamage():getBodyParts()
		for i=0, bodyParts:size()-1 do
			local bodyPart = bodyParts:get(i)
			local burn = bodyPart:getBurnTime();
			if burn > 0 then
				local target = math.max(burn - 1, 0)
				bodyPart:setBurnTime(target)
			end
		end
	end

	local muscleWasting = WME_SymptomHandler.currentSymptoms[WME_Symptom.MUSCLE_WASTING]
	if muscleWasting and muscleWasting >= WME_SymptomHandler.MIN_THRESHOLD then
		-- This works on some players but not others. There seems to be a bug in AddXP, even on the player stats panel.
		local strengthLevel = player:getPerkLevel(Perks.Strength)
		WL_Utils.reduceXP(Perks.Strength, muscleWasting * strengthLevel)
	end

	local anger = WME_SymptomHandler.currentSymptoms[WME_Symptom.ANGER]
	if anger and anger >= WME_SymptomHandler.MIN_THRESHOLD then
		player:getStats():setAnger(anger / WME_SymptomHandler.MAX_SEVERITY)
	end

	local painSuppressant = WME_SymptomHandler.currentSymptoms[WME_Symptom.SUPPRESS_PAIN]
	if painSuppressant and painSuppressant >= WME_SymptomHandler.MIN_THRESHOLD then
		if player:getPainDelta() < 0.45 then
			player:setPainDelta(0.45) -- This does nothing but vanilla does it so we do too for future compatibility
		end

		if player:getPainEffect() < 5400 then
			player:setPainEffect(5400) -- This is actually the pain suppressor effect
		end
	end

	applyPainSymptom(WME_Symptom.HEADACHE, BodyPartType.Head)
	applyPainSymptom(WME_Symptom.CHEST_PAIN, BodyPartType.Torso_Upper)
	applyPainSymptom(WME_Symptom.STOMACH_ACHE, BodyPartType.Torso_Lower)
end

local dizzyChanceMultiplier = 1.0
local MIN_DIZZY_CHANCE_MULTIPLIER = 0.05
local DIZZY_CHANCE_INCREASE = 0.05

local function rollDizzySpellChance(dizzySeverity)
	if dizzyChanceMultiplier >= 1.0 then
		if WME_OverlayManager.attemptDizzySpell() then
			dizzyChanceMultiplier = MIN_DIZZY_CHANCE_MULTIPLIER
		end
		return
	end

	local dizzySpellTriggered = ZombRand(200) < (dizzySeverity * dizzyChanceMultiplier)

	if dizzySpellTriggered then
		if WME_OverlayManager.attemptDizzySpell() then
			dizzyChanceMultiplier = MIN_DIZZY_CHANCE_MULTIPLIER
			return
		end
	end

	dizzyChanceMultiplier = dizzyChanceMultiplier + DIZZY_CHANCE_INCREASE
end

--- Fixed effects are symptoms that are not increasing or decreasing by some increment, but are instead simply being
--- set to a fixed value based on current severities. As such this function can be called repeatedly without issue to
--- 'refresh' the current symptoms.
function WME_SymptomHandler.applyFixedEffects()
	local player = getPlayer()
	if not player then return end

	local stress = WME_SymptomHandler.currentSymptoms[WME_Symptom.STRESS]
	local suppressStress = WME_SymptomHandler.currentSymptoms[WME_Symptom.SUPPRESS_STRESS]
	local isStressSuppressed = suppressStress and suppressStress >= WME_SymptomHandler.MIN_THRESHOLD

	if isStressSuppressed then
		local stressLevel = player:getStats():getStress()
		if stressLevel > 0 then
			player:getStats():setStress(0)
		end
	else
		if stress and stress >= WME_SymptomHandler.MIN_THRESHOLD then -- The moodle won't actually show up until 25+
			local targetStress = stress / WME_SymptomHandler.MAX_SEVERITY
			if player:getStats():getStress() < targetStress then
				player:getStats():setStress(targetStress)
			end
		end
	end

	local euphoria = WME_SymptomHandler.currentSymptoms[WME_Symptom.EUPHORIA]
	if euphoria and euphoria >= WME_SymptomHandler.MIN_THRESHOLD then
		player:getBodyDamage():setUnhappynessLevel(0);
		if euphoria >= 30 then
			player:getBodyDamage():setBoredomLevel(0)
		end
		if euphoria >= 55 then
			player:getStats():setStress(0.0);
		end
		if euphoria >= 80 then
			player:getStats():setHunger(0.0) -- This is fine as it's just the feeling not actual calories
		end
	end

	local drugsMoveBoost = 0
	local movementBoost = WME_SymptomHandler.currentSymptoms[WME_Symptom.MOVEMENT_BOOST]
	if movementBoost then
		if movementBoost >= 80 then
			drugsMoveBoost = 0.4
		elseif movementBoost >= 55 then
			drugsMoveBoost = 0.3
		elseif movementBoost >= 30 then
			drugsMoveBoost = 0.2
		elseif movementBoost >= 10 then
			drugsMoveBoost = 0.1
		end
	end

	local movementPenalty= WME_SymptomHandler.currentSymptoms[WME_Symptom.MOVEMENT_PENALTY]
	if movementPenalty then
		if movementPenalty >= 80 then
			drugsMoveBoost = -0.4
		elseif movementPenalty >= 55 then
			drugsMoveBoost = -0.3
		elseif movementPenalty >= 30 then
			drugsMoveBoost = -0.2
		elseif movementPenalty >= 10 then
			drugsMoveBoost = -0.1
		end
	end
	WL_Utils.setMoveBoostType(player, "Drugs", drugsMoveBoost)

	local drugsActionBoost = 0
	local actionBoost = WME_SymptomHandler.currentSymptoms[WME_Symptom.ACTION_BOOST]
	if actionBoost then
		if actionBoost >= 80 then
			drugsActionBoost = 0.4
		elseif actionBoost >= 55 then
			drugsActionBoost = 0.3
		elseif actionBoost >= 30 then
			drugsActionBoost = 0.2
		elseif actionBoost >= 10 then
			drugsActionBoost = 0.1
		end
	end

	local actionPenalty = WME_SymptomHandler.currentSymptoms[WME_Symptom.ACTION_PENALTY]
	if actionPenalty then
		if actionPenalty >= 80 then
			drugsActionBoost = -0.4
		elseif actionPenalty >= 55 then
			drugsActionBoost = -0.3
		elseif actionPenalty >= 30 then
			drugsActionBoost = -0.2
		elseif actionPenalty >= 10 then
			drugsActionBoost = -0.1
		end
	end
	WL_Utils.setActionBoostType(player, "Drugs", drugsActionBoost)
end

function WME_SymptomHandler.checkTenMinuteSymptoms()
	local player = getPlayer()
	if not player then return end
	if player:isGodMod() then
		WME_SymptomHandler.currentSymptoms = {}
		WME_OverlayManager.clearAllEffects()
		return
	end

	WME_SymptomHandler.applyFixedEffects()

	local reduceHunger = WME_SymptomHandler.currentSymptoms[WME_Symptom.REDUCE_HUNGER]
	if reduceHunger and reduceHunger >= WME_SymptomHandler.MIN_THRESHOLD then
		local hunger = player:getStats():getHunger()
		local newHunger = hunger - (0.1 * (reduceHunger / WME_SymptomHandler.MAX_SEVERITY))
		player:getStats():setHunger(math.max(newHunger, 0.0))
	end

	local muscleHealing = WME_SymptomHandler.currentSymptoms[WME_Symptom.MUSCLE_HEALING]
	if muscleHealing and muscleHealing >= WME_SymptomHandler.MIN_THRESHOLD then
		local bodyParts = player:getBodyDamage():getBodyParts()
		for i=0, bodyParts:size()-1 do
			local bodyPart = bodyParts:get(i)
			local stiffness = bodyPart:getStiffness()
			if stiffness > 0 then
				local target = math.max(stiffness - 2, 0)
				bodyPart:setStiffness(target)
			end
		end
	end

	local diarrheaWalkSpeedPenalty = 0
	local diarrhea = WME_SymptomHandler.currentSymptoms[WME_Symptom.DIARRHEA]
	if diarrhea and diarrhea >= WME_SymptomHandler.MIN_THRESHOLD then
		local thirst = player:getStats():getThirst()
		local newThirst = thirst + (0.05 * (diarrhea / WME_SymptomHandler.MAX_SEVERITY))
		player:getStats():setThirst(math.min(newThirst, 1.0))

		if diarrhea > 80 then
			diarrheaWalkSpeedPenalty = -0.25
		end
	end
	WL_Utils.setMoveBoostType(player, "Diarrhea", diarrheaWalkSpeedPenalty)

	local blurLevel = player:getStats():getDrunkenness()
	local blurrySeverity = WME_SymptomHandler.currentSymptoms[WME_Symptom.BLURRY_VISION]
	if blurrySeverity and blurrySeverity >= WME_SymptomHandler.MIN_THRESHOLD then
		if blurrySeverity > blurLevel then
			blurLevel = blurrySeverity
		end
	end
	WME_OverlayManager.updateBlurLevel(blurLevel)

	local dizzySeverity = WME_SymptomHandler.currentSymptoms[WME_Symptom.DIZZY]
	if dizzySeverity and dizzySeverity >= WME_SymptomHandler.MIN_THRESHOLD then
		rollDizzySpellChance(dizzySeverity)
	end

	local euphoria = WME_SymptomHandler.currentSymptoms[WME_Symptom.EUPHORIA]
	if not euphoria or euphoria < WME_SymptomHandler.MIN_THRESHOLD then
		local depression = WME_SymptomHandler.currentSymptoms[WME_Symptom.DEPRESSION]
		if depression and depression >= WME_SymptomHandler.MIN_THRESHOLD then
			local unhappyLevel = player:getBodyDamage():getUnhappynessLevel()
			unhappyLevel = unhappyLevel + (0.05 * depression)
			player:getBodyDamage():setUnhappynessLevel(unhappyLevel);
		end
	end

	local nausea = WME_SymptomHandler.currentSymptoms[WME_Symptom.NAUSEA]
	if nausea and nausea > WME_SymptomHandler.MIN_THRESHOLD then -- Really 26+ is needed before it shows
		local sickness = player:getBodyDamage():getFoodSicknessLevel()
		if sickness < nausea then
			player:getBodyDamage():setFoodSicknessLevel(nausea)
		end
	end
end

Events.OnPlayerMove.Add(tripChance);
Events.EveryTenMinutes.Add(WME_SymptomHandler.checkTenMinuteSymptoms)
Events.OnTick.Add(WME_SymptomHandler.onTick)

-- Makes drunk last longer, default is 0.0042 in vanilla
Events.OnCreatePlayer.Add(function(_, player)
	player:getBodyDamage():setDrunkReductionValue(0.001)
	WME_SymptomHandler.checkTenMinuteSymptoms() -- This will set the drunk/fuzzy levels
	WME_OverlayManager.restoreOverlayEffects() -- Jumps direct to the highest fuzzy level
end)