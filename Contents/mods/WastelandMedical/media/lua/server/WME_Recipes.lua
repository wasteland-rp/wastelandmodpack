---
--- WME_Recipes.lua
--- 03/12/2024
---

function Recipe.OnCreate.heroineSyringe(items, result, player)
	player:getInventory():AddItem("Base.Spoon");
end

function Recipe.OnCreate.takeBloodSample(items, result, player)
	if result then
		local username = player:getUsername()
		result:setAge(0)
		result:getModData().WME_Username = username
		result:getModData().WME_BloodAlcohol = player:getStats():getDrunkenness()

		if WME_ConditionHandler and WME_ConditionHandler.currentPlayerMedicalState then
			local serialisedConditions = {}
			for condition, severity in pairs(WME_ConditionHandler.currentPlayerMedicalState.conditions) do
				if condition.bloodTest then
					serialisedConditions[condition.key] = severity
				end
			end
			result:getModData().WME_Conditions = serialisedConditions
		end

		-- Compatibility with WastelandInfection
		if getActivatedMods():contains("WastelandInfection") then
			local amount = WLIF_InfectionSystem:getInfectionAmount(username)
			result:getModData().WLIF_InfectionLevel = amount
			result:getModData().WLIF_Username = username
		end

		player:getInventory():AddItem("WLSyringeDirty")
	end
end