---
--- WME_Server.lua
--- 13/06/2024
---

if isClient() then return end

WME_PlayerData = WME_PlayerData or nil
WME_ServerDataIsDirty = WME_ServerDataIsDirty or false

local function onInitGlobalModData()
	WME_PlayerData = ModData.getOrCreate("WME_PlayerData")
end

local function saveModData()
	ModData.add("WME_PlayerData", WME_PlayerData)
end

local function onMinutePassed()
	if WME_ServerDataIsDirty then
		saveModData()
		WME_ServerDataIsDirty = false
	end
end

local Commands = {}

function Commands.fetchPlayerMedicalData(player, args)
	local medicalData = WME_PlayerData[args.username]
	if not medicalData then
		medicalData = { username = args.username }
	end
	sendServerCommand(player, "WastelandMedical", "receivePlayerMedicalData", medicalData)
end

function Commands.setPlayerMedicalData(player, medicalData)
	WME_PlayerData[medicalData.username] = medicalData
	WME_ServerDataIsDirty = true -- We will get every player's data at once on the hour so this is important
end

local function getPlayerByUserNameSafer(username)
	local player = getPlayerFromUsername(username)
	if player then
		return player
	end
	local players = getOnlinePlayers()
	for i=0, players:size()-1 do
		local player = players:get(i)
		if player:getUsername() == username then
			return player
		end
	end
	return nil
end

function Commands.setPlayerMedicalDataAndUpdate(player, medicalData)
	Commands.setPlayerMedicalData(player, medicalData)
	local updatedPlayer = getPlayerByUserNameSafer(medicalData.username)
	if updatedPlayer then
		sendServerCommand(updatedPlayer, "WastelandMedical", "receivePlayerMedicalData", medicalData)
	end
end

local function onClientCommand(module, command, player, args)
	if module ~= "WastelandMedical" then return end
	if not Commands[command] then return end
	if not WME_PlayerData then return end
	Commands[command](player, args)
end

Events.OnInitGlobalModData.Add(onInitGlobalModData)
Events.OnClientCommand.Add(onClientCommand)
Events.EveryOneMinute.Add(onMinutePassed)
