require('NPCs/MainCreationMethods');

local function onGameBoot()
    TraitFactory.addTrait("Alcoholic", getText("UI_trait_alcoholic"), -4, getText("UI_trait_alcoholicdesc"), false, false)
end

Events.OnGameBoot.Add(onGameBoot);
