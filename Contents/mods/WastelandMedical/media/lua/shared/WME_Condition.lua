---
--- WME_Condition.lua
--- 10/10/2024
---
--- Conditions are long term health effects that can be detected by doctors. They cause symptoms. They can be negative
--- such as diseases and poisoning, or positive such as medicine.
---
--- Fields
--- key: MANDATORY string, unique key for the condition
--- max: OPTIONAL number, the maximum severity the condition can reach, default is 100
--- hourlyChange: OPTIONAL number, the change in severity per game hour, default is 0. e.g. hourlyChange of -1 means
--- the condition decreases by 1 per game hour or -8 per real life hour (average, depends on day/night time)
--- bloodTest: OPTIONAL string, the result of a blood test that confirms the condition. If missing it doesn't show up.
--- symptoms: OPTIONAL table, symptoms that the condition causes. If missing it doesn't cause any symptoms.
--- sideEffects: OPTIONAL table, symptoms that the condition causes when it's a medicine ingested without the condition
--- that it cures. Uses the same format as the symptoms table.
--- removeConditions: OPTIONAL table, conditions that are countered by the condition.
--- rpHints: OPTIONAL table, roleplay hints that the player can get when the condition reaches a certain severity.
---
--- Symptoms Table:
--- symptom: MANDATORY WME_Symptom, the symptom that the condition causes
--- start: OPTIONAL number, the severity at which the symptom starts building up, default is 0
--- max: OPTIONAL number, the maximum severity the symptom can reach, default is the condition's max
--- multiplier: OPTIONAL number, the multiplier for the symptom's severity, default is 1
--- chance: OPTIONAL number, the chance for the symptom to appear, default is 100%
--- Example: { symptom = WME_Symptom.DIZZY, start = 30, max = 50, multiplier = 2, chance = 20 }
--- This symptom wouldn't begin building up until 30, but would increase by 2 per condition severity due to the
--- multiplier. Then when it hits 40 points (at 50 severity) it would stop increasing. It has a 20% chance to appear
--- each hour.
---
--- Side Effects Table: Identical format to the symptoms table above but triggers only when the condition has conditions
--- that it removes and those conditions are not present. The medicine will 'self destruct' to zero if it successfully
--- cures all of it's removeConditions, so side effects are only afflicted on people needlessly taking the medicine.
---
--- removeConditions Table:
--- key: MANDATORY string, the key of the condition that is countered by the condition
--- speed: MANDATORY number, the speed at which the condition is countered, e.g. speed of 5 means the condition is
--- reduced by 5 per game hour or 40 per real life hour (average, depends on day/night time)
---
--- rpHints Table:
--- severity: MANDATORY number, the severity at which the hint is shown. Only the worst severity hint is shown.
--- text: MANDATORY string, the text of the hint
---
--- addiction Table:
--- start: MANDATORY number, the severity at which there begins a chance for addiction
--- max: MANDATORY number, the maximum severity the addiction chance can reach
--- chanceIn: MANDATORY number, chance for the addiction. For example: Condition severity is 30. Addiction start=10 and
--- max=20, and chanceIn = 200. The player has a 10/200 chance each hour to become addicted
--- type: MANDATORY string, the key of the addiction condition that will be added when the addiction starts building up

require "WME_Symptom"

WME_Condition = {
	
	--TODO if the player dies we need to remove all conditions (but not addictions)

	BACTERIAL_INFECTION = {  -- cellulitis
		key = "bacterial_infection",
		max = 60,
		symptoms = {
			{ symptom = WME_Symptom.DIZZY, chance = 20 },
			{ symptom = WME_Symptom.SHORT_BREATH, start = 30, max = 50, multiplier = 2 },
			{ symptom = WME_Symptom.NAUSEA, start = 20, max = 60 }
		},
		applyHealSpeedTraits = true,
		bloodTest = "elevated leukocytes and inflammatory markers suggest a bacterial infection consistent with cellulitis.",
		rpHints = {
			{ severity = 10, text = "Stitched wound is red and swollen." },
			{ severity = 50, text = "Stitched wound is hot, red and swollen\nFever and chills." },
		}
	},

	SEPSIS = {
		key = "sepsis",
		hourlyChange = -1,
		symptoms = {
			{ symptom = WME_Symptom.NAUSEA },
			{ symptom = WME_Symptom.DIZZY, chance = 20 },
			{ symptom = WME_Symptom.SHORT_BREATH, start = 30, max = 50, multiplier = 2 },
		},
		applyHealSpeedTraits = true,
		bloodTest = "elevated leukocytes, inflammatory markers, and blood cultures confirm a systemic infection consistent with sepsis.",
		rpHints = {
			{ severity = 20, text = "Mild fever, fatigue, elevated heart rate." },
			{ severity = 50, text = "High fever, rapid breathing, feeling confused." },
		}
	},

	FUNGAL_INFECTION = { -- sporotrichosis
		key = "fungal_infection",
		max = 60,
		symptoms = {
			{ symptom = WME_Symptom.DIZZY, chance = 20 },
			{ symptom = WME_Symptom.SHORT_BREATH, start = 30, max = 50, multiplier = 2 },
		},
		applyHealSpeedTraits = true,
		bloodTest = "granulomatous inflammation and fungal elements in tissue culture, consistent with sporotrichosis.",
		rpHints = {
			{ severity = 10, text = "Stitched wound is red and swollen." },
			{ severity = 50, text = "Stitched wound shows red, firm nodules extending along the arm." },
		}
	},

	-- This should have muscle wasting but it was disabled due to a bug
	MALNUTRITION = {
		key = "malnutrition",
		symptoms = {
			{ symptom = WME_Symptom.SHORT_BREATH },
		},
		bloodTest = "low serum albumin, hypoglycemia, and micronutrient deficiencies indicative of prolonged nutrient deficiency and consistent with malnutrition.",
	},

	DYSENTERY = {
		key = "dysentery",
		hourlyChange = 4,
		symptoms = {
			{ symptom = WME_Symptom.STOMACH_ACHE, chance = 30 },
			{ symptom = WME_Symptom.NAUSEA, max = 30},
			{ symptom = WME_Symptom.DIARRHEA, chance = 50}
		},
		applyHealSpeedTraits = true,
		bloodTest = "elevated leukocytes and neutrophils with an inflammatory response pattern, suggestive of enteric bacterial infection consistent with dysentery.",
	},

	NO2_POISONING = {
		key = "no2_poisoning",
		symptoms = {
			{ symptom = WME_Symptom.SHORT_BREATH },
			{ symptom = WME_Symptom.DIZZY, chance = 30 },
		},
		bloodTest = "methemoglobinemia, suggestive of Nitrogen Oxide exposure.",
	},

	RADIATION = {
		key = "radiation_poisoning",
		symptoms = {
			{ symptom = WME_Symptom.HEADACHE, chance = 20 },
			{ symptom = WME_Symptom.NAUSEA, start = 10 },
			{ symptom = WME_Symptom.DIZZY, start = 55, multiplier = 3, chance = 25 }
		},
		bloodTest = "leukopenia, thrombocytopenia, and anemia suggest acute radiation sickness.",
	},

	----------------------- Recreational Drugs -----------------------

	OPIOIDS = {
		key = "opioids",
		hourlyChange = -4,  -- IRL, small doses run out in 4-6 hours, heavy doses would be 12-24
		                       -- in PZ terms for us that means 35 severity = 4-6 hrs, 80 severity = 20 hrs
		symptoms = {
			{ symptom = WME_Symptom.BLURRY_VISION, start=15, multiplier=2, },
			{ symptom = WME_Symptom.DIZZY },
			{ symptom = WME_Symptom.EUPHORIA },
			{ symptom = WME_Symptom.SUPPRESS_PAIN },
			{ symptom = WME_Symptom.SHORT_BREATH, start=50, multiplier=3 },
			{ symptom = WME_Symptom.HEALTH_DAMAGE, start=85, multiplier=10 },
		},
		bloodTest = "opioid compounds, suggestive of Morphine, Fentanyl or Oxycodone use.",
		rpHints = {
			{ severity = 10, text = "Euphoria, numb to pain, slowed breathing, floaty sensation." },
			{ severity = 60, text = "Euphoria, heaviness, sedation and shallow breathing." },
			{ severity = 85, text = "Dangerously slow breathing, drowsiness and extreme lethargy" }
		},
		addiction = { start = 30, max = 80, chanceIn = 500, type = "opioidWithdrawal" },
	},

	OPIOID_WITHDRAWAL = {
		key = "opioidWithdrawal",
		max = 120,
		symptoms = {
			{ symptom = WME_Symptom.STRESS },
			{ symptom = WME_Symptom.HEADACHE, start = 40, chance = 35 },
			{ symptom = WME_Symptom.STOMACH_ACHE, start = 75, chance = 30 },
			{ symptom = WME_Symptom.NAUSEA, start = 75 },
			{ symptom = WME_Symptom.DIARRHEA, start = 85, chance = 20 },
		},
		rpHints = {
			{ severity = 25, text = "Mild opioid withdrawal\n - Anxious, restless, muscle aches." },
			{ severity = 50, text = "Opioid withdrawal\n - Experiencing headaches, shivering, difficulty sleeping." },
			{ severity = 100, text = "Extreme opioid withdrawal\n - Nausea, vomiting, stomach cramps, sweating, craving opium" },
		},
	},

	COCAINE = {
		key = "cocaine",
		hourlyChange = -5,
		symptoms = {
			{ symptom = WME_Symptom.EUPHORIA, start=25, multiplier=3, max=40 },
			{ symptom = WME_Symptom.REDUCE_HUNGER, start=15, multiplier=10, max=25 },
			{ symptom = WME_Symptom.MOVEMENT_BOOST, start=25, multiplier=2 },
			{ symptom = WME_Symptom.ACTION_BOOST, start=25, max=35 },
			{ symptom = WME_Symptom.HEALTH_DAMAGE, start = 75, multiplier = 5 },
			{ symptom = WME_Symptom.PANIC, start=85, multiplier=10 },
		},
		bloodTest = "cocaine metabolites, indicative of recent cocaine use.",
		rpHints = {
			{ severity = 25, text = "Euphoria, confidence, restlessness" },
			{ severity = 35, text = "Intense euphoria, confidence, alert and energetic" },
			{ severity = 75, text = "Intense euphoria, extreme confidence, paranoia, rapid heart rate" },
			{ severity = 85, text = "Paranoia, extremely energetic, heart attack risk" }
		},
		addiction = { start = 30, max = 80, chanceIn = 500, type = "cocaineWithdrawal" },
	},

	COCAINE_WITHDRAWAL = {
		key = "cocaineWithdrawal",
		max = 80,
		symptoms = {
			{ symptom = WME_Symptom.DEPRESSION, start=15 },
			{ symptom = WME_Symptom.MOVEMENT_PENALTY, start=15, max=45 },
			{ symptom = WME_Symptom.STRESS, start=25 },
			{ symptom = WME_Symptom.ANGER, start=25, chance=20 },
			{ symptom = WME_Symptom.HEADACHE, start=30, chance=35 },
		},
		rpHints = {
			{ severity = 25, text = "Mild cocaine withdrawal\n - Depressed and low energy." },
			{ severity = 50, text = "Cocaine withdrawal\n - Cravings, irritability, difficulty sleeping." },
			{ severity = 65, text = "Extreme cocaine withdrawal\n - Cravings, depression and fatigue" },
		},
	},

	AMPHETAMINE = {
		key = "amphetamine",
		hourlyChange = -5,
		symptoms = {
			{ symptom = WME_Symptom.REDUCE_HUNGER },
			{ symptom = WME_Symptom.EUPHORIA, start=20, max=30 },
			{ symptom = WME_Symptom.MOVEMENT_BOOST, start=10, max=20 },
			{ symptom = WME_Symptom.ACTION_BOOST, start=10 },
			{ symptom = WME_Symptom.STRESS, start=50 },
			{ symptom = WME_Symptom.NAUSEA, start = 80, multiplier = 5 },
			{ symptom = WME_Symptom.CHEST_PAIN, start = 80, multiplier = 5 },
			{ symptom = WME_Symptom.PANIC, start=90, multiplier=10 },
		},
		bloodTest = "amphetamine metabolites, indicative of recent drug use.",
		rpHints = {
			{ severity = 20, text = "Alert, elevated mood" },
			{ severity = 35, text = "Euphoria, confidence, talkative and restless" },
			{ severity = 60, text = "Euphoria, Hyperfocused, paranoia and rapid heart rate" },
			{ severity = 80, text = "Paranoia, extremely energetic, rapid heartbeat" }
		},
		addiction = { start = 30, max = 80, chanceIn = 500, type = "amphetamineWithdrawal" },
	},

	AMPHETAMINE_WITHDRAWAL = {
		key = "amphetamineWithdrawal",
		max = 80,
		symptoms = {
			{ symptom = WME_Symptom.ACTION_PENALTY, start=15, max=45 },
			{ symptom = WME_Symptom.DEPRESSION, start=30 },
			{ symptom = WME_Symptom.STRESS, start=50 },
			{ symptom = WME_Symptom.PANIC, start=60, multiplier=3, chance=10 },
			{ symptom = WME_Symptom.ANGER, start=60, chance=20 },
		},
		rpHints = {
			{ severity = 25, text = "Mild amphetamine withdrawal\n - Cravings, lethargic, restless." },
			{ severity = 50, text = "Amphetamine withdrawal\n - Intense craving, depression, persistent fatigue" },
			{ severity = 65, text = "Extreme amphetamine withdrawal\n - Irritability, severe depression, panic attacks" },
		},
	},

	BENZODIAZEPINE = { -- Xanax etc
		key = "benzodiazepine",
		hourlyChange = -5,
		symptoms = {
			{ symptom = WME_Symptom.SUPPRESS_STRESS },
			{ symptom = WME_Symptom.EUPHORIA, start=40 },
			{ symptom = WME_Symptom.ACTION_PENALTY, start=50, multiplier=2 },
			{ symptom = WME_Symptom.MOVEMENT_PENALTY, start=50, multiplier=2 },
			{ symptom = WME_Symptom.SUPPRESS_PANIC, start=70 },
			{ symptom = WME_Symptom.SHORT_BREATH, start=70, multiplier=3 },
			{ symptom = WME_Symptom.DIZZY, start=70, multiplier=3 },
			{ symptom = WME_Symptom.BLURRY_VISION, start=70, multiplier=3 },
		},
		bloodTest = "benzodiazepine compounds, suggestive of recent use of medications such as Xanax or Valium.",
		rpHints = {
			{ severity = 20, text = "Feeling calm" },
			{ severity = 50, text = "Feeling tranquil" },
			{ severity = 60, text = "Drowsy but happy, slurred speech" },
			{ severity = 75, text = "Shallow breathing, sedated" },
		},
		addiction = { start = 40, max = 60, chanceIn = 500, type = "benzodiazepineWithdrawal" },
	},

	BENZODIAZEPINE_WITHDRAWAL = {
		key = "benzodiazepineWithdrawal",
		max = 70,
		symptoms = {
			{ symptom = WME_Symptom.STRESS, start=15 },
			{ symptom = WME_Symptom.DEPRESSION, start=20 },
			{ symptom = WME_Symptom.PANIC, start=50, multiplier=3, chance=15 },
			{ symptom = WME_Symptom.HEADACHE, start=25, chance=20 },
			{ symptom = WME_Symptom.ACTION_PENALTY, start=50 },
		},
		rpHints = {
			{ severity = 25, text = "Mild benzo withdrawal\n - Anxiety, feeling down, minor headaches" },
			{ severity = 50, text = "Benzo withdrawal\n - Depression, severe anxiety, headaches" },
			{ severity = 60, text = "Extreme benzo withdrawal\n - Anxiety, shaky hands, panic attacks" },
		},
	},

	SEDATIVE = {  -- Lorazepam
		key = "sedative",
		hourlyChange = -5,
		symptoms = {
			{ symptom = WME_Symptom.ACTION_PENALTY, start=10 },
			{ symptom = WME_Symptom.MOVEMENT_PENALTY, start=10 },
			{ symptom = WME_Symptom.SUPPRESS_PAIN, start=10 },
			{ symptom = WME_Symptom.SUPPRESS_PANIC, start=10 },
			{ symptom = WME_Symptom.SUPPRESS_STRESS, start=10 },
			{ symptom = WME_Symptom.BLURRY_VISION, start=30, multiplier=3},
			{ symptom = WME_Symptom.SHORT_BREATH, start=50, multiplier=2 },
			{ symptom = WME_Symptom.DIZZY, start=50, multiplier=2 },
			{ symptom = WME_Symptom.HEALTH_DAMAGE, start=90, multiplier=10 },
		},
		bloodTest = "lorazepam detected in plasma, indicative of sedative use.",
		rpHints = {
			{ severity = 20, text = "Feeling calm and drowsy, reduced alertness, poor coordination" },
			{ severity = 53, text = "Poor coordination, reduced alertness, shallow breathing" },
			{ severity = 95, text = "Starting to black out, the world is fading away" },
		},
	},

	ADRENALINE = { -- epinephrine
		key = "adrenaline",
		hourlyChange = -20,
		symptoms = {
			{ symptom = WME_Symptom.MOVEMENT_BOOST, start=20, max=50 },
			{ symptom = WME_Symptom.PANIC, start=10, multiplier=2 },
			{ symptom = WME_Symptom.HEADACHE, start=60, multiplier=2 },
			{ symptom = WME_Symptom.DIZZY, start=60, multiplier=2 },
			{ symptom = WME_Symptom.CHEST_PAIN, start=60, multiplier=2 },
			{ symptom = WME_Symptom.HEALTH_DAMAGE, start=90, multiplier=5 },
		},
		bloodTest = "elevated catecholamines, consistent with adrenaline administration or acute stress response.",
		rpHints = {
			{ severity = 10, text = "Alert, energetic, sweating, rapid heartbeat" },
			{ severity = 65, text = "Extremely alert, stressed, rapid heartbeat" },
			{ severity = 90, text = "Extremely rapid heartbeat, severe chest pains" },
		},
	},

	DRUNK_ALCOHOLIC = {
		key = "drunk_alcoholic",
		max = 20,
		symptoms = {
			{ symptom = WME_Symptom.SUPPRESS_STRESS },
		},
	},

	ALCOHOLIC_WITHDRAWAL = {
		key = "alcoholicWithdrawal",
		max = 80,
		symptoms = {
			{ symptom = WME_Symptom.STRESS, start=10 },
			{ symptom = WME_Symptom.ACTION_PENALTY, start=30 },
			{ symptom = WME_Symptom.NAUSEA, start = 60, multiplier = 2 },
		},
		rpHints = {
			{ severity = 25, text = "Mild alcohol withdrawal\n - Anxiety, restlessness" },
			{ severity = 40, text = "Moderate alcohol withdrawal\n - Irritability, sweating, noticeable hand tremors" },
			{ severity = 72, text = "Severe alcohol withdrawal\n - Nausea, sweating, hands shake uncontrollably" },
		},
	},

	----------------------- Cures -----------------------

	INFLUENZA_CURE = { -- Amantadine
		key = "influenza_cure",
		hourlyChange = -5,
		symptoms = {
			{ symptom = WME_Symptom.FLU_HEALING }
		},
		bloodTest = "elevated serum creatinine and electrolyte imbalances consistent with amantadine use."
	},

	POTASSIUM_IODIDE = {
		key = "potassium_iodide",
		hourlyChange = -5,
		removeConditions = {
			{ key = "fungal_infection", speed = 10 },
			{ key = "radiation_poisoning", speed = 10 },
		},
		sideEffects = {
			{ symptom = WME_Symptom.NAUSEA, max = 50 },
			{ symptom = WME_Symptom.STOMACH_ACHE, chance = 35 },
			{ symptom = WME_Symptom.DIARRHEA, chance = 50 },
		},
		bloodTest = "iodine compounds, suggestive of potassium iodide ingestion.",
	},

	OPIOIDS_CURE = {
		key = "opioids_cure",
		hourlyChange = -5,
		removeConditions = {
			{ key = "opioids", speed = 20 },
		},
		sideEffects = {
			{ symptom = WME_Symptom.CHEST_PAIN  },
			{ symptom = WME_Symptom.DIZZY, chance = 25  },
			{ symptom = WME_Symptom.HEADACHE, chance = 30 },
			{ symptom = WME_Symptom.NAUSEA, max = 50 },
		},
		bloodTest = "naloxone detected in plasma, an opioid overdose treatment.",
	},

	NO2_MEDICINE = { -- Methylene Blue
		key = "no2_medicine",
		hourlyChange = -5,
		removeConditions = {
			{ key = "no2_poisoning", speed = 5 }
		},
		bloodTest = "methylene blue, used to treat Methemoglobinemia.",
		sideEffects = {
			{ symptom = WME_Symptom.NAUSEA, max = 35 },
			{ symptom = WME_Symptom.HEADACHE, chance = 35 },
			{ symptom = WME_Symptom.DIZZY, chance = 30 },
		}
	},

	ANTIBIOTICS = {
		key = "antibiotics",
		hourlyChange = -5,
		removeConditions = {
			{ key = "dysentery", speed = 14 },
			{ key = "bacterial_infection", speed = 10 },
			{ key = "sepsis", speed = 10 },
		},
	},

	VITAMINS = { -- Vitamin
		key = "vitamins",
		hourlyChange = -1,
		removeConditions = {
			{ key = "malnutrition", speed = 5 }
		},
	},

	MUSCLE_HEALING = {
		key = "muscle_healing",
		hourlyChange = -5,
		sideEffects = {
			{ symptom = WME_Symptom.MUSCLE_HEALING },
		}
	},

	BURN_HEALING = {
		key = "burn_healing",
		hourlyChange = -5,
		sideEffects = {
			{ symptom = WME_Symptom.BURN_HEALING },
		}
	},

	----------------------- Dummy Medicines -----------------------

	MALARIA_MEDICINE = { -- Chloroquine
		key = "malaria_medicine",
		hourlyChange = -5,
		symptoms = {
			{ symptom = WME_Symptom.NAUSEA, max = 50 },
			{ symptom = WME_Symptom.STOMACH_ACHE, chance = 20 },
			{ symptom = WME_Symptom.DIARRHEA, chance = 25 },
			{ symptom = WME_Symptom.DIZZY, chance = 20 },
			{ symptom = WME_Symptom.HEADACHE, chance = 10 },
		},
		bloodTest = "hypokalemia and elevated creatine kinase, which suggests chloroquine toxicity from malaria treatments",
	},

	IVERMECTIN = {
		key = "ivermectin",
		hourlyChange = -5,
		symptoms = {
			{ symptom = WME_Symptom.NAUSEA, max = 50 },
			{ symptom = WME_Symptom.DIARRHEA, chance = 25 },
			{ symptom = WME_Symptom.DIZZY, chance = 20 },
			{ symptom = WME_Symptom.HEADACHE, chance = 10 },
			{ symptom = WME_Symptom.HEALTH_DAMAGE, start=85, multiplier=10 },
		},
		bloodTest = "ivermectin detected in plasma, indicative of recent use of anti-parasitic medication.",
	},

}