---
--- WME_ConditionUtils.lua
--- 05/01/2025
---

WME_ConditionUtils = {}

--- Default Max, can be overridden by individual conditions
WME_ConditionUtils.DEFAULT_MAX_SEVERITY = 100

function WME_ConditionUtils.getConditionFromKey(conditionKey)
	if conditionKey == nil then return nil end
	for _, condition in pairs(WME_Condition) do
		if condition.key == conditionKey then
			return condition
		end
	end
	return nil
end

function WME_ConditionUtils.getSymptomFromKey(symptomKey)
	if symptomKey == nil then return nil end
	for _, symptom in pairs(WME_Symptom) do
		if symptom.key == symptomKey then
			return symptom
		end
	end
	return nil
end

function WME_ConditionUtils.isBloodTestDetectable(conditionKey)
	local condition = WME_ConditionUtils.getConditionFromKey(conditionKey)
	if not condition then return false end
	return condition.bloodTest ~= nil
end