---
--- WME_Symptom.lua
---
--- Symptoms are consequences of conditions and have direct negative effects on the player and show up as moodles.
---
--- 10/10/2024
---

WME_Symptom = {
	FLU_HEALING = { key = "flu_cure", name = "Cure Flu" },
	MOVEMENT_PENALTY = { key = "movement_penalty", name = "Movement Penalty", moodle = "MovementPenalty" },
	MOVEMENT_BOOST = { key = "movement_boost", name = "Movement Boost", moodle = "MovementBoost", isGood = true },
	ACTION_PENALTY = { key = "action_penalty", name = "Action Penalty", moodle = "ActionPenalty" },
	ACTION_BOOST = { key = "action_boost", name = "Action Boost", moodle = "ActionBoost", isGood = true },
	REDUCE_HUNGER = { key = "reduce_hunger", name = "Reduced Appetite" },
	DIARRHEA = { key = "diarrhea", name = "Diarrhea", moodle = "Diarrhea" },
	BLURRY_VISION = { key = "blurry_vision", name = "Blurry Vision" },
	MUSCLE_HEALING = { key = "muscle_healing", name = "Muscle Healing" },
	BURN_HEALING = { key = "burn_healing", name = "Burn Healing" },
	EUPHORIA = { key = "euphoria", name = "Euphoria", moodle = "Euphoria", isGood = true },
	SUPPRESS_PAIN = { key = "suppress_pain", name = "Pain Suppressant" },
	MUSCLE_WASTING = { key = "muscle_wasting", name = "Muscle Wasting", moodle = "MuscleWasting" },
	HEALTH_DAMAGE = { key = "health_damage", name = "Health Deterioration" },
	STOMACH_ACHE = { key = "stomach_ache", name = "Stomach Ache" },
	CHEST_PAIN = { key = "chest_pain", name = "Chest Pain" },
	PANIC = { key = "panic", name = "Panic" },
	SUPPRESS_PANIC = { key = "suppress_panic", name = "Suppressed Panic" },
	STRESS = { key = "stress", name = "Stress" },
	SUPPRESS_STRESS = { key = "suppress_stress", name = "Suppressed Stress" },
	DEPRESSION = { key = "depression", name = "Depression" },
	ANGER = { key = "anger", name = "Anger" }, -- Anger doesn't actually do anything other than the moodle rn
	NAUSEA = { key = "nausea", name = "Nausea" },
	HEADACHE = { key = "headache", name = "Headache" },
	DIZZY = { key = "dizzy", name = "Dizziness", moodle = "Dizzy" },
	SHORT_BREATH = { key = "short_breath", name = "Short of Breath", moodle = "ShortBreath" }
}

