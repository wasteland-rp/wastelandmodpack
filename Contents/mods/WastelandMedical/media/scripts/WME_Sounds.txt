module Base {

	sound DizzyTones
	{
		category = Item, loop = false, is3D = true,
		clip {
			file = media/sound/DizzyTones.ogg,
			distanceMin = 10,
			distanceMax = 10,
			reverbFactor = 0.1,
			volume = 0.3,
		}
	}

	sound DrugSnort
	{
		category = Item, loop = false, is3D = false,
		clip {
			file = media/sound/DrugSnort.ogg,
			distanceMin = 10,
			distanceMax = 10,
			reverbFactor = 0.1,
			volume = 0.5,
		}
	}

	sound MassSpectrometer
	{
		category = Item, loop = false, is3D = true,
        clip {
            file = media/sound/MassSpectrometer.ogg,
            distanceMin = 15,
            distanceMax = 15,
            reverbFactor = 0.3,
            volume = 0.5,
        }
	}
}