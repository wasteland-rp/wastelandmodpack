--***********************************************************
--** Credits to:
--** Masayoshi Soken, Zach Beever, Danny Elfman, Koji Kondo, Rousseau, 
--** Tom Mucenieks, Sheet Music Boss, Spectral Piano, Piano Karaoke, 
--** KeijiFukudaMusic, NintenMusic, Montechait, Slava Makovsky,
--** Motoi Sakuraba, Yoko Shimomura, The Coffee Elf (Youtube),
--** John Carpenter, Noud van Harskamp, Susumu Hirasawa, Yasunori Mitsuda,
--** Kassia, Kenneth Nappier, Pianella Piano, Ivan Larionov
--***********************************************************

local performancePieces = require("TimedActions/SPPPerformancePieces") 
table.insert(performancePieces, {level=1,sound="KCCM01Sweden",length=214,name="ContextMenu_KCCM01_05"})
table.insert(performancePieces, {level=1,sound="KCCM01MakeAMan",length=83,name="ContextMenu_KCCM01_06"})
table.insert(performancePieces, {level=1,sound="KCCM01Spiderman",length=64,name="ContextMenu_KCCM01_07"})
table.insert(performancePieces, {level=2,sound="KCCM02WhatWasLost",length=93,name="ContextMenu_KCCM02_05"})
table.insert(performancePieces, {level=2,sound="KCCM02StillAlive",length=175,name="ContextMenu_KCCM02_06"})
table.insert(performancePieces, {level=3,sound="KCCM03SongofStorms",length=94,name="ContextMenu_KCCM03_05"})
table.insert(performancePieces, {level=4,sound="KCCM04MyImmortal",length=260,name="ContextMenu_KCCM04_06"})
table.insert(performancePieces, {level=4,sound="KCCM04NeathDarkWaters",length=329,name="ContextMenu_KCCM04_07"})
table.insert(performancePieces, {level=4,sound="KCCM04Halloween",length=79,name="ContextMenu_KCCM04_08"})
table.insert(performancePieces, {level=4,sound="KCCM04Zombie",length=287,name="ContextMenu_KCCM04_09"})
table.insert(performancePieces, {level=5,sound="KCCM05Prelude",length=177,name="ContextMenu_KCCM05_06"})
table.insert(performancePieces, {level=5,sound="KCCM05ThisIsHalloween",length=186,name="ContextMenu_KCCM05_07"})
table.insert(performancePieces, {level=5,sound="KCCM05MyWay",length=237,name="ContextMenu_KCCM05_08"})
table.insert(performancePieces, {level=6,sound="KCCM06StarsLongDead",length=220,name="ContextMenu_KCCM06_05"})
table.insert(performancePieces, {level=6,sound="KCCM06OogieBoogieSong",length=199,name="ContextMenu_KCCM06_06"})
table.insert(performancePieces, {level=6,sound="KCCM06FlyMeToTheMoon",length=159,name="ContextMenu_KCCM06_07"})
table.insert(performancePieces, {level=7,sound="KCCM07RunningUpThatHill",length=300,name="ContextMenu_KCCM07_06"})
table.insert(performancePieces, {level=7,sound="KCCM07ComeSailAway",length=356,name="ContextMenu_KCCM07_07"})
table.insert(performancePieces, {level=7,sound="KCCM07OnceUponADecember",length=170,name="ContextMenu_KCCM07_08"})
table.insert(performancePieces, {level=7,sound="KCCM07CruelAngelThesis",length=90,name="ContextMenu_KCCM07_09"})
table.insert(performancePieces, {level=8,sound="KCCM08DreamsOfMan",length=233,name="ContextMenu_KCCM08_06"})
table.insert(performancePieces, {level=8,sound="KCCM08TheEntertainer",length=221,name="ContextMenu_KCCM08_07"})
table.insert(performancePieces, {level=8,sound="KCCM08GutsTheme",length=166,name="ContextMenu_KCCM08_08"})
table.insert(performancePieces, {level=8,sound="KCCM08DearlyBeloved",length=238,name="ContextMenu_KCCM08_09"})
table.insert(performancePieces, {level=8,sound="KCCM08ADistantPromise",length=207,name="ContextMenu_KCCM08_10"})
table.insert(performancePieces, {level=9,sound="KCCM09ChopinRaindrop",length=380,name="ContextMenu_KCCM09_06"})
table.insert(performancePieces, {level=9,sound="KCCM09Katyusha",length=125,name="ContextMenu_KCCM09_07"})
table.insert(performancePieces, {level=9,sound="KCCM09SwanLake",length=211,name="ContextMenu_KCCM09_08"})
table.insert(performancePieces, {level=9,sound="KCCM09Kalinka",length=268,name="ContextMenu_KCCM09_09"})
table.insert(performancePieces, {level=10,sound="KCCM10MrBrightside",length=230,name="ContextMenu_KCCM10_11"})
table.insert(performancePieces, {level=10,sound="KCCM10LordOfCinders",length=185,name="ContextMenu_KCCM10_12"})
table.insert(performancePieces, {level=10,sound="KCCM10Liebesleid",length=269,name="ContextMenu_KCCM10_13"})
table.insert(performancePieces, {level=10,sound="KCCM10OneWingedAngel",length=238,name="ContextMenu_KCCM10_14"})
table.insert(performancePieces, {level=10,sound="KCCM10Megalovania",length=161,name="ContextMenu_KCCM10_15"})
