WLP_PortalConfig = ISCollapsableWindow:derive("WLP_PortalConfig")
WLP_PortalConfig.instance = nil

local function getTextOrNil(item)
    if item == nil or item == "" then
        return nil
    end
    return item
end

function WLP_PortalConfig:show(portalGroupId)
    if self.instance then
        self.instance:close()
    end
    local scale = getTextManager():MeasureStringY(UIFont.Small, "XXX") / 12
    local w = 600 * scale
    local h = 400 * scale
    local o = WLP_PortalConfig:new(getCore():getScreenWidth()/2-w/2,getCore():getScreenHeight()/2-h/2, w, h)
    o.scale = scale
    setmetatable(o, self)
    self.__index = self
    o:initialise(portalGroupId)
    o:addToUIManager()
    self.instance = o
    return o
end

function WLP_PortalConfig:initialise(portalGroupId)
    ISCollapsableWindow.initialise(self)
    self.page = 0
    self.moveWithMouse = true
    self:setResizable(false)
    self.portalGroupId = portalGroupId

    local p = 5 * self.scale

    local win = GravyUI.Node(self.width, self.height, self):pad(p, 20*self.scale, p, 10*self.scale)
    local head, body, footer = win:rows({0.2, 0.75, 0.05}, p)

    local head1, head2, head3 = head:cols({0.35, 0.35, 0.3}, p)
    local groupName, soundName, fadeTime = head1:rows(4, p)
    local itemType, itemCount, takeItem, itemName = head2:rows(4, p)
    local homingRange, homingRangeZ, requiresPower = head3:rows(4, p)

    local groupNameLabel, groupNameInput = groupName:cols({0.3, 0.7})
    local soundNameLabel, soundNameInput = soundName:cols({0.3, 0.7})
    local fadeTimeLabel, fadeTimeInput = fadeTime:cols({0.3, 0.7})

    local itemTypeLabel, itemTypeInput = itemType:cols({0.3, 0.7})
    local itemCountLabel, itemCountInput = itemCount:cols({0.3, 0.7})
    local takeItemLabel, takeItemInput = takeItem:cols({0.3, 0.7})
    local itemNameLabel, itemNameInput = itemName:cols({0.3, 0.7})

    local homingRangeLabel, homingRangeInput = homingRange:cols({0.6, 0.4})
    local homingRangeZLabel, homingRangeZInput = homingRangeZ:cols({0.6, 0.4})
    local requiresPowerLabel, requiresPowerInput = requiresPower:cols({0.6, 0.4})

    groupNameLabel:makeLabel("Name:", UIFont.Small)
    soundNameLabel:makeLabel("Sound:", UIFont.Small)
    fadeTimeLabel:makeLabel("Fade Time:", UIFont.Small)

    itemTypeLabel:makeLabel("Item Type:", UIFont.Small)
    itemCountLabel:makeLabel("Item Count:", UIFont.Small)
    takeItemLabel:makeLabel("Take Item:", UIFont.Small)
    itemNameLabel:makeLabel("Item Name:", UIFont.Small)

    homingRangeLabel:makeLabel("Homing Range:", UIFont.Small)
    homingRangeZLabel:makeLabel("Homing Z Range:", UIFont.Small)
    requiresPowerLabel:makeLabel("Requires Power:", UIFont.Small)

    self.groupNameInput = groupNameInput:makeTextBox("")
    self.soundNameInput = soundNameInput:makeComboBox()
    self.fadeTimeInput = fadeTimeInput:makeTextBox("350", true)

    self.itemTypeInput = itemTypeInput:makeTextBox("")
    self.itemCountInput = itemCountInput:makeTextBox("1", true)
    self.takeItemInput = takeItemInput:makeTickBox()
    self.itemNameInput = itemNameInput:makeTextBox("")

    self.homingRangeInput = homingRangeInput:makeTextBox("5", true)
    self.homingRangeZInput = homingRangeZInput:makeTextBox("0", true)
    self.requiresPowerInput = requiresPowerInput:makeTickBox()

    local blocks = {body:grid(5, {0.2, 0.35, 0.15, 0.3}, p*2, p*2)}
    self.blocks = {}
    for i=1,5 do
        local block = blocks[i]
        local bNameLabel, bNameInput = block[1]:rows(2, p)
        local bItemType, bItemCount, bTakeItem, bItemName = block[4]:rows(4, p)
        local bItemTypeLabel, bItemTypeInput = bItemType:cols({0.5, 0.5})
        local bItemCountLabel, bItemCountInput = bItemCount:cols({0.5, 0.5})
        local bTakeItemLabel, bTakeItemInput = bTakeItem:cols({0.5, 0.5})
        local bItemNameLabel, bItemNameInput = bItemName:cols({0.5, 0.5})
        self.blocks[i] = {
            name = bNameInput:makeTextBox(""),
            area = block[2]:makeAreaPicker(),
            point = block[3]:makePointPicker(),
            itemType = bItemTypeInput:makeTextBox(""),
            itemCount = bItemCountInput:makeTextBox("1", true),
            takeItem = bTakeItemInput:makeTickBox(),
            itemName = bItemNameInput:makeTextBox(""),
        }
        bNameLabel:makeLabel("Name:", UIFont.Small)
        bItemTypeLabel:makeLabel("Item Type:", UIFont.Small)
        bItemCountLabel:makeLabel("Item Count:", UIFont.Small)
        bTakeItemLabel:makeLabel("Take Item:", UIFont.Small)
        bItemNameLabel:makeLabel("Item Name:", UIFont.Small)
        self.blocks[i].takeItem:addOption("")
        self.blocks[i].area.showAlways = false
        self.blocks[i].point.showAlways = false
    end

    local fLeft, fCenter, fRight = footer:cols({0.3, 0.4, 0.3}, p)

    self.prevPageBtn = fLeft:makeButton("Prev", self, self.onPageChange, {-1})
    self.nextPageBtn = fRight:makeButton("Next", self, self.onPageChange, {1})
    self.saveBtn = fCenter:makeButton("Save", self, self.onSave)

    self.soundNameInput:addOption("None")
    self.soundNameInput:addOption("ElevatorDoors")
    self.soundNameInput:addOption("WoodenStairs")
    self.soundNameInput:addOption("RocksFalling")
    self.soundNameInput:addOption("BusDriving")
    self.soundNameInput:addOption("ForestWalking")
    self.soundNameInput:addOption("MetalLadderClimb")

    self.takeItemInput:addOption("")
    self.requiresPowerInput:addOption("")

    self:load()
end

function WLP_PortalConfig:reset()
    self.groupNameInput:setText("")
    self.soundNameInput:select("None")
    self.fadeTimeInput:setText("350")
    self.itemTypeInput:setText("")
    self.itemCountInput:setText("1")
    self.takeItemInput:setSelected(1, false)
    self.itemNameInput:setText("")
    self.homingRangeInput:setText("5")
    self.homingRangeZInput:setText("0")
    self.requiresPowerInput:setSelected(1, false)
    self:resetBlocks()
end

function WLP_PortalConfig:resetBlocks()
    for i=1,5 do
        local block = self.blocks[i]
        block.name:setText("")
        block.area:setValue({x1=0,y1=0,z1=0,x2=0,y2=0,z2=0})
        block.point:setValue({x=0,y=0,z=0})
        block.itemType:setText("")
        block.itemCount:setText("1")
        block.takeItem:setSelected(1, false)
        block.itemName:setText("")
    end
end

function WLP_PortalConfig:load()
    self:reset()
    for _, portalData in ipairs(WLP_PortalRegistry.configData.Groups) do
        if portalData.id == self.portalGroupId then
            self.portalData = portalData
            break
        end
    end

    if not self.portalData then
        self.portalData = {
            id = getRandomUUID(),
            name = "",
            soundName = "None",
            fadeoutTime = 350,
            homingRange = 5,
            homingZRange = 0,
            requiresPower = false,
            requiredItem = nil,
            takeRequiredItem = false,
            requiredItemCount = 1,
            requiredItemName = nil,
            portals = {},
        }
    end

    self.groupNameInput:setText(self.portalData.name or "")
    self.soundNameInput:select(self.portalData.soundName or "None")
    self.fadeTimeInput:setText(tostring(self.portalData.fadeoutTime or 350))
    self.itemTypeInput:setText(self.portalData.requiredItem or "")
    self.itemCountInput:setText(tostring(self.portalData.requiredItemCount or 1))
    self.takeItemInput:setSelected(1, self.portalData.takeRequiredItem or false)
    self.itemNameInput:setText(self.portalData.requiredItemName or "")
    self.homingRangeInput:setText(tostring(self.portalData.homingRange or 5))
    self.homingRangeZInput:setText(tostring(self.portalData.homingZRange or 0))
    self.requiresPowerInput:setSelected(1, self.portalData.requiresPower or false)

    self.page = 0
    self:setBlocks()
end

function WLP_PortalConfig:setBlocks()
    local s = self.page*5
    for i=1,5 do
        local block = self.blocks[i]
        local portalData = self.portalData.portals[i + s]
        print("loading: " .. i .. " " .. i+s)
        if portalData then
            block.name:setText(portalData.name or "")
            block.area:setValue({
                x1 = portalData.areaNW[1],
                y1 = portalData.areaNW[2],
                z1 = portalData.areaNW[3],
                x2 = portalData.areaSE[1],
                y2 = portalData.areaSE[2],
                z2 = portalData.areaSE[3],
            })
            block.point:setValue({
                x = portalData.target[1],
                y = portalData.target[2],
                z = portalData.target[3],
            })
            block.itemType:setText(portalData.requiredItem or "")
            block.itemCount:setText(tostring(portalData.requiredItemCount or 1))
            block.takeItem:setSelected(1, portalData.takeRequiredItem or false)
            block.itemName:setText(portalData.requiredItemName or "")
        end
    end
    self:updatePager()
end

function WLP_PortalConfig:onPageChange(_, dir)
    self:readBlocks()
    self:resetBlocks()
    self.page = self.page + dir
    if self.page <= 0 then
        self.page = 0
    end
    local total = math.ceil(#self.portalData.portals / 5)
    if self.page >= total then
        self.page = total
    end
    self:setBlocks()
end

function WLP_PortalConfig:updatePager()
    if self.page == 0 then
        self.prevPageBtn:setEnable(false)
    else
        self.prevPageBtn:setEnable(true)
    end
    local total = math.ceil(#self.portalData.portals / 5)
    if self.page >= total then
        self.nextPageBtn:setEnable(false)
    else
        self.nextPageBtn:setEnable(true)
    end
end

function WLP_PortalConfig:readHeader()
    self.portalData.name = self.groupNameInput:getText()
    self.portalData.soundName = self.soundNameInput:getSelectedText() == "None" and nil or self.soundNameInput:getSelectedText()
    self.portalData.fadeoutTime = tonumber(self.fadeTimeInput:getText())
    self.portalData.homingRange = tonumber(self.homingRangeInput:getText())
    self.portalData.homingZRange = tonumber(self.homingRangeZInput:getText())
    self.portalData.requiresPower = self.requiresPowerInput:isSelected(1)
    self.portalData.requiredItem = getTextOrNil(self.itemTypeInput:getText())
    self.portalData.requiredItemCount = tonumber(self.itemCountInput:getText())
    self.portalData.takeRequiredItem = self.takeItemInput:isSelected(1)
    self.portalData.requiredItemName = getTextOrNil(self.itemNameInput:getText())
end

function WLP_PortalConfig:readBlocks()
    for i=1,5 do
        local j = (self.page)*5 + i
        local block = self.blocks[i]
        if not self.portalData.portals[j] then
            self.portalData.portals[j] = {}
        end
        self.portalData.portals[j].name = getTextOrNil(block.name:getText())
        local area = block.area:getValue()
        self.portalData.portals[j].areaNW = {area.x1, area.y1, area.z1}
        self.portalData.portals[j].areaSE = {area.x2, area.y2, area.z2}
        local point = block.point:getValue()
        self.portalData.portals[j].target = {point.x, point.y, point.z}
        self.portalData.portals[j].requiredItem = getTextOrNil(block.itemType:getText())
        self.portalData.portals[j].requiredItemCount = tonumber(block.itemCount:getText())
        self.portalData.portals[j].takeRequiredItem = block.takeItem:isSelected(1)
        self.portalData.portals[j].requiredItemName = getTextOrNil(block.itemName:getText())
    end
    local toRemoveIndexes = {}
    for i,data in ipairs(self.portalData.portals) do
        if not data.name then
            table.insert(toRemoveIndexes, i)
        end
    end
    for i=#toRemoveIndexes,1,-1 do
        table.remove(self.portalData.portals, toRemoveIndexes[i])
    end
    self:setBlocks()
end

function WLP_PortalConfig:onSave()
    self:readHeader()
    self:readBlocks()
    sendClientCommand(getPlayer(), "WastelandPortals", "UpdatePortalGroup", self.portalData)
    self:close()
end

function WLP_PortalConfig:close()
    self:removeFromUIManager()
    WLP_PortalConfig.instance = nil
end