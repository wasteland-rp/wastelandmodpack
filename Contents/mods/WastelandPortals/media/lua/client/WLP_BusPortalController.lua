---
--- WLP_BusPortalController.lua
--- 19/10/2023
---

require "WLP_PortalController"

WLP_BusPortalController = WLP_PortalController:derive("WLP_BusPortalController")

function WLP_BusPortalController:new()
    local o = WLP_BusPortalController.parentClass.new(self)
    return o
end

function WLP_BusPortalController:_getWeight(player)
    local weight = 0
    for i = 0, player:getInventory():getItems():size() - 1 do
        local item = player:getInventory():getItems():get(i)
        if (not player:isEquipped(item) and not player:isEquippedClothing(item)) or player:isHandItem(item) then
            weight = weight + item:getUnequippedWeight()
        elseif instanceof(item, "InventoryContainer") then
            weight = weight + item:getContentsWeight()
        end
    end
    return weight
end

function WLP_BusPortalController:_calcCost(player, portal)
    local cost = 0

    if portal.busBaseCost then
        cost = cost + portal.busBaseCost
    end

    if portal.busCostPerWeight then
        local weight = self:_getWeight(player)
        if portal.busFreeWeight then
            weight = math.max(weight - portal.busFreeWeight, 0)
        end
        if portal.busWeightIncrement then
            weight = math.ceil(weight / portal.busWeightIncrement)
        end
        cost = cost + weight * portal.busCostPerWeight
    end

    return cost
end

function WLP_BusPortalController:generateTooltip(player, portal)
    local tooltip = self.parentClass.generateTooltip(self, player, portal)
    local cost = self:_calcCost(player, portal)
    local name = getItemNameFromFullType(portal.busCurrency)
    if cost > 0 then
        tooltip = tooltip .. " <LINE><RGB:0.9,0.8,0.1>Cost of ride: " .. cost .. " " .. name .. " <LINE>"
    else
        tooltip = tooltip .. " <LINE><RGB:0.9,0.8,0.1>Cost of ride: Free  <LINE>"
    end
    tooltip = tooltip .. " <LINE><RGB:0.8,0.8,0.8>Summary of Costs"
    if portal.busBaseCost then
        tooltip = tooltip .. " <LINE>Ticket cost: " .. portal.busBaseCost .. " " .. name
    else
        tooltip = tooltip .. " <LINE>Ticket cost: Free"
    end
    if portal.busCostPerWeight then
        local extraCost = cost - (portal.busBaseCost or 0)
        if extraCost > 0 then
            tooltip = tooltip .. " <LINE><RGB:0.7,0.2,0.2>Weight fee: " .. extraCost .. " " .. name
        end

        tooltip = tooltip .. " <LINE><LINE><RGB:0.8,0.8,0.8>Your weight: " .. math.ceil(self:_getWeight(player))
        if portal.busFreeWeight then
            tooltip = tooltip .. " (" .. portal.busFreeWeight .. " free)"
        end

        local inc = portal.busWeightIncrement or 1
        tooltip = tooltip .. " <LINE>Cost per " .. inc .. " weight: " .. portal.busCostPerWeight .. " " .. name
    end
    return tooltip
end

function WLP_BusPortalController:canEnter(player, portal)
    local p = self.parentClass.canEnter(self, player, portal)
    if not p then return false end
    if player:isGodMod() then return true end
    local cost = self:_calcCost(player, portal)
    if player:getInventory():getItemCountFromTypeRecurse(portal.busCurrency) < cost then
        return false
    end
    return true
end

function WLP_BusPortalController:getTooltipSprite(portal)
    return nil
end

function WLP_BusPortalController:playerTeleported(player, portal)
    local fromLocation = portal.center.x .. "," .. portal.center.y .. "," .. portal.center.z
    local toLocation = portal.target.x .. "," .. portal.target.y .. "," .. portal.target.z
    local cost = "free"
    local calcCost = self:_calcCost(player, portal)
    if calcCost then
        local name = getItemNameFromFullType(portal.busCurrency)
        cost = calcCost .. " " .. name
        for i = 1, calcCost do
            local item = player:getInventory():getFirstTypeRecurse(portal.busCurrency)
            if item then
                item:getContainer():Remove(item)
            end
        end
    end
    if portal.safetime then
        WL_Utils.makePlayerSafe(portal.safetime)
    end
    sendClientCommand(player, "WastelandPortals", "Log", {fromLocation, toLocation, cost})
end