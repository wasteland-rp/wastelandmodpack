require("WLP_PortalRegistry")

Events.OnInitGlobalModData.Add(function ()
	ModData.request("WLP_Portals")
end)

Events.OnReceiveGlobalModData.Add(function (key, portalData)
    WLP_PortalRegistry.OnReceiveGlobalModData(key, portalData)
end)