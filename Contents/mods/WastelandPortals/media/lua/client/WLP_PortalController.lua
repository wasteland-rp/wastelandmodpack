---
--- WLP_PortalController.lua
--- 19/10/2023
---

require "WLBaseObject"
require "WL_Utils"

WLP_PortalController = WLBaseObject:derive("WLP_PortalController")

function WLP_PortalController:new()
	local o = WLP_PortalController.parentClass.new(self)
	return o
end

function WLP_PortalController:getMenuName(player, portal)
	if portal.groupName then
		return portal.groupName
	end
	return nil
end

function WLP_PortalController:getEnterText(portal)
	if portal.name then
		return portal.name
	end
	return "Enter"
end

function WLP_PortalController:generateTooltip(player, portal)
	local tooltip = "Travel"

	if portal.name then
		if portal.tooltipAction then
			tooltip = portal.tooltipAction .. " to " .. portal.name
		else
			tooltip = tooltip .. " to " .. portal.name
		end
	end

	tooltip = tooltip .. self:getRequiredItemString(portal)
	if portal.requiresPower then
		tooltip = tooltip .. " <LINE><RGB:1,1,1> Requires:" .. WL_Utils.MagicSpace .. "<RGB:1,0.85,0.05> Power"
	end
	return tooltip
end

function WLP_PortalController:getRequiredItemString(portal)
	local requiredItemStr = ""
	if self:requiresItem(portal) then
		local name = portal.requiredItemName or getItemNameFromFullType(portal.requiredItem)
		requiredItemStr = " <LINE><RGB:1,1,1> Requires:" .. WL_Utils.MagicSpace ..
				"<RGB:1,0.85,0.05> " .. portal.requiredItemCount .. " " .. name
		if portal.takeRequiredItem then
			requiredItemStr = requiredItemStr .. " (consumed)"
		end
	end
	return requiredItemStr
end

function WLP_PortalController:canEnter(player, portal)
	if player:isGodMod() then return true end

	if portal.requiresPower then
		local square = getCell():getGridSquare(portal.center.x, portal.center.y, portal.center.z)
		if not square then
			return false
		end
		if not square:haveElectricity() then
			return false
		end
	end
	if self:requiresItem(portal) then
		if portal.requiredItem == "Base.GoldCurrency" then
			if WIT_Gold.amountOnPlayer(player) < portal.requiredItemCount then
				return false
			end
		else
			if player:getInventory():getItemCountFromTypeRecurse(portal.requiredItem) < portal.requiredItemCount then
				return false
			end
		end
		if portal.requiredItemName then
			local item = player:getInventory():getFirstTypeRecurse(portal.requiredItem)
			if not item then
				return false
			end
			if item:getName() ~= portal.requiredItemName then
				return false
			end
		end
	end
	return true
end

function WLP_PortalController:getTooltipSprite(portal)
	return nil
end

--- This is for override in derived tables
function WLP_PortalController:requiresItem(portal)
	return portal.requiredItem ~= nil and portal.requiredItem ~= ""
end

function WLP_PortalController:playerTeleported(player, portal)
	local fromLocation = portal.center.x .. "," .. portal.center.y .. "," .. portal.center.z
	local toLocation = portal.target.x .. "," .. portal.target.y .. "," .. portal.target.z
	local cost = "free"
	if self:requiresItem(portal) and portal.takeRequiredItem then
		cost = portal.requiredItemCount .. " " .. getItemNameFromFullType(portal.requiredItem)
		if portal.requiredItem == "Base.GoldCurrency" then
			WIT_Gold.removeAmountFromPlayer(player, portal.requiredItemCount)
		else
			for i = 1, portal.requiredItemCount do
				local item = player:getInventory():getFirstTypeRecurse(portal.requiredItem)
				if item then
					item:getContainer():Remove(item)
				end
			end
		end
	end
	if portal.safetime then
		WL_Utils.makePlayerSafe(portal.safetime)
	end
	sendClientCommand(player, "WastelandPortals", "Log", {fromLocation, toLocation, cost})
end