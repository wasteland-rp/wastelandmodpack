---
--- WLP_PortalRegistry.lua
--- 22/10/2023
---

require "WLP_Portal"

WLP_PortalRegistry = WLP_PortalRegistry or {}
WLP_Portals = WLP_Portals or {}

function WLP_PortalRegistry.createPortal(areaNW, areaSE, target, portalName)
	local newPortal = WLP_Portal:new(areaNW[1], areaNW[2], areaNW[3], areaSE[1], areaSE[2], areaSE[3])
	newPortal:setTeleportTo(target[1], target[2], target[3])
	if portalName then
		newPortal:setName(portalName)
	end
	table.insert(WLP_Portals, newPortal)
	return newPortal
end

function WLP_PortalRegistry.createPortalGroup(portalListConfig, groupName)
	local portals = {}
	for i, iEntry in ipairs(portalListConfig) do
		for j, jEntry in ipairs(portalListConfig) do
			if i ~= j then
				local portal = WLP_PortalRegistry.createPortal(iEntry[1], iEntry[2], jEntry[3], jEntry[4])
				if groupName then
					portal:setGroupName(groupName)
				end
				table.insert(portals, portal)
			end
		end
	end
	return portals
end

function WLP_PortalRegistry.getPortalsAt(x, y, z)
	local zones = {}
	for _, zone in pairs(WLP_Portals) do
		if zone:isInZone(x, y, z) then
			table.insert(zones, zone)
		end
	end
	return zones
end

function WLP_PortalRegistry.getPortalById(id)
	for _, portal in pairs(WLP_Portals) do
		if portal.modDataId == id then
			return portal
		end
	end
	return nil
end

function WLP_PortalRegistry.removePortalById(id)
	for i, portal in pairs(WLP_Portals) do
		if portal.modDataId == id then
			portal:remove()
			table.remove(WLP_Portals, i)
			return
		end
	end
end

local function applyStandardData(portal, portalData)
	if portalData.type == "bus" then
		portal:setPortalController(WLP_BusPortalController:new())
	end
	if portalData.requiresPower then
		portal:setRequiresPower()
	end
	if portalData.soundName then
		portal:setSound(portalData.soundName)
	end
	if portalData.fadeoutTime then
		portal:setFadeoutTime(portalData.fadeoutTime)
	end
	if portalData.homingRange then
		portal:setHomingRange(portalData.homingRange, portalData.homingZRange)
	end
	if portalData.requiredItem then
		portal:setRequiredItem(portalData.requiredItem, portalData.takeRequiredItem, portalData.requiredItemCount, portalData.requiredItemName)
	end
	if portalData.tooltipAction then
		portal:setTooltipAction(portalData.tooltipAction)
	end
	if portalData.disabled then
		portal.disabled = true
		portal.homingArrow.enabled = false
	end
	if portalData.safetime then
		portal:setSafetime(portalData.safetime)
	end

end

local function applyBusData(portal, portalData)
	if portalData.busCurrency then
		portal.busCurrency = portalData.busCurrency
	end
	if portalData.busBaseCost then
		portal.busBaseCost = portalData.busBaseCost
	end
	if portalData.busFreeWeight then
		portal.busFreeWeight = portalData.busFreeWeight
	end
	if portalData.busWeightIncrement then
		portal.busWeightIncrement = portalData.busWeightIncrement
	end
	if portalData.busCostPerWeight then
		portal.busCostPerWeight = portalData.busCostPerWeight
	end
	if portalData.busFreePass then
		portal.busFreePass = portalData.busFreePass
	end
	if portalData.busFreePassName then
		portal.busFreePassName = portalData.busFreePassName
	end
	if portalData.busFreePassConsumed then
		portal.busFreePassConsumed = portalData.busFreePassConsumed
	end
end

-- TODO: Break into smaller functions
function WLP_PortalRegistry.OnReceiveGlobalModData(key, portalData)
	if key ~= "WLP_Portals" then return end

	if not portalData then
		portalData = {}
	end
	if not portalData.Groups then
		portalData.Groups = {}
	end

	WLP_PortalRegistry.configData = portalData

	local seenPortalIds = {}

	for _, portalGroupData in ipairs(portalData.Groups) do
		-- remove all portal that start with this group id
		local toRemove = {}
		for _, portal in pairs(WLP_Portals) do
			if portal.groupModDataId and portal.groupModDataId == portalGroupData.id then
				table.insert(toRemove, portal.modDataId)
			end
		end
		for _, id in ipairs(toRemove) do
			WLP_PortalRegistry.removePortalById(id)
		end
		-- create new portals
		for _, lPortalData in ipairs(portalGroupData.portals) do
			for _, lPortalData2 in ipairs(portalGroupData.portals) do
				if lPortalData.name ~= lPortalData2.name then
					local portal = WLP_PortalRegistry.createPortal(lPortalData.areaNW, lPortalData.areaSE, lPortalData2.target, lPortalData2.name)
					portal.groupModDataId = portalGroupData.id
					portal.isOwn = true
					portal:setGroupName(portalGroupData.name)
					portal:setModDataId(portalGroupData.id .. "_" .. lPortalData.name .. "_" .. lPortalData2.name)
					applyStandardData(portal, portalGroupData)
					applyStandardData(portal, lPortalData2)
					seenPortalIds[portal.modDataId] = true
				end
			end
		end
	end
	-- remove all portals that were not in the data
	local toRemove = {}
	for _, portal in pairs(WLP_Portals) do
		if portal.modDataId and not seenPortalIds[portal.modDataId] and portal.isOwn then
			table.insert(toRemove, portal.modDataId)
		end
	end
	for _, id in ipairs(toRemove) do
		WLP_PortalRegistry.removePortalById(id)
	end
end