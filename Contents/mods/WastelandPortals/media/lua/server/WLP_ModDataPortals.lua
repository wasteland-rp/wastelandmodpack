if isClient() then return end

WLP_ModDataPortals = {}

function WLP_ModDataPortals.OnInitGlobalModData()
    WLP_ModDataPortals.StoredPortalData = ModData.getOrCreate("WLP_Portals")
    if not WLP_ModDataPortals.StoredPortalData then
        WLP_ModDataPortals.StoredPortalData = {}
    end
    if not WLP_ModDataPortals.StoredPortalData.Singles then
        WLP_ModDataPortals.StoredPortalData.Singles = {}
    end
    if not WLP_ModDataPortals.StoredPortalData.Groups then
        WLP_ModDataPortals.StoredPortalData.Groups = {}
    end
    if not WLP_ModDataPortals.StoredPortalData.DisabledSingles then
        WLP_ModDataPortals.StoredPortalData.DisabledSingles = {}
    end
    if not WLP_ModDataPortals.StoredPortalData.DisabledGroups then
        WLP_ModDataPortals.StoredPortalData.DisabledGroups = {}
    end
end

function WLP_ModDataPortals.Save()
    ModData.add("WLP_Portals", WLP_ModDataPortals.StoredPortalData)
    ModData.transmit("WLP_Portals")
end

function WLP_ModDataPortals.OnClientCommand(module, command, player, args)
    if module == "WastelandPortals" then
        if command == "EnablePortal" then
            writeLog("PortalUse", string.format("%s enabled portal %s", player:getUsername(), args[1]))
            WLP_ModDataPortals.StoredPortalData.DisabledSingles[args[1]] = nil
            for _, portal in ipairs(WLP_ModDataPortals.StoredPortalData.Singles) do
                if portal.id == args[1] then
                    portal.disabled = false
                end
            end
            WLP_ModDataPortals.Save()
        elseif command == "DisablePortal" then
            writeLog("PortalUse", string.format("%s disabled portal %s", player:getUsername(), args[1]))
            WLP_ModDataPortals.StoredPortalData.DisabledSingles[args[1]] = true
            for _, portal in ipairs(WLP_ModDataPortals.StoredPortalData.Singles) do
                if portal.id == args[1] then
                    portal.disabled = true
                end
            end
            WLP_ModDataPortals.Save()
        elseif command == "EnablePortalGroup" then
            writeLog("PortalUse", string.format("%s enabled portal group %s", player:getUsername(), args[1]))
            WLP_ModDataPortals.StoredPortalData.DisabledGroups[args[1]] = nil
            for _, portalGroup in ipairs(WLP_ModDataPortals.StoredPortalData.Groups) do
                if portalGroup.id == args[1] then
                    portalGroup.disabled = false
                end
            end
            WLP_ModDataPortals.Save()
        elseif command == "DisablePortalGroup" then
            writeLog("PortalUse", string.format("%s disabled portal group %s", player:getUsername(), args[1]))
            WLP_ModDataPortals.StoredPortalData.DisabledGroups[args[1]] = true
            for _, portalGroup in ipairs(WLP_ModDataPortals.StoredPortalData.Groups) do
                if portalGroup.id == args[1] then
                    portalGroup.disabled = true
                end
            end
            WLP_ModDataPortals.Save()
        elseif command == "UpdatePortalGroup" then
            writeLog("PortalUse", string.format("%s updated portal group %s", player:getUsername(), args.id))
            local groupData = args
            local portalGroup
            for _, pGroup in ipairs(WLP_ModDataPortals.StoredPortalData.Groups) do
                if pGroup.id == groupData.id then
                    portalGroup = pGroup
                    break
                end
            end
            if not portalGroup then
                portalGroup = {id = args.id}
                table.insert(WLP_ModDataPortals.StoredPortalData.Groups, portalGroup)
            end
            portalGroup.name = groupData.name
            portalGroup.soundName = groupData.soundName
            portalGroup.fadeoutTime = groupData.fadeoutTime
            portalGroup.homingRange = groupData.homingRange
            portalGroup.homingZRange = groupData.homingZRange
            portalGroup.requiresPower = groupData.requiresPower
            portalGroup.requiredItem = groupData.requiredItem
            portalGroup.requiredItemCount = groupData.requiredItemCount
            portalGroup.takeRequiredItem = groupData.takeRequiredItem
            portalGroup.requiredItemName = groupData.requiredItemName
            portalGroup.portals = groupData.portals

            WLP_ModDataPortals.Save()
        elseif command == "DeletePortalGroup" then
            writeLog("PortalUse", string.format("%s deleted portal group %s", player:getUsername(), args[1]))
            for i, portalGroup in ipairs(WLP_ModDataPortals.StoredPortalData.Groups) do
                if portalGroup.id == args[1] then
                    table.remove(WLP_ModDataPortals.StoredPortalData.Groups, i)
                    break
                end
            end
            WLP_ModDataPortals.Save()
        elseif command == "Log" then
            writeLog("PortalUse", string.format("%s used portal %s to %s for %s", player:getUsername(), args[1], args[2], args[3]))
        end
    end
end