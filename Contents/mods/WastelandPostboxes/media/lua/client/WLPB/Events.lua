Events.OnPreFillWorldObjectContextMenu.Add(function(playerIdx, context, worldobjects, test)
    if test then return end
    WLPB.WorldContext.doMenu(getSpecificPlayer(playerIdx), context, worldobjects)
end)

Events.OnPreFillInventoryObjectContextMenu.Add(function(playerIdx, context, items, test)
    if test then return end
    items = ISInventoryPane.getActualItems(items)
    WLPB.InventoryContext.doMenu(getSpecificPlayer(playerIdx), context, items)
end)