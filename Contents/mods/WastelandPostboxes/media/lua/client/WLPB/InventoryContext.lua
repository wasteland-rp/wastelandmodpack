WLPB = WLPB or {}
WLPB.InventoryContext = {}

function WLPB.InventoryContext.doMenu(player, context, items)
    for _, item in pairs(items) do
        if player:getInventory():contains(item) then
            local stamps = player:getInventory():getCountTypeRecurse("WLPB.Stamp")
            if item:getFullType() == "WLPB.Letter" then
                local s = context:addOption("Seal & Stamp [Standard]", player, WLPB.InventoryContext.startStampLetter, item, "Standard")
                s.toolTip = ISInventoryPaneContextMenu.addToolTip()
                s.toolTip:setName("Seal & Stamp [Standard]")
                s.toolTip.description = "Costs 1 " .. getItemNameFromFullType("WLPB.Stamp") .. ".\n\nDelivered in 24 hours."
                if stamps < 1 then
                    s.notAvailable = true
                    s.toolTip.description = "<RGB:1,0,0> " .. s.toolTip.description
                end

                local e = context:addOption("Seal & Stamp [Express]", player, WLPB.InventoryContext.startStampLetter, item, "Express")
                e.toolTip = ISInventoryPaneContextMenu.addToolTip()
                e.toolTip:setName("Seal & Stamp [Express]")
                e.toolTip.description = "Costs 5 " .. getItemNameFromFullType("WLPB.Stamp") .. ".\n\nDelivered in 8 hours."
                if stamps < 5 then
                    e.notAvailable = true
                    e.toolTip.description = "<RGB:1,0,0> " .. e.toolTip.description
                end

                local c = context:addOption("Seal & Stamp [Courier]", player, WLPB.InventoryContext.startStampLetter, item, "Courier")
                c.toolTip = ISInventoryPaneContextMenu.addToolTip()
                c.toolTip:setName("Seal & Stamp [Courier]")
                c.toolTip.description = "Costs 20 " .. getItemNameFromFullType("WLPB.Stamp") .. ".\n\nDelivered in 1 hour."
                if stamps < 20 then
                    c.notAvailable = true
                    c.toolTip.description = "<RGB:1,0,0> " .. c.toolTip.description
                end

                if WL_Utils.isStaff(player) then
                    local i = context:addOption("Stamp & Stamp [Instant]", player, WLPB.InventoryContext.startStampLetter, item, "Instant")
                    i.toolTip = ISInventoryPaneContextMenu.addToolTip()
                    i.toolTip:setName("Stamp & Stamp [Instant]")
                    i.toolTip.description = "For Staff use only."
                end
            end
        end
    end
end

function WLPB.InventoryContext.startStampLetter(player, item, type)
    local allAddresses = WastelandPostboxes:getAllAddresses()
    table.sort(allAddresses)

    local width = 200
    local height = 80
    local x = getPlayerScreenLeft(player:getPlayerNum()) + (getPlayerScreenWidth(player:getPlayerNum()) - width) / 2
    local y = getPlayerScreenTop(player:getPlayerNum()) + (getPlayerScreenHeight(player:getPlayerNum()) - height) / 2
    local modal = ISPanel:new(x, y, width, height)
    modal:initialise()
    modal.backgroundColor = {r=0, g=0, b=0, a=0.8}
    modal.borderColor = {r=0, g=0, b=0, a=0}
    modal.combo = ISComboBox:new(10, 10, 180, 25)
    modal.combo:initialise()
    for _, address in pairs(allAddresses) do
        local mailbox = WastelandPostboxes.publicData.mailboxes[address]
        if mailbox then
            local town = WWP_Town.findTownAt(mailbox.x, mailbox.y, mailbox.z)
            if town then
                modal.combo:addOptionWithData("[" .. town.name .. "] " .. address, address)
            else
                modal.combo:addOptionWithData(address, address)
            end
        end
    end
    modal:addChild(modal.combo)
    modal.ok = ISButton:new(10, 45, 80, 25, "Stamp", nil, function()
        local stamps = player:getInventory():getAllTypeRecurse("WLPB.Stamp")
        if type == "Standard" then
            if stamps:size() < 1 then
                player:Say("I need a stamp to send this letter.")
                return
            end
            stamps:get(0):getContainer():Remove(stamps:get(0))
        elseif type == "Express" then
            if stamps:size() < 5 then
                player:Say("I need 5 stamps to send this letter express.")
                return
            end
            for i=1,5 do
                stamps:get(i-1):getContainer():Remove(stamps:get(i-1))
            end
        elseif type == "Courier" then
            if stamps:size() < 20 then
                player:Say("I need 20 stamps to send this letter by courier.")
                return
            end
            for i=1,20 do
                stamps:get(i-1):getContainer():Remove(stamps:get(i-1))
            end
        end
        local stamppedLetter = WastelandPostboxes:stampLetter(item, type, modal.combo:getOptionData(modal.combo.selected))
        player:getInventory():Remove(item)
        player:getInventory():AddItem(stamppedLetter)
        modal:setVisible(false)
        modal:removeFromUIManager()
    end)
    modal.ok:initialise()
    modal:addChild(modal.ok)
    modal.cancel = ISButton:new(110, 45, 80, 25, "Cancel", nil, function()
        modal:setVisible(false)
        modal:removeFromUIManager()
    end)
    modal.cancel:initialise()
    modal:addChild(modal.cancel)
    modal:addToUIManager()
end