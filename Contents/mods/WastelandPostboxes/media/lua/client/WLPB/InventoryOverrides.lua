require "WLL"

WLPB_PostboxLockSystem = {}
table.insert(WLL.Systems, WLPB_PostboxLockSystem)

local function getMailbox(container)
    local obj = container:getParent()
    if not obj then return nil end
    if not WastelandPostboxes:isMailbox(obj) then return nil end
    local x, y, z = obj:getX(), obj:getY(), obj:getZ()
    local mailbox = WastelandPostboxes:getRegisteredMailboxAddress(x, y, z)
    if not mailbox then return nil end
    return mailbox
end

function WLPB_PostboxLockSystem.IsLocked(container)
    local mailbox = getMailbox(container)
    if not mailbox then return false end
    return true
end

function WLPB_PostboxLockSystem.CanView(player, container)
    local mailbox = getMailbox(container)
    if not mailbox then return true end
    return mailbox.allowedViewers[player:getUsername()]
end

function WLPB_PostboxLockSystem.CanTake(player, container)
    local mailbox = getMailbox(container)
    if not mailbox then return true end
    return mailbox.allowedViewers[player:getUsername()]
end

function WLPB_PostboxLockSystem.CanPut(player, container)
    local mailbox = getMailbox(container)
    if not mailbox then return true end
    return mailbox.allowedViewers[player:getUsername()]
end

function WLPB_PostboxLockSystem.GetLockedTitle(container)
    return nil
end

function WLPB_PostboxLockSystem.GetLockedDescription(container)
    return nil
end

function WLPB_PostboxLockSystem.OnContainerContext()
    return nil
end
WLPB = WLPB or {}
if not WLPB.ISToolTipInv_render then
    WLPB.ISToolTipInv_render = ISToolTipInv.render
end
function ISToolTipInv:render()
    WLPB.ISToolTipInv_render(self)
    if not self.item or not self.tooltip then return end
    local text = {}
    if self.item:getFullType() == "WLPB.LetterStamped" then
        table.insert(text, {0, "To:"})
        table.insert(text, {10, self.item:getModData().WLPB_toAddress or "Unknown"})
        table.insert(text, {0, "Speed:"})
        table.insert(text, {10, self.item:getModData().WLPB_speed or "Unknown"})
    elseif self.item:getFullType() == "WLPB.LetterDelivered" or self.item:getFullType() == "WLPB.LetterOpened" then
        table.insert(text, {0, "From:"})
        table.insert(text, {10, self.item:getModData().WLPB_fromAddress or "Unknown"})
        table.insert(text, {0, "To:"})
        table.insert(text, {10, self.item:getModData().WLPB_toAddress or "Unknown"})
    end
    if #text == 0 then return end
    local lineHeight = getTextManager():MeasureStringY(self.tooltip:getFont(), text[1][2])+2
    local height = lineHeight * #text
    self:drawRect(0, self.height, self.width, height + 10, self.backgroundColor.a, self.backgroundColor.r, self.backgroundColor.g, self.backgroundColor.b);
    self:drawRectBorder(0, self.height, self.width, height + 10, self.borderColor.a, self.borderColor.r, self.borderColor.g, self.borderColor.b);
    for i=1,#text do
        self.tooltip:DrawText(self.tooltip:getFont(), text[i][2], text[i][1], self.height + 5 + (i-1)*lineHeight, 1, 1, 1, 1)
    end
    self.tooltip:setHeight(self.tooltip:getHeight() + height + 10)
    self:setHeight(self.height + height + 10)
end
