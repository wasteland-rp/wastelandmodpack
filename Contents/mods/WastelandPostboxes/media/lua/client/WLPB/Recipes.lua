WLPB = WLPB or {}
WLPB.Recipes = WLPB.Recipes or {}

function WLPB.Recipe_MakeLetter(items, result)
    for i=0,items:size()-1 do
        local item = items:get(i)
        if item:getFullType() == "Base.SheetPaper2" then
            if item:isCustomName() then
                result:setCustomName(true)
                result:setName(item:getName())
            end
            for j=0,item:getCustomPages():size() - 1 do
                result:addPage(j+1, item:seePage(j+1))
            end
            return
        end
    end
end

function WLPB.Recipe_OpenLetter(items, result)
    for i=0,items:size()-1 do
        local item = items:get(i)
        if item:getFullType() == "WLPB.LetterDelivered" then
            print("WLPB.LetterDelivered!")
            WastelandPostboxes:populateOpenedLetterFromDelivered(item, result)
        end
    end
end