WLPB = WLPB or {}
WLPB.WorldContext = WLPB.WorldContext or {
    toolTipsAvailable = {},
    toolTipUsed = {},
}

function WLPB.WorldContext.getTooltip()
    local pool = WLPB.WorldContext.toolTipsAvailable
    if #pool == 0 then
        table.insert(pool, ISToolTip:new())
    end
    local tooltip = table.remove(pool, #pool)
    tooltip:reset()
    table.insert(WLPB.WorldContext.toolTipUsed, tooltip)
    return tooltip
end

function WLPB.WorldContext.resetTooltips()
    for _,tooltip in ipairs(WLPB.WorldContext.toolTipUsed) do
        table.insert(WLPB.WorldContext.toolTipsAvailable, tooltip)
    end
    table.wipe(WLPB.WorldContext.toolTipUsed)
end

function WLPB.WorldContext.doMenu(player, context, worldobjects)
    WLPB.WorldContext.resetTooltips()
    for _, object in ipairs(worldobjects) do
        if WastelandPostboxes:isMailbox(object) then
            local x, y, z = object:getX(), object:getY(), object:getZ()
            local mailbox = WastelandPostboxes:getRegisteredMailboxAddress(x, y, z)
            if mailbox then
                WLPB.WorldContext.addRegisteredMailboxOptions(player, context, mailbox)
            else
                WLPB.WorldContext.addUnregisteredMailboxOptions(player, context, object)
            end
            return
        end
    end
end

function WLPB.WorldContext.addRegisteredMailboxOptions(player, context, mailbox)
    local distance = player:DistToSquared(mailbox.x, mailbox.y)
    if distance > 3 then
        local tooltip = WLPB.WorldContext.getTooltip()
        tooltip:setName("Mailbox: " .. mailbox.address)
        tooltip.description = "You are too far away from this mailbox to interact."
        local i = context:addOption("Mailbox: " .. mailbox.address)
        i.notAvailable = true
        i.toolTip = tooltip
        return
    end
    local mailboxContext = WL_ContextMenuUtils.getOrCreateSubMenu(context, "Mailbox: " .. mailbox.address)

    local lettersToSend = player:getInventory():getCountTypeRecurse("WLPB.LetterStamped")
    local o = mailboxContext:addOption("Send Letters", player, WLPB.WorldContext.sendLetters, mailbox)
    o.toolTip = WLPB.WorldContext.getTooltip()
    o.toolTip:setName("Send Letters")
    o.toolTip.description = "Send all letters in your inventory from this mailbox.\n\nYou have " .. lettersToSend .. " letters to send."
    if lettersToSend == 0 then
        o.notAvailable = true
        o.toolTip.description = "<RGB:1,0,0> You do not have any letters on you."
    elseif not mailbox.allowedViewers[player:getUsername()] then
        o.notAvailable = true
        o.toolTip.description = "<RGB:1,0,0> You are not authorized to use this mailbox."
    end

    if mailbox.owner == player:getUsername() or WL_Utils.canModerate(player) then
        local players = getOnlinePlayers()
        local playersUsernames = {}
        for i=0,players:size()-1 do
            if not mailbox.allowedViewers[players:get(i):getUsername()] then
                table.insert(playersUsernames, players:get(i):getUsername())
            end
        end
        table.sort(playersUsernames)
        if #playersUsernames > 0 then
            local addViewersContext = WL_ContextMenuUtils.getOrCreateSubMenu(mailboxContext, "Add Authorized Person")
            for _, username in pairs(playersUsernames) do
                addViewersContext:addOption(username, WastelandPostboxes, WastelandPostboxes.addAllowedViewer, player, mailbox.address, username)
            end
        end

        local viewersUsernames = {}
        for username, _ in pairs(mailbox.allowedViewers) do
            if username ~= mailbox.owner then
                table.insert(viewersUsernames, username)
            end
        end
        table.sort(viewersUsernames)
        if #viewersUsernames > 0 then
            local removeViewersContext = WL_ContextMenuUtils.getOrCreateSubMenu(mailboxContext, "Remove Authorized Person")
            for _, username in pairs(viewersUsernames) do
                removeViewersContext:addOption(username, WastelandPostboxes, WastelandPostboxes.removeAllowedViewer, player, mailbox.address, username)
            end
        end

        local otherViewers = {}
        for username, _ in pairs(mailbox.allowedViewers) do
            if username ~= player:getUsername() then
                table.insert(otherViewers, username)
            end
        end
        table.sort(otherViewers)
        if #otherViewers > 0 then
            local changeOwnerContext = WL_ContextMenuUtils.getOrCreateSubMenu(mailboxContext, "Change Owner")
            for _, username in pairs(otherViewers) do
                changeOwnerContext:addOption(username, WastelandPostboxes, WastelandPostboxes.changeMailboxOwner, player, mailbox.address, username)
            end
        end

        local rename = mailboxContext:addOption("Rename Mailbox", player, WLPB.WorldContext.startRenameMailbox, mailbox)
        rename.toolTip = WLPB.WorldContext.getTooltip()
        rename.toolTip:setName("Rename Mailbox")
        rename.toolTip.description = "Change the name of this mailbox.\nCosts 10 " .. getItemNameFromFullType("Base.GoldCurrency") .. "."
        if WIT_Gold.amountOnPlayer(player) then
            rename.notAvailable = true
            rename.toolTip.description = "<RGB:1,0,0> " .. rename.toolTip.description
        end

        mailboxContext:addOption("Unregister Mailbox", player, WLPB.WorldContext.confirmUnregisterMailbox, mailbox)
    end
end

function WLPB.WorldContext.confirmUnregisterMailbox(player, mailbox)
    local width = 300
    local height = 55
    local x = getPlayerScreenLeft(player:getPlayerNum()) + (getPlayerScreenWidth(player:getPlayerNum()) - width) / 2
    local y = getPlayerScreenTop(player:getPlayerNum()) + (getPlayerScreenHeight(player:getPlayerNum()) - height) / 2
    local modal = ISPanel:new(x, y, width, height)
    modal:initialise()
    modal.backgroundColor = {r=0, g=0, b=0, a=0.8}
    modal.borderColor = {r=0, g=0, b=0, a=0}
    modal.ok = ISButton:new(60, 25, 80, 20, "Unregister", nil, function()
        WastelandPostboxes:unregisterMailbox(player, mailbox.address)
        modal:setVisible(false)
        modal:removeFromUIManager()
        player:Say("Mailbox unregistered.")
    end)
    modal.ok:initialise()
    modal:addChild(modal.ok)
    modal.cancel = ISButton:new(160, 25, 80, 20, "Cancel", nil, function()
        modal:setVisible(false)
        modal:removeFromUIManager()
    end)
    modal.cancel:initialise()

    modal.prerender = function(s)
        ISPanel.prerender(s)
        s:drawTextCentre("Unregister " .. mailbox.address .. "?", s.width / 2, 5, 1, 1, 1, 1, UIFont.Small)
    end

    modal:addChild(modal.cancel)
    modal:addToUIManager()
end

function WLPB.WorldContext.sendLetters(player, mailbox)
    local lettersToSend = player:getInventory():getAllTypeRecurse("WLPB.LetterStamped")
    for i=0,lettersToSend:size()-1 do
        local letter = lettersToSend:get(i)
        local letterData = WastelandPostboxes:createDataFromLetter(letter, mailbox.address)
        local time = 1440 -- 1 day
        if letter:getModData().WLPB_speed == "Express" then
            time = 480 -- 8 hours
        elseif letter:getModData().WLPB_speed == "Courier" then
            time = 60 -- 1 hour
        elseif letter:getModData().WLPB_speed == "Instant" then
            time = 0
        end
        WastelandPostboxes:queueDelivery(player, letterData, time)
        letter:getContainer():Remove(letter)
    end
    player:Say(lettersToSend:size() .. " letters sent.")
end

function WLPB.WorldContext.addUnregisteredMailboxOptions(player, context, object)
    local money = WIT_Gold.amountOnPlayer(player)
    local registerContext = context:addOption("Register Mailbox", player, WLPB.WorldContext.startRegisterMailbox, object)
    registerContext.toolTip = WLPB.WorldContext.getTooltip()
    registerContext.toolTip:setName("Register Mailbox")
    registerContext.toolTip.description = "You need 50 " .. getItemNameFromFullType("Base.GoldCurrency") .. " to register a mailbox."
    if money < 50 then
        registerContext.notAvailable = true
        registerContext.toolTip.description = "<RGB:1,0,0> " .. registerContext.toolTip.description
    end
end

function WLPB.WorldContext.startRegisterMailbox(player, object)
    local width = 200
    local height = 110
    local x = getPlayerScreenLeft(player:getPlayerNum()) + (getPlayerScreenWidth(player:getPlayerNum()) - width) / 2
    local y = getPlayerScreenTop(player:getPlayerNum()) + (getPlayerScreenHeight(player:getPlayerNum()) - height) / 2
    local modal = ISPanel:new(x, y, width, height)
    modal:initialise()
    modal.backgroundColor = {r=0, g=0, b=0, a=0.8}
    modal.borderColor = {r=0, g=0, b=0, a=0}
    modal.textEntry = ISTextEntryBox:new("", 10, 35, 180, 25)
    modal.textEntry:initialise()
    modal:addChild(modal.textEntry)
    modal.ok = ISButton:new(10, 75, 80, 25, "Register", nil, function()
        local address = modal.textEntry:getText()
        if not address or address == "" then
            player:Say("You need to enter an address.")
            return
        end
        if WastelandPostboxes.publicData.mailboxes[address] then
            player:Say("This address is already in use.")
            return
        end
        if string.len(address) > 30 then
            player:Say("The address is too long.")
            return
        end
        if string.len(address) < 10 then
            player:Say("The address is too short.")
            return
        end
        if not WIT_Gold.removeAmountFromPlayer(player, 50) then
            player:Say("You need 50 " .. getItemNameFromFullType("Base.GoldCurrency") .. " to register a mailbox.")
            return
        end
        local x, y, z = object:getX(), object:getY(), object:getZ()
        WastelandPostboxes:registerMailbox(player, x, y, z, address)
        player:Say("Mailbox registered.")
        modal:setVisible(false)
        modal:removeFromUIManager()
    end)
    modal.ok:initialise()
    modal:addChild(modal.ok)
    modal.cancel = ISButton:new(110, 75, 80, 25, "Cancel", nil, function()
        modal:setVisible(false)
        modal:removeFromUIManager()
    end)
    modal.cancel:initialise()

    modal.prerender = function(s)
        ISPanel.prerender(s)
        s:drawTextCentre("Enter Mailbox Name", s.width / 2, 10, 1, 1, 1, 1, UIFont.Small)
    end

    modal:addChild(modal.cancel)
    modal:addToUIManager()
end

function WLPB.WorldContext.startRenameMailbox(player, mailbox)
    local width = 200
    local height = 110
    local x = getPlayerScreenLeft(player:getPlayerNum()) + (getPlayerScreenWidth(player:getPlayerNum()) - width) / 2
    local y = getPlayerScreenTop(player:getPlayerNum()) + (getPlayerScreenHeight(player:getPlayerNum()) - height) / 2
    local modal = ISPanel:new(x, y, width, height)
    modal:initialise()
    modal.backgroundColor = {r=0, g=0, b=0, a=0.8}
    modal.borderColor = {r=0, g=0, b=0, a=0}
    modal.textEntry = ISTextEntryBox:new(mailbox.address, 10, 35, 180, 25)
    modal.textEntry:initialise()
    modal:addChild(modal.textEntry)
    modal.ok = ISButton:new(10, 75, 80, 25, "Rename", nil, function()
        local address = modal.textEntry:getText()
        if not address or address == "" then
            player:Say("You need to enter an address.")
            return
        end
        if WastelandPostboxes.publicData.mailboxes[address] then
            player:Say("This address is already in use.")
            return
        end
        if string.len(address) > 30 then
            player:Say("The address is too long.")
            return
        end
        if string.len(address) < 10 then
            player:Say("The address is too short.")
            return
        end
        if not WIT_Gold.removeAmountFromPlayer(player, 10) then
            player:Say("You need 10 " .. getItemNameFromFullType("Base.GoldCurrency") .. " to rename a mailbox.")
            return
        end
        WastelandPostboxes:renameMailbox(player, mailbox.address, address)
        modal:setVisible(false)
        modal:removeFromUIManager()
        player:Say("Mailbox renamed.")
    end)
    modal.ok:initialise()
    modal:addChild(modal.ok)
    modal.cancel = ISButton:new(110, 75, 80, 25, "Cancel", nil, function()
        modal:setVisible(false)
        modal:removeFromUIManager()
    end)
    modal.cancel:initialise()

    modal.prerender = function(s)
        ISPanel.prerender(s)
        s:drawTextCentre("Enter Mailbox Name", s.width / 2, 10, 1, 1, 1, 1, UIFont.Small)
    end

    modal:addChild(modal.cancel)
    modal:addToUIManager()
end

-- local street_names = {
--     "Oak", "Maple", "Pine", "Cedar", "Elm", "Walnut", "Birch", "Chestnut", "Spruce",
--     "Ash", "Willow", "Poplar", "Hickory", "Sycamore", "Cherry", "Dogwood", "Magnolia",
--     "Redwood", "Fir", "Beech", "Cottonwood", "Palm", "Cypress", "Juniper", "Alder",
--     "Locust", "Ironwood", "Sequoia", "Aspen", "Bay", "Holly", "Laurel", "Linden",
--     "Myrtle", "Olive", "Peach", "Pear", "Plum", "Apple", "Orange", "Lemon", "Lime",
--     "Grapevine", "Blueberry", "Blackberry", "Raspberry", "Strawberry", "Cranberry", "Mulberry"
-- }
-- local postfixes = {"Street", "Avenue", "Road", "Drive", "Lane", "Boulevard", "Way", "Court", "Place", "Circle"}

-- function WLPB.WorldContext.RandomStreetName()
--     local name = street_names[ZombRand(#street_names) + 1]
--     local postfix = postfixes[ZombRand(#postfixes) + 1]
--     return name .. " " .. postfix
-- end
