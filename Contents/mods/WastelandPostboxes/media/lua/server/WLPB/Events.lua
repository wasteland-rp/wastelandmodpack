if not isClient() then
    Events.EveryTenMinutes.Add(function()
        WastelandPostboxes:checkDeliveries()
    end)

    Events.EveryHours.Add(function()
        WastelandPostboxes:checkOwners()
    end)
end