---@class WastelandPostboxes : WL_ClientServerBase
WastelandPostboxes = WastelandPostboxes or WL_ClientServerBase:new("WastelandPostboxes")
WastelandPostboxes.needsPrivateData = true
WastelandPostboxes.needsPublicData = true

function WastelandPostboxes:onModDataInit()
    if not self.publicData.mailboxes then self.publicData.mailboxes = {} end
    if not self.privateData.pendingDeliveries then self.privateData.pendingDeliveries = {} end
end

--== Mailboxes ==--

function WastelandPostboxes:getRegisteredMailboxAddress(x, y, z)
    if not self.publicData or not self.publicData.mailboxes then return end
    for _, mailbox in pairs(self.publicData.mailboxes) do
        if mailbox.x == x and mailbox.y == y and mailbox.z == z then
            return mailbox
        end
    end
end

function WastelandPostboxes:getAllAddresses()
    local addresses = {}
    for address, _ in pairs(self.publicData.mailboxes) do
        table.insert(addresses, address)
    end
    return addresses
end

function WastelandPostboxes:registerMailbox(player, x, y, z, address)
    if isClient() then
        self:sendToServer(player, "registerMailbox", x, y, z, address)
        return
    end
    local mailbox = {
        owner = player:getUsername(),
        allowedViewers = {[player:getUsername()] = true},
        x = x,
        y = y,
        z = z,
        address = address,
        lastSeenOwner = getTimestamp(),
    }
    self.publicData.mailboxes[address] = mailbox
    self:savePublicData()
    self:logInfo(player:getUsername() .. " registered mailbox " .. address .. " at " .. x .. "," .. y .. "," .. z)
end

function WastelandPostboxes:unregisterMailbox(player, address)
    if isClient() then
        if player then
            self:sendToServer(player, "unregisterMailbox", address)
        end
        return
    end
    self.privateData.pendingDeliveries[address] = nil
    self.publicData.mailboxes[address] = nil
    self:savePublicData()
    self:savePrivateData()
    self:logInfo((player and player:getUsername() or "system") .. " unregistered mailbox " .. address)
end

function WastelandPostboxes:addAllowedViewer(player, address, username)
    if isClient() then
        self:sendToServer(player, "addAllowedViewer", address, username)
        return
    end
    local mailbox = self.publicData.mailboxes[address]
    if not mailbox then return end
    mailbox.allowedViewers[username] = true
    self:savePublicData()
    self:logInfo(player:getUsername() .. " added " .. username .. " as an allowed viewer for mailbox " .. address)
end

function WastelandPostboxes:removeAllowedViewer(player, address, username)
    if isClient() then
        self:sendToServer(player, "removeAllowedViewer", address, username)
        return
    end
    local mailbox = self.publicData.mailboxes[address]
    if not mailbox then return end
    mailbox.allowedViewers[username] = nil
    self:savePublicData()
    self:logInfo(player:getUsername() .. " removed " .. username .. " as an allowed viewer for mailbox " .. address)
end

function WastelandPostboxes:changeMailboxOwner(player, address, username)
    if isClient() then
        self:sendToServer(player, "changeMailboxOwner", address, username)
        return
    end
    local mailbox = self.publicData.mailboxes[address]
    if not mailbox then return end
    mailbox.allowedViewers[mailbox.owner] = nil
    mailbox.owner = username
    mailbox.allowedViewers[username] = true
    self:savePublicData()
    self:logInfo((player:getUsername() or "system") .. " changed the owner of mailbox " .. address .. " to " .. username)
end

function WastelandPostboxes:renameMailbox(player, address, newAddress)
    if isClient() then
        self:sendToServer(player, "renameMailbox", address, newAddress)
        return
    end
    local mailbox = self.publicData.mailboxes[address]
    if not mailbox then return end
    mailbox.address = newAddress
    self.publicData.mailboxes[newAddress] = mailbox
    self.publicData.mailboxes[address] = nil
    self.privateData.pendingDeliveries[newAddress] = self.privateData.pendingDeliveries[address]
    self.privateData.pendingDeliveries[address] = nil
    self:savePublicData()
    self:logInfo(player:getUsername() .. " renamed mailbox " .. address .. " to " .. newAddress)
end

--== Deliveries ==--

function WastelandPostboxes:queueDelivery(player, letterData, deliveryMinutes)
    if isClient() then
        self:sendToServer(player, "queueDelivery", letterData, deliveryMinutes)
        return
    end
    if not letterData.to then
        self:logError(player:getUsername() .. " tried to send a letter without a recipient")
        return
    end
    if not self.publicData.mailboxes[letterData.to] then
        self:logError(player:getUsername() .. " tried to send a letter to an unknown address: " .. letterData.to)
        return
    end
    if not self.privateData.pendingDeliveries[letterData.to] then self.privateData.pendingDeliveries[letterData.to] = {} end
    table.insert(self.privateData.pendingDeliveries[letterData.to], {
        letter = letterData,
        time = deliveryMinutes,
    })
    self:savePrivateData()
    self:logInfo(player:getUsername() .. " queued a delivery to " .. letterData.to .. " from " .. letterData.from .. " in " .. deliveryMinutes .. " minutes")

    if deliveryMinutes == 0 then
        self:checkDeliveries()
    end
end

--- Called from Events.lua every in-game 10 minutes
function WastelandPostboxes:checkDeliveries()
    if isClient() then return end
    local anyDelivered = false
    for address, deliveries in pairs(self.privateData.pendingDeliveries) do
        if #deliveries > 0 then
            local object = self:getMailboxObject(address)
            if object then
                local addressDelivered = false
                local mailboxInventory = object:getContainer()
                for i=#deliveries,1,-1 do
                    local delivery = deliveries[i]
                    delivery.time = delivery.time - 10
                    if delivery.time <= 0 then
                        self:logInfo("Delivering a letter to " .. address)
                        local letter = self:createLetterDeliveredFromData(delivery.letter)
                        mailboxInventory:addItem(letter)
                        table.remove(deliveries, i)
                        anyDelivered = true
                        addressDelivered = true
                    end
                end
                if addressDelivered then
                    sendItemsInContainer(object, mailboxInventory)
                    self:dingMailbox(nil, address)
                end
            end
        end
    end
    if anyDelivered then
        self:savePrivateData()
    end
end

function WastelandPostboxes:dingMailbox(player, address)
    if isServer() then
        self:logInfo("Sending ding mailbox " .. address .. " to all clients")
        self:sendToAllClients("dingMailbox", address)
        return
    end
    local mailbox = self.publicData.mailboxes[address]
    if not mailbox then return end
    local square = getCell():getGridSquare(mailbox.x, mailbox.y, mailbox.z)
    if not square then return end
	getSoundManager():PlayWorldSoundImpl("WLPB_MailboxDing", false, square:getX(), square:getY(), square:getZ(), 1, 50, 1, false)
end

--== Utilities ==--

-- Called from Events.lua every in-game hour to check for mailboxes that have been abandoned
WastelandPostboxes.ownerTimeout = 60*60*24*14 -- 14 days
function WastelandPostboxes:checkOwners()
    if isClient() then return end
    local timestamp = getTimestamp()
    local onlinePlayers = getOnlinePlayers()
    local usernameLookup = {}
    for i=0,onlinePlayers:size()-1 do
        usernameLookup[onlinePlayers:get(i):getUsername()] = true
    end
    local didChange = false
    for address, mailbox in pairs(self.publicData.mailboxes) do
        if usernameLookup[mailbox.owner] then
            mailbox.lastSeenOwner = timestamp
        elseif mailbox.lastSeenOwner + WastelandPostboxes.ownerTimeout < timestamp then
            self:logInfo("Mailbox " .. address .. " for " .. mailbox.owner .. " has been abandoned")
            local newOwner = nil
            for username, _ in pairs(mailbox.allowedViewers) do
                if username ~= mailbox.owner then
                    newOwner = username
                    break
                end
            end
            if newOwner then
                mailbox.allowedViewers[mailbox.owner] = nil
                mailbox.owner = newOwner
                mailbox.lastSeenOwner = timestamp
                self:logInfo("Mailbox " .. address .. " has been reassigned to " .. newOwner)
            else
                self:unregisterMailbox(nil, address)
                self:logInfo("Mailbox " .. address .. " has been unregistered due to abandonment")
            end
            didChange = true
        end
    end
    self:savePublicData(didChange)
end

function WastelandPostboxes:stampLetter(letter, speed, toAddress)
    local stampedLetter = InventoryItemFactory.CreateItem("WLPB.LetterStamped")
    stampedLetter:getModData().WLPB_toAddress = toAddress
    stampedLetter:getModData().WLPB_name = letter:getName()
    stampedLetter:getModData().WLPB_speed = speed
    local pages = {}
    for i=0,letter:getCustomPages():size() - 1 do
        pages[i+1] = letter:seePage(i+1);
    end
    stampedLetter:getModData().WLPB_pages = pages
    return stampedLetter
end

function WastelandPostboxes:createDataFromLetter(letter, fromAddress)
    local data = {}
    data.name = letter:getModData().WLPB_name
    data.to = letter:getModData().WLPB_toAddress
    data.from = fromAddress
    data.pages = letter:getModData().WLPB_pages
    return data
end

function WastelandPostboxes:createLetterDeliveredFromData(letterData)
    local letter = InventoryItemFactory.CreateItem("WLPB.LetterDelivered")
    letter:getModData().WLPB_toAddress = letterData.to
    letter:getModData().WLPB_fromAddress = letterData.from
    letter:getModData().WLPB_name = letterData.name
    letter:getModData().WLPB_pages = letterData.pages
    return letter
end

function WastelandPostboxes:populateOpenedLetterFromDelivered(deliveredLetter, openedLetter)
    openedLetter:setName(deliveredLetter:getModData().WLPB_name)
    openedLetter:setCustomName(true)
    for i,page in ipairs(deliveredLetter:getModData().WLPB_pages or {}) do
        openedLetter:addPage(i, page)
    end
    openedLetter:setLockedBy("admin")
    openedLetter:getModData().WLPB_toAddress = deliveredLetter:getModData().WLPB_toAddress
    openedLetter:getModData().WLPB_fromAddress = deliveredLetter:getModData().WLPB_fromAddress
    return openedLetter
end

function WastelandPostboxes:getMailboxObject(address)
    local mailbox = self.publicData.mailboxes[address]
    if not mailbox then return end
    local x, y, z = mailbox.x, mailbox.y, mailbox.z
    local square = getCell():getGridSquare(x, y, z)
    if not square then return end
    local objects = square:getObjects()
    for i=0,objects:size()-1 do
        local object = objects:get(i)
        if self:isMailbox(object) then
            return object
        end
    end
end

WastelandPostboxes.KnownMailboxes = {
    ["street_decoration_01_18"] = true,
    ["street_decoration_01_19"] = true,
    ["street_decoration_01_20"] = true,
    ["street_decoration_01_21"] = true,
    ["Vault_8_28"] = true,
    ["street_decoration_01_8"] = true,
    ["street_decoration_01_9"] = true,
    ["street_decoration_01_10"] = true,
    ["street_decoration_01_11"] = true,
    ["pert_Christmas_01_44"] = true,
    ["pert_Christmas_01_45"] = true,
    ["pert_Christmas_01_46"] = true,
    ["pert_Christmas_01_47"] = true,
}
function WastelandPostboxes:isMailbox(object)
    if not object then return false end
    if not object:getSprite() then return false end
    return self.KnownMailboxes[object:getSprite():getName()]
end