if WP_AfraidOfHeights then
    Events.OnPlayerUpdate.Remove(WP_AfraidOfHeights.CheckPlayer)
else
    WP_AfraidOfHeights = {}
    WP_AfraidOfHeights.DEBUG = false
end

local noPhrases = {
    "No Way!",
    "Absolutely not!",
    "You've got to be kidding me!",
    "There's no way I'm doing that!",
    "I'll just stay right here, thanks.",
    "Do I really have to climb that?",
    "Is there another option?",
    "My feet are staying on the ground!",
    "I can't do heights, sorry!",
    "Ain't a chance in hell!",
}

local gt = getGameTime()

local climbUpState = ClimbSheetRopeState.instance()
local climbDownState = ClimbDownSheetRopeState.instance()
local climbThroughWindowState = ClimbThroughWindowState.instance()
local idleState = IdleState.instance()

local directions = {
    IsoDirections.N,
    IsoDirections.NE,
    IsoDirections.E,
    IsoDirections.SE,
    IsoDirections.S,
    IsoDirections.SW,
    IsoDirections.W,
    IsoDirections.NW,
}

function WP_AfraidOfHeights.CheckPlayer(player)
    if not player:HasTrait("AfraidOfHeights") then return end

    local state = player:getCurrentState()
    if state == climbUpState or state == climbDownState then
        player:Say(noPhrases[ZombRand(#noPhrases) + 1])
        player:changeState(idleState)
    end
    local z = player:getZ()

    if state == climbThroughWindowState and z > 0 then
        player:Say(noPhrases[ZombRand(#noPhrases) + 1])
        player:changeState(idleState)
        climbThroughWindowState:exit(player)
        player:setVariable("ClimbWindowFinished", true)
    end

    if z > 0 then
        local panic = 20 + 5 * z
        local unhappinessMultiplier = 0.001 * z

        -- if on stairs
        if player:getZ() - math.floor(player:getZ()) > 0 then
            panic = math.min(100, panic + 50)
            unhappinessMultiplier = unhappinessMultiplier + 0.03
        end

        -- if next to a drop
        local square = player:getSquare()
        if square and square:isOutside() then
            for _,d in ipairs(directions) do
                local tile = square:getTileInDirection(d)
                if not tile or not tile:isSolidFloor() then
                    panic = 100
                    unhappinessMultiplier = unhappinessMultiplier + 0.06
                    break
                end
            end
        end

        if player:getStats():getPanic() < panic then
            player:getStats():setPanic(panic)
        end
        local unhappiness = math.min(1, unhappinessMultiplier * gt:getMultiplier())
        player:getBodyDamage():setUnhappynessLevel(player:getBodyDamage():getUnhappynessLevel() + unhappiness)
    end
end

Events.OnPlayerUpdate.Add(WP_AfraidOfHeights.CheckPlayer)
