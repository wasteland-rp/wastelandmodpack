if WP_ChronicPain then
    -- hot reload
    Events.OnPlayerUpdate.Remove(WP_ChronicPain.CheckPlayer)
else
    WP_ChronicPain = {}
    WP_ChronicPain.DEBUG = false
end

function WP_ChronicPain.Log(message)
    print("WP_ChronicPain: " .. message)
end

function WP_ChronicPain.CheckPlayer(player)
    local player = getPlayer()
    if not player or not player:HasTrait("ChronicPain") then
        if WP_ChronicPain.DEBUG then WP_ChronicPain.Log("Player does not have ChronicPain trait") end
        return
    end

    local modData = player:getModData()
    if not modData.WP_ChronicPain then
        if WP_ChronicPain.DEBUG then WP_ChronicPain.Log("Initializing") end
        modData.WP_ChronicPain = 10000
    end

    if modData.WP_ChronicPain <= 0 then
        WP_ChronicPain.ApplyRandomPain(player)
        modData.WP_ChronicPain = ZombRand(10000)

        WP_ChronicPain.Log("Next pain in " .. modData.WP_ChronicPain)
    else
        modData.WP_ChronicPain = modData.WP_ChronicPain - (1 * getGameTime():getMultiplier())
    end
    if WP_ChronicPain.DEBUG then WP_ChronicPain.Log("Updates to next pain: " .. modData.WP_ChronicPain) end
end

local function weightedRandom(min, max)
    -- Generate a random number between 0 and 1
    local baseRand = ZombRandFloat(0, 1)
    local skewedRand = baseRand * baseRand * baseRand  -- Squaring creates a bias towards 0. Cubing would bias even more.
    -- Scale this biased number into the desired range of 10 to 100
    return math.floor(min + (max - min) * skewedRand)
end

function WP_ChronicPain.ApplyRandomPain(player)
    local bodyPartType = BodyPartType.getRandom()
    local additionalPain = weightedRandom(10, 100)
    local bodyPart = player:getBodyDamage():getBodyPart(bodyPartType)
    local currentAdditionalPain = bodyPart:getAdditionalPain()
    bodyPart:setAdditionalPain(currentAdditionalPain + additionalPain)
    WP_ChronicPain.Log(additionalPain .. " pain to " .. BodyPartType.ToString(bodyPartType) .. " (total: " .. currentAdditionalPain + additionalPain .. ")")
end

Events.OnPlayerUpdate.Add(WP_ChronicPain.CheckPlayer)
