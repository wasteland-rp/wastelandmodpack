---
--- FastReaderBuff.lua
--- 12/11/2024
--- 

local originalISReadABook_adjustMaxTime = ISReadABook.adjustMaxTime

function ISReadABook:adjustMaxTime(maxTime)
    maxTime = originalISReadABook_adjustMaxTime(self, maxTime)
    local skillTrained = self.item:getSkillTrained()
    if skillTrained and maxTime > 350 then
        if self.character:HasTrait("FastReader") and SkillBook[skillTrained] then
            maxTime = 350
        end
    end
    return maxTime
end

