if WP_FocusHandler then
    Events.EveryOneMinute.Remove(WP_FocusHandler.CheckPlayer)
else
    WP_FocusHandler = {}
end

function WP_FocusHandler.CheckPlayer()
    local player = getPlayer()
    if not player then return end

    if player:HasTrait("HyperFocused") and WL_Utils.getActionBoostType(player, "HyperFocused") == 0 then
        WL_Utils.setActionBoostType(player, "HyperFocused", 0.1)
    elseif not player:HasTrait("HyperFocused") and WL_Utils.getActionBoostType(player, "HyperFocused") ~= 0 then
        WL_Utils.setActionBoostType(player, "HyperFocused", 0)
    end

    if player:HasTrait("Distracted") and WL_Utils.getActionBoostType(player, "Distracted") == 0 then
        WL_Utils.setActionBoostType(player, "Distracted", -0.1)
    elseif not player:HasTrait("Distracted") and WL_Utils.getActionBoostType(player, "Distracted") ~= 0 then
        WL_Utils.setActionBoostType(player, "Distracted", 0)
    end
end

Events.EveryOneMinute.Add(WP_FocusHandler.CheckPlayer)