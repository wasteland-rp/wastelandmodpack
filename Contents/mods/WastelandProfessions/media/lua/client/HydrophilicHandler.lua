if WP_Hydrophilic then
    Events.OnPlayerUpdate.Remove(WP_Hydrophilic.CheckPlayer)
else
    require "MF_ISMoodle"
    MF.createMoodle("Hydrophilic")

    WP_Hydrophilic = {}
    WP_Hydrophilic.DEBUG = false
end


local function isRoofed(square)
    local room = square:getRoom()
    if room then
        return true
    end
    for i=square:getZ()+1,8 do
        local zSquare = getCell():getGridSquare(square:getX(), square:getY(), i)
        if zSquare and zSquare:isSolidFloor() then
            return true
        end
    end
    return false
end

local function isWater(square)
    local objs = square:getObjects()
    for i=0,objs:size()-1 do
        local v = objs:get(i)
        if instanceof(v, "IsoObject") and v:getSprite() and v:getSprite():getProperties() and v:getSprite():getProperties():Is(IsoFlagType.water) then
            return true
        end
    end
    return false
end

local function log(message)
    print("HydrophilicHandler: " .. message)
end

function WP_Hydrophilic.CheckPlayer(player)
    if not player:HasTrait("Hydrophilic") then return end


    local gt = getGameTime()
    local cm = getClimateManager()
    local unhappiness = 0
    local boredom = 0
    local moveBoost = 0
    local bonuses = {}

    local playerSquare = player:getSquare()
    -- if raining
    if cm:isRaining() then
        if WP_Hydrophilic.DEBUG then log("It's raining") end
        table.insert(bonuses, "It's raining.")
        if isRoofed(playerSquare) then
            if WP_Hydrophilic.DEBUG then log("Player is under a roof") end
            boredom = 0.001
            unhappiness = 0.001
        else
            if WP_Hydrophilic.DEBUG then log("Player is not under a roof") end
            boredom = 0.005
            unhappiness = 0.005
            moveBoost = 0.5
            table.insert(bonuses, "Out in the rain.")
        end
    end

    -- if player is near water
    if playerSquare:getZ() == 0 then
        local didFind = false
        for x=playerSquare:getX()-3,playerSquare:getX()+3 do
            for y=playerSquare:getY()-3,playerSquare:getY()+3 do
                local square = getCell():getGridSquare(x, y, 0)
                if square and isWater(square) then
                    if WP_Hydrophilic.DEBUG then log("Player is near water") end
                    boredom = boredom + 0.005
                    unhappiness = unhappiness + 0.005
                    moveBoost = 0.5
                    didFind = true
                    table.insert(bonuses, "Near a body of water.")
                    break
                end
            end
            if didFind then break end
        end
    end

    if player:getBodyDamage():getWetness() > 15 then
        if WP_Hydrophilic.DEBUG then log("Player is wet " .. player:getBodyDamage():getWetness()) end
        unhappiness = unhappiness + player:getBodyDamage():getWetness() * 0.0001
        table.insert(bonuses, "I am wet.")
    end

    if boredom > 0 then
        player:getStats():setBoredom(math.max(0, player:getStats():getBoredom() - (boredom * gt:getMultiplier())))
        if WP_Hydrophilic.DEBUG then log("Reduced boredom by " .. (boredom * gt:getMultiplier())) end
    end
    if unhappiness > 0 then
        player:getBodyDamage():setUnhappynessLevel(math.max(0, player:getBodyDamage():getUnhappynessLevel() - (unhappiness * gt:getMultiplier())))
        if WP_Hydrophilic.DEBUG then log("Reduced unhappiness by " .. (unhappiness * gt:getMultiplier())) end
    end

    if WL_Utils.getMoveBoostType(player, "Hydrophilic") ~= moveBoost then
        WL_Utils.setMoveBoostType(player, "Hydrophilic", moveBoost)
        if moveBoost > 0 and WP_Hydrophilic.DEBUG then log("Set Move Boost to " .. moveBoost) end
    end

    WP_Hydrophilic.Moodle.level = #bonuses
    WP_Hydrophilic.Moodle.description = table.concat(bonuses, "\n")
    WP_Hydrophilic.Moodle:setValue(0.5)
    if WP_Hydrophilic.Moodle.pLevel ~= WP_Hydrophilic.Moodle.level then
        WP_Hydrophilic.Moodle.pLevel = WP_Hydrophilic.Moodle.level
        WP_Hydrophilic.Moodle.MoodleOscilationLevel = 1
    end
end

Events.OnPlayerUpdate.Add(WP_Hydrophilic.CheckPlayer)
Events.OnCreatePlayer.Add(function(playerIndex, player)
    WP_Hydrophilic.Moodle = MF.getMoodle("Hydrophilic", playerIndex)
    WP_Hydrophilic.Moodle.level = 0
    WP_Hydrophilic.Moodle.pLevel = 0
    WP_Hydrophilic.Moodle.description = "None"
    WP_Hydrophilic.Moodle.getLevel = function (s)
        return s.level
    end
    WP_Hydrophilic.Moodle.getGoodBadNeutral = function (s)
        if s.level > 0 then
            return 1
        end
        return 0
    end
    WP_Hydrophilic.Moodle.getDescription = function (s)
        return s.description
    end
    WP_Hydrophilic.Moodle.getTitle = function (s)
        return "Hydrophilic"
    end
end)