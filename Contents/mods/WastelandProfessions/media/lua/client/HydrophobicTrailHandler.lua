if WP_Hydrophobic then
    -- hot reload
    Events.OnPlayerUpdate.Remove(WP_Hydrophobic.CheckPlayer)
else
    WP_Hydrophobic = {}
    WP_Hydrophobic.DEBUG = false
end

local gt = getGameTime()

function WP_Hydrophobic.Log(message)
    print("WP_Hydrophobic: " .. message)
end

function WP_Hydrophobic.CheckPlayer(player)
    if not player or not player:HasTrait("Hydrophobic") then
        return
    end

    WP_Hydrophobic.CheckRain(player)
    WP_Hydrophobic.CheckWet(player)
end

function WP_Hydrophobic.CheckRain(player)
    local cm = getClimateManager()
    if not cm:isRaining() and not cm:isSnowing() then return end
    if WP_Hydrophobic.DEBUG then WP_Hydrophobic.Log("It's raining or snowing") end

    local increaseRate = 0.001 * gt:getMultiplier()
    player:getBodyDamage():setUnhappynessLevel(player:getBodyDamage():getUnhappynessLevel() + increaseRate)
    if WP_Hydrophobic.DEBUG then WP_Hydrophobic.Log("Unhappiness increase: " .. increaseRate) end
end

function WP_Hydrophobic.CheckWet(player)
    local wetness = player:getBodyDamage():getWetness()
    if wetness <= 15 then return end
    if WP_Hydrophobic.DEBUG then WP_Hydrophobic.Log("Player is wet") end
    player:getStats():setPanic(player:getBodyDamage():getWetness() * 2)
    if wetness <= 70 then return end
    player:getStats():setStress(100)
end

Events.OnPlayerUpdate.Add(WP_Hydrophobic.CheckPlayer)