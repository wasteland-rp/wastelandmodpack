require "PlayerReady"

WLP_SelectAltLangWindow = nil

WL_PlayerReady.Add(function ()
    if getPlayer():HasTrait("NonEnglishSpeaker") then
        local myLangs = WRC.Meta.GetKnownLanguages()
        local hasEnglish = false
        for i = 1, #myLangs do
            if myLangs[i] == "en" then
                hasEnglish = true
                break
            end
        end
        if hasEnglish and #myLangs == 1 then
            -- show prompt for new language
            local w = 300
            local h = 50
            local x = (getCore():getScreenWidth() - w) / 2
            local y = (getCore():getScreenHeight() - h) / 2
            WLP_SelectAltLangWindow = ISPanel:new(x, y, w, h)
            WLP_SelectAltLangWindow:initialise()
            WLP_SelectAltLangWindow.backgroundColor = {r=0, g=0, b=0, a=0.8}
            WLP_SelectAltLangWindow.borderColor = {r=1, g=1, b=1, a=0.2}
            WLP_SelectAltLangWindow.alwaysOnTop = true

            local win = GravyUI.Node(w, h, WLP_SelectAltLangWindow)
            local top, bot = win:rows(2)
            top:makeLabel("Choose a non-English language.", UIFont.Small, nil, "center")

            local bl, br = bot:cols({0.5, 0.5})
            local dd = bl:makeComboBox()
            br:makeButton("Select", nil, function()
                local lang = dd:getOptionData(dd.selected)
                if lang then
                    print("Selected language: " .. lang)
                    WRC.Meta.AddKnownLanguage(lang)
                    WRC.Meta.SetCurrentLanguage(lang)
                    WRC.Meta.RemoveKnownLanguage("en")
                    WLP_SelectAltLangWindow:removeFromUIManager()
                    WLP_SelectAltLangWindow = nil
                end
            end)

            local langs = {}
            for lang, data in pairs(WRC.Languages) do
                if lang ~= "en" then
                    table.insert(langs, {lang, data.name})
                end
            end
            table.sort(langs, function(a, b) return a[2] < b[2] end)
            for i = 1, #langs do
                dd:addOptionWithData(langs[i][2], langs[i][1])
            end

            WLP_SelectAltLangWindow:addToUIManager()
        elseif hasEnglish then
            WRC.Meta.RemoveKnownLanguage("en")
        end
    end
end)