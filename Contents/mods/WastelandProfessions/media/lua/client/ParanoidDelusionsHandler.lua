if WP_ParanoidDelusionsHandler then
    Events.OnPlayerUpdate.Remove(WP_ParanoidDelusionsHandler.CheckPlayer)
else
    WP_ParanoidDelusionsHandler = {}
    WP_ParanoidDelusionsHandler.DEBUG = false
    WP_ParanoidDelusionsHandler.Zombie = nil
    WP_ParanoidDelusionsHandler.ZombieTime = nil
    WP_ParanoidDelusionsHandler.ZombieLastX = nil
    WP_ParanoidDelusionsHandler.ZombieLastY = nil
    WP_ParanoidDelusionsHandler.ZombieLastZ = nil
    WP_ParanoidDelusionsHandler.CountSame = 0
    WP_ParanoidDelusionsHandler.NumToSpawn = 0
    WP_ParanoidDelusionsHandler.LastSecondChecked = getTimestamp()
end

function WP_ParanoidDelusionsHandler.spawnZombie(player)
    local x, y, z = player:getX(), player:getY(), player:getZ()
    local dir = ZombRand(360)
    local distance = 2 + ZombRand(8)
    local x2, y2 = math.floor(x + distance * math.cos(math.rad(dir))), math.floor(y + distance * math.sin(math.rad(dir)))
    local sq = getCell():getGridSquare(x2, y2, z)
    if sq and sq:isFree(false) then
        WP_ParanoidDelusionsHandler.Zombie = addZombiesInOutfit(x2, y2, z, 1, nil, 50):get(0)
        WP_ParanoidDelusionsHandler.Zombie:dressInRandomOutfit()
        WP_ParanoidDelusionsHandler.Zombie:pathToLocationF(x, y, z)
        WP_ParanoidDelusionsHandler.Zombie:setCanWalk(true)
        WP_ParanoidDelusionsHandler.Zombie:setHealth(500)
        WP_ParanoidDelusionsHandler.Zombie:DoZombieStats()
        WP_ParanoidDelusionsHandler.Zombie:makeInactive(false)
        WP_ParanoidDelusionsHandler.Zombie:setUseless(false)
        WP_ParanoidDelusionsHandler.Zombie:getModData().ParanoidDelusions = true
        WP_ParanoidDelusionsHandler.Zombie:Hit(nil,player,0,true,0)
        WP_ParanoidDelusionsHandler.ZombieLastX = 0
        WP_ParanoidDelusionsHandler.ZombieLastY = 0
        WP_ParanoidDelusionsHandler.ZombieLastZ = 0
        WP_ParanoidDelusionsHandler.CountSame = 0
        return true
    else
        return false
    end
end

function WP_ParanoidDelusionsHandler.CheckZombie(player)
    player:getStats():setPanic(100)
    local isOld = getTimestamp() - WP_ParanoidDelusionsHandler.ZombieTime > 30
    local isClose = player:DistTo(WP_ParanoidDelusionsHandler.Zombie) < 1.5
    if isOld or isClose then
        WP_ParanoidDelusionsHandler.Zombie:removeFromWorld()
        WP_ParanoidDelusionsHandler.Zombie = nil
        WP_ParanoidDelusionsHandler.ZombieTime = nil
        if WP_ParanoidDelusionsHandler.DEBUG then
            if isOld then print("ParanoidDelusionsHandler: Zombie removed because it was old") end
            if isClose then print("ParanoidDelusionsHandler: Zombie removed because it was close") end
        end
        return
    else
        local x, y, z = player:getX(), player:getY(), player:getZ()
        WP_ParanoidDelusionsHandler.Zombie:pathToLocationF(x, y, z)
        WP_ParanoidDelusionsHandler.Zombie:Hit(nil,player,0,true,0)
    end
    local zombieX, zombieY, zombieZ = WP_ParanoidDelusionsHandler.Zombie:getX(), WP_ParanoidDelusionsHandler.Zombie:getY(), WP_ParanoidDelusionsHandler.Zombie:getZ()
    if WP_ParanoidDelusionsHandler.ZombieLastX == zombieX and WP_ParanoidDelusionsHandler.ZombieLastY == zombieY and WP_ParanoidDelusionsHandler.ZombieLastZ == zombieZ then
        WP_ParanoidDelusionsHandler.CountSame = WP_ParanoidDelusionsHandler.CountSame + 1
        if WP_ParanoidDelusionsHandler.CountSame > 30 then
            WP_ParanoidDelusionsHandler.Zombie:removeFromWorld()
            WP_ParanoidDelusionsHandler.spawnZombie(player)
            if WP_ParanoidDelusionsHandler.DEBUG then print("ParanoidDelusionsHandler: Zombie removed because it was stuck") end
        end
    else
        WP_ParanoidDelusionsHandler.ZombieLastX = zombieX
        WP_ParanoidDelusionsHandler.ZombieLastY = zombieY
        WP_ParanoidDelusionsHandler.ZombieLastZ = zombieZ
        WP_ParanoidDelusionsHandler.CountSame = 0
    end
end

function WP_ParanoidDelusionsHandler.CheckPlayer(player)
    if not player:HasTrait("ParanoidDelusions") then return end

    if WP_ParanoidDelusionsHandler.Zombie then
        WP_ParanoidDelusionsHandler.CheckZombie(player)
        return
    end

    if WP_ParanoidDelusionsHandler.NumToSpawn > 0 then
        if WP_ParanoidDelusionsHandler.spawnZombie(player) then
            WP_ParanoidDelusionsHandler.ZombieTime = getTimestamp()
            WP_ParanoidDelusionsHandler.NumToSpawn = WP_ParanoidDelusionsHandler.NumToSpawn - 1
            player:getStats():setStress(player:getStats():getStress() + 0.5)
        end
        return
    end


    if WP_ParanoidDelusionsHandler.LastSecondChecked == getTimestamp() then return end
    WP_ParanoidDelusionsHandler.LastSecondChecked = getTimestamp()

    local md = player:getModData()

    local nextSimple = md.ParanoidDelusionsSimple or ZombRand(0, 7200)
    if nextSimple <= 0 then
        player:getStats():setPanic(100)
        player:getStats():setStress(player:getStats():getStress() + 0.5)
        local soundId = player:getEmitter():playSoundImpl("ZombieSurprisedPlayer", nil)
        player:getEmitter():setVolume(soundId, getCore():getOptionJumpScareVolume() / 10.0)
        md.ParanoidDelusionsSimple = ZombRand(0, 7200)
        return
    else
        md.ParanoidDelusionsSimple = nextSimple - 1
        if WP_ParanoidDelusionsHandler.DEBUG then print("ParanoidDelusionsHandler: Next simple attack in " .. nextSimple .. " seconds") end
    end


    local nextBasic = md.ParanoidDelusionsNextBasic or ZombRand(0, 28800)
    if nextBasic <= 0 then
        WP_ParanoidDelusionsHandler.NumToSpawn = 1
        md.ParanoidDelusionsNextBasic = ZombRand(0, 28800)
        return
    else
        md.ParanoidDelusionsNextBasic = nextBasic - 1
        if WP_ParanoidDelusionsHandler.DEBUG then print("ParanoidDelusionsHandler: Next basic attack in " .. nextBasic .. " seconds") end
    end

    local nextAdvanced = md.ParanoidDelusionsNextAdvanced or ZombRand(0, 86400)
    if nextAdvanced <= 0 then
        WP_ParanoidDelusionsHandler.NumToSpawn = ZombRand(3, 8)
        md.ParanoidDelusionsNextAdvanced = ZombRand(0, 86400)
        return
    else
        md.ParanoidDelusionsNextAdvanced = nextAdvanced - 1
        if WP_ParanoidDelusionsHandler.DEBUG then print("ParanoidDelusionsHandler: Next advanced attack in " .. nextAdvanced .. " seconds") end
    end
end

Events.OnPlayerUpdate.Add(WP_ParanoidDelusionsHandler.CheckPlayer)