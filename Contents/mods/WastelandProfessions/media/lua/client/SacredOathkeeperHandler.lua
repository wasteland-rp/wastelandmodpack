if WP_SacredOathkeeperHandler then
    Events.OnPlayerUpdate.Remove(WP_SacredOathkeeperHandler.CheckPlayer)
    Events.OnZombieDead.Remove(WP_SacredOathkeeperHandler.OnZombieDead)
else
    WP_SacredOathkeeperHandler = {}
    WP_SacredOathkeeperHandler.DEBUG = false
    WP_SacredOathkeeperHandler.DidApply = false
end

function WP_SacredOathkeeperHandler.CheckPlayer(player)
    if player:HasTrait("SacredOathkeeper") and player:getPrimaryHandItem() then
        player:setBannedAttacking(true)
        WP_SacredOathkeeperHandler.DidApply = true
    elseif WP_SacredOathkeeperHandler.DidApply then
        player:setBannedAttacking(false)
        WP_SacredOathkeeperHandler.DidApply = false
    end
end

function WP_SacredOathkeeperHandler.OnZombieDead(zombie)
    local player = getPlayer()
    if not player:HasTrait("SacredOathkeeper") then return end
    if zombie:getAttackedBy() ~= player then return end
	player:getBodyDamage():setUnhappynessLevel(100)
    player:getStats():setStress(100)
    player:getStats():setPanic(100)
end

Events.OnZombieDead.Add(WP_SacredOathkeeperHandler.OnZombieDead)
Events.OnPlayerUpdate.Add(WP_SacredOathkeeperHandler.CheckPlayer)
