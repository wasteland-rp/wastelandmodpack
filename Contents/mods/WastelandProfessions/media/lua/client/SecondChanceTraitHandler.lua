if WP_SecondChance then
    -- hot reload
    Events.OnPlayerUpdate.Remove(WP_SecondChance.CheckPlayer)
    Events.OnCreatePlayer.Remove(WP_SecondChance.OnCreatePlayer)
else
    WP_SecondChance = {}
    WP_SecondChance.CurrentInjuries = {}
    WP_SecondChance.DEBUG = false
end

WP_SecondChance.SavedPhrases = {
    "I dodged a bullet there!",
    "Luck's on my side today.",
    "Can't keep me down that easily!",
    "Made it through… again.",
    "That was a close one!",
    "Still standing!",
    "Not today!",
    "They’ll have to try harder!",
    "Not my time yet!",
    "Got to be my lucky day!",
}

function WP_SecondChance.Log(message)
    print("WP_SecondChance: " .. message)
end

function WP_SecondChance.CatalogCurrentInjuries(player)
    local bodyDamage = player:getBodyDamage()
    local bodyParts = bodyDamage:getBodyParts()
    local injuries = {}

    for i=0, bodyParts:size()-1 do
        local bodyPart = bodyParts:get(i)
        local bodyPartType = bodyPart:getType()
        injuries[bodyPartType] = bodyPart:HasInjury()
    end
    return injuries
end

function WP_SecondChance.GetNewInjuries(player)
    local currentInjuries = WP_SecondChance.CatalogCurrentInjuries(player)
    local newInjuries = {}
    local isNew = false

    for bodyPartType, hasInjury in pairs(currentInjuries) do
        if hasInjury and not WP_SecondChance.CurrentInjuries[bodyPartType] then
            newInjuries[bodyPartType] = true
            isNew = true
        end
    end

    return isNew, newInjuries
end

function WP_SecondChance.RollInjury(player, bodyPartType)
    if ZombRand(2) == 0 and not player:getBodyDamage():getBodyPart(bodyPartType):haveBullet() then
        player:getBodyDamage():getBodyPart(bodyPartType):RestoreToFullHealth()
        player:Say(WP_SecondChance.SavedPhrases[ZombRand(#WP_SecondChance.SavedPhrases) + 1])
        if WP_SecondChance.DEBUG then WP_SecondChance.Log("Player saved from injury on " .. tostring(bodyPartType)) end
    end
end

function WP_SecondChance.CheckPlayer(player)
    if not player:HasTrait("SecondChance") then return end

    local isNew, newInjuries = WP_SecondChance.GetNewInjuries(player)
    if isNew then
        for bodyPartType, hasInjury in pairs(newInjuries) do
            if hasInjury then
                if WP_SecondChance.DEBUG then WP_SecondChance.Log("Player has new injury: " .. tostring(bodyPartType)) end
                WP_SecondChance.RollInjury(player, bodyPartType)
            end
        end
    end
    WP_SecondChance.CurrentInjuries = WP_SecondChance.CatalogCurrentInjuries(player)
end

function WP_SecondChance.OnCreatePlayer(playerIdx, player)
    WP_SecondChance.CurrentInjuries = WP_SecondChance.CatalogCurrentInjuries(player)
end

Events.OnPlayerUpdate.Add(WP_SecondChance.CheckPlayer)
Events.OnCreatePlayer.Add(WP_SecondChance.OnCreatePlayer)