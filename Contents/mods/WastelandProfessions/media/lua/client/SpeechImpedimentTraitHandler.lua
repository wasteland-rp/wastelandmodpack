local function shouldMangle(player)
    return player ~= getPlayer() and player:HasTrait("SpeechImpediment")
end

-- replace random words or groups of words with <unintelligible>
local function manglePhrase(message)
    local originalWords = {}
    local finalWords = {}
    local wasLastReplaced = false
    for word in string.gmatch(message, "%S+") do
        table.insert(originalWords, word)
    end
    for _, word in ipairs(originalWords) do
        if ZombRand(100) < 7 then -- 5% chance to replace a word
            table.insert(finalWords, string.rep("*", string.len(word)))
        else
            table.insert(finalWords, word)
        end
    end

    message = table.concat(finalWords, " ")
    return message
end

local function mangleMessage(message)
    local chunks = {}
    for chunk in string.gmatch(message, "([^\"]+)") do
        table.insert(chunks, chunk)
    end
    for i, chunk in ipairs(chunks) do
        if i % 2 == 0 then
            chunks[i] = manglePhrase(chunk)
        end
    end
    return table.concat(chunks, "\"")
end

local original_ISChat_addLineInChat = ISChat.addLineInChat;
ISChat.addLineInChat = function(message, tabID)
    local line = message:getTextWithPrefix()
    local player = getPlayerFromUsername(message:getAuthor())

    if not player or
       message:isServerAlert()
       or not shouldMangle(player)
       or string.match(line, "%[ASL%]")
       or string.match(line, "IMAGE%:")
       or string.match(line, "Event%) %*%*")
    then
        original_ISChat_addLineInChat(message, tabID)
        return
    end
    message:setText(mangleMessage(message:getText()))
    original_ISChat_addLineInChat(message, tabID)
end