if not Recipe then
    Recipe = {}
end
if not Recipe.OnCreate then
    Recipe.OnCreate = {}
end

function Recipe.OnCreate.BrewAllergyRemedy(items, result, player)
    player:getInventory():AddItem("Bowl")
    player:getInventory():AddItem("RippedSheetsDirty")
end

function Recipe.OnCreate.TakeAllergyRemedy(items, result, player)
    player:getInventory():AddItem("JarLid")
    player:getModData().WP_Allergies_Allergens = math.max(player:getModData().WP_Allergies_Allergens - 25, 0)
    WP_Allergies.UpdateAllergiesMoodle(player)
end