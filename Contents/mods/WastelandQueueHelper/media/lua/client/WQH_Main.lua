local originalCreate = ServerList.create
function ServerList:create()
    originalCreate(self)

    if isSteamOverlayEnabled() then

        self.viewWLQueueButton = ISButton:new(self.refreshBtn:getRight() + 10, self.refreshBtn:getY(), 140, self.refreshBtn:getHeight(), "WASTELAND QUEUE", self, ServerList.viewWLQueue)
        self.viewWLQueueButton:initialise();
        self.viewWLQueueButton:instantiate();
        self.viewWLQueueButton:setAnchorLeft(true);
        self.viewWLQueueButton:setAnchorRight(false);
        self.viewWLQueueButton:setAnchorTop(false);
        self.viewWLQueueButton:setAnchorBottom(true);
        self:addChild(self.viewWLQueueButton)
    end
end

function ServerList:viewWLQueue()
    activateSteamOverlayToWebPage("https://wastelandrp.net/Queue")
end