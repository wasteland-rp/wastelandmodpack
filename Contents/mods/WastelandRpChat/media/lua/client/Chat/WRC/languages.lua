if not isClient() then return end -- only in MP
WRC = WRC or {}

-- Define possible languages
WRC.Languages = {}
WRC.Languages["en"] = {
    name = "English",
}
WRC.Languages["asl"] = {
    name = "American Sign Language",
}
WRC.Languages["es"] = {
    name = "Spanish",
    canFullyUnderstand = {"pt"},
}
WRC.Languages["fr"] = {
    name = "French",
}
WRC.Languages["de"] = {
    name = "German",
    canPartiallyUnderstand = {"yi"},
    canFullyUnderstand = {"nl"},
}
WRC.Languages["it"] = {
    name = "Italian",
    canPartiallyUnderstand = {"es", "pt", "ro"},
}
WRC.Languages["ru"] = {
    name = "Russian",
    canPartiallyUnderstand = {"bg"},
    canFullyUnderstand = {"uk"},
}
WRC.Languages["zh"] = {
    name = "Chinese",
}
WRC.Languages["ja"] = {
    name = "Japanese",
}
WRC.Languages["ko"] = {
    name = "Korean",
}
WRC.Languages["pt"] = {
    name = "Portuguese",
    canFullyUnderstand = {"es"},
}
WRC.Languages["pl"] = {
    name = "Polish",
}
WRC.Languages["sv"] = {
    name = "Swedish",
    canPartiallyUnderstand = {"no", "da"},
}
WRC.Languages["nl"] = {
    name = "Dutch",
    canPartiallyUnderstand = {"af"},
    canFullyUnderstand = {"de"},
}
WRC.Languages["cs"] = {
    name = "Czech",
    canPartiallyUnderstand = {"sk"},
}
WRC.Languages["hu"] = {
    name = "Hungarian",
}
WRC.Languages["fn"] = {
    name = "Finnish",
}
WRC.Languages["tr"] = {
    name = "Turkish",
}
WRC.Languages["no"] = {
    name = "Norwegian",
    canPartiallyUnderstand = {"sv", "da"},
}
WRC.Languages["da"] = {
    name = "Danish",
    canPartiallyUnderstand = {"sv", "no"},
}
WRC.Languages["ro"] = {
    name = "Romanian",
    canPartiallyUnderstand = {"it"},
}
WRC.Languages["bg"] = {
    name = "Bulgarian",
    canPartiallyUnderstand = {"ru", "sr", "mk"},
}
WRC.Languages["el"] = {
    name = "Greek",
}
WRC.Languages["uk"] = {
    name = "Ukrainian",
    canFullyUnderstand = {"ru"},
}
WRC.Languages["sk"] = {
    name = "Slovak",
    canPartiallyUnderstand = {"cs"},
}
WRC.Languages["hr"] = {
    name = "Croatian",
    canPartiallyUnderstand = {"sr", "bo", "sl"},
}
WRC.Languages["sr"] = {
    name = "Serbian",
    canPartiallyUnderstand = {"hr", "bg", "mk", "bo"},
}
WRC.Languages["sl"] = {
    name = "Slovenian",
    canPartiallyUnderstand = {"hr", "sr", "bo"},
}
WRC.Languages["lt"] = {
    name = "Lithuanian",
}
WRC.Languages["lv"] = {
    name = "Latvian",
}
WRC.Languages["et"] = {
    name = "Estonian",
}
WRC.Languages["ar"] = {
    name = "Arabic",
}
WRC.Languages["he"] = {
    name = "Hebrew",
}
WRC.Languages["th"] = {
    name = "Thai",
}
WRC.Languages["vi"] = {
    name = "Vietnamese",
}
WRC.Languages["id"] = {
    name = "Indonesian",
    canPartiallyUnderstand = {"ms", "fi", "ta"},
}
WRC.Languages["ms"] = {
    name = "Malay",
    canPartiallyUnderstand = {"id", "fi", "ta"},
}
WRC.Languages["hi"] = {
    name = "Hindi",
    canPartiallyUnderstand = {"ur", "mr", "pnb", "gu", "bho"},
}
WRC.Languages["bn"] = {
    name = "Bengali",
}
WRC.Languages["fa"] = {
    name = "Persian",
    canPartiallyUnderstand = {"prs"},
}
WRC.Languages["ur"] = {
    name = "Urdu",
    canPartiallyUnderstand = {"hi"},
}
WRC.Languages["sw"] = {
    name = "Swahili",
}
WRC.Languages["af"] = {
    name = "Afrikaans",
    canPartiallyUnderstand = {"nl"},
}
WRC.Languages["eo"] = {
    name = "Esperanto",
}
WRC.Languages["is"] = {
    name = "Icelandic",
}
WRC.Languages["cy"] = {
    name = "Welsh",
}
WRC.Languages["yi"] = {
    name = "Yiddish",
    canPartiallyUnderstand = {"de"},
}
WRC.Languages["la"] = {
    name = "Latin",
}
WRC.Languages["ga"] = {
    name = "Gaelic",
}
WRC.Languages["hw"] = {
    name = "Hawaiian",
}
WRC.Languages["al"] = {
    name = "Albanian",
}
WRC.Languages["bo"] = {
    name = "Bosnian",
    canPartiallyUnderstand = {"hr", "sr", "sl"},
}
WRC.Languages["fi"] = {
    name = "Filipino",
    canPartiallyUnderstand = {"ms", "id", "ta"},
    canFullyUnderstand = {"ta"},
}
WRC.Languages["ta"] = {
    name = "Tagalog",
    canPartiallyUnderstand = {"ms", "id", "fi"},
    canFullyUnderstand = {"fi"},
}
WRC.Languages["bn"] = {
    name = "Bengali",
}
WRC.Languages["pcm"] = {
    name = "Nigerian Pidgin",
}
WRC.Languages["mr"] = {
    name = "Marathi",
    canPartiallyUnderstand = {"hi"},
}
WRC.Languages["te"] = {
    name = "Telugu",
    canPartiallyUnderstand = {"ta"},
}
WRC.Languages["ha"] = {
    name = "Hausa",
}
WRC.Languages["tam"] = {
    name = "Tamil",
    canPartiallyUnderstand = {"te"},
    canFullyUnderstand = {"si"},
}
WRC.Languages["sw"] = {
    name = "Swahili",
}
WRC.Languages["pnb"] = {
    name = "Western Punjabi",
    canPartiallyUnderstand = {"hi", "ur"},
}
WRC.Languages["fa"] = {
    name = "Iranian Persian",
    canPartiallyUnderstand = {"prs"},
}
WRC.Languages["jv"] = {
    name = "Javanese",
}
WRC.Languages["gu"] = {
    name = "Gujarati",
    canPartiallyUnderstand = {"hi"},
}
WRC.Languages["bho"] = {
    name = "Bhojpuri",
    canPartiallyUnderstand = {"hi"},
}
WRC.Languages["si"] = {
    name = "Sinhala",
    canFullyUnderstand = {"tam"},
}
WRC.Languages["prs"] = {
    name = "Farsi",
    canFullyUnderstand = {"fa"},
}