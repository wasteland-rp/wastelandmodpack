local Intoxicants = {
    weed = {
        name = "Weed",
        items = {
            ["MoreSmokes.JointNorthernLights"] = 1,
            ["MoreSmokes.JointPurpleHaze"] = 1,
            ["MoreSmokes.JointSourDiesel"] = 1,
            ["MoreSmokes.JointIndigoFog"] = 2,
            ["MoreSmokes.BluntNorthernLights"] = 2,
            ["MoreSmokes.BluntPurpleHaze"] = 2,
            ["MoreSmokes.BluntSourDiesel"] = 2,
            ["MoreSmokes.BluntBackwoods"] = 3,
            ["MoreSmokes.SpliffNorthernLights"] = 1,
            ["MoreSmokes.SpliffPurpleHaze"] = 1,
            ["MoreSmokes.SpliffSourDiesel"] = 1,
            ["MoreSmokes.CannabisButter"] = 8,
            ["MoreSmokes.CannabisButterGreenDreams"] = 12,
            ["MoreSmokes.CannabisChocolate"] = 2,
            ["MoreSmokes.CannabisChocolateBar"] = 2,
            ["MoreSmokes.CannabisBrowniesPan"] = 8,
            ["MoreSmokes.CannabisBrownie"] = 1,
            ["MoreSmokes.CannabisCookieChocolateChipDough"] = 4,
            ["MoreSmokes.CannabisCookieChocolateChip"] = 1,
            ["MoreSmokes.CannabisCookiesSugarDough"] = 8,
            ["MoreSmokes.CannabisCookiesSugar"] = 1,
            ["MoreSmokes.CannabisGummies"] = 1,
            ["MoreSmokes.JointNorthernLightsPlus"] = 2,
            ["MoreSmokes.JointPurpleHazePlus"] = 2,
            ["MoreSmokes.JointSourDieselPlus"] = 2,
            ["MoreSmokes.CannabisBrowniesPanPlus"] = 16,
            ["MoreSmokes.CannabisBrowniePlus"] = 2,
            ["MoreSmokes.CannabisCookiesSugarDoughPlus"] = 16,
            ["MoreSmokes.CannabisCookiesSugarPlus"] = 2,

            ["MoreSmokes.SmokePipe1NorthernLights"] = 1,
            ["MoreSmokes.SmokePipe2NorthernLights"] = 1,
            ["MoreSmokes.SmokePipe3NorthernLights"] = 1,
            ["MoreSmokes.SmokePipeGlassBlueNorthernLights"] = 1,
            ["MoreSmokes.SmokePipeGlassGreenNorthernLights"] = 1,
            ["MoreSmokes.SmokePipeGlassYellowNorthernLights"] = 1,
            ["MoreSmokes.SmokePipePinkNorthernLights"] = 1,
            ["MoreSmokes.SmokePipe1PurpleHaze"] = 1,
            ["MoreSmokes.SmokePipe2PurpleHaze"] = 1,
            ["MoreSmokes.SmokePipe3PurpleHaze"] = 1,
            ["MoreSmokes.SmokePipeGlassBluePurpleHaze"] = 1,
            ["MoreSmokes.SmokePipeGlassGreenPurpleHaze"] = 1,
            ["MoreSmokes.SmokePipeGlassYellowPurpleHaze"] = 1,
            ["MoreSmokes.SmokePipePinkPurpleHaze"] = 1,
            ["MoreSmokes.SmokePipe1SourDiesel"] = 1,
            ["MoreSmokes.SmokePipe2SourDiesel"] = 1,
            ["MoreSmokes.SmokePipe3SourDiesel"] = 1,
            ["MoreSmokes.SmokePipeGlassBlueSourDiesel"] = 1,
            ["MoreSmokes.SmokePipeGlassGreenSourDiesel"] = 1,
            ["MoreSmokes.SmokePipeGlassYellowSourDiesel"] = 1,
            ["MoreSmokes.SmokePipePinkSourDiesel"] = 1,
            ["MoreSmokes.Hookah1NorthernLights"] = 1,
            ["MoreSmokes.Hookah1PurpleHaze"] = 1,
            ["MoreSmokes.Hookah1SourDiesel"] = 2,
            ["MoreSmokes.Hookah2NorthernLights"] = 2,
            ["MoreSmokes.Hookah2PurpleHaze"] = 2,
            ["MoreSmokes.Hookah2SourDiesel"] = 2,
            ["MoreSmokes.Hookah3NorthernLights"] = 2,
            ["MoreSmokes.Hookah3PurpleHaze"] = 2,
            ["MoreSmokes.Hookah3SourDiesel"] = 2,
            ["MoreSmokes.Bong1v1NorthernLights"] = 2,
            ["MoreSmokes.Bong1v2NorthernLights"] = 2,
            ["MoreSmokes.Bong1v3NorthernLights"] = 2,
            ["MoreSmokes.Bong1v4NorthernLights"] = 2,
            ["MoreSmokes.Bong2v1NorthernLights"] = 2,
            ["MoreSmokes.Bong2v2NorthernLights"] = 2,
            ["MoreSmokes.Bong2v3NorthernLights"] = 2,
            ["MoreSmokes.Bong2v4NorthernLights"] = 2,
            ["MoreSmokes.Bong3v1NorthernLights"] = 2,
            ["MoreSmokes.Bong3v2NorthernLights"] = 2,
            ["MoreSmokes.Bong3v3NorthernLights"] = 2,
            ["MoreSmokes.Bong3v4NorthernLights"] = 2,
            ["MoreSmokes.Bong4v1NorthernLights"] = 2,
            ["MoreSmokes.Bong4v2NorthernLights"] = 2,
            ["MoreSmokes.Bong4v3NorthernLights"] = 2,
            ["MoreSmokes.Bong4v4NorthernLights"] = 2,
            ["MoreSmokes.Bong5NorthernLights"] = 2,
            ["MoreSmokes.Bong1v1PurpleHaze"] = 2,
            ["MoreSmokes.Bong1v2PurpleHaze"] = 2,
            ["MoreSmokes.Bong1v3PurpleHaze"] = 2,
            ["MoreSmokes.Bong1v4PurpleHaze"] = 2,
            ["MoreSmokes.Bong2v1PurpleHaze"] = 2,
            ["MoreSmokes.Bong2v2PurpleHaze"] = 2,
            ["MoreSmokes.Bong2v3PurpleHaze"] = 2,
            ["MoreSmokes.Bong2v4PurpleHaze"] = 2,
            ["MoreSmokes.Bong3v1PurpleHaze"] = 2,
            ["MoreSmokes.Bong3v2PurpleHaze"] = 2,
            ["MoreSmokes.Bong3v3PurpleHaze"] = 2,
            ["MoreSmokes.Bong3v4PurpleHaze"] = 2,
            ["MoreSmokes.Bong4v1PurpleHaze"] = 2,
            ["MoreSmokes.Bong4v2PurpleHaze"] = 2,
            ["MoreSmokes.Bong4v3PurpleHaze"] = 2,
            ["MoreSmokes.Bong4v4PurpleHaze"] = 2,
            ["MoreSmokes.Bong5PurpleHaze"] = 2,
            ["MoreSmokes.Bong1v1SourDiesel"] = 2,
            ["MoreSmokes.Bong1v2SourDiesel"] = 2,
            ["MoreSmokes.Bong1v3SourDiesel"] = 2,
            ["MoreSmokes.Bong1v4SourDiesel"] = 2,
            ["MoreSmokes.Bong2v1SourDiesel"] = 2,
            ["MoreSmokes.Bong2v2SourDiesel"] = 2,
            ["MoreSmokes.Bong2v3SourDiesel"] = 2,
            ["MoreSmokes.Bong2v4SourDiesel"] = 2,
            ["MoreSmokes.Bong3v1SourDiesel"] = 2,
            ["MoreSmokes.Bong3v2SourDiesel"] = 2,
            ["MoreSmokes.Bong3v3SourDiesel"] = 2,
            ["MoreSmokes.Bong3v4SourDiesel"] = 2,
            ["MoreSmokes.Bong4v1SourDiesel"] = 2,
            ["MoreSmokes.Bong4v2SourDiesel"] = 2,
            ["MoreSmokes.Bong4v3SourDiesel"] = 2,
            ["MoreSmokes.Bong4v4SourDiesel"] = 2,
            ["MoreSmokes.Bong5SourDiesel"] = 2,
        },
        max = 6,
        declineRate = 0.00167,
        levelMessages = {
            [1] = "Slightly High\n  - Bit Munchy, Relaxed, Happy",
            [2] = "Stoned\n  - Very Munchy, Reduced perception of pain, Sluggish, Think you're hilarious",
            [3] = "Very High\n  - Can eat until sick, Numbness to most pain, About to fall asleep, Find everything interesting",
            [4] = "Too High\n  - Can barely walk or talk, Passing out, Throwing up"
        }
    },
    cocaine = {
        name = "Cocaine",
        items = {
            ["DrugMod.Cocaine"] = 5,
            ["DrugMod.MirrorWith1LineCocaine"] = 1,
            ["DrugMod.MirrorWith2LineCocaine"] = 1,
            ["DrugMod.MirrorWith3LineCocaine"] = 1,
            ["DrugMod.MirrorWith4LineCocaine"] = 1,
        },
        max = 6,
        declineRate = 0.01,
        levelMessages = {
            [1] = "Slightly High\n  - Energetic, Talkative, Confident",
            [2] = "Rush\n  - Very Energetic, Talkative, Overly Confident",
            [4] = "Very High\n  - Overly Energetic, Paranoid, Heart Pumping Fast",
            [5] = "Too High\n  - Heart Attack Risk, Paranoid, Very Energetic"
        }
    },
    crack = {
        name = "Crack",
        items = {
            ["DrugMod.CrackPipe"] = 2,
        },
        max = 4,
        declineRate = 0.02,
        levelMessages = {
            [1] = "High\n  - Intense Euphoria, Burst of Energy, Jittery, Reckless",
            [3] = "Too High\n  - Heart Attack, Passed Out"
        }
    },
    heroin = {
        name = "Heroin",
        items = {
            ["DrugMod.HeroinNeedle"] = 1
        },
        max = 4,
        declineRate = 0.00083,
        levelMessages = {
            [1] = "High\n  - Intense Warmth, Numbness to Pain, Drowsy",
            [3] = "Too High\n  - Unable to Speak or Move, Passing Out, Heart Failure Risk"
        }
    },
    meth = {
        name = "Meth",
        items = {
            ["DrugMod.MethPipe"] = 1,
        },
        max = 3,
        declineRate = 0.00037,
        levelMessages = {
            [1] = "High\n  - Extreme Energy, Invincibility, Erratic Thoughts",
            [3] = "Too High\n  - Heart Failure Risk, Unable to Move or Communicate"
        }
    },
    fentanyl = {
        name = "Fentanyl",
        items = {
            ["DrugMod.Fentanyl"] = 1,
        },
        max = 3,
        declineRate = 0.00222,
        levelMessages = {
            [1] = "High\n  - Intense Euphoria, Numbness to Pain, Slowed Breathing, Floaty",
            [3] = "Too High\n  - Heart Failure Risk, Unable to Communicate or Move"
        }
    },
    speed = {
        name = "Speed",
        items = {
            ["DrugMod.Speed"] = 1,
        },
        max = 3,
        declineRate = 0.00056,
        levelMessages = {
            [1] = "High\n  - Intense Focus, Energy, Suppressed Appetite, Oversharing",
            [3] = "Too High\n  - Heart Attack Risk, Unable to Move or Communicate"
        }
    },
    xanax = {
        name = "Xanax",
        items = {
            ["DrugMod.Xanax"] = 1,
        },
        max = 5,
        declineRate = 0.00067,
        levelMessages = {
            [1] = "Calm\n  - Reduced Anxiety, Mild Sedation",
            [2] = "Sedated\n  - Drowsy, Slowed Reactions, Reduced Coordination",
            [4] = "Heavily Sedated\n  - Very Tired, Slow Responses, Disconnected, Passing Out Risk"
        }
    },
    percocet = {
        name = "Percocet",
        items = {
            ["DrugMod.Percocet"] = 1,
        },
        max = 5,
        declineRate = 0.00095,
        levelMessages = {
            [1] = "Relaxed\n  - Reduced Pain, Mild Euphoria, Calming",
            [2] = "Numbed\n  - Very Relaxed, Drowsy",
            [4] = "Heavily Sedated\n  - Strong Numbness, Slowed Breathing, Passing Out Risk"
        }
    },
    acid = {
        name = "Acid",
        items = {
            ["DrugMod.Acid"] = 1,
        },
        max = 4,
        declineRate = 0.00037,
        levelMessages = {
            [1] = "Tripping\n  - Intense Visuals, Altered Reality, Deep Thoughts",
            [2] = "Heavy Trip\n  - Extreme Hallucinations, Time Distortion, Vivid Patterns",
            [3] = "Bad Trip\n  - Overwhelmed, Paranoia, Intense Anxiety"
        }
    },
    shrooms = {
        name = "Mushroom",
        items = {
            ["DrugMod.Shrooms"] = 1,
        },
        max = 5,
        declineRate = 0.00067,
        levelMessages = {
            [1] = "Slight Trip\n  - Mild Hallucinations, Enhanced Connection to Nature",
            [2] = "Tripping\n  - Intense Visuals, Unity with Nature, Time Distortion",
            [3] = "Heavy Trip\n  - Deep Connection, Profound Thoughts, Altered Reality",
            [4] = "Bad Trip\n  - Overwhelmed, Anxiety, Confusion, Fear"
        }
    }
}

local currentLevels = {}

for k, v in pairs(Intoxicants) do
    currentLevels[k] = 0
end

local ISEatFoodAction_perform = ISEatFoodAction.perform
function ISEatFoodAction:perform()
    ISEatFoodAction_perform(self)
    local itemID = self.item:getFullType()
    for k, v in pairs(Intoxicants) do
        if v.items[itemID] then
            currentLevels[k] = currentLevels[k] + v.items[itemID]
            if currentLevels[k] > v.max then
                currentLevels[k] = v.max
            end
            if currentLevels[k] < 0 then
                currentLevels[k] = 0
            end
        end
    end
end

local lastSecond = 0
local lastSync = 0
local needsSync = false
local function reduceLevels()
    if lastSecond == getTimestamp() then
        return
    end
    lastSecond = getTimestamp()
    for k, v in pairs(Intoxicants) do
        if currentLevels[k] > 0 then
            currentLevels[k] = currentLevels[k] - v.declineRate
            if currentLevels[k] < 0 then
                currentLevels[k] = 0
            end
            needsSync = true
        end
    end
    if needsSync and getTimestamp() - lastSync > 60 then
        lastSync = getTimestamp()
        WL_UserData.Set("WRH.Intoxicants", currentLevels)
        needsSync = false
    end
end
Events.OnTick.Add(reduceLevels)

WL_PlayerReady.Add(function()
    WL_UserData.Fetch("WRH.Intoxicants", function(data)
        if data then
            currentLevels = data
            for k, v in pairs(Intoxicants) do
                if currentLevels[k] == nil then
                    currentLevels[k] = 0
                end
            end
        end
    end)
end)

local WRH_System = {}
WRH_System.name = "Intoxicants"
--- @return string[]
function WRH_System.GetHints()
    local hints = {}
    for k, v in pairs(Intoxicants) do
        if currentLevels[k] > 0 then
            local currentLevel = currentLevels[k] + 0.99
            local currentMaxLevel = 0
            local currentMessage = ""
            for level, message in pairs(v.levelMessages) do
                if level > currentMaxLevel and level <= currentLevel then
                    currentMaxLevel = level
                    currentMessage = message
                end
            end
            if currentMaxLevel > 0 then
                table.insert(hints, v.name .. ": " .. currentMessage)
            end
        end
    end
    return hints
end

WRH.RegisterSystem(WRH_System)