ISChat.WRH_oPrerender = ISChat.WRH_oPrerender or ISChat.prerender
function ISChat:prerender()
    ISChat.WRH_oPrerender(self)

    local totalNeededHeight = 4
    local hints = WRH.GetHints()

    local wasAny = false
    for _, hint in ipairs(hints) do
        totalNeededHeight = totalNeededHeight + getTextManager():MeasureStringY(UIFont.Small, hint) + 10
        wasAny = true
    end
    if not wasAny then
        return
    end
    totalNeededHeight = totalNeededHeight - 10
    self:drawRect(0, -totalNeededHeight, self.width, totalNeededHeight, self:calcAlpha(ISChat.minTextEntryOpaque, ISChat.maxTextEntryOpaque, self.fade:fraction()), 0, 0, 0);
    totalNeededHeight = totalNeededHeight - 2
    for _, hint in ipairs(hints) do
        self:drawText(hint, 2, -totalNeededHeight, 1, 1, 1, 1, UIFont.Small)
        totalNeededHeight = totalNeededHeight - getTextManager():MeasureStringY(UIFont.Small, hint) - 10
    end
end