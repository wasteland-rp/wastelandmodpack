if isServer() then
    return
end

--- @class WRH
WRH = WRH or {
    registeredSystems = {}
}

function WRH.RegisterSystem(system)
    WRH.registeredSystems[system.name] = system
end

function WRH.GetHints()
    local hints = {}
    for _, system in pairs(WRH.registeredSystems) do
        for _, hint in ipairs(system.GetHints()) do
            table.insert(hints, hint)
        end
    end
    return hints
end

--- @function GetHintsFunc
--- @return string[]

--- @class WRH_System
--- @field name string
--- @field GetHints GetHintsFunc

