local ISConsolidateDrainableAll_new = ISConsolidateDrainableAll.new
function ISConsolidateDrainableAll:new(character, drainable, consolidateList, time)
    return ISConsolidateDrainableAll_new(self, character, drainable, consolidateList, 5)
end

local ISConsolidateDrainable_new = ISConsolidateDrainable.new
function ISConsolidateDrainable:new(character, drainable, intoItem, time, otherItems)
    return ISConsolidateDrainable_new(self, character, drainable, intoItem, 5, otherItems)
end