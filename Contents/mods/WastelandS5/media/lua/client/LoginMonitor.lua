local timeStarted = -1

Events.OnGameBoot.Add(function()
    timeStarted = 0
end)

Events.OnGameTimeLoaded.Add(function()
    if timeStarted == 0 then
        timeStarted = getTimestamp()
    end
end)

WL_PlayerReady.Add(function(idx, player)
    if player:getHoursSurvived() < 0.25 then
        timeStarted = -1
        return
    end

    if timeStarted > 0 then
        if getTimestamp() - timeStarted > 900 then
            sendClientCommand(player, "LoginMonitor", "OverTime", {})
        end
        timeStarted = -1
    end
end)