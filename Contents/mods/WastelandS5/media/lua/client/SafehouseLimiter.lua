local function addNegativeTooltip(option, reason)
    local toolTip = ISWorldObjectContextMenu.addToolTip();
    toolTip:setVisible(false);
    toolTip.description = reason;
    option.notAvailable = true;
    option.toolTip = toolTip;
end

local function WastelandSafehouseLimiter(playerIdx, context, worldObjects)
    if WL_Utils.isStaff(getPlayer()) then return end

    local safehouseOption = context:getOptionFromName(getText("ContextMenu_SafehouseClaim"))
    if not safehouseOption then
        return
    end
    local clickedSquare = safehouseOption.param1
    if not clickedSquare then
        return
    end
    local building = clickedSquare:getBuilding()
    if not building or not building:isResidential() then
        addNegativeTooltip(safehouseOption, "Not residential. <LINE> Make ticket to claim.")
        return
    end
    if not building:getDef() then
        addNegativeTooltip(safehouseOption, "No building definition. <LINE> Make ticket to claim.")
        return
    end
    local def = building:getDef()
    local numBedrooms = 0
    local rooms = def:getRooms()
    for i=0,rooms:size()-1 do
        local room = rooms:get(i)
        if room:getName() == "bedroom" then
            numBedrooms = numBedrooms + 1
        end
    end
    if numBedrooms > 4 then
        addNegativeTooltip(safehouseOption, "Too many bedrooms. <LINE> Make ticket to claim.")
        return
    end
    local size = def:getW() * def:getH()
    if size > 400 then
        addNegativeTooltip(safehouseOption, "Too big. <LINE> Make ticket to claim.")
        return
    end
end

Events.OnFillWorldObjectContextMenu.Add(WastelandSafehouseLimiter)
