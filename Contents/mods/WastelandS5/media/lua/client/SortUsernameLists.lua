function ISFactionAddPlayerUI:populateList()
    self.playerList:clear();
    self.addPlayer.enable = false;
    if not self.scoreboard then return end

    local allUsernames = {}
    for i=1,self.scoreboard.usernames:size() do
        local username = self.scoreboard.usernames:get(i-1)
        local displayName = self.scoreboard.displayNames:get(i-1)
        table.insert(allUsernames, {username=username, displayName=displayName})
    end
    table.sort(allUsernames, function(a,b) return a.username > b.username end)

    local i = 0
    for _, user in ipairs(allUsernames) do
        i = i + 1
        local username = user.username
        local displayName = user.displayName
        local doIt = false;
        if self.changeOwnership then
            doIt = not self.faction:isOwner(username);
        else
            doIt = username ~= self.player:getUsername() and not self.faction:isMember(username);
        end
        if doIt then
            local newPlayer = {};
            newPlayer.name = username;
			if self.changeOwnership then 
				local alreadyFaction = self.faction:isMember(username);
				if not alreadyFaction then
				   newPlayer.tooltip = getText("IGUI_FactionUI_NoMember");
				end
			else
				local alreadyFaction = Faction.isAlreadyInFaction(username);
				if alreadyFaction then
				   newPlayer.tooltip = getText("IGUI_FactionUI_AlreadyHaveFaction");
				end
			end
            local index = self.playerList:addItem(displayName, newPlayer);
            if newPlayer.tooltip then
                if self.playerList.items[i] then
                    self.playerList.items[i].tooltip = newPlayer.tooltip;
                end
            end
        end
    end
end


function ISSafehouseAddPlayerUI:populateList()
    self.playerList:clear();
    if not self.scoreboard then return end

    local allUsernames = {}
    for i=1,self.scoreboard.usernames:size() do
        local username = self.scoreboard.usernames:get(i-1)
        local displayName = self.scoreboard.displayNames:get(i-1)
        table.insert(allUsernames, {username=username, displayName=displayName})
    end
    table.sort(allUsernames, function(a,b) return a.username > b.username end)

    local i = 0
    for _, user in ipairs(allUsernames) do
        i = i + 1
        local username = user.username
        local displayName = user.displayName
        if self.safehouse:getOwner() ~= username then
            local newPlayer = {};
            newPlayer.username = username;
            local alreadySafe = self.safehouse:alreadyHaveSafehouse(username);
            if alreadySafe and alreadySafe ~= self.safehouse then
                if alreadySafe:getTitle() ~= "Safehouse" then
                    newPlayer.tooltip = getText("IGUI_SafehouseUI_AlreadyHaveSafehouse", "(" .. alreadySafe:getTitle() .. ")");
                else
                    newPlayer.tooltip = getText("IGUI_SafehouseUI_AlreadyHaveSafehouse" , "");
                end
            end
            local item = self.playerList:addItem(displayName, newPlayer);
            if newPlayer.tooltip then
               item.tooltip = newPlayer.tooltip;
            end
        end
    end
end