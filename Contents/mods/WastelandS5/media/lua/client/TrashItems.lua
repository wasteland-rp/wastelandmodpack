WL_TrashItemsWindow = ISPanel:derive("TrashItemsWindow")

function WL_TrashItemsWindow:show(player, items)
    local w = 400
	local h = 300
	local o = ISPanel:new(getCore():getScreenWidth()/2-w/2,getCore():getScreenHeight()/2-h/2, w, h)

    setmetatable(o, self)
    self.__index = self

    o.player = player
    o.items = items
	o:initialise()
	o:addToUIManager()
end


local FONT_HGT_LARGE = getTextManager():getFontHeight(UIFont.Large)

function WL_TrashItemsWindow:initialise()
    ISPanel.initialise(self)

	local win = GravyUI.Node(self.width, self.height):pad(5)
    local title, items, buttons = win:rows({FONT_HGT_LARGE, 1, FONT_HGT_LARGE}, 5)
    local confirmButton, declineButton = buttons:cols(2, 5)

    self:addChild(title:makeLabel("Confirm Trash Items", UIFont.Large, {r=1,g=1,b=1,a=1}, "center"))

    local countsByName = {}
    for _, item in ipairs(self.items) do
        local name = (item:getDisplayName() or item:getName() or "???") .. " [" .. item:getFullType() .. "]"
        countsByName[name] = (countsByName[name] or 0) + 1
    end

    local itemsStr = ""
    local logStr = ""
    for name, count in pairs(countsByName) do
        if count > 1 then
            itemsStr = itemsStr .. name .. " (" .. tostring(count) .. ")\n"
        else
            itemsStr = itemsStr .. name .. "\n"
        end
        logStr = logStr .. tostring(count) .. "x " .. name .. ", "
    end
    self.logStr = logStr

    local textBoxList = items:makeTextBox(itemsStr)
    self:addChild(textBoxList)
    textBoxList:setMultipleLine(true)
    textBoxList:setEditable(false)
    textBoxList:setSelectable(false)
	textBoxList:addScrollBars()

    local cb = confirmButton:makeButton("Delete All", self, self.onConfirm)
    self:addChild(cb)
    cb.backgroundColorMouseOver = {r=1.0,g=0.3,b=0.3,a=1}

    local db = declineButton:makeButton("Keep", self, self.onClose)
    self:addChild(db)
    db.backgroundColorMouseOver = {r=0.2,g=0.8,b=0.2,a=1}
end

function WL_TrashItemsWindow:onConfirm()
    for _, item in ipairs(self.items) do
        ISRemoveItemTool.removeItem(item, self.player)
    end
    local player = getSpecificPlayer(self.player)
    local locationStr = tostring(math.floor(player:getX())) .. "," .. tostring(math.floor(player:getY())) .. "," .. tostring(math.floor(player:getZ()))
    sendClientCommand(player, 'ISLogSystem', 'writeLog', {loggerName = "TrashLog", logText = "[" .. locationStr .. "] " .. player:getUsername() .." deleted: " .. self.logStr})
    self:onClose()
end

function WL_TrashItemsWindow:onClose()
    self:close()
    self:removeFromUIManager()
end

function WL_TrashItemsWindow.OnFillInventoryObjectContextMenu(player, context, items)
    local availableItems = {}

    local submenu = WL_ContextMenuUtils.getOrCreateSubMenu(context, "Trash")

    items = ISInventoryPane.getActualItems(items)
    local playerObj = getSpecificPlayer(player)
    for _, item in ipairs(items) do
        if item:isFavorite() then
            local opt = submenu:addOption("Refusing to Trash Favorites", nil, nil)
            opt.notAvailable = true
            return
        end
        if item:hasTag("WSS_Shop") and not WL_Utils.isStaff(playerObj) and not WSS:isShopOwner(playerObj, item) then
            local opt = submenu:addOption("Refusing to Trash Shop Items", nil, nil)
            opt.notAvailable = true
            return
        end
        -- if we have Wasteland Auto Claims enabled, we need to check if the player has permission to the items in the vehicle
        if WastelandAutoClaimsCore and not WL_Utils.isStaff(playerObj) and item:getContainer() and item:getContainer():getParent() and instanceof(item:getContainer():getParent(), "BaseVehicle") and not WastelandAutoClaimsCore:playerCan(playerObj, item:getContainer():getParent(), "accessInventory") then
            local opt = submenu:addOption("No Permission", nil, nil)
            opt.notAvailable = true
            return
        end
        table.insert(availableItems, item)
    end

    submenu:addOption("Delete " .. #availableItems .. " items", WL_TrashItemsWindow, WL_TrashItemsWindow.show, player, availableItems)
end

if not WL_TrashItemsWindow.initialized then
    Events.OnFillInventoryObjectContextMenu.Add(function (player, context, items)
        WL_TrashItemsWindow.OnFillInventoryObjectContextMenu(player, context, items)
    end)
    WL_TrashItemsWindow.initialized = true
end