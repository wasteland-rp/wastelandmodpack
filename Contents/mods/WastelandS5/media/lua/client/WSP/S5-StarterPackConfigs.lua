local function isCruise(spawnLocation)
    return spawnLocation == "Cruise Preseason"
end

local function isIsland(spawnLocation)
    -- return true
    return spawnLocation == "Island Preseason"
end

local function isMainSeason(spawnLocation)
    return not isCruise(spawnLocation) and not isIsland(spawnLocation)
end

Events.OnGameStart.Add(function()
    WSP.KitGenerator.AppendConfig({
        onTest = function (spawnLocation, spawnCount)
            return isCruise(spawnLocation)
        end,
        clothing = {
            "Glasses_Sun",
            "Shirt_HawaiianTINT",
            "AuthenticZClothing.HawaiianLei",
        },
        items = {
            WSP.Utilities.rollList(1, 100, {
                {{ id = "CreditCard", customName = "Undada-C Ticket Stub: {username}" }, 1},
                {{ id = "CreditCard", customName = "Undada-C Alcohol Pass: {username}" }, 1},
                {{ id = "CreditCard", customName = "Undada-C Food Pass: {username}" }, 1},
            }),
            WSP.Utilities.funJunk(2, 50), -- 2 rolls, 50% chance each roll
            WSP.Utilities.booksAndMags(4, 50), -- 4 rolls, 50%  chance each roll
            {id = "Money", count = 300},
            WSP.Utilities.rollList(10, 50, {{"Money", 1}}),
            "BeerBottle",
            { id = "Suitcase", customName = "{username}'s Suitcase" },
        }
    })

    WSP.KitGenerator.AppendConfig({
        onTest = function (spawnLocation, spawnCount)
            return isCruise(spawnLocation) and getPlayer():isFemale()
        end,
        items = {
            WSP.Utilities.rollList(1, 100, {
                {"CBX_SK1", 1},
                {"CBX_kupalnuk", 1},
                {"Swimsuit_TINT", 1},
                {"Bakini_TINT", 1},
                {"Bakini_Pattern01", 1},
                {"Bakini_Pattern02", 1},
                {"Bakini_Pattern03", 1},
                {"Bakini_Pattern04", 1},
            }),
        }
    })

    WSP.KitGenerator.AppendConfig({
        onTest = function (spawnLocation, spawnCount)
            return isCruise(spawnLocation) and not getPlayer():isFemale()
        end,
        items = {
            -- bottom
            WSP.Utilities.rollList(1, 100, {
                {"SwimTrunks_Blue", 1},
                {"SwimTrunks_Green", 1},
                {"SwimTrunks_Yellow", 1},
                {"SwimTrunks_Red", 1},
            }),
        }
    })

    WSP.KitGenerator.AppendConfig({
        onTest = function (spawnLocation, spawnCount)
            return isIsland(spawnLocation)
        end,
        items = {
            WSP.Utilities.rollList(1, 100, {
                {{ id = "CreditCard", customName = "Braddock Parking Pass: {username}" }, 1},
                {{ id = "CreditCard", customName = "Braddock Postcard: {username}" }, 1},
            }),
            WSP.Utilities.funJunk(2, 50), -- 2 rolls, 50% chance each roll
            WSP.Utilities.booksAndMags(2, 50), -- 4 rolls, 50%  chance each roll
            {id = "Money", count = 300},
            WSP.Utilities.rollList(10, 50, {{"Money", 1}}),
            "Shoes_Wellies",
            "Hat_WoolyHat",
            "CheeseSandwich",
        }
    })

    WSP.KitGenerator.AppendConfig({
        onTest = function (spawnLocation, spawnCount)
            return isMainSeason(spawnLocation)
        end,
        items = {
            { id = "Radio.WalkieTalkie2", inHands = true },
        },
    })
end)
