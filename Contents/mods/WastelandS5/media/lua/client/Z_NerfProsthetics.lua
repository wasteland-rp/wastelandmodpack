---
--- Z_NerfProsthetics.lua
--- 02-Jan-2025
--- This is Z_ so it runs after Trash.

require "Amputations (ver. Wasteland)"
require "WL_Utils"

if NerfPros then
    Events.OnPlayerUpdate.Remove(NerfPros.checkPlayer)
    Events.OnCreatePlayer.Remove(NerfPros.OnCreatePlayer)
else
    NerfPros = {}
    NerfPros.CurrentInjuries = {}
end

local restrictedItems = {
    "AmputationsWLRP.Amp_LA",
    "AmputationsWLRP.Amp_RA",
    "AmputationsWLRP.Amp_BA",
    "AmputationsWLRP.Amp_LL",
    "AmputationsWLRP.Amp_RL",
    "AmputationsWLRP.Amp_LA_Sock",
    "AmputationsWLRP.Amp_RA_Sock",
    "AmputationsWLRP.Amp_BA_Sock",
    "AmputationsWLRP.Amp_LL_Sock",
    "AmputationsWLRP.Amp_RL_Sock",
    "AmputationsWLRP.Amp_LL_SockShoes",
    "AmputationsWLRP.Amp_RL_SockShoes",
}

local prosthetics = {
    "AmputationsWLRP.Pros_LL_M_B2",
    "AmputationsWLRP.Pros_RL_M_B2",
    "AmputationsWLRP.Pros_LL_B",
    "AmputationsWLRP.Pros_RL_B",
    "AmputationsWLRP.Pros_LA",
    "AmputationsWLRP.Pros_LL",
    "AmputationsWLRP.Pros_RA",
    "AmputationsWLRP.Pros_RL",
    "AmputationsWLRP.Pros_LA_F_S",
    "AmputationsWLRP.Pros_RA_F_S",
}

local SavedPhrases = {
    "Good thing I don't have a ",
    "That would've hurt if I had a ",
    "Ouch! Wait, I don't have a ",
    "It's a good day to be missing a ",
    "You really went for my ",
    "Joke's on you, I don't have a ",
}

local cleanNames = {
    ["ForeArm_L"] = "Left Forearm",
    ["Hand_L"] = "Left Hand",
    ["ForeArm_R"] = "Right Forearm",
    ["Hand_R"] = "Right Hand",
    ["LowerLeg_L"] = "Left Shin",
    ["Foot_L"] = "Left Foot",
    ["LowerLeg_R"] = "Right Shin",
    ["Foot_R"] = "Right Foot",
}

local bodyPartLocations = {
    ["Amputation_LA"] = { BodyLocations = { "ForeArm_L", "Hand_L" } },
    ["Amputation_RA"] = { BodyLocations = { "ForeArm_R", "Hand_R" } },
    ["Amputation_BA"] = { BodyLocations = { "ForeArm_L", "Hand_L", "ForeArm_R", "Hand_R" } },
    ["Amputation_LL"] = { BodyLocations = { "LowerLeg_L", "Foot_L" } },
    ["Amputation_RL"] = { BodyLocations = { "LowerLeg_R", "Foot_R" } },
}

local messageTime = nil

local tickCount = 0
local disabled = false
local dblTap = false
local toggleSprint = false

local function unbindSprint()
    if disabled == true then
        if getCore():isToggleToSprint() then
            toggleSprint = true
        end
        if getCore():isOptiondblTapJogToSprint() then
            dblTap = true
        end
        getCore():setOptiondblTapJogToSprint(false)
        getCore():setToggleToSprint(false)
    else
        print("Toggle Sprint", toggleSprint)
        print("Dbl Tap", dblTap)
        getCore():setOptiondblTapJogToSprint(dblTap)
        getCore():setToggleToSprint(toggleSprint)
    end
end

local function onTick()
    if tickCount < 100 then
        tickCount = tickCount + 1
        return
    elseif tickCount == 100 then
        local player = getPlayer()
        local inventory = player:getInventory()
        local equippedItems = player:getWornItems()

        local hasAmp_LL = equippedItems:getItem("Amputation_LL") ~= nil
        local hasAmp_RL = equippedItems:getItem("Amputation_RL") ~= nil
        local hasPros_LL = equippedItems:getItem("Prosthetic_LL") ~= nil
        local hasPros_RL = equippedItems:getItem("Prosthetic_RL") ~= nil

        if not hasPros_LL and hasAmp_LL then
            player:getBodyDamage():getBodyPart(BodyPartType.Foot_R):setSplint(true,0)
        elseif hasPros_LL and hasAmp_LL then
            player:getBodyDamage():getBodyPart(BodyPartType.Foot_R):setSplint(false,0)
        end

        if not hasPros_RL and hasAmp_RL then
            if player:isAllowRun() then
                player:setAllowRun(false)
            end
        elseif hasPros_RL and hasAmp_RL then
            if not player:isAllowRun() then
                player:setAllowRun(true)
            end
        end

        if not hasAmp_LL and not hasAmp_RL then
            if not player:canSprint() then
                player:setAllowSprint(true)
            end

            if not player:isAllowRun() then
                player:setAllowRun(true)
            end
        end

        if hasAmp_LL or hasAmp_RL then
            if player:canSprint() then
                player:setAllowSprint(false)
            end
            if player:isSprinting() then
                player:setSprinting(false)
            end
            disabled = true
            unbindSprint()
        else
            if disabled == true then
                disabled = false
                player:getBodyDamage():getBodyPart(BodyPartType.Foot_R):setSplint(false,0)
                unbindSprint()
            end
        end

        if player:isGodMod() then
            player:setAllowSprint(true)
            player:setSprinting(false)
        end
        tickCount = tickCount + 1
    elseif tickCount > 100 then
        tickCount = 0
    end
end

local function onEquipAttempt(player, item)
    if not item then
        return
    end

    local equippedItems = player:getWornItems()
    local hasAmp_LA = equippedItems:getItem("Amputation_LA") ~= nil
    local hasAmp_RA = equippedItems:getItem("Amputation_RA") ~= nil
    local hasAmp_BA = equippedItems:getItem("Amputation_BA") ~= nil

    if hasAmp_LA or hasAmp_RA or hasAmp_BA then
        if item:isTwoHandWeapon() and not item:isAimedFirearm() then
            if item and item:isRequiresEquippedBothHands() then
                player:setPrimaryHandItem(nil)
                player:setSecondaryHandItem(nil)
                if messageTime == nil then
                    player:Say("I can't use this with one arm.")
                    messageTime = getTimestamp()
                else
                    if getTimestamp() - messageTime > 1 then
                        player:Say("I can't use this with one arm.")
                        messageTime = getTimestamp()
                    end
                end
                return
            else
                player:setSecondaryHandItem(nil)
                if messageTime == nil then
                    player:Say("I guess I'll have to use this with one arm.")
                    messageTime = getTimestamp()
                else
                    if getTimestamp() - messageTime > 1 then
                        player:Say("I guess I'll have to use this with one arm.")
                        messageTime = getTimestamp()
                    end
                end
                return
            end
        end
        if item:isAimedFirearm() and item:getAttachmentType() ~= "Holster" then
            if item:isRequiresEquippedBothHands() then
                player:setPrimaryHandItem(nil)
                player:setSecondaryHandItem(nil)
                if messageTime == nil then
                    player:Say("I can't use this with one arm.")
                    messageTime = getTimestamp()
                else
                    if getTimestamp() - messageTime > 1 then
                        player:Say("I can't use this with one arm.")
                        messageTime = getTimestamp()
                    end
                end
                return
            end
        end
    end
end

function NerfPros.CatalogCurrentInjuries(player)
    local bodyDamage = player:getBodyDamage()
    local bodyParts = bodyDamage:getBodyParts()
    local injuries = {}

    for i=0, bodyParts:size()-1 do
        local bodyPart = bodyParts:get(i)
        local bodyPartType = bodyPart:getType()
        injuries[bodyPartType] = bodyPart:HasInjury()
    end
    return injuries
end

function NerfPros.getNewInjuries(player)
    local currentInjuries = NerfPros.CatalogCurrentInjuries(player)
    local newInjuries = {}
    local isNew = false

    for bodyPartType, hasInjury in pairs(currentInjuries) do
        if hasInjury and not NerfPros.CurrentInjuries[bodyPartType] then
            newInjuries[bodyPartType] = true
            isNew = true
        end
    end

    return isNew, newInjuries
end

function NerfPros.checkPlayer(player)
    local isNew, newInjuries = NerfPros.getNewInjuries(player)
    if isNew then
        for bodyPartType, hasInjury in pairs(newInjuries) do
            if hasInjury then
                for bodyPartKey, bodyLocations in pairs(bodyPartLocations) do
                    local wornItems = player:getWornItems():getItem(bodyPartKey)
                    if wornItems then
                        for _, bodyLocation in ipairs(bodyLocations.BodyLocations) do
                            if tostring(bodyPartType) == tostring(bodyLocation) then
                                player:getBodyDamage():getBodyPart(bodyPartType):RestoreToFullHealth()
                                local cleanName = cleanNames[tostring(bodyPartType)]
                                player:Say(SavedPhrases[ZombRand(#SavedPhrases) + 1] .. cleanName)
                                local prostheticPart = "Prosthetic_" .. string.sub(bodyPartKey, -2)
                                local equippedProsthetic = player:getWornItems():getItem(prostheticPart)
                                if equippedProsthetic then
                                    local condition = equippedProsthetic:getCondition()
                                    if condition > 5 then
                                        equippedProsthetic:setCondition(condition - 1)
                                        HaloTextHelper.addText(player, "Prosthetic Lost Condition", HaloTextHelper.getColorRed())
                                        player:playSound("ShovelHit")
                                    end
                                    if condition > 2 and condition <=5 then
                                        local roll = ZombRand(10)
                                        if roll < condition then
                                            equippedProsthetic:setCondition(condition - 1)
                                            player:removeWornItem(equippedProsthetic)
                                            player:getCurrentSquare():AddWorldInventoryItem(equippedProsthetic, 0.5, 0.5, 0)
                                            player:getInventory():Remove(equippedProsthetic)
                                            HaloTextHelper.addText(player, "Prosthetic Lost Condition", HaloTextHelper.getColorRed())
                                            HaloTextHelper.addText(player, "Prosthetic Dropped", HaloTextHelper.getColorRed())
                                            player:playSound("ShovelSwing")
                                        end
                                    elseif condition <= 2 then
                                        player:removeWornItem(equippedProsthetic)
                                        player:getInventory():Remove(equippedProsthetic)
                                        HaloTextHelper.addText(player, "Prosthetic Irreparably Broke", HaloTextHelper.getColorRed())
                                        player:playSound("ShovelBreak")
                                    end
                                end
                                break
                            end
                        end
                    end
                end
            end
        end
    end
end

function NerfPros.OnCreatePlayer(playerIdx, player)
    NerfPros.CurrentInjuries = NerfPros.CatalogCurrentInjuries(player)
end

local function InventoryContextMenu(player, context, items)
    local playerObj = getSpecificPlayer(player)
    items = ISInventoryPane.getActualItems(items)
    for _, item in ipairs(items) do
        for _, restrictedItem in ipairs(restrictedItems) do
            if item and item:getFullType() == restrictedItem then
                context:removeOptionByName(getText("ContextMenu_Unequip"))
                context:removeOptionByName("Trash")
                context:removeOptionByName(getText("ContextMenu_Drop"))
                break
            end
        end
        for _, prosthetic in ipairs(prosthetics) do
            if prosthetic == item:getFullType() then
                local texture = item:getTex():getName()
                if item:getCondition() == 10 then
                    WL_ContextMenuUtils.missingRequirement(context, "Repair Prosthetic", "Prosthetic is in perfect condition", nil, texture)
                else
                    local firstAid = playerObj:getPerkLevel(Perks.Doctor)
                    local metalWorking = playerObj:getPerkLevel(Perks.MetalWelding)
                    if firstAid >= 6 and metalWorking >= 4 then
                        if playerObj:getInventory():contains("Base.DuctTape") then
                            if playerObj:getInventory():contains(prosthetic) then
                                local ductTape = playerObj:getInventory():FindAndReturn("Base.DuctTape")
                                local option = context:addOption("Repair Prosthetic", playerObj, function() ISTimedActionQueue.add(NerfProsRepair:new(playerObj, item, ductTape)) end, item)
                                WL_ContextMenuUtils.addToolTip(option, "Repair Prosthetic", "Requires 1 unit of Duct Tape", texture)
                            else
                                WL_ContextMenuUtils.missingRequirement(context, "Repair Prosthetic", "Prosthetic must be in inventory to repair", nil, texture)
                            end
                        else
                            WL_ContextMenuUtils.missingRequirement(context, "Repair Prosthetic", "Requires 1 unit of Duct Tape", nil, texture)
                        end
                    else
                        WL_ContextMenuUtils.missingRequirement(context, "Repair Prosthetic", "Requires Doctor 6 and Metalworking 4", nil, texture)
                    end
                end
            end
        end
    end
end

Events.OnTick.Add(onTick)
Events.OnEquipPrimary.Add(onEquipAttempt)
Events.OnEquipSecondary.Add(onEquipAttempt)
Events.OnPlayerUpdate.Add(NerfPros.checkPlayer)
Events.OnFillInventoryObjectContextMenu.Add(InventoryContextMenu)