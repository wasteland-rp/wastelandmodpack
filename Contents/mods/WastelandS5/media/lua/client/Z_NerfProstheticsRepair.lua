---
--- Z_NerfProstheticsRepair.lua
--- 04-Jan-2025
---

NerfProsRepair = ISBaseTimedAction:derive("NerfProsRepair")

function NerfProsRepair:isValid()
    return true
end

function NerfProsRepair:start()
    self:setActionAnim(CharacterActionAnims.Craft)
end

function NerfProsRepair:stop()
    ISBaseTimedAction.stop(self)
end

function NerfProsRepair:perform()
    local condition = self.prosthetic:getCondition()
    if (self.prosthetic:getCondition() + 3) >= 10 then
        self.prosthetic:setCondition(10)
    else
        self.prosthetic:setCondition(self.prosthetic:getCondition() + 3)
    end
    self.repairItem:setUsedDelta(self.repairItem:getUsedDelta() - 0.25)
    triggerEvent("OnClothingUpdated", self.character)
    ISBaseTimedAction.perform(self)
end

function NerfProsRepair:new(character, prosthetic, repairItem)
    local o = {}
    setmetatable(o, self)
    self.__index = self
    o.character = character
    o.prosthetic = prosthetic
    o.repairItem = repairItem
    o.stopOnWalk = true
    o.stopOnRun = true
    o.maxTime = 150
    return o
end