if not LiqudTanker then LiqudTanker = {} end

LiqudTanker.LiquidTypes = {
	["Water"] = true,
	["Gasoline"] = true,
}

if not LiqudTanker.old_ISVehicleMenu_FillPartMenu then
	LiqudTanker.old_ISVehicleMenu_FillPartMenu = ISVehicleMenu.FillPartMenu
end

function ISVehicleMenu.FillPartMenu(playerIndex, context, slice, vehicle)
	local playerObj = getSpecificPlayer(playerIndex)
	local typeToItem = VehicleUtils.getItems(playerIndex)
	local fuel_truck_source = FindVehicleGas(playerObj, vehicle)
	for i = 1, vehicle:getPartCount() do
		local part = vehicle:getPartByIndex(i - 1)
		if part:isContainer() and part:getContainerContentType() == "Gasoline Storage" then
			if (typeToItem["Base.PetrolCan"] or typeToItem["Base.JerryCan"]) and part:getContainerContentAmount() < part:getContainerCapacity() then
				if slice then
					slice:addSlice(getText("ContextMenu_Add_Gasoline_To_Gasoline_Storage_Tank"), getTexture("Item_Petrol"), ISVehiclePartMenu.onAddGasoline, playerObj, part)
				else
					context:addOption(getText("ContextMenu_Add_Gasoline_To_Gasoline_Storage_Tank"), playerObj, ISVehiclePartMenu.onAddGasoline, part)
				end
			end
			if part:getContainerContentAmount() > 0 then
				if ISVehiclePartMenu.getGasCanNotFull(playerObj, typeToItem) then
					if slice then
						slice:addSlice(getText("ContextMenu_Remove_Gasoline_From_Gasoline_Storage_Tank"), getTexture("Item_Petrol"), ISVehiclePartMenu.onTakeGasoline, playerObj, part)
					else
						context:addOption(getText("ContextMenu_Remove_Gasoline_From_Gasoline_Storage_Tank"), playerObj, ISVehiclePartMenu.onTakeGasoline, part)
					end
				else
					if slice then
						-- Empty placeholder, as the original logic left this empty
					else
						context:addOption(getText("ContextMenu_For_get_Gasoline_Need_Gas_Can"), nil, nil, nil)
					end
				end
			end
			-- DISABLED DUE TO DUPE BUG
			-- local fuelStation = LiqudTanker.getNearbyFuelPumpForTank(vehicle)
			-- if fuelStation then
			-- 	local square = fuelStation:getSquare()
			-- 	if square and ((SandboxVars.AllowExteriorGenerator and square:haveElectricity()) or (SandboxVars.ElecShutModifier > -1 and GameTime:getInstance():getNightsSurvived() < SandboxVars.ElecShutModifier)) then
			-- 		if square and part:getContainerContentAmount() < part:getContainerCapacity() then
			-- 			if slice then
			-- 				slice:addSlice(getText("ContextMenu_Fill_Gasoline_Storage_Tank_From_Pump"), getTexture("media/ui/vehicles/vehicle_refuel_from_pump.png"), LiqudTanker.onPumpGasolineForTank, playerObj, part)
			-- 			else
			-- 				context:addOption(getText("ContextMenu_Fill_Gasoline_Storage_Tank_From_Pump"), playerObj, LiqudTanker.onPumpGasolineForTank, part)
			-- 			end
			-- 		end
			-- 	end
			-- end
			-- if fuel_truck_source and fuel_truck_source:getContainerContentAmount() > 0 and part:getContainerContentAmount() < part:getContainerCapacity() then
			-- 	if slice then
			-- 		slice:addSlice(getText("ContextMenu_Fill_Gasoline_Storage_Tank_From_Fuel_Truck"), getTexture("media/ui/vehicles/vehicle_gas_hose.png"), ISVehiclePartMenu.onPumpGasolineFromTruck, playerObj, part, fuel_truck_source)
			-- 	else
			-- 		context:addOption(getText("ContextMenu_Fill_Gasoline_Storage_Tank_From_Fuel_Truck"), playerObj, ISVehiclePartMenu.onPumpGasolineFromTruck, part, fuel_truck_source)
			-- 	end
			-- end
		end

		-- DISABLED DUE TO DUPE BUG
		-- if not vehicle:isEngineStarted() and part:isContainer() and part:getContainerContentType() == "Gasoline" then
		-- 	if fuel_truck_source and fuel_truck_source:getContainerContentAmount() > 0 and part:getContainerContentAmount() < part:getContainerCapacity() then
		-- 		if slice then
		-- 			slice:addSlice(getText("ContextMenu_Remove_Gasoline_From_Fuel_Truck"), getTexture("media/ui/vehicles/vehicle_gas_hose.png"), ISVehiclePartMenu.onPumpGasolineFromTruck, playerObj, part, fuel_truck_source)
		-- 		else
		-- 			context:addOption(getText("ContextMenu_Remove_Gasoline_From_Fuel_Truck"), playerObj, ISVehiclePartMenu.onPumpGasolineFromTruck, part, fuel_truck_source)
		-- 		end
		-- 	end
		-- end

		if part:isContainer() and part:getContainerContentType() == "Propane Storage" then
			if ISVehiclePartMenu.getPropaneTankNotEmpty(playerObj, typeToItem)
			and part:getContainerContentAmount() < part:getContainerCapacity() then
				if slice then
					slice:addSlice(getText("Add Propane To Propane Storage Tank"), getTexture("Item_PropaneTank"), ISVehiclePartMenu.onAddPropane, playerObj, part)
				else
					context:addOption(getText("Add Propane To Propane Storage Tank"), playerObj,ISVehiclePartMenu.onAddPropane, part)
				end
			end
			if ISVehiclePartMenu.getPropaneTankNotFull(playerObj, typeToItem)
			and part:getContainerContentAmount() > 0 then
				if slice then
					slice:addSlice(getText("Remove Propane From Propane Storage Tank"), getTexture("Item_PropaneTank"), ISVehiclePartMenu.onTakePropane, playerObj, part)
				else
					context:addOption(getText("Remove Propane From Propane Storage Tank"), playerObj, ISVehiclePartMenu.onTakePropane, part)
				end
			end		
		end			
	end
	LiqudTanker.old_ISVehicleMenu_FillPartMenu(playerIndex, context, slice, vehicle)
end

originalOnPumpGasolineFromTruck = ISVehiclePartMenu.onPumpGasolineFromTruck

function ISVehiclePartMenu.onPumpGasolineFromTruck(playerObj, part, source_Tank)
	if not getActivatedMods():contains("AnotherVehicleClaimSystemB41") then
		return originalOnPumpGasolineFromTruck(playerObj, part, source_Tank)
	else

		local checkResult = AVCS.getPublicPermission(source_Tank:getVehicle(), "AllowSiphonFuel")
		if checkResult then
			return originalOnPumpGasolineFromTruck(playerObj, part, source_Tank)
		end

		checkResult = AVCS.checkPermission(playerObj, source_Tank:getVehicle())
		checkResult = AVCS.getSimpleBooleanPermission(checkResult)
		if checkResult then
			return originalOnPumpGasolineFromTruck(playerObj, part, source_Tank)
		end
		
		playerObj:setHaloNote(getText("IGUI_AVCS_Vehicle_No_Permission"), 250, 250, 250, 300)
		local temp = {
			ignoreAction = true
		}
		return temp
	end
end