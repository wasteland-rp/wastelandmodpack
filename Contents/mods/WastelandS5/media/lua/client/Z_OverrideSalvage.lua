if AVCS and ISVehicleSalvage then
    local original_ISVehicleSalvage_isValid = ISVehicleSalvage.isValid

    function ISVehicleSalvage:isValid()
        if not original_ISVehicleSalvage_isValid(self) then
            return false
        end
        local permission = AVCS.checkPermission(self.character, self.vehicle)
        return type(permission) == "boolean" and permission
    end
end

if ISVehicleSalvage then
    function ISVehicleSalvage:perform()
        if self.sound ~= 0 then
            self.character:getEmitter():stopSound(self.sound)
        end
        local totalXp = 10;
        for i=1,math.max(6,self.character:getPerkLevel(Perks.MetalWelding)) do
            if self:checkAddItem("ElectronicsScrap", 15) then totalXp = totalXp + 2 end;
            if self:checkAddItem("ElectronicsScrap", 15) then totalXp = totalXp + 2 end;
            if self:checkAddItem("LightBulb", 20) then totalXp = totalXp + 2 end;
            if self:checkAddItem("Radio.ElectricWire", 15) then totalXp = totalXp + 2 end;

            if self:checkAddItem("EngineParts", 25 - self.character:getPerkLevel(Perks.Mechanics)) then totalXp = totalXp + 3 end;

            if self:checkAddItem("LeatherStripsDirty", 15) then totalXp = totalXp + 2 end;
            if self:checkAddItem("RippedSheetsDirty", 15) then totalXp = totalXp + 2 end;

            if self:checkAddItem("MetalBar", 12) then totalXp = totalXp + 2 end;
            if self:checkAddItem("MetalBar", 15) then totalXp = totalXp + 2 end;
            if self:checkAddItem("MetalBar", 20) then totalXp = totalXp + 2 end;
            if self:checkAddItem("MetalBar", 20) then totalXp = totalXp + 2 end;

            if self:checkAddItem("MetalPipe", 12) then totalXp = totalXp + 2 end;
            if self:checkAddItem("MetalPipe", 15) then totalXp = totalXp + 2 end;
            if self:checkAddItem("MetalPipe", 20) then totalXp = totalXp + 2 end;
            if self:checkAddItem("MetalPipe", 20) then totalXp = totalXp + 2 end;

            if self:checkAddItem("SheetMetal", 12) then totalXp = totalXp + 2 end;
            if self:checkAddItem("SheetMetal", 15) then totalXp = totalXp + 2 end;
            if self:checkAddItem("SheetMetal", 20) then totalXp = totalXp + 2 end;
            if self:checkAddItem("SheetMetal", 20) then totalXp = totalXp + 2 end;

            if self:checkAddItem("SmallSheetMetal", 20) then totalXp = totalXp + 2 end;
            if self:checkAddItem("SmallSheetMetal", 25) then totalXp = totalXp + 2 end;

            if self:checkAddItem("ScrapMetal", 18) then totalXp = totalXp + 2 end;
            if self:checkAddItem("ScrapMetal", 18) then totalXp = totalXp + 2 end;
            if self:checkAddItem("ScrapMetal", 18) then totalXp = totalXp + 2 end;
            if self:checkAddItem("ScrapMetal", 18) then totalXp = totalXp + 2 end;

            if self:checkAddItem("Screws", 12) then totalXp = totalXp + 2 end;
            if self:checkAddItem("Screws", 12) then totalXp = totalXp + 2 end;
            if self:checkAddItem("Screws", 12) then totalXp = totalXp + 2 end;
            if self:checkAddItem("Wire", 20) then totalXp = totalXp + 2 end;
        end
        for i=1,10 do   --Propane uses--
            self.item:Use();
        end
        self.character:getXp():AddXP(Perks.MetalWelding, totalXp);
        sendClientCommand(self.character, "vehicle", "remove", { vehicle = self.vehicle:getId() })
        self.item:setJobDelta(0);
        -- needed to remove from queue / start next.
        ISBaseTimedAction.perform(self)
    end
end