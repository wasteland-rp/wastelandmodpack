function SC_Board.Permissions.IsDeletePossible()
    return WL_Utils.isStaff(getPlayer())
end

function SC_Board.Permissions.IsCreatePossible()
    return WL_Utils.isStaff(getPlayer())
end