local items = {
    "Base.PropaneTank",
    "Base.BlowTorch",
}

local sm = getScriptManager()

for _, item in ipairs(items) do
    local itemScript = sm:getItem(item)
    if itemScript then
        print("Adding consolidate option to " .. item)
        itemScript:DoParam("CantBeConsolided = false")
        itemScript:DoParam("ConsolidateOption = ContextMenu_Merge")
    else
        print("Failed to find item for consolidate " .. item)
    end
end