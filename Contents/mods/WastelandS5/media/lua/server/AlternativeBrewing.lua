local function isPetrolCan(item)
    return item:getType() == "EmptyPetrolCan" or item:getType() == "PetrolCan"
end

local function isJerryCan(item)
    return item:getType() == "EmptyJerryCan" or item:getType() == "JerryCan"
end

function Test_OpenEthanolBrewBarrelIntoCans(item)
    if isPetrolCan(item) then
        local player = getPlayer()
        local allPetrolCans = player:getInventory():getAllEval(isPetrolCan)
        local spaceAvailable = 0
        for i=0, allPetrolCans:size()-1 do
            local petrolCan = allPetrolCans:get(i)
            if petrolCan:getType() == "EmptyPetrolCan" then
                spaceAvailable = spaceAvailable + 8
            else
                spaceAvailable = spaceAvailable + (1-petrolCan:getUsedDelta())/petrolCan:getUseDelta()
            end
        end
        return spaceAvailable >= 10
    end
    return true
end

function OnCreate_OpenEthanolBrewBarrelIntoCans(items, result, player)
    local allPetrolCans = player:getInventory():getAllEval(isPetrolCan)
    local unitsRemaining = 10
    for i=0, allPetrolCans:size()-1 do
        local petrolCan = allPetrolCans:get(i)
        if petrolCan:getType() == "EmptyPetrolCan" then
            player:getInventory():Remove(petrolCan)
            local replacementCan = player:getInventory():AddItem("PetrolCan")
            local units = math.min(unitsRemaining, 8)
            replacementCan:setUsedDelta(units*replacementCan:getUseDelta())
            unitsRemaining = unitsRemaining - units
        else
            local spaceRemaining = (1-petrolCan:getUsedDelta())/petrolCan:getUseDelta()
            local units = math.min(unitsRemaining, spaceRemaining)
            if units > 0 then
                petrolCan:setUsedDelta(petrolCan:getUsedDelta() + units*petrolCan:getUseDelta())
                unitsRemaining = unitsRemaining - units
            end
        end
        if unitsRemaining <= 0 then
            break
        end
    end
end

function Test_OpenEthanolBrewBarrelIntoJerry(item)
    if isJerryCan(item) then
        local player = getPlayer()
        local allJerryCans = player:getInventory():getAllEval(isJerryCan)
        local spaceAvailable = 0
        local maxCapacity = 20

        for i = 0, allJerryCans:size() - 1 do
            local jerryCan = allJerryCans:get(i)
            if jerryCan:getType() == "EmptyJerryCan" then
                spaceAvailable = spaceAvailable + maxCapacity
            else
                spaceAvailable = spaceAvailable + (1 - jerryCan:getUsedDelta()) * maxCapacity
            end
        end

        local requiredUnits = 12.5
        return spaceAvailable >= requiredUnits
    end
    return true
end


function OnCreate_OpenEthanolBrewBarrelIntoJerry(items, result, player)
    local allJerryCans = player:getInventory():getAllEval(isJerryCan)
    local unitsToFill = 12.5
    local maxCapacity = 20

    for i = 0, allJerryCans:size() - 1 do
        local jerryCan = allJerryCans:get(i)
        if jerryCan:getType() == "EmptyJerryCan" then
            player:getInventory():Remove(jerryCan)
            local replacementCan = player:getInventory():AddItem("JerryCan")
            replacementCan:setUsedDelta(unitsToFill / maxCapacity)
            break
        else
            local spaceAvailable = (1 - jerryCan:getUsedDelta()) * maxCapacity
            local units = math.min(unitsToFill, spaceAvailable)
            if units > 0 then
                local newUsedDelta = jerryCan:getUsedDelta() + (units / maxCapacity)
                jerryCan:setUsedDelta(newUsedDelta)
                unitsToFill = unitsToFill - units
            end
            if unitsToFill <= 0 then
                break
            end
        end
    end
end

