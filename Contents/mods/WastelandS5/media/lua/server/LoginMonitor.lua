Events.OnClientCommand.Add(function(module, command, player, args)
    if module ~= 'LoginMonitor' then return end

    if command == 'OverTime' then
        local file = getFileWriter('loginbans.txt', true, true)
        file:writeln(player:getUsername())
        file:close()
        writeLog("user", player:getUsername() .. " took too long to login")
    end
end)