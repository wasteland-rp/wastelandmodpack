if not getActivatedMods():contains("amclub") then
    return
end

local sm = getScriptManager()
local tweaks = "{ wheel FrontRight { front = true, offset = -0.2706 0.2588 1.4706, radius = 0.3f, width = 0.5f, } wheel FrontLeft { front = true, offset = 0.2706 0.2588 1.4706, radius = 0.3f, width = 0.5f, } }"

local AMC_bmw_classic = sm:getVehicle("AMC_bmw_classic")
if not AMC_bmw_classic then
    print("AMC_bmw_classic not found")
else
    AMC_bmw_classic:Load(AMC_bmw_classic:getName(), tweaks)
end

local AMC_bmw_custom = sm:getVehicle("AMC_bmw_custom")
if not AMC_bmw_custom then
    print("AMC_bmw_custom not found")
else
    AMC_bmw_custom:Load(AMC_bmw_custom:getName(), tweaks)
end

local AMC_harley = sm:getVehicle("AMC_harley")
if not AMC_harley then
    print("AMC_harley not found")
else
    AMC_harley:Load(AMC_harley:getName(), tweaks)
end