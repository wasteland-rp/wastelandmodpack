if not getActivatedMods():contains("RidingMower") then
    return
end

local sm = getScriptManager()
local tweaks = "{ steeringIncrement = 0.5 , steeringClamp = 1 , gearRatioR = 0.75 }"

local RidingMower = sm:getVehicle("RidingMower")
if not RidingMower then
    print("RidingMower not found")
else
    RidingMower:Load(RidingMower:getName(), tweaks)
end

local MiniMower = sm:getVehicle("MiniMower")
if not MiniMower then
    print("MiniMower not found")
else
    MiniMower:Load(MiniMower:getName(), tweaks)
end

local RacingMower = sm:getVehicle("RacingMower")
if not RacingMower then
    print("RacingMower not found")
else
    RacingMower:Load(RacingMower:getName(), tweaks)
end