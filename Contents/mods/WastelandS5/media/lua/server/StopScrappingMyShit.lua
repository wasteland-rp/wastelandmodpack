function StopScrappingMyShitTest(item)
    if item:getType() == "MetalWorkbench" then
        return true
    end
    local player = getPlayer()
    if not player:getInventory():contains(item) then
        return false
    end
    if item:isEquipped() then
        return false
    end
    if item:getAttachedSlot() >= 0 then
        return false
    end
    if item:isFavorite() then
        return false
    end
    return true
end

local recipes = {
    "TW.Deconstruct Car Battery",
    "TW.Deconstruct Car Suspension",
    "TW.Deconstruct Car Brake",
    "TW.Deconstruct Car Door",
    "TW.Deconstruct Car Gas Tank",
    "TW.Deconstruct Car Muffler",
    "TW.Deconstruct Car Wheel",
    "TW.Deconstruct Car Seat",
    "TW.Disassamble Metal Armor",
    "TW.Disassamble Gun",
    "TW.Disassemble Gun Magazine",
    "TW.Disassemble Motor",
    "TW.Deconstruct Small Items",
    "TW.Deconstruct Medium Items",
    "TW.Deconstruct Large Items",
    "TW.Disassemble Battery Powered Tool",
    "TW.Disassemble Golfclub",
    "TW.Disassemble Hammer",
    "TW.Disassemble Sledgehammer",
    "TW.Disassemble Axe",
    "TW.Disassemble SmallBlade",
    "TW.Disassemble Screwdriver",
    "TW.Disassemble MeatCleaver",
    "TW.Disassemble Scythe",
    "TW.Disassemble LongBlade",
    "TW.Disassemble Shovel",
    "TW.Disassemble Hoe",
    "TW.Disassemble Pickaxe",
    "TW.Disassemble Rake",
    "TW.Disassemble Fork",
    "TW.Deconstruct Crowbar",
    "TW.Deconstruct Pan",
    "TW.Deconstruct Fishing Rod",
    "TW.Deconstruct Golf Club Head",
    "TW.Deconstruct Hammer Head",
    "TW.Deconstruct SledgeHammer Head",
    "TW.Deconstruct Axe Head",
    "TW.Deconstruct SmallBlade",
    "TW.Deconstruct Small Metal Bar",
    "TW.Deconstruct Meat Cleaver Head",
    "TW.Deconstruct Scythe Head",
    "TW.Deconstruct Long Blade Head",
    "TW.Deconstruct Wrench",
    "TW.Deconstruct Shovel Head",
    "TW.Deconstruct Hoe Head",
    "TW.Deconstruct Pickaxe Head",
    "TW.Deconstruct Rake Head",
    "TW.Deconstruct BarBell",
    "TW.Deconstruct DumbBell",
    "TW.Deconstruct Scalpel",
    "TW.Deconstruct Fork Head",
    "TW.Deconstruct Tool Box",
}

local sm = getScriptManager()
for _, recipeName in ipairs(recipes) do
    local recipe = sm:getRecipe(recipeName)
    if recipe then
        print("StopScrappingMyShitTest: Adding to " .. recipeName)
        recipe:setLuaTest("StopScrappingMyShitTest")
    else
        print("StopScrappingMyShitTest: Recipe not found: " .. recipeName)
    end
end