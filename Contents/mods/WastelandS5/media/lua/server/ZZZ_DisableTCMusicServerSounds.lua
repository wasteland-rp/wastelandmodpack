if isClient() then return end

if getActivatedMods():contains("truemusic") then
    require "TCMusicPlay"
    Events.OnTick.Remove(OnTickServerCheckMusic)
end