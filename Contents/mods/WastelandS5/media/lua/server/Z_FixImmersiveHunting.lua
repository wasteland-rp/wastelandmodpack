Events.EveryOneMinute.Remove(SIHOneMinuteDropThatBeat)
if isServer() then return true end
function DropTraceSmall(player)
    local items = player:getInventory():getAllTypeRecurse("SIHTraceSmall")
    for i=items:size()-1,0,-1 do
        local item = items:get(i)
        if item then
            item:getContainer():Remove(item)
            player:getCurrentSquare():AddWorldInventoryItem(InventoryItemFactory.CreateItem("ImmersiveHunting.SIHTrackSmall"), 0.0, 0.0, 0.0)
        end
    end
end
function DropTraceBig(player)
    local items = player:getInventory():getAllTypeRecurse("SIHTraceBig")
    for i=items:size()-1,0,-1 do
        local item = items:get(i)
        if item then
            item:getContainer():Remove(item)
            player:getCurrentSquare():AddWorldInventoryItem(InventoryItemFactory.CreateItem("ImmersiveHunting.SIHTrackBig"), 0.0, 0.0, 0.0)
        end
    end
end
function DropBirdSighting(player)
    local items = player:getInventory():getAllTypeRecurse("SIHSpottedBird")
    for i=items:size()-1,0,-1 do
        local item = items:get(i)
        if item then
            item:getContainer():Remove(item)
            player:getCurrentSquare():AddWorldInventoryItem(InventoryItemFactory.CreateItem("ImmersiveHunting.SIHTrackBird"), 0.0, 0.0, 0.0)
        end
    end
end
function SIHOneMinuteDropThatBeat()
    local player = getPlayer()
    if not player then return end
    if player:isDead() then return end
    DropTraceSmall(player)
    DropTraceBig(player)
    DropBirdSighting(player)
end
Events.EveryOneMinute.Add(SIHOneMinuteDropThatBeat)