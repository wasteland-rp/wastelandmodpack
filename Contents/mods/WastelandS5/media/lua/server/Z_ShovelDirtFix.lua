-- Makes it so you can shovel dirt from the same square without grossing up the world.
local ISShovelGroundCursor_GetDirtGravelSand = ISShovelGroundCursor.GetDirtGravelSand
function ISShovelGroundCursor.GetDirtGravelSand(square)
	for i=1,square:getObjects():size() do
		local obj = square:getObjects():get(i-1)
        if not isServer() and CFarmingSystem.instance:getLuaObjectOnSquare(square) then
		elseif obj:getSprite() and obj:getSprite():getName() then
			local spriteName = obj:getSprite():getName()
			if luautils.stringStarts(spriteName, "blends_natural_01_") or
					luautils.stringStarts(spriteName, "floors_exterior_natural") then
				return "dirt",obj
			end
		end
	end
	return ISShovelGroundCursor_GetDirtGravelSand(square)
end