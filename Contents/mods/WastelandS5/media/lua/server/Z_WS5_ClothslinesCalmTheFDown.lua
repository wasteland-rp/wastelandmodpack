if CL and CL.UpdateSprite then
    -- Change from EveryOneMinute to EveryHours for clothsline sprite update
    Events.EveryOneMinute.Remove(CL.UpdateSprite)
    Events.EveryHours.Add(CL.UpdateSprite)
end