require 'Items/SuburbsDistributions'
require 'Items/ProceduralDistributions'
require 'Vehicles/VehicleDistributions'

local divisor = 50
print("WLS5 - Reducing DrugMod rates")

for area,areaDist in pairs(SuburbsDistributions) do
    if type(areaDist) == "table" and areaDist.items then
        local numItems = #areaDist.items
        for i = 1, numItems, 2 do
            if type(areaDist.items[i]) ~= "string" then
                print("Broken entry in "..area..": "..i.." "..tostring(areaDist.items[i]))
            else
                if areaDist.items[i]:sub(1, 8) == "DrugMod."then
                    if areaDist.items[i+1] and type(areaDist.items[i+1]) == "number" then
                        local newRate = areaDist.items[i+1]/divisor
                        print("Area: " .. area .. " - Reducing "..areaDist.items[i].." from "..areaDist.items[i+1].." to "..newRate)
                        areaDist.items[i+1] = newRate
                    else
                        print("Broken DrugMod entry in "..area..": "..areaDist.items[i].." "..tostring(areaDist.items[i+1]))
                    end
                end
            end
        end
    elseif type(areaDist) == "table" then
        for room, roomDist in pairs(areaDist) do
            if type(roomDist) == "table" and roomDist.items then
                local numItems = #roomDist.items
                for i = 1, numItems, 2 do
                    if type(roomDist.items[i]) ~= "string" then
                        print("Broken entry in "..area.." "..room..": "..i.." "..tostring(roomDist.items[i]))
                    else
                        if roomDist.items[i]:sub(1, 8) == "DrugMod." then
                            if roomDist.items[i+1] and type(roomDist.items[i+1]) == "number" then
                                local newRate = roomDist.items[i+1]/divisor
                                print("Area: " .. area .. " Room: " .. room .. " - Reducing "..roomDist.items[i].." from "..roomDist.items[i+1].." to "..newRate)
                                roomDist.items[i+1] = newRate
                            else
                                print("Broken DrugMod entry in "..area.." "..room..": "..roomDist.items[i].." "..tostring(roomDist.items[i+1]))
                            end
                        end
                    end
                end
            end
        end
    end
end

for container, containerDist in pairs(ProceduralDistributions["list"]) do
    if containerDist.items then
        local numItems = #containerDist.items
        for i = 1, numItems, 2 do
            if type(containerDist.items[i]) == "string" and containerDist.items[i]:sub(1, 8) == "DrugMod." then
                if containerDist.items[i+1] and type(containerDist.items[i+1]) == "number" then
                    local newRate = containerDist.items[i+1]/divisor
                    print("Container: " .. container .. " - Reducing "..containerDist.items[i].." from "..containerDist.items[i+1].." to "..newRate)
                    containerDist.items[i+1] = newRate
                else
                    print("Broken DrugMod entry in "..container..": "..containerDist.items[i].." "..tostring(containerDist.items[i+1]))
                end
            end
        end
    end
end

for container, containerDist in pairs(VehicleDistributions) do
    if containerDist.items then
        local numItems = #containerDist.items
        for i = 1, numItems, 2 do
            if type(containerDist.items[i]) == "string" and containerDist.items[i]:sub(1, 8) == "DrugMod." then
                if containerDist.items[i+1] and type(containerDist.items[i+1]) == "number" then
                    local newRate = containerDist.items[i+1]/divisor
                    print("Vehicle Container: " .. container .. " - Reducing "..containerDist.items[i].." from "..containerDist.items[i+1].." to "..newRate)
                    containerDist.items[i+1] = newRate
                else
                    print("Broken DrugMod entry in "..container..": "..containerDist.items[i].." "..tostring(containerDist.items[i+1]))
                end
            end
        end
    end
end