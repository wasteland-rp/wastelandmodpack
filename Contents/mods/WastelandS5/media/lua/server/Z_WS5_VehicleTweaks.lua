function Vehicles.Update.Heater(vehicle, part, elapsedMinutes)
	if not Vehicles.elaspedMinutesForHeater[vehicle:getId()] then
		Vehicles.elaspedMinutesForHeater[vehicle:getId()] = 0
	end
	local pc = vehicle:getPartById("PassengerCompartment")
	local engine = vehicle:getPartById("Engine")
	if not pc or not engine then return end
	local pcData = pc:getModData()
	if not tonumber(pcData.temperature) then
		pcData.temperature = 0.0
	end
	local partData = part:getModData()
	if not tonumber(partData.temperature) then
		partData.temperature = 0
	end
	local tempInc = partData.temperature > 0 and 0.5 + (math.min(engine:getModData().temperature / 100, 0.7)) or 2.0
	local previousTemp = pcData.temperature

    -- to heat, engine needs to be warm, to cool, doesn't matter
    local isEngineReady = partData.temperature > 0 and engine:getModData().temperature > 20 or partData.temperature < 0
    local isValidTemp = (partData.temperature > 0 and pcData.temperature <= partData.temperature) or (partData.temperature < 0 and pcData.temperature >= partData.temperature)

	if partData.active and vehicle:isEngineRunning() and isEngineReady and isValidTemp then
		if partData.temperature > 0 then
			pcData.temperature = math.min(pcData.temperature + tempInc * elapsedMinutes, partData.temperature)
		else
			pcData.temperature = math.max(pcData.temperature - tempInc * elapsedMinutes, partData.temperature)
		end
		if partData.temperature > 0 and pcData.temperature > partData.temperature then
			pcData.temperature = partData.temperature
		end
		if partData.temperature < 0 and pcData.temperature < partData.temperature then
			pcData.temperature = partData.temperature
		end
	else
		if pcData.temperature > 0 then
			pcData.temperature = math.max(pcData.temperature - 0.1 * elapsedMinutes, 0)
		else
			pcData.temperature = math.min(pcData.temperature + 0.1 * elapsedMinutes, 0)
		end
	end

    -- Don't use battery for heater...
    -- if partData.active and vehicle:isEngineRunning() then
	-- 	VehicleUtils.chargeBattery(vehicle, -0.000035 * elapsedMinutes)
	-- end

    Vehicles.elaspedMinutesForHeater[vehicle:getId()] = Vehicles.elaspedMinutesForHeater[vehicle:getId()] + elapsedMinutes
	if isServer() and VehicleUtils.compareFloats(previousTemp, pcData.temperature, 2) and Vehicles.elaspedMinutesForHeater[vehicle:getId()] > 1 then
		Vehicles.elaspedMinutesForHeater[vehicle:getId()] = 0
		vehicle:transmitPartModData(pc)
	end
end

if ISVehicleACUI then
	function ISVehicleACUI:addKnobValues()
		self.tempKnob:addValue(0, 0);

		self.tempKnob:addValue(15, 5);
		self.tempKnob:addValue(30, 10);
		self.tempKnob:addValue(45, 15);
		self.tempKnob:addValue(60, 20);
		self.tempKnob:addValue(75, 25);
		self.tempKnob:addValue(90, 30);

		self.tempKnob:addValue(270, -35);
		self.tempKnob:addValue(285, -30);
		self.tempKnob:addValue(300, -25);
		self.tempKnob:addValue(315, -20);
		self.tempKnob:addValue(330, -15);
		self.tempKnob:addValue(345, -10);
	end
end
