require "VehicleZoneDistribution"

if not VehicleZoneDistribution then return end

for zoneId, data in pairs(VehicleZoneDistribution) do
    if data.vehicles["Base.TrailerGenerator"] then
        data.vehicles["Base.TrailerGenerator"] = nil
    end
    if data.vehicles["Base.generallee"] then
        data.vehicles["Base.generallee"] = nil
    end
    if data.vehicles["Base.generalmeh"] then
        data.vehicles["Base.generalmeh"] = nil
    end
end