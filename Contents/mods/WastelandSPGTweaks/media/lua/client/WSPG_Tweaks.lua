local stateMap, CheckAndUpdateSprite = unpack(require 'SteamGeneratorLEDs')

-- Override to remove logs and reduce the effectivness of charcoal as fuel for the steam generator
function ISSteamGeneratorAddFuel:perform()
	ISBaseTimedAction.perform(self)
	self.item:setJobDelta(0)
	self.generator:setFuel(self.generator:getFuel() + 1)
	self.character:removeFromHands(self.item)
	local inv = self.character:getInventory()
	if self.item:getName():find('Charcoal') then
		self.item:Use()
	else
		inv:Remove(self.item)
	end
	CheckAndUpdateSprite(self.generator)
	local nextLog = inv:getFirstTypeRecurse('Base.Charcoal') or inv:getFirstTypeRecurse('Base.Log')
	if nextLog then
		ISTimedActionQueue.add(ISSteamGeneratorAddFuel:new(self.character, self.generator, nextLog, self.maxTime))
	end
end

-- Override to make the generator need a lot more water to run
function ISSteamGeneratorAddWater:start()
	self.item:setJobType(getText("ContextMenu_AddWaterFromItem"))
	self.item:setJobDelta(0)
	self.itemFromStartDelta = self.item:getUsedDelta()
	local waterAvailable = self.item:getUsedDelta() / self.item:getUseDelta()
	waterAvailable = (waterAvailable - math.floor(waterAvailable) > 0.99) and math.ceil(waterAvailable) or waterAvailable
	local modData = self.generator:getModData()
	local water = modData.water or 0
	local destCapacity = 100 - water
	self.addUnits = math.min(destCapacity*10, waterAvailable)
	self.action:setTime(math.max(6, self.addUnits) * 7)
	self.sound = self.character:playSound("PourWaterIntoObject")
	
	self:setActionAnim('Loot')
	self.character:SetVariable('LootPosition', 'High')
	self.character:reportEvent('EventTakeWater')
end

function ISSteamGeneratorAddWater:stop()
	self:stopSound()
	self.item:setJobDelta(0)
	if self.addUnits and self.addUnits > 0 then
		local unitsSoFar = math.floor((self.addUnits) * self:getJobDelta())
		self.item:setUsedDelta(self.itemFromStartDelta - (unitsSoFar/10) * self.item:getUseDelta())
		if self.item:getUsedDelta() < 0.0001 then
			self.item:Use()
		end
		local modData = self.generator:getModData()
		modData.water = math.min(100, modData.water + (unitsSoFar/10))
		self.generator:transmitModData()
	end
	ISBaseTimedAction.stop(self)
end

function ISSteamGeneratorAddWater:perform()
	self:stopSound()
	self.item:getContainer():setDrawDirty(true)
	self.item:setJobDelta(0)
	self.item:setUsedDelta(self.itemFromStartDelta - self.addUnits * self.item:getUseDelta())
	if self.item:getUsedDelta() < 0.0001 then
		self.item:Use()
	end
	local modData = self.generator:getModData()
	modData.water = math.min(100, (modData.water or 0) + (self.addUnits/10))
	self.generator:transmitModData()
	ISBaseTimedAction.perform(self)
end