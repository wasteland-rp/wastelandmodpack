---
--- WL_ISFactionUI_Override.lua
--- 12/02/2024
---
---
require "ISUI/UserPanel/ISFactionUI"

local factionUiRefresh = ISFactionUI.updateButtons

function ISFactionUI:updateButtons()
	factionUiRefresh(self)

	if self.faction:isMember(self.player:getUsername()) then
		self.addPlayer.enable = true -- Enable the Add Player button for anyone in the faction
	end
end
