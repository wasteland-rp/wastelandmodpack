local MAX_CHECKS = 120
local safehousesToCheck = {}
local function checkPendingSafehouses()
    local itemsToKeep = {}
    for _,safehouseData in pairs(safehousesToCheck) do
        local safehouse = SafeHouse.getSafeHouse(safehouseData.x,safehouseData.y,safehouseData.w,safehouseData.h)
        if safehouse and safehouse:getOwner() == safehouseData.temp_owner_name then
            safehouse:setOwner(safehouseData.player:getUsername())
            sendServerCommand(safehouseData.player, "WastelandSafehouses", "SyncSuccess", {safehouseData.x,safehouseData.y,safehouseData.w,safehouseData.h,safehouseData.temp_owner_name})
        else
            safehouseData.checks = safehouseData.checks + 1
            if safehouseData.checks < MAX_CHECKS then
                table.insert(itemsToKeep, safehouseData)
            else
                sendServerCommand(safehouseData.player, "WastelandSafehouses", "SyncFailed")
            end
        end
    end
    safehousesToCheck = itemsToKeep
    if #safehousesToCheck == 0 then
        Events.OnTick.Remove(checkPendingSafehouses)
    end
end


SafeHouseOfficerDb = {}
-- use ModData to store the safehouse officer data
local SafehouseOfficerDbKey = "SafehouseOfficerDb"
function LoadSafehouseOfficerDb()
    SafeHouseOfficerDb = ModData.getOrCreate(SafehouseOfficerDbKey)
end
function SaveSafehouseOfficerDb()
    ModData.add(SafehouseOfficerDbKey, SafeHouseOfficerDb)
    ModData.transmit(SafehouseOfficerDbKey)
end
function AddSafehouseOfficer(safehouse, username)
    if not SafeHouseOfficerDb[safehouse] then
        SafeHouseOfficerDb[safehouse] = {}
    end
    SafeHouseOfficerDb[safehouse][username] = true
    SaveSafehouseOfficerDb()
end

function RemoveSafehouseOfficer(safehouse, username)
    if SafeHouseOfficerDb[safehouse] then
        SafeHouseOfficerDb[safehouse][username] = nil
        SaveSafehouseOfficerDb()
    end
end

Events.OnInitGlobalModData.Add(LoadSafehouseOfficerDb)

Events.OnClientCommand.Add(function (module, command, player, args)
    if module == "WastelandSafehouses" then
        if command == "CreateNew" then
            table.insert(safehousesToCheck, {
                player=player,
                x=tonumber(args[1]),
                y=tonumber(args[2]),
                w=tonumber(args[3]),
                h=tonumber(args[4]),
                temp_owner_name=args[5],
                checks = 0,
            })
            if #safehousesToCheck == 1 then
                Events.OnTick.Add(checkPendingSafehouses)
            end
        elseif command == "AddOfficer" then
            AddSafehouseOfficer(args[1], args[2])
        elseif command == "RemoveOfficer" then
            RemoveSafehouseOfficer(args[1], args[2])
        end
    end
end)