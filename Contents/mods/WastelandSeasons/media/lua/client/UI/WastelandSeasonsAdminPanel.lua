require "GravyUI"
WastelandSeasonsAdminPanel = ISPanel:derive("WastelandSeasonsAdminPanel")

local FONT_HGT_SMALL = getTextManager():getFontHeight(UIFont.Small)
local FONT_HGT_MEDIUM = getTextManager():getFontHeight(UIFont.Medium)
local HGT_DROPDOWN = FONT_HGT_SMALL + 5
local HGT_BUTTON = FONT_HGT_SMALL + 10

function WastelandSeasonsAdminPanel:show()
    if WastelandSeasonsAdminPanel.instance then
        WastelandSeasonsAdminPanel.instance:onClose()
    end
    local scale = FONT_HGT_SMALL / 12
    local w = 400 * scale
    local h = 10 + FONT_HGT_MEDIUM + 5 + FONT_HGT_SMALL * 2 + HGT_DROPDOWN * 3 + HGT_BUTTON * 4 + 9 * 5 + 10
    local o = ISPanel:new(getCore():getScreenWidth()/2-w/2,getCore():getScreenHeight()/2-h/2, w, h)
    setmetatable(o, self)
    o.__index = self
    o:initialise()
    o:addToUIManager()
    sendClientCommand(getPlayer(), "WastelandSeasons", "RequestData", {})
    WastelandSeasonsAdminPanel.instance = o
    return o
end

function WastelandSeasonsAdminPanel:updateData(data)
    if not WastelandSeasonsAdminPanel.instance then return end
    WastelandSeasonsAdminPanel.instance:setData(data or {})
end

function WastelandSeasonsAdminPanel:initialise()
    ISPanel.initialise(self)
    self.moveWithMouse = true

    local win = GravyUI.Node(self.width, self.height):pad(5)
    local header, body = win:rows({FONT_HGT_MEDIUM, 1}, 5)

    local headerLabel = header:makeLabel("Wasteland Seasons Admin Panel", UIFont.Medium, {r=1,g=1,b=1,a=1}, "left")
    self:addChild(headerLabel)

    local bodyLeft, bodyRight = body:cols(2, 5)

    local weatherEventLabel, currentStatusLabel,
          chooseEvent, chooseStart, chooseDuration,
          scheduleButton, forceButton, cancelButton = bodyLeft:rows({
            FONT_HGT_SMALL, FONT_HGT_SMALL,
            HGT_DROPDOWN, HGT_DROPDOWN, HGT_DROPDOWN,
            HGT_BUTTON, HGT_BUTTON, HGT_BUTTON}, 5)


    local eventHeaderLabel = weatherEventLabel:makeLabel("Weather Event:", UIFont.Small, {r=1,g=1,b=1,a=1}, "left")
    self:addChild(eventHeaderLabel)

    self.currentStatusLabel = currentStatusLabel:makeLabel("None", UIFont.Small, {r=1,g=1,b=1,a=1}, "left")
    self:addChild(self.currentStatusLabel)

    self.chooseEvent = chooseEvent:makeComboBox()
    for key, event in pairs(WastelandSeasons.Events) do
        self.chooseEvent:addOptionWithData(event.name, key)
    end
    self:addChild(self.chooseEvent)

    self.chooseStart = chooseStart:makeComboBox()
    -- Now (1), 2 hours, 6 hours, 12 hours, 1 day, 2 days, 3 days, 1 week, 2 weeks
    self.chooseStart:addOptionWithData("Now", 0)
    self.chooseStart:addOptionWithData("in 2 Hours", 2)
    self.chooseStart:addOptionWithData("in 6 Hours", 6)
    self.chooseStart:addOptionWithData("in 12 Hours", 12)
    self.chooseStart:addOptionWithData("in 1 Day", 24)
    self.chooseStart:addOptionWithData("in 2 Days", 48)
    self.chooseStart:addOptionWithData("in 3 Days", 72)
    self.chooseStart:addOptionWithData("in 1 Week", 168)
    self.chooseStart:addOptionWithData("in 2 Weeks", 336)
    self:addChild(self.chooseStart)

    self.chooseDuration = chooseDuration:makeComboBox()
    -- 6 hours, 8 hours, 12 hours, 18 hours, 1 day, 2 days, 3 days
    self.chooseDuration:addOptionWithData("for 1 Hour", 1)
    self.chooseDuration:addOptionWithData("for 2 Hours", 2)
    self.chooseDuration:addOptionWithData("for 4 Hours", 4)
    self.chooseDuration:addOptionWithData("for 6 Hours", 6)
    self.chooseDuration:addOptionWithData("for 8 Hours", 8)
    self.chooseDuration:addOptionWithData("for 12 Hours", 12)
    self.chooseDuration:addOptionWithData("for 18 Hours", 18)
    self.chooseDuration:addOptionWithData("for 1 Day", 24)
    self.chooseDuration:addOptionWithData("for 2 Days", 48)
    self.chooseDuration:addOptionWithData("for 3 Days", 72)
    self:addChild(self.chooseDuration)

    self.scheduleButton = scheduleButton:makeButton("Schedule Event", self, self.onScheduleEvent)
    self:addChild(self.scheduleButton)

    self.forceButton = forceButton:makeButton("Force Start Now", self, self.onForceEvent)
    self:addChild(self.forceButton)

    self.cancelButton = cancelButton:makeButton("Cancel Event", self, self.onCancelEvent)
    self:addChild(self.cancelButton)

    local precipitationLabel, currentPrecipitationLabel,
          choosePrecipitation, startPrecipitation, clearPrecipitation, _,
          chooseStorm, chooseStormDuration, startStorm, clearStorm = bodyRight:rows({
            FONT_HGT_SMALL, FONT_HGT_SMALL,
            HGT_DROPDOWN, HGT_BUTTON, HGT_BUTTON, 10,
            HGT_DROPDOWN, HGT_DROPDOWN, HGT_BUTTON, HGT_BUTTON}, 5)

    local precLabel = precipitationLabel:makeLabel("Precipitation:", UIFont.Small, {r=1,g=1,b=1,a=1}, "left")
    self:addChild(precLabel)

    self.currentPrecipitationLabel = currentPrecipitationLabel:makeLabel("None", UIFont.Small, {r=1,g=1,b=1,a=1}, "left")
    self:addChild(self.currentPrecipitationLabel)

    self.choosePrecipitation = choosePrecipitation:makeComboBox()
    self.choosePrecipitation:addOptionWithData("Clear", "none")
    self.choosePrecipitation:addOptionWithData("Light Rain", "lightrain")
    self.choosePrecipitation:addOptionWithData("Medium Rain", "mediumrain")
    self.choosePrecipitation:addOptionWithData("Heavy Rain", "heavyrain")
    self.choosePrecipitation:addOptionWithData("Light Snow", "lightsnow")
    self.choosePrecipitation:addOptionWithData("Medium Snow", "mediumsnow")
    self.choosePrecipitation:addOptionWithData("Heavy Snow", "heavysnow")
    self:addChild(self.choosePrecipitation)

    self.startPrecipitation = startPrecipitation:makeButton("Start Precipitation", self, self.onStartPrecipitation)
    self:addChild(self.startPrecipitation)

    self.clearPrecipitation = clearPrecipitation:makeButton("Clear Precipitation", self, self.onClearPrecipitation)
    self:addChild(self.clearPrecipitation)

    self.chooseStorm = chooseStorm:makeComboBox()
    self.chooseStorm:addOptionWithData("Blizzard", "blizzard")
    self.chooseStorm:addOptionWithData("Tropical Storm", "tropicalstorm")
    self:addChild(self.chooseStorm)

    self.chooseStormDuration = chooseStormDuration:makeComboBox()
    self.chooseStormDuration:addOptionWithData("for 1 Hour", 1)
    self.chooseStormDuration:addOptionWithData("for 2 Hours", 2)
    self.chooseStormDuration:addOptionWithData("for 4 Hours", 4)
    self.chooseStormDuration:addOptionWithData("for 6 Hours", 6)
    self.chooseStormDuration:addOptionWithData("for 8 Hours", 8)
    self.chooseStormDuration:addOptionWithData("for 12 Hours", 12)
    self.chooseStormDuration:addOptionWithData("for 18 Hours", 18)
    self.chooseStormDuration:addOptionWithData("for 24 Hours", 24)
    self:addChild(self.chooseStormDuration)

    self.startStorm = startStorm:makeButton("Start Storm", self, self.onStartStorm)
    self:addChild(self.startStorm)

    self.clearStorm = clearStorm:makeButton("Clear Storm", self, self.onClearStorm)
    self:addChild(self.clearStorm)

    local closeButton = win:corner("topRight", 50, HGT_BUTTON):makeButton("Close", self, self.onClose)
    local refreshButton = win:corner("topRight", 50, HGT_BUTTON):offset(-55, 0):makeButton("Refresh", self, self.refresh)
    self:addChild(closeButton)
    self:addChild(refreshButton)
end

function WastelandSeasonsAdminPanel:setData(data)
    self.scheduleButton:setEnable(false)
    self.forceButton:setEnable(false)
    self.cancelButton:setEnable(false)

    if data.scheduledEvent then
        if data.scheduledEventStart and data.scheduledEventStart > 0 then
            self.currentStatusLabel.text = WastelandSeasons.Events[data.scheduledEvent].name .. " in " .. data.scheduledEventStart .. " hours"
            self.forceButton:setEnable(true)
        elseif data.scheduledEventEnd and data.scheduledEventEnd > 0 then
            self.currentStatusLabel.text = WastelandSeasons.Events[data.scheduledEvent].name .. " for " .. data.scheduledEventEnd .. " hours"
        else
            self.currentStatusLabel.text = "Error"
        end

        self.cancelButton:setEnable(true)
    else
        self.currentStatusLabel.text = "None"
        self.scheduleButton:setEnable(true)
    end

    self.startPrecipitation:setEnable(false)
    self.clearPrecipitation:setEnable(false)
    if data.setPrecipitation then
        self.currentPrecipitationLabel.text = data.setPrecipitation
        self.clearPrecipitation:setEnable(true)
    else
        self.currentPrecipitationLabel.text = "None"
        self.startPrecipitation:setEnable(true)
    end
end

function WastelandSeasonsAdminPanel:onScheduleEvent()
    local event = self.chooseEvent:getOptionData(self.chooseEvent.selected)
    local start = self.chooseStart:getOptionData(self.chooseStart.selected)
    local duration = self.chooseDuration:getOptionData(self.chooseDuration.selected)
    if event and start and duration then
        sendClientCommand(getPlayer(), "WastelandSeasons", "ScheduleEvent", {event, start, duration})
    end
end

function WastelandSeasonsAdminPanel:onForceEvent()
    sendClientCommand(getPlayer(), "WastelandSeasons", "ForceEvent", {})
end

function WastelandSeasonsAdminPanel:onCancelEvent()
    sendClientCommand(getPlayer(), "WastelandSeasons", "CancelEvent", {})
end

function WastelandSeasonsAdminPanel:onStartPrecipitation()
    local precipitation = self.choosePrecipitation:getOptionData(self.choosePrecipitation.selected)
    if precipitation then
        sendClientCommand(getPlayer(), "WastelandSeasons", "ForcePrecipitation", {precipitation})
    end
end

function WastelandSeasonsAdminPanel:onClearPrecipitation()
    sendClientCommand(getPlayer(), "WastelandSeasons", "ClearPrecipitation", {})
end

function WastelandSeasonsAdminPanel:onStartStorm()
    local storm = self.chooseStorm:getOptionData(self.chooseStorm.selected)
    local duration = self.chooseStormDuration:getOptionData(self.chooseStormDuration.selected)
    if storm and duration then
        sendClientCommand(getPlayer(), "WastelandSeasons", "ForceStorm", {storm, duration})
    end
end

function WastelandSeasonsAdminPanel:onClearStorm()
    sendClientCommand(getPlayer(), "WastelandSeasons", "ClearStorm", {})
end

function WastelandSeasonsAdminPanel:refresh()
    sendClientCommand(getPlayer(), "WastelandSeasons", "RequestData", {})
end

function WastelandSeasonsAdminPanel:onClose()
    self:removeFromUIManager()
    WastelandSeasonsAdminPanel.instance = nil
end