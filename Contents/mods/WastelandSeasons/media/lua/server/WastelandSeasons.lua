if isClient() then return end

WastelandSeasons = WastelandSeasons or {}

-- for hotreload
if WastelandSeasons.SERVER_INITIALIZED then
    Events.OnClientCommand.Remove(WastelandSeasons.OnClientCommand)
    Events.EveryHours.Remove(WastelandSeasons.CheckHourly)
    Events.OnInitGlobalModData.Remove(WastelandSeasons.OnInitGlobalModData)
end

function WastelandSeasons.GetCurrentSeason()
    local season = getClimateManager():getSeason():getSeason()
    local seasonName
    if season == 0 then
        seasonName = "Unknown"
    elseif season == 1 then
        seasonName = "Spring"
    elseif season == 2 then
        seasonName = "Early Summer"
    elseif season == 3 then
        seasonName = "Late Summer"
    elseif season == 4 then
        seasonName = "Autumn"
    elseif season == 5 then
        seasonName = "Winter"
    else
        seasonName = "Unknown"
    end
    return seasonName
end

function WastelandSeasons.RollEvent()
    local season = WastelandSeasons.GetCurrentSeason()
    local possibleEvents = {}
    for id, event in pairs(WastelandSeasons.Events) do
        local useEvent = true
        if event.seasons then
            useEvent = false
            for _, seasonName in pairs(event.seasons) do
                if seasonName == season then
                    useEvent = true
                    break
                end
            end
        end
        if useEvent then
            table.insert(possibleEvents, {id = id, chance = event.chance})
        end
    end
    local event = WL_Utils.weightedRandom(possibleEvents, "chance")
    if event then
        WastelandSeasons.Data.scheduledEvent = event.id
        WastelandSeasons.Data.scheduledEventStart = ZombRand(WastelandSeasons.Events[event.id].leadupHours[1], WastelandSeasons.Events[event.id].leadupHours[2])
        WastelandSeasons.Save()
        print("[Wasteland Seasons] Scheduled event " .. WastelandSeasons.Events[event.id].name .. " to start in " .. WastelandSeasons.Data.scheduledEventStart .. " hours")
    end
end

function WastelandSeasons.TryAddEvent()
    if SandboxVars.WastelandSeasons.EnableEvents then
        local roll = ZombRand(SandboxVars.WastelandSeasons.EventChance)
        print("[Wasteland Seasons] Rolled " .. roll .. " for event chance " .. SandboxVars.WastelandSeasons.EventChance)
        if roll == 0 then
            WastelandSeasons.RollEvent()
        end
    end
end

function WastelandSeasons.SendEnv(message)
    sendServerCommand("WastelandSeasons", "env", {message})
end

function WastelandSeasons.AlertStaffWeatherEventUpcoming()
    -- sends an alert to staff when a weather event is 1, 2, 3, 6, 12 hours away
    if not WastelandSeasons.Data.scheduledEvent then return end
    if not WastelandSeasons.Data.scheduledEventStart then return end
    if WastelandSeasons.Data.scheduledEventStart ~= 1
    and WastelandSeasons.Data.scheduledEventStart ~= 2
    and WastelandSeasons.Data.scheduledEventStart ~= 3
    and WastelandSeasons.Data.scheduledEventStart ~= 6
    and WastelandSeasons.Data.scheduledEventStart ~= 12 then return end
    if not WastelandSeasons.Events[WastelandSeasons.Data.scheduledEvent] then return end
    local event = WastelandSeasons.Events[WastelandSeasons.Data.scheduledEvent]
    local message = event.name .. " starting in " .. WastelandSeasons.Data.scheduledEventStart .. " hours."
    message = "<RGB:1,1,1>[Weather Event] " .. message

    local allPlayers = getOnlinePlayers()
    if allPlayers:size() == 0 then return end
    for i=0, allPlayers:size()-1 do
        local player = allPlayers:get(i)
        if WL_Utils.isStaff(player) then
            sendServerCommand(player, "WRC", "StaffChat", {"Weather Event", message})
        end
    end
end

function WastelandSeasons.ProcessCurrentEvent()
    local event = WastelandSeasons.Events[WastelandSeasons.Data.scheduledEvent]
    if WastelandSeasons.Data.scheduledEventStart ~= nil and WastelandSeasons.Data.scheduledEventStart > 0 then
        WastelandSeasons.Data.scheduledEventStart = WastelandSeasons.Data.scheduledEventStart - 1
        print("[Wasteland Seasons] Event " .. event.name .. " starting in " .. WastelandSeasons.Data.scheduledEventStart .. " hours")
        WastelandSeasons.AlertStaffWeatherEventUpcoming()
        if WastelandSeasons.Data.scheduledEventStart > 0 then
            if event.messages[WastelandSeasons.Data.scheduledEventStart] then
                WastelandSeasons.SendEnv(event.messages[WastelandSeasons.Data.scheduledEventStart])
            end
        elseif WastelandSeasons.Data.scheduledEventStart <= 0 then
            WastelandSeasons.Data.scheduledEventStart = 0
            if WastelandSeasons.Data.scheduledEventEndOverride then
                WastelandSeasons.Data.scheduledEventEnd = WastelandSeasons.Data.scheduledEventEndOverride
                WastelandSeasons.Data.scheduledEventEndOverride = nil
            else
                WastelandSeasons.Data.scheduledEventEnd = ZombRand(event.durationHours[1], event.durationHours[2])
            end

            if event.tempAdjust then
                WastelandSeasons.Data.adjustedTemp = ZombRand(event.tempAdjust[1], event.tempAdjust[2])
                WastelandSeasons.WriteTemps()
            elseif event.tempTarget then
                local dayMax = getClimateManager():getClimateForecaster():getForecast():getTemperature():getTotalMax()
                local adjustment = math.floor(event.tempTarget - dayMax)
                WastelandSeasons.Data.adjustedTemp = adjustment
                WastelandSeasons.WriteTemps()
            else
                WastelandSeasons.Data.adjustedTemp = 0
                print("[Wasteland Seasons] No temp adjustment for event " .. event.name)
            end

            if event.precipitation then
                WastelandSeasons.Data.setPrecipitation = event.precipitation
                WastelandSeasons.Data.clearPrecipitation = false
                WastelandSeasons.DoPrecipitation()
            end

            if event.fog then
                WastelandSeasons.Data.setFog = event.fog
                WastelandSeasons.Data.clearFog = false
                WastelandSeasons.DoFog()
            end

            if event.dayColor then
                WastelandSeasons.Data.setDayColor = event.dayColor
                WastelandSeasons.Data.clearDayColor = false
                WastelandSeasons.DoDayColor()
            end

            if event.trigger then
                WastelandSeasons.Data.trigger = event.trigger
                WastelandSeasons.TriggerWeather()
            end

            if event.harmType and event.harmRate then
                WastelandSeasons.DoHarm(event.harmType, event.harmRate)
            end

            if event.messages["start"] then
                WastelandSeasons.SendEnv(event.messages["start"])
            end
            print("[Wasteland Seasons] Event " .. event.name .. " starting for " .. WastelandSeasons.Data.scheduledEventEnd .. " hours")
        end
    elseif WastelandSeasons.Data.scheduledEventEnd > 0 then
        WastelandSeasons.Data.scheduledEventEnd = WastelandSeasons.Data.scheduledEventEnd - 1
        print("[Wasteland Seasons] Event " .. event.name .. " ending in " .. WastelandSeasons.Data.scheduledEventEnd .. " hours")
        if WastelandSeasons.Data.scheduledEventEnd <= 0 then

            if event.trigger then
                WastelandSeasons.Data.trigger = "clear"
                WastelandSeasons.TriggerWeather()
            end

            if event.precipitation then
                WastelandSeasons.Data.setPrecipitation = nil
                WastelandSeasons.Data.clearPrecipitation = true
                WastelandSeasons.DoPrecipitation()
            end

            if event.fog then
                WastelandSeasons.Data.setFog = nil
                WastelandSeasons.Data.clearFog = true
                WastelandSeasons.DoFog()
            end

            if event.dayColor then
                WastelandSeasons.Data.setDayColor = nil
                WastelandSeasons.Data.clearDayColor = true
                WastelandSeasons.DoDayColor()
            end

            if event.harmType then
                WastelandSeasons.DoHarm("none", 0)
            end

            if event.messages["end"] then
                WastelandSeasons.SendEnv(event.messages["end"])
            end

            WastelandSeasons.Data.scheduledEventEnd = 0
            WastelandSeasons.Data.scheduledEvent = nil
            WastelandSeasons.Data.scheduledEventStart = nil
            WastelandSeasons.Data.scheduledEventEnd = nil
            WastelandSeasons.Data.scheduledEventEndOverride = nil
            WastelandSeasons.Data.adjustedTemp = nil
            WastelandSeasons.Data.clearPrecipitation = true
            WastelandSeasons.Data.trigger = nil
            WastelandSeasons.Save()
            WastelandSeasons.WriteTemps()
            print("[Wasteland Seasons] Event " .. event.name .. " ending")
        end
    else
        WastelandSeasons.Data.scheduledEvent = nil
        WastelandSeasons.Data.scheduledEventStart = nil
        WastelandSeasons.Data.scheduledEventEndOverride = nil
    end
    WastelandSeasons.Save()
end

function WastelandSeasons.WriteTemps()
    local fileWriter = getFileWriter("PzWebStats/weatherchange.txt", true, false)
    if WastelandSeasons.Data.adjustedTemp then
        fileWriter:writeln(tostring(SandboxVars.WastelandSeasons.BaseTempMin + WastelandSeasons.Data.adjustedTemp))
        fileWriter:writeln(tostring(SandboxVars.WastelandSeasons.BaseTempMax + WastelandSeasons.Data.adjustedTemp))
        print("[Wasteland Seasons] Adjusted temp " .. WastelandSeasons.Data.adjustedTemp)
    else
        fileWriter:writeln(tostring(SandboxVars.WastelandSeasons.BaseTempMin))
        fileWriter:writeln(tostring(SandboxVars.WastelandSeasons.BaseTempMax))
        print("[Wasteland Seasons] Reset temp")
    end
    fileWriter:close()
end

function WastelandSeasons.CheckHourly()
    WastelandSeasons.TriggerWeather()
    local gameTime = getGameTime()

    -- clear any scheduled events if it's Christmas
    if gameTime:getMonth() == 11 and gameTime:getDay() == 24 then
        if WastelandSeasons.Data.scheduledEvent then
            if WastelandSeasons.Data.scheduledEventStart and WastelandSeasons.Data.scheduledEventStart > 0 then
                WastelandSeasons.Data.scheduledEvent = nil
                WastelandSeasons.Data.scheduledEventStart = nil
                WastelandSeasons.Data.scheduledEventEnd = nil
                WastelandSeasons.Save()
            else
                WastelandSeasons.Data.scheduledEventEnd = 1
                WastelandSeasons.CheckHourly()
            end
            print("[Wasteland Seasons] Force cancelled current event for Christmas")
        end
    end

    if WastelandSeasons.Data.scheduledEvent then
        if WastelandSeasons.Events[WastelandSeasons.Data.scheduledEvent] then
            WastelandSeasons.ProcessCurrentEvent()
        else
            WastelandSeasons.Data.scheduledEvent = nil
            WastelandSeasons.Data.scheduledEventStart = nil
            WastelandSeasons.Data.scheduledEventEnd = nil
            WastelandSeasons.Data.scheduledEventEndOverride = nil
            WastelandSeasons.Data.adjustedTemp = nil
            if WastelandSeasons.Data.setPrecipitation then
                WastelandSeasons.Data.clearPrecipitation = true
            end
            WastelandSeasons.Save()
        end
    else
        WastelandSeasons.TryAddEvent()
    end

    if gameTime:getMonth() == 11 and gameTime:getDay() == 24 then
        WastelandSeasons.Data.setPrecipitation = "mediumsnow"
        WastelandSeasons.Data.clearPrecipitation = false
        WastelandSeasons.Save()
        WastelandSeasons.DoPrecipitation()
    elseif gameTime:getMonth() == 11 and gameTime:getDay() == 25 then
        WastelandSeasons.Data.setPrecipitation = nil
        WastelandSeasons.Data.clearPrecipitation = true
        WastelandSeasons.Save()
        WastelandSeasons.DoPrecipitation()
    end
end

function WastelandSeasons.SavePublic()
    ModData.add("WastelandSeasonsPublic", WastelandSeasons.Public)
    ModData.transmit("WastelandSeasonsPublic")
end

function WastelandSeasons.Save()
    ModData.add("WastelandSeasons", WastelandSeasons.Data)
end

local FLOAT_PRECIPITATION_INTENSITY = 3;
local FLOAT_GLOBAL_LIGHT_INTENSITY = 1;
local COLOR_GLOBAL_LIGHT = 0;
local FLOAT_FOG_INTENSITY = 5;
local BOOL_IS_SNOW = 0;

function WastelandSeasons.TriggerWeather()
    if not WastelandSeasons.Data.trigger then return end
    if WastelandSeasons.Data.trigger == "clear" then
        getClimateManager():stopWeatherAndThunder()
        print("[Wasteland Seasons] Cleared weather")
    else
        if not WastelandSeasons.Data.scheduledEventEnd then return end
        local dur = WastelandSeasons.Data.scheduledEventEnd - 3.0
        if WastelandSeasons.Data.trigger == "blizzard" then
            getClimateManager():triggerCustomWeatherStage(WeatherPeriod.STAGE_BLIZZARD, dur)
            print("[Wasteland Seasons] Triggered blizzard for " .. dur .. " hours")
        elseif WastelandSeasons.Data.trigger == "tropicalstorm" then
            getClimateManager():triggerCustomWeatherStage(WeatherPeriod.STAGE_TROPICAL_STORM, dur)
            print("[Wasteland Seasons] Triggered tropical storm for " .. dur .. " hours")
        end
    end
    WastelandSeasons.Data.trigger = nil
    WastelandSeasons.Save()
end

function WastelandSeasons.DoTriggerWeather()
    WastelandSeasons.TriggerWeather()
    Events.EveryOneMinutes.Remove(WastelandSeasons.DoTriggerWeather)
end

function WastelandSeasons.DoFog()
    local clim = getClimateManager()
    if WastelandSeasons.Data.setFog and not clim:getClimateFloat(FLOAT_FOG_INTENSITY):isEnableAdmin() then
        clim:getClimateFloat(FLOAT_FOG_INTENSITY):setEnableAdmin(true)
        clim:getClimateFloat(FLOAT_FOG_INTENSITY):setAdminValue(WastelandSeasons.Data.setFog)
        print("[Wasteland Seasons] Set fog to " .. WastelandSeasons.Data.setFog)
    elseif WastelandSeasons.Data.clearFog then
        local clim = getClimateManager()
        clim:getClimateFloat(FLOAT_FOG_INTENSITY):setEnableAdmin(false)
        WastelandSeasons.Data.clearFog = nil
        WastelandSeasons.Save()
        print("[Wasteland Seasons] Cleared fog")
    end
end

function WastelandSeasons.DoDayColor()
    local clim = getClimateManager()
    if WastelandSeasons.Data.setDayColor and not clim:getClimateColor(COLOR_GLOBAL_LIGHT):isEnableAdmin() then
        clim:getClimateFloat(FLOAT_GLOBAL_LIGHT_INTENSITY):setEnableAdmin(true)
        clim:getClimateColor(COLOR_GLOBAL_LIGHT):setEnableAdmin(true)
        local color = WastelandSeasons.Data.setDayColor
        clim:getClimateColor(COLOR_GLOBAL_LIGHT):setAdminValueExterior(color.r, color.g, color.b, color.a)
        clim:getClimateColor(COLOR_GLOBAL_LIGHT):setAdminValueInterior(0, 0, 0, 0)
        print("[Wasteland Seasons] Set day color to " .. color.r .. ", " .. color.g .. ", " .. color.b .. ", " .. color.a)
    elseif WastelandSeasons.Data.clearDayColor then
        clim:getClimateFloat(FLOAT_GLOBAL_LIGHT_INTENSITY):setEnableAdmin(false)
        clim:getClimateColor(COLOR_GLOBAL_LIGHT):setEnableAdmin(false)
        WastelandSeasons.Data.clearDayColor = nil
        WastelandSeasons.Save()
        print("[Wasteland Seasons] Cleared day color")
    end
end

function WastelandSeasons.DoPrecipitation()
    if WastelandSeasons.Data.setPrecipitation then
        -- Set Precipitation
        local clim = getClimateManager()
        if WastelandSeasons.Data.setPrecipitation == "none" then
            clim:getClimateFloat(FLOAT_PRECIPITATION_INTENSITY):setEnableAdmin(true)
            clim:getClimateFloat(FLOAT_PRECIPITATION_INTENSITY):setAdminValue(0)
            print("[Wasteland Seasons] Set precipitation to none")
        elseif WastelandSeasons.Data.setPrecipitation == "lightrain" then
            clim:getClimateFloat(FLOAT_PRECIPITATION_INTENSITY):setEnableAdmin(true)
            clim:getClimateFloat(FLOAT_PRECIPITATION_INTENSITY):setAdminValue(0.3)
            clim:getClimateBool(BOOL_IS_SNOW):setEnableAdmin(true)
            clim:getClimateBool(BOOL_IS_SNOW):setAdminValue(false)
            print("[Wasteland Seasons] Set precipitation to light rain")
        elseif WastelandSeasons.Data.setPrecipitation == "mediumrain" then
            clim:getClimateFloat(FLOAT_PRECIPITATION_INTENSITY):setEnableAdmin(true)
            clim:getClimateFloat(FLOAT_PRECIPITATION_INTENSITY):setAdminValue(0.6)
            clim:getClimateBool(BOOL_IS_SNOW):setEnableAdmin(true)
            clim:getClimateBool(BOOL_IS_SNOW):setAdminValue(false)
            print("[Wasteland Seasons] Set precipitation to medium rain")
        elseif WastelandSeasons.Data.setPrecipitation == "heavyrain" then
            clim:getClimateFloat(FLOAT_PRECIPITATION_INTENSITY):setEnableAdmin(true)
            clim:getClimateFloat(FLOAT_PRECIPITATION_INTENSITY):setAdminValue(1)
            clim:getClimateBool(BOOL_IS_SNOW):setEnableAdmin(true)
            clim:getClimateBool(BOOL_IS_SNOW):setAdminValue(false)
            print("[Wasteland Seasons] Set precipitation to heavy rain")
        elseif WastelandSeasons.Data.setPrecipitation == "lightsnow" then
            clim:getClimateFloat(FLOAT_PRECIPITATION_INTENSITY):setEnableAdmin(true)
            clim:getClimateFloat(FLOAT_PRECIPITATION_INTENSITY):setAdminValue(0.3)
            clim:getClimateBool(BOOL_IS_SNOW):setEnableAdmin(true)
            clim:getClimateBool(BOOL_IS_SNOW):setAdminValue(true)
            print("[Wasteland Seasons] Set precipitation to light snow")
        elseif WastelandSeasons.Data.setPrecipitation == "mediumsnow" then
            clim:getClimateFloat(FLOAT_PRECIPITATION_INTENSITY):setEnableAdmin(true)
            clim:getClimateFloat(FLOAT_PRECIPITATION_INTENSITY):setAdminValue(0.6)
            clim:getClimateBool(BOOL_IS_SNOW):setEnableAdmin(true)
            clim:getClimateBool(BOOL_IS_SNOW):setAdminValue(true)
            print("[Wasteland Seasons] Set precipitation to medium snow")
        elseif WastelandSeasons.Data.setPrecipitation == "heavysnow" then
            clim:getClimateFloat(FLOAT_PRECIPITATION_INTENSITY):setEnableAdmin(true)
            clim:getClimateFloat(FLOAT_PRECIPITATION_INTENSITY):setAdminValue(1)
            clim:getClimateBool(BOOL_IS_SNOW):setEnableAdmin(true)
            clim:getClimateBool(BOOL_IS_SNOW):setAdminValue(true)
            print("[Wasteland Seasons] Set precipitation to heavy snow")
        end
    elseif WastelandSeasons.Data.clearPrecipitation then
        -- FORCE NO PRECIPITATION
        local clim = getClimateManager()
        clim:getClimateFloat(FLOAT_PRECIPITATION_INTENSITY):setEnableAdmin(false)
        clim:getClimateBool(BOOL_IS_SNOW):setEnableAdmin(false)
        WastelandSeasons.Data.clearPrecipitation = nil
        WastelandSeasons.Save()
        print("[Wasteland Seasons] Cleared precipitation")
    end
end

function WastelandSeasons.DoHarm(type, rate)
    WastelandSeasons.Public.harmType = type
    WastelandSeasons.Public.harmRate = rate
    WastelandSeasons.SavePublic()
end

function WastelandSeasons.OnInitGlobalModData()
    WastelandSeasons.Data = ModData.getOrCreate("WastelandSeasons")
    WastelandSeasons.Public = ModData.getOrCreate("WastelandSeasonsPublic")
    WastelandSeasons.DoPrecipitation()
    if WastelandSeasons.Data.trigger then
        Events.EveryOneMinutes.Add(WastelandSeasons.DoTriggerWeather)
    end
end

function WastelandSeasons.SendData(player)
    sendServerCommand(player, "WastelandSeasons", "data", WastelandSeasons.Data)
end

function WastelandSeasons.OnClientCommand(module, command, sendingPlayer, args)
    if module ~= "WastelandSeasons" then return end
    if command == "RequestData" then
        WastelandSeasons.SendData(sendingPlayer)
    elseif command == "CancelEvent" then
        if WastelandSeasons.Data.scheduledEvent then
            if WastelandSeasons.Data.scheduledEventStart and WastelandSeasons.Data.scheduledEventStart > 0 then
                WastelandSeasons.Data.scheduledEvent = nil
                WastelandSeasons.Data.scheduledEventStart = nil
                WastelandSeasons.Data.scheduledEventEnd = nil
                WastelandSeasons.Save()
            else
                WastelandSeasons.Data.scheduledEventEnd = 1
                WastelandSeasons.CheckHourly()
            end
            print("[Wasteland Seasons] " .. sendingPlayer:getUsername() .. " cancelled current event")
        end
        WastelandSeasons.SendData(sendingPlayer)
    elseif command == "ScheduleEvent" then
        local event = args[1]
        local startTime = args[2] + 1
        local duration = args[3]
        if WastelandSeasons.Events[event] then
            WastelandSeasons.Data.scheduledEvent = event
            WastelandSeasons.Data.scheduledEventStart = startTime
            WastelandSeasons.Data.scheduledEventEndOverride = duration
            WastelandSeasons.Save()
            WastelandSeasons.ProcessCurrentEvent()
            print("[Wasteland Seasons] " .. sendingPlayer:getUsername() .. " forced event " .. event)
        end
        WastelandSeasons.SendData(sendingPlayer)
    elseif command == "ForceEvent" then
        if WastelandSeasons.Data.scheduledEvent then
            WastelandSeasons.Data.scheduledEventStart = 1
            WastelandSeasons.ProcessCurrentEvent()
        end
        WastelandSeasons.SendData(sendingPlayer)
    elseif command == "ForcePrecipitation" then
        local precipitation = args[1]
        WastelandSeasons.Data.setPrecipitation = precipitation
        WastelandSeasons.Data.clearPrecipitation = false
        WastelandSeasons.Save()
        print("[Wasteland Seasons] " .. sendingPlayer:getUsername() .. " forced precipitation to " .. precipitation)
        WastelandSeasons.DoPrecipitation()
        WastelandSeasons.SendData(sendingPlayer)
    elseif command == "ClearPrecipitation" then
        WastelandSeasons.Data.setPrecipitation = nil
        WastelandSeasons.Data.clearPrecipitation = true
        WastelandSeasons.Save()
        WastelandSeasons.DoPrecipitation()
        print("[Wasteland Seasons] " .. sendingPlayer:getUsername() .. " cleared precipitation")
        WastelandSeasons.SendData(sendingPlayer)
    elseif command == "ForceStorm" then
        local storm = args[1]
        local duration = args[2]
        WastelandSeasons.Data.trigger = storm
        WastelandSeasons.Data.scheduledEventEnd = duration
        WastelandSeasons.Save()
        WastelandSeasons.TriggerWeather()
        WastelandSeasons.SendData(sendingPlayer)
        print("[Wasteland Seasons] " .. sendingPlayer:getUsername() .. " forced trigger to " .. storm .. " for " .. duration .. " hours")
    elseif command == "ClearStorm" then
        WastelandSeasons.Data.trigger = "clear"
        WastelandSeasons.Save()
        WastelandSeasons.TriggerWeather()
        print("[Wasteland Seasons] " .. sendingPlayer:getUsername() .. " cleared storm")
        WastelandSeasons.SendData(sendingPlayer)
    end
end

Events.OnClientCommand.Add(WastelandSeasons.OnClientCommand)
Events.EveryHours.Add(WastelandSeasons.CheckHourly)
Events.OnInitGlobalModData.Add(WastelandSeasons.OnInitGlobalModData)

WastelandSeasons.SERVER_INITIALIZED = true