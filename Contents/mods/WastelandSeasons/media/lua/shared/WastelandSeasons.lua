WastelandSeasons = WastelandSeasons or {}
WastelandSeasons.Events = {
    ColdSnap = {
        name = "Cold Snap",
        chance = 8,
        tempAdjust = {-20, -10},
        durationHours = {96, 168},
        leadupHours = {96, 168},
        messages = {
            [2] = "A subtle coolness brushes the air, hinting at a change.",
            [1] = "The air turns brisk as the cold sharpens, hinting at what's to come.",
            ["start"] = "Suddenly, the temperature plummets as a cold front sweeps through the area.",
            ["end"] = "The chill dissipates, and normal temperatures slowly returns to the air.",
        }
    },
    HeatWave = {
        name = "Heat Wave",
        chance = 8,
        tempAdjust = {10, 20},
        durationHours = {96, 168},
        leadupHours = {96, 168},
        messages = {
            [2] = "A noticeable warmth flows in the air, hinting at an unusual shift.",
            [1] = "The warmth intensifies, becoming increasingly out of step with the seasonal norm.",
            ["start"] = "Temperatures climb higher than expected as the heat wave establishes itself.",
            ["end"] = "The anomalous warmth subsides, returning to more typical temperatures.",
        }
    },
    -- ExtremeColdSnap = {
    --     name = "Extreme Cold Snap",
    --     chance = 5,
    --     tempAdjust = {-30, -20},
    --     durationHours = {24, 48},
    --     leadupHours = {48, 72},
    --     messages = {
    --         [2] = "A piercing chill begins to unsettle the usual climate, foreshadowing a sharp descent.",
    --         [1] = "The cold deepens dramatically, deviating far from seasonal norms.",
    --         ["start"] = "An intense cold snap hits, dropping temperatures drastically below what's expected.",
    --         ["end"] = "The severe chill eases, allowing temperatures to approach normal for the season.",
    --     }
    -- },
    -- ExtremeHeatWave = {
    --     name = "Extreme Heat Wave",
    --     chance = 5,
    --     tempAdjust = {20, 30},
    --     durationHours = {24, 48},
    --     leadupHours = {48, 72},
    --     messages = {
    --         [2] = "Unusual warmth begins to build, signaling a major departure from expected conditions.",
    --         [1] = "The heat intensifies rapidly, far surpassing typical temperatures for this time of year.",
    --         ["start"] = "A severe heat wave takes hold, pushing temperatures to extreme levels above the norm.",
    --         ["end"] = "The extreme heat diminishes, gradually falling back to seasonal averages.",
    --     }
    -- },
    FreakBlizzard = {
        name = "Freak Blizzard",
        chance = 10,
        tempTarget = 0,
        durationHours = {6, 18},
        leadupHours = {1, 1},
        trigger = "blizzard",
        seasons = {"Spring", "Autumn"},
        messages = {
            [1] = "A cold wind blows through the area, thick clouds visible over the horizon.",
            ["start"] = "The temperature begins to drop and snow begins to fall.",
            ["end"] = "The temperature returns to normal and the snow stops.",
        }
    },
    -- FreakTropicalStorm = {
    --     name = "Freak Tropical Storm",
    --     chance = 10,
    --     tempTarget = 28,
    --     durationHours = {6, 18},
    --     leadupHours = {1, 1},
    --     trigger = "tropicalstorm",
    --     seasons = {"Winter"},
    --     messages = {
    --         [1] = "The wind picks up with moist air and thick clouds visible over the horizon.",
    --         ["start"] = "The temperature starts to rise and the winds pick up.",
    --         ["end"] = "The storm seems to pass.",
    --     }
    -- },
    -- RadioactiveFog = {
    --     name = "Radioactive Fog",
    --     chance = 1,
    --     durationHours = {1, 3},
    --     leadupHours = {2, 2},
    --     fog = 0.25,
    --     dayColor = {r=0.0,g=0.6,b=0.0,a=0.3},
    --     harmType = "radiation",
    --     harmRate = 0.2,
    --     messages = {
    --         [2] = "A strange, eerie glow begins to suffuse the fog rolling in from the wastelands.",
    --         [1] = "The fog thickens, its radioactive haze creeping closer.",
    --         ["start"] = "Visibility drops as a dense radioactive fog envelops the area, its presence ominous.",
    --         ["end"] = "The radioactive fog dissipates, clearing the air of its toxic embrace.",
    --     }
    -- },
    -- AcidRain = {
    --     name = "Acid Rain",
    --     chance = 1,
    --     durationHours = {1, 3},
    --     leadupHours = {2, 2},
    --     endMessage = "The rain clears out.",
    --     precipitation = "heavyrain",
    --     dayColor = {r=0.7,g=0.7,b=0.2,a=1.0},
    --     harmType = "acid",
    --     harmRate = 2,
    --     messages = {
    --         [2] = "The air takes on a sharp, tangy odor as dark clouds gather overhead.",
    --         [1] = "First drops of rain bring a bearing a strong smell of vinegar.",
    --         ["start"] = "The skies open up, pouring down toxic rain.",
    --         ["end"] = "The toxic rain stops, and the air clears.",
    --     }
    -- },
}

-- We do not do adjustments on client, only server
if isClient() then return end

-- this is temporarily disabled until we can figure out how why its erroring
-- local function OnInitSeasons(season)
--     local tempMin = SandboxVars.WastelandSeasons.TempMin
--     local tempMax = SandboxVars.WastelandSeasons.TempMax
--     if WastelandSeasons.Data and WastelandSeasons.Data.adjustedTemp then
--         print("WastelandSeasons adjusting temp " .. WastelandSeasons.Data.adjustedTemp)
--         tempMin = tempMin + WastelandSeasons.Data.adjustedTemp
--         tempMax = tempMax + WastelandSeasons.Data.adjustedTemp
--     end
--     print("WastelandSeasons:OnInitSeasons")
--     season:init(
--         SandboxVars.WastelandSeasons.Latitude,
--         tempMin,
--         tempMax,
--         SandboxVars.WastelandSeasons.TempDiff,
--         season:getSeasonLag(),
--         season:getHighNoon(),
--         season:getSeedA(),
--         season:getSeedB(),
--         season:getSeedC()
--     );
-- end

-- Events.OnInitSeasons.Add(OnInitSeasons)