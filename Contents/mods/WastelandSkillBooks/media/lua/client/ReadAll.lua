require "ISUI/ISInventoryPane"

local function readWithPutBack(self)
    ISReadABook.perform(self)
    if self.returnTo then
        local newItem = self.character:getInventory():FindAndReturn(self.item:getFullType())
        if newItem then
            ISTimedActionQueue.add(ISInventoryTransferAction:new(self.character, newItem, newItem:getContainer(), self.returnTo))
        end
    end
end

local function doRead(items, playerObj)
    for _, item in ipairs(items) do
        local playerInv = playerObj:getInventory()
        local putBack = nil
        if luautils.haveToBeTransfered(playerObj, item, true) then
            putBack = item:getContainer()
            ISTimedActionQueue.add(ISInventoryTransferAction:new(playerObj, item, putBack, playerInv))
        end
        local action = ISReadABook:new(playerObj, item, 150)
        action.returnTo = putBack
        action.perform = readWithPutBack
        ISTimedActionQueue.add(action)
    end
end

local function OnPreFillInventoryObjectContextMenu(player, context, items)
    local actualItems = ISInventoryPane.getActualItems(items)
    for _, item in ipairs(actualItems) do
        if item:getCategory() ~= "Literature" or item:canBeWrite() then
            return
        end
    end

    local playerObj = getSpecificPlayer(player)
    if playerObj:getTraits():isIlliterate() then
        return
    end

    local readBooks = playerObj:getAlreadyReadBook()
    local items = {}
    local seenTypes = {}
	for _,item in ipairs(actualItems) do
        local cantRead = false
        local alreadyKnown = false
        local alreadyRead = false

        local skillBook = SkillBook[item:getSkillTrained()]
        if skillBook then
            local perkLevel = playerObj:getPerkLevel(skillBook.perk)
            local minLevel = item:getLvlSkillTrained()
            local maxLevel = item:getMaxLevelTrained()
            if perkLevel + 1 < minLevel or perkLevel + 1 > maxLevel then
                cantRead = true
            end
            local readPages = playerObj:getAlreadyReadPages(item:getFullType())
            if readPages >= item:getNumberOfPages() then
                alreadyKnown = true
            end
        end
        if item:getTeachedRecipes() and not item:getTeachedRecipes():isEmpty() then
            if playerObj:getKnownRecipes():containsAll(item:getTeachedRecipes()) then
                alreadyKnown = true
            end
        end
        if readBooks:contains(item:getFullType()) then
            alreadyRead = true
        end

        if not cantRead and not alreadyKnown and not alreadyRead and not seenTypes[item:getFullType()] then
            table.insert(items, item)
            seenTypes[item:getFullType()] = true
        end
    end
    if #items > 1 then
        context:addOption(getText("ContextMenu_ReadAllUnread"), items, doRead, playerObj)
    end
end

Events.OnPreFillInventoryObjectContextMenu.Add(OnPreFillInventoryObjectContextMenu)