if getActivatedMods():contains("WastelandRpChat") then
    require "Chat/WRC"
end

local cachedMediaLineGuids = {}
local cachedRpLanguages = {}
local lastKnownMediaCount = 0
local lastKnownRecipesCount = 0
local lastKnownKills = 0
local lastKnownMakeup = {}
local lastKnownMotw = {}

WSK_Lib = WSK_Lib or {}

function WSK_Lib.PlayerCreated()
    lastKnownMediaCount = 0
    lastKnownRecipesCount = 0
    lastKnownMakeup = {}
    cachedRpLanguages = {}
    lastKnownMotw = {}
end

local function CacheMediaLineGuids()
    cachedMediaLineGuids = {}
    local recordedMedia = getZomboidRadio():getRecordedMedia()
    local categories = recordedMedia:getCategories()
    for i=1,categories:size() do
        local category = categories:get(i-1)
        local mediaType = RecordedMedia.getMediaTypeForCategory(category)
        local mediaList = recordedMedia:getAllMediaForType(mediaType)
        for j=1,mediaList:size() do
            local mediaData = mediaList:get(j-1)
            for k=1, mediaData:getLineCount() do
                local mediaLine = mediaData:getLine(k-1)
                if mediaLine then
                    for l = 0, getNumClassFields(mediaLine) - 1 do
                        local field = getClassField(mediaLine, l)
                        if string.find(tostring(field), "%.text") then
                            table.insert(cachedMediaLineGuids, getClassFieldVal(mediaLine, field))
                        end
                    end
                end
            end
        end
    end
end

local strAndFitnessTraits = {
    ["Weak"] = true,
    ["Feeble"] = true,
    ["Stout"] = true,
    ["Strong"] = true,
    ["Unfit"] = true,
    ["Out of Shape"] = true,
    ["Fit"] = true,
    ["Athletic"] = true,
}

--- Returns a table of all the level boosts from traits and professions.
--- @return table<string, number> A table of perk boosts, where the key is the perk ID and the value is the level boost.
function WSK_Lib.GetPerkBoosts()
    local player = getPlayer()
    local perkBonus = {}

    local function AddFromMap(map)
        for perk,level in pairs(map) do
            local perkStr = perk:getId()
            perkBonus[perkStr] = (perkBonus[perkStr] or 0) + tonumber(tostring(level))
        end
    end

    local profession = ProfessionFactory.getProfession(player:getDescriptor():getProfession())
    if profession then
        local professionXpMap = transformIntoKahluaTable(profession:getXPBoostMap())
        if professionXpMap then
            AddFromMap(professionXpMap)
        end
    end

    local playerTraits = player:getTraits()
    for i=0, playerTraits:size()-1 do
        local playerTrait = playerTraits:get(i)
        if not strAndFitnessTraits[playerTrait] then
            local trait = TraitFactory.getTrait(playerTrait)
            if trait then
                local traitXpMap = transformIntoKahluaTable(trait:getXPBoostMap())
                if traitXpMap then
                    AddFromMap(traitXpMap)
                end
            end
        end
    end

    local modData = player:getModData()
    -- We need to do this because PZ is giving trait bumps REALLY quickly.
    if modData.WSK_StartingStrengthPerk and modData.WSK_StartingStrengthPerk ~= "None" then
        local trait = TraitFactory.getTrait(modData.WSK_StartingStrengthPerk)
        if trait then
            local traitXpMap = transformIntoKahluaTable(trait:getXPBoostMap())
            if traitXpMap then
                AddFromMap(traitXpMap)
            end
        end
    end
    if modData.WSK_StartingFitnessPerk and modData.WSK_StartingFitnessPerk ~= "None" then
        local trait = TraitFactory.getTrait(modData.WSK_StartingFitnessPerk)
        if trait then
            local traitXpMap = transformIntoKahluaTable(trait:getXPBoostMap())
            if traitXpMap then
                AddFromMap(traitXpMap)
            end
        end
    end

    return perkBonus
end

--- Returns a table of all the starting skill levels for the player if > 0
--- @return table<string, number> A table of perk boosts, where the key is the perk ID and the value is the starting level.
---@see professions.lua Events.OnNewGame seems to have this logic duplicated to give players the initial XP?
function WSK_Lib.GetStartingSkillLevels()
    local boostMap = WSK_Lib.GetPerkBoosts()
    boostMap["Strength"] = (boostMap["Strength"] or 0) + 5
    boostMap["Fitness"] = (boostMap["Fitness"] or 0) + 5

    if getActivatedMods():contains("WastelandProfessions") then
        for perk,level in pairs(boostMap) do
            if WastelandProfession_DoubleableSkills[perk] then
                local startingLevel = math.min(level * 2, 6)
                if level > 1 then
                    startingLevel = startingLevel + 1
                end
                boostMap[perk] = startingLevel
            end
        end
    end

    return boostMap
end

function WSK_Lib.GetPlayerMedia(player)
    local mediaLines = {}
    for _, lineGuid in ipairs(cachedMediaLineGuids) do
        if player.isKnownMediaLine and player:isKnownMediaLine(lineGuid) then
            table.insert(mediaLines, lineGuid)
        end
    end
    return mediaLines
end

function WSK_Lib.SetPlayerMedia(player, media)
    print("WSK Restoring Media")
    for _, line in ipairs(media) do
        player:addKnownMediaLine(line)
    end
end

function WSK_Lib.IsNewMedia(player)
    local total = 0
    for _, lineGuid in ipairs(cachedMediaLineGuids) do
        if player.isKnownMediaLine and player:isKnownMediaLine(lineGuid) then
            total = total + 1
        end
    end

    if total > lastKnownMediaCount then
        lastKnownMediaCount = total
        return true
    end

    return false
end

function WSK_Lib.GetPlayerRecipes(player)
    local recipes = {}
    local knownRecipes = player:getKnownRecipes()

    for i=0, knownRecipes:size()-1 do
        local recipeID = knownRecipes:get(i)
        recipes[recipeID] = true
    end

    local playerDesc = player:getDescriptor()
    local playerTraits = player:getTraits()
    for i=0, playerTraits:size()-1 do
        local playerTrait = playerTraits:get(i)
        local trait = TraitFactory.getTrait(playerTrait)
        if trait then
            local traitRecipes = trait:getFreeRecipes()
            for ii=0, traitRecipes:size()-1 do
                local traitRecipe = traitRecipes:get(ii)
                recipes[traitRecipe] = nil
            end
        end
    end

    local playerProfession = playerDesc:getProfession()
    local profession = ProfessionFactory.getProfession(playerProfession)
    if profession then
        local profFreeRecipes = profession:getFreeRecipes()
        for i=0, profFreeRecipes:size()-1 do
            local profRecipe = profFreeRecipes:get(i)
            recipes[profRecipe] = nil
        end
    end

    local returnedGainedRecipes = {}
    for recipeID,_ in pairs(recipes) do
        table.insert(returnedGainedRecipes, recipeID)
    end

    return returnedGainedRecipes
end

function WSK_Lib.SetPlayerRecipes(player, recipes)
    print("WSK Restoring Recipes")
    for _, recipeID in ipairs(recipes) do
        player:learnRecipe(recipeID)
    end
end

function WSK_Lib.IsNewRecipes(player)
    local knownRecipesCount = player:getKnownRecipes():size()
    if lastKnownRecipesCount ~= knownRecipesCount then
        lastKnownRecipesCount = knownRecipesCount
        return true
    end
    return false
end

function WSK_Lib.GetPlayerSkills(player)
    local skills = {}
    local xpBoostMap = WSK_Lib.GetStartingSkillLevels()

    for i=1, Perks.getMaxIndex()-1 do
        local perks = Perks.fromIndex(i)
        local perk = PerkFactory.getPerk(perks)
        if perk then
            local currentXP = player:getXp():getXP(perk)
            local perkStr = perk:getId()
            local bonusLevels = (xpBoostMap[perkStr] or 0)
            local savingXP = currentXP-perk:getTotalXpForLevel(bonusLevels)
            if savingXP > 0 then
                skills[perkStr] = savingXP
            end
        end
    end

    return skills
end

function WSK_Lib.SetPlayerSkills(player, skills, numDeaths)
    print("WSK Restoring XP")
    local percentKeep = 100 - SandboxVars.WastelandOptions.SkillKeeperPunishPercent
    local hasWastelandXP = getActivatedMods():contains("WastelandXP")
    local xpBoostMap = WSK_Lib.GetStartingSkillLevels()
    local xp = player:getXp()
    for i=1, Perks.getMaxIndex()-1 do
        local perks = Perks.fromIndex(i)
        local perk = PerkFactory.getPerk(perks)
        if perk then
            local perkStr = perk:getId()
            if skills[perkStr] then
                if numDeaths > SandboxVars.WastelandOptions.SkillKeeperMaxFreeDeaths then
                    skills[perkStr] = skills[perkStr] * (percentKeep / 100)
                end
                local bonusLevels = (xpBoostMap[perkStr] or 0)
                local totalXp = skills[perkStr] + perk:getTotalXpForLevel(bonusLevels)
                if hasWastelandXP then
                    local cap = getLevelCap(player, perk)
                    if cap then
                        totalXp = math.min(totalXp, perk:getTotalXpForLevel(cap))
                    end
                end
                local neededXp = totalXp - xp:getXP(perk)
                if neededXp > 0 then
                    xp:AddXP(perk, neededXp, false, false, true)
                end
            end
        end
    end
    if numDeaths > SandboxVars.WastelandOptions.SkillKeeperMaxFreeDeaths then
        player:addLineChatElement("Recovered " .. percentKeep .. "% of XP", 1, 0, 0)
    else
        player:addLineChatElement("Recovered all XP", 1, 0, 0)
    end
end

function WSK_Lib.GetPlayerLanguages(player)
    if getActivatedMods():contains("roleplaychat") then
        local modData = player:getModData()
        return {modData.rpLanguage1, modData.rpLanguage2}
    end
    if getActivatedMods():contains("WastelandRpChat") then
        return WRC.Meta.GetKnownLanguages()
    end
    return nil
end

function WSK_Lib.SetPlayerLanguages(player, languages)
    if getActivatedMods():contains("roleplaychat") and languages then
        print("WSK Restoring RP Languages")
        cachedRpLanguages.rpLanguage1 = languages[1] or "Empty Slot"
        cachedRpLanguages.rpLanguage2 = languages[2] or "Empty Slot"
        local modData = player:getModData()
        modData.rpLanguage1 = cachedRpLanguages.rpLanguage1
        modData.rpLanguage2 = cachedRpLanguages.rpLanguage2
        ISChat.instance.rpLanguage1 = cachedRpLanguages.rpLanguage1
        ISChat.instance.rpLanguage2 = cachedRpLanguages.rpLanguage2
    end

    if getActivatedMods():contains("WastelandRpChat") and languages then
        print("WSK Restoring WastelandRpChat Languages")

        if player:HasTrait("Deaf") then
            WRC.Meta.AddKnownLanguage("asl")
            WRC.Meta.RemoveKnownLanguage("en")
            WRC.Meta.SetCurrentLanguage("asl")
            return
        elseif not player:HasTrait("Deaf") and #languages == 1 and languages[1] == "asl" then
            WRC.Meta.AddKnownLanguage("en")
            WRC.Meta.RemoveKnownLanguage("asl")
            WRC.Meta.SetCurrentLanguage("en")
            return
        end

        for _, language in ipairs(languages) do
            WRC.Meta.AddKnownLanguage(language)
        end

        if WLP_SelectAltLangWindow then
            local hasEn = false
            for i = 1, #languages do
                if languages[i] == "en" then
                    hasEn = true
                    break
                end
            end
            if hasEn then
                if #languages > 1 then
                    WLP_SelectAltLangWindow:removeFromUIManager()
                    WLP_SelectAltLangWindow = nil
                    WRC.Meta.RemoveKnownLanguage("en")
                end
            else
                WLP_SelectAltLangWindow:removeFromUIManager()
                WLP_SelectAltLangWindow = nil
            end
        end
    end
end

function WSK_Lib.IsNewLanguages(player)
    if getActivatedMods():contains("roleplaychat") then
        local modData = player:getModData()
        return modData.rpLanguage1 ~= cachedRpLanguages.rpLanguage1 or modData.rpLanguage2 ~= cachedRpLanguages.rpLanguage2
    end
    if getActivatedMods():contains("WastelandRpChat") then
        local areNew = false
        local langs = WRC.Meta.GetKnownLanguages()
        if #langs ~= #cachedRpLanguages then
            areNew = true
        end
        for i, lang in ipairs(langs) do
            if lang ~= cachedRpLanguages[i] then
                areNew = true
                break
            end
        end
        cachedRpLanguages = langs
        return areNew
    end
    return false
end

function WSK_Lib.GetPlayerMOTW(player)
    if getActivatedMods():contains("WastelandMusicians") then
        local motw = {}
        local modData = player:getModData()
        for key, value in pairs(modData) do
            if string.find(key, "MOTW_") then
                motw[key] = value
            end
        end
        return motw
    end
    return nil
end

function WSK_Lib.SetPlayerMOTW(player, motw)
    if getActivatedMods():contains("WastelandMusicians") then
        print("WSK Restoring MOTW")
        local modData = player:getModData()
        for key, value in pairs(motw) do
            modData[key] = value
        end
    end
end

function WSK_Lib.IsNewMOTW(player)
    if getActivatedMods():contains("WastelandMusicians") then
        local motw = WSK_Lib.GetPlayerMOTW(player)
        for key, value in pairs(motw) do
            if value ~= lastKnownMotw[key] then
                lastKnownMotw = motw
                return true
            end
        end
        for key, value in pairs(lastKnownMotw) do
            if value ~= motw[key] then
                lastKnownMotw = motw
                return true
            end
        end
    end
    return false
end

function WSK_Lib.GetPlayerKills(player)
    return player:getZombieKills()
end

function WSK_Lib.SetPlayerKills(player, kills)
    player:setZombieKills(kills)
end

function WSK_Lib.IsNewPLayerKills(player)
    local kills = player:getZombieKills()
    if kills ~= lastKnownKills then
        lastKnownKills = kills
        return true
    end
    lastKnownKills = kills
    return false
end

function WSK_Lib.IsNewPlayerMakeup(player)
    if ISMakeUpUI.windows[player:getPlayerNum()+1] then return false end

    local currentMakeup = WSK_Lib.GetPlayerMakeup(player)
    if #lastKnownMakeup ~= #currentMakeup then
        lastKnownMakeup = currentMakeup
        return true
    end
    for i=1,#currentMakeup do
        if currentMakeup[i].item ~= lastKnownMakeup[i].item then
            lastKnownMakeup = currentMakeup
            return true
        end
    end
    lastKnownMakeup = currentMakeup
    return false
end

function WSK_Lib.GetPlayerMakeup(player)
    local appliedMakeup = {}
    for i=0, player:getWornItems():size()-1 do
		local item = player:getWornItems():get(i):getItem()
        for _,makeup in ipairs(MakeUpDefinitions.makeup) do
            if makeup.item == item:getFullType() then
                table.insert(appliedMakeup, item:getFullType())
                break
            end
        end
	end
    table.sort(appliedMakeup)
    return appliedMakeup
end

function WSK_Lib.ApplyMakeup(player, makeupList)
    print("WSK Restoring Makeup")
    for i=player:getWornItems():size()-1, 0, -1 do
        local item = player:getWornItems():get(i):getItem()
        for _,makeup in ipairs(MakeUpDefinitions.makeup) do
            if makeup.item == item:getFullType() then
                player:removeWornItem(item)
                break
            end
        end
    end
    for _, makeup in ipairs(makeupList) do
        print("WSK Applying Makeup: " .. makeup)
        local item = InventoryItemFactory.CreateItem(makeup)
        player:getInventory():AddItem(item)
        player:setWornItem(item:getBodyLocation(), item)
    end
    triggerEvent("OnClothingUpdated", player);
end

WSK_Lib.DeathButton = ISUIElement:derive("WSK_DeathButton")

function WSK_Lib.DeathButton:create(player, data)
    local textHeight = getTextManager():MeasureStringY(UIFont.Small, "XXXXXX")
    local textWidth = getTextManager():MeasureStringX(UIFont.Small, "XXXXXX")
    local w = textWidth + 6
    local h = textHeight * 3 + 10
    local o = ISUIElement:new(getCore():getScreenWidth() - w - 3, getCore():getScreenHeight()/2 - h/2, w, h)
    setmetatable(o, self)
    self.__index = self
    o.player = player
    o.background = true
    o.backgroundColor = {r=0, g=0, b=0, a=0.5}
    o.borderColor = {r=1, g=1, b=1, a=0.5}
    o.endTime = data.timestamp + SandboxVars.WastelandOptions.SkillKeeperDeathTpAllowTime
    o.tpX = data.x
    o.tpY = data.y
    o.tpZ = data.z
    o.textHeight = textHeight
    o:initialise()
    o:instantiate()
    o:addToUIManager()
end

function WSK_Lib.DeathButton:prerender()
    local timeleft = self.endTime - getTimestamp()

    if self.player:isDead() or timeleft <= 0 then
        self:removeFromUIManager()
        return
    end

    ISPanel.prerender(self)
    self:drawTextCentre("TP to", self.width / 2, 3, 1, 1, 1, 1, UIFont.Small)
    self:drawTextCentre("Death", self.width / 2, 3 + self.textHeight, 1, 1, 1, 1, UIFont.Small)
    self:drawTextCentre("( " .. tostring(timeleft) .. "s )", self.width / 2, 7 + self.textHeight * 2, 1, 1, 1, 1, UIFont.Small)

    if self:isMouseOver() then
        self:drawRect(0, 0, self.width, self.height, 0.3, 0.5, 0.5, 0.5)
    end
end

function WSK_Lib.DeathButton:onMouseUp()
    if not self:isMouseOver() then return end
    WL_UserData.Set("WSK_DeathPoint", false, self.player:getUsername(), true)
    WL_Utils.teleportPlayerToCoords(self.player, self.tpX, self.tpY, self.tpZ, WSK_Lib.RetrieveDeathBox)
    WL_Utils.makePlayerSafe(SandboxVars.WastelandOptions.SkillKeeperDeathTpSafetime)
    self:removeFromUIManager()
end

function WSK_Lib.DropItemsToBox(player)
    local playerInv = player:getInventory()
    local box = InventoryItemFactory.CreateItem("Base.WSKDeathBox")
    local boxInv = box:getInventory()
    box:setCustomName(true)
    box:setName(player:getUsername() .. "'s Death Box")
    box:getModData().owner = player:getUsername()
    box:getModData().time = getTimestamp()

    local items = playerInv:getItems()
    for i=items:size()-1,0,-1 do
        local item = items:get(i)
        playerInv:DoRemoveItem(item)
        boxInv:AddItem(item)
    end

    player:getCurrentSquare():AddWorldInventoryItem(box, 0.5, 0.5, 0)
end

function WSK_Lib.EmptyDeathBox(box)
    local boxInv = box:getInventory()
    local items = boxInv:getItems()
    for i=items:size()-1,0,-1 do
        local item = items:get(i)
        boxInv:DoRemoveItem(item)
    end
end

function WSK_Lib.RetrieveDeathBox()
    if true then return end -- TODO fix this
    local player = getPlayer()
    local playerInv = player:getInventory()
    local box = nil
    local square = player:getCurrentSquare()
    -- search for the box up to 2 tiles away
    for x=-2,2 do
        for y=-2,2 do
            local sq = getCell():getGridSquare(square:getX() + x, square:getY() + y, square:getZ())
            if sq then
                local objects = sq:getWorldObjects()
                for i=0,objects:size()-1 do
                    local obj = objects:get(i)
                    local item = obj:getItem()
                    if item:getFullType() == "Base.WSKDeathBox" then
                        if item:getModData().owner == player:getUsername() then
                            box = obj
                            break
                        end
                    end
                end
            end
        end
    end

    if box then
        local boxInv = box:getItem():getItemContainer()
        local items = boxInv:getItems()
        for i=items:size()-1,0,-1 do
            local item = items:get(i)
            boxInv:DoRemoveItem(item)
            playerInv:AddItem(item)
        end
        ISRemoveItemTool.removeItem(box, player)
    end
end

Events.OnGameStart.Add(function()
    CacheMediaLineGuids()
end)
