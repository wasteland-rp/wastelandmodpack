require "UserData"
require "PlayerReady"
require "WSK_Lib"

local wasXpAdded = true
local checkKey = "WSK_Restored"
local didInit = false
local myDeaths = nil


local Listeners = {}

local checkSaveDelay = 0
function Listeners.CheckSavePlayer()
    if checkSaveDelay > 0 then
        checkSaveDelay = checkSaveDelay - 1
        return
    end
    checkSaveDelay = ZombRand(300, 1200)

    local player = getPlayer()
    if not player:getModData()[checkKey] then return end
    if player:isDead() then	return end

    local username = player:getUsername()
    if wasXpAdded then
        wasXpAdded = false
        print("WSK Saving XP")
        local skills = WSK_Lib.GetPlayerSkills(player)
        WL_UserData.Append("WSK_Data", {skills = skills}, username, true)
    end
    if WSK_Lib.IsNewRecipes(player) then
        print("WSK Saving Recipes")
        local recipes = WSK_Lib.GetPlayerRecipes(player)
        WL_UserData.Append("WSK_Data", {recipes = recipes}, username, true)
    end
    if WSK_Lib.IsNewMedia(player) then
        print("WSK Saving Media")
        local media = WSK_Lib.GetPlayerMedia(player)
        WL_UserData.Append("WSK_Data", {media = media}, username, true)
    end
    if WSK_Lib.IsNewLanguages(player) then
        local languages = WSK_Lib.GetPlayerLanguages(player)
        if languages then
            print("WSK Saving Languages")
            WL_UserData.Append("WSK_Data", {languages = languages}, username, true)
        end
    end
    if WSK_Lib.IsNewPLayerKills(player) then
        local kills = WSK_Lib.GetPlayerKills(player)
        if kills then
            print("WSK Saving Kills")
            WL_UserData.Append("WSK_Data", {kills = kills}, username, true)
        end
    end
    if WSK_Lib.IsNewPlayerMakeup(player) then
        local makeup = WSK_Lib.GetPlayerMakeup(player)
        if makeup then
            print("WSK Saving Makeup")
            WL_UserData.Append("WSK_Data", {makeup = makeup}, username, true)
        end
    end
    if WSK_Lib.IsNewMOTW(player) then
        local motw = WSK_Lib.GetPlayerMOTW(player)
        if motw then
            print("WSK Saving MOTW")
            WL_UserData.Append("WSK_Data", {motw = motw}, username, true)
        end
    end
end

function Listeners.ReceiveMyDeaths(data)
    myDeaths = data
    if not didInit then
        Events.OnTick.Add(Listeners.CheckSavePlayer)
        didInit = true
        if not getPlayer():getModData()[checkKey] then
            WL_UserData.Fetch("WSK_Data")
        end
    end
end

function Listeners.ReceiveMyData(data)
    local player = getPlayer()
    if data.skills then
        WSK_Lib.SetPlayerSkills(player, data.skills, #myDeaths)
    end
    if data.recipes then
        WSK_Lib.SetPlayerRecipes(player, data.recipes)
    end
    if data.media then
        WSK_Lib.SetPlayerMedia(player, data.media)
    end
    if data.languages then
        WSK_Lib.SetPlayerLanguages(player, data.languages)
    end
    if data.kills then
        WSK_Lib.SetPlayerKills(player, data.kills)
    end
    if data.makeup then
        WSK_Lib.ApplyMakeup(player, data.makeup)
    end
    if data.motw then
        WSK_Lib.SetPlayerMOTW(player, data.motw)
    end
    player:getModData()[checkKey] = true
end

function Listeners.ReceiveMyDeathPoint(data)
    if not data or not data.timestamp then return end
    if getTimestamp() - data.timestamp > SandboxVars.WastelandOptions.SkillKeeperDeathTpAllowTime then
        WL_UserData.Set("WSK_DeathPoint", false, getPlayer():getUsername(), true)
        return
    end

    WSK_Lib.DeathButton:create(getPlayer(), data)
end

Events.AddXP.Add(function ()
    wasXpAdded = true
end)

WL_PlayerReady.Add(function ()
    WSK_Lib.PlayerCreated()
    myDeaths = nil
    myData = nil
    didInit = false
    wasXpAdded = true
    WL_UserData.Listen("WSK_Deaths", Listeners.ReceiveMyDeaths)
    WL_UserData.Listen("WSK_Data", Listeners.ReceiveMyData)
    WL_UserData.Fetch("WSK_Deaths")
    if SandboxVars.WastelandOptions.SkillKeeperDeathTp then
        WL_UserData.Fetch("WSK_DeathPoint", Listeners.ReceiveMyDeathPoint)
    end
    if SandboxVars.WastelandOptions.SkillKeeperLoginSafetime then
        WL_Utils.makePlayerSafe(SandboxVars.WastelandOptions.SkillKeeperLoginSafetime)
    end
end)

Events.OnPlayerDeath.Add(function (player)
    local positionX = math.floor(player:getX())
    local positionY = math.floor(player:getY())
    local positionZ = math.floor(player:getZ())
    if SandboxVars.WastelandOptions.SkillKeeperDeathTp then
        local timestamp = getTimestamp()
        local username = player:getUsername()
        WL_UserData.Set("WSK_DeathPoint", {x = positionX, y = positionY, z = positionZ, timestamp = timestamp}, username, true)
    end
    if not WL_Utils.isStaff(player) and not player:getModData().NoDeathBox then
        WSK_Lib.DropItemsToBox(player)
    end
    Events.OnTick.Remove(Listeners.CheckSavePlayer)
    WL_UserData.StopListening("WSK_Deaths", Listeners.ReceiveMyDeaths)
    WL_UserData.StopListening("WSK_Data", Listeners.ReceiveMyData)

    local wasFreeDeath = false
    local zones = WEZ_EventZone.getZonesAt(positionX, positionY, positionZ)
    for _, zone in ipairs(zones) do
        if zone.freeDeathZone then
            wasFreeDeath = true
            break
        end
    end
    sendClientCommand(player, "WSK_Main", "playerDied", {wasFreeDeath})
end)

-- output a nice days/hours/minutes/seconds string
local function calcTimeAgo(seconds)
    local days = math.floor(seconds / 86400)
    seconds = seconds % 86400
    local hours = math.floor(seconds / 3600)
    seconds = seconds % 3600
    local minutes = math.floor(seconds / 60)
    seconds = seconds % 60
    local str = ""
    if days > 0 then
        str = str .. days .. " days "
    end
    if hours > 0 then
        str = str .. hours .. " hours "
    end
    if minutes > 0 then
        str = str .. minutes .. " minutes "
    end
    if seconds > 0 then
        str = str .. seconds .. " seconds "
    end
    return str
end

local function AdminManageDeathsContextEntry(context, username, deaths)
    if deaths and #deaths > 0 then
        local submenu = ISContextMenu:getNew(context)
        local option = context:addOption("Remove Deaths: " .. username, nil, nil)
        context:addSubMenu(option, submenu)
        for i, death in ipairs(deaths) do
            local timeAgo = calcTimeAgo(getTimestamp() - death)
            submenu:addOption(tostring(i) .. ": " .. timeAgo .. " ago", nil, function()
                sendClientCommand(getPlayer(), "WSK_Main", "removeDeath", {username, death})
            end)
        end
    else
        context:addOption("No Deaths: " .. username, nil, nil)
    end
end

local function AdminRestoreSkillsContextEntry(context, username, times)
    if times and #times > 0 then
        local submenu = ISContextMenu:getNew(context)
        local option = context:addOption("Restore from Archive: " .. username, nil, nil)
        context:addSubMenu(option, submenu)
        for i, time in ipairs(times) do
            local timeAgo = calcTimeAgo(getTimestamp() - time)
            submenu:addOption(tostring(i) .. ": " .. timeAgo .. " ago", nil, function()
                sendClientCommand(getPlayer(), "WSK_Main", "restoreArchive", {username, time})
            end)
        end
    else
        context:addOption("No Archives: " .. username, nil, nil)
    end
end

Events.OnFillWorldObjectContextMenu.Add(function(playerIdx, context, worldobjects)
    if myDeaths and #myDeaths > 0 then
        local submenu = ISContextMenu:getNew(context)
        local option = context:addOption("My Deaths", nil, nil)
        context:addSubMenu(option, submenu)
        for i,death in ipairs(myDeaths) do
            local timeago = calcTimeAgo(getTimestamp() - death)
            submenu:addOption(timeago .. " ago", nil, nil)
        end
    else
        context:addOption("No Logged Deaths", nil, nil)
    end

    if not WL_Utils.canModerate(getPlayer()) then return end

    local seenPlayers = {}
    for _, v in ipairs(worldobjects) do
        local movingObjects = v:getSquare():getMovingObjects()
        for i = 0, movingObjects:size() - 1 do
            local o = movingObjects:get(i)
            if instanceof(o, "IsoPlayer") then
                local username = o:getUsername()
                if not seenPlayers[username] then
                    seenPlayers[username] = true
                    WL_UserData.Fetch("WSK_Deaths", username, function (data)
                        AdminManageDeathsContextEntry(context, username, data)
                    end)
                    WL_UserData.Fetch("WSK_ArchiveTimes", username, function (data)
                        AdminRestoreSkillsContextEntry(context, username, data)
                    end)
                end
            end
        end
    end
end)