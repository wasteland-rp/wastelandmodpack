local function canTakeDeathbox(player, box)
    if WL_Utils.isStaff(player) then
        return true
    end

    local username = player:getUsername()
    local md = box:getModData()

    if md.owner == username then
        return true
    end

    local ts = getTimestamp()
    if md.time + SandboxVars.WastelandOptions.SkillKeeperDeathboxTime < ts then
        return true
    end

    return false
end

local function getContainingItem(container)
    local parent = container:getParent()
    if parent and instanceof(parent, "IsoWorldInventoryObject") then
        return parent:getItem()
    end
    return container:getContainingItem()
end

local original_WWP_ISInventoryTransferAction_transferItem = ISInventoryTransferAction.transferItem
function ISInventoryTransferAction:transferItem(item)
    local player = getPlayer()
    if item and item:getFullType() == "Base.WSKDeathBox" and not canTakeDeathbox(player, item) then
        WL_Utils.addErrorToChat("You can't take this death box.")
        return
    end

    local srcItem = getContainingItem(self.srcContainer)
    if srcItem and srcItem:getFullType() == "Base.WSKDeathBox" and not canTakeDeathbox(player, srcItem) then
        WL_Utils.addErrorToChat("You can't take items from this death box.")
        return
    end

    local destItem = getContainingItem(self.destContainer)
    if destItem and destItem:getFullType() == "Base.WSKDeathBox" and not canTakeDeathbox(player, destItem) then
        WL_Utils.addErrorToChat("You can't put items into this death box.")
        return
    end

    return original_WWP_ISInventoryTransferAction_transferItem(self, item)
end

local function AddOverrideDeathbox(player, context, items)
    if not WL_Utils.isAtLeastGM(getSpecificPlayer(player)) then return end
    items = ISInventoryPane.getActualItems(items)
    for _, item in ipairs(items) do
		if item:getFullType() == "Base.WSKDeathBox" and item:getModData().time then
            context:addOption("Unlock Deathbox", nil, function()
                item:getModData().time = 0
                item:transmitModData()
            end)
        end
	end
end

Events.OnFillInventoryObjectContextMenu.Add(AddOverrideDeathbox)