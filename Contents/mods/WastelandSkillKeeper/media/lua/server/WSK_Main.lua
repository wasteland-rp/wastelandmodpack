if isClient() then return end

require "UserData"

local function onPlayerDied(username, wasFreeDeath)
	local data = WL_UserData.GetServer(username, "WSK_Data")
	local archives = WL_UserData.GetServer(username, "WSK_Archive")
	local archiveTimes = WL_UserData.GetServer(username, "WSK_ArchiveTimes")
	local deaths = WL_UserData.GetServer(username, "WSK_Deaths")
	local now = getTimestamp()

	archives[now] = data
	table.insert(archiveTimes, now)

	WL_UserData.SetServer(username, "WSK_Archive", archives, true)
	WL_UserData.SetServer(username, "WSK_ArchiveTimes", archiveTimes, true)

	if wasFreeDeath then
		local numberOfDeaths = #deaths
		writeLog("WastelandSkillKeeper", string.format("Player %s has died in free death zone, %d deaths on record", username, numberOfDeaths))
	else
		table.insert(deaths, now)
		WL_UserData.SetServer(username, "WSK_Deaths", deaths, true)
		local numberOfDeaths = #deaths
		writeLog("WastelandSkillKeeper", string.format("Player %s has died, %d deaths on record", username, numberOfDeaths))
	end
end

local function removeDeath(username, timestamp)
	local deaths = WL_UserData.GetServer(username, "WSK_Deaths")
	for k,v in ipairs(deaths) do
		if v == timestamp then
			table.remove(deaths, k)
			WL_UserData.SetServer(username, "WSK_Deaths", deaths)
			return
		end
	end
end

local function restoreArchive(username, timestamp)
	local archives = WL_UserData.GetServer(username, "WSK_Archive")
	local data = archives[timestamp]
	if not data then print("WSK Invalid Archive Data Requested") return end
	WL_UserData.SetServer(username, "WSK_Data", data)
end

Events.OnClientCommand.Add(function (module, command, player, args)
    if module == "WSK_Main" then
        if command == "playerDied" then
			onPlayerDied(player:getUsername(), args[1])
		elseif command == "removeDeath" then
			writeLog("WastelandSkillKeeper", "Remove Death: " .. player:getUsername() .. "," .. args[1] .. "," .. args[2])
			removeDeath(args[1], args[2])
		elseif command == "restoreArchive" then
			writeLog("WastelandSkillKeeper", "Restore Archive: " .. player:getUsername() .. "," .. args[1] .. "," .. args[2])
			restoreArchive(args[1], args[2])
		end
	end
end)

-- Removes deaths older than the configured number of days
Events.EveryTenMinutes.Add(function()
	local players = getOnlinePlayers()
	if not players then return end
	for i=0, players:size()-1 do
		local player = players:get(i)
		local username = player:getUsername()
		local deaths = WL_UserData.GetServer(username, "WSK_Deaths")
		local now = getTimestamp()
		local validDeaths = {}
		for _,v in ipairs(deaths) do
			if now - v < SandboxVars.WastelandOptions.SkillKeeperExpireDays * 24 * 60 * 60 then
				table.insert(validDeaths, v)
			end
		end
		if #validDeaths ~= #deaths then
			WL_UserData.SetServer(username, "WSK_Deaths", validDeaths)
		end
	end
end)