Sandbox_EN = {
    Sandbox_WastelandSkillKeeper = "Wasteland Skill Keeper",
    Sandbox_WastelandSkillKeeper.MaxFreeDeaths = "Number of Free Deaths",
    Sandbox_WastelandSkillKeeper.ExpireDays = "Days until Deaths expire",
    Sandbox_WastelandSkillKeeper.PunishPercent = "Percentage of XP to lose",
    Sandbox_WastelandSkillKeeper.DeathTp = "Enable TP to Death Location",
    Sandbox_WastelandSkillKeeper.DeathTpAllowTime = "Time after death TP allowed (seconds)",
    Sandbox_WastelandSkillKeeper.DeathTpSafetime = "Death TP Safetime (seconds)",
    Sandbox_WastelandSkillKeeper.LoginSafetime = "Login Safetime (seconds)",
    Sandbox_WastelandSkillKeeper.DeathboxTime = "Deathbox Exclusive to Owner (seconds)",
}
