---
--- WSB_AdminPanelOverride.lua
--- 06/08/2023
---
if not isClient() then return end -- This is for MP only

require "UI/WSB_SoundboardWindow"
require "WL_Utils"

local FONT_HGT_SMALL = getTextManager():getFontHeight(UIFont.Small)

local adminPanelInitialise = ISAdminPanelUI.initialise
function ISAdminPanelUI:initialise()
	adminPanelInitialise(self)

	local btnWid = 150
    local btnHgt = math.max(25, FONT_HGT_SMALL + 3 * 2)
    local btnGapY = 5

    local last_btn = self.children[self.IDMax - 1]
    if last_btn.internal == "CANCEL" then
        last_btn = self.children[self.IDMax - 2]
    end
    local x = last_btn.x
    local y = last_btn.y + btnHgt + btnGapY

	-- Add our new button
	self.showSoundboardBtn = ISButton:new(x, y, btnWid, btnHgt, "Wasteland Soundboard", self, ISAdminPanelUI.showSoundboard)
	self.showSoundboardBtn.internal = "SOUNDBOARD"
	self.showSoundboardBtn:initialise()
	self.showSoundboardBtn:instantiate()
	self.showSoundboardBtn.borderColor = self.buttonBorderColor
	self:addChild(self.showSoundboardBtn)
	self.showSoundboardBtn.tooltip = "Open the Soundboard Window"
	self.showSoundboardBtn.enable = WL_Utils.canModerate(getPlayer())
	-- Adjust positions like the admin panel does (Copy paste)
	local width = 0
	local bottom = 0
	for _,child in pairs(self:getChildren()) do
		width = math.max(width, child:getWidth())
		bottom = math.max(bottom, child:getBottom())
	end
	for _,child in pairs(self:getChildren()) do
		if child:getX() > 10 then
			child:setX(10 + width + 20)
		end
		child:setWidth(width)
	end

end

function ISAdminPanelUI:showSoundboard()
	WSB_SoundboardWindow.show()
end