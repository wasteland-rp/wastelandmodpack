---
--- WSB_ContextMenu.lua
---
--- This solely exists for single player testing of new sounds.
---
--- 23/05/2024
---

require 'UI/WSB_SoundboardWindow'

local WSB_ContextMenu = {}

if not isClient() then -- SP Testing
	WSB_ContextMenu.doMenu = function(playerIdx, context)
		context:addOption("Soundboard", nil, WSB_SoundboardWindow.show)
	end
	Events.OnFillWorldObjectContextMenu.Add(WSB_ContextMenu.doMenu)
end
