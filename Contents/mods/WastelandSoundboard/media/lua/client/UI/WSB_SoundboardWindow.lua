---
--- WSB_SoundboardWindow.lua
--- 05/08/2023
---
require "GravyUI"
require "ISUI/ISCollapsableWindow"
require "WastelandSoundSystem"

WSB_SoundboardWindow = ISCollapsableWindow:derive("WSB_SoundboardWindow")
WSB_SoundboardWindow.instance = nil

function WSB_SoundboardWindow.show()
	if WSB_SoundboardWindow.instance then
		return
	end
	WSB_SoundboardWindow.instance = WSB_SoundboardWindow:new()
	WSB_SoundboardWindow.instance:addToUIManager()
end

function WSB_SoundboardWindow:new()
	local scale =  getTextManager():getFontHeight(UIFont.Small) / 14
	local w = 300 * scale
	local h = 340 * scale
	local o = ISCollapsableWindow:new(getCore():getScreenWidth()/2-w/2,getCore():getScreenHeight()/2-h/2, w, h)
	setmetatable(o, self)
	o.__index = self
	o:initialise()
	return o
end

function WSB_SoundboardWindow:initialise()
	self.moveWithMouse = true
	self.resizable = false

	local window = GravyUI.Node(self.width, self.height, self):pad(5, 15, 5, 5)
	local header, soundSource, body = window:rows({30, 25, 1}, 10)
	self.headerLabel = header:makeLabel("Soundboard", UIFont.Medium, nil, "center")
	local soundCheck, soundSet = soundSource:cols({1, 80}, 5)
	self.soundOnMeCombobox = soundCheck:makeComboBox()
	self.soundOnMeCombobox:addOption("On Player")

	self.soundSetBtn = soundSet:makeButton("Set Position", self, self.onSetSourcePoint)

	local select, quickOptions = body:rows({30, 1}, 5)
	local selectCombo, selectPlay = select:cols({1, 80}, 5)
	self.selectSoundCombo = selectCombo:makeComboBox()
    self.playBtn = selectPlay:makeButton("Play", self, self.playSelectedSound)

	for category, sounds in pairs(WSB_SoundLibrary) do
		self.selectSoundCombo:addOption("== " .. category .. " ==")
		for _, soundName in ipairs(sounds) do
			self.selectSoundCombo:addOptionWithData(soundName, {category, soundName})
		end
	end

	local q = {quickOptions:grid(8, {1, 60}, 5, 5)}

	local player = getPlayer()
	local md = player:getModData()

	for i=1,8 do
		self["qPlay" .. i] = q[i][1]:makeButton("", self, self.playQuick, {i})
		q[i][2]:makeButton("Set", self, self.setQuick, {i})
	end

	WL_UserData.Fetch("WSB_SoundboardWindow", function (data)
		for i=1,8 do
			if data["q" .. i] then
				self["qPlay" .. i].title = data["q" .. i]
			end
		end
	end)
end

function WSB_SoundboardWindow:playQuick(_, index)
	local soundName = self["qPlay" .. index].title
	if soundName == "" then
		return
	end
	self:playSound(soundName)
end

function WSB_SoundboardWindow:setQuick(_, index)
	local soundData = self.selectSoundCombo:getOptionData(self.selectSoundCombo.selected)
	if not soundData then
		return
	end
	self["qPlay" .. index].title = soundData[2]
	WL_UserData.Append("WSB_SoundboardWindow", {["q" .. index] = soundData[2]})
end

function WSB_SoundboardWindow:playSelectedSound()
	local soundData = self.selectSoundCombo:getOptionData(self.selectSoundCombo.selected)
	if not soundData then
		return
	end
	self:playSound(soundData[2])
end

function WSB_SoundboardWindow:onSetSourcePoint()
	local player = getPlayer()
	local sq = player:getSquare()
	self.soundOnMeCombobox:addOptionWithData("At " .. sq:getX() .. ", " .. sq:getY() .. ", " .. sq:getZ(), sq)
	self.soundOnMeCombobox.selected = #self.soundOnMeCombobox.options
end

function WSB_SoundboardWindow:playSound(soundName)
	if WastelandSoundSystem:isLoopingSound(soundName) then
		WastelandSoundSystem:stopLoopingSound(soundName)
	else
		if self.soundOnMeCombobox.selected == 1 then
			WastelandSoundSystem:playSound(soundName, getPlayer():getSquare())
		else
			local sq = self.soundOnMeCombobox:getOptionData(self.soundOnMeCombobox.selected)
			WastelandSoundSystem:playSound(soundName, sq)
		end
	end
end

function WSB_SoundboardWindow:close()
	WSB_SoundboardWindow.instance = nil
	self:removeFromUIManager()
end