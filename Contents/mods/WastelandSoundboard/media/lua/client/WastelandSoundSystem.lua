---
--- WastelandSoundSystem.lua
--- 06/08/2023
---

require "WL_Utils"

WastelandSoundSystem = {}
WastelandLoopingSounds = {}

function WastelandSoundSystem:isLoopingSound(str)
	return string.sub(str, -string.len("Loop")) == "Loop"
end

---@param soundName string
---@param square IsoGridSquare
function WastelandSoundSystem:playSound(soundName, square)
	getSoundManager():PlayWorldSound(soundName, square, 1, 100, 1, false);
end

-- This only works on the local client so I don't use it anymore :(
function WastelandSoundSystem:playLoopingSound(soundName, x, y, z)

	--TODO try this - soundEmitter:playSoundLooped
	--local soundEmitter = getWorld():getFreeEmitter(x, y, 0)
	--soundEmitter:playSound("soundName", x, y, 0)
	--soundEmitter:stopSound(arg0)

	local audio = getSoundManager():PlayWorldSoundImpl(soundName, false, x, y, z, 1, 100, 1, false);
	if audio and WastelandSoundSystem:isLoopingSound(soundName) then
		WastelandLoopingSounds[soundName] = audio
	end
end

--TODO some way to play music, so send to every client and play music locally only on their character

function WastelandSoundSystem:isActivelyLoopingSound(soundName)
	local audio = WastelandLoopingSounds[soundName]
	if(audio) then
		return true
	else
		return false
	end
end

function WastelandSoundSystem:stopLoopingSound(soundName)
	local audio = WastelandLoopingSounds[soundName]  -- fmod.fmod.Audio
	if(audio) then
		audio:stop()
		WastelandLoopingSounds[soundName] = nil
	end
end

