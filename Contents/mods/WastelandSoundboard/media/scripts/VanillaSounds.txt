module Base {
    sound ManScreaming
    {
        category = World, loop = false, is3D = true,
        clip {
            file = media/sound/distscream1.ogg,
            distanceMin = 10,
            distanceMax = 300,
            reverbFactor = 0.1,
            volume = 0.4,
        }
    }

	sound WomanScreaming
    {
        category = World, loop = false, is3D = true,
        clip {
            file = media/sound/distscream2.ogg,
            distanceMin = 10,
            distanceMax = 300,
            reverbFactor = 0.1,
            volume = 0.8,
        }
    }

    sound WomanScreaming2
    {
        category = World, loop = false, is3D = true,
        clip {
            file = media/sound/distscream3.ogg,
            distanceMin = 10,
            distanceMax = 300,
            reverbFactor = 0.1,
            volume = 0.8,
        }
    }

    sound WomanScreaming3
    {
        category = World, loop = false, is3D = true,
        clip {
            file = media/sound/distscream4.ogg,
            distanceMin = 10,
            distanceMax = 300,
            reverbFactor = 0.1,
            volume = 0.5,
        }
    }

    sound WomanShortScream
    {
        category = World, loop = false, is3D = true,
        clip {
            file = media/sound/distscream5.ogg,
            distanceMin = 10,
            distanceMax = 300,
            reverbFactor = 0.1,
            volume = 0.5,
        }
    }

    sound ManShortScream
    {
        category = World, loop = false, is3D = true,
        clip {
            file = media/sound/distscream6.ogg,
            distanceMin = 10,
            distanceMax = 300,
            reverbFactor = 0.1,
            volume = 0.8,
        }
    }

     sound ManShortScream2
    {
        category = World, loop = false, is3D = true,
        clip {
            file = media/sound/distscream7.ogg,
            distanceMin = 10,
            distanceMax = 300,
            reverbFactor = 0.1,
            volume = 0.8,
        }
    }

    sound DogBarking
    {
        category = World, loop = false, is3D = true,
        clip {
            file = media/sound/dog1.ogg,
            distanceMin = 10,
            distanceMax = 100,
            reverbFactor = 0.1,
            volume = 0.8,
        }
    }

    sound DistantRifleShot
    {
        category = World, loop = false, is3D = true,
        clip {
            file = media/sound/assaultrifledistant2.ogg,
            distanceMin = 10,
            distanceMax = 400,
            reverbFactor = 0.1,
            volume = 0.8,
        }
    }

	sound DistantRifleShot2
    {
        category = World, loop = false, is3D = true,
        clip {
            file = media/sound/assaultrifledistant5.ogg,
            distanceMin = 10,
            distanceMax = 400,
            reverbFactor = 0.1,
            volume = 0.8,
        }
    }

    sound DistantRifleBurst
    {
        category = World, loop = false, is3D = true,
        clip {
            file = media/sound/assaultrifledistant4.ogg,
            distanceMin = 10,
            distanceMax = 300,
            reverbFactor = 0.1,
            volume = 0.8,
        }
    }

    sound DistantRifleBurst2
    {
        category = World, loop = false, is3D = true,
        clip {
            file = media/sound/assaultrifledistant8.ogg,
            distanceMin = 10,
            distanceMax = 300,
            reverbFactor = 0.1,
            volume = 0.8,
        }
    }

    sound ShotgunPump
    {
        category = World, loop = false, is3D = true,
        clip {
            file = media/sound/stormyShotgunPump.ogg,
            distanceMin = 10,
            distanceMax = 20,
            reverbFactor = 0.1,
            volume = 0.8,
        }
    }

	sound BurglarAlarm
    {
        category = World, loop = false, is3D = true,
        clip {
            file = media/sound/burglar2.ogg,
            distanceMin = 10,
            distanceMax = 300,
            reverbFactor = 0.1,
            volume = 0.5,
        }
    }

	sound BurglarAlarm2
    {
        category = World, loop = false, is3D = true,
        clip {
            file = media/sound/burglar1.ogg,
            distanceMin = 10,
            distanceMax = 300,
            reverbFactor = 0.1,
            volume = 0.5,
        }
    }

	sound Helicopter
    {
        category = World, loop = false, is3D = true,
        clip {
            file = media/sound/chopper5.ogg,
            distanceMin = 10,
            distanceMax = 750,
            reverbFactor = 0.1,
            volume = 1.0,
        }
    }

	sound AlarmClock
    {
        category = World, loop = false, is3D = true,
        clip {
            file = media/sound/alarmclock.ogg,
            distanceMin = 10,
            distanceMax = 60,
            reverbFactor = 0.1,
            volume = 0.9,
        }
    }

	sound ShortBeeping
    {
        category = World, loop = false, is3D = true,
        clip {
            file = media/sound/multipleBeep.ogg,
            distanceMin = 10,
            distanceMax = 60,
            reverbFactor = 0.1,
            volume = 0.7,
        }
    }

	sound Explosion
    {
        category = World, loop = false, is3D = true,
        clip {
            file = media/sound/bigExplosion.ogg,
             distanceMin = 10,
            distanceMax = 450,
            reverbFactor = 0.1,
            volume = 1.0,
        }
    }

    sound Owl
    {
        category = World, loop = false, is3D = true,
        clip {
            file = media/sound/PZ_Owl_02.ogg,
             distanceMin = 10,
            distanceMax = 100,
            reverbFactor = 0.1,
            volume = 0.7,
        }
    }

	sound Bushes1
    {
        category = World, loop = false, is3D = true,
        clip {
            file = media/sound/bushes1.ogg,
            distanceMin = 10,
            distanceMax = 70,
            reverbFactor = 0.1,
            volume = 0.7,
        }
    }

	sound Bushes2
    {
        category = World, loop = false, is3D = true,
        clip {
            file = media/sound/bushes2.ogg,
            distanceMin = 10,
            distanceMax = 70,
            reverbFactor = 0.1,
            volume = 0.7,
        }
    }

}