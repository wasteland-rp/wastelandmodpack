Events.OnFillWorldObjectContextMenu.Add(function (playerIdx, context)
    local player = getSpecificPlayer(playerIdx)
    if WL_Utils.isStaff(player) then
        local menu = WL_ContextMenuUtils.getOrCreateSubMenu(context, "WL Admin")
        menu:addOption("Reset Starter Pack", nil, function ()
            WL_UserData.Set("WSP", {SpawnCount = 0}, player:getUsername())
        end)
    end
end)