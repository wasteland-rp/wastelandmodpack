Events.OnGameStart.Add(function ()
    WSP.KitGenerator.PrependConfig({
        onTest = function (spawnLocation, spawnCount)
            return true
        end,
        items = {
            WSP.Utilities.digitalWatch(),
            "Notebook",
            WSP.Utilities.writingImplement(),
            "WaterBottleFull",
            "Matches",
            "Saucepan",
            { id = "GoldCurrency", count = 20 },
        }
    })

    WSP.KitGenerator.PrependConfig({
        onTest = function (spawnLocation, spawnCount)
            return WSP.Utilities.isCold()
        end,
        allowOnRespawn = true,
        clothing = {
            {rolls = 1, items = {
                { "Scarf_White", 1 },
                { "Scarf_StripeBlackWhite", 1 },
                { "Scarf_StripeBlueWhite", 1 },
                { "Scarf_StripeRedWhite", 1 },
            }},
            "Gloves_WhiteTINT",
            "JacketLong_Random",
        },
    })

    WSP.KitGenerator.PrependConfig({
        onTest = function (spawnLocation, spawnCount)
            return WSP.Utilities.hasTrait("Smoker")
        end,
        items = {
            "MoreSmokes.MSCigarettePack",
        },
    })

    WSP.KitGenerator.PrependConfig({
        onTest = function (spawnLocation, spawnCount)
            return WSP.Utilities.hasTrait("Tailor")
        end,
        items = {
            {id = "DenimStrips", count = 2},
            {id = "RippedSheets", count = 4},
            "Needle",
            "Scissors",
            "Thread",
        },
    })

    WSP.KitGenerator.PrependConfig({
        onTest = function (spawnLocation, spawnCount)
            return WSP.Utilities.hasTrait("ShortSighted")
        end,
        items = {
            "Glasses_Normal",
        },
    })

    WSP.KitGenerator.PrependConfig({
        onTest = function (spawnLocation, spawnCount)
            return WSP.Utilities.hasTrait("Mechanics")
        end,
        items = {
            {id = "EngineParts", count = 5},
            "Wrench",
            "LugWrench",
        },
    })

    WSP.KitGenerator.PrependConfig({
        onTest = function (spawnLocation, spawnCount)
            return WSP.Utilities.isProfession("unemployed")
        end,
        clothing = {
            "Hat_GolfHatTINT",
        },
        items = {
            {id = "GolfBall", count = 3},
            "Golfclub",
        },
        bags = {
            "Bag_GolfBag",
        },
    })

    WSP.KitGenerator.PrependConfig({
        onTest = function (spawnLocation, spawnCount)
            return WSP.Utilities.isProfession("parkranger")
        end,
        clothing = {
            "Hat_Raccoon",
        },
        items = {
            "CraftedFishingRodTwineLine",
            "TrapStick",
            "HandAxe",
            "KCMweapons.KCM_Handmade",
            { id = "KCMweapons.WoodenBolt", count = 30 },
        },
        bags = {
            "Bag_BigHikingBag",
        },
    })

    WSP.KitGenerator.PrependConfig({
        onTest = function (spawnLocation, spawnCount)
            return WSP.Utilities.isProfession("securityguard")
        end,
        clothing = {
            "Hat_RiotHelmet",
            "HolsterSimple",
            "Shirt_OfficerWhite",
        },
        items = {
            "Nightstick",
            "Radio.WalkieTalkie3",
        },
        bags = {
            WSP.Utilities.starterBag()
        },
    })

    WSP.KitGenerator.PrependConfig({
        onTest = function (spawnLocation, spawnCount)
            return WSP.Utilities.isProfession("fireofficer")
        end,
        clothing = {
            "Tshirt_Profession_FiremanRed",
        },
        items = {
            "Axe",
            "Extinguisher",
        },
        bags = {
            "Bag_WorkerBag",
        },
    })

    WSP.KitGenerator.PrependConfig({
        onTest = function (spawnLocation, spawnCount)
            return WSP.Utilities.isProfession("constructionworker")
        end,
        items = {
            "KitchenKnife",
            "SheetRope",
            "CardDeck",
            "Screwdriver",
            "DrugMod.Cocaine",
        },
        bags = {
            "Bag_InmateEscapedBag",
        },
    })

    WSP.KitGenerator.PrependConfig({
        onTest = function (spawnLocation, spawnCount)
            return WSP.Utilities.isProfession("carpenter")
        end,
        clothing = {
            "Glasses_SafetyGoggles",
            "Hat_DustMask",
        },
        items = {
            {id = "NailsBox", count = 3},
            "Hammer",
            "Saw",
            "Screwdriver",
            "PlankNail",
            "KCMweapons.KCM_Handmade",
            { id = "KCMweapons.WoodenBolt", count = 30 },
        },
        bags = {
            "Toolbox",
        },
    })

    WSP.KitGenerator.PrependConfig({
        onTest = function (spawnLocation, spawnCount)
            return WSP.Utilities.isProfession("burglar")
        end,
        clothing = {
            "Hat_BalaclavaFull",
        },
        items = {
            {id = "Paperclip", count = 15},
            "Crowbar",
            "Screwdriver",
            "EmptySandbag",
            "Necklace_GoldDiamond",
            "Earring_Dangly_Emerald",
        },
        bags = {
            "Bag_ToolBag",
        },
    })

    WSP.KitGenerator.PrependConfig({
        onTest = function (spawnLocation, spawnCount)
            return WSP.Utilities.isProfession("chef")
        end,
        clothing = {
            "Hat_ChefHat",
            "Jacket_Chef",
            "Trousers_Chef",
        },
        items = {
            "Pan",
            "MeatCleaver",
            "KitchenKnife",
            "Onion",
            "Salt",
            "Wine",
            "TinOpener",
            "DeadRat",
            "TableLeg",
        },
        bags = {
            WSP.Utilities.starterBag()  -- 1 roll, 100% chance
        },
    })

    WSP.KitGenerator.PrependConfig({
        onTest = function (spawnLocation, spawnCount)
            return WSP.Utilities.isProfession("farmer")
        end,
        clothing = {
            "Dungarees",
            "Shirt_Workman",
        },
        items = {
            "GardenFork",
            "farming.HandShovel",
            "farming.PotatoBagSeed",
            "farming.CarrotBagSeed",
            "farming.GardeningSprayEmpty",
            "Shovel",
            "EmptySandbag",
        },
        bags = {
            WSP.Utilities.starterBag()  -- 1 roll, 100% chance
        }
    })

    WSP.KitGenerator.PrependConfig({
        onTest = function (spawnLocation, spawnCount)
            return WSP.Utilities.isProfession("fisherman")
        end,
        items = {
            "FishingRod",
            "FishingLine",
            "FishingTackle",
            "FishingTackle2",
            "FishingNet",
            "SpearHandFork",
        },
        bags = {
            WSP.Utilities.starterBag()  -- 1 roll, 100% chance
        }
    })

    WSP.KitGenerator.PrependConfig({
        onTest = function (spawnLocation, spawnCount)
            return WSP.Utilities.isProfession("doctor")
        end,
        clothing = {
            "Hat_SurgicalMask_Blue",
            "Hat_SurgicalCap_Blue",
            "Trousers_Scrubs",
            "Tshirt_Scrubs",
            "Gloves_Surgical",
        },
        items = {
            {id = "SutureNeedle", count = 4},
            {id = "Scalpel", count = 3},
            "SutureNeedleHolder",
            "Tweezers",
            "Tissue",
            "JacketLong_Doctor",
            "WoodenMallet",
        },
        bags = {
            "Bag_DoctorBag",
        },
    })

    WSP.KitGenerator.PrependConfig({
        onTest = function (spawnLocation, spawnCount)
            return WSP.Utilities.isProfession("nurse")
        end,
        items = {
            {id = "AlcoholBandage", count = 6},
            {id = "Antibiotics", count = 2},
            "AlcoholWipes",
            "Pills",
            "SutureNeedle",
        },
        bags = {
            "Bag_MedicalBag",
        },
    })

    WSP.KitGenerator.PrependConfig({
        onTest = function (spawnLocation, spawnCount)
            return WSP.Utilities.isProfession("engineer")
        end,
        clothing = {
            "Boilersuit_Yellow",
        },
        items = {
            {id = "ScrapMetal", count = 3},
            {id = "EngineParts", count = 5},
            "Screwdriver",
            "PipeWrench",
            "Hammer",
            "ElectronicsScrap",
            "ElectricWire",
            "RadioReceiver",
            "Battery",
            "Screws",
            "DuctTape",
            "KCMweapons.KCM_Handmade",
            { id = "KCMweapons.WoodenBolt", count = 30 },
        },
        bags = {
            "Bag_WorkerBag",
        },
    })

    WSP.KitGenerator.PrependConfig({
        onTest = function (spawnLocation, spawnCount)
            return WSP.Utilities.isProfession("metalworker")
        end,
        clothing = {
            "Boilersuit_BlueRed",
        },
        items = {
            {id = "ScrapMetal", count = 4},
            {id = "SmallSheetMetal", count = 2},
            {id = "Screws", count = 2},
            "BlowTorch",
            "BallPeenHammer",
            "Screwdriver",
            "WeldingMask",
            "Base.MetalPipe",
        },
        bags = {
            WSP.Utilities.starterBag()  -- 1 roll, 100% chance
        }
    })

    WSP.KitGenerator.PrependConfig({
        onTest = function (spawnLocation, spawnCount)
            return WSP.Utilities.isProfession("gunsmith")
        end,
        items = {
            {id = "ScrapMetal", count = 4},
            {id = "GunPowder", count = 4},
            {id = "MachinedFirearmComponents", count = 2},
            "ShellCasingsBox",
            "MetalPipe",
            "Hairspray",
            "Wire",
            "DuctTape",
            "Screwdriver",
            "BallPeenHammer",
            "Wrench",
        },
        bags = {
            "Bag_WeaponBag",
        }
    })

    WSP.KitGenerator.PrependConfig({
        onTest = function (spawnLocation, spawnCount)
            return WSP.Utilities.isProfession("veteran")
        end,
        clothing = {
            "HolsterSimple",
            "Jacket_CoatArmy",
        },
        items = {
            "GreysWLWeaponsFirearms.BoltActionScrapPistol",
            "Bullets357Box",
            "HuntingKnife",
        },
        bags = {
            "Bag_Military",
        }
    })

    WSP.KitGenerator.PrependConfig({
        onTest = function (spawnLocation, spawnCount)
            return WSP.Utilities.isProfession("lumberjack")
        end,
        items = {
            "WoodAxe",
            "HandAxe",
            "Bandage",
            "CheeseSandwich",
            "BeerWaterFull",
        },
        bags = {
            WSP.Utilities.starterBag()  -- 1 roll, 100% chance
        }
    })

    WSP.KitGenerator.PrependConfig({
        onTest = function (spawnLocation, spawnCount)
            return WSP.Utilities.isProfession("mechanics")
        end,
        items = {
            {id = "EngineParts", count = 8},
            "SheetMetal",
            "TirePump",
            "Wrench",
            "LugWrench",
            "Jack",
            "WaterBottlePetrol",
            "BlowTorch",
            "Screwdriver",
            "WeldingMask",
        },
        bags = {
            "Toolbox",
        },
    })

    WSP.KitGenerator.PrependConfig({
        onTest = function (spawnLocation, spawnCount)
            return WSP.Utilities.isProfession("policeofficer")
        end,
        clothing = {
            "HolsterSimple",
            "Shirt_OfficerWhite",
        },
        items = {
            "Nightstick",
            "Radio.WalkieTalkie3",
            "KCMweapons.KCM_Handmade",
            { id = "KCMweapons.WoodenBolt", count = 30 },
        },
        bags = {
            WSP.Utilities.starterBag()  -- 1 roll, 100% chance
        }
    })

    WSP.KitGenerator.PrependConfig({
        onTest = function (spawnLocation, spawnCount)
            return WSP.Utilities.isProfession("coastguard")
        end,
        clothing = {
            "HolsterSimple",
            "MPonchoYellowUP",
            "Tshirt_Profession_FiremanBlue",
        },
        items = {
            "HandTorch",
            {id = "AlcoholBandage", count = 3},
            "HuntingKnife",
            { id = "Vest_Hunting_Orange", customName = "Coast Guard Life Vest" },
            "KCMweapons.KCM_Handmade",
            { id = "KCMweapons.WoodenBolt", count = 30 },
        },
        bags = {
            WSP.Utilities.starterBag()  -- 1 roll, 100% chance
        }
    })

    WSP.KitGenerator.PrependConfig({
        onTest = function (spawnLocation, spawnCount)
            return WSP.Utilities.isProfession("operative")
        end,
        clothing = {

        },
        items = {
            {id = "AlcoholBandage", count = 2},
            {id = "Paperclip", count = 15},
            "HuntingKnife",
            "UndeadSurvivor.PrepperFlashlight",
            "Hat_BalaclavaFull",
            "Screwdriver",
            "IcePick",
            "Radio.WalkieTalkie3"
        },
        bags = {
           "Bag_DuffelBagTINT"
        }
    })

    WSP.KitGenerator.PrependConfig({
        onTest = function (spawnLocation, spawnCount)
            return WSP.Utilities.isProfession("tailor")
        end,
        items = {
            {id = "LeatherStrips", count = 4},
            {id = "DenimStrips", count = 7},
            {id = "RippedSheets", count = 10},
            {id = "KevlarSheet", count = 2},
            "Needle",
            "Scissors",
        },
        bags = {
            WSP.Utilities.starterBag()  -- 1 roll, 100% chance
        }
    })

    WSP.KitGenerator.PrependConfig({
        onTest = function (spawnLocation, spawnCount)
            return WSP.Utilities.isProfession("electrician")
        end,
        items = {
            {id = "ElectronicsScrap", count = 10},
            {id = "Battery", count = 4},
            {id = "ElectricWire", count = 2},
            "Screwdriver",
            "CarBatteryCharger",
            "LightBulb",
            "RadioTransmitter",
            "RadioReceiver",
            WSP.Utilities.starterWeapon()
        },
        bags = {
            "Bag_ToolBag",
        },
    })

    WSP.KitGenerator.PrependConfig({
        onTest = function (spawnLocation, spawnCount)
            return WSP.Utilities.isProfession("burgerflipper")
        end,
        items = {
            {id = "RippedSheets", count = 6},
            "Needle",
            "Scissors",
            "TinOpener",
            "Pan",
            "RollingPin",
            "KitchenKnife",
            "Ham",
            "Cheese",
            "Bread",
        },
        bags = {
            "Cooler",
        },
    })

    WSP.KitGenerator.PrependConfig({
        onTest = function (spawnLocation, spawnCount)
            return WSP.Utilities.isProfession("fitnessInstructor")
        end,
        clothing = {
            "Hat_Fedora",
            "Hat_GasMask",
            "JacketLong_Random",
        },
        items = {
            {id = "Battery", count = 2},
            "Shoes_RedTrainers",
            "GranolaBar",
            "UndeadSurvivor.PrepperFlashlight",
            "Pop",
            "Pop2",
            "Pop3",
            "Snackcakes_Plonkies",
            "HiHis",
            "CookieChocolateChip",
            "LicoriceBlack",
            "Bandage",
            WSP.Utilities.starterWeapon()
        },
        bags = {
            "Bag_Satchel",
        },
    })

    WSP.KitGenerator.PrependConfig({
        onTest = function (spawnLocation, spawnCount)
            return WSP.Utilities.isProfession("racer")
        end,
        clothing = {
            "Glasses_SkiGoggles",
            "Hat_BandanaTINT",
        },
        items = {
            {id = "EngineParts", count = 4},
            "TirePump",
            "Wrench",
            "WhiskeyPetrol",
            "WhiskeyFull",
            "SpearIcePick",
        },
        bags = {
            WSP.Utilities.starterBag()  -- 1 roll, 100% chance
        }
    })

    WSP.KitGenerator.PrependConfig({
        onTest = function (spawnLocation, spawnCount)
            return WSP.Utilities.isProfession("homeless")
        end,
        clothing = {
            "MPonchoYellowDOWN",
        },
        items = {
            "DeadRabbit",
            "AlarmClock2",
            "TinCanEmpty",
            "Spoon",
            "Bowl",
            "Toothbrush",
            "TrapMouse",
            {id = "RippedSheets", count = 6},
            "Needle",
            "Scissors",
            "ClosedUmbrellaBlue",
            "CraftedFishingRodTwineLine",
            "IcePick",
        },
        bags = {
            "Bag_DuffelBag",
        },
    })

    WSP.KitGenerator.PrependConfig({
        onTest = function (spawnLocation, spawnCount)
            return WSP.Utilities.isProfession("librarian")
        end,
        clothing = {
            "Glasses_Reading",
        },
        items = {
            WSP.Utilities.starterWeapon(),
            "KCMweapons.KCM_Handmade",
            { id = "KCMweapons.WoodenBolt", count = 30 },
            {id = "Book", count = 3},
            {id = "Peppermint", count = 7},
            {id = "Battery", count = 2},
            "Notebook",
            "Pencil",
            "Newspaper",
            "Magazine",
            "ComicBook",
            "Torch",
            {rolls = 4, items = {
                { "BookCarpentry1", 1 },
                { "BookCarpentry2", 1 },
                { "BookCarpentry3", 1 },
                { "BookCarpentry4", 1 },
                { "BookCarpentry5", 1 },
                { "BookCooking1", 1 },
                { "BookCooking2", 1 },
                { "BookCooking3", 1 },
                { "BookCooking4", 1 },
                { "BookCooking5", 1 },
                { "BookElectrician1", 1 },
                { "BookElectrician2", 1 },
                { "BookElectrician3", 1 },
                { "BookElectrician4", 1 },
                { "BookElectrician5", 1 },
                { "BookFarming1", 1 },
                { "BookFarming2", 1 },
                { "BookFarming3", 1 },
                { "BookFarming4", 1 },
                { "BookFarming5", 1 },
                { "BookFirstAid1", 1 },
                { "BookFirstAid2", 1 },
                { "BookFirstAid3", 1 },
                { "BookFirstAid4", 1 },
                { "BookFirstAid5", 1 },
                { "BookForaging1", 1 },
                { "BookForaging2", 1 },
                { "BookForaging3", 1 },
                { "BookForaging4", 1 },
                { "BookForaging5", 1 },
                { "BookFishing1", 1 },
                { "BookFishing2", 1 },
                { "BookFishing3", 1 },
                { "BookFishing4", 1 },
                { "BookFishing5", 1 },
                { "BookMechanic1", 1 },
                { "BookMechanic2", 1 },
                { "BookMechanic3", 1 },
                { "BookMechanic4", 1 },
                { "BookMechanic5", 1 },
                { "BookMetalWelding1", 1 },
                { "BookMetalWelding2", 1 },
                { "BookMetalWelding3", 1 },
                { "BookMetalWelding4", 1 },
                { "BookMetalWelding5", 1 },
                { "BookTailoring1", 1 },
                { "BookTailoring2", 1 },
                { "BookTailoring3", 1 },
                { "BookTailoring4", 1 },
                { "BookTailoring5", 1 },
                { "BookTrapping1", 1 },
                { "BookTrapping2", 1 },
                { "BookTrapping3", 1 },
                { "BookTrapping4", 1 },
                { "BookTrapping5", 1 },
                { "WastelandBooks.WastelandStrength1", 1 },
                { "WastelandBooks.WastelandStrength2", 1 },
                { "WastelandBooks.WastelandStrength3", 1 },
                { "WastelandBooks.WastelandStrength4", 1 },
                { "WastelandBooks.WastelandStrength5", 1 },
                { "WastelandBooks.WastelandFitness1", 1 },
                { "WastelandBooks.WastelandFitness2", 1 },
                { "WastelandBooks.WastelandFitness3", 1 },
                { "WastelandBooks.WastelandFitness4", 1 },
                { "WastelandBooks.WastelandFitness5", 1 },
                { "WastelandBooks.WastelandGunsmith1", 1 },
                { "WastelandBooks.WastelandGunsmith2", 1 },
                { "WastelandBooks.WastelandGunsmith3", 1 },
                { "WastelandBooks.WastelandGunsmith4", 1 },
                { "WastelandBooks.WastelandGunsmith5", 1 },
            }},
        },
        bags = {
            "Bag_Satchel",
        },
    })
end)
