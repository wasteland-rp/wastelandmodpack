
WL_PlayerReady.Add(function ()
    local player = getPlayer()
    if not player:getModData().WSP_DidSpawnAlready then
        WL_UserData.Fetch("WSP", player:getUsername(), function (data)
            player:getModData().WSP_DidSpawnAlready = true
            local spawnCount = data and data.SpawnInCount or 0
            local kit = WSP.KitGenerator.GetKitForPlayer(player, spawnCount)
            kit:apply(player)
            WL_UserData.Set("WSP", {SpawnInCount = spawnCount + 1},player:getUsername())
        end)
    end
end)