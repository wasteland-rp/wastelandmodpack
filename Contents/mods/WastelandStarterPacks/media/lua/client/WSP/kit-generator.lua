WSP = WSP or {}

WSP.KitGenerator = WSP.KitGenerator or {}
WSP.KitConfigs = WSP.KitConfigs or {}


function WSP.KitGenerator.AppendConfig(config)
    table.insert(WSP.KitConfigs, config)
end

function WSP.KitGenerator.PrependConfig(config)
    table.insert(WSP.KitConfigs, 1, config)
end

function WSP.KitGenerator.GetKitForPlayer(player, spawnCount)
    local spawnLocation = WSP.KitGenerator.GetSpawnLocation(player)

    local items = {}
    local clothing = {}
    local bags = {}
    local stripClothing = false
    local stripItems = false
    local damageClothing = 0

    firstSpawn = spawnCount == 0

    print("WSP: Finding kit for " .. player:getUsername() .. " at " .. spawnLocation .. " (spawnCount=" .. spawnCount .. ")");

    local skipRest = false
    for _, kitConfig in ipairs(WSP.KitConfigs) do
        if kitConfig.onTest(spawnLocation, spawnCount) and (firstSpawn or kitConfig.allowOnRespawn) and not skipRest then
            if kitConfig.items then
                if kitConfig.overrideItems then
                    items = {}
                end
                for _, item in ipairs(kitConfig.items) do
                    table.insert(items, item)
                end
            end
            if kitConfig.clothing then
                if kitConfig.overrideClothing then
                    clothing = {}
                end
                for _, item in ipairs(kitConfig.clothing) do
                    table.insert(clothing, item)
                end
            end
            if kitConfig.bags then
                if kitConfig.overrideBags then
                    bags = {}
                end
                for _, item in ipairs(kitConfig.bags) do
                    table.insert(bags, item)
                end
            end
            if kitConfig.stripAll or kitConfig.stripClothing then
                stripClothing = true
            end
            if kitConfig.stripAll or kitConfig.stripItems then
                stripItems = true
            end
            if kitConfig.skipRest then
                skipRest = true
            end
            if kitConfig.damageClothing then
                damageClothing = math.max(damageClothing, kitConfig.damageClothing)
            end
        end
    end

    local kit = WSP.Kit:new()
    kit:initializeFromConfig({
        items = items,
        clothing = clothing,
        bags = bags,
        stripClothing = stripClothing,
        stripItems = stripItems,
        damageClothing = damageClothing,
    })
    return kit
end

function WSP.KitGenerator.GetSpawnLocation(player)
    local spawnRegions = SpawnRegionMgr.getSpawnRegionsAux()
    for _, spawnRegion in ipairs(spawnRegions) do
        for _, points in pairs(spawnRegion.points) do
            for _, point in ipairs(points) do
                local x = point.worldX * 300 + point.posX
                local y = point.worldY * 300 + point.posY
                local z = point.posZ
                if math.abs(player:getX() - x) <= 2 and math.abs(player:getY() - y) <= 2 and math.abs(player:getZ() - z) <= 2 then
                    return spawnRegion.name
                end
            end
        end
    end
    return "unknown"
end

function WSP.KitGenerator.GetTraits(player)
    local traits = ArrayList.new()
    for i=0,player:getTraits():size()-1 do
        traits:add(player:getTraits():get(i))
    end
    return traits
end