WSP = WSP or {}

WSP.Kit = WSP.Kit or WLBaseObject:derive("WSP.Kit")


local function doRolls(item, list)
    local total = 0
    for _, rollItem in ipairs(item.items) do
        total = total + rollItem[2]
    end
    for i = 1, item.rolls do
        local roll = ZombRand(total)
        local current = 0
        local didAdd = false
        for _, rollItem in pairs(item.items) do
            current = current + rollItem[2]
            if roll <= current and not didAdd then
                if not item.chance or ZombRand(100) <= item.chance then
                    table.insert(list, rollItem[1])
                end
                didAdd = true
            end
        end
    end
    return list
end

function WSP.Kit:new()
    local o = self:super()
    o:initializeEmpty()
    return o
end

function WSP.Kit:initializeEmpty()
    self._items = {}
    self._clothing = {}
    self._bags = {}
    self._stripClothing = false
    self._stripItems = false
    self._damageClothing = 0

    self._bagItems = {}
end

function WSP.Kit:initializeFromConfig(config)
    self._items = config.items or {}
    self._clothing = config.clothing or {}
    self._bags = config.bags or {}
    self._stripClothing = config.stripClothing or false
    self._stripItems = config.stripItems or false
    self._damageClothing = config.damageClothing or 0
end

function WSP.Kit:apply(player)
    if self._stripClothing then
        print("WSP: Stripping clothing")
        self:stripClothing(player)
    end
    if self._stripItems then
        print("WSP: Stripping items")
        self:stripItems(player)
    end
    print("WSP: Applying clothing")
    self:applyClothing(player)
    if self._damageClothing > 0 then
        print("WSP: Damaging clothing")
        self:damageClothing(player)
    end
    print("WSP: Applying bags")
    self:applyBags(player)
    print("WSP: Applying items")
    self:applyItems(player)
end

function WSP.Kit:stripClothing(player)
    local wornItems = player:getWornItems()
    print("WSP: Stripping " .. wornItems:size() .. " worn items")
    for i=wornItems:size()-1,0,-1  do
        print("WSP: Removing worn item " .. tostring(i))
        local item = wornItems:get(i)
        player:removeWornItem(item:getItem())
        player:getInventory():DoRemoveItem(item:getItem())
    end
end

function WSP.Kit:stripItems(player)
    local items = player:getInventory():getItems()
    local wornItems = player:getWornItems()
    local function isWorn(item)
        for i=0,wornItems:size()-1 do
            if item == wornItems:get(i):getItem() then
                return true
            end
        end
        return false
    end
    print("WSP: Stripping " .. items:size() .. " items")
    for i=items:size()-1,0,-1 do
        print("WSP: Removing item " .. tostring(i))
        local item = items:get(i)
        if item:getType() ~= "KeyRing" and not isWorn(item) then
            player:getInventory():DoRemoveItem(item)
        end
    end
end

function WSP.Kit:applyClothing(player)
    local clothings = {}
    for _, item in ipairs(self._clothing) do
        if type(item) == "string" then
            table.insert(clothings, item)
        else
            if item.rolls then
                doRolls(item, clothings)
            else
                if not item.chance or ZombRand(100) <= item.chance then
                    table.insert(clothings, item.id)
                end
            end
        end
    end
    for _, item in ipairs(clothings) do
        local i = player:getInventory():AddItem(item)
        if i then
            player:setWornItem(i:getBodyLocation(), i)
        end
    end
end

function WSP.Kit:damageClothing(player)
    local numBodyParts = BloodBodyPartType.MAX:index()
    for i=0,numBodyParts-1 do
        if ZombRand(100) <= self._damageClothing then
            local type = BloodBodyPartType.FromIndex(i)
            player:addHole(type)
        end
    end
end

function WSP.Kit:applyBags(player)
    local bags = {}
    for _, item in ipairs(self._bags) do
        if type(item) == "string" then
            table.insert(bags, item)
        else
            if item.rolls then
                doRolls(item, bags)
            else
                if not item.chance or ZombRand(100) <= item.chance then
                    table.insert(bags, item.id)
                end
            end
        end
    end
    for i, item in ipairs(bags) do
       local bag = player:getInventory():AddItem(item)
       if bag then
           table.insert(self._bagItems, bag)
           if i == 1 then
                local location = instanceof(bag, "InventoryContainer") and bag:canBeEquipped() or ""
                if location ~= "" then
                    player:setWornItem(location, bag)
                else
                    player:setSecondaryHandItem(bag)
                end
           end
        else
           print("WSP: Bag is nil")
       end
    end
end

function WSP.Kit:applyItems(player)
    local items = {}

    for _, item in pairs(self._items) do
        if type(item) == "string" then
            table.insert(items, item)
        elseif item.rolls then
            doRolls(item, items)
        else
            if not item.chance or ZombRand(100) <= item.chance then
                table.insert(items, item)
            end
        end
    end

    for _, item in ipairs(items) do
        local id = type(item) == "string" and item or item.id
        local count = type(item) == "string" and 1 or item.count or 1
        local inHands = type(item) == "string" and false or item.inHands or false
        local customName = type(item) == "string" and nil or item.customName or nil
        local toAdd = {}
        for i=1,count do
            table.insert(toAdd, InventoryItemFactory.CreateItem(id))
        end
        if customName then
            customName = string.gsub(customName, "{username}", player:getUsername())
            for _, toAddItem in ipairs(toAdd) do
                toAddItem:setName(customName)
                toAddItem:setCustomName(true)
            end
        end
        for _, toAddItem in ipairs(toAdd) do
            if inHands then
                self:addItemToHands(player, toAddItem)
            else
                self:addItemToBag(player, toAddItem)
            end
        end
    end
end

function WSP.Kit:addItemToBag(player, item)
    if item == nil then
        print("WSP: Item is nil")
        return
    end
    if #self._bagItems == 0 then
        self:addItemToHands(player, item)
    else
        local bag = self._bagItems[1]
        bag:getInventory():AddItem(item)
    end
end

function WSP.Kit:addItemToHands(player, item)
    if item == nil then
        print("WSP: Item is nil")
        return
    end
    player:getInventory():AddItem(item)
end