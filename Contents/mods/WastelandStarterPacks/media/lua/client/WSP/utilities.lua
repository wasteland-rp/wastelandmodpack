WSP = WSP or {}
WSP.Utilities = WSP.Utilities or {}

--- Helper Functions
function WSP.Utilities.combine(tbl1, tbl2)
    local tbl = {}
    for _, v in ipairs(tbl1) do
        table.insert(tbl, v)
    end
    for _, v in ipairs(tbl2) do
        table.insert(tbl, v)
    end
    return tbl
end
function WSP.Utilities.rollList(rolls, chance, items)
    return {rolls = rolls or 1, chance = chance or 100, items = items}
end
function WSP.Utilities.isProfession(profession)
    return getPlayer():getDescriptor():getProfession() == profession
end
function WSP.Utilities.hasTrait(trait)
    return getPlayer():HasTrait(trait)
end
function WSP.Utilities.getAverageTemp()
    return getClimateManager():getClimateForecaster():getForecast():getTemperature():getTotalMean()
end
function WSP.Utilities.isCold()
    return true -- return WSP.Utilities.getAverageTemp() < 10
end

function WSP.Utilities.digitalWatch(rolls, chance)
    return WSP.Utilities.rollList(rolls, chance, {
        { "WristWatch_Left_DigitalRed", 1 },
        { "WristWatch_Left_DigitalBlack", 1 },
    })
end

function WSP.Utilities.booksAndMags(rolls, chance)
    return WSP.Utilities.rollList(rolls, chance, {
        { "Book", 5},
        { "ComicBook", 10},
        { "Crossword1", 20},
        { "Crossword2", 20},
        { "Crossword3", 20},
        { "HottieZ", 1},
        { "Magazine", 10},
        { "MagazineWordsearch1", 20},
        { "MagazineWordsearch2", 20},
        { "MagazineWordsearch3", 20},
        { "Journal", 10},
    })
end

function WSP.Utilities.plushies(rolls, chance)
    return WSP.Utilities.rollList(rolls, chance, {
            { "BorisBadger", 1 },
            { "JacquesBeaver", 1 },
            { "FluffyfootBunny", 1 },
            { "FreddyFox", 1 },
            { "PancakeHedgehog", 1 },
            { "MoleyMole", 1 },
            { "Spiffo", 1 },
            { "FurbertSquirrel", 1 },
    })
end

function WSP.Utilities.funJunk(rolls, chance)
    return WSP.Utilities.rollList(rolls, chance, {
        { "BackgammonBoard", 1 },
        { "CameraExpensive", 1 },
        { "CheckerBoard", 1 },
        { "GamePieceBlack", 1 },
        { "CatToy", 1 },
        { "Money", 1 },
        { "CardDeck", 1 },
        { "Yoyo", 1 },
    })
end

function WSP.Utilities.writingImplement(rolls, chance)
    return WSP.Utilities.rollList(rolls, chance, {
        {"Pen", 1},
        {"BluePen", 1},
        {"Pencil", 4}
    })
end


function WSP.Utilities.starterWeapon(rolls, chance)
    return WSP.Utilities.rollList(rolls, chance, {
        { "GreysWLWeaponsMelee.FemurKnife", 1 },
        { "Base.HandAxe", 1 },
        { "Base.LeadPipe", 1 },
        { "Base.MeatCleaver", 1 },
        { "Base.Nightstick", 1 },
        { "Base.PipeWrench", 1 },
        { "Base.SpearIcePick", 1 },
        { "Base.PlankNail", 1 },
        { "Base.PlankNail", 1 },
        { "Base.TableLeg", 1 },
    })
end

function WSP.Utilities.starterBag(rolls, chance)
    return WSP.Utilities.rollList(rolls, chance, {
        { "Bag_DuffelBagTINT", 1 },
        { "Bag_Military", 1 }, -- duffle
        { "Postal_Worker_Satchel", 4 },
        { "Handbag", 6 },
        { "Bag_Satchel", 7},
        { "Bag_Schoolbag", 10 },
        { "Bag_Packsport_Rim", 10 },
    })
end