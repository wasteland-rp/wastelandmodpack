if isClient() then return end

WVD = {}

function WVD.UpdateSandbox(sandbox, option, daylength)
    option:setValue(daylength)
    sandbox:applySettings()
    sandbox:toLua()
    sandbox:saveServerLuaFile(getServerName())
    sendServerCommand("WVD", "setDayLength", {daylength})
end

function WVD.SetDaylengthSpeed()
    local sandboxOptions = getSandboxOptions()
    local gameTime = getGameTime()
    local option = sandboxOptions:getOptionByName("DayLength")
    local currentDayLength = option:getValue()
    local dayTimeStartHour = SandboxVars.WastelandVariableDays.DaytimeStart
    local nightTimeStartHour = SandboxVars.WastelandVariableDays.NighttimeStart
    local dayTimeValue = SandboxVars.WastelandVariableDays.DayLength
    local nightTimeValue = SandboxVars.WastelandVariableDays.NightLength
    local currentHour = gameTime:getHour()

    -- Is Halloween
    if gameTime:getMonth() == 9 and gameTime:getDay() == 30 then
        if currentDayLength ~= 25 then
            WVD.UpdateSandbox(sandboxOptions, option, 25)
        end
        return
    end

    -- Is Christmas
    if gameTime:getMonth() == 11 and gameTime:getDay() == 24 then
        if currentDayLength ~= 25 then
            WVD.UpdateSandbox(sandboxOptions, option, 25)
        end
        return
    end

    if currentHour < dayTimeStartHour or currentHour >= nightTimeStartHour then
        if currentDayLength ~= nightTimeValue then
            WVD.UpdateSandbox(sandboxOptions, option, nightTimeValue)
        end
    else
        if currentDayLength ~= dayTimeValue then
            WVD.UpdateSandbox(sandboxOptions, option, dayTimeValue)
        end
    end
end

Events.EveryHours.Add(WVD.SetDaylengthSpeed)
Events.OnGameStart.Add(WVD.SetDaylengthSpeed)