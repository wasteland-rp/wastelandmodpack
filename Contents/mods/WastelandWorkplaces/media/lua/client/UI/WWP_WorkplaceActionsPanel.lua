---
--- WWP_WorkplaceActionsPanel.lua
--- 03/08/2024
---

require "GravyUI_WL"

WWP_WorkplaceActionsPanel = ISPanel:derive("WWP_WorkplaceActionsPanel")

local FONT_HGT_SMALL = getTextManager():getFontHeight(UIFont.Small)
local FONT_HGT_MEDIUM = getTextManager():getFontHeight(UIFont.Medium)
local FONT_HGT_LARGE = getTextManager():getFontHeight(UIFont.Large)

local COLOR_WHITE = {r=1,g=1,b=1,a=1}
local COLOR_YELLOW = {r=1,g=1,b=0,a=1}

function WWP_WorkplaceActionsPanel:new(x, y, width, height, workplace, workplacePanel)
	local o = ISPanel:new(x, y, width, height)
	setmetatable(o, self)
	self.__index = self
	o.workplace = workplace
	o.workplacePanel = workplacePanel
	o.images = {}
	o:initialise()
	return o
end

function WWP_WorkplaceActionsPanel:initialise()
	ISPanel.initialise(self)
	local win =  GravyUI.Node(self.width, self.height, self)
	win = win:pad(15, 15, 15, 20)
	local earningMoney, actions =	win:rows({ FONT_HGT_LARGE * 2, win.height - (FONT_HGT_LARGE * 2) - 15}, 15)
	local rowPadding = 10
	local moneyRow, moneyExplanation = earningMoney:rows({ FONT_HGT_LARGE, FONT_HGT_LARGE }, rowPadding)
	local moneyTitle, moneyValue = moneyRow:cols( {0.9, 0.1})
	self.moneyValueNode = moneyValue
	self.moneyTexture =  getTexture("media/ui/coins-bonus.png")
	moneyTitle:makeLabel("Earning Currency", UIFont.Large, COLOR_WHITE, "left")
	local salary = self.workplace.type:getSalaryWithBonuses(self.workplace:getTown())
	moneyValue:makeLabel(string.format("%.2f", salary), UIFont.Medium, COLOR_YELLOW, "left")
	if self.workplace.type:requireCustomersForRewards() then
		moneyExplanation:makeLabel("You can earn local currency by roleplaying inside this area with a customer.", UIFont.Small, COLOR_WHITE, "left")
	else
		moneyExplanation:makeLabel("You can earn local currency by spending time inside this area.", UIFont.Small, COLOR_WHITE, "left")
	end

	local headers, actionsArea =	actions:rows({ 0.06, 0.94}, 15)
	local iconsTitle, nameTitle, skillTitle, costTitle = headers:cols({0.18, 0.38, 0.1, 0.32}, 20)
	iconsTitle:makeLabel("Items", UIFont.Large, COLOR_WHITE, "left")
	nameTitle:makeLabel("Name", UIFont.Large, COLOR_WHITE, "left")
	skillTitle:makeLabel("Skill", UIFont.Large, COLOR_WHITE, "left")
	costTitle:makeLabel("Work Points", UIFont.Large, COLOR_WHITE, "center")

	local totalActions = #self.workplace.type.actions
	local rowPixelSize = 30
	local rowPixels = {}
	for i=1,totalActions do
		table.insert(rowPixels, rowPixelSize)
	end

	local allRows = {actionsArea:rows(rowPixels, 15)}
	local rowNumber = 1
	for _, action in ipairs(self.workplace.type.actions) do
		local icons, name, skill, cost, button = allRows[rowNumber]:cols({0.18, 0.40, 0.23, 0.04, 0.15}, 10)
		local maxIcons = 3
		if icons.width < 90 then
			maxIcons = 2
		end

		self:loadIconsFromTable(icons, action.items, maxIcons)

		local skillName = "None"
		if action.skill then
			skillName = action.skill:getName()
		end

		name:makeLabel(action.name, UIFont.Medium, COLOR_WHITE, "left", true)
		skill:makeLabel(skillName, UIFont.Medium, COLOR_WHITE, "left", true)
		cost:makeLabel(tostring(action.work), UIFont.Medium, COLOR_WHITE, "left")
		local startButton = button:makeButton("Start", self, self.onAction, {action})

		if action.mod and not getActivatedMods():contains(action.mod) then
			startButton:setTooltip("Mod " .. action.mod .. " is not active")
			startButton.enable = false
		end

		if action.skill and action.minSkill then
			local playerSkill = getPlayer():getPerkLevel(action.skill)
			if playerSkill < action.minSkill then
				startButton:setTooltip("Requires " .. action.skill:getName() .. " " .. tostring(action.minSkill))
				startButton.enable = false
			end
		end

		rowNumber = rowNumber + 1
	end

end

local function getIconTexture(itemID)
	local item = getScriptManager():getItem(itemID)
	if not item then
	--	error("Can't find item: " .. itemID)
		return
	end
	local icon = item:getIcon()

	if item:getIconsForTexture() and not item:getIconsForTexture():isEmpty() then
		icon = item:getIconsForTexture():get(0)
	end

	local texture = nil
	if icon then
		texture = getTexture("Item_" .. icon)
	end

	if not texture then
		texture = getTexture("media/textures/Item_" .. icon .. ".png") -- Try our best to guess
	end

	return texture
end

function WWP_WorkplaceActionsPanel:loadIconsFromTable(parentNode, lootTable, maxIcons)
	local totalIcons = 0
	for itemID, _ in pairs(lootTable) do
		if totalIcons == maxIcons then
			break
		end

		local offset = totalIcons * 32
		totalIcons = totalIcons + 1

		local itemTexture = getIconTexture(itemID)
		if itemTexture then
			table.insert(self.images, { node = parentNode, xOffset=offset, texture = itemTexture })
		end
	end
end

function WWP_WorkplaceActionsPanel:prerender()
	ISPanel.prerender(self)
	self:drawTextureScaled(self.moneyTexture, self.moneyValueNode.left - 35, self.moneyValueNode.top, 28, 28, 1, 1.0, 1.0, 1.0)
	for _, image in ipairs(self.images) do
		if image.texture:getWidth() > 32 or image.texture:getHeight() > 32 then
			self:drawTextureScaled(image.texture, image.node.left + image.xOffset, image.node.top, 32, 32, 1, 1.0, 1.0, 1.0)
		else
			self:drawTexture(image.texture, image.node.left + image.xOffset, image.node.top, 1, 1.0, 1.0, 1.0)
		end

	end
end


function WWP_WorkplaceActionsPanel:updateState()

end

function WWP_WorkplaceActionsPanel:onAction(_, action)
	ISTimedActionQueue.add(WWP_WorkplaceTimedAction:new(getPlayer(), action, self.workplace))
end

