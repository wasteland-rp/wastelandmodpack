---
--- WWP_GovernmentType.lua
--- 22/07/2024
---

WWP_GovernmentType = {
	ANARCHY = {displayName = "Anarchy", key = "anarchy", bonusIcon="red-coins.png", bonusText="0%", incomeBonus=0,
	           bonusTextColor={r=1,g=0,b=0,a=1 }},
	AUTOCRACY = {displayName = "Autocracy", key = "autocracy", bonusIcon="coins-bonus.png",
	             bonusText="+18%", incomeBonus=18, bonusTextColor={r=1,g=1,b=0,a=1 }},
	MONARCHY = {displayName = "Monarchy", key = "monarchy", bonusIcon="coins-bonus.png",
	            bonusText="+18%", incomeBonus=18, bonusTextColor={r=1,g=1,b=0,a=1 }},
	OLIGARCHY = {displayName = "Oligarchy", key = "oligarchy", bonusIcon="coins-bonus.png",
	             bonusText="+21%", incomeBonus=21, bonusTextColor={r=1,g=1,b=0,a=1 }},

	-- Disabled until we have elections coded
	--REPUBLIC = {displayName = "Republic", key = "republic", bonusIcon="coins-bonus.png", bonusText="+15%",
	-- bonusTextColor={r=1,g=1,b=0,a=1 }},

	--COMMUNIST_STATE = {displayName = "Communist State", key = "communist_state"},
	--THEOCRATIC_STATE = {displayName = "Theocratic State", key = "theocratic_state"},
}