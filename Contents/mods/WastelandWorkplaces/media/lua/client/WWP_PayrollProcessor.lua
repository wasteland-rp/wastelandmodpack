---
--- WWP_PayrollProcessor.lua
--- 27/07/2024
---

WWP_PayrollProcessor = {}

--- Pays a salary after deducting tax. Decimals are resolved via RNG. This function assumes town bonuses have already
--- been applied (e.g. +10% to bars or whatever)
---@param baseSalaryDecimal number can be decimal, must 0 or higher
---@param town table|nil the local town, can be nil if the player is not in one
---@param salaryPrefix string describing the source of income e.g. Waitress, Bar, Mayor. Should be capitalised.
---@param townSalaryCategory string optional parameter, if set then the town will pay from treasury instead of taxing
function WWP_PayrollProcessor.paySalary(player, baseSalaryDecimal, town, salaryPrefix, townSalaryCategory)
	if baseSalaryDecimal == 0 then return end
	local salaryInteger = WWP_PayrollProcessor.randomRound(baseSalaryDecimal)
	local username = player:getUsername()

	if town and town:isExile(username) then
		WL_Utils.addToChat("You are an exile here and cannot earn wages", { color = "1.0,0.2,0", })
		return
	end

	if townSalaryCategory then
		WWP_PayrollProcessor.payTownSalary(player, town, salaryInteger, salaryPrefix, townSalaryCategory)
	else
		WWP_PayrollProcessor.payRegularSalary(player, town, salaryInteger, salaryPrefix, username)
	end
end

function WWP_PayrollProcessor.payTownSalary(player, town, salaryInteger, salaryPrefix, townSalaryCategory)
	WWP_TownLedger.getClient():attemptWithdrawal(town.id, salaryInteger, townSalaryCategory,
			function(_, success, newBalance)
				if success then
					WWP_PlayerStats.deductWorkPoints(player, WWP_TownZone.PAY_WORK_POINTS_DEDUCTED)
					WWP_PayrollProcessor.giveCurrency(player, salaryInteger)
					WWP_PayrollProcessor.reportSalary(player, salaryPrefix, salaryInteger, "",
							76, 154, 237)
				else
					player:setHaloNote("Your town is bankrupt and can't pay you", 250, 20, 60, 800.0)
				end
			end)
end

function WWP_PayrollProcessor.payRegularSalary(player, town, salaryInteger, salaryPrefix, username)
	local taxRate = WWP_PayrollProcessor.calculateTaxRate(town, username)
	local finalSalary = salaryInteger
	local taxInteger = 0
	local taxRateString = ""

	if taxRate > 0 then
		taxInteger = WWP_PayrollProcessor.calculateTax(salaryInteger, taxRate)
		if taxInteger > 0 and town.type ~= WWP_TownType.NPC_HUB then
			WWP_TownLedger.getClient():makeDeposit(town.id, taxInteger, WWP_TownLedger.INCOME_TAX)
		end
		taxRateString = " (" .. tostring(taxInteger) .. " lost to " .. tostring(taxRate) ..  "% tax)"
		finalSalary = salaryInteger - taxInteger
	end

	WWP_PlayerStats.deductWorkPoints(player, WWP_WorkplaceZone.PAY_WORK_POINTS_DEDUCTED)
	WWP_PayrollProcessor.giveCurrency(player, finalSalary)
	WWP_PayrollProcessor.reportSalary(player, salaryPrefix, finalSalary, taxRateString, 253, 216, 12)
end

function WWP_PayrollProcessor.calculateTaxRate(town, username)
	if not town then return 0 end

	local taxRate = town.incomeTaxRate
	if not town:isCitizen(username) then
		taxRate = taxRate + 20
		WL_Utils.addToChat("Non-citizen status incurs an extra 20% tax on your earnings in this town.",
				{ color = "1.0,0.5,0", })
	end
	return taxRate
end

---@param income number expected to be a whole number, of the income to be taxed
---@param taxRate number between 0 and 100
---@return number a whole number representing the money lost to tax. This needs to be deducted from the income.
function WWP_PayrollProcessor.calculateTax(salary, taxRate)
	local taxRateDecimal = taxRate / 100 -- Convert tax rate to decimal (e.g., 40% becomes 0.4)
	local taxDecimal = salary * taxRateDecimal  -- Calculate the exact tax amount (can be a decimal) e.g. 4 * 0.4 = 1.6
	local taxInteger = math.floor(taxDecimal)  -- Get the integer part of the tax e.g. 1 or 2
	local taxFraction = taxDecimal - taxInteger  -- -- Calculate the fractional part of the tax e.g. 1.6 - 1 = 0.6

	-- Determine if the fractional part goes to tax or salary
	local randomChance = ZombRand(0, 100)  -- Returns a number from 0 to 99
	if randomChance < (taxFraction * 100) then
		taxInteger = taxInteger + 1 -- If the random number is less than the fractional part * 100, round up
	end
	return taxInteger
end

function WWP_PayrollProcessor.giveCurrency(player, currencyAmount)
	local inventory = player:getInventory()
	local newItem
	for _ = 1, currencyAmount do
		newItem = inventory:AddItem("GoldCurrency")
	end

	if(newItem) then
		getSoundManager():playUISound("CoinsClinkHeavy")
		WL_Utils.addToChat("Gained Item: " .. newItem:getName() .. " (" .. tostring(currencyAmount) .. ")",
				{ color = "1.0,0.8,0.2", })
	end
end

function WWP_PayrollProcessor.reportSalary(player, salarySource, salary, taxRateString, r, g, b)
	if not taxRateString then taxRateString = "" end
	local salaryStr = salarySource .. " salary: " .. tostring(salary) .. taxRateString
	salaryStr = salaryStr .. "\n"  .. WWP_PlayerStats.getWorkPointsRemainingString(player)
	player:setHaloNote(salaryStr, r, g, b, 800.0)
end

--- Takes a decimal and rounds it up or down randomly according to how close it is to either whole number
function WWP_PayrollProcessor.randomRound(number)
	local integerPart = math.floor(number)
	if integerPart == number then return number
	end
	local decimalPart = number - integerPart
	local randomChance = ZombRand(0, 100) -- ZombRand(0, 100) will return a number from 0 to 99
	if randomChance < (decimalPart * 100) then -- If the random number is less than the decimal part * 100, round up
		return integerPart + 1
	else
		return integerPart
	end
end