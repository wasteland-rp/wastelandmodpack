---
--- WWP_Town.lua
--- 17/07/2024
---
require 'WL_Utils'

WWP_Town = WLBaseObject:derive("WWP_Town")

--- @type table WWP_Town[]
WWP_Towns = WWP_Towns or {}

--- Get all towns at a given location
--- @param x number
--- @param y number
--- @param z number
---@return table WWP_Town or nil
function WWP_Town.findTownAt(x, y, z)
	for _, town in pairs(WWP_Towns) do
		if town.zone:isInZone(x, y, z) then
			return town
		end
	end
	return nil
end

function WWP_Town.getPlayerTownMembership(username, isHubSearch)
	for _, town in pairs(WWP_Towns) do
		if town:isCitizen(username) and town:isHub() == isHubSearch then
			return town
		end
	end
	return nil
end

--- Makes a brand new town
--- @param name string human readable name
function WWP_Town.createTown(name, zoneCoordinates)
	local town = WWP_Town:new()
	town.name = name
	town.id = getRandomUUID()
	town.type = WWP_TownType.DEN
	town.governmentType = WWP_GovernmentType.ANARCHY
	town.incomeTaxRate = 0
	town.salesTaxRate = 0
	town.citizenshipCost = 0
	town.lawsText = "This settlement is a lawless wasteland"
	town.leadership = "?"
	town.citizens = {}
	town.exiles = {}
	town.upgrades = {}
	town.zone = WWP_TownZone:new(town, zoneCoordinates)
	WL_TriggerZones.addZone(town.zone)
	town:init()
	WWP_Towns[town.id] = town
	return town
end

--- Creates a new Town
function WWP_Town:new()
	return WWP_Town.parentClass.new(self)
end

function WWP_Town:init()
	self.zone.mapType = "Town"
	self.zone.mapColor = {0.3, 0.74, 1.0}
end

function WWP_Town:setName(newName)
	self.name = newName
	self:save()
end

function WWP_Town:setLeadershipName(newName)
	self.leadership = newName
	self:save()
end

function WWP_Town:setTownType(newType)
	self.type = newType
	self:setIncomeTaxRate(self.incomeTaxRate) -- Adjusts to min/max tax rate and then calls self:save() for us
end

function WWP_Town:setGovernmentType(newType)
	self.governmentType = newType
	if newType == WWP_GovernmentType.ANARCHY then
		self.leadership = "None"
		self.citizenshipCost = 0
	end

	-- Tax rate caps may change so we need to re-cap them to be sure
	self.incomeTaxRate = math.max(self:getMinIncomeTaxRate(), math.min(self.incomeTaxRate, self:getMaxIncomeTaxRate()))
	self.salesTaxRate = math.max(self:getMinSalesTaxRate(), math.min(self.salesTaxRate, self:getMaxSalesTaxRate()))
	self:save()
end

function WWP_Town:getMinIncomeTaxRate()
	if self.governmentType == WWP_GovernmentType.ANARCHY then return 0 end
	if self.type.isHub then return 20 else return 15 end
end

function WWP_Town:getMaxIncomeTaxRate()
	if self.governmentType == WWP_GovernmentType.ANARCHY then return 0 end
	return 40
end

function WWP_Town:getMinSalesTaxRate()
	if self.governmentType == WWP_GovernmentType.ANARCHY then return 0 end
	if self.type.isHub then return 6 end
	return 4
end

function WWP_Town:getMaxSalesTaxRate()
	if self.governmentType == WWP_GovernmentType.ANARCHY then return 0 end
	return 30
end

function WWP_Town:setIncomeTaxRate(newTaxRate)
	local newRate = tonumber(newTaxRate)
	assert(newRate, "Tax rate is not a number")
	self.incomeTaxRate = math.max(self:getMinIncomeTaxRate(), math.min(newRate, self:getMaxIncomeTaxRate()))
	self:save()
end

function WWP_Town:setSalesTaxRate(newTaxRate)
	local newRate = tonumber(newTaxRate)
	assert(newRate, "Tax rate is not a number")
	self.salesTaxRate = math.max(self:getMinSalesTaxRate(), math.min(newRate, self:getMaxSalesTaxRate()))
	self:save()
end

function WWP_Town:setCitizenshipCost(newCost)
	local newCostNum = tonumber(newCost)
	assert(newCostNum, "Citizen cost is not a number")
	newCostNum = math.max(0, newCostNum)
	self.citizenshipCost = newCostNum
	self:save()
end

function WWP_Town:isHub()
	return self.type.isHub == true
end

function WWP_Town:getCitizenCount()
	local count = 0
	for _ in pairs(self.citizens) do
		count = count + 1
	end
	return count
end

function WWP_Town:isCitizen(username)
	return self.citizens[username] ~= nil
end

function WWP_Town:getGovernmentRankName(rankNumber)
	return WWP_TownRank.DEFAULT_RANK_NAMES[rankNumber] -- Later we can allow customisation of these
end

function WWP_Town:getPlayerPermissionLevel()
	if WL_Utils.canModerate(getPlayer()) then return WWP_TownRank.STAFF end
	return self:getGovernmentRank(getPlayer():getUsername())
end

--- Get the salary for a player in the town zone, if any. Can be safely called for anyone, including non-citizens
---@return number between 0 and 2, can be a decimal. Never nil.
function WWP_Town:getSalary(username)
	local playerRank = self:getGovernmentRank(username)
	if playerRank <= WWP_TownRank.CITIZEN then
		return 0 -- Not a town employee
	elseif playerRank == WWP_TownRank.ENFORCEMENT_LOWEST then
		return 1
	elseif playerRank == WWP_TownRank.ENFORCEMENT_OFFICER then
		return 1.1
	elseif playerRank == WWP_TownRank.ENFORCEMENT_MANAGER then
		return 1.2
	elseif playerRank == WWP_TownRank.ENFORCEMENT_LIEUTENANT then
		return 1.3
	elseif playerRank == WWP_TownRank.ENFORCEMENT_HIGHEST then
		return 1.4
	elseif playerRank == WWP_TownRank.ENFORCEMENT_LEADER then
		return 1.6
	elseif playerRank == WWP_TownRank.GOVERNMENT_LOWEST then
		return 1
	elseif playerRank == WWP_TownRank.GOVERNMENT_CLERK then
		return 1.1
	elseif playerRank == WWP_TownRank.GOVERNMENT_MANAGER then
		return 1.2
	elseif playerRank == WWP_TownRank.GOVERNMENT_ADVISOR then
		return 1.4
	elseif playerRank == WWP_TownRank.GOVERNMENT_HIGHEST then
		return 1.5
	elseif playerRank >= WWP_TownRank.TOWN_LEADER then
		return 2
	else
		return 0 -- Default case, shouldn't occur with given ranks
	end
end

---@return number of government rank, 0 if just a citizen, -1 if not even a citizen here. Never nil.
function WWP_Town:getGovernmentRank(username)
	return self.citizens[username] or -1
end

function WWP_Town:setGovernmentRank(username, rank)
	self.citizens[username] = rank
	self:save()
end

function WWP_Town:setLaws(lawsText)
	self.lawsText = lawsText
	self:save()
end

function WWP_Town:addCitizen(username)
	self.citizens[username] = WWP_TownRank.CITIZEN
	self:save()
end

function WWP_Town:removeCitizen(username)
	self.citizens[username] = nil
	self:save()
end

function WWP_Town:isExile(username)
	return self.exiles[username] ~= nil
end

function WWP_Town:addExile(username)
	self.exiles[username] = true
	self:save()
end

function WWP_Town:removeExile(username)
	self.exiles[username] = nil
	self:save()
end

function WWP_Town:getExileCost()
	if self.type.isHub then return 50 end
	return 10
end

function WWP_Town:addUpgrade(upgrade)
	self.upgrades[upgrade.key] = upgrade
	self:save()
	self:updateMonthlyTransactions()
end

function WWP_Town:removeUpgrade(upgrade)
	self.upgrades[upgrade.key] = nil
	self:save()
	self:updateMonthlyTransactions()
end

function WWP_Town:updateMonthlyTransactions()
	local transactions = { withdrawals = {} }
	for _, upgrade in pairs(self.upgrades) do
		if upgrade.cost then
			transactions.withdrawals[upgrade.key] = upgrade.cost
		end
	end
	WWP_TownLedger.getClient():setMonthlyTransactions(self.id, transactions)
end

function WWP_Town:getWorkplaces()
	local zonesFound = {}
	for _, workplace in pairs(WWP_WorkplaceZone.getAllZones()) do
		if self.zone:isInZone(workplace.minX, workplace.minY, workplace.minZ) then
			table.insert(zonesFound, workplace)
		end
	end
	return zonesFound
end

---@return number the bonus guilders the player is paid for working here e.g. pass in 1 and get back 0.17 if 17% bonus
function WWP_Town:getSalaryBonus(baseSalary, isCustomer, workplaceTypeKey)
	local bonus = 0
	if isCustomer then
		local customerBonus = self.type.customerWorkplaceBonus
		if customerBonus then
			bonus = bonus + (baseSalary * (customerBonus/100))
		end
	end

	for _, improvedType in ipairs(self.type.improvedWorkplaces) do
		if improvedType == workplaceTypeKey then
			bonus = bonus + (baseSalary * 0.1)
		end
	end

	local incomeBonus = self.governmentType.incomeBonus
	if incomeBonus and incomeBonus > 0 then
		bonus = bonus + (baseSalary *  (incomeBonus/100))
	end

	return bonus
end

local function findTownTypeByKey(key)
	for _, townType in pairs(WWP_TownType) do
		if townType.key == key then
			return townType
		end
	end
	return nil
end

local function findGovernmentTypeByKey(key)
	for _, governmentType in pairs(WWP_GovernmentType) do
		if governmentType.key == key then
			return governmentType
		end
	end
	return nil
end

local function deserialiseUpgrades(keysTable)
	local upgrades = {}
	if not keysTable then return upgrades end
	for _, key in ipairs(keysTable) do
		for _, upgrade in pairs(WWP_TownUpgrade) do
			if upgrade.key == key then
				upgrades[key] = upgrade
				break
			end
		end
	end
	return upgrades
end

local function serialiseUpgrades(upgrades)
	local keys = {}
	if not upgrades then return keys end
	for key, _ in pairs(upgrades) do
		table.insert(keys, key)
	end
	return keys
end

function WWP_Town:updateFrom(serialisedData)
	self.name = serialisedData.name
	self.id = serialisedData.id
	self.type = findTownTypeByKey(serialisedData.typeKey)
	self.governmentType = findGovernmentTypeByKey(serialisedData.governmentTypeKey)
	self.incomeTaxRate = serialisedData.incomeTaxRate
	self.salesTaxRate = serialisedData.salesTaxRate
	self.citizenshipCost = serialisedData.citizenshipCost
	self.lawsText = serialisedData.lawsText
	self.leadership = serialisedData.leadership
	self.citizens = serialisedData.citizens
	self.exiles = serialisedData.exiles
	self.upgrades = deserialiseUpgrades(serialisedData.upgrades)
	if not self.zone then
		self.zone = WWP_TownZone:new(self, serialisedData.zone)
		WL_TriggerZones.addZone(self.zone)
	else
		self.zone:setAreaFromTable(serialisedData.zone)
	end
	self:init()
end

function WWP_Town:save()
	local serialisedData = {
		id = self.id,
		name = self.name,
		typeKey = self.type.key,
		governmentTypeKey = self.governmentType.key,
		incomeTaxRate = self.incomeTaxRate,
		salesTaxRate = self.salesTaxRate,
		citizenshipCost = self.citizenshipCost,
		lawsText = self.lawsText,
		leadership = self.leadership,
		citizens = self.citizens,
		exiles = self.exiles,
		upgrades = serialiseUpgrades(self.upgrades),
		zone = self.zone:toCoordinatesTable(),
	}
	WWP_TownSystem:saveTown(getPlayer(), serialisedData)
end

function WWP_Town:delete()
	WWP_TownSystem:deleteTown(getPlayer(), self.id)
end

function WWP_Town:dispose()
	WL_TriggerZones.removeZone(self.zone)
	self.zone:delete()
end