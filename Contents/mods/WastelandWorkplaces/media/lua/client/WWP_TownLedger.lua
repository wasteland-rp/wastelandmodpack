---
--- WWP_TownLedger.lua
--- 11/08/2024
---
require 'WLE_Client'

WWP_TownLedger = {}

-- Deposits
WWP_TownLedger.INCOME_TAX = "incomeTax"
WWP_TownLedger.SALES_TAX = "salesTax"
WWP_TownLedger.CITIZENSHIP_FEE = "citizenshipFee"
WWP_TownLedger.MANUALLY_ADDED = "manuallyAdded"

-- Withdrawals
WWP_TownLedger.SALARY_CIVIL = "salaryGovernment"
WWP_TownLedger.SALARY_ENFORCEMENT = "salaryEnforcement"
WWP_TownLedger.STAFF_REMOVAL = "staffRemoval"
WWP_TownLedger.EXILE_FEE = "exileFee"

function WWP_TownLedger.getClient()
	return WLE_Client.getClient("WastelandTowns")
end


