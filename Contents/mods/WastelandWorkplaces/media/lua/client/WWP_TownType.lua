---
--- WWP_TownType.lua
--- 18/07/2024
---


WWP_TownType = {
	NPC_HUB = {displayName = "Trade Hub (NPC)", key = "hub_npc", banner="npc-hub-town.png",
	           bonusIcon="trade-bonus.png", bonusText="+50%", isHub = true, customerWorkplaceBonus=50,
	           improvedWorkplaces = {}, openSound="TownTradeHub" },

	HUB = {displayName = "Trade Hub", key = "hub", banner="hub-town.png",
	       bonusIcon="trade-bonus.png", bonusText="+50%", isHub = true, customerWorkplaceBonus=50,
	       improvedWorkplaces = {}, openSound="TownTradeHub" },

	FARMING = { displayName = "Farming Hamlet", key = "farminghamlet", banner="farming-town.png",
	            bonusIcon="grain-bonus.png", bonusText="+10%", improvedWorkplaces = {"farm"},
	            openSound="TownFarm"},

	WOODLAND = { displayName = "Woodland Outpost", key = "woodlandoutpost", banner="forest-town.png",
	             bonusIcon="tree-bonus.png", bonusText="+10%", improvedWorkplaces = {"logging_camp", "hunting_grounds"},
	             openSound="TownWoodland"},

	MINING = { displayName = "Mining Town", key = "miningtown", banner="mining-town.png",
	           bonusIcon="mine-bonus.png", bonusText="+10%", improvedWorkplaces = {"mine"}, openSound="TownMining" },

	FACTORY = { displayName = "Factory Town", key = "factorytown", banner="factory-town.png",
	            bonusIcon="shells-bonus.png", bonusText="+10%",
	            improvedWorkplaces = {"munitions_factory", "gun-crafter"}, openSound="TownFactory"},

	DEN = { displayName = "Drifter's Den", key = "den", banner="drifter-den.png",
	        bonusIcon="barrel-bonus.png", bonusText="+10%", improvedWorkplaces = {"drug_lab", "brewery"},
	        openSound="TownDrifers" },

	FISHING = { displayName = "Fishing Village", key = "fishingvillage", banner="fishing-village.png",
	            bonusIcon="fishing-bonus.png", bonusText="+10%", improvedWorkplaces = {"fishing_pier" },
	            openSound="TownFishing" },

	MOTOR = { displayName = "Motorworks", key = "motorworks", banner="motorworks.png",
	          bonusIcon="mechanic-bonus.png", bonusText="+10%",  improvedWorkplaces = {"mechanic_shop", "scrapyard"},
	          openSound="TownMotorworks" },
}