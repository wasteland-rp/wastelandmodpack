---
--- WWP_TownUpgrade.lua
--- 31/08/2024
---
if not isClient() then return end

WWP_TownUpgrade = {
	GENERATOR_ONE = { key = "gen1", name = "Municipal Generator", cost = 25, banner = "upgrade-generator1.png",
	                  prerequisites = {}, needsTicket = true,
	                  description = "Provides constant power to a public building,\nmaintained and fueled by local work crews",
	                  requirements = "You must place the generator by a building accessible by\nvisitors, ideally in an area with workplaces and not private homes.\nExcessive strain on the generator will cause it to explode.",
	                  instructions = "You need to open a ticket with a screenshot of where you\nwant staff to place the generator.",
	},

	GENERATOR_TWO = { key = "gen2", name = "Secondary Generator", cost = 25, banner = "upgrade-generator2.png",
	                  prerequisites = { "gen1" }, needsTicket = true,
	                  description = "Provides power to a second public building,\nmaintained and fueled by local work crews",
	                  requirements = "You must place the generator by a building accessible by\nvisitors, ideally in an area with workplaces and not private homes.\nExcessive strain on the generator will cause it to explode.",
	                  instructions = "Open a ticket with a screenshot of where you\nwant staff to place the generator.",
	},

	DOCK = { key = "dock", name = "Dock", cost = 40, banner = "upgrade-dock.png",
	         prerequisites = {}, needsTicket = true,
	         description = "A dock allows for ferries to transport\n people to and from the town for a small fee",
	         requirements = "You must build a dock of at least 8 x 4 tiles by the water.\nIt should be accessible to all citizens and visitors.",
	         instructions = "Open a ticket with a screenshot of your dock.",
	},

	SHUTTLE = { key = "shuttle", name = "Short-Range Shuttle", cost = 15, banner = "upgrade-shuttle.png",
				prerequisites = {}, needsTicket = true,
				description = "A shuttle bus allows for short-range transport\nto a nearby neighboring settlement",
				requirements = "You must designate a small area for a bus stop within\nthe settlement. It should be accessible to all citizens and visitors.\nBoth settlements must have their own bus stop and may only\nhave one connection to another settlement.",
				instructions = "Open a ticket with a screenshot of the area.\nProvide coordinates within the ticket.",
	},
}