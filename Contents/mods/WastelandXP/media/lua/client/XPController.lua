---
--- XPController.lua
--- Manages XP gains on Wasteland RP server
--- By Zoomies
--- 10/10/2022
---

WastelandXP_SkillBonus = {
	Aiming = 0.0,
	Axe = 0.2,
	Blunt = 0.5,
	Brewing = 0.5,
	Cooking = 1.0,
	Cultivation = 4,
	Doctor = 1.0,
	Electricity = 1.0,
	Farming = 1.0,
	Fishing = 0.5,
	Fitness = 1.0,
	Lightfoot = 1.0,
	LongBlade = 0.2,
	Maintenance = 1.0,
	Mechanics = 1.0,
	MetalWelding = 1.0,
	Nimble = 1.0,
	PlantScavenging = 0.0,
	PseudonymousEdPiano = 1.0,
	Reloading = 0.0,
	SmallBlade = 0.5,
	SmallBlunt = 0.5,
	Sneak = 1.0,
	Spear = 0.5,
	Sprinting = 1.0,
	Strength = 1.0,
	Tailoring = 0.5,
	Trapping = 0.5,
	WineMaking = 1.5,
	Woodwork = 0.5,
	Gunsmith = 0.5,
}

WastelandXP_CappedPerks = {
    Aiming = true,
    Cooking = true,
    Doctor = true,
    Electricity = true,
    Farming = true,
    Fishing = true,
    Mechanics = true,
    MetalWelding = true,
    PlantScavenging = true,
    Reloading = true,
    Tailoring = true,
    Trapping = true,
    Woodwork = true,
	Gunsmith = true,
}

---@param character IsoGameCharacter
local function isPiercingBullets(character)
	local weapon = character:getPrimaryHandItem()
	if not weapon then return false end
	local piercing = weapon:isPiercingBullets()
	if not piercing then return false end
	return piercing
end

---@param character IsoGameCharacter
local function getMaxHitCount(character)
	local weapon = character:getPrimaryHandItem()
	if not weapon then return 1 end
	local maxHit = weapon:getMaxHitCount()
	if not maxHit then return 1 end
	return maxHit
end

---@param character IsoGameCharacter
local function isUsingNailgun(character)
	local weapon = character:getPrimaryHandItem()
	if not weapon then return false end
	return weapon:getType() == "NailGun"
end

--- Calculates extra XP based on the weapon type. This stops sawn-off shotguns with 5x XP from being insanely
--- overpowered for leveling up with, while helping weak leveling weapons like pistols.
---@param character IsoGameCharacter
---@param xp number the original xp value for the shot
local function calculateExtraAimXP(character, xp)
	local maxHit = getMaxHitCount(character)
	if maxHit == 1 then
		if isPiercingBullets(character) then
			return xp * 1.7  -- Bolt action and Semi-auto rifles
		else
			if isUsingNailgun(character) then
				return xp * 2 -- Nail guns
			else
				return xp * 2.5 -- Mostly just pistols
			end
		end
	end

	if maxHit == 2 then
		return xp * 1.5 -- This is assault rifles and the M60
	end

	if maxHit == 3 then
		return xp * 0.66 -- 3x Shotguns
	end

	return 0  -- Sawn-off 5x Shotguns
end

---@param character IsoGameCharacter
---@param perk PerkFactory.Perk
---@param xp Float
local function giveBonusAimAndReloadXp(character, perk, xp)
    local perkId = perk:getId()
    if(perkId == "Aiming" or perkId == "Reloading") then

	    local extraXP = 0
	    if perkId == "Reloading" then
		    extraXP = xp * 2.0 -- Adds 200% more, bringing us to 1.5 XP gain if server is set to 0.5 XP gain globally
	    else -- Aiming
		    extraXP = calculateExtraAimXP(character, xp)
	    end
	    local baseXP = xp + extraXP

	    -- Calculate some easing of the 37% XP penalty that IS put on for 5+ aim and reload levels, based on perk level
        local bonusXP = 0
        local perkLevel = character:getPerkLevel(perk)
        local perkBoost = character:getXp():getPerkBoost(perk)
        if(perkId == "Aiming" and perkLevel > 4 and perkBoost ~= 0) then
            bonusXP = (0.21 * perkBoost) * (baseXP / 0.37)
        end

        if(perkId == "Reloading" and perkLevel > 4 and perkBoost ~= 0) then
            bonusXP = (0.25 * perkBoost) * (baseXP / 0.25)
        end

	    if(isDebugEnabled()) then
		    local gainedXp = string.format(" +%.2f", tostring(baseXP + bonusXP))
		    HaloTextHelper.addTextWithArrow(character, perk:getName() .. " " .. gainedXp, true,
				    HaloTextHelper.getColorGreen())
	    end

	    local totalXPtoAdd = bonusXP + extraXP
	    if(totalXPtoAdd > 0) then
		    character:getXp():AddXP(perk, totalXPtoAdd, false, false, false)
	    end
    end
end

-- Nimble caps (Not implemented in the end)
--if perk == Perks.Nimble then
--	local perkBoost = character:getXp():getPerkBoost(perk)
--	if(perkBoost == 0) then return 3 end
--	if(perkBoost == 1) then return 4 end
--	if(perkBoost == 2) then return 5 end
--	if(perkBoost == 3) then return 6 end
--end

---@param character IsoGameCharacter
---@param perk PerkFactory.Perk
---@return number|nil
function getLevelCap(character, perk)
	local perkId = perk:getId()
	if not WastelandXP_CappedPerks[perkId] then
        return nil
    end
    local perkBoost = character:getXp():getPerkBoost(perk)
    return 4 + (2 * math.min(3, perkBoost))
end

---@param character IsoGameCharacter
---@param perk PerkFactory.Perk
---@return boolean true if capped or false otherwise
local function doLevelCapping(character, perk)
    local cap = getLevelCap(character, perk)
    if not cap then
        return false
    end
    local perkLevel = character:getPerkLevel(perk)
    if perkLevel >= cap then  -- If we have hit the cap, set XP to the cap level exactly
        character:getXp():setXPToLevel(perk, cap)
        return true
    end
    return false
end

local function giveBonusXP(character, perk, xp)
    local bonusMultiplier = WastelandXP_SkillBonus[perk:getId()]
    if(bonusMultiplier and bonusMultiplier > 0) then
        local bonusXP = xp * bonusMultiplier
        character:getXp():AddXP(perk, bonusXP, false, false, false)
    end
end

local lastTimeBoopedZombie = 0

---@param character IsoGameCharacter
---@param perk PerkFactory.Perk
---@param xp Float
local function onXP(character, perk, xp)
	local perkId = perk:getId()
    if perkId == "Strength" or perkId == "Fitness" then
        return -- We never do anything with these skills and they spam too much
    end

	if perkId == "Nimble" then
		-- This will "undo" the XP gain if the player has not booped a zombie in the last 3 minutes
		if getTimestamp() - lastTimeBoopedZombie > 180 then
			local currentXp = character:getXp():getXP(perk)
			local newXP = currentXp - xp
			if newXP <= 0 then
				return
			end
			local currentLevel = character:getPerkLevel(perk)
			local currentLevelStartXp = perk:getTotalXpForLevel(currentLevel)
			local toAddXp = newXP - currentLevelStartXp
			if toAddXp < 0 then
				toAddXp = 0
			end
			if toAddXp > 0 then
				character:getXp():setXPToLevel(perk, currentLevel)
				character:getXp():AddXP(perk, toAddXp, false, false, false)
			end
		-- If the player has booped a zombie in the last 3 minutes, give 3x XP
		else
			character:getXp():AddXP(perk, xp * 3, false, false, false)
		end

		return
	end

    local capped = doLevelCapping(character, perk)

    if(not capped) then
        giveBonusAimAndReloadXp(character, perk, xp)
        giveBonusXP(character, perk, xp)
    end
end

local function logZombieBoop(character, otherCharacter)
	if character and getPlayer() == character and otherCharacter and otherCharacter:isZombie() then
		lastTimeBoopedZombie = getTimestamp()
	end
end

Events.AddXP.Add(onXP)
Events.OnWeaponHitCharacter.Add(logZombieBoop)