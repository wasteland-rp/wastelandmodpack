---
--- AdjustAttachedWeapons.lua
---
--- Some zombies spawn carrying weapons, this file is where we adjust those.
---
--- 24/08/2023
---

if AttachedWeaponDefinitions.assaultRifleOnBack then
	AttachedWeaponDefinitions.assaultRifleOnBack.weapons = {} -- Remove the actual Assault Rifles
	if getActivatedMods():contains("firearmmod_WL") then -- Add some varied rifles from b41 firearms
		table.insert(AttachedWeaponDefinitions.assaultRifleOnBack.weapons, "Base.Winchester94")
		table.insert(AttachedWeaponDefinitions.assaultRifleOnBack.weapons, "Base.Winchester73")
		table.insert(AttachedWeaponDefinitions.assaultRifleOnBack.weapons, "Base.Rossi92")
		table.insert(AttachedWeaponDefinitions.assaultRifleOnBack.weapons, "Base.Rugerm7722")
		table.insert(AttachedWeaponDefinitions.assaultRifleOnBack.weapons, "Base.VarmintRifle")
		table.insert(AttachedWeaponDefinitions.assaultRifleOnBack.weapons, "Base.HuntingRifle")
	else
		table.insert(AttachedWeaponDefinitions.assaultRifleOnBack.weapons, "Base.HuntingRifle")
		table.insert(AttachedWeaponDefinitions.assaultRifleOnBack.weapons, "Base.VarmintRifle")
	end
end

if AttachedWeaponDefinitions.huntingRifleOnBack then
	if getActivatedMods():contains("firearmmod_WL") then -- Add some varied rifles from b41 firearms
		table.insert(AttachedWeaponDefinitions.huntingRifleOnBack.weapons, "Base.Winchester94")
		table.insert(AttachedWeaponDefinitions.huntingRifleOnBack.weapons, "Base.Winchester73")
		table.insert(AttachedWeaponDefinitions.huntingRifleOnBack.weapons, "Base.Rossi92")
		table.insert(AttachedWeaponDefinitions.huntingRifleOnBack.weapons, "Base.Rugerm7722")
	end
end

if AttachedWeaponDefinitions.shotgunPolice then
	if getActivatedMods():contains("firearmmod_WL") then -- Add some varied shotguns from b41 firearms
		table.insert(AttachedWeaponDefinitions.shotgunPolice.weapons, "Base.DoubleBarrelShotgun")
		table.insert(AttachedWeaponDefinitions.shotgunPolice.weapons, "Base.Remington870Wood")
	end
end


if AttachedWeaponDefinitions.handgunHolster then
	AttachedWeaponDefinitions.handgunHolster.weapons = { 	-- Add back all but the m9
		"Base.Pistol2",
		"Base.Pistol3",
		"Base.Revolver",
		"Base.Revolver_Long",
		"Base.Revolver_Short",
	}
	if getActivatedMods():contains("firearmmod_WL") then -- Increase variety of pistols and revolvers
		table.insert(AttachedWeaponDefinitions.handgunHolster.weapons, "Base.ColtSingleAction22")
		table.insert(AttachedWeaponDefinitions.handgunHolster.weapons, "Base.ColtPeacemaker")
		table.insert(AttachedWeaponDefinitions.handgunHolster.weapons, "Base.ColtAce")
		table.insert(AttachedWeaponDefinitions.handgunHolster.weapons, "Base.ColtAnaconda")
	end
end

-- Auth Z Zombies - Remove assault rifles

if AttachedWeaponDefinitions.HazardassaultRifleOnBack then
	AttachedWeaponDefinitions.HazardassaultRifleOnBack.weapons = {
		"Base.VarmintRifle",
		"Base.HuntingRifle",
	}
	if getActivatedMods():contains("firearmmod_WL") then
		table.insert(AttachedWeaponDefinitions.HazardassaultRifleOnBack.weapons, "Base.SKS")
	end
end

if AttachedWeaponDefinitions.M16Cherry then
	AttachedWeaponDefinitions.M16Cherry.weapons = {
		"Base.HuntingRifle",
	}
	if getActivatedMods():contains("firearmmod_WL") then
		table.insert(AttachedWeaponDefinitions.M16Cherry.weapons, "Base.SKS")
	end
end

if AttachedWeaponDefinitions.AuthenticBillOverbeck then
	AttachedWeaponDefinitions.AuthenticBillOverbeck.weapons = {
		"Base.HuntingRifle",
	}
	if getActivatedMods():contains("firearmmod_WL") then
		table.insert(AttachedWeaponDefinitions.AuthenticBillOverbeck.weapons, "Base.SKS")
	end
end