---
--- ZoomProceduralDistributions.lua
--- 01/12/2022
---
require "Items/ProceduralDistributions"

-- Add more jars to help people preserve food
table.insert(ProceduralDistributions.list["ArtStoreOther"].items, "BoxOfJars");
table.insert(ProceduralDistributions.list["ArtStoreOther"].items, 10);
table.insert(ProceduralDistributions.list["GigamartPots"].items, "BoxOfJars");
table.insert(ProceduralDistributions.list["GigamartPots"].items, 10);
table.insert(ProceduralDistributions.list["KitchenPots"].items, "BoxOfJars");
table.insert(ProceduralDistributions.list["KitchenPots"].items, 10);
table.insert(ProceduralDistributions.list["KitchenCannedFood"].items, "BoxOfJars");
table.insert(ProceduralDistributions.list["KitchenCannedFood"].items, 4);
table.insert(ProceduralDistributions.list["StoreKitchenPots"].items, "BoxOfJars");
table.insert(ProceduralDistributions.list["StoreKitchenPots"].items, 6);
table.insert(ProceduralDistributions.list["KitchenPots"].items, "BoxOfJars");
table.insert(ProceduralDistributions.list["KitchenPots"].items, 8);