---
--- zzAdjustContainerLoot.lua
--- 30/12/2023
---


require "Items/LootTableEditor"

local addItem = WL_LootTableEditor.addItemToProceduralDistributions
local removeFromTable = WL_LootTableEditor.removeFromProceduralDistributions
local setWeighting = WL_LootTableEditor.setProceduralDistributionsWeighting

if getActivatedMods():contains("WastelandItemTweaks") then
	addItem("CrateLinens", "FeatherPillow", 10)
	addItem("GigamartBedding", "FeatherPillow", 10)
	addItem("MotelLinens", "FeatherPillow", 10)
	addItem("WardrobeChild", "FeatherPillow", 5)
	addItem("WardrobeMan", "FeatherPillow", 5)
	addItem("WardrobeManClassy", "FeatherPillow", 15)
	addItem("WardrobeWoman", "FeatherPillow", 5)
	addItem("WardrobeWomanClassy", "FeatherPillow", 15)

	-- Small chance for a bird nest to be on a shelf or in these locations
	addItem("GarageMetalwork", "BirdNest", 1)
	addItem("GarageTools", "BirdNest", 1)
	addItem("GarageCarpentry", "BirdNest", 1)
	addItem("GarageFirearms", "BirdNest", 1)
	addItem("GarageMechanics", "BirdNest", 1)
	addItem("MechanicShelfBrakes", "BirdNest", 1)
	addItem("MechanicShelfElectric", "BirdNest", 1)
	addItem("MechanicShelfMisc", "BirdNest", 1)
	addItem("MechanicShelfMufflers", "BirdNest", 1)
	addItem("ClassroomDesk", "BirdNest", 1)
end

-- More Gunpowder for ammo crafting
addItem("GunStoreCounter", "Base.GunPowder", 10)
addItem("GunStoreShelf", "Base.GunPowder", 10)
addItem("GunStoreDisplayCase", "Base.GunPowder", 10)
addItem("ArmyStorageGuns", "Base.GunPowder", 6)
addItem("FirearmWeapons", "Base.GunPowder", 4)
addItem("GarageFirearms", "Base.GunPowder", 5)
addItem("PoliceStorageAmmunition", "Base.GunPowder", 10)

-- Used in Pneumatic Rifle
addItem("FireDeptLockers", "Base.Extinguisher", 10)
addItem("FireStorageTools", "Base.Extinguisher", 10)
addItem("ForestFireTools", "Base.Extinguisher", 10)
addItem("CrateTools", "Base.Extinguisher", 1)
addItem("GarageTools", "Base.Extinguisher", 1)
addItem("ArmyHangarTools", "Base.Extinguisher", 1)
addItem("ClosetShelfGeneric", "Base.Extinguisher", 0.1)

-- Used in Pneumatic Pistol
addItem("BathroomCabinet", "Base.Hairspray", 10)
addItem("BathroomCounter", "Base.Hairspray", 10)
addItem("BathroomCounterNoMeds", "Base.Hairspray", 10)
addItem("BathroomShelf", "Base.Hairspray", 10)
addItem("SalonShelfHaircare", "Base.Hairspray", 10)
addItem("SalonCounter", "Base.Hairspray", 10)

if getActivatedMods():contains("WastelandFirearms") then

	-- Used in Last Resort crafted gun
	addItem("ForestFireTools", "Base.FlareGun", 8)
	addItem("PoliceStorageGuns", "Base.FlareGun", 1)
	addItem("CampingLockers", "Base.FlareGun", 6)
	addItem("CampingStoreGear", "Base.FlareGun", 10)
	addItem("CrateCamping", "Base.FlareGun", 5)
	addItem("Hiker", "Base.FlareGun", 5)

	-- Shell casing boxes
	addItem("GunStoreCounter", "Base.ShellCasingsBox", 10)
	addItem("GunStoreShelf", "Base.ShellCasingsBox", 10)
	addItem("GunStoreDisplayCase", "Base.ShellCasingsBox", 10)
	addItem("ArmyStorageGuns", "Base.ShellCasingsBox", 6)
	addItem("FirearmWeapons", "Base.ShellCasingsBox", 4)
	addItem("GarageFirearms", "Base.ShellCasingsBox", 5)
end

if getActivatedMods():contains("SGarden-Homestead") or getActivatedMods():contains("SGarden-Homestead+") or getActivatedMods():contains("SGarden") or getActivatedMods():contains("SGarden-GardenOnly") then
	addItem("BookstoreMisc", "Sprout.FlowerPaintMag1", 1)
	addItem("BookstoreMisc", "Sprout.FlowerPaintMag2", 1)
	addItem("BookstoreMisc", "Sprout.NatDyeMag1", 1)
	addItem("BookstoreMisc", "Sprout.FlowerMag", 1)
	addItem("CrateMagazines", "Sprout.FlowerPaintMag1", 0.2)
	addItem("CrateMagazines", "Sprout.FlowerPaintMag2", 0.2)
	addItem("CrateMagazines", "Sprout.NatDyeMag1", 0.2)
	addItem("CrateMagazines", "Sprout.FlowerMag", 0.2)
	addItem("LibraryBooks", "Sprout.FlowerPaintMag1", 0.5)
	addItem("LibraryBooks", "Sprout.FlowerPaintMag2", 0.5)
	addItem("LibraryBooks", "Sprout.NatDyeMag1", 0.5)
	addItem("LibraryBooks", "Sprout.FlowerMag", 0.5)
	addItem("LivingRoomShelf", "Sprout.FlowerPaintMag1", 0.001)
	addItem("LivingRoomShelf", "Sprout.FlowerPaintMag2", 0.001)
	addItem("LivingRoomShelf", "Sprout.NatDyeMag1", 0.001)
	addItem("LivingRoomShelf", "Sprout.FlowerMag", 0.001)
	addItem("LivingRoomShelfNoTapes", "Sprout.FlowerPaintMag1", 0.001)
	addItem("LivingRoomShelfNoTapes", "Sprout.FlowerPaintMag2", 0.001)
	addItem("LivingRoomShelfNoTapes", "Sprout.NatDyeMag1", 0.001)
	addItem("LivingRoomShelfNoTapes", "Sprout.FlowerMag", 0.001)
	addItem("MagazineRackMixed", "Sprout.FlowerPaintMag1", 0.01)
	addItem("MagazineRackMixed", "Sprout.FlowerPaintMag2", 0.01)
	addItem("MagazineRackMixed", "Sprout.NatDyeMag1", 0.01)
	addItem("MagazineRackMixed", "Sprout.FlowerMag", 0.01)
	addItem("ShelfGeneric", "Sprout.FlowerPaintMag1", 0.001)
	addItem("ShelfGeneric", "Sprout.FlowerPaintMag2", 0.001)
	addItem("ShelfGeneric", "Sprout.NatDyeMag1", 0.001)
	addItem("ShelfGeneric", "Sprout.FlowerMag", 0.001)
end