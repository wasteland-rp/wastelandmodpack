---
--- zzAdjustGunLoot.lua
--- 12/07/2023
---
require "WL_Utils"
require "Items/WL_LootTableEditor"

local addItem = WL_LootTableEditor.addItemToProceduralDistributions
local removeFromTable = WL_LootTableEditor.removeFromProceduralDistributions
local setWeighting = WL_LootTableEditor.setProceduralDistributionsWeighting


removeFromTable("WardrobeMan", "ShotgunCase1")
removeFromTable("WardrobeMan", "ShotgunCase2")
setWeighting("WardrobeMan", "DoubleBarrelShotgun", 0.001)
setWeighting("WardrobeMan", "Shotgun", 0.001)
removeFromTable("WardrobeMan", "AssaultRifle2")

removeFromTable("WardrobeManClassy", "ShotgunCase1")
removeFromTable("WardrobeManClassy", "ShotgunCase2")
setWeighting("WardrobeManClassy", "DoubleBarrelShotgun", 0.001)
setWeighting("WardrobeManClassy", "Shotgun", 0.001)
removeFromTable("WardrobeManClassy", "AssaultRifle2")
if getActivatedMods():contains("firearmmod_WL") then
	addItem("WardrobeManClassy", "Rugerm7722", 0.01)
	addItem("WardrobeManClassy", "Bullets22", 0.03)
end

removeFromTable("WardrobeRedneck", "ShotgunCase1")
removeFromTable("WardrobeRedneck", "ShotgunCase2")
setWeighting("WardrobeRedneck", "DoubleBarrelShotgun", 0.001)
setWeighting("WardrobeRedneck", "Shotgun", 0.001)
removeFromTable("WardrobeRedneck", "AssaultRifle2")
if getActivatedMods():contains("firearmmod_WL") then
	addItem("WardrobeRedneck", "Base.Bullets4440Box", 0.03)
end

removeFromTable("WardrobeWoman", "ShotgunCase1")
removeFromTable("WardrobeWoman", "ShotgunCase2")
setWeighting("WardrobeWoman", "DoubleBarrelShotgun", 0.001)
setWeighting("WardrobeWoman", "Shotgun", 0.001)
removeFromTable("WardrobeWoman", "AssaultRifle2")
if getActivatedMods():contains("firearmmod_WL") then
	addItem("WardrobeWoman", "Rugerm7722", 0.01)
	addItem("WardrobeWoman", "Bullets22", 0.03)
end

removeFromTable("WardrobeWomanClassy", "ShotgunCase1")
removeFromTable("WardrobeWomanClassy", "ShotgunCase2")
setWeighting("WardrobeWomanClassy", "DoubleBarrelShotgun", 0.001)
setWeighting("WardrobeWomanClassy", "Shotgun", 0.001)
removeFromTable("WardrobeWomanClassy", "AssaultRifle2")
if getActivatedMods():contains("firearmmod_WL") then
	addItem("WardrobeWoman", "Bullets22", 0.03)
end

removeFromTable("ClosetShelfGeneric", "ShotgunCase1")
removeFromTable("ClosetShelfGeneric", "ShotgunCase2")
removeFromTable("ClosetShelfGeneric", "AssaultRifle2")

setWeighting("ClosetShelfGeneric", "DoubleBarrelShotgun", 0.001)
setWeighting("ClosetShelfGeneric", "Shotgun", 0.001)

if getActivatedMods():contains("WastelandFirearms") then
	addItem("GunStoreAmmunition", "ShellCasings", 5)
	addItem("GunStoreCounter", "GunPowder", 10)
	addItem("GunStoreShelf", "GunPowder", 10)
	addItem("GunStoreShelf", "ShellCasings", 10)
	addItem("PawnShopGuns", "ShellCasings", 10)
end

if getActivatedMods():contains("firearmmod_WL") then
	addItem("PawnShopGunsSpecial", "Base.Bullets3006Box", 10)
end

removeFromTable("HuntingLockers", "AssaultRifle2")
if getActivatedMods():contains("firearmmod_WL") then -- Replace with an actual hunting rifle
	addItem("HuntingLockers", "Base.Rugerm7722", 1)
	addItem("HuntingLockers", "Base.Bullets22Box", 2)
	removeFromTable("HuntingLockers", "AmmoCan9mm")
end

removeFromTable("PawnShopGunsSpecial", "AssaultRifle")
removeFromTable("PawnShopGunsSpecial", "AssaultRifle2")
removeFromTable("PawnShopGunsSpecial", "M14Clip")
removeFromTable("PawnShopGunsSpecial", "556Box")
removeFromTable("PawnShopGunsSpecial", "556Clip")

-- Not sure this has a use with b41 firearms as they removed the clip on the rifle that uses it in vanilla
removeFromTable("PawnShopGunsSpecial", "308Clip")

if getActivatedMods():contains("firearmmod_WL") then
	addItem("PawnShopGunsSpecial", "Base.SKS", 10)
	addItem("PawnShopGunsSpecial", "Base.Winchester94", 5)
	addItem("PawnShopGunsSpecial", "Base.Winchester73", 5)
	addItem("PawnShopGunsSpecial", "Base.Bullets44Box", 10)
	addItem("PawnShopGunsSpecial", "Base.Bullets4440Box", 10)
	addItem("PawnShopGunsSpecial", "Base.762x39Box", 6)
end

-- Adjust DrugLabGuns
removeFromTable("DrugLabGuns", "AssaultRifle")
removeFromTable("DrugLabGuns", "AssaultRifle2")
removeFromTable("DrugLabGuns", "M14Clip")
removeFromTable("DrugLabGuns", "556Box")
removeFromTable("DrugLabGuns", "556Clip")
removeFromTable("DrugLabGuns", "9mmClip")
removeFromTable("DrugLabGuns", "Pistol")
removeFromTable("DrugLabGuns", "Bullets9mmBox")
addItem("DrugLabGuns", "Base.GunPowder", 10)

if getActivatedMods():contains("firearmmod_WL") then
	addItem("DrugLabGuns", "Base.ColtPeacemaker", 4)
	addItem("DrugLabGuns", "Base.ColtAnaconda", 4)
	addItem("DrugLabGuns", "Base.Bullets357Box", 10)
	addItem("DrugLabGuns", "Base.Bullets357Box", 5)
	addItem("DrugLabGuns", "Base.SKS", 10)
	addItem("DrugLabGuns", "Base.762x39Box", 20)
	addItem("DrugLabGuns", "Base.762x39Box", 10)
end

-- Adjust GunStoreShelf
removeFromTable("GunStoreShelf", "Bullets9mmBox")
removeFromTable("GunStoreShelf", "9mmClip")
if getActivatedMods():contains("firearmmod_WL") then
	removeFromTable("GunStoreShelf", "308Clip") -- Not used in b41 firearms
	removeFromTable("GunStoreShelf", "UZI_Stock_Detracted")
	removeFromTable("GunStoreShelf", "MP5_Stock_Detracted")
	addItem("GunStoreShelf", "Base.Bullets357Box", 10)
	addItem("GunStoreShelf", "Base.Bullets22Box", 10)
	addItem("GunStoreShelf", "Base.Bullets4440Box", 10)
end

-- Rifle case 3 from Distributions.lua has an M14 in it so take it out
removeFromTable("ClosetShelfGeneric", "RifleCase3")
removeFromTable("GarageFirearms", "RifleCase3")
removeFromTable("HuntingLockers", "RifleCase3")
removeFromTable("WardrobeMan", "RifleCase3")
removeFromTable("WardrobeManClassy", "RifleCase3")
removeFromTable("WardrobeRedneck", "RifleCase3")
removeFromTable("WardrobeWoman", "RifleCase3")
removeFromTable("WardrobeWomanClassy", "RifleCase3")

-- Readjust FirearmWeapons
removeFromTable("FirearmWeapons", "AssaultRifle")
removeFromTable("FirearmWeapons", "AssaultRifle2")
removeFromTable("FirearmWeapons", "M14Clip")
removeFromTable("FirearmWeapons", "556Clip")
removeFromTable("FirearmWeapons", "Bullets9mmBox")

-- Not sure this has a use with b41 firearms as they removed the clip on the rifle that uses it in vanilla
removeFromTable("FirearmWeapons", "308Clip")
removeFromTable("FirearmWeapons", "Pistol")
removeFromTable("FirearmWeapons", "9mmClip")
setWeighting("FirearmWeapons", "Pistol2", 12)
setWeighting("FirearmWeapons", "45Clip", 20)

if getActivatedMods():contains("firearmmod_WL") then
	removeFromTable("FirearmWeapons", "Base.Mossberg500Tactical")
	removeFromTable("FirearmWeapons", "Base.ColtPython")
	removeFromTable("FirearmWeapons", "Base.ColtPythonHunter")
	removeFromTable("FirearmWeapons", "Base.AR15")
	removeFromTable("FirearmWeapons", "Base.UZI")
	removeFromTable("FirearmWeapons", "Base.UZIMag")
	removeFromTable("FirearmWeapons", "Base.SPAS12")
	removeFromTable("FirearmWeapons", "Base.Mac10Mag")
	removeFromTable("FirearmWeapons", "Base.Mac10")
	removeFromTable("FirearmWeapons", "Base.AK47")
	removeFromTable("FirearmWeapons", "Base.AK_Mag")
	removeFromTable("FirearmWeapons", "Base.FN_FAL")
	removeFromTable("FirearmWeapons", "Base.FN_FAL_Mag")
	removeFromTable("FirearmWeapons", "Base.MP5")
	removeFromTable("FirearmWeapons", "Base.MP5Mag")
	removeFromTable("FirearmWeapons", "Base.Glock17")
	removeFromTable("FirearmWeapons", "Base.Glock17Mag")

	addItem("FirearmWeapons", "ColtAce", 10)
	addItem("FirearmWeapons", "Base.22Clip", 20)
	addItem("FirearmWeapons", "Base.22Clip", 20)
	addItem("FirearmWeapons", "Base.Bullets22Box", 10)
	addItem("FirearmWeapons", "Base.Bullets22Box", 10)

	-- Adjust ammo
	removeFromTable("FirearmWeapons", "308Box")
	addItem("FirearmWeapons", "Base.762x39Box", 20)

	-- Make much more common
	setWeighting("FirearmWeapons", "Base.SKS", 10)

	-- It's not really clear what FirearmWeapons is for so just add lots of random guns and ammo
	setWeighting("FirearmWeapons", "Winchester94", 3)
	setWeighting("FirearmWeapons", "Winchester73", 3)
	addItem("FirearmWeapons", "Base.ColtPeacemaker", 2)
	addItem("FirearmWeapons", "Base.ColtAnaconda", 2)
	addItem("FirearmWeapons", "Base.Bullets44Box", 5)
	addItem("FirearmWeapons", "Base.Bullets4440Box", 20)
	addItem("FirearmWeapons", "Base.Bullets4440Box", 10)
	addItem("FirearmWeapons", "Base.Bullets3006Box", 10)
end

-- Readjust GunStoreAmmunition
removeFromTable("GunStoreAmmunition", "Bullets9mmBox")
if getActivatedMods():contains("firearmmod_WL") then -- Increase these ammo types
	setWeighting("GunStoreAmmunition", "Bullets3006Box", 20)
	setWeighting("GunStoreAmmunition", "Bullets4440Box", 20)
	addItem("GunStoreAmmunition", "Bullets4440Box", 10)
	removeFromTable("GunStoreAmmunition", "AmmoCan9mm")
end

-- Readjust GunStoreCounter
removeFromTable("GunStoreCounter", "M14Clip")
removeFromTable("GunStoreCounter", "9mmClip")
removeFromTable("GunStoreCounter", "AmmoCan9mm")
removeFromTable("GunStoreCounter", "Bullets9mmBox")
if getActivatedMods():contains("firearmmod_WL") then -- Swap clips for less military-ish weapons
	addItem("GunStoreCounter", "223Clip", 8)
	removeFromTable("GunStoreCounter", "308Clip") -- Not used in b41 firearms

end

-- Readjust GunStoreDisplayCase
removeFromTable("GunStoreDisplayCase", "AssaultRifle2")
removeFromTable("GunStoreDisplayCase", "9mmClip")
removeFromTable("GunStoreDisplayCase", "Pistol")
removeFromTable("GunStoreDisplayCase", "Bullets9mmBox")
addItem("GunStoreDisplayCase", "GunPowder", 5)

if getActivatedMods():contains("firearmmod_WL") then
	removeFromTable("GunStoreDisplayCase", "Base.SPAS12")
	removeFromTable("GunStoreDisplayCase", "Base.Mossberg500Tactical")
	removeFromTable("GunStoreDisplayCase", "Base.ColtPython")
	removeFromTable("GunStoreDisplayCase", "Base.ColtPythonHunter")
	removeFromTable("GunStoreDisplayCase", "9mmSilencer")
	removeFromTable("GunStoreDisplayCase", "308Clip") -- Not used in b41 firearms
	removeFromTable("GunStoreDisplayCase", "Base.M1Garand")
	removeFromTable("GunStoreDisplayCase", "Base.M1GarandClip")
	removeFromTable("GunStoreDisplayCase", "Base.Glock17")
	removeFromTable("GunStoreDisplayCase", "Base.Glock17Mag")
	removeFromTable("GunStoreDisplayCase", "Base.556Clip")
	removeFromTable("GunStoreDisplayCase", "AmmoCan9mm")

	removeFromTable("GunStoreDisplayCase", "45Silencer")
	removeFromTable("GunStoreDisplayCase", "223Silencer")
	removeFromTable("GunStoreDisplayCase", "308Silencer")
	removeFromTable("GunStoreDisplayCase", "22Silencer")

	-- Make some old weapons more common to find
	setWeighting("GunStoreDisplayCase", "Winchester94", 8)
	setWeighting("GunStoreDisplayCase", "Winchester73", 8)
	setWeighting("GunStoreDisplayCase", "ColtAnaconda", 3)
	setWeighting("GunStoreDisplayCase", "ColtSingleAction22", 3)
	setWeighting("GunStoreDisplayCase", "Base.SKS", 3)

	-- Remove some stronger weapons and clips (Most of these were very rare anyway tbf)
	-- Repetition is intentional, some stuff is put in there twice
	removeFromTable("GunStoreDisplayCase", "Base.AK47")
	removeFromTable("GunStoreDisplayCase", "Base.AK_Mag")
	removeFromTable("GunStoreDisplayCase", "Base.AR15")
	removeFromTable("GunStoreDisplayCase", "Base.UZI")
	removeFromTable("GunStoreDisplayCase", "Base.UZIMag")
	removeFromTable("GunStoreDisplayCase", "Base.Mac10")
	removeFromTable("GunStoreDisplayCase", "Base.Mac10Mag")
	removeFromTable("GunStoreDisplayCase", "Base.FN_FAL")
	removeFromTable("GunStoreDisplayCase", "Base.FN_FAL_Mag")
	removeFromTable("GunStoreDisplayCase", "Base.MP5")
	removeFromTable("GunStoreDisplayCase", "Base.MP5Mag")
	removeFromTable("GunStoreDisplayCase", "Base.M14Clip")
	removeFromTable("GunStoreDisplayCase", "M14Clip")
	removeFromTable("GunStoreDisplayCase", "Glock17")
	removeFromTable("GunStoreDisplayCase", "Glock17Mag")

	addItem("GunStoreDisplayCase", "Pistol2", 5)
	addItem("GunStoreDisplayCase", "45Clip", 5)
	addItem("GunStoreDisplayCase", "Base.Bullets357Box", 5)
	addItem("GunStoreDisplayCase", "Base.Bullets4440Box", 5)
	addItem("GunStoreDisplayCase", "Base.762x39Box", 5)

end

-- Readjust PoliceStorageGuns
removeFromTable("PoliceStorageGuns", "AssaultRifle2")
removeFromTable("PoliceStorageGuns", "M14Clip")
removeFromTable("PoliceStorageGuns", "556Clip")
removeFromTable("PoliceStorageGuns", "Bullets9mmBox")

if getActivatedMods():contains("firearmmod_WL") then
	removeFromTable("PoliceStorageGuns", "45Silencer")
	removeFromTable("PoliceStorageGuns", "223Silencer")
	removeFromTable("PoliceStorageGuns", "308Silencer")
	removeFromTable("PoliceStorageGuns", "22Silencer")

	removeFromTable("PoliceStorageGuns", "SPAS12")
	removeFromTable("PoliceStorageGuns", "Mossberg500Tactical")
	removeFromTable("PoliceStorageGuns", "LAW12")
	removeFromTable("PoliceStorageGuns", "ColtPython")
	removeFromTable("PoliceStorageGuns", "MP5")
	removeFromTable("PoliceStorageGuns", "MP5Mag")
	removeFromTable("PoliceStorageGuns", "AR15")
	removeFromTable("PoliceStorageGuns", "AK47")
	removeFromTable("PoliceStorageGuns", "AK_Mag")
	removeFromTable("PoliceStorageGuns", "Base.556Box")
	removeFromTable("PoliceStorageGuns", "308Clip") -- Not used in b41 firearms
	removeFromTable("PoliceStorageGuns", "9mmClip")
	removeFromTable("PoliceStorageGuns", "Pistol")
	removeFromTable("PoliceStorageGuns", "Glock17")
	removeFromTable("PoliceStorageGuns", "Glock17Mag")
	removeFromTable("PoliceStorageGuns", "9mmSilencer")
	removeFromTable("PoliceStorageGuns", "AmmoCan9mm")

	addItem("PoliceStorageGuns", "Base.Revolver", 5)
	addItem("PoliceStorageGuns", "Base.Revolver_Long", 5)
	addItem("PoliceStorageGuns", "Base.Bullets38Box", 10)
	addItem("PoliceStorageGuns", "Base.Bullets45Box", 10)
	addItem("PoliceStorageGuns", "Base.ColtPeacemaker", 2)
	addItem("PoliceStorageGuns", "Base.ColtAnaconda", 2)
	addItem("PoliceStorageGuns", "Base.HuntingRifle_Sawn", 5)
	addItem("PoliceStorageGuns", "Base.308Box", 5)
	addItem("PoliceStorageGuns", "Base.Bullets4440Box", 10)
	addItem("PoliceStorageGuns", "Base.Bullets44Box", 10)
	addItem("PoliceStorageGuns", "Base.Bullets357Box", 10)
	addItem("PoliceStorageGuns", "Base.Bullets22Box", 10)
	addItem("PoliceStorageGuns", "Base.Bullets4440Box", 10)
	addItem("PoliceStorageGuns", "Base.SKS", 10)
end

-- Readjust LockerArmyBedroom
removeFromTable("LockerArmyBedroom", "AssaultRifle")
removeFromTable("LockerArmyBedroom", "556Clip")
removeFromTable("LockerArmyBedroom", "556Box")
setWeighting("LockerArmyBedroom", "Pistol2", 1)
addItem("LockerArmyBedroom", "45Clip", 10)

if getActivatedMods():contains("firearmmod_WL") then

	removeFromTable("LockerArmyBedroom", "45Silencer")
	removeFromTable("LockerArmyBedroom", "223Silencer")
	removeFromTable("LockerArmyBedroom", "308Silencer")
	removeFromTable("LockerArmyBedroom", "22Silencer")

	removeFromTable("LockerArmyBedroom", "Base.Mossberg500Tactical")
	removeFromTable("LockerArmyBedroom", "Base.M24Rifle")
	removeFromTable("LockerArmyBedroom", "Base.M16A2")
	removeFromTable("LockerArmyBedroom", "Base.FN_FAL")
	removeFromTable("LockerArmyBedroom", "Base.FN_FAL_Mag")
	removeFromTable("LockerArmyBedroom", "Base.M60")
	removeFromTable("LockerArmyBedroom", "Base.M60Mag")
	removeFromTable("LockerArmyBedroom", "Base.MP5")
	removeFromTable("LockerArmyBedroom", "Base.MP5Mag")
	removeFromTable("LockerArmyBedroom", "Base.M733")
	removeFromTable("LockerArmyBedroom", "9mmSilencer")
	addItem("LockerArmyBedroom", "Base.Bullets3006Box", 10)
end

-- Readjust ArmyStorageGuns
removeFromTable("ArmyStorageGuns", "AssaultRifle")
removeFromTable("ArmyStorageGuns", "AssaultRifle2")
removeFromTable("ArmyStorageGuns", "M14Clip")
removeFromTable("ArmyStorageGuns", "308Clip") -- Not used in b41 firearms
removeFromTable("ArmyStorageGuns", "556Clip")

if getActivatedMods():contains("firearmmod_WL") then
	removeFromTable("ArmyStorageGuns", "Base.Mossberg500Tactical")
	removeFromTable("ArmyStorageGuns", "Base.M16A2")
	removeFromTable("ArmyStorageGuns", "Base.FN_FAL")
	removeFromTable("ArmyStorageGuns", "Base.FN_FAL_Mag")
	removeFromTable("ArmyStorageGuns", "Base.M60")
	removeFromTable("ArmyStorageGuns", "Base.M60Mag")
	removeFromTable("ArmyStorageGuns", "Base.MP5")
	removeFromTable("ArmyStorageGuns", "Base.MP5Mag")
	removeFromTable("ArmyStorageGuns", "Base.M733")
	removeFromTable("ArmyStorageGuns", "9mmSilencer")
	removeFromTable("ArmyStorageGuns", "Base.M24Rifle")

	-- Remove suppressors
	removeFromTable("ArmyStorageGuns", "45Silencer")
	removeFromTable("ArmyStorageGuns", "223Silencer")
	removeFromTable("ArmyStorageGuns", "308Silencer")
	removeFromTable("ArmyStorageGuns", "22Silencer")

	-- M1911 common army pistol
	setWeighting("ArmyStorageGuns", "Pistol2", 20)
	addItem("ArmyStorageGuns", "45Clip", 20)
	addItem("ArmyStorageGuns", "45Clip", 30)
	addItem("ArmyStorageGuns", "ColtAce", 10)
	addItem("ArmyStorageGuns", "Base.22Clip", 20)
	addItem("ArmyStorageGuns", "Base.22Clip", 20)

	addItem("ArmyStorageGuns", "Base.M1Garand", 20)
	addItem("ArmyStorageGuns", "Base.M1Garand", 20)
	addItem("ArmyStorageGuns", "Base.M1GarandClip", 35)
	addItem("ArmyStorageGuns", "Base.M1GarandClip", 25)
	addItem("ArmyStorageGuns", "Base.M1GarandClip", 25)

	-- Loads of shotguns don't make much sense for army, tactical only and less ammo
	setWeighting("ArmyStorageGuns", "Base.Mossberg500Tactical", 5)
	removeFromTable("ArmyStorageGuns", "Base.Remington870Wood")
	removeFromTable("ArmyStorageGuns", "Shotgun")
	removeFromTable("ArmyStorageGuns", "ShotgunShellsBox")
	addItem("ArmyStorageGuns", "ShotgunShellsBox", 5)

	-- Removed bc the M16 is and swapped for 30.06
	removeFromTable("ArmyStorageGuns", "308Box")
	removeFromTable("ArmyStorageGuns", "556Box")
	addItem("ArmyStorageGuns", "Base.Bullets3006Box", 20)
	addItem("ArmyStorageGuns", "Base.Bullets3006Box", 20)
	addItem("ArmyStorageGuns", "Base.Bullets3006Box", 20)

end

-- Readjust ArmyStorageAmmunition
-- 308 is hunting rifle ammo, this and shotty ammo don't make any sense for military storage
removeFromTable("ArmyStorageAmmunition", "308Box")
removeFromTable("ArmyStorageAmmunition", "ShotgunShellsBox")

if getActivatedMods():contains("firearmmod_WL") then
	-- Remove so we can re-add our own for this type (It was in there 4 times)
	removeFromTable("ArmyStorageAmmunition", "Bullets45Box")
	addItem("ArmyStorageAmmunition", "Bullets45Box", 20)
	addItem("ArmyStorageAmmunition", "Bullets45Box", 10)

	-- Add other army types
	addItem("ArmyStorageAmmunition", "Base.Bullets3006Box", 20)
	addItem("ArmyStorageAmmunition", "Base.Bullets3006Box", 10)
	addItem("ArmyStorageAmmunition", "Base.Bullets3006Box", 10)
	addItem("ArmyStorageAmmunition", "Base.Bullets3006Box", 10)
	addItem("ArmyStorageAmmunition", "Base.M1Garand", 5)
	addItem("ArmyStorageAmmunition", "Base.M1GarandClip", 10)
	addItem("ArmyStorageAmmunition", "Base.M1GarandClip", 10)
	addItem("ArmyStorageAmmunition", "45Clip", 10)
end

-- Readjust PrisonGuardLockers

-- More nightsticks!
setWeighting("PrisonGuardLockers", "Nightstick", 15)

if getActivatedMods():contains("firearmmod_WL") then
	removeFromTable("PrisonGuardLockers", "ColtPython")
	removeFromTable("PrisonGuardLockers", "LAW12")

	-- 308 is hunting rifle ammo, this and shotty ammo don't make any sense for military storage
	removeFromTable("PrisonGuardLockers", "MP5")
	removeFromTable("PrisonGuardLockers", "MP5Mag")
	removeFromTable("PrisonGuardLockers", "AR15")
	removeFromTable("PrisonGuardLockers", "556Clip")
	removeFromTable("PrisonGuardLockers", "Glock17")
	removeFromTable("PrisonGuardLockers", "Glock17Mag")
	removeFromTable("PrisonGuardLockers", "AK47")
	removeFromTable("PrisonGuardLockers", "AK_Mag")
	removeFromTable("PrisonGuardLockers", "762x39Box")

	-- Add shotgun and ammo
	addItem("PrisonGuardLockers", "Base.Remington870Wood", 3)
	addItem("PrisonGuardLockers", "ShotgunShellsBox", 7)
end

-- Readjust PoliceStorageAmmunition

-- Hunting rifle ammo doesn't make much sense for police
removeFromTable("PoliceStorageAmmunition", "308Box")
removeFromTable("PoliceStorageAmmunition", "Bullets9mmBox")

if getActivatedMods():contains("firearmmod_WL") then
	-- Add a selection of pistol ammo types
	addItem("PoliceStorageAmmunition", "Base.Bullets4440Box", 10)
	addItem("PoliceStorageAmmunition", "Base.Bullets38Box", 20)
	addItem("PoliceStorageAmmunition", "Base.Bullets45Box", 10)
	addItem("PoliceStorageAmmunition", "Base.Bullets22Box", 10)
	addItem("PoliceStorageAmmunition", "Base.Bullets44Box", 10)
end

-- Readjust SecurityLockers
removeFromTable("SecurityLockers", "9mmClip")
removeFromTable("SecurityLockers", "Bullets9mmBox")
removeFromTable("SecurityLockers", "Pistol")
addItem("SecurityLockers", "Nightstick", 15)
addItem("SecurityLockers", "Base.Revolver", 10)
addItem("SecurityLockers", "Base.Revolver", 10)
addItem("SecurityLockers", "Base.Bullets45Box", 10)
addItem("SecurityLockers", "Base.Bullets45Box", 10)

-- Readjust PawnShopGuns
removeFromTable("PawnShopGuns", "Pistol")
removeFromTable("PawnShopGuns", "9mmClip")
removeFromTable("PawnShopGuns", "Bullets9mmBox")
if getActivatedMods():contains("firearmmod_WL") then
	addItem("PawnShopGuns", "Base.SKS", 4)
	addItem("PawnShopGuns", "Base.Winchester94", 4)
	addItem("PawnShopGuns", "Base.Winchester73", 4)
	addItem("PawnShopGuns", "Base.Bullets44Box", 4)
	addItem("PawnShopGuns", "Base.Bullets4440Box", 4)
	addItem("PawnShopGuns", "Base.Bullets22Box", 4)
	addItem("PawnShopGuns", "Base.Bullets4440Box", 4)
	addItem("PawnShopGuns", "Base.762x39Box", 4)
	addItem("PawnShopGuns", "Base.ColtPeacemaker", 4)
	addItem("PawnShopGuns", "Base.ColtAnaconda", 4)
end

-- Readjust DrugShackWeapons
removeFromTable("DrugShackWeapons", "Pistol")
addItem("DrugShackWeapons", "Revolver_Long", 8)

-- Readjust GarageFirearms
removeFromTable("GarageFirearms", "AssaultRifle2")
removeFromTable("GarageFirearms", "Pistol")
removeFromTable("GarageFirearms", "PistolCase1")

if getActivatedMods():contains("firearmmod_WL") then
	removeFromTable("GarageFirearms", "SPAS12")
	removeFromTable("GarageFirearms", "Mossberg500Tactical")
	removeFromTable("GarageFirearms", "PistolCase2")
	removeFromTable("GarageFirearms", "PistolCase3")

	addItem("GarageFirearms", "Base.ColtPeacemaker", 2)
	addItem("GarageFirearms", "Base.ColtAnaconda", 2)
	addItem("GarageFirearms", "Base.Winchester94", 2)
	addItem("GarageFirearms", "Base.Winchester73", 2)

	addItem("GarageFirearms", "ColtAce", 4)
	addItem("GarageFirearms", "Base.22Clip", 5)
	addItem("GarageFirearms", "Base.22Clip", 6)
	addItem("BedroomDresser", "Base.Bullets22", 3)
end

-- Readjust PlankStashGun
removeFromTable("PlankStashGun", "Pistol")

if getActivatedMods():contains("firearmmod_WL") then
	addItem("PlankStashGun", "Base.ColtPeacemaker", 2)
	addItem("PlankStashGun", "Base.ColtAnaconda", 2)
end

-- Readjust PoliceDesk
removeFromTable("PoliceDesk", "Pistol")
addItem("PoliceDesk", "Base.Revolver_Short", 0.5)
addItem("PoliceDesk", "Base.Bullets38", 10) -- Loose bullets
addItem("PoliceDesk", "Base.Bullets38", 10) -- Loose bullets

-- Readjust OfficeDeskHome
removeFromTable("OfficeDeskHome", "RevolverCase1")
removeFromTable("OfficeDeskHome", "RevolverCase2")
removeFromTable("OfficeDeskHome", "RevolverCase3")
setWeighting("OfficeDeskHome", "Revolver_Long", 0.005)
addItem("OfficeDeskHome","Base.Bullets44", 0.01)
setWeighting("OfficeDeskHome", "Revolver", 0.01)
addItem("OfficeDeskHome","Base.Bullets45", 0.1)
removeFromTable("OfficeDeskHome", "PistolCase1")
removeFromTable("OfficeDeskHome", "PistolCase2")
removeFromTable("OfficeDeskHome", "PistolCase3")
removeFromTable("OfficeDeskHome", "Pistol")

-- Readjust KitchenRandom
removeFromTable("KitchenRandom", "Pistol")
addItem("KitchenRandom","Revolver_Short", 0.01)

-- Readjust DresserGeneric
removeFromTable("DresserGeneric", "RevolverCase1")
removeFromTable("DresserGeneric", "RevolverCase2")
removeFromTable("DresserGeneric", "RevolverCase3")
removeFromTable("DresserGeneric", "Pistol")
setWeighting("DresserGeneric", "Revolver", 0.001)
setWeighting("DresserGeneric", "Pistol", 0.001)
setWeighting("DresserGeneric", "Revolver_Long", 0.001)
addItem("DresserGeneric", "Base.Bullets38Box", 0.1)
removeFromTable("DresserGeneric", "PistolCase1")
removeFromTable("DresserGeneric", "PistolCase2")
removeFromTable("DresserGeneric", "PistolCase3")

if getActivatedMods():contains("firearmmod_WL") then
	addItem("DresserGeneric", "Base.ColtPeacemaker", 0.01)
	addItem("DresserGeneric", "Base.Bullets4440Box", 0.02)
end

-- Readjust BedroomSideTable
removeFromTable("BedroomSideTable", "Pistol")
removeFromTable("BedroomSideTable", "PistolCase1")
addItem("BedroomSideTable", "Revolver_Short", 0.05)

if getActivatedMods():contains("firearmmod_WL") then
	removeFromTable("BedroomSideTable", "Pistol3")
	setWeighting("BedroomSideTable", "Pistol2", 0.001)
	addItem("BedroomSideTable", "ColtAce", 0.01)
	addItem("BedroomSideTable", "Base.22Clip", 0.05)
	addItem("BedroomSideTable", "Base.Bullets22", 0.1)
end

-- Readjust BedroomDresser
removeFromTable("BedroomDresser", "RevolverCase1")
removeFromTable("BedroomDresser", "RevolverCase2")
removeFromTable("BedroomDresser", "RevolverCase3")
setWeighting("BedroomDresser", "Revolver_Long", 0.001)
setWeighting("BedroomDresser", "Revolver", 0.001)
removeFromTable("BedroomDresser", "Pistol")
removeFromTable("BedroomDresser", "PistolCase1")
removeFromTable("BedroomDresser", "PistolCase2")
removeFromTable("BedroomDresser", "PistolCase3")
setWeighting("BedroomDresser", "Revolver_Short", 0.1) -- Actually increased
addItem("BedroomDresser", "Base.Bullets38Box", 0.1) -- Ammo for m36 above

if getActivatedMods():contains("firearmmod_WL") then
	removeFromTable("BedroomDresser", "Pistol2")
	removeFromTable("BedroomDresser", "Pistol3")
	addItem("BedroomDresser", "Base.ColtAce", 0.1)
	addItem("BedroomDresser", "Base.22Clip", 0.1)
	addItem("BedroomDresser", "Base.Bullets22", 0.5)
end

--Readjust PawnShopCases
removeFromTable("PawnShopCases", "PistolCase1")
addItem("PawnShopCases", "Revolver_Long", 5)
addItem("PawnShopCases", "Revolver", 5)

--Readjust BarCounterWeapon
if getActivatedMods():contains("firearmmod_WL") then
	removeFromTable("BarCounterWeapon", "SPAS12")
	removeFromTable("BarCounterWeapon", "Mossberg500Tactical")
end

-- Fix TryHonesty's Tile Mod
if getActivatedMods():contains("TryhonestyTiles") then
	if SuburbsDistributions.Rampage and SuburbsDistributions.Rampage.Rampage then
		WL_LootTableEditor.removeFromDistributionTable("AssaultRifle", SuburbsDistributions.Rampage.Rampage.items)
	end
end

-- Comment this back in to output the final loot tables after all mods have been in there
--WL_LootTableEditor.printAllProceduralDistributions()

