---
--- zzAdjustVehicleLoot.lua
--- 29/12/2023
---

require "WL_Utils"

local function removeItemWithName(itemName)
	for _, data in pairs(VehicleDistributions) do
		WL_LootTableEditor.removeFromDistributionTable(itemName, data.items)

		if data.junk then
			WL_LootTableEditor.removeFromDistributionTable(itemName, data.junk.items)
		end
	end
end

removeItemWithName("Pistol")
removeItemWithName("AssaultRifle")
removeItemWithName("AssaultRifle2")

--local str = WL_Utils.tableToString(VehicleDistributions)
--local fileWriterObj = getFileWriter("VehicleDistributions.txt", true, false)
--fileWriterObj:write(str)
--fileWriterObj:close()

if ATATruckItemDistributions.PoliceMegaTruckBed then
	WL_LootTableEditor.removeFromDistributionTable("AssaultRifle", ATATruckItemDistributions.PoliceMegaTruckBed.items)
	WL_LootTableEditor.removeFromDistributionTable("AssaultRifle2", ATATruckItemDistributions.PoliceMegaTruckBed.items)
end

if CommonVehicleDistributions.Holster then
	WL_LootTableEditor.removeFromDistributionTable("AssaultRifle2", CommonVehicleDistributions.Holster.items)
end