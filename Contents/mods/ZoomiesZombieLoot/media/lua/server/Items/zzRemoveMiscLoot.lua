---
--- zzRemoveMiscLoot.lua
--- 18/08/2024
---

require "WL_Utils"
require "Items/LootTableEditor"

local addItem = WL_LootTableEditor.addItemToProceduralDistributions
local removeFromTable = WL_LootTableEditor.removeFromProceduralDistributions
local setWeighting = WL_LootTableEditor.setProceduralDistributionsWeighting
local removeFromAllTables = WL_LootTableEditor.removeFromAllTables

removeFromAllTables("Radio.WalkieTalkie5")



-- Comment this back in to output the final loot tables after all mods have been in there
--LootTableEditor.printAllProceduralDistributions()
--LootTableEditor.printAllDistributions()