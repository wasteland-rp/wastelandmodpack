---
--- zzRemoveSapphZombieLoot.lua
--- 15/12/2024
---

if not getActivatedMods():contains("sapphcooking") then return end

-- Define a function to remove items from the loot table
local function removeItemsFromTable(lootTable, itemsToRemove)
	for i = #lootTable, 1, -1 do
		local item = lootTable[i]
		-- Check if the current item matches any key in the itemsToRemove
		if itemsToRemove[item] then
			table.remove(lootTable, i)
			table.remove(lootTable, i) -- Remove the paired value as well
		end
	end
end

-- Define the items to be removed
local itemsToRemove = {
	["SapphCooking.ProteinBar"] = true,
	["SapphCooking.PackofCandyCigarretes"] = true,
	["SapphCooking.Bonbon"] = true,
	["SapphCooking.Bonbon_Liqueur"] = true,
	["SapphCooking.Heart_Chocolate"] = true,
	["SapphCooking.FortuneCookie"] = true
}

-- Remove items from the male inventory
removeItemsFromTable(SuburbsDistributions["all"]["inventorymale"].items, itemsToRemove)

-- Remove items from the female inventory
removeItemsFromTable(SuburbsDistributions["all"]["inventoryfemale"].items, itemsToRemove)