---
--- RemoveOpForageLoot.lua
--- 19/11/2023
---

require 'Foraging/forageSystem'

Events.onAddForageDefs.Add(function()
	local m16 = forageSystem.itemDefs["Base.AssaultRifle"];
	forageSystem.removeItemDef(m16);
	local m14 = forageSystem.itemDefs["Base.AssaultRifle2"];
	forageSystem.removeItemDef(m14);
	local m9 = forageSystem.itemDefs["Base.Pistol"];
	forageSystem.removeItemDef(m9);
end)

Events.preAddForageDefs.Add(function ()
	forageDefs["AssaultRifle"] = nil
	forageDefs["AssaultRifle2"] = nil
	forageDefs["Pistol"] = nil
end)