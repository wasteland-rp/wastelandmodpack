[![Issues](https://badgen.net/gitlab/issues/wasteland-rp/wastelandmodpack)](https://badgen.net/gitlab/issues/wasteland-rp/wastelandmodpack "Lifetime Issues")
[![Commits](https://badgen.net/gitlab/last-commit/wasteland-rp/wastelandmodpack)](https://badgen.net/gitlab/last-commit/wasteland-rp/wastelandmodpack "Last Commit")


# Wasteland RP Modpack

This is the modpack used for Wasteland RP server. When there are changes, this gitlab repo is automatically published during EU mornings to [Steam Workshop](https://steamcommunity.com/sharedfiles/filedetails/?id=2946513801)

If you would like to use our mods, please let us know!

## License

Copyright 2023 Wasteland RP for all code except where otherwise noted.

You are not permitted to use, modify, or distribute this code without explicit written permission from the author. If you would like to use this code for any purpose, please contact Zoomies or Gravy in Discord: https://discord.gg/wastelandrp to request permission.

Unauthorized copying, reproduction, or distribution of this software is strictly prohibited without written permission.

-----------
[WastelandAutoClaims](WastelandAutoClaims/) - GPLv3, see [LICENSE](https://gitlab.com/wasteland-rp/wastelandmodpack/-/blob/main/WastelandAutoClaims/LICENSE)
    - Much of the code in WastelandAutoClaims mod is based on the work [AnotherVehicleClaimSystem](https://github.com/Lu5ck/AnotherVehicleClaimSystem)
