Trait Ideas
===========

**Fashion Purist**

*Bags and backpacks are just soo gaudy. Hates wearing backpacks and bags.*

- Bags hold less, and cause unhappyness when worn.


**Weak Lungs**

*Breathing is chore..*

- Cough when low on endurance
- Auto apply to smokers, or if you smoke too often
